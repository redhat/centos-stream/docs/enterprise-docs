:_newdoc-version: 2.15.0
:_template-generated: 2023-10-31

ifdef::context[:parent-context-of-performing-the-upgrade: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="performing-the-upgrade"]
endif::[]
ifdef::context[]
[id="performing-the-upgrade_{context}"]
endif::[]
= Performing the upgrade

:context: performing-the-upgrade

[role="_abstract"]
After you have completed the preparatory steps and reviewed and resolved the problems found in the pre-upgrade report, you can perform the in-place upgrade on your system.

include::modules/upgrades-and-differences/proc_performing-the-upgrade-from-rhel-8-to-rhel-9.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/proc_performing-the-upgrade-from-rhel-8.8-to-rhel-9.adoc[leveloffset=+1]

ifdef::parent-context-of-performing-the-upgrade[:context: {parent-context-of-performing-the-upgrade}]
ifndef::parent-context-of-performing-the-upgrade[:!context:]

