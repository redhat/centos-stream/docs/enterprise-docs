ifdef::context[:parent-context-of-assembly_using-cgroupfs-to-manually-manage-cgroups: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_using-cgroupfs-to-manually-manage-cgroups"]
endif::[]
ifdef::context[]
[id="assembly_using-cgroupfs-to-manually-manage-cgroups_{context}"]
endif::[]
= Using cgroupfs to manually manage cgroups

:context: assembly_using-cgroupfs-to-manually-manage-cgroups

You can manage `cgroup` hierarchies on your system by creating directories on the `cgroupfs` virtual file system. The file system is mounted by default on the `/sys/fs/cgroup/` directory and you can specify desired configurations in dedicated control files.

[IMPORTANT]
====
In general, Red Hat recommends you use `systemd` for controlling the usage of system resources. You should manually configure the `cgroups` virtual file system only in special cases. For example, when you need to use `cgroup-v1` controllers that have no equivalents in `cgroup-v2` hierarchy.
====

include::modules/core-kernel/proc_creating-cgroups-and-enabling-controllers-in-cgroups-v2-file-system.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/proc_controlling-distribution-of-cpu-time-for-applications-by-adjusting-cpu-weight.adoc[leveloffset=+1]

include::modules/core-kernel/proc_mounting-cgroups-v1.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/proc_setting-cpu-limits-to-applications-using-cgroups-v1.adoc[leveloffset=+1]






ifdef::parent-context-of-assembly_using-cgroupfs-to-manually-manage-cgroups[:context: {parent-context-of-assembly_using-cgroupfs-to-manually-manage-cgroups}]
ifndef::parent-context-of-assembly_using-cgroupfs-to-manually-manage-cgroups[:!context:]
