:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_modifying-printer-settings: {context}]


ifndef::context[]
[id="assembly_modifying-printer-settings"]
endif::[]
ifdef::context[]
[id="assembly_modifying-printer-settings_{context}"]
endif::[]

= Modifying printer settings

:context: assembly_modifying-printer-settings

[role="_abstract"]
In GNOME, you can modify printer settings using the [application]*Settings* application.

.Prerequisites

* You have started Settings for setting up printing by following the procedure xref:starting-settings-for-setting-up-printing_assembly_setting-up-a-printer-in-gnome[Accessing printer settings in GNOME]

include::modules/desktop/proc_displaying-and-modifying-printers-details.adoc[leveloffset=+1]

include::modules/desktop/proc_setting-the-default-printer.adoc[leveloffset=+1]

include::modules/desktop/proc_setting-print-options.adoc[leveloffset=+1]

include::modules/desktop/proc_removing-a-printer.adoc[leveloffset=+1]


////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_modifying-printer-settings[:context: {parent-context-of-assembly_modifying-printer-settings}]
ifndef::parent-context-of-assembly_modifying-printer-settings[:!context:]
