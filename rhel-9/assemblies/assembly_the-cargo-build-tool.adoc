ifdef::context[:parent-context-of-assembly_the-cargo-build-tool: {context}]

ifndef::context[]
[id="assembly_the-cargo-build-tool"]
endif::[]
ifdef::context[]
[id="assembly_the-cargo-build-tool{context}"]
endif::[]
= The Cargo build tool

:context: assembly_the-cargo-build-tool

[role="_abstract"]
Cargo is a build tool and front end for the {comp} compiler `rustc` as well as a package and dependency manager.
//+
//[NOTE]
//====
//Consider using [application]#Cargo# over [application]#rustc#.
//====
//why?
//
It allows {comp} projects to declare dependencies with specific version requirements, resolves the full dependency graph, downloads packages, and builds as well as tests your entire project.

{comp} Toolset is distributed with Cargo {cargo-ver}.

include::modules/compiler-toolsets/rust/con_cargo-directory-structure-and-file-placements.adoc[leveloffset=+1] 
include::modules/compiler-toolsets/rust/proc_creating-a-rust-project.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_creating-a-library-for-a-rust-project.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_building-a-rust-project.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_building-a-rust-project-in-release-mode.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_running-a-rust-program.adoc[leveloffset=+1] 
include::modules/compiler-toolsets/rust/proc_testing-a-rust-project.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_testing-a-rust-project-in-release-mode.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_configuring-rust-project-dependencies.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_building-documentation-for-a-rust-project.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_installing-webassembly.adoc[leveloffset=+1]
include::modules/compiler-toolsets/rust/proc_vendoring-rust-project-dependencies.adoc[leveloffset=+1]

//include::topics/modules/[leveloffset=+1]

//include::modules/TEMPLATE_PROCEDURE_doing_one_procedure.adoc[leveloffset=+2]
//include::modules/TEMPLATE_PROCEDURE_reference-material.adoc[leveloffset=2]


[role="_additional-resources"]
[[sect-cargo-Resources]]
== Additional resources

* For more information on Cargo, see the link:http://doc.crates.io/guide.html[Official Cargo Guide].

* To display the manual page included in {comp} Toolset, run:
ifdef::publish-rhel-7[]
** For {RHEL}{nbsp}7:
+
[subs="quotes,attributes"]
----
$ scl enable {comp-pkg} 'man cargo'
----

endif::publish-rhel-7[]
ifdef::publish-rhel-8[]
** For {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ man cargo
----
endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
** For {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
$ man cargo
----
endif::publish-rhel-9[]


ifdef::parent-context-of-assembly_assembly_the-cargo-build-tool[:context: {parent-context-of-assembly_assembly_rust-cargo}]
ifndef::parent-context-of-assembly_assembly_the-cargo-build-tool[:!context:]

