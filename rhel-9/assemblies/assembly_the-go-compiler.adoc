ifdef::context[:parent-context-of-assembly_the-go-compiler: {context}]

ifndef::context[]
[id="assembly_the-go-compiler"]
endif::[]
ifdef::context[]
[id="assembly_the-go-compiler_{context}"]
endif::[]
= The Go compiler

:context: assembly_the-go-compiler


[role="_abstract"]
The Go compiler is a build tool and dependency manager for the Go programming language.
It offers error checking and optimization of your code.

== Prerequisites
* {comp} Toolset is installed. +
For more information, see xref:proc_installing-comp-toolset_assembly_go-toolset[Installing {comp} Toolset].

include::modules/compiler-toolsets/go/proc_setting-up-a-go-workspace.adoc[leveloffset=+1]
include::modules/compiler-toolsets/go/proc_compiling-a-go-program.adoc[leveloffset=+1]
include::modules/compiler-toolsets/go/proc_running-a-go-program.adoc[leveloffset=+1]
include::modules/compiler-toolsets/go/proc_installing-compiled-go-projects.adoc[leveloffset=+1]
include::modules/compiler-toolsets/go/proc_downloading-go-projects.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources 

* For more information on the Go compiler, see the link:https://golang.org/cmd/go/[official {comp} documentation].
* To display the [command]`help` index included in {comp} Toolset, run:
+
ifdef::publish-rhel-8[]
** On {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ go help
----
endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
** On {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
$ go help
----
endif::publish-rhel-9[]
* To display documentation for specific Go packages, run: +
ifdef::publish-rhel-8[]
** On {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ go doc &lt;__package_name__&gt;
----
endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
** On {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
$ go doc &lt;__package_name__&gt;
----
endif::publish-rhel-9[]
+
See https://golang.org/pkg/[{comp} packages] for an overview of {comp} packages.


ifdef::parent-context-of-assembly_the-go-compiler[:context: {parent-context-of-assembly_the-go-compiler}]
ifndef::parent-context-of-assembly_the-go-compiler[:!context:]

