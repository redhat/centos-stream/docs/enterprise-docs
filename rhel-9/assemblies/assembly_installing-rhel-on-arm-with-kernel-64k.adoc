ifdef::context[:parent-context-of-installing-rhel-on-arm-with-kernel-64k: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="installing-rhel-on-arm-with-kernel-64k"]
endif::[]
ifdef::context[]
[id="installing-rhel-on-arm-with-kernel-64k_{context}"]
endif::[]
= Installing {productshortname} on ARM with Kernel-64k

By default, {productshortname} {productNumber} is distributed  with a kernel supporting a 4k page size. This 4k kernel is sufficient for efficient memory usage in smaller environments or small cloud instances where the usage of a 64k page kernel is not practical due to space, power, and cost constraints.

[IMPORTANT]
====
It is not recommended to move between 4k and 64k page size kernels after the initial boot without reinstallation of the OS.
====
:context: installing-rhel-on-arm-with-kernel-64k

include::modules/installer/proc_installing-kernel-64k-on-arm-using-kickstart.adoc[leveloffset=+1]

include::modules/installer/proc_installing-kernel-64k-on-arm-using-the-command-line.adoc[leveloffset=+1]


ifdef::parent-context-of-installing-rhel-on-arm-with-kernel-64k[:context: {parent-context-of-installing-rhel-on-arm-with-kernel-64k}]
ifndef::parent-context-of-installing-rhel-on-arm-with-kernel-64k[:!context:]

