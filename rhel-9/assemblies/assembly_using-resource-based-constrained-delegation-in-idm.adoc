ifdef::context[:parent-context-of-using-resource-based-constrained-delegation-in-idm: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="using-resource-based-constrained-delegation-in-idm"]
endif::[]
ifdef::context[]
[id="using-resource-based-constrained-delegation-in-idm_{context}"]
endif::[]
= Using resource-based constrained delegation in IdM

:context: using-resource-based-constrained-delegation-in-idm

[role="_abstract"]
You can use resource-based constrained delegation (RBCD) to allow access to a service. Using RBCD allows a granular control of delegation on a resource level. Access can be set by the owner of the service to which credentials are delegated. This is useful, for example, in an integration between Identity Management (IdM) and Active Directory (AD).

Microsoft AD requires RBCD when both target and proxy services belong to different forests, as of 2019. 

[role="_additional-resources"]
.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/assembly_using-constrained-delegation-in-idm_configuring-and-managing-idm[Using constrained delegation in IdM]

include::modules/identity-management/con_resource-based-constrained-delegation-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-rbcd-to-delegate-access-to-a-service.adoc[leveloffset=+1]

ifdef::parent-context-of-using-resource-based-constrained-delegation-in-idm[:context: {parent-context-of-using-resource-based-constrained-delegation-in-idm}]
ifndef::parent-context-of-using-resource-based-constrained-delegation-in-idm[:!context:]

