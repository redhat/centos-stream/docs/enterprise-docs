:_newdoc-version: 2.15.0
:_template-generated: 2023-11-1

ifdef::context[:parent-context-of-runtime-verification-of-the-real-time-kernel: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="runtime-verification-of-the-real-time-kernel"]
endif::[]
ifdef::context[]
[id="runtime-verification-of-the-real-time-kernel_{context}"]
endif::[]
= Runtime verification of the real-time kernel

:context: runtime-verification-of-the-real-time-kernel

[role="_abstract"]
Runtime verification is a lightweight and rigorous method to check the behavioral equivalence between system events and their formal specifications. Runtime verification has monitors integrated in the kernel that attach to `tracepoints`. If a system state deviates from defined specifications, the runtime verification program activates reactors to inform or enable a reaction, such as capturing the event in log files or a system shutdown to prevent failure propagation in an extreme case.   

 


include::modules/rt-kernel/con_runtime-monitors-and-reactors.adoc[leveloffset=+1]

include::modules/rt-kernel/con_online-runtime-monitors.adoc[leveloffset=+1]

include::modules/rt-kernel/con_the-user-interface.adoc[leveloffset=+1]




ifdef::parent-context-of-runtime-verification-of-the-real-time-kernel[:context: {parent-context-of-runtime-verification-of-the-real-time-kernel}]
ifndef::parent-context-of-runtime-verification-of-the-real-time-kernel[:!context:]

