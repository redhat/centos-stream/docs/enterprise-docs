ifdef::context[:parent-context-of-creating-vmdk-images-for-rhel-for-edge: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="creating-vmdk-images-for-rhel-for-edge"]
endif::[]
ifdef::context[]
[id="creating-vmdk-images-for-rhel-for-edge_{context}"]
endif::[]
= Creating VMDK images for RHEL for Edge

:context: creating-vmdk-images-for-rhel-for-edge

[role="_abstract"]
You can create a  `.vmdk` image for RHEL for Edge by using the RHEL image builder. You can create an `edge-vsphere` image type with Ignition support, to inject the user configuration into the image at an early stage of the boot process. Then, you can load the image on vSphere and boot the image in a vSphere VM. The image is compatible with ESXi 7.0 U2, ESXi 8.0 and later. The vSphere VM is compatible with version 19 and 20.

include::modules/edge/proc_creating-a-blueprint-with-the-ignition-configuration.adoc[leveloffset=+1]

include::modules/edge/proc_creating-a-vmdk-image-for-rhel-for-edge.adoc[leveloffset=+1]

include::modules/edge/proc_uploading-vmdk-images-and-creating-a-rhel-virtual-machine-in-vsphere.adoc[leveloffset=+1]

ifdef::parent-context-of-creating-vmdk-images-for-rhel-for-edge[:context: {parent-context-of-creating-vmdk-images-for-rhel-for-edge}]
ifndef::parent-context-of-creating-vmdk-images-for-rhel-for-edge[:!context:]

