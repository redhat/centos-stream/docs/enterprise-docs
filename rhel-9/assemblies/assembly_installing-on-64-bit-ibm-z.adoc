

ifdef::context[:parent-context-of-assembly_installing-on-64-bit-ibm-z: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_installing-on-64-bit-ibm-z"]
endif::[]
ifdef::context[]
[id="assembly_installing-on-64-bit-ibm-z_{context}"]
endif::[]
= Installing {ProductName} on 64-bit IBM Z


:context: assembly_installing-on-64-bit-ibm-z

You can install {ProductName} on the 64-bit IBM Z architecture.


include::rhel-8/modules/installer/con_planning-for-installation-on-ibm-z.adoc[leveloffset=+1]

include::rhel-8/modules/installer/con_overview-of-the-ibm-z-installation-process.adoc[leveloffset=+1]

include::rhel-8/modules/installer/con_booting-the-installation.adoc[leveloffset=+1]

include::rhel-8/modules/installer/con_customizing-boot-parameters.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_parameters-and-configuration-files-on-ibm-z.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-in-an-lpar.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-under-z-vm.adoc[leveloffset=+1]

include::rhel-8/modules/installer/proc_installing-under-kvm.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-a-linux-instance-on-ibm-z.adoc[leveloffset=+1]

include::assembly_booting-a-system-with-uefi-secure-boot.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_installing-on-64-bit-ibm-z[:context: {parent-context-of-assembly_installing-on-64-bit-ibm-z}]
ifndef::parent-context-of-assembly_installing-on-64-bit-ibm-z[:!context:]
