ifdef::context[:parent-context-of-idm-api-example-scenarios: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="idm-api-example-scenarios"]
endif::[]
ifdef::context[]
[id="idm-api-example-scenarios_{context}"]
endif::[]
= IdM API example scenarios

:context: idm-api-example-scenarios

[role="_abstract"]

The following examples provide you with the common scenarios of using IdM API commands.

include::modules/identity-management/ref_managing-users-with-idm-api-commands.adoc[leveloffset=+1]

include::modules/identity-management/ref_managing-groups-with-idm-api-commands.adoc[leveloffset=+1]

include::modules/identity-management/ref_managing-access-control-with-idm-api-commands.adoc[leveloffset=+1]

include::modules/identity-management/ref_managing-sudo-rules-with-idm-api-commands.adoc[leveloffset=+1]

include::modules/identity-management/ref_managing-host-based-access-control-with-idm-api-commands.adoc[leveloffset=+1]

ifdef::parent-context-of-idm-api-example-scenarios[:context: {parent-context-of-idm-api-example-scenarios}]
ifndef::parent-context-of-idm-api-example-scenarios[:!context:]

