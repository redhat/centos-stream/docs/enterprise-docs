:_newdoc-version: 2.18.3
:_template-generated: 2024-11-18

ifdef::context[:parent-context-of-using-the-sudo-system-role: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="using-the-sudo-system-role"]
endif::[]
ifdef::context[]
[id="using-the-sudo-system-role_{context}"]
endif::[]
= Using the sudo system role

:context: using-the-sudo-system-role

As an administrator, you can consistently configure the `/etc/sudoers` files on multiple systems by using the `sudo` RHEL system role.

include::modules/security/proc_apply-custom-sudoers-configuration-by-using-system-roles.adoc[leveloffset=+1]

ifdef::parent-context-of-using-the-sudo-system-role[:context: {parent-context-of-using-the-sudo-system-role}]
ifndef::parent-context-of-using-the-sudo-system-role[:!context:]

