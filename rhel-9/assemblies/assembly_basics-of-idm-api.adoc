ifdef::context[:parent-context-of-assembly_basics-of-idm-api: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_basics-of-idm-api"]
endif::[]
ifdef::context[]
[id="assembly_basics-of-idm-api_{context}"]
endif::[]
= Basics of IdM API

:context: assembly_basics-of-idm-api

You can use the IdM API to automate the access to IdM environment with your custom scripts.

include::modules/identity-management/proc_initializing-idm-api.adoc[leveloffset=+1]

include::modules/identity-management/proc_running-idm-api-commands.adoc[leveloffset=+1]

include::modules/identity-management/con_idm-api-commands-output-structure.adoc[leveloffset=+1]

include::modules/identity-management/proc_listing-the-idm-api-commands-and-parameters.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-batches-for-executing-idm-api-commands.adoc[leveloffset=+1]

include::modules/identity-management/ref_idm-api-context.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_basics-of-idm-api[:context: {parent-context-of-assembly_basics-of-idm-api}]
ifndef::parent-context-of-assembly_basics-of-idm-api[:!context:]

