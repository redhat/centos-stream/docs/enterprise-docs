:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-setting-up-graphical-representation-of-pcp-metrics: {context}]

ifndef::context[]
[id="setting-up-graphical-representation-of-pcp-metrics"]
endif::[]
ifdef::context[]
[id="setting-up-graphical-representation-of-pcp-metrics_{context}"]
endif::[]
= Setting up graphical representation of PCP metrics

:context: setting-up-graphical-representation-of-pcp-metrics


[role="_abstract"]
Using a combination of `pcp`, `grafana`, `pcp redis`, `pcp bpftrace`, and `pcp vector` provides graphical representation of the live data or data collected by Performance Co-Pilot (PCP).


include::rhel-8/modules/performance/proc_setting-up-pcp-with-pcp-zeroconf.adoc[leveloffset=+1]

include::modules/performance/proc_setting-up-a-grafana-server.adoc[leveloffset=+1]

include::modules/performance/proc_accessing-the-grafana-web-ui.adoc[leveloffset=+1]

include::modules/performance/proc_configuring-secure-connections-for-grafana.adoc[leveloffset=+1]

include::modules/performance/proc_configuring-pcp-redis.adoc[leveloffset=+1]

//include::modules/performance/proc_configuring-secure-connections-for-pcp-redis.adoc[leveloffset=+1]

include::modules/performance/proc_creating-panels-and-alerts-in-pcp-redis-data-source.adoc[leveloffset=+1]

include::modules/performance/proc_adding-notification-channels-for-alerts.adoc[leveloffset=+1]

include::rhel-8/modules/performance/proc_setting-up-authentication-between-pcp-components.adoc[leveloffset=+1]

include::modules/performance/proc_installing-pcp-bpftrace.adoc[leveloffset=+1]

include::modules/performance/proc_viewing-the-pcp-bpftrace-system-analysis-dashboard.adoc[leveloffset=+1]

include::modules/performance/proc_installing-pcp-vector.adoc[leveloffset=+1]

include::modules/performance/proc_viewing-the-pcp-vector-checklist.adoc[leveloffset=+1]

include::modules/performance/proc_using-heatmaps-in-grafana.adoc[leveloffset=+1]

include::rhel-8/modules/performance/proc_troubleshooting-grafana-issues.adoc[leveloffset=+1]

ifdef::parent-context-of-setting-up-graphical-representation-of-pcp-metrics[:context: {parent-context-of-setting-up-graphical-representation-of-pcp-metrics}]
ifndef::parent-context-of-setting-up-graphical-representation-of-pcp-metrics[:!context:]
