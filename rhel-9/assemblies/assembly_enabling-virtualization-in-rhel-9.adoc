ifdef::context[:parent-context-of-assembly_enabling-virtualization-in-rhel-9: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_enabling-virtualization-in-rhel-9"]
endif::[]
ifdef::context[]
[id="assembly_enabling-virtualization-in-rhel-9_{context}"]
endif::[]
= Enabling virtualization

:context: assembly_enabling-virtualization-in-rhel-9

[role="_abstract"]
To use virtualization in RHEL 9, you must install virtualization packages and ensure your system is configured to host virtual machines (VMs). The specific steps to do this vary based on your CPU architecture.

////
== Prerequisites

* <Detailed in individual modules>
////

include::modules/virtualization/proc_enabling-virtualization-on-amd64-and-intel-64.adoc[leveloffset=+1]

include::rhel-8/modules/virtualization/proc_enabling-virtualization-on-ibm-z.adoc[leveloffset=+1]

include::rhel-8/modules/virtualization/proc_enabling-virtualization-on-arm-64.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_enabling-qemu-guest-agent-features-on-your-virtual-machines.adoc[leveloffset=+1]

////
[role="_additional-resources"]
== Additional resources (or Next steps)

* TBA?
////

ifdef::parent-context-of-assembly_enabling-virtualization-in-rhel-9[:context: {parent-context-of-assembly_enabling-virtualization-in-rhel-9}]
ifndef::parent-context-of-assembly_enabling-virtualization-in-rhel-9[:!context:]
