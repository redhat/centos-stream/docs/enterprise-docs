:_mod-docs-content-type: ASSEMBLY
//include::modules/core-services/attributes-core-services.adoc[]

:parent-context-of-using-mariadb: {context}

[id='using-mariadb_{context}']
= Using MariaDB

:context: using-mariadb

The *MariaDB* server is an open source fast and robust database server that is based on the *MySQL* technology. *MariaDB* is a relational database that converts data into structured information and provides an SQL interface for accessing data. It includes multiple storage engines and plugins, as well as geographic information system (GIS) and JavaScript Object Notation (JSON) features.

Learn how to install and configure *MariaDB* on a RHEL system, how to back up *MariaDB* data, how to migrate from an earlier *MariaDB* version, and how to replicate a database using the *MariaDB Galera Cluster*.

include::modules/core-services/proc_installing-mariadb.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_running-multiple-mariadb-versions-in-containers.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/proc_configuring-mariadb.adoc[leveloffset=+1]

include::assembly_setting-up-tls-encryption-on-a-mariadb-server.adoc[leveloffset=+1]

include::assembly_globally-enabling-tls-encryption-in-mariadb-clients.adoc[leveloffset=+1]

include::assembly_backing-up-mariadb-data.adoc[leveloffset=+1]

include::assembly_migrating-to-mariadb-10-5.adoc[leveloffset=+1]

include::assembly_upgrading-from-mariadb-10-5-to-mariadb-10-11.adoc[leveloffset=+1]

include::assembly_replicating-mariadb-with-galera.adoc[leveloffset=+1]

include::rhel-8/modules/core-services/con_developing-mariadb-client-applications.adoc[leveloffset=+1]
:context: {parent-context-of-using-mariadb}
