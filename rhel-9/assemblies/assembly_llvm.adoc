ifdef::context[:parent-context-of-assembly_llvm: {context}]

ifndef::context[]
[id="assembly_llvm"]
endif::[]
ifdef::context[]
[id="assembly_llvm_{context}"]
endif::[]
= {comp} Toolset
:context: assembly_llvm

[role="_abstract"]

{comp} Toolset is a Red Hat offering for developers on {RHEL} (RHEL).
It provides the LLVM compiler infrastructure framework, the Clang compiler for the C and {cpp} languages, the LLDB debugger, and related tools for code analysis.

ifdef::publish-rhel-7[{comp} Toolset is distributed as a part of {RHDT} for {RHEL}{nbsp}7.]
ifdef::publish-rhel-8[For {RHEL}{nbsp}8, {comp} Toolset is available as a module.]
ifdef::publish-rhel-9[{comp} Toolset is available as packages for {RHEL}{nbsp}9.]

include::modules/compiler-toolsets/llvm/ref_llvm-components.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/ref_compatibility.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_installing-comp-toolset.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_installing-cmake.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_installing-llvm-documentation.adoc[leveloffset=+1]
include::modules/compiler-toolsets/llvm/proc_installing-cmake-documentation.adoc[leveloffset=+1]
[role="_additional-resources"]
== Additional resources 

* For more information on {comp} Toolset, see the link:http://llvm.org/docs/[official {comp} documentation].

ifdef::parent-context-of-assembly_llvm[:context: {parent-context-of-assembly_llvm}]
ifndef::parent-context-of-assembly_llvm[:!context:]

