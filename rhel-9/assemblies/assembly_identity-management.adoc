:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_identity-management: {context}]


ifndef::context[]
[id="assembly_identity-management"]
endif::[]
ifdef::context[]
[id="assembly_identity-management_{context}"]
endif::[]
= Identity Management


:context: assembly_identity-management



[role="_abstract"]
The following chapters contain the most notable changes to Identity Management (IdM) between RHEL 8 and RHEL 9.


include::modules/upgrades-and-differences/ref_new-features-identity-management.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_technology-previews-identity-management.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_known-issues-identity-management.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_relocated-packages-identity-management.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_removed-functionality-identity-management.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_identity-management[:context: {parent-context-of-assembly_identity-management}]
ifndef::parent-context-of-assembly_identity-management[:!context:]
