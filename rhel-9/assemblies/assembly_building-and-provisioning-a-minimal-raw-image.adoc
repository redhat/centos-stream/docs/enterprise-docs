ifdef::context[:parent-context-of-building-and-provisioning-a-minimal-raw-image: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="building-and-provisioning-a-minimal-raw-image"]
endif::[]
ifdef::context[]
[id="building-and-provisioning-a-minimal-raw-image_{context}"]
endif::[]
= Building and provisioning a minimal raw image

:context: building-and-provisioning-a-minimal-raw-image

[role="_abstract"]
The `minimal-raw` image is a pre-packaged, bootable, minimal RPM image, compressed in the `xz` format. The image consists of a file containing a partition layout with an existing deployed OSTree commit in it.
You can build a RHEL for Edge Minimal Raw image type by using RHEL image builder and deploy the Minimal Raw image to the `aarch64` and `x86` architectures. 

include::modules/edge/con_the-minimal-raw-image-build-and-deployment.adoc[leveloffset=+1]

include::modules/edge/proc_creating-the-blueprint-for-a-minimal-raw-image-using-image-builder-cli.adoc[leveloffset=+1]

include::modules/edge/proc_creating-a-rhel-for-edge-minimal-raw-image-using-image-builder-cli.adoc[leveloffset=+1]

include::modules/edge/proc_downloading-and-decompressing-the-minimal-raw-image.adoc[leveloffset=+1]

include::modules/edge/proc_deploying-the-minimal-raw-image-from-a-usb-flash-drive.adoc[leveloffset=+1]

include::modules/edge/proc_serving-a-rhel-for-edge-container-image-to-build-a-rhel-for-edge-raw-image.adoc[leveloffset=+1]

ifdef::parent-context-of-building-and-provisioning-a-minimal-raw-image[:context: {parent-context-of-building-and-provisioning-a-minimal-raw-image}]
ifndef::parent-context-of-building-and-provisioning-a-minimal-raw-image[:!context:]

