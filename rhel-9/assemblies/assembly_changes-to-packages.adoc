:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_changes-to-packages: {context}]


ifndef::context[]
[appendix,id="assembly_changes-to-packages"]
endif::[]
ifdef::context[]
[appendix,id="assembly_changes-to-packages_{context}"]
endif::[]
= Changes to packages


:context: assembly_changes-to-packages


[role="_abstract"]
The following chapters contain changes to packages between RHEL 8 and RHEL 9, as well as changes between minor releases of RHEL 9.

include::modules/upgrades-and-differences/ref_new-packages.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_package-replacements.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_moved-packages.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_removed-packages.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_packages-with-removed-support.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_changes-to-packages[:context: {parent-context-of-assembly_changes-to-packages}]
ifndef::parent-context-of-assembly_changes-to-packages[:!context:]

