ifdef::context[:parent-context-of-assembly_using-systemd-to-manage-resources-used-by-applications: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_using-systemd-to-manage-resources-used-by-applications"]
endif::[]
ifdef::context[]
[id="assembly_using-systemd-to-manage-resources-used-by-applications_{context}"]
endif::[]
= Using systemd to manage resources used by applications

:context: assembly_using-systemd-to-manage-resources-used-by-applications

RHEL 9 moves the resource management settings from the process level to the application level by binding the system of `cgroup` hierarchies with the `systemd` unit tree.
Therefore, you can manage the system resources with the `systemctl` command, or by modifying the `systemd` unit files.

To achieve this, `systemd` takes various configuration options from the unit files or directly via the `systemctl` command.
Then `systemd` applies those options to specific process groups by using the Linux kernel system calls and features like `cgroups` and `namespaces`.

[NOTE]
====
You can review the full set of configuration options for `systemd` in the following manual pages:

* `systemd.resource-control(5)`
* `systemd.exec(5)`

====

////
commenting out, BZ#2097323
Certain unit file options can have more variants. For example `MemoryMax=` and `MemoryLimit=`. These variants relate to applying the configuration in `cgroup-v2` or `cgroup-v1` hierarchy. The `systemd` system and service manager functions as an abstraction layer and translates the two versions of settings. To avoid any possible inconveniences, Red Hat recommends that you use the unit file option that is relevant for the version of the cgroup hierarchy you use on the system.

////
include::modules/core-kernel/con_role-of-systemd-in-resource-management.adoc[leveloffset=+1]

include::modules/core-kernel/con_distribution-models-of-system-sources.adoc[leveloffset=+1]

include::modules/core-kernel/proc_allocating-system-resources-using-systemd.adoc[leveloffset=+1]

include::modules/core-kernel/con_overview-of-systemd-hierarchy-for-cgroups.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/proc_listing-systemd-units.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/proc_viewing-systemd-control-group-hierarchy.adoc[leveloffset=+1]

include::modules/core-kernel/proc_viewing-cgroups-of-processes.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/proc_monitoring-resource-consumption.adoc[leveloffset=+1]

include::modules/core-kernel/proc_using-systemd-unit-files-to-set-limits-for-applications.adoc[leveloffset=+1]

include::modules/core-kernel/proc_using-systemctl-command-to-set-limits-to-applications.adoc[leveloffset=+1]

include::modules/core-kernel/proc_setting-global-default-cpu-affinity-through-manager-configuration.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/proc_configuring-numa-using-systemd.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/ref_numa-policy-configuration-options-with-systemd.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/proc_creating-transient-control-groups.adoc[leveloffset=+1]

include::rhel-8/modules/core-kernel/proc_removing-transient-control-groups.adoc[leveloffset=+1]



ifdef::parent-context-of-assembly_using-systemd-to-manage-resources-used-by-applications[:context: {parent-context-of-assembly_using-systemd-to-manage-resources-used-by-applications}]
ifndef::parent-context-of-assembly_using-systemd-to-manage-resources-used-by-applications[:!context:]
