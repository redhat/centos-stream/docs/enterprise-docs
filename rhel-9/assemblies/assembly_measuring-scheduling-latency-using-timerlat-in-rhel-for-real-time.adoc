ifdef::context[:parent-context-of-measuring-scheduling-latency-using-timerlat-in-rhel-for-real-time: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="measuring-scheduling-latency-using-timerlat-in-rhel-for-real-time"]
endif::[]
ifdef::context[]
[id="measuring-scheduling-latency-using-timerlat-in-rhel-for-real-time_{context}"]
endif::[]
= Measuring scheduling latency using timerlat in RHEL for Real Time

:context: measuring-scheduling-latency-using-timerlat-in-rhel-for-real-time

The `rtla-timerlat` tool is an interface for the `timerlat` tracer. The `timerlat` tracer finds sources of wake-up latencies for real-time threads. The `timerlat` tracer creates a kernel thread per CPU with a real-time priority and these threads set a periodic timer to wake up and go back to sleep. On a wake up, `timerlat` finds and collects information, which is useful to debug operating system timer latencies. The `timerlat` tracer generates an output and prints the following two lines at every activation:

* The `timerlat` tracer periodically prints the timer latency seen at timer interrupt requests (IRQs) handler. This is the first output seen at the `hardirq` context before a thread activation. 
* The second output is the timer latency of a thread. The `ACTIVATION ID` field displays the interrupt requests (IRQs) performance to its respective thread execution.


include::modules/rt-kernel/proc_configuring-the-timerlat-tracer-to-measure-scheduling-latency.adoc[leveloffset=+1]

include::modules/rt-kernel/ref_the-timerlat-tracer-options.adoc[leveloffset=+1]

include::modules/rt-kernel/proc_measuring-timer-latency-with-rtla-timerlat-top.adoc[leveloffset=+1]

include::modules/rt-kernel/ref_the-rtla-timerlat-top-tracer-options.adoc[leveloffset=+1]




ifdef::parent-context-of-measuring-scheduling-latency-using-timerlat-in-rhel-for-real-time[:context: {parent-context-of-measuring-scheduling-latency-using-timerlat-in-rhel-for-real-time}]
ifndef::parent-context-of-measuring-scheduling-latency-using-timerlat-in-rhel-for-real-time[:!context:]

