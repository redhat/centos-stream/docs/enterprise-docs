ifdef::context[:parent-context-of-assembly_changes-in-rust-toolset: {context}]

ifndef::context[]
[id="assembly_changes-in-rust-toolset"]
endif::[]
ifdef::context[]
[id="assembly_changes-in-rust-toolset_{context}"]
endif::[]

= Changes in {comp} {comp-ver} Toolset

:context: assembly_changes-in-rust-toolset

//[role="_abstract"]

include::modules/compiler-toolsets/rust/ref_changes-in-rust-toolset.adoc[leveloffset=+1]

//[role="_additional-resources"]
//== Additional resources 

ifdef::parent-context-of-assembly_changes-in-rust-toolset[:context: {parent-context-of-assembly_changes-in-rust-toolset}]
ifndef::parent-context-of-assembly_changes-in-rust-toolset[:!context:]

