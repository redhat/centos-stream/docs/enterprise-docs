:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_setting-up-tls-encryption-on-a-mariadb-server: {context}]

ifndef::context[]
[id="assembly_setting-up-tls-encryption-on-a-mariadb-server"]
endif::[]
ifdef::context[]
[id="assembly_setting-up-tls-encryption-on-a-mariadb-server_{context}"]
endif::[]
= Setting up TLS encryption on a MariaDB server

:context: assembly_setting-up-tls-encryption-on-a-mariadb-server

[role="_abstract"]
By default, *MariaDB* uses unencrypted connections. For secure connections, enable TLS support on the *MariaDB* server and configure your clients to establish encrypted connections.

include::rhel-8/modules/core-services/proc_placing-the-ca-certificate-server-certificate-and-private-key-on-the-mariadb-server.adoc[leveloffset=+1]

include::modules/core-services/proc_configuring-tls-on-a-mariadb-server.adoc[leveloffset=+1]

include::modules/core-services/proc_requiring-tls-encrypted-connections-for-specific-user-accounts.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_setting-up-tls-encryption-on-a-mariadb-server[:context: {parent-context-of-assembly_setting-up-tls-encryption-on-a-mariadb-server}]
ifndef::parent-context-of-assembly_setting-up-tls-encryption-on-a-mariadb-server[:!context:]
