:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-assembly_installing-rhel-9-content: {context}]


ifndef::context[]
[id="assembly_installing-rhel-9-content"]
endif::[]
ifdef::context[]
[id="assembly_installing-rhel-9-content_{context}"]
endif::[]
= Installing RHEL 9 content

:context: assembly_installing-rhel-9-content


[role="_abstract"]
In the following sections, learn how to install content in {RHEL9} by using *{PackageManagerName}*. 

 
include::modules/core-services/proc_installing-packages.adoc[leveloffset=+1]

include::modules/core-services/proc_installing-package-groups.adoc[leveloffset=+1]

// include::modules/core-services/proc_specifying-a-package-name-in-yum-input.adoc[leveloffset=+1]

include::modules/core-services/proc_installing-modular-content.adoc[leveloffset=+1]

include::modules/core-services/proc_defining-custom-default-module-streams-and-profiles.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* xref:ref_commands-for-installing-content-in-rhel-9_assembly_yum-commands-list[Commands for installing content in RHEL 9]


ifdef::parent-context-of-assembly_installing-rhel-9-content[:context: {parent-context-of-assembly_installing-rhel-9-content}]
ifndef::parent-context-of-assembly_installing-rhel-9-content[:!context:]

