:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_supplementary-topics-in-rhel-9: {context}]

ifndef::context[]
[id="assembly_supplementary-topics-in-rhel-9"]
endif::[]
ifdef::context[]
[id="assembly_supplementary-topics-in-rhel-9_{context}"]
endif::[]
= Supplementary topics

:context: assembly_supplementary-topics-in-rhel-9

[role="_abstract"]
include::modules/platform-tools/ref_compatability-breaking-changes-in-compilers-and-development-tools.adoc[leveloffset=+1]



//== Prerequisites

//[role="_additional-resources"]
//== Additional resources (or Next steps)

ifdef::parent-context-of-assembly_supplementary-topics-in-rhel-9[:context: {parent-context-of-assembly_supplementary-topics-in-rhel-9}]
ifndef::parent-context-of-assembly_supplementary-topics-in-rhel-9[:!context:]

