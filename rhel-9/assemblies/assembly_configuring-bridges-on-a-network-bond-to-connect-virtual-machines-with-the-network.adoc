:_newdoc-version: 2.17.0
:_template-generated: 2024-05-23

ifdef::context[:parent-context-of-configuring-bridges-on-a-network-bond-to-connect-vms-with-the-network: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-bridges-on-a-network-bond-to-connect-virtual-machines-with-the-network"]
endif::[]
ifdef::context[]
[id="configuring-bridges-on-a-network-bond-to-connect-virtual-machines-with-the-network_{context}"]
endif::[]
= Configuring bridges on a network bond to connect virtual machines with the network

:context: configuring-bridges-on-a-network-bond-to-connect-vms-with-the-network

[role="_abstract"]

The network bridge connects VMs with the same network as the host. If you want to connect VMs on one host to another host or VMs on another host, a bridge establishes communication between them. However, the bridge does not provide a fail-over mechanism. To handle failures in communication, a network bond handles communication in the event of failure of a network interface. To maintain fault tolerance and redundancy, the `active-backup` bonding mechanism determines that only one port is active in the bond and does not require any switch configuration. If an active port fails, an alternate port becomes active to retain communication between configured VMs in the network.

include::modules/proc_configuring-network-interfaces-on-a-network-bond-by-using-nmcli.adoc[leveloffset=+1]

include::modules/proc_configuring-a-network-bridge-for-network-bonds-by-using-nmcli.adoc[leveloffset=+1]

include::modules/proc_creating-a-virtual-network-in-libvirt-with-an-existing-bond-interface.adoc[leveloffset=+1]

include::modules/proc_configuring-virtual-machines-to-use-a-bond-interface.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-bridges-on-a-network-bond-to-connect-vms-with-the-network[:context: {parent-context-of-configuring-bridges-on-a-network-bond-to-connect-vms-with-the-network}]
ifndef::parent-context-of-configuring-bridges-on-a-network-bond-to-connect-vms-with-the-network[:!context:]

