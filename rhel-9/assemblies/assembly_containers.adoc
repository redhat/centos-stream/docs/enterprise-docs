:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_containers: {context}]



ifndef::context[]
[id="assembly_containers"]
endif::[]
ifdef::context[]
[id="assembly_containers_{context}"]
endif::[]
= Containers


:context: assembly_containers


[role="_abstract"]
The following chapter contains the most notable changes to containers between RHEL 8 and RHEL 9.

//Include modules here.
include::modules/upgrades-and-differences/ref_changes-to-containers.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_containers[:context: {parent-context-of-assembly_containers}]
ifndef::parent-context-of-assembly_containers[:!context:]
