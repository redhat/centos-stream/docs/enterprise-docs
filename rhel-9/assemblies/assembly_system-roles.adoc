:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_system-roles: {context}]


ifndef::context[]
[id="assembly_system-roles"]
endif::[]
ifdef::context[]
[id="assembly_system-roles_{context}"]
endif::[]
= System roles


:context: assembly_system-roles


[role="_abstract"]
The following chapter contains the most notable changes to system roles between RHEL 8 and RHEL 9.


include::modules/upgrades-and-differences/ref_changes-to-system-roles.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_system-roles[:context: {parent-context-of-assembly_system-roles}]
ifndef::parent-context-of-assembly_system-roles[:!context:]

