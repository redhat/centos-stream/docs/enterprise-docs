:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_compilers-and-development-tools: {context}]



ifndef::context[]
[id="assembly_compilers-and-development-tools"]
endif::[]
ifdef::context[]
[id="assembly_compilers-and-development-tools_{context}"]
endif::[]
= Compilers and development tools


:context: assembly_compilers-and-development-tools



[role="_abstract"]
The following chapter contains the most notable changes to compilers and development tools between RHEL 8 and RHEL 9.

include::modules/upgrades-and-differences/ref_changes-to-glibc.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_red-hat-build-of-openjdk.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_dotnet-on-rhel-9.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_compilers-and-development-tools[:context: {parent-context-of-assembly_compilers-and-development-tools}]
ifndef::parent-context-of-assembly_compilers-and-development-tools[:!context:]
