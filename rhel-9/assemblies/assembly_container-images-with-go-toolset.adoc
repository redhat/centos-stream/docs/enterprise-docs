ifdef::context[:parent-context-of-assembly_container-images-with-go-toolset: {context}]

ifndef::context[]
[id="assembly_container-images-with-go-toolset"]
endif::[]
ifdef::context[]
[id="assembly_container-images-with-go-toolset_{context}"]
endif::[]
= Container images with Go Toolset

:context: assembly_container-images-with-go-toolset

[role="_abstract"]
You can build your own {comp} Toolset containers from either {RHEL} container images or Red Hat Universal Base Images (UBI). 
//== Prerequisites

include::modules/compiler-toolsets/go/ref_rhel-container-images-contents.adoc[leveloffset=+1]
include::modules/compiler-toolsets/go/proc_accessing-container-images.adoc[leveloffset=+1]
ifdef::publish-rhel-8[]
include::modules/compiler-toolsets/go/proc_accessing-the-ubi-go-toolset-container-image.adoc[leveloffset=+1]
ifdef::publish-rhel-8[]
ifdef::publish-rhel-8[]
include::modules/compiler-toolsets/go/proc_accessing-ubi-container-images.adoc[leveloffset=+1]
endif::publish-rhel-8[]

[role="_additional-resources"]
== Additional resources 

* link:https://catalog.redhat.com/software/containers/search?q=go-toolset&p=1[{comp} Toolset Container Images in the Red Hat Container Registry].

* For more information on Red Hat UBI images, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/building_running_and_managing_containers/index/#assembly_working-with-container-images_building-running-and-managing-containers[Working with Container Images].

* For more information on Red Hat UBI repositories, see link:https://access.redhat.com/articles/4238681[Universal Base Images (UBI): Images, repositories, packages, and source code].

ifdef::parent-context-of-assembly_container-images-with-go-toolset[:context: {parent-context-of-assembly_container-images-with-go-toolset}]
ifndef::parent-context-of-assembly_container-images-with-go-toolset[:!context:]

