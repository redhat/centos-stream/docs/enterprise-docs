

ifdef::context[:parent-context-of-assembly_installing-on-amd64-intel-64-and-64-bit-arm: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_installing-on-amd64-intel-64-and-64-bit-arm"]
endif::[]
ifdef::context[]
[id="assembly_installing-on-amd64-intel-64-and-64-bit-arm_{context}"]
endif::[]
= Installing {ProductName} on AMD64, Intel 64, and 64-bit ARM


:context: assembly_installing-on-amd64-intel-64-and-64-bit-arm

You can install {ProductName} on AMD64, Intel 64, and 64-bit ARM systems by using the graphical user interface.


include::rhel-8/modules/installer/con_preparing-for-your-installation-workflow.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_booting-installer.adoc[leveloffset=+1]

include::rhel-8/modules/installer/proc_performing-a-quick-install-with-gui.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_register-and-install-from-cdn-gui.adoc[leveloffset=+1]

include::assembly_rhel-9-post-installation-tasks.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_installing-on-amd64-intel-64-and-64-bit-arm[:context: {parent-context-of-assembly_installing-on-amd64-intel-64-and-64-bit-arm}]
ifndef::parent-context-of-assembly_installing-on-amd64-intel-64-and-64-bit-arm[:!context:]
