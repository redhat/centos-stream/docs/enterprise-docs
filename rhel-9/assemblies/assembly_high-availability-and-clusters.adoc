:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_high-availability-and-clusters: {context}]



ifndef::context[]
[id="assembly_high-availability-and-clusters"]
endif::[]
ifdef::context[]
[id="assembly_high-availability-and-clusters_{context}"]
endif::[]
= High availability and clusters

:context: assembly_high-availability-and-clusters



[role="_abstract"]
The following chapter contains the most notable changes to high availability and clusters between RHEL 8 and RHEL 9.


//Include modules here.

include::modules/upgrades-and-differences/ref_changes-to-haclusters.adoc[leveloffset=+1]



ifdef::parent-context-of-assembly_high-availability-and-clusters[:context: {parent-context-of-assembly_high-availability-and-clusters}]
ifndef::parent-context-of-assembly_high-availability-and-clusters[:!context:]

