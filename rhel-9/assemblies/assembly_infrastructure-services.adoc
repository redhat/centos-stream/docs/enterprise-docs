:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-assembly_infrastructure-services: {context}]


ifndef::context[]
[id="assembly_infrastructure-services"]
endif::[]
ifdef::context[]
[id="assembly_infrastructure-services_{context}"]
endif::[]
= Infrastructure services


:context: assembly_infrastructure-services


[role="_abstract"]
The following chapter contains the most notable changes to infrastructure services between RHEL 8 and RHEL 9.

include::modules/upgrades-and-differences/ref_changes-to-infrastructure.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_infrastructure-services[:context: {parent-context-of-assembly_infrastructure-services}]
ifndef::parent-context-of-assembly_infrastructure-services[:!context:]

