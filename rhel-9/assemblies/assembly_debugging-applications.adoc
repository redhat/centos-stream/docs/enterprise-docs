:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of_debugging-applications: {context}]

ifndef::context[]
[id="debugging-applications"]
endif::[]
ifdef::context[]
[id="debugging-applications_{context}"]
endif::[]
= Debugging Applications

:context: debugging-applications

[role="_abstract"]
Debugging applications is a very wide topic. This part provides a developer with the most common techniques for debugging in multiple situations.

include::rhel-8/assemblies/assembly_enabling-debugging-with-debugging-information.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_inspecting-application-internal-state-with-gdb.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_recording-application-interactions.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_debugging-a-crashed-application.adoc[leveloffset=+1]

include::modules/platform-tools/ref_compatability-breaking-changes-in-gdb.adoc[leveloffset=+1]

include::rhel-8/modules/platform-tools/ref_debugging-applications-in-containers.adoc[leveloffset=+1]

//== Prerequisites

//[role="_additional-resources"]
//== Additional resources (or Next steps)

ifdef::parent-context-of_debugging-applications[:context: {parent-context-of_debugging-applications}]
ifndef::parent-context-of_debugging-applications[:!context:]

