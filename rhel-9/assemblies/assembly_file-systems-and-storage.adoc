:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_file-systems-and-storage: {context}]


ifndef::context[]
[id="assembly_file-systems-and-storage"]
endif::[]
ifdef::context[]
[id="assembly_file-systems-and-storage_{context}"]
endif::[]
= File systems and storage


:context: assembly_file-systems-and-storage


[role="_abstract"]
The following chapters contain the most notable changes to file systems and storage between RHEL 8 and RHEL 9.

//Include modules here.
include::modules/upgrades-and-differences/ref_file-systems-file-systems-and-storage.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_storage-file-systems-and-storage.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_file-systems-and-storage[:context: {parent-context-of-assembly_file-systems-and-storage}]
ifndef::parent-context-of-assembly_file-systems-and-storage[:!context:]
