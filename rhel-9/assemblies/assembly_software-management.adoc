:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_software-management: {context}]

ifndef::context[]
[id="assembly_software-management"]
endif::[]
ifdef::context[]
[id="assembly_software-management_{context}"]
endif::[]
= Software management

:context: assembly_software-management

[role="_abstract"]
The following chapter contains the most notable changes to software management between RHEL 8 and RHEL 9.

include::modules/upgrades-and-differences/ref_notable-changes-to-software-management.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_software-management[:context: {parent-context-of-assembly_software-management}]
ifndef::parent-context-of-assembly_software-management[:!context:]
