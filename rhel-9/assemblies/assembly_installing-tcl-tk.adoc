:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_installing-tcl-tk: {context}]

ifndef::context[]
[id="assembly_installing-tcl-tk"]
endif::[]
ifdef::context[]
[id="assembly_installing-tcl-tk_{context}"]
endif::[]
= Installing Tcl/Tk

:context: assembly_installing-tcl-tk

== Introduction to Tcl/Tk
`Tcl` is a dynamic programming language, while `Tk` is a graphical user interface (GUI) toolkit. They provide a powerful and easy-to-use platform for developing cross-platform applications with graphical interfaces.
As a dynamic programming language, 'Tcl' provides simple and flexible syntax for writing scripts. The `tcl` package provides the interpreter for this language and the C library.
You can use `Tk` as GUI toolkit that provides a set of tools and widgets for creating graphical interfaces. You can use various user interface elements such as buttons, menus, dialog boxes, text boxes, and canvas for drawing graphics. `Tk` is the GUI for many dynamic programming languages.

For more information about Tcl/Tk, see the link:https://www.tcl.tk/man/tcl8.6/[Tcl/Tk manual] or link:https://www.tcl.tk/doc/[Tcl/Tk documentation web page].

include::modules/core-services/proc_installing-tcl.adoc[leveloffset=+1]

include::modules/core-services/proc_installing-tk.adoc[leveloffset=+1]





ifdef::parent-context-of-assembly_installing-tcl-tk[:context: {parent-context-of-assembly_installing-tcl-tk}]
ifndef::parent-context-of-assembly_installing-tcl-tk[:!context:]
