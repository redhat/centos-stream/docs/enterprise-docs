:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_creating-c-or-cpp-applications: {context}]

ifndef::context[]
[id="assembly_creating-c-or-cpp-applications"]
endif::[]
ifdef::context[]
[id="assembly_creating-c-or-cpp-applications_{context}"]
endif::[]
= Creating C or C++ Applications

:context: assembly_creating-c-or-cpp-applications

[role="_abstract"]

ifdef::user-stories[__As a developer, I want to create a program using a systems programming language__]

{rh} offers multiple tools for creating applications using the C and C++ languages. This part of the book lists some of the most common development tasks.

include::modules/platform-tools/con_gcc-in-rhel.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_building-code-with-gcc.adoc[leveloffset=+1]

include::assembly_using-libraries-with-gcc.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_creating-libraries-with-gcc.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-more-code-with-make.adoc[leveloffset=+1]

//include::rhel-8/assemblies/assembly_changes-in-toolchain-since-rhel-7.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_creating-c-or-cpp-applications[:context: {parent-context-of-assembly_creating-c-or-cpp-applications}]
ifndef::parent-context-of-assembly_creating-c-or-cpp-applications[:!context:]
