:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-assembly_automating-software-updates-in-rhel-9: {context}]


ifndef::context[]
[id="assembly_automating-software-updates-in-rhel-9"]
endif::[]
ifdef::context[]
[id="assembly_automating-software-updates-in-rhel-9_{context}"]
endif::[]
= Automating software updates in RHEL 9

:context: assembly_automating-software-updates-in-rhel-9


[role="_abstract"]
[application]*DNF Automatic* is an alternative command-line interface to *{PackageManagerName}* that is suited for automatic and regular execution by using systemd timers, cron jobs, and other such tools.

[application]*DNF Automatic* synchronizes package metadata as needed, checks for updates available, and then performs one of the following actions depending on how you configure the tool:

* Exit
* Download updated packages
* Download and apply the updates

The outcome of the operation is then reported by a selected mechanism, such as the standard output or email.


include::modules/core-services/proc_installing-dfn-automatic.adoc[leveloffset=+1]

include::modules/core-services/con_dnf-automatic-configuration-file.adoc[leveloffset=+1]

include::modules/core-services/proc_enabling-dnf-automatic.adoc[leveloffset=+1]

include::modules/core-services/ref_overview-of-the-systemd-timer-units-included-in-the-dnf-automatic-package.adoc[leveloffset=+1]



ifdef::parent-context-of-assembly_automating-software-updates-in-rhel-9[:context: {parent-context-of-assembly_automating-software-updates-in-rhel-9}]
ifndef::parent-context-of-assembly_automating-software-updates-in-rhel-9[:!context:]
