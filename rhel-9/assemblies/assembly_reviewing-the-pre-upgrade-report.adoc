:_mod-docs-content-type: ASSEMBLY
// Module included in the following assemblies:
//
// assembly_upgrading-from-rhel-7-to-rhel-8


[id="reviewing-the-pre-upgrade-report_{context}"]
= Reviewing the pre-upgrade report

[role="_abstract"]
To assess upgradability of your system, start the pre-upgrade process by using the `leapp preupgrade` command. During this phase, the `Leapp` utility collects data about the system, assesses upgradability, and generates a pre-upgrade report.
The pre-upgrade report summarizes potential problems and suggests recommended solutions. The report also helps you decide whether it is possible or advisable to proceed with the upgrade.

[NOTE]
====
The pre-upgrade assessment does not modify the system configuration, but it does consume non-negligible space in the `/var/lib/leapp` directory. In most cases, the pre-upgrade assessment requires up to 4 GB of space, but the actual size depends on your system configuration. If there is not enough space in the hosted file system, the pre-upgrade report might not show complete results of the analysis. To prevent issues, ensure that your system has enough space in the `/var/lib/leapp` directory or move the directory to a dedicated partition so that space consumption does not affect other parts of the system.
====

[IMPORTANT]
====
Always review the entire pre-upgrade report, even when the report finds no inhibitors to the upgrade. The pre-upgrade report contains recommended actions to complete before the upgrade to ensure that the upgraded system functions correctly. 

Reviewing a pre-upgrade report can also be useful if you want to perform a fresh installation of a RHEL 9 system instead of the in-place upgrade process.
====

You can assess upgradability in the pre-upgrade phase using either of the following ways:

* Review the pre-upgrade report in the generated `leapp-report.txt` file and manually resolve reported problems using the command line.
* Use the web console to review the report, apply automated remediations where available, and fix remaining problems using the suggested remediation hints.

[NOTE]
====
You can process the pre-upgrade report by using your own custom scripts, for example, to compare results from multiple reports across different environments. For more information, see link:https://access.redhat.com/articles/5777571[Automating your Red Hat Enterprise Linux pre-upgrade report workflow]. 
====

[IMPORTANT]
====
The pre-upgrade report cannot simulate the entire in-place upgrade process and therefore cannot identify all inhibiting problems with your system. As a result, your in-place upgrade might still be terminated even after you have reviewed and remediated all problems in the report. For example, the pre-upgrade report cannot detect issues related to broken package downloads.
==== 

include::modules/upgrades-and-differences/proc_assessing-upgradability-from-the-command-line.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/proc_assessing-upgradability-from-the-command-line-from-8.8.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/proc_assessing-upgradability-and-applying-automated-remediations-through-the-web-console.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/proc_assessing-upgradability-and-applying-automated-remediations-through-the-web-console-8.8.adoc[leveloffset=+1]
