:_newdoc-version: 2.18.3
:_template-generated: 2024-08-15

ifdef::context[:parent-context-of-contributing-to-the-upstream-projects: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="contributing-to-the-upstream-projects"]
endif::[]
ifdef::context[]
[id="contributing-to-the-upstream-projects_{context}"]
endif::[]
= Appendix: Contributing to the upstream projects

:context: contributing-to-the-upstream-projects

You can contribute to the following upstream bootc projects:

* The upstream git repository is in link:https://gitlab.com/redhat/centos-stream/containers/bootc[CentOS Stream].
* The CentOS Stream sources primarily track the link:https://docs.fedoraproject.org/en-US/bootc[Fedora upstream project].


//[role="_additional-resources"]
//== Next steps

//[role="_additional-resources"]
//== Additional resources

ifdef::parent-context-of-contributing-to-the-upstream-projects[:context: {parent-context-of-contributing-to-the-upstream-projects}]
ifndef::parent-context-of-contributing-to-the-upstream-projects[:!context:]

