:_newdoc-version: 2.17.0
:_template-generated: 2024-04-11

ifdef::context[:parent-context-of-upgrading-from-mariadb-10-5-to-mariadb-10-11: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="upgrading-from-mariadb-10-5-to-mariadb-10-11"]
endif::[]
ifdef::context[]
[id="upgrading-from-mariadb-10-5-to-mariadb-10-11_{context}"]
endif::[]
= Upgrading from MariaDB 10.5 to MariaDB 10.11

:context: upgrading-from-mariadb-10-5-to-mariadb-10-11

This part describes migration from *MariaDB 10.5* to *MariaDB 10.11* within RHEL 9. 

include::rhel-8/modules/core-services/ref_notable-differences-between-mariadb-10-5-and-mariadb-10-11.adoc[leveloffset=+1]

include::modules/core-services/proc_upgrading-from-a-rhel-9-version-of-mariadb-10-5-to-mariadb-10-11.adoc[leveloffset=+1]

ifdef::parent-context-of-upgrading-from-mariadb-10-5-to-mariadb-10-11[:context: {parent-context-of-upgrading-from-mariadb-10-5-to-mariadb-10-11}]
ifndef::parent-context-of-upgrading-from-mariadb-10-5-to-mariadb-10-11[:!context:]

