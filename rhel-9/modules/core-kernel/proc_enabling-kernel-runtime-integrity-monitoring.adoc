:_mod-docs-content-type: PROCEDURE

[id="enabling-kernel-runtime-integrity-monitoring_{context}"]
= Enabling kernel runtime integrity monitoring

You can enable kernel runtime integrity monitoring that IMA appraisal provides.

.Prerequisites

* The `kernel` installed on your system has version `5.14.0-359` or higher.
* The `dracut` package has version `057-43.git20230816` or higher.
* The `keyutils` package is installed.
* The `ima-evm-utils` package is installed.
* The files covered by the policy have valid signatures. For instructions, see xref:adding-ima-signatures-to-package-files_enhancing-security-with-the-kernel-integrity-subsystem[Adding IMA signatures to package files].

.Procedure

. To copy the Red Hat IMA code signing key to the `/etc/ima/keys` file, run:
+
[subs="+quotes"]
....
$ *mkdir -p /etc/keys/ima*
$ *cp /usr/share/doc/kernel-keys/$(uname -r)/ima.cer /etc/ima/keys*
....

. To add the IMA code signing key to the `.ima` keyring, run:
+
[subs="+quotes"]
....
# *keyctl padd asymmetric RedHat-IMA %:.ima < /etc/ima/keys/ima.cer*
....

. Depending on your threat model, define an IMA policy in the `/etc/sysconfig/ima-policy` file.
For example, the following IMA policy checks the integrity of both executables and involved memory mapping library files:
+
[subs="+quotes"]
....
# PROC_SUPER_MAGIC = 0x9fa0
dont_appraise fsmagic=0x9fa0
# SYSFS_MAGIC = 0x62656572
dont_appraise fsmagic=0x62656572
# DEBUGFS_MAGIC = 0x64626720
dont_appraise fsmagic=0x64626720
# TMPFS_MAGIC = 0x01021994
dont_appraise fsmagic=0x1021994
# RAMFS_MAGIC
dont_appraise fsmagic=0x858458f6
# DEVPTS_SUPER_MAGIC=0x1cd1
dont_appraise fsmagic=0x1cd1
# BINFMTFS_MAGIC=0x42494e4d
dont_appraise fsmagic=0x42494e4d
# SECURITYFS_MAGIC=0x73636673
dont_appraise fsmagic=0x73636673
# SELINUX_MAGIC=0xf97cff8c
dont_appraise fsmagic=0xf97cff8c
# SMACK_MAGIC=0x43415d53
dont_appraise fsmagic=0x43415d53
# NSFS_MAGIC=0x6e736673
dont_appraise fsmagic=0x6e736673
# EFIVARFS_MAGIC
dont_appraise fsmagic=0xde5e81e4
# CGROUP_SUPER_MAGIC=0x27e0eb
dont_appraise fsmagic=0x27e0eb
# CGROUP2_SUPER_MAGIC=0x63677270
dont_appraise fsmagic=0x63677270
appraise func=BPRM_CHECK
appraise func=FILE_MMAP mask=MAY_EXEC
....

. To load the IMA policy to make sure the kernel accepts this IMA policy, run:
+
[subs="quotes,macros,attributes"]
....
# *echo /etc/sysconfig/ima-policy > /sys/kernel/security/ima/policy*
# *echo $?*
0
....

. To enable the `dracut` integrity module to automatically load the IMA code signing key and the IMA policy, run:
+
[subs="+quotes"]
....
# *echo 'add_dracutmodules+=" integrity "' > /etc/dracut.conf.d/98-integrity.conf*
# *dracut -f*
....
