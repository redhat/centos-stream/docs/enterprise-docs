:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="running-and-exiting-the-crash-utility_{context}"]
= Running and exiting the crash utility

[role="_abstract"]
Start the `crash` utility for analyzing the cause of the system crash.

.Prerequisites

* Identify the currently running kernel (for example `4.18.0-5.el9.x86_64`).

.Procedure

. To start the `crash` utility, two necessary parameters need to be passed to the command:
+
* The debug-info (a decompressed vmlinuz image), for example `/usr/lib/debug/lib/modules/4.18.0-5.el9.x86_64/vmlinux` provided through a specific `kernel-debuginfo` package.
* The actual vmcore file, for example `/var/crash/127.0.0.1-2018-10-06-14:05:33/vmcore`
+
The resulting [command]`crash` command then looks like this:
+
[subs="quotes,attributes,macros"]
----
# *crash /usr/lib/debug/lib/modules/4.18.0-5.el9.x86_64/vmlinux /var/crash/127.0.0.1-2018-10-06-14:05:33/vmcore*
----
+
Use the same _<kernel>_ version that was captured by `kdump`.
+
.Running the crash utility

The following example shows analyzing a core dump created on October 6 2018 at 14:05 PM, using the 4.18.0-5.el8.x86_64 kernel.
+
----
WARNING: kernel relocated [202MB]: patching 90160 gdb minimal_symbol values

      KERNEL: /usr/lib/debug/lib/modules/4.18.0-5.el8.x86_64/vmlinux
    DUMPFILE: /var/crash/127.0.0.1-2018-10-06-14:05:33/vmcore  [PARTIAL DUMP]
        CPUS: 2
        DATE: Sat Oct  6 14:05:16 2018
      UPTIME: 01:03:57
LOAD AVERAGE: 0.00, 0.00, 0.00
       TASKS: 586
    NODENAME: localhost.localdomain
     RELEASE: 4.18.0-5.el8.x86_64
     VERSION: #1 SMP Wed Aug 29 11:51:55 UTC 2018
     MACHINE: x86_64  (2904 Mhz)
      MEMORY: 2.9 GB
       PANIC: "sysrq: SysRq : Trigger a crash"
         PID: 10635
     COMMAND: "bash"
        TASK: ffff8d6c84271800  [THREAD_INFO: ffff8d6c84271800]
         CPU: 1
       STATE: TASK_RUNNING (SYSRQ)

crash>
----
+
. To exit the interactive prompt and terminate crash, type `exit` or `q`.
+
[subs="quotes,attributes,macros"]
----
*crash> exit*
~]#
----


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/articles/206873[A Guide to Unexpected System Restarts]
