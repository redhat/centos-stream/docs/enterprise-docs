:_mod-docs-content-type: PROCEDURE

[id="proc_mounting-cgroups-v1_{context}"]
= Mounting cgroups-v1

During the boot process, {ProductShortName} {ProductNumber} mounts the `cgroup-v2` virtual filesystem by default. To utilize `cgroup-v1` functionality in limiting resources for your applications, manually configure the system.

[NOTE]
====
Both `cgroup-v1` and `cgroup-v2` are fully enabled in the kernel. There is no default control group version from the kernel point of view, and is decided by `systemd` to mount at startup.
====

.Prerequisites

* You have root permissions.

.Procedure

. Configure the system to mount `cgroups-v1` by default during system boot by the `systemd` system and service manager:
+
[literal,subs="quotes,attributes"]
....
# *grubby --update-kernel=/boot/vmlinuz-$(uname -r) --args="systemd.unified_cgroup_hierarchy=0 systemd.legacy_systemd_cgroup_controller"*
....
+
This adds the necessary kernel command-line parameters to the current boot entry.
+
To add the same parameters to all kernel boot entries:
+
[literal,subs="quotes,attributes"]
....
# *grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0 systemd.legacy_systemd_cgroup_controller"*
....


. Reboot the system for the changes to take effect.



.Verification

. Verify that the `cgroups-v1` filesystem was mounted:
+
[literal,subs="quotes,attributes"]
....
# *mount -l | grep cgroup*
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,seclabel,size=4096k,nr_inodes=1024,mode=755,inode64)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd)
cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,perf_event)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,cpu,cpuacct)
cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,pids)
cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,cpuset)
cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,net_cls,net_prio)
cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,hugetlb)
cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,memory)
cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,blkio)
cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,devices)
cgroup on /sys/fs/cgroup/misc type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,misc)
cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,freezer)
cgroup on /sys/fs/cgroup/rdma type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,rdma)
....
+
The `cgroups-v1` filesystems that correspond to various `cgroup-v1` controllers, were successfully mounted on the `/sys/fs/cgroup/` directory.

. Inspect the contents of the `/sys/fs/cgroup/` directory:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *ll /sys/fs/cgroup/*
dr-xr-xr-x. 10 root root  0 Mar 16 09:34 blkio
lrwxrwxrwx.  1 root root 11 Mar 16 09:34 cpu -> cpu,cpuacct
lrwxrwxrwx.  1 root root 11 Mar 16 09:34 cpuacct -> cpu,cpuacct
dr-xr-xr-x. 10 root root  0 Mar 16 09:34 cpu,cpuacct
dr-xr-xr-x.  2 root root  0 Mar 16 09:34 cpuset
dr-xr-xr-x. 10 root root  0 Mar 16 09:34 devices
dr-xr-xr-x.  2 root root  0 Mar 16 09:34 freezer
dr-xr-xr-x.  2 root root  0 Mar 16 09:34 hugetlb
dr-xr-xr-x. 10 root root  0 Mar 16 09:34 memory
dr-xr-xr-x.  2 root root  0 Mar 16 09:34 misc
lrwxrwxrwx.  1 root root 16 Mar 16 09:34 net_cls -> net_cls,net_prio
dr-xr-xr-x.  2 root root  0 Mar 16 09:34 net_cls,net_prio
lrwxrwxrwx.  1 root root 16 Mar 16 09:34 net_prio -> net_cls,net_prio
dr-xr-xr-x.  2 root root  0 Mar 16 09:34 perf_event
dr-xr-xr-x. 10 root root  0 Mar 16 09:34 pids
dr-xr-xr-x.  2 root root  0 Mar 16 09:34 rdma
dr-xr-xr-x. 11 root root  0 Mar 16 09:34 systemd
....
+
The `/sys/fs/cgroup/` directory, also called the _root control group_, by default, contains controller-specific directories such as `cpuset`. In addition, there are some directories related to `systemd`.



[role="_additional-resources"]
.Additional resources
* xref:what-kernel-resource-controllers-are_setting-limits-for-applications[What are kernel resource controllers]
* `cgroups(7)`, `sysfs(5)` manual pages
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/9.0_release_notes/index#BZ-1953515[cgroup-v2 enabled by default in RHEL 9]
