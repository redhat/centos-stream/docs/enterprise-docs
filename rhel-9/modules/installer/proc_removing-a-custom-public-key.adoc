:_mod-docs-content-type: PROCEDURE
[id="removing-a-custom-public-key_{context}"]
= Removing a custom public key

[role="_abstract"]
The procedure describes how to remove a custom public key.

.Procedure

. Remove the Red Hat custom public key from the system's Machine Owner Key (MOK) list:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] mokutil --reset
....
+
. Enter a password when prompted.

. Reboot the system and press any key to continue the startup.
  The Shim UEFI key management utility starts during the system startup.

. Select `Reset MOK`.

. Select `Continue`.

. Select `Yes` and enter the password that you had specified in step 2.
  The key is removed from the system's firmware.

. Select `Reboot`.
