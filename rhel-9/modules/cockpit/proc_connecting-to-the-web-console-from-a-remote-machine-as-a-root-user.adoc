:_mod-docs-content-type: PROCEDURE

[id="connecting-to-the-web-console-from-a-remote-machine-as-a-root-user_{context}"]
= Connecting to the web console from a remote machine as a root user

On new installations of {ProductShortName} 9.2 or later, the {ProductShortName} web console disallows root account logins by default for security reasons. You can allow the `root` login in the `/etc/cockpit/disallowed-users` file.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

.Procedure

. Open the `disallowed-users` file in the `/etc/cockpit/` directory in a text editor of your preference, for example:
+
[subs="quotes"]
----
# *vi /etc/cockpit/disallowed-users*
----

. Edit the file and remove the line for the `root` user:
+
[subs="attributes"]
----
# List of users which are not allowed to login to Cockpit root
&nbsp;
----

. Save the changes and quit the editor.

.Verification

* Log in to the web console as a `root` user.
+
For details, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].
