:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-a-wireguard-server-by-using-nmtui_{context}"]
= Configuring a WireGuard server by using `nmtui`

[role="_abstract"]
You can configure the WireGuard server by creating a connection profile in NetworkManager. Use this method to let NetworkManager manage the WireGuard connection.

This procedure assumes the following settings:

* Server:

** Private key: `YFAnE0psgIdiAF7XR4abxiwVRnlMfeltxu10s/c4JXg=`
** Tunnel IPv4 address: `192.0.2.1/24`
** Tunnel IPv6 address: `2001:db8:1::1/32`

* Client:

** Public key: `bnwfQcC8/g2i4vvEqcRUM2e6Hi3Nskk6G9t4r26nFVM=`
** Tunnel IPv4 address: `192.0.2.2/24`
** Tunnel IPv6 address: `2001:db8:1::2/32`



.Prerequisites

* You have generated the public and private key for both the server and client.

* You know the following information:
+
** The private key of the server
** The static tunnel IP addresses and subnet masks of the client
** The public key of the client
** The static tunnel IP addresses and subnet masks of the server

* You installed the `NetworkManager-tui` package.



.Procedure

. Start the `nmtui` application:
+
[literal,subs="+quotes"]
....
# *nmtui*
....

. Select *Edit a connection*, and press kbd:[Enter].

. Select btn:[Add], and press kbd:[Enter].

. Select the *WireGuard* connection type in the list, and press kbd:[Enter].

. In the *Edit connection* window:

.. Enter the name of the connection and the virtual interface, such as `wg0`, that NetworkManager should assign to the connection.

.. Enter the private key of the server.

.. Set the listen port number, such as `51820`, for incoming WireGuard connections.
+
Always set a fixed port number on hosts that receive incoming WireGuard connections. If you do not set a port, WireGuard uses a random free port each time you activate the interface.
+
image::nmtui-WireGuard-server-general.png[]

.. Click btn:[Add] next to the *Peers* pane:

... Enter the public key of the client.

... Set the *Allowed IPs* field to the tunnel IP addresses of the client that are allowed to send data to this server.

... Select btn:[OK], and press kbd:[Enter].
+
image::nmtui-WireGuard-server-peer-config.png[]

.. Select btn:[Show] next to *IPv4 Configuration, and press kbd:[Enter].

... Select the IPv4 configuration method *Manual*.

... Enter the tunnel IPv4 address and the subnet mask. Leave the *Gateway* field empty.

.. Select btn:[Show] next to *IPv6 Configuration*, and press kbd:[Enter].

... Select the IPv6 configuration method *Manual*.

... Enter the tunnel IPv6 address and the subnet mask. Leave the `Gateway` field empty.

.. Select btn:[OK], and press kbd:[Enter]
+
image::nmtui-WireGuard-server-ip-config.png[]

. In the window with the list of connections, select btn:[Back], and press kbd:[Enter].

. In the *NetworkManager TUI* main window, select btn:[Quit], and press kbd:[Enter].



.Next steps

ifdef::networking-title[]
* xref:proc_configuring-firewalld-on-a-wireguard-server-by-using-the-command-line_assembly_setting-up-a-wireguard-vpn[Configure the firewalld service on the WireGuard server].
endif::[]

ifndef::networking-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/assembly_setting-up-a-wireguard-vpn_configuring-and-managing-networking#proc_configuring-firewalld-on-a-wireguard-server-by-using-the-command-line_assembly_setting-up-a-wireguard-vpn[Configure the firewalld service on the WireGuard server]
endif::[]



.Verification

. Display the interface configuration of the `wg0` device:
+
[literal,subs="+quotes"]
....
# *wg show wg0*
interface: wg0
  public key: UtjqCJ57DeAscYKRfp7cFGiQqdONRn69u249Fa4O6BE=
  private key: (hidden)
  listening port: 51820

peer: bnwfQcC8/g2i4vvEqcRUM2e6Hi3Nskk6G9t4r26nFVM=
  allowed ips: 192.0.2.2/32, 2001:db8:1::2/128
....
+
To display the private key in the output, use the `WG_HIDE_KEYS=never wg show wg0` command.

. Display the IP configuration of the `wg0` device:
+
[literal,subs="+quotes"]
....
# *ip address show wg0*
20: wg0: <POINTOPOINT,NOARP,UP,LOWER_UP> mtu 1420 qdisc noqueue state UNKNOWN group default qlen 1000
    link/none
    inet 192.0.2.1/24 brd 192.0.2.255 scope global noprefixroute wg0
       valid_lft forever preferred_lft forever
    inet6 _2001:db8:1::1/32 scope global noprefixroute
       valid_lft forever preferred_lft forever
    inet6 fe80::3ef:8863:1ce2:844/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
....



[role="_additional-resources"]
.Additional resources
* `wg(8)` man page on your system

