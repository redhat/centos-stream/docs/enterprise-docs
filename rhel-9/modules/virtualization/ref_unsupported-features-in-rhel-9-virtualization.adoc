:_mod-docs-content-type: REFERENCE

[id="unsupported-features-in-rhel-9-virtualization_{context}"]
= Unsupported features in {ProductShortName}{nbsp}9 virtualization
// TODO?: Add sample XML snippets and/or virsh commands to help users recognize the unsupported features.

[role="_abstract"]
The following features are not supported by the KVM hypervisor included with {ProductName}{nbsp}9 ({ProductShortName}{nbsp}9):

[IMPORTANT]
====
Many of these limitations may not apply to other virtualization solutions provided by {RH}, such as OpenShift{nbsp}Virtualization or {RH}{nbsp}OpenStack{nbsp}Platform (RHOSP).

Features supported by other virtualization solutions are described as such in the following paragraphs.
====

.Host system architectures

{ProductShortName}{nbsp}9 with KVM is not supported on any host architectures that are not listed in xref:recommended-features-in-rhel-9-virtualization_feature-support-and-limitations-in-rhel-9-virtualization[Recommended features in {ProductShortName}{nbsp}9 virtualization].

.Guest operating systems

KVM virtual machines (VMs) that use the following guest operating systems (OSs) are not supported on a {ProductShortName}{nbsp}9 host:

* Windows 8.1 and earlier
* Windows Server 2012 R2 and earlier
* macOS
* Solaris for x86 systems
* Any operating system released before 2009

For a list of guest OSs supported on {ProductShortName} hosts and other virtualization solutions, see link:https://access.redhat.com/articles/973163[Certified Guest Operating Systems in {RH}{nbsp}OpenStack{nbsp}Platform, {RH}{nbsp}Virtualization, OpenShift Virtualization and {ProductName} with KVM].

.Creating VMs in containers

{RH} does not support creating KVM virtual machines in any type of container that includes the elements of the {ProductShortName}{nbsp}9 hypervisor (such as the `QEMU` emulator or the `libvirt` package).

To create VMs in containers, {RH} recommends using the link:https://access.redhat.com/documentation/en-us/openshift_container_platform/4.14/html/virtualization/about#about-virt[OpenShift{nbsp}Virtualization] offering.

.Specific virsh commands and options

Not every parameter that you can use with the `virsh` utility has been tested and certified as production-ready by {RH}. Therefore, any `virsh` commands and options that are not explicitly recommended by {RH} documentation may not work correctly, and {RH} recommends not using them in your production environment.

Notably, unsupported `virsh` commands include the following:

* `virsh iface-*` commands, such as `virsh iface-start` and `virsh iface-destroy`
* `virsh blkdeviotune`
* `virsh snapshot-*` commands, such as `virsh snapshot-create` and `virsh snapshot-revert`

.The QEMU command line

QEMU is an essential component of the virtualization architecture in {ProductShortName}{nbsp}9, but it is difficult to manage manually, and improper QEMU configurations might cause security vulnerabilities. Therefore, using `qemu-*` command-line utilities, such as, `qemu-kvm` is not supported by {RH}. Instead, use _libvirt_ utilities, such as `virt-install`, `virt-xml`, and supported `virsh` commands, as these orchestrate QEMU according to the best practices.
However, the `qemu-img` utility is supported for xref:managing-virtual-disk-images-by-using-the-cli_managing-storage-for-virtual-machines[management of virtual disk images].

.vCPU hot unplug

Removing a virtual CPU (vCPU) from a running VM, also referred to as a vCPU hot unplug, is not supported in {ProductShortName}{nbsp}9.

// Other solutions:

// * vCPU hot unplugs are supported in RHV. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_virtualization/4.3/html/virtual_machine_management_guide/cpu_hot_plug[Hot plugging VCPUs].

//* Also planned for OpenShift Virtualization -> monitor https://docs.google.com/spreadsheets/d/1Gho3X-enEtMPcNad-dOJqMocRXohYmtHEa7BkYHdKuY/edit#gid=1786802242 -> line 112 (currently)

.Memory hot unplug

Removing a memory device attached to a running VM, also referred to as a memory hot unplug, is unsupported in {ProductShortName}{nbsp}9.

// Other solutions:

// * Memory hot unplugs are supported in RHV, but only on guest VMs running {ProductShortName} with specific guest configurations. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_virtualization/4.3/html/virtual_machine_management_guide/sect-virtual_memory#Hot_Unplugging_Virtual_Memory[Hot Unplugging Virtual Memory].

//* Also planned for OpenShift Virtualization -> monitor https://docs.google.com/spreadsheets/d/1Gho3X-enEtMPcNad-dOJqMocRXohYmtHEa7BkYHdKuY/edit#gid=1786802242 -> line 114 (currently)

.QEMU-side I/O throttling

Using the `virsh blkdeviotune` utility to configure maximum input and output levels for operations on virtual disk, also known as QEMU-side I/O throttling, is not supported in {ProductShortName}{nbsp}9.

To set up I/O throttling in {ProductShortName}{nbsp}9, use `virsh blkiotune`. This is also known as libvirt-side I/O throttling. For instructions, see xref:disk-i-o-throttling-in-virtual-machines_optimizing-virtual-machine-i-o-performance[Disk I/O throttling in virtual machines].

Other solutions:

// * QEMU-side I/O throttling is supported in RHV. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_virtualization/4.3/html/administration_guide/chap-quality_of_service#sect-Storage_Quality_of_Service[Storage quality of service].

* QEMU-side I/O throttling is also supported in RHOSP. For more information, see Red Hat Knowledgebase solutions link:https://access.redhat.com/solutions/875363[Setting Resource Limitation on Disk] and the *Use Quality-of-Service Specifications* section in the link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/storage_guide/index[RHOSP Storage Guide].

* In addition, OpenShift Virtualizaton supports QEMU-side I/O throttling as well.
+
// Link for relevant docs somewhere?^

.Storage live migration

Migrating a disk image of a running VM between hosts is not supported in {ProductShortName}{nbsp}9.

Other solutions:

// * Storage live migration is supported in RHV. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_virtualization/4.3/html-single/administration_guide/index#Overview_of_Live_Storage_Migration[Overview of Live Storage Migration].

* Storage live migration is supported in RHOSP, but with some limitations. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/storage_guide/ch-cinder#section-volumes-advanced-migrate[Migrate a Volume].



.Internal snapshots

Creating and using internal snapshots for VMs is deprecated in {ProductShortName}{nbsp}9, and highly discouraged for use in production environment. Instead, use external snapshots. For details, see xref:support-limitations-for-virtual-machine-snapshots_creating-virtual-machine-snapshots[Support limitations for virtual machine snapshots].

Other solutions:

* RHOSP supports live snapshots. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html/director_installation_and_usage/performing-basic-overcloud-administration-tasks#importing-virtual-machines-into-the-overcloud[Importing virtual machines into the overcloud].

* Live snapshots are also supported on OpenShift Virtualization.


// For more info on snapshot development, see https://libvirt.org/kbase/domainstatecapture.html and https://libvirt.org/formatsnapshot.html

.vHost Data Path Acceleration

On {ProductShortName}{nbsp}9 hosts, it is possible to configure vHost Data Path Acceleration (vDPA) for virtio devices, but {RH} currently does not support this feature, and strongly discourages its use in production environments.

.vhost-user

{ProductShortName}{nbsp}9 does not support the implementation of a user-space vHost interface.

Other solutions:

* `vhost-user` is supported in RHOSP, but only for `virtio-net` interfaces. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/3394851[virtio-net implementation] and link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/10/html/network_functions_virtualization_planning_guide/ch-vhost-user-ports[vhost user ports].

* OpenShift Virtualization supports `vhost-user` as well.
+
// Link to relevant docs about that?

.S3 and S4 system power states

Suspending a VM to the *Suspend to RAM* (S3) or *Suspend to disk* (S4) system power states is not supported. Note that these features are disabled by default, and enabling them will make your VM not supportable by {RH}.

Note that the S3 and S4 states are also currently not supported in any other virtualization solution provided by {RH}.

// NOT TRUE S3 AND S4 IIUC: Note that S3 and S4 system power states are supported in RHOSP. For details, see *Suspend/Resume Instance* in link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.0/html-single/instances_and_images_guide/index#update_an_instance_actions_menu[Update a VM instance].


.S3-PR on a multipathed vDisk

SCSI3 persistent reservation (S3-PR) on a multipathed vDisk is not supported in {ProductShortName}{nbsp}9. As a consequence, Windows{nbsp}Cluster is not supported in {ProductShortName}{nbsp}9.


// Other solutions:
// * S3-PR on a multipathed vDisk is supported in RHV. Therefore, if you require Windows Cluster support, Red Hat recommends using RHV as your virtualization solution. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/2937021[Cluster support on RHV guests].

.virtio-crypto

Using the _virtio-crypto_ device in {ProductShortName}{nbsp}9 is not supported and {ProductShortName} strongly discourages its use.

Note that _virtio-crypto_ devices are also not supported in any other virtualization solution provided by {RH}.

.virtio-multitouch-device, virtio-multitouch-pci

Using the _virtio-multitouch-device_ and _virtio-multitouch-pci_ devices in {ProductShortName}{nbsp}9 is not supported and {ProductShortName} strongly discourages their use.

.Incremental live backup

Configuring a VM backup that only saves VM changes since the last backup, also known as incremental live backup, is not supported in {ProductShortName}{nbsp}9, and {RH} highly discourages its use.

// Other solutions:

// * Incremental live backup is provided as a Technology Preview in RHV 4.4 and later.

// TODO: Add a link to the RHV doc when there is one, ping Steve Goodman about this.

// * Incremental live backup is supported with RHEL 9 when used via a certified backup vendor interface. -> TBD look into what that actually means!


.net_failover

Using the `net_failover` driver to set up an automated network device failover mechanism is not supported in {ProductShortName}{nbsp}9.

Note that `net_failover` is also currently not supported in any other virtualization solution provided by {RH}.

////
.vTPM

Attaching virtual Trusted Platform Module (vTPM) devices to VMs hosted on a RHEL 9 system is unsupported.

Note that vTPM is also currently not supported in any other virtualization solution provided by Red Hat.

TBD - this should actually be supported, also as per https://docs.google.com/spreadsheets/d/1Gho3X-enEtMPcNad-dOJqMocRXohYmtHEa7BkYHdKuY/edit#gid=1786802242 -> update docs accordingly!
////
// and also in RHV 4.4 and later. - NOT UNTIL 4.4.z AT THE EARLIEST AS PER MSKRIVAN.
// stephenfin: This is not supported yet. It's targeted for OSP 17. https://bugzilla.redhat.com/show_bug.cgi?id=1782128


// .NVMe devices

// Attaching Non-volatile Memory express (NVMe) devices to VMs as a PCIe device with PCI-passthrough is not supported.

// Note that attaching `NVMe` devices to VMs is also currently not supported in any other virtualization solution provided by {RH}.

// commented out based on https://issues.redhat.com/browse/RHELDOCS-17171 

// IN 4.4.z AT THE EARLIEST IN RHV AS PER MSKRIVAN.

// https://docs.google.com/spreadsheets/d/1Gho3X-enEtMPcNad-dOJqMocRXohYmtHEa7BkYHdKuY/edit#gid=1786802242 says otherwise (line 103) -> look into!


.TCG

QEMU and libvirt include a dynamic translation mode using the QEMU Tiny Code Generator (TCG). This mode does not require hardware virtualization support. However, TCG is not supported by {RH}.

TCG-based guests can be recognized by examining its XML configuration, for example using the `virsh dumpxml` command.

* The configuration file of a TCG guest contains the following line:
+
[source, xml]
--
<domain type='qemu'>
--

* The configuration file of a KVM guest contains the following line:
+
[source, xml]
--
<domain type='kvm'>
--

.SR-IOV InfiniBand networking devices

Attaching InfiniBand networking devices to VMs using Single-root I/O virtualization (SR-IOV) is not supported.

.SGIO

Attaching SCSI devices to VMs by using SCSI generic I/O (SGIO) is not supported on {ProductShortName}{nbsp}9. To detect whether your VM has an attached SGIO device, check the VM configuration for the following lines:

[source, xml]
----
<disk type="block" device="lun">
----

[source, xml]
----
<hostdev mode='subsystem' type='scsi'>
----

////
[IMPORTANT]
====
For automatic detection if your system is using unsupported VM configurations, you can use Red Hat Insights (?)  - TBD more info and link when this gets finalized
====
////

[role="_additional-resources"]
.Additional resources

* xref:recommended-features-in-rhel-9-virtualization_feature-support-and-limitations-in-rhel-9-virtualization[Recommended features in {ProductShortName}{nbsp}9 virtualization]

* xref:resource-allocation-limits-in-rhel-9-virtualization_feature-support-and-limitations-in-rhel-9-virtualization[Resource allocation limits in {ProductShortName}{nbsp}9 virtualization]
