:_newdoc-version: 2.17.0
:_template-generated: 2024-05-02

:_mod-docs-content-type: REFERENCE

[id="comparison-of-memory-onlining-configurations_{context}"]
= Comparison of memory onlining configurations

When attaching memory to a running {ProductShortName} virtual machine (also known as memory hot-plugging), you must set the hot-plugged memory to an online state in the virtual machine (VM) operating system. Otherwise, the system will not be able to use the memory.

The following table summarizes the main considerations when choosing between the available memory onlining configurations.

.Comparison of memory onlining configurations
[cols="5*"]
|===
|Configuration name |Unplugging memory from a VM |A risk of creating a memory zone imbalance |A potential use case |Memory requirements of the intended workload

|`online_movable` |Hot-plugged memory can be reliably unplugged. |Yes |Hot-plugging a comparatively small amount of memory |Mostly user-space memory

|`auto-movable` |Movable portions of hot-plugged memory can be reliably unplugged. |Minimal |Hot-plugging a large amount of memory |Mostly user-space memory

|`online_kernel` |Hot-plugged memory cannot be reliably unplugged. |No |Unreliable memory unplugging is acceptable. |User-space or kernel-space memory
|===

A _zone imbalance_ is a lack of available memory pages in one of the Linux memory zones. A _zone imbalance_ can negatively impact the system performance. For example, the kernel might crash if it runs out of free memory for unmovable allocations. Usually, movable allocations contain mostly user-space memory pages and unmovable allocations contain mostly kernel-space memory pages.

[role="_additional-resources"]
.Additional resources
* link:https://www.kernel.org/doc/html/latest/admin-guide/mm/memory-hotplug.html#onlining-and-offlining-memory-blocks[Onlining and Offlining Memory Blocks]
* link:https://www.kernel.org/doc/html/latest/admin-guide/mm/memory-hotplug.html#zone-movable[Zone Imbalances]
* xref:configuring-memory-onlining-in-virtual-machines_adding-and-removing-virtual-machine-memory-by-using-virtio-mem[Configuring memory onlining in virtual machines]

