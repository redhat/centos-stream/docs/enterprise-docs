:_mod-docs-content-type: PROCEDURE

[id="configuring-the-passt-user-space-connection_{context}"]
= Configuring the passt user-space connection

If you require non-privileged access to a virtual network, for example when using a `session` connection of `libvirt`, you can configure your virtual machine (VM) to use the `passt` networking back end.

.Prerequisites

* The `passt` package has been installed on your system.
+
[subs="+quotes"]
----
# *dnf install passt*
----

.Procedure

. Open the XML configuration of the VM on which you want to use a `passt` connection. For example:
+
[subs="+quotes"]
----
# *virsh edit _<testguest1>_*
----

. In the `<devices>` section, add an `<interface type='user'>` element that uses `passt` as its backend type.
+
For example, the following configuration sets up a `passt` connection that uses addresses and routes copied from the host interface associated with the first default route:
+
[subs="+quotes"]
----
<devices>
  [...]
  <interface type='user'>
    <backend type='passt'/>
  </interface>
</devices>
----
+
Optionally, when using `passt`, you can specify multiple `<portForward>` elements to forward incoming network traffic for the host to this VM interface. You can also customize interface IP addresses. For example:
+
[subs="+quotes"]
----
<devices>
  [...]
  <interface type='user'>
    <backend type='passt'/>
    <mac address="52:54:00:98:d8:b7"/>
    <source dev='eth0'/>
    <ip family='ipv4' address='192.0.2.1' prefix='24'/>
    <ip family='ipv6' address='::ffff:c000:201'/>
    <portForward proto='tcp'>
      <range start='2022' to='22'/>
    </portForward>
    <portForward proto='udp' address='1.2.3.4'>
       <range start='5000' end='5020' to='6000'/>
       <range start='5010' end='5015' exclude='yes'/>
    </portForward>
    <portForward proto='tcp' address='2001:db8:ac10:fd01::1:10' dev='eth0'>
      <range start='8080'/>
      <range start='4433' to='3444'/>
    </portForward>
  </interface>
</devices>
----
+
This example configuration sets up a `passt` connection with the following parameters:
+
** The VM copies the network routes for forwarding traffic from the `eth0` host interface.
** The interface MAC is set to `52:54:00:98:d8:b7`. If unset, a random one will be generated.
** The IPv4 address is set to `192.0.2.1/24`, and the IPv6 address is set to `::ffff:c000:201`.
** The TCP port `2022` on the host forwards its network traffic to port `22` on the VM.
** The TCP address `2001:db8:ac10:fd01::1:10` on host interface `eth0` and port `8080` forwards its network traffic to port `8080` on the VM. Port `4433` forwards to port `3444` on the VM.
** The UDP address `1.2.3.4` and ports `5000 - 5009` and `5016 - 5020` on the host forward their network traffic to ports `6000 - 6009` and `6016 - 6020` on the VM.

. Save the XML configuration.

.Verification

* Start or restart the VM you configured with `passt`:
+
[subs="+quotes"]
----
# *virsh reboot _<vm-name>_*
# *virsh start _<vm-name>_*
----
+
If the VM boots successfully, it is now using the `passt` networking backend.

[role="_additional-resources"]
.Additional resources
* xref:system-and-session-connections_securing-virtual-machines-in-rhel[System and session connections in `libvirt`]
// * link:https://libvirt.org/formatdomain.html#userspace-slirp-or-passt-connection[passt configuration details in libvirt]
// * link:https://passt.top/passt/about/[Upstream project documentation for passt]
