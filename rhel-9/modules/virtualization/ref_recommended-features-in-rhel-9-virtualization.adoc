:_module-type: REFERENCE

[id="recommended-features-in-rhel-9-virtualization_{context}"]
= Recommended features in {ProductShortName}{nbsp}9 virtualization

[role="_abstract"]
The following features are recommended for use with the KVM hypervisor included with {ProductName}{nbsp}9 ({ProductShortName}{nbsp}9):

.Host system architectures

{ProductShortName}{nbsp}9 with KVM is only supported on the following host architectures:

** AMD64 and Intel 64
** IBM Z - IBM z13 systems and later
** ARM 64

Any other hardware architectures are not supported for using {ProductShortName}{nbsp}9 as a KVM virtualization host, and {RH} highly discourages doing so.

.Guest operating systems

{RH} provides support with KVM virtual machines that use specific guest operating systems (OSs). For a detailed list of supported guest OSs, see the Certified Guest Operating Systems in link:https://access.redhat.com/articles/973163#rhelkvm[the Red Hat KnowledgeBase].

Note, however, that by default, your guest OS does not use the same subscription as your host. Therefore, you must activate a separate licence or subscription for the guest OS to work properly.
// Advertise virt-who here?

In addition, the pass-through devices that you attach to the VM must be supported by both the host OS and the guest OS.

Similarly, for optimal function of your deployment, {RH} recommends that the CPU model and features that you define in the XML configuration of a VM are supported by both the host OS and the guest OS.

To view the certified CPUs and other hardware for various versions of RHEL, see the link:https://catalog.redhat.com/hardware/search?type=Component|CPU%20Collection&p=1[Red Hat Ecosystem Catalog].

.Machine types

To ensure that your VM is compatible with your host architecture and that the guest OS runs optimally, the VM must use an appropriate machine type.

[IMPORTANT]
====
In {ProductShortName}{nbsp}9, `pc-i440fx-rhel7.5.0` and earlier machine types, which were default in earlier major versions of {ProductShortName}, are no longer supported. As a consequence, attempting to start a VM with such machine types on a {ProductShortName}{nbsp}9 host fails with an `unsupported configuration` error. If you encounter this problem after upgrading your host to RHEL 9, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/6999469[Invalid virtual machines that used to work with RHEL 9 and newer hypervisors].
====

When xref:creating-virtual-machines-using-the-command-line-interface_assembly_creating-virtual-machines[creating a VM by using the command line], the `virt-install` utility provides multiple methods of setting the machine type.

* When you use the `--os-variant` option, `virt-install` automatically selects the machine type recommended for your host CPU and supported by the guest OS.
* If you do not use `--os-variant` or require a different machine type, use the `--machine` option to specify the machine type explicitly.
* If you specify a `--machine` value that is unsupported or not compatible with your host, `virt-install` fails and displays an error message.

The recommended machine types for KVM virtual machines on supported architectures, and the corresponding values for the `--machine` option, are as follows. __Y__ stands for the latest minor version of {ProductShortName}{nbsp}9.

* On *Intel 64 and AMD64* (x86_64): `pc-q35-rhel9.__Y__.0` -> `--machine=q35`
* On *IBM Z* (s390x): `s390-ccw-virtio-rhel9.__Y__.0` -> `--machine=s390-ccw-virtio`
* On *ARM 64*: `virt-rhel9.__Y__.0` -> `--machine=virt`

To obtain the machine type of an existing VM:

[subs="+quotes",options="nowrap",role="white-space-pre"]
----
# *virsh dumpxml _VM-name_ | grep machine=*
----

To view the full list of machine types supported on your host:

[subs="+quotes",options="nowrap",role="white-space-pre"]
----
# */usr/libexec/qemu-kvm -M help*
----


[role="_additional-resources"]
.Additional resources

* xref:unsupported-features-in-rhel-9-virtualization_feature-support-and-limitations-in-rhel-9-virtualization[Unsupported features in {ProductShortName}{nbsp}9 virtualization]
* xref:resource-allocation-limits-in-rhel-9-virtualization_feature-support-and-limitations-in-rhel-9-virtualization[Resource allocation limits in {ProductShortName}{nbsp}9 virtualization]
