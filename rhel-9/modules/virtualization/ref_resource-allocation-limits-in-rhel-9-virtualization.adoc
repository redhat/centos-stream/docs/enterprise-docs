:_module-type: REFERENCE

[id="resource-allocation-limits-in-rhel-9-virtualization_{context}"]
= Resource allocation limits in {ProductShortName}{nbsp}9 virtualization

[role="_abstract"]
The following limits apply to virtualized resources that can be allocated to a single KVM virtual machine (VM) on a {ProductName}{nbsp}9 ({ProductShortName}{nbsp}9) host.

[IMPORTANT]
====
Many of these limitations do not apply to other virtualization solutions provided by {RH}, such as OpenShift{nbsp}Virtualization or {RH}{nbsp}OpenStack{nbsp}Platform (RHOSP).
====

.Maximum vCPUs per VM

For the maximum amount of vCPUs and memory that is supported on a single VM running on a {ProductShortName}{nbsp}{ProductNumber} host, see: link:https://access.redhat.com/articles/rhel-kvm-limits[Virtualization limits for Red Hat Enterprise Linux with KVM]

.PCI devices per VM

{ProductShortName}{nbsp}9 supports *32* PCI device slots per VM bus, and *8* PCI functions per device slot. This gives a theoretical maximum of 256 PCI functions per bus when multi-function capabilities are enabled in the VM, and no PCI bridges are used.

Each PCI bridge adds a new bus, potentially enabling another 256 device addresses. However, some buses do not make all 256 device addresses available for the user; for example, the root bus has several built-in devices occupying slots.

.Virtualized IDE devices

KVM is limited to a maximum of *4* virtualized IDE devices per VM.
