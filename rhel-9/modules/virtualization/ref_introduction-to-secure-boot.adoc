:_newdoc-version: 2.18.3
:_template-generated: 2024-09-30
:_mod-docs-content-type: REFERENCE

[id="introduction-to-secure-boot_{context}"]
= Introduction to Secure Boot

The Secure Boot mechanism is a security protocol that provides authentication access for specific device paths by using defined interfaces. Successive authentication configuration overwrites the former configuration, by making it non-retrievable. It ensures that a trusted vendor has signed the boot loader and the kernel. {RHEL} firmware checks the digital signature of the boot loader and related components against trusted keys stored on the hardware. If any component is tampered with or signed by an untrusted entity, the booting process aborts, which prevents potentially malicious software from taking control of the system. Additionally, the RHEL kernel offers the `lockdown` mode that ensures only a trusted vendor signed kernel modules are loaded.