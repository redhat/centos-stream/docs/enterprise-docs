:_module-type: REFERENCE

[id="ref_llvm-components_{context}"]
= LLVM Toolset components

[role="_abstract"]

The following components are available as a part of {comp} Toolset:

ifdef::publish-rhel-7[:package-clang: RHEL 7 &mdash; {clang-ver-rhel-7}]
ifdef::publish-rhel-8[:package-clang: RHEL 8 &mdash; {clang-ver-rhel-8}]
ifdef::publish-rhel-9[:package-clang: RHEL 9 &mdash; {clang-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8[:package-clang: RHEL 7 &mdash; {clang-ver-rhel-7}, RHEL 8 &mdash; {clang-ver-rhel-8}]
ifdef::publish-rhel-8+publish-rhel-9[:package-clang: RHEL 8 &mdash; {clang-ver-rhel-8}, RHEL 9 &mdash; {clang-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-9[:package-clang: RHEL 7 &mdash; {clang-ver-rhel-7}, RHEL 9 &mdash; {clang-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:package-clang: RHEL 7 &mdash; {clang-ver-rhel-7}, RHEL 8 &mdash; {clang-ver-rhel-8}, RHEL 9 &mdash; {clang-ver-rhel-9}]
ifdef::publish-rhel-7+same-versions[:package-clang: {clang-ver}]
ifdef::publish-rhel-8+same-versions[:package-clang: {clang-ver}]
ifdef::publish-rhel-9+same-versions[:package-clang: {clang-ver}]

ifdef::publish-rhel-7[:package-lldb: RHEL 7 &mdash; {lldb-ver-rhel-7}]
ifdef::publish-rhel-8[:package-lldb: RHEL 8 &mdash; {lldb-ver-rhel-8}]
ifdef::publish-rhel-9[:package-lldb: RHEL 9 &mdash; {lldb-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8[:package-lldb: RHEL 7 &mdash; {lldb-ver-rhel-7}, RHEL 8 &mdash; {lldb-ver-rhel-8}]
ifdef::publish-rhel-8+publish-rhel-9[:package-lldb: RHEL 8 &mdash; {lldb-ver-rhel-8}, RHEL 9 &mdash; {lldb-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-9[:package-lldb: RHEL 7 &mdash; {lldb-ver-rhel-7}, RHEL 9 &mdash; {lldb-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:package-lldb: RHEL 7 &mdash; {lldb-ver-rhel-7}, RHEL 8 &mdash; {lldb-ver-rhel-8}, RHEL 9 &mdash; {lldb-ver-rhel-9}]
ifdef::publish-rhel-7+same-versions[:package-lldb: {lldb-ver}]
ifdef::publish-rhel-8+same-versions[:package-lldb: {lldb-ver}]
ifdef::publish-rhel-9+same-versions[:package-lldb: {lldb-ver}]

ifdef::publish-rhel-7[:package-compiler-rt: RHEL 7 &mdash; {compiler-rt-ver-rhel-7}]
ifdef::publish-rhel-8[:package-compiler-rt: RHEL 8 &mdash; {compiler-rt-ver-rhel-8}]
ifdef::publish-rhel-9[:package-compiler-rt: RHEL 9 &mdash; {compiler-rt-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8[:package-compiler-rt: RHEL 7 &mdash; {compiler-rt-ver-rhel-7}, RHEL 8 &mdash; {compiler-rt-ver-rhel-8}]
ifdef::publish-rhel-8+publish-rhel-9[:package-compiler-rt: RHEL 8 &mdash; {compiler-rt-ver-rhel-8}, RHEL 9 &mdash; {compiler-rt-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-9[:package-compiler-rt: RHEL 7 &mdash; {compiler-rt-ver-rhel-7}, RHEL 9 &mdash; {compiler-rt-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:package-compiler-rt: RHEL 7 &mdash; {compiler-rt-ver-rhel-7}, RHEL 8 &mdash; {compiler-rt-ver-rhel-8}, RHEL 9 &mdash; {compiler-rt-ver-rhel-9}]
ifdef::publish-rhel-7+same-versions[:package-compiler-rt: {compiler-rt-ver}]
ifdef::publish-rhel-8+same-versions[:package-compiler-rt: {compiler-rt-ver}]
ifdef::publish-rhel-9+same-versions[:package-compiler-rt: {compiler-rt-ver}]

ifdef::publish-rhel-7[:package-llvm}: RHEL 7 &mdash; {llvm-ver-rhel-7}]
ifdef::publish-rhel-8[:package-llvm}: RHEL 8 &mdash; {llvm-ver-rhel-8}]
ifdef::publish-rhel-9[:package-llvm}: RHEL 9 &mdash; {llvm-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8[:package-llvm}: RHEL 7 &mdash; {llvm-ver-rhel-7}, RHEL 8 &mdash; {llvm-ver-rhel-8}]
ifdef::publish-rhel-8+publish-rhel-9[:package-llvm}: RHEL 8 &mdash; {llvm-ver-rhel-8}, RHEL 9 &mdash; {llvm-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-9[:package-llvm}: RHEL 7 &mdash; {llvm-ver-rhel-7}, RHEL 9 &mdash; {llvm-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:package-llvm}: RHEL 7 &mdash; {llvm-ver-rhel-7}, RHEL 8 &mdash; {llvm-ver-rhel-8}, RHEL 9 &mdash; {llvm-ver-rhel-9}]
ifdef::publish-rhel-7+same-versions[:package-llvm}: {llvm-ver}]
ifdef::publish-rhel-8+same-versions[:package-llvm}: {llvm-ver}]
ifdef::publish-rhel-9+same-versions[:package-llvm}: {llvm-ver}]

ifdef::publish-rhel-7[:package-libomp: RHEL 7 &mdash; {libomp-ver-rhel-7}]
ifdef::publish-rhel-8[:package-libomp: RHEL 8 &mdash; {libomp-ver-rhel-8}]
ifdef::publish-rhel-9[:package-libomp: RHEL 9 &mdash; {libomp-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8[:package-libomp: RHEL 7 &mdash; {libomp-ver-rhel-7}, RHEL 8 &mdash; {libomp-ver-rhel-8}]
ifdef::publish-rhel-8+publish-rhel-9[:package-libomp: RHEL 8 &mdash; {libomp-ver-rhel-8}, RHEL 9 &mdash; {libomp-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-9[:package-libomp: RHEL 7 &mdash; {libomp-ver-rhel-7}, RHEL 9 &mdash; {libomp-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:package-libomp: RHEL 7 &mdash; {libomp-ver-rhel-7}, RHEL 8 &mdash; {libomp-ver-rhel-8}, RHEL 9 &mdash; {libomp-ver-rhel-9}]
ifdef::publish-rhel-7+same-versions[:package-libomp: {libomp-ver}]
ifdef::publish-rhel-8+same-versions[:package-libomp: {libomp-ver}]
ifdef::publish-rhel-9+same-versions[:package-libomp: {libomp-ver}]

ifdef::publish-rhel-7[:package-lld: RHEL 7 &mdash; {lld-ver-rhel-7}]
ifdef::publish-rhel-8[:package-lld: RHEL 8 &mdash; {lld-ver-rhel-8}]
ifdef::publish-rhel-9[:package-lld: RHEL 9 &mdash; {lld-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8[:package-lld: RHEL 7 &mdash; {lld-ver-rhel-7}, RHEL 8 &mdash; {lld-ver-rhel-8}]
ifdef::publish-rhel-8+publish-rhel-9[:package-lld: RHEL 8 &mdash; {lld-ver-rhel-8}, RHEL 9 &mdash; {lld-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-9[:package-lld: RHEL 7 &mdash; {lld-ver-rhel-7}, RHEL 9 &mdash; {lld-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:package-lld: RHEL 7 &mdash; {lld-ver-rhel-7}, RHEL 8 &mdash; {lld-ver-rhel-8}, RHEL 9 &mdash; {lld-ver-rhel-9}]
ifdef::publish-rhel-7+same-versions[:package-lld: {lld-ver}]
ifdef::publish-rhel-8+same-versions[:package-lld: {lld-ver}]
ifdef::publish-rhel-9+same-versions[:package-lld: {lld-ver}]

ifdef::publish-rhel-7[:package-python-lit: RHEL 7 &mdash; {python-lit-ver-rhel-7}]
ifdef::publish-rhel-8[:package-python-lit: RHEL 8 &mdash; {python-lit-ver-rhel-8}]
ifdef::publish-rhel-9[:package-python-lit: RHEL 9 &mdash; {python-lit-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8[:package-python-lit: RHEL 7 &mdash; {python-lit-ver-rhel-7}, RHEL 8 &mdash; {python-lit-ver-rhel-8}]
ifdef::publish-rhel-8+publish-rhel-9[:package-python-lit: RHEL 8 &mdash; {python-lit-ver-rhel-8}, RHEL 9 &mdash; {python-lit-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-9[:package-python-lit: RHEL 7 &mdash; {python-lit-ver-rhel-7}, RHEL 9 &mdash; {python-lit-ver-rhel-9}]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:package-python-lit: RHEL 7 &mdash; {python-lit-ver-rhel-7}, RHEL 8 &mdash; {python-lit-ver-rhel-8}, RHEL 9 &mdash; {python-lit-ver-rhel-9}]
ifdef::publish-rhel-7+same-versions[:package-python-lit: {python-lit-ver}]
ifdef::publish-rhel-8+same-versions[:package-python-lit: {python-lit-ver}]
ifdef::publish-rhel-9+same-versions[:package-python-lit: {python-lit-ver}]

[options="header"]
|===
|Name|Version|Description
|[package]*clang*|
{package-clang}
|An LLVM compiler front end for C and {cpp}.
|[package]*lldb*|
{package-lldb}
|A C and {cpp} debugger using portions of LLVM.
|[package]*compiler-rt*|
{package-compiler-rt}
|Runtime libraries for LLVM and Clang.
|[package]*llvm*|
{package-llvm}
|A collection of modular and reusable compiler and toolchain technologies.
|[package]*libomp*|
{package-libomp}
|A library for using Open MP API specification for parallel programming.
|[package]*lld*|
{package-lld}
|An LLVM linker.
|[package]*python-lit*|
{package-python-lit}
|A software testing tool for LLVM- and Clang-based test suites.
|===

[NOTE]
====
The CMake build manager is not part of {comp} Toolset.
ifdef::publish-rhel-7[On {RHEL}{nbsp}7, CMake is provided as a separate package.]
ifdef::publish-rhel-8[On {RHEL}{nbsp}8, CMake is available in the system repository.]
ifdef::publish-rhel-9[On {RHEL}{nbsp}9, CMake is available in the system repository.]
For more information on how to install CMake, see xref:proc_installing-cmake_{context}[Installing CMake on {rhel}].
====

//[role="_additional-resources"]
//.Additional resources

