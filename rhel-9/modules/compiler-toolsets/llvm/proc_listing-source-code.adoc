:_module-type: PROCEDURE

[id="proc_listing-source-code_{context}"]
= Listing source code 

[role="_abstract"]
Before you execute the program you are debugging, the LLDB debugger automatically displays the first 10 lines of source code.
Each time the execution of the program is stopped, LLDB displays the line of source code on which it stopped as well as its surrounding lines. 
You can use LLDB to manually trigger the display of source code during your debugging session.

.Prerequisites
* You have started an interactive debugging session. +
For more information, see xref:proc_starting-a-debugging-session-with-lldb_{context}[Starting a debugging session with LLDB].

.Procedure

* To list the first 10 lines of the source code of the program you are debugging, run:
[subs="quotes,attributes"]
+
----
(lldb) list
----

* To display the source code from a specific line, run:
+
[subs="quotes,attributes"]
----
(lldb) list &lt;__source_file_name__&gt;:&lt;__line_number__&gt;
----
+
** Replace `&lt;__source_file_name__&gt;` with the name of your source file and `&lt;__line_number__&gt;` with the number of the line you want to display.

//.Verification

//[role="_additional-resources"]
//.Additional resources


