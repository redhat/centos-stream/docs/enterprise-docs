:_module-type: PROCEDURE

[id="proc_displaying-current-program-data_{context}"]
= Displaying current program data

[role="_abstract"]

The LLDB debugger provides data on variables of any complexity, any valid expressions, and function call return values.
You can use LLDB to display data relevant to the program state. 

.Prerequisites
* You have started an interactive debugging session. +
For more information, see xref:proc_starting-a-debugging-session-with-lldb_{context}[Starting a debugging session with LLDB].

.Procedure
To display the current value of a certain variable, expression, or return value, run:

[subs="quotes,attributes"]
----
(lldb) print &lt;__data_name__&gt;
----

* Replace `&lt;__data_name__&gt;` with data you want to display.

//.Verification

//[role="_additional-resources"]


