:_module-type: PROCEDURE

[id="proc_installing-comp-toolset_{context}"]
= Installing {comp} Toolset

[role="_abstract"]
//{comp} Toolset is distributed as a collection of RPM packages that can be installed, updated, uninstalled, and inspected by using the standard package management tools that are included in {RHEL}.
Complete the following steps to install {comp} Toolset including all development and debugging tools as well as dependent packages.

.Prerequisites

* All available {RHEL} updates are installed.


.Procedure
ifdef::publish-rhel-8[]
On {RHEL}{nbsp}8, install the [package]`{comp-module}` module by running:

[subs="quotes,attributes"]
----
# yum module install {comp-module}
----

[IMPORTANT]
====
This does not install the LLDB debugger or the `python3-lit` package on {RHEL}{nbsp}8. To install the LLDB debugger and the `python3-lit` package, run:

[subs="quotes,attributes"]
----
# yum install lldb python3-lit
----
====
endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
On {RHEL}{nbsp}9, install the [package]`{comp-module}` package by running:

[subs="quotes,attributes"]
----
# dnf install {comp-module}
----

[IMPORTANT]
====
This does not install the LLDB debugger or the `python3-lit` package on {RHEL}{nbsp}9. To install the LLDB debugger and the `python3-lit` package, run:

[subs="quotes,attributes"]
----
# dnf install lldb python3-lit
----
====
endif::publish-rhel-9[]


//.Verification

//[role="_additional-resources"]
//.Additional resources


