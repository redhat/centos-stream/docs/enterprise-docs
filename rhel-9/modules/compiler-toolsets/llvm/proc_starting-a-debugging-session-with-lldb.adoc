:_module-type: PROCEDURE

[id="proc_starting-a-debugging-session-with-lldb_{context}"]
= Starting a debugging session 

[role="_abstract"]
Use LLDB to start an interactive debugging session. 

.Procedure
* To run LLDB on a program you want to debug, use the following command:

ifdef::publish-rhel-7[]
**  On {RHEL}{nbsp}7:
+
[subs="quotes,attributes"]
----
$ scl enable {comp-module}-{comp-ver-short} 'lldb &lt;__binary_file__&gt;'
----
*** Replace `&lt;__binary_file__&gt;` with the name of your compiled program.
+
You have started your LLDB debugging session in interactive mode.
Your command-line terminal now displays the default prompt `(lldb)`.
endif::publish-rhel-7[]

ifdef::publish-rhel-8[]
** On {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ lldb &lt;__binary_file_name__&gt;
----
*** Replace `&lt;__binary_file__&gt;` with the name of your compiled program.
+
You have started your LLDB debugging session in interactive mode.
Your command-line terminal now displays the default prompt `(lldb)`.
endif::publish-rhel-8[]

ifdef::publish-rhel-9[]
** On {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
$ lldb &lt;__binary_file__&gt;
----
*** Replace `&lt;__binary_file__&gt;` with the name of your compiled program.
+
You have started your LLDB debugging session in interactive mode.
Your command-line terminal now displays the default prompt `(lldb)`.
endif::publish-rhel-9[]

* To quit the debugging session and return to the shell prompt, run the following command:

[subs="quotes,attributes"]
----
(lldb) quit
----

//.Verification

//[role="_additional-resources"]
//.Additional resources


