:_module-type: REFERENCE

[id="ref_compatibility_{context}"]

= {comp} Toolset compatibility

ifdef::publish-rhel-7[:provided-for: {RHEL}{nbsp}7]
ifdef::publish-rhel-8[:provided-for: {RHEL}{nbsp}8]
ifdef::publish-rhel-9[:provided-for: {RHEL}{nbsp}9]
ifdef::publish-rhel-7+publish-rhel-8[:provided-for: {RHEL}{nbsp}7 and {RHEL}{nbsp}8]
ifdef::publish-rhel-8+publish-rhel-9[:provided-for: {RHEL}{nbsp}8 and {RHEL}{nbsp}9]
ifdef::publish-rhel-7+publish-rhel-9[:provided-for: {RHEL}{nbsp}7 and {RHEL}{nbsp}9]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:provided-for: {RHEL}{nbsp}7 and {RHEL}{nbsp}8 and {RHEL}{nbsp}9]

{comp} Toolset is available
for {provided-for}
on the following architectures:

ifdef::publish-rhel-8[:64-bit-arm: 64-bit ARM]
ifdef::publish-rhel-9[:64-bit-arm: 64-bit ARM]
ifdef::publish-rhel-7+publish-rhel-8[:64-bit-arm: 64-bit ARM (Only RHEL 8)]
ifdef::publish-rhel-8+publish-rhel-9[:64-bit-arm: 64-bit ARM]
ifdef::publish-rhel-7+publish-rhel-9[:64-bit-arm: 64-bit ARM (Only RHEL 9)]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:64-bit-arm: 64-bit ARM (RHEL 8 and RHEL 9)]

* AMD and Intel 64-bit
* {64-bit-arm} 
* IBM Power Systems, Little Endian
//ifdef::publish-rhel-7[]
//* IBM Power Systems, Big Endian
//endif::publish-rhel-7[]
//ifdef::publish-rhel-7+publish-rhel-8[]
//(Only RHEL 7)
//endif::publish-rhel-7+publish-rhel-8[]
* 64-bit IBM Z

