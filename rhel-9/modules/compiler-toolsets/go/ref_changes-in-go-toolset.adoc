:_module-type: REFERENCE

[id="ref_changes-in-go-toolset_{context}"]
//= Changes in Go Toolset

[role="_abstract"]

ifdef::publish-rhel-7[:new-version: {comp} Toolset has been updated from version {prev-comp-ver} to {comp-ver-rhel-7}.]
ifdef::publish-rhel-8[:new-version: {comp} Toolset has been updated from version {prev-comp-ver} to {comp-ver-rhel-8}.]
ifdef::publish-rhel-9[:new-version: {comp} Toolset has been updated from version {prev-comp-ver} to {comp-ver-rhel-9}.]
ifdef::publish-rhel-7+publish-rhel-8[:new-version: {comp} Toolset has been updated from version {prev-comp-ver} to {comp-ver-rhel-7} on RHEL{nbsp}7 and to {comp-ver-rhel-8} on RHEL{nbsp}8.]
ifdef::publish-rhel-8+publish-rhel-9[:new-version: {comp} Toolset has been updated from version {prev-comp-ver} to {comp-ver-rhel-8} on RHEL{nbsp}8 and RHEL{nbsp}9.]
ifdef::publish-rhel-7+publish-rhel-9[:new-version: {comp} Toolset has been updated from version {prev-comp-ver} to {comp-ver-rhel-7} on RHEL{nbsp}7 and RHEL{nbsp}9.]
ifdef::publish-rhel-7+publish-rhel-8+publish-rhel-9[:new-version: {comp} Toolset has been updated from version {prev-comp-ver} to {comp-ver-rhel-7} on RHEL{nbsp}7, RHEL{nbsp}8, and RHEL{nbsp}9.]

{new-version}

Notable changes include:

* Security fixes to the following packages:

** `crypto/tls`
** `mime/multipart`
** `net/http`
** `path/filepath`

* Bug fixes to:

** The `go` command
** The linker
** The runtime
** The `crypto/x509` package
** The `net/http` package
** The `time` package

[role="_additional-resources"]
For detailed information regarding the updates, see the upstream link:https://golang.org/doc/go1.19[Go {comp-ver-short} Release Notes].
//For more information on Delve, see the upstream link:https://github.com/go-delve/delve/tree/master/Documentation[Delve documentation].

//[role="_additional-resources"]
//.Additional resources


