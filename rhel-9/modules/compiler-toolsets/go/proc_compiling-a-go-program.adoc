:_module-type: PROCEDURE

[id="proc_compiling-a-go-program_{context}"]
= Compiling a Go program

[role="_abstract"]
You can compile your {comp} program using the {comp} compiler.
The {comp} compiler creates an executable binary file as a result of compiling. 

.Prerequisites
* A set up {comp} workspace. +
For information on how to set up a workspace, see xref:proc_setting-up-a-go-workspace_{context}[Setting up a {comp} workspace].

.Procedure
In your project directory, run:

ifdef::publish-rhel-7[]
* On {RHEL}{nbsp}7:
+
[subs="quotes,attributes"]
----
$ scl enable {comp-module}-{comp-ver-short} 'go build &lt;__output_file__&gt;'
----
+
** Replace `&lt;__output_file__&lt;` with the desired name of your output file and `&lt;__go_main_package__&gt;` with the name of your main package.
endif::publish-rhel-7[]
ifdef::publish-rhel-8[]
* On {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ go build &lt;__output_file__&gt; 
----
+
** Replace `&lt;__output_file__&gt;` with the desired name of your output file and `&lt;__go_main_package__&gt;` with the name of your main package.
endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
* On {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
$ go build &lt;__output_file__&gt;
----
+
** Replace `&lt;__output_file__&gt;` with the desired name of your output file and `&lt;__go_main_package__&gt;` with the name of your main package.

endif::publish-rhel-9[]

////
[NOTE]
====
To check if your project can be built, use the `go build` command without the `-o` option. 
That way, the {comp} compiler names your output file _go_main_package_.
If _go_main_package_ is not the main package, or if multiple projects or `*.go` files are specified, the resulting binaries are discarded.
====
////
//[role="_additional-resources"]
//.Additional resources


