:_module-type: PROCEDURE

[id="proc_accessing-the-ubi-go-toolset-container-image_{context}"]
= Accessing the UBI {comp} Toolset container image on RHEL 8

[role="_abstract"]
On RHEL{nbsp}8, install the UBI {comp} Toolset container image to access {comp} Toolset. +
Alternatively, you can install {comp} Toolset to the RHEL 8 base UBI container image. For further information, see xref:proc_accessing-ubi-container-images_{context}[Accessing {comp} Toolset from the base UBI container image on RHEL 8].

//.Prerequisites

.Procedure

To pull the UBI {comp} Toolset container image from the Red Hat registry, run:

//ifdef::publish-rhel-7[]
//For an image based on UBI 7:
//ifdef::publish-rhel-7[]
[subs="quotes,attributes"]
----
# podman pull registry.access.redhat.com/ubi8/go-toolset
----
//.Verification

//[role="_additional-resources"]
//.Additional resources


