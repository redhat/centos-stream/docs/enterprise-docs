:_module-type: PROCEDURE


[id="proc_creating-a-container-image-of-comp-toolset_{context}"]
= Creating a container image of {comp} Toolset on RHEL 8

[role="_abstract"]
On RHEL 8, {comp} Toolset packages are part of the Red Hat Universal Base Images (UBIs) repositories.
To keep the container size small, install only individual packages instead of the entire {comp} Toolset.

.Prerequisites
* An existing Containerfile. +
For more information on creating Containerfiles, see the link:https://docs.docker.com/engine/reference/builder/[Dockerfile reference] page.

.Procedure
* Visit the link:https://catalog.redhat.com/software/container-stacks/search?q=Red%20Hat%20universal%20base%20image&p=1[Red Hat Container Catalog].
* Select a UBI.
* Click *Get this image* and follow the instructions.

* To create a container containing {comp} Toolset, add the following lines to your Containerfile:
////
ifdef::publish-rhel-7[]
** On {rhel} 7:
+
[subs="quotes,attributes"] 
----
FROM registry.access.redhat.com/ubi7/ubi:__latest__

RUN yum install -y {comp-module}-{comp-ver-short} 
----
endif::publish-rhel-7[]
////
//ifdef::publish-rhel-8[]
//** On {rhel} 8:

[subs="quotes,attributes"] 
----
FROM registry.access.redhat.com/ubi8/ubi:__latest__

RUN yum install -y {comp-module}
----
//endif::publish-rhel-8[]
////
ifdef::publish-rhel-9[]
** On {rhel} 9:
+
[subs="quotes,attributes"] 
----
FROM registry.access.redhat.com/ubi9/ubi:__latest__

RUN yum install -y {comp-module}
----
endif::publish-rhel-9[]
////
//.Verification
* To create a container image containing an individual package only, add the following lines to your Containerfile:
////
ifdef::publish-rhel-7[]
** On {RHEL} 7:
+
[subs="quotes,attributes"]
----
RUN yum install {comp-module}-{comp-ver-short}-&lt;__package-name__&gt;
----
+
*** Replace `&lt;__package_name__&gt;` with the name of the package you want to install.
endif::publish-rhel-7[]
////
//ifdef::publish-rhel-8[]
//** On {RHEL} 8:

[subs="quotes,attributes"]
----
RUN yum install &lt;__package-name__&gt;
----

** Replace `&lt;__package_name__&gt;` with the name of the package you want to install.
//endif::publish-rhel-8[]
////
ifdef::publish-rhel-9[]
** On {RHEL} 9:
+
[subs="quotes,attributes"]
----
RUN yum install &lt;__package-name__&gt;
----
+
*** Replace `&lt;__package_name__&gt;` with the name of the package you want to install.
endif::publish-rhel-9[]
////
