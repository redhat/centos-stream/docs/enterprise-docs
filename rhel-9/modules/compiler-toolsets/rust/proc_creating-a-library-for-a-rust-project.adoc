:_module-type: PROCEDURE

[id="proc_creating-a-library-for-a-rust-project_{context}"]
= Creating a {comp} library project

//[role="_abstract"]
Complete the following steps to create a {comp} library project using the Cargo build tool.

.Procedure
To create a {comp} library project, run the following command:

ifdef::publish-rhel-7[]
* On {RHEL}{nbsp}7:
+
[subs="quotes,attributes"]
----
$ scl enable {comp-pkg} 'cargo new --lib &lt;__project_name__&gt;'
----
** Replace `&lt;__project_name__&gt;` with the name of your {comp} project.

endif::publish-rhel-7[]
ifdef::publish-rhel-8[]
* On {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ cargo new --lib &lt;__project_name__&gt;
----
** Replace `&lt;__project_name__&gt;` with the name of your {comp} project.

endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
* On {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
$ cargo new --lib &lt;__project_name__&gt;
----
** Replace `&lt;__project_name__&gt;` with the name of your {comp} project.

endif::publish-rhel-9[]

[NOTE]
====
To edit the project code, edit the source file, `lib.rs`, in the [filename]`src` subdirectory.
====

.Additional resources
* link:https://doc.rust-lang.org/book/ch07-00-managing-growing-projects-with-packages-crates-and-modules.html[Managing Growing Projects with Packages, Crates, and Modules]