:_module-type: PROCEDURE

[id="proc_setting-up-a-ubi-repository_{context}"]
= Setting up a UBI repository

[role="_abstract"]

{comp} Toolset packages are part of the Red Hat Universal Base Images (UBIs) repositories.
To build a container image of {comp}, set up UBI to access UBI repositories and build a container image of {comp}.

.Procedure

To set up a UBI, complete the following steps:

ifdef::publish-rhel-7[]
. On {rhel} 7, visit the link:https://access.redhat.com/containers/[Red Hat Container Catalog] and search for UBI 7.
. Select a UBI.
. Click *Get this image* and follow the instructions.
endif::publish-rhel-7[]
ifdef::publish-rhel-8[]
. On {rhel} 8, visit the link:https://access.redhat.com/containers/[Red Hat Container Catalog] and search for UBI 8.
. Select a UBI.
. Click *Get this image* and follow the instructions.
endif::publish-rhel-8[]


[role="_additional-resources"]
.Additional resources
//* For more information on using UBI images, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/building_running_and_managing_containers/index/#assembly_working-with-container-images_building-running-and-managing-containers[Building, Running, and Managing Containers - Chapter 3, Working with Container Images].

