:_module-type: PROCEDURE

[id="proc_creating-a-binary-program-on-the-command-line_{context}"]
= Creating a binary program on the command line


.Procedure

To create a binary program on the command line, run the [command]`cargo` tool with the option `--bin`:

ifdef::publish-rhel-7[]
* For {RHEL}{nbsp}7:
+
[subs="quotes,attributes"]
----
$ scl enable {comp-pkg} 'cargo new --bin __project_name__'
----
endif::publish-rhel-7[]
ifdef::publish-rhel-8[]
* For {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ cargo new --bin __project_name__
----
endif::publish-rhel-8[]

                                                                  43,0-1        78%


