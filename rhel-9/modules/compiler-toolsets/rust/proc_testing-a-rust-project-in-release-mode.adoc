:_module-type: PROCEDURE

[id="proc_testing-a-rust-project-in-release-mode_{context}"]
= Testing a Rust project in release mode

[role="_abstract"]

Test your {comp} program in release mode using the Cargo build tool.
Release mode is optimizing your source code and can therefore increase compilation time while ensuring that the compiled binary will run faster. Use this mode to produce optimized artifacts suitable for release and production. +
Cargo first rebuilds your project and then runs the tests found in the project.
Note that you can only test functions that are free, monomorphic, and take no arguments.
The function return type must be either `()` or `Result<(), E> where E: Error`.

For information on testing your project in debug mode, see xref:proc_testing-a-rust-project_{context}[Testing a {comp} project].

.Prerequisites
* A built {comp} project. +
For information on how to build a {comp} project, see xref:proc_building-a-rust-project_{context}[Building a {comp} project].

.Procedure

* Add the test attribute `#[test]` in front of your function.
* To run tests for a {comp} project managed by Cargo in release mode, run in the project directory:

ifdef::publish-rhel-7[]
** On {RHEL}{nbsp}7:
+
[subs="quotes,attributes"]
----
$ scl enable {comp-pkg} 'cargo test --release'
----
endif::publish-rhel-7[]
ifdef::publish-rhel-8[]
** On {RHEL}{nbsp}8:
+
[subs="quotes,attributes"]
----
$ cargo test --release
----
endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
** On {RHEL}{nbsp}9:
+
[subs="quotes,attributes"]
----
$ cargo test --release
----
endif::publish-rhel-9[]



[role="_additional-resources"]
.Additional resources
* For more information on performing tests in your {comp} project, see https://doc.rust-lang.org/reference/attributes/testing.html[The {comp} Reference &mdash; Testing attributes].
