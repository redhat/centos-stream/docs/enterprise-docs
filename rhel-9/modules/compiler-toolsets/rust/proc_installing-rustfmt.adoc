:_module-type: PROCEDURE

[id="proc_installing-rustfmt_{context}"]

= Installing rustfmt
//[role="_abstract"]
Complete the following steps to install the `rustfmt` formatting tool.

.Prerequisites
* {comp} Toolset is installed. +
For more information, see xref:proc_installing-comp-toolset_assembly_rust-toolset[Installing {comp} Toolset].

.Procedure

Run the following command to install `rustfmt`:

ifdef::publish-rhel-7[]
* On {RHEL}{nbsp}7:
+
[subs="attributes"]
----
# yum install {comp-pkg}-rustfmt
----
endif::publish-rhel-7[]
ifdef::publish-rhel-8[]
* On {RHEL}{nbsp}8:
+
[subs="attributes"]
----
# yum install rustfmt
----
endif::publish-rhel-8[]
ifdef::publish-rhel-9[]
* On {RHEL}{nbsp}9:
+
[subs="attributes"]
----
# dnf install rustfmt
----
endif::publish-rhel-9[]

//.Verification
//[role="_additional-resources"]
//.Additional resources

