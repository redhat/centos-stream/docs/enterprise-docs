:_mod-docs-content-type: PROCEDURE
[id="installing-the-ansible-freeipa-package_{context}"]
= Installing the ansible-freeipa package

The following procedure describes how to install the the [package]`ansible-freeipa` roles.

.Prerequisites

* Ensure that the controller is a {RHEL} system with a valid subscription. If this is not the case, see the official Ansible documentation https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html[Installation guide] for alternative installation instructions.
* Ensure that you can reach the managed node over the `SSH` protocol from the controller. Check that the managed node is listed in the `/root/.ssh/known_hosts` file of the controller.



.Procedure

Run the following procedure on the Ansible controller.

. Enable the required repository:
+
[literal,subs="quotes"]
....
# *subscription-manager repos --enable rhel-9-for-x86_64-appstream-rpms*
....

. Install the IdM Ansible roles:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *{PackageManagerCommand} install ansible-freeipa*
....


The roles are installed to the `/usr/share/ansible/roles/` directory.
