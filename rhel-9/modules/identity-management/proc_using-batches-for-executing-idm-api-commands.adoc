:_mod-docs-content-type: PROCEDURE

[id="using-batches-for-executing-idm-api-commands_{context}"]
= Using batches for executing IdM API commands

[role="_abstract"]

You can execute multiple IdM API commands with a single call by using the  `batch` command. The following example shows how to create multiple IdM users.

.Prerequisites

* The IdM API is initialized. For more information, see xref:proc_initializing-idm-api_assembly_basics-of-idm-api[Initializing IdM API]. 

.Procedure

* To create 100 IdM users in one batch, include the following code into your script:
+
[literal,subs="+quotes,attributes"]
----

batch_args = []
for i in range(100):
    user_id = "user%i" % i
    args = [user_id]
    kw = {
        'givenname' : user_id,
        'sn' : user_id
    }
    batch_args.append({
        'method' : 'user_add',
        'params' : [args, kw]
    })
ret = api.Command.batch(*batch_args)

----

