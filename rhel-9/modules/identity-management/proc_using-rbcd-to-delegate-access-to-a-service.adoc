:_mod-docs-content-type: PROCEDURE

[id="using-rbcd-to-delegate-access-to-a-service_{context}"]
= Using RBCD to delegate access to a service

[role="_abstract"]
To use RBCD to delegate access to a service, add a rule on the host where the service is running. This example procedure describes how to delegate user credentials to a file server `nfs/client.example.test` for a web application with a Kerberos service `HTTP/client.example.test`. You can do this on the `client.example.test` host, because a host always manages services running on itself.

.Prerequisites

* You have access to the `/etc/krb5.keytab` file of the `client.example.test` host.
* A `nfs/client.example.test` service keytab exists.
* A keytab `/path/to/web-service.keytab` for `HTTP/client.example.test` exists.

.Procedure

. On the `client.example.test` host, obtain a Kerberos ticket:
+
----
# kinit -k
----

. Define the RBCD ACL:
+
----
# ipa service-add-delegation nfs/client.example.test HTTP/client.example.test

-------------------------------------------------------
Added new resource delegation to the service principal "nfs/client.example.test@EXAMPLE.TEST"
-------------------------------------------------------
  Principal name: nfs/client.example.test@EXAMPLE.TEST
  Delegation principal: HTTP/client.example.test@EXAMPLE.TEST
  
----

.Verification

To verify that the delegation is set up correctly, you can simulate a `testuser` user logging in through the `HTTP` service and performing a protocol transition to the `NFS` service.

. View the NFS service to verify that the delegation rule is present:
+
----
# ipa service-show nfs/client.example.test

  Principal name: nfs/client.example.test@EXAMPLE.TEST
  Principal alias: nfs/client.example.test@EXAMPLE.TEST
  Delegation principal: HTTP/client.example.test@EXAMPLE.TEST
  Keytab: True
  Managed by: client.example.test
----

. Obtain a Kerberos ticket for the HTTP service principal:
+
----
# kinit -kt http.keytab HTTP/client.example.test
----

. Verify that the ticket granting ticket is present:
+
----
# klist -f
Ticket cache: KCM:0:99799
Default principal: HTTP/client.example.test@EXAMPLE.TEST

Valid starting       Expires              Service principal
10/13/2023 14:39:23  10/14/2023 14:05:07  krbtgt/EXAMPLE.TEST@EXAMPLE.TEST
	Flags: FIA

----

. Perform a protocol transition on behalf of `testuser`:
+
----
# kvno -U testuser -P nfs/client.example.test
nfs/client.example.test@EXAMPLE.TEST: kvno = 1
----

. Verify that tickets obtained during protocol transition on behalf of `testuser` are present:
+
----
# klist -f
Ticket cache: KCM:0:99799
Default principal: HTTP/client.example.test@EXAMPLE.TEST

Valid starting       Expires              Service principal
10/13/2023 14:39:38  10/14/2023 14:05:07  HTTP/client.example.test@EXAMPLE.TEST
	for client testuser@EXAMPLE.TEST, Flags: FAT
10/13/2023 14:39:23  10/14/2023 14:05:07  krbtgt/EXAMPLE.TEST@EXAMPLE.TEST
	Flags: FIA
10/13/2023 14:39:38  10/14/2023 14:05:07  nfs/client.example.test@EXAMPLE.TEST
	for client testuser@EXAMPLE.TEST, Flags: FAT
----
