:_mod-docs-content-type: CONCEPT

[id="idm-api-and-idm-cli-commands-comparison_{context}"]
= IdM API and IdM CLI commands comparison

[role="_abstract"]

You can use the IdM API commands in the Python interactive console. The IdM API commands are different from the `ipa` tool commands.

.IdM CLI and IdM API commands difference

Command naming structure:: The `ipa` CLI commands use the hyphen, as in `user-add`, but IdM API commands use the underscore instead, as in `user_add`.

Parameter naming:: The parameters are different for IdM CLI commands and IdM API commands. For example, the IdM CLI [command]`user-add` command has a parameter `first` but the IdM API [command]`user_add` command has a parameter `givenname`. 

Date format:: The following date formats are available for IdM CLI:
+
* `%Y%m%d%H%M%SZ`
* `%Y-%m-%dT%H:%M:%SZ`
* `%Y-%m-%dT%H:%MZ`
* `%Y-%m-%dZ`
* `%Y-%m-%d %H:%M:%SZ`
* `%Y-%m-%d %H:%MZ`
+
Additionally, the IdM API can use the Python built-in class `datetime`.

.Useful CLI tools

* The `console` starts an interactive Python console, which you can use to run IdM API commands.
+
* The [command]`help` command shows description of the topics and the commands and includes various examples.
+
* The [command]`show-mapping` command shows the mapping between CLI parameter names and LDAP attributes.

