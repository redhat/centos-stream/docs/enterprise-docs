:_mod-docs-content-type: PROCEDURE

[id="ensuring-fips-compliance-for-a-rhel-9-replica-joining-a-rhel-8-idm-environment_{context}"]
= Ensuring FIPS compliance for a RHEL 9 replica joining a RHEL 8 IdM environment

If RHEL {IPA} (IdM) was originally installed on a RHEL 8.6 or earlier system, then the `AES HMAC-SHA1` encryption types it uses are not supported by RHEL 9 in FIPS mode by default. To add a RHEL 9 replica in FIPS mode to the deployment, you must enable these encryption keys on the RHEL 9. For more information, see the link:https://access.redhat.com/solutions/7003853[AD Domain Users unable to login in to the FIPS-compliant environment] KCS solution.
