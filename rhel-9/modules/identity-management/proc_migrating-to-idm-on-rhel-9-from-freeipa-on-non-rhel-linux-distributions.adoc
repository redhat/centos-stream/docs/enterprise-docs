:_mod-docs-content-type: PROCEDURE

[id="migrating-to-idm-on-rhel-9-from-freeipa-on-non-rhel-linux-distributions_{context}"]
= Migrating to IdM on RHEL 9 from FreeIPA on non-RHEL Linux distributions

[role="_abstract"]
To migrate a FreeIPA deployment on a non-RHEL Linux distribution to an Identity Management (IdM) deployment on RHEL 9 servers, you must first add a new RHEL 9 IdM Certificate Authority (CA) replica to your existing FreeIPA environment, transfer certificate-related roles to it, and then retire the non-RHEL FreeIPA servers.

[WARNING]
====
Performing an in-place conversion of a non-RHEL FreeIPA server to a RHEL 9 IdM server using the Convert2RHEL tool is not supported.
====

[IMPORTANT]
====
Because the use of the `SHA-1` algorithm is disabled in the `DEFAULT` system-wide cryptographic policy in RHEL 9, multiple known issues might arise if a RHEL 9 system is used in the same IdM deployment as a non-RHEL-9 system. For details, see:

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/9.0_release_notes/index#known-issue_identity-management[Release Notes for Red Hat Enterprise Linux 9.0]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/9.1_release_notes/index#known-issue_identity-management[Release Notes for Red Hat Enterprise Linux 9.1]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/9.2_release_notes/index#known-issues-identity-management[Release Notes for Red Hat Enterprise Linux 9.2]
====

[IMPORTANT]
====
After upgrading your IdM replica to RHEL 9.2, the IdM Kerberos Distribution Centre (KDC) might fail to issue ticket-granting tickets (TGTs) to users who do not have Security Identifiers (SIDs) assigned to their accounts. Consequently, the users cannot log in to their accounts.

To work around the problem, generate SIDs by running `# ipa config-mod --enable-sid --add-sids` as an IdM administrator on another IdM replica in the topology. Afterward, if users still cannot log in, examine the Directory Server error log. You might have to adjust ID ranges to include user POSIX identities.
====


.Prerequisites

On the RHEL 9 system:

. The latest version of {RHEL} is installed on the system. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/interactively_installing_rhel_from_installation_media/index[Interactively installing RHEL from installation media].

. Ensure the system is an IdM client enrolled into the domain for which the FreeIPA server is authoritative. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_installing-an-idm-client_installing-identity-management[Installing an IdM client: Basic scenario].

. Ensure the system meets the requirements for IdM server installation. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/preparing-the-system-for-ipa-server-installation_installing-identity-management[Preparing the system for IdM server installation].

. Ensure the system is authorized for the installation of an IdM replica. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/preparing-the-system-for-ipa-replica-installation_installing-identity-management#authorizing-the-installation-of-a-replica-on-an-ipa-client_preparing-the-system-for-ipa-replica-installation[Authorizing the installation of a replica on an IdM client].

On the non-RHEL FreeIPA server:

. Ensure you know the time server that the system is synchronized with:
+
[literal,subs="+quotes,verbatim"]
....
[root@freeipaserver ~]# *ntpstat*
synchronised to NTP server (*ntp.example.com*) at stratum 3
   time correct to within 42 ms
   polling server every 1024 s
....

+
. Update the [package]*ipa-** packages to their latest version:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@freeipaserver ~]# *{PackageManagerCommand} update ipa-**
....

.Procedure

. To perform the migration, follow the same procedure as link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/migrating_to_identity_management_on_rhel_9/assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers_migrating-to-idm-on-rhel-9[Migrating your IdM environment from RHEL 8 servers to RHEL 9 servers], with your non-RHEL FreeIPA CA replica acting as the RHEL 8 server:

.. Configure a RHEL 9 server and add it as an IdM replica to your current FreeIPA environment on the non-RHEL Linux distribution. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/migrating_to_identity_management_on_rhel_9/assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers_migrating-to-idm-on-rhel-9#install-replica_assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers[Installing the RHEL 9 Replica].

.. Make the RHEL 9 replica the certificate authority (CA) renewal server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/migrating_to_identity_management_on_rhel_9/assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers_migrating-to-idm-on-rhel-9#assigning-the-ca-renewal-server-role-to-the-rhel-9-idm-server_assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers[Assigning the CA renewal server role to the RHEL 9 IdM server].

.. Stop generating the certificate revocation list (CRL) on the non-RHEL server and redirect CRL requests to the RHEL 9 replica. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/migrating_to_identity_management_on_rhel_9/assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers_migrating-to-idm-on-rhel-9#stopping-crl-generation-on-rhel8-IdM-CA-server_assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers[Stopping CRL generation on a RHEL 8 IdM CA server].

.. Start generating the CRL on the RHEL 9 server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/migrating_to_identity_management_on_rhel_9/assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers_migrating-to-idm-on-rhel-9#starting-crl-generation-on-the-new-rhel-9-idm-ca-server_assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers[Starting CRL generation on the new RHEL 9 IdM CA server].

.. Stop and decommission the original non-RHEL FreeIPA CA renewal server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/migrating_to_identity_management_on_rhel_9/assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers_migrating-to-idm-on-rhel-9#stop-decommission-server_assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers[Stopping and decommissioning the RHEL 8 server].



[role="_additional-resources"]
.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/migrating_to_identity_management_on_rhel_9/assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers_migrating-to-idm-on-rhel-9[Migrating your IdM environment from RHEL 8 servers to RHEL 9 servers]
