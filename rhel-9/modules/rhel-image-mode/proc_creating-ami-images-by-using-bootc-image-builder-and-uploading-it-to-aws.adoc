:_newdoc-version: 2.16.0
:_template-generated: 2024-04-15
:_mod-docs-content-type: PROCEDURE

[id="creating-ami-images-by-using-bootc-image-builder-and-uploading-it-to-aws_{context}"]
= Creating AMI images by using bootc-image-builder and uploading it to AWS

[role="_abstract"]
Create an Amazon Machine Image (AMI) from a bootc image and use it to launch an Amazon Web Service EC2 (Amazon Elastic Compute Cloud) instance.

.Prerequisites

* You have Podman installed on your host machine.
* You have an existing `AWS S3` bucket within your AWS account.
* You have root access to run the `bootc-image-builder` tool, and run the containers in `--privileged` mode, to build the images.
* You have the `vmimport` service role configured on your account to import an AMI into your AWS account.

.Procedure

. Create a disk image from the bootc image.
* Configure the user details in the Containerfile. Make sure that you assign it with sudo access.
* Build a customized operating system image with the configured user from the Containerfile. It creates a default user with passwordless sudo access.

. Optional: Configure the machine image with `cloud-init`. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/using_image_mode_for_rhel_to_build_deploy_and_manage_operating_systems/index#users-and-groups-configuration_managing-users-groups-ssh-key-and-secrets-in-image-mode-for-rhel[Users and groups configuration - Injecting users and SSH keys by using cloud-init]. The following is an example:
+
[literal,subs="+quotes"]
----
FROM registry.redhat.io/rhel9/rhel-bootc:9.4

RUN dnf -y install cloud-init && \
    ln -s ../cloud-init.target /usr/lib/systemd/system/default.target.wants && \
    rm -rf /var/{cache,log} /var/lib/{dnf,rhsm}
----
+
[NOTE]
====
You can also use `cloud-init` to add users and additional configuration by using instance metadata.
====

. Build the bootc image. For example, to deploy the image to an `x86_64` AWS machine, use the following commands:
+
[literal,subs="+quotes"]
----
$ *podman build -t quay.io/_<namespace>_/_<image>_:__<tag>__ .*
$ *podman push quay.io/_<namespace>_/_<image>_:__<tag>__ .*
----

. Use the `bootc-image-builder` tool to create an AMI from the bootc container image. 
+
NOTE: If you do not have the container storage mount and `--local` image options, your image must be public.

.. The following is an example of creating a public AMI image:
+
[literal,subs="+quotes"]
----
$ *sudo podman run \*
  *--rm \*
  *-it \*
  *--privileged \*
  *--pull=newer \*
  *-v $HOME/.aws:/root/.aws:ro \*
  *--env AWS_PROFILE=default \*
  *registry.redhat.io/rhel9/bootc-image-builder:latest \*
  *--type ami \*
  *--aws-ami-name rhel-bootc-x86 \*
  *--aws-bucket rhel-bootc-bucket \*
  *--aws-region us-east-1 \*
*quay.io/_<namespace>_/_<image>_:__<tag>__*
----

.. The following is an example of creating a private AMI image:
+
[literal,subs="+quotes"]
----
$ *sudo podman run \*
    *--rm \*
    *-it \*
    *--privileged \*
    *--pull=newer \*
    *-v $HOME/.aws:/root/.aws:ro \*
    *--env AWS_PROFILE=default \*
    *registry.redhat.io/rhel9/bootc-image-builder:latest \*
    *--type ami \*
    *--aws-ami-name rhel-bootc-x86 \*
    *--aws-bucket rhel-bootc-bucket \*
    *--local*    
  *quay.io/_<namespace>_/_<image>_:__<tag>__*
----
+
You can also inject all your AWS configuration parameters by using `--env AWS_*`. 
+
IMPORTANT: The following flags must be specified all together. If you do not specify any flag, the AMI is exported to your output directory.

* `--aws-ami-name` - The name of the AMI image in AWS
* `--aws-bucket` - The target S3 bucket name for intermediate storage when you are creating the AMI
* `--aws-region` - The target region for AWS uploads
+
The `bootc-image-builder` tool builds an AMI image and uploads it to your AWS s3 bucket by using your AWS credentials to push and register an AMI image after building it. 

.Next steps

* You can deploy your image. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/using_image_mode_for_rhel_to_build_deploy_and_manage_operating_systems/index#deploying-a-container-image-to-aws-with-an-ami-disk-image_deploying-the-rhel-bootc-images[Deploying a container image to AWS with an AMI disk image]. 
* You can make updates to the image and push the changes to a registry. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/using_image_mode_for_rhel_to_build_deploy_and_manage_operating_systems/index#managing-rhel-bootc-images[Managing RHEL bootc images].


.Additional resources

* link:https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html[AWS CLI documentation]



