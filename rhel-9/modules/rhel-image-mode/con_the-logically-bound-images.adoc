:_newdoc-version: 2.18.3
:_template-generated: 2024-10-24

:_mod-docs-content-type: CONCEPT

[id="the-logically-bound-images_{context}"]
= Logically bound images

Logically bound images enable an association of the container application images to a base bootc system image. The term _logically bound_ is used to contrast with physically bound images. The logically bound images offer the following benefits:

* You can update the bootc system without re-downloading the application container images.
* You can update the application container images without modifying the bootc system image, which is especially useful for development work.
 
The following are examples for lifecycle bound workloads, whose activities are usually not updated outside of the host:

* Logging, for example, journald->remote log forwarder container
* Monitoring, for example, Prometheus node_exporter
* Configuration management agents
* Security agents

Another important property of the logically bound images is that they must be present and available on the host, possibly from a very early stage in the boot process. 

Differently from the default usage of tools like Podman or Docker, images might be pulled dynamically after the boot starts, which requires a functioning network. For example, if the remote registry is temporarily unavailable, the host system might run longer without log forwarding or monitoring, which is not desirable.
Logically bound images enable you to reference container images similarly to you can with `ExecStart=` in a systemd unit.

When using logically bound images, you must manage multiple container images for the system to install the logically bound images. This is an advantage and also a disadvantage. For example, for a disconnected or offline installation, you must mirror all the containers, not just one. The application images are only referenced from the base image as `.image` files or an equivalent.

Logically bound images installation::

When you run `bootc install`, logically bound images must be present in the default `/var/lib/containers` container store. The images will be copied into the target system and present directly at boot, alongside the bootc base image.

Logically bound images lifecycle::

Logically bound images are referenced by the bootable container and have guaranteed availability when the bootc based server starts. The image is always upgraded by using `bootc upgrade` and is available as `read-only` to other processes, such as Podman.

Logically bound images management on upgrades, rollbacks, and garbage collection::

* During upgrades, the logically bound images are managed exclusively by bootc. 
* During rollbacks, the logically bound images corresponding to rollback deployments are retained. 
* bootc performs garbage collection of unused bound images.



