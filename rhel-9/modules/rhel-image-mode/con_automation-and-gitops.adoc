:_newdoc-version: 2.16.0
:_template-generated: 2024-04-13

:_mod-docs-content-type: CONCEPT

[id="automation-and-gitops_{context}"]
= Automation and GitOps

[role="_abstract"]
You can automate the building process by using CI/CD pipelines so that an update process can be triggered by events, such as updating an application. You can use automation tools that track these updates and trigger the CI/CD pipelines. The pipeline keeps the systems up to date by using the transactional background operating system updates.

