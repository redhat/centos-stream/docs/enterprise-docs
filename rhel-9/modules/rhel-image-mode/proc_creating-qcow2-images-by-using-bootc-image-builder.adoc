:_newdoc-version: 2.16.0
:_template-generated: 2024-04-15
:_mod-docs-content-type: PROCEDURE

[id="creating-qcow2-images-by-using-bootc-image-builder_{context}"]
= Creating QCOW2 images by using bootc-image-builder

[role="_abstract"]
Build a RHEL bootc image into a QEMU Disk Images (QCOW2) image for the architecture that you are running the commands on.

The RHEL base image does not include a default user. Optionally, you can inject a user configuration with the `--config` option to run the bootc-image-builder container. Alternatively, you can configure the base image with `cloud-init` to inject users and SSH keys on first boot. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/using_image_mode_for_rhel_to_build_deploy_and_manage_operating_systems/index#users-and-groups-configuration_managing-users-groups-ssh-key-and-secrets-in-image-mode-for-rhel[Users and groups configuration - Injecting users and SSH keys by using cloud-init].

.Prerequisites

* You have Podman installed on your host machine.
* You have `virt-install` installed on your host machine.
* You have root access to run the `bootc-image-builder` tool, and run the containers in `--privileged` mode, to build the images.

.Procedure

. Optional: Create a `config.toml` to configure user access, for example:
+
[literal,subs="+quotes"]
----
[[customizations.user]]
name = "user"
password = "pass"
key = "ssh-rsa AAA ... user@email.com"
groups = ["wheel"]
----

. Run `bootc-image-builder`. Optionally, if you want to use user access configuration, pass the `config.toml` as an argument.
+
NOTE: If you do not have the container storage mount and `--local` image options, your image must be public. 

.. The following is an example of creating a public QCOW2 image:
+
[literal,subs="+quotes"]
----
$ *sudo podman run \*
    *--rm \*
    *-it \*
    *--privileged \*
    *--pull=newer \*
    *--security-opt label=type:unconfined_t \*
    *-v ./config.toml:/config.toml \*
    *-v ./output:/output \*
    *registry.redhat.io/rhel9/bootc-image-builder:latest \*
    *--type qcow2 \*
    *--config /config.toml \*
  *quay.io/_<namespace>_/_<image>_:__<tag>__*
----

.. The following is an example of creating a private QCOW2 image:
+
[literal,subs="+quotes"]
----
$ *sudo podman run \*
    *--rm \*
    *-it \*
    *--privileged \*
    *--pull=newer \*
    *--security-opt label=type:unconfined_t \*
    *-v $(pwd)/config.toml:/config.toml:ro \*
    *-v $(pwd)/output:/output \*
    *-v /var/lib/containers/storage:/var/lib/containers/storage \* 
    *registry.redhat.io/rhel9/bootc-image-builder:latest \*
    *--local*
    *--type qcow2 \*
    *quay.io/_<namespace>_/_<image>_:__<tag>__*
----
+
You can find the `.qcow2` image in the output folder.

[role="_additional-resources"]
.Next steps

* You can deploy your image. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/using_image_mode_for_rhel_to_build_deploy_and_manage_operating_systems/index#deploying-a-container-image-using-kvm-qemu-with-a-qcow2-disk-image_deploying-the-rhel-bootc-images[Deploying a container image using KVM with a QCOW2 disk image]. 

* You can make updates to the image and push the changes to a registry. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/using_image_mode_for_rhel_to_build_deploy_and_manage_operating_systems/index#managing-rhel-bootc-images[Managing RHEL bootc images].


