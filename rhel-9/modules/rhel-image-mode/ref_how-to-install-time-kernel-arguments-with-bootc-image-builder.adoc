:_newdoc-version: 2.18.3
:_template-generated: 2024-07-16

:_mod-docs-content-type: REFERENCE

[id="how-to-install-time-kernel-arguments-with-bootc-image-builder_{context}"]
= How to add install-time kernel arguments with `bootc-image-builder`

The `bootc-image-builder` tool supports the `customizations.kernel.append` during install-time. 

To add the kernel arguments with `bootc-image-builder`, use the following customization:
[literal,subs="+quotes"]
----
{
  "customizations": {
    "kernel": {
      "append": "mitigations=auto,nosmt"
    }
  }
}
----


