:_newdoc-version: 2.15.0
:_template-generated: 2024-4-17
:_mod-docs-content-type: PROCEDURE

[id="using-bootc-install-to-existing-root_{context}"]
= Using bootc install to-existing-root

[role="_abstract"]
The `bootc install to-existing-root` is a variant of `install to-filesystem`. You can use it to convert an existing system into the target container image. 

[WARNING]
====
This conversion eliminates the `/boot` and `/boot/efi` partitions and can delete the existing Linux installation. The conversion process reuses the filesystem, and even though the user data is preserved, the system no longer boots in package mode. 
====

.Prerequisites

* You must have root permissions to complete the procedure. 
* You must match the host environment and the target container version, for example, if your host is a RHEL 9 host, then you must have a RHEL 9 container. Installing a RHEL container on a Fedora host by using `btrfs` as the RHEL kernel will not support that filesystem.


.Procedure

* Run the following command to convert an existing system into the target container image. Pass the target `rootfs` by using the `-v /:/target` option.
+
[literal,subs="+quotes"]
----
# *podman run --rm --privileged -v /dev:/dev -v /var/lib/containers:/var/lib/containers -v /:/target \*
            *--pid=host --security-opt label=type:unconfined_t \*
            *_<image>_ \*
            *bootc install to-existing-root*
----
+
This command deletes the data in `/boot`, but everything else in the existing operating system is not automatically deleted. This can be useful because the new image can automatically import data from the previous host system.
Consequently, container images, database, the user home directory data, configuration files in `/etc` are all available after the subsequent reboot in `/sysroot`.
+
You can also use the `--root-ssh-authorized-keys` flag to inherit the root user SSH keys, by adding `--root-ssh-authorized-keys /target/root/.ssh/authorized_keys`. For example:
+
[literal,subs="+quotes"]
----
# *podman run --rm --privileged -v /dev:/dev -v /var/lib/containers:/var/lib/containers -v /:/target \*
            *--pid=host --security-opt label=type:unconfined_t \*
            *_<image>_ \*
            *bootc install to-existing-root --root-ssh-authorized-keys /target/root/.ssh/authorized_keys*
----

