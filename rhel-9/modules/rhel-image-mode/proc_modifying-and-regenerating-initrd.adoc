:_newdoc-version: 2.18.2
:_template-generated: 2024-06-20
:_mod-docs-content-type: PROCEDURE

[id="modifying-and-regenerating-initrd_{context}"]
= Modifying and regenerating initrd

The default container image includes a pre-generated initial RAM disk (initrd) in `/usr/lib/modules/$kver/initramfs.img`. To regenerate the `initrd`, for example, to add a dracut module, follow the steps:

.Procedure

. Write your drop-in configuration file. For example:
+
[literal,subs="+quotes"]
----
dracutmodules = "_module_"
----

. Place your drop-in configuration file in the location that `dracut` normally uses: `/usr`.  For example:
+
[literal,subs="+quotes"]
----
/usr/lib/dracut/dracut.conf.d/50-custom-added-modules.conf
----

. Regenerate the `initrd` as part of the container build. You must explicitly pass the kernel version to target to `dracut`, because it tries to pull the running kernel version, which can cause an error. The following is an example:
+
[literal,subs="+quotes"]
----
FROM <baseimage>
COPY 50-custom-added-modules.conf /usr/lib/dracut/dracut.conf.d
RUN set -x; kver=$(cd /usr/lib/modules && echo *); dracut -vf /usr/lib/modules/$kver/initramfs.img $kver
----

