:_newdoc-version: 2.18.3
:_template-generated: 2025-01-10

:_mod-docs-content-type: CONCEPT

[id="describing-disk-customizations-in-a-blueprint_{context}"]
= Describing disk customizations in a blueprint

When using the Disk customizations, you can describe the partition table almost entirely by using a blueprint. The customizations have the following structure:

* *Partitions*: The top level is a list of partitions.

** `type`: Each partition has a type, which can be either `plain` or `lvm`. If the type is not set, it defaults to `plain`. The remaining  required and optional properties of the partition depend on the type.
*** `plain`: A plain partition is a partition with a filesystem. It supports the following properties:
**** `fs_type`: The filesystem type, which should be one of `xfs`, `ext4`, `vfat`, or `swap`. Setting it to `swap` will create a swap partition. The mountpoint for a swap partition must be empty.
**** `minsize`: The minimum size of the partition, as an integer (in bytes) or a string with a data unit (for example 3 GiB). The final size of the partition in the image might be larger for specific mountpoints. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/using_image_mode_for_rhel_to_build_deploy_and_manage_operating_systems/index#understanding-partitions_customizing-disk-images-of-rhel-image-mode-with-advanced-partitioning[Understanding partitions] section.
**** `mountpoint` The mountpoint for the filesystem. For swap partitions, this must be empty.
**** `label`: The label for the filesystem (optional).

*** `lvm`: An lvm partition is a partition with an LVM volume group. Only single Persistent Volumes volume groups are supported. It supports the following properties:
**** `name`: The name of the volume group (optional; if unset, a name will be generated automatically).
**** `minsize`: The minimum size of the volume group, as an integer (in bytes) or a string with a data unit (for example 3 GiB). The final size of the partition and volume group in the image might be larger if the value is smaller than the sum of logical volumes it contains.
**** `logical_volumes`: One or more logical volumes for the volume group. Each volume group supports the following properties:
***** `name`: The name of the logical volume (optional; if unset, a name will be generated automatically based on the mountpoint).
***** `minsize`: The minimum size of the logical volume, as an integer (in bytes) or a string with a data unit (for example 3 GiB). The final size of the logical volume in the image might be larger for specific mountpoints. See the General principles chapter for more details).
***** `label`: The label for the filesystem (optional).
***** `fs_type`: The filesystem type, which should be one of `xfs`, `ext4`, `vfat`, or `swap`. Setting it to swap will create a swap logical volume. The mountpoint for a swap logical volume must be empty.
***** `mountpoint`: The mountpoint for the logical volume's filesystem. For swap logical volumes, this must be empty.

* *Order*​:

The order of each element in a list is respected when creating the partition table. The partitions are created in the order they are defined, regardless of their type.

* *Incomplete partition tables​:*

Incomplete partitioning descriptions are valid. Partitions, LVM logical volumes, are added automatically to create a valid partition table. The following rules are applied:

* A root filesystem is added if one is not defined. This is identified by the mountpoint `/`. If an LVM volume group is defined, the root filesystem is created as a logical volume, otherwise it will be created as a plain partition with a filesystem. The type of the filesystem, for plain and LVM, depends on the distribution (xfs for RHEL and CentOS, ext4 for Fedora). See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/using_image_mode_for_rhel_to_build_deploy_and_manage_operating_systems/index#understanding-partitions_customizing-disk-images-of-rhel-image-mode-with-advanced-partitioning[Understanding partitions] section for information about the sizing and order of the root partition.

* A boot partition is created if needed and if one is not defined. This is identified by the mountpoint `/boot`. A boot partition is needed when the root partition (mountpoint `/`) is on an LVM logical volume. It is created as the first partition after the ESP (see next item).

* An EFI system partition (ESP) is created if needed. This is identified by the mountpoint `/boot/efi`. An ESP is needed when the image is configured to boot with UEFI. This is defined by the image definition and depends on the image type, the distribution, and the architecture. The type of the filesystem is always `vfat`. By default, the ESP is 200 MiB and is the first partition after the BIOS boot (see next item).

* A 1 MiB unformatted BIOS boot partition is created at the start of the partition table if the image is configured to boot with BIOS. This is defined by the image definition and depends on the image type, the distribution, and the architecture.
Both a BIOS boot partition and an ESP are created for images that are configured for hybrid boot.

* *Combining partition types​:*

You can define multiple partitions. The following combination of partition types are valid:

* `plain` and `lvm`: Plain partitions can be created alongside an LVM volume group. However, only one LVM volume group can be defined.


* *Examples:* Blueprint to define two partitions

The following blueprint defines two partitions. The first is a 50 GiB partition with an `ext4` filesystem that will be mounted at `/data`. The second is an LVM volume group with three logical volumes, one for root `/`, one for home directories `/home`, and a `swap` space in that order. The LVM volume group will have 15 GiB of non-allocated space.

[literal,subs="+quotes"]
----
[[customizations.disk.partitions]]
type = "plain"
label = "data"
mountpoint = "/data"
fs_type = "ext4"
minsize = "50 GiB"

[[customizations.disk.partitions]]
type = "lvm"
name = "mainvg"
minsize = "20 GiB"

[[customizations.disk.partitions.logical_volumes]]
name = "rootlv"
mountpoint = "/"
label = "root"
fs_type = "ext4"
minsize = "2 GiB"

[[customizations.disk.partitions.logical_volumes]]
name = "homelv"
mountpoint = "/home"
label = "home"
fs_type = "ext4"
minsize = "2 GiB"

[[customizations.disk.partitions.logical_volumes]]
name = "swaplv"
fs_type = "swap"
minsize = "1 GiB"
----
