
:_mod-docs-content-type: REFERENCE

[id="ref_commands-for-installing-content-in-rhel-9_{context}"]
= Commands for installing content in {ProductShortName} {ProductNumber}

[role="_abstract"]
The following are the commonly used *{PackageManagerName}* commands for installing content in {ProductName} {ProductNumber}:


[options="header"]
|====
|Command|Description
|[command]`{PackageManagerCommand} install _package_name_`|Install a package.

If the package is provided by a module stream, [command]`{PackageManagerCommand}` resolves the required module stream and enables it automatically while installing this package. This also happens recursively for all package dependencies. If more module streams satisfy the requirement, the default ones are used.
|[command]`{PackageManagerCommand} install _package_name_1_ _package_name_2_`|Install multiple packages and their dependencies simultaneously.
|[command]`{PackageManagerCommand} install _package_name.arch_`|Specify the architecture of the package by appending it to the package name when installing packages on a _multilib_ system (AMD64, Intel 64 machine).
|[command]`{PackageManagerCommand} install _/usr/sbin/binary_file_`| Install a binary by using the path to the binary as an argument.
|[command]`{PackageManagerCommand} install _/path/_`|Install a previously downloaded package from a local directory.
|[command]`{PackageManagerCommand} install _package_url_`|Install a remote package by using a package URL.
|[command]`{PackageManagerCommand} module enable _module_name:stream_`|Enable a module by using a specific stream.

Note that running this command does not install any RPM packages.
|[command]`{PackageManagerCommand} module install _module_name:stream_`

[command]`{PackageManagerCommand} install @_module_name:stream_`|Install a default profile from a specific module stream.

Note that running this command also enables the specified stream.
|[command]`{PackageManagerCommand} module install _module_name:stream/profile_`

[command]`{PackageManagerCommand} install @_module_name:stream/profile_`|Install a selected profile by using a specific stream.
|[command]`{PackageManagerCommand} group install _group_name_`|Install a package group by a group name.
|[command]`{PackageManagerCommand} group install _group_ID_`|Install a package group by the groupID.
|====
