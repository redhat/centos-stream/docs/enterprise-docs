:_newdoc-version: 2.18.3
:_template-generated: 2024-09-24
:_mod-docs-content-type: PROCEDURE

[id="excluding-packages-from-dnf-operations_{context}"]
= Excluding packages from DNF operations

You can configure *DNF* to exclude packages from any *DNF* operation by using the `excludepkgs` option. You can define `excludepkgs` in the `[main]` or the repository section of the `/etc/dnf/dnf.conf` file.

NOTE: You can temporarily disable excluding the configured packages from an operation by using the `--disableexcludes` option.


.Procedure

* Exclude packages from the *DNF* operation by adding the following line to the `/etc/dnf/dnf.conf` file:
+
[literal,subs="+quotes,attributes"]
....
excludepkgs=_<package_name_1>_,_<package_name_2>_ ...
....
+
Alternatively, use global expressions instead of package names to define packages you want to exclude. For more information, see xref:proc_specifying-global-expressions-in-yum-input_assembly_searching-for-rhel-9-content[Specifying global expressions in DNF input].


[role="_additional-resources"]
.Additional resources

* `dnf.conf(5)` man page on your system
* xref:proc_specifying-global-expressions-in-yum-input_assembly_searching-for-rhel-9-content[Specifying global expressions in DNF input]
