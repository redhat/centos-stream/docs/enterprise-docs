
:_mod-docs-content-type: CONCEPT


[id="con_other-rpm-features_{context}"]
= Other features

Other new features related to RPM packaging in {RHEL} 9 include:

* Fast macro-based dependency generators
* Powerful macro and `%if` expressions, including ternary operator and native version comparison
* Meta (unordered) dependencies
* Caret version operator (`^`), which can be used to express a version that is higher than the base version. This operator complements the tilde (`~`) operator, which has the opposite semantics.
* `%elif`, `%elifos` and `%elifarch` statements
