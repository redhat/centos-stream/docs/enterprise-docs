
:_mod-docs-content-type: CONCEPT


[id="con_dynamic-build-dependencies_{context}"]
= Dynamic build dependencies

{RHEL} 9 introduces the `%generate_buildrequires` section that enables generating dynamic build dependencies.

Additional build dependencies can now be generated programmatically at RPM build time, using the newly available `%generate_buildrequires` script. This is useful when packaging software written in a language in which a specialized utility is commonly used to determine run-time or build-time dependencies, such as Rust, Golang, Node.js, Ruby, Python, or Haskell.

You can use the `%generate_buildrequires` script to dynamically determine which `BuildRequires` directives are added to a SPEC file at build-time. If present, `%generate_buildrequires` is executed after the `%prep` section and can access the unpacked and patched source files. The script must print the found build dependencies to standard output using the same syntax as a regular `BuildRequires` directive.

The `rpmbuild` utility then checks if the dependencies are met before continuing the build.

If some dependencies are missing, a package with the `.buildreqs.nosrc.rpm` suffix is created, which contains the found `BuildRequires` and no source files. You can use this package to install the missing build dependencies with the `dnf builddep` command before restarting the build.

For more information, see the `DYNAMIC BUILD DEPENDENCIES` section in the `rpmbuild(8)` man page on your system.

[role="_additional-resources"]
.Additional resources
* `rpmbuild(8)` man page on your system
* `yum-builddep(1)` man page on your system
