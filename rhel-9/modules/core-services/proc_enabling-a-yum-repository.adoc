
:_mod-docs-content-type: PROCEDURE

[id="proc_enabling-a-yum-repository_{context}"]
= Enabling a DNF repository

[role="_abstract"]
You can enable a *{PackageManagerName}* repository added to your system by using the [command]`{PackageManagerCommand} config-manager` command.

.Procedure

* Enable a repository:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} config-manager --enable _<repository_id>_*
....

