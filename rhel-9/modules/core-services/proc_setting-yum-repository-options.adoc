
:_mod-docs-content-type: PROCEDURE

[id="proc_setting-yum-repository-options_{context}"]
= DNF repository options

[role="_abstract"]
The `/etc/dnf/dnf.conf` configuration file contains repository sections with a unique repository ID in brackets (`[]`). You can use such sections to define individual *{PackageManagerName}* repositories.

IMPORTANT: Repository IDs in `[]` must be unique.

For a complete list of available repository ID options, see the `[_<repository-ID>_] OPTIONS` section of the `dnf.conf(5)` man page on your system.
