
:_mod-docs-content-type: REFERENCE

[id="ref_additional-resources-about-rpm-packaging_{context}"]

= Additional resources

References to various topics related to RPMs, RPM packaging, and RPM building follows.

* link:https://rpm-packaging-guide.github.io/#mock[Mock]
* link:http://rpm.org/documentation[RPM Documentation]
* link:https://rpm.org/wiki/Releases/4.15.0[RPM 4.15.0 Release Notes]
* link:https://rpm.org/wiki/Releases/4.16.0[RPM 4.16.0 Release Notes]
* link:https://docs.fedoraproject.org/en-US/packaging-guidelines/[Fedora Packaging Guidelines]
