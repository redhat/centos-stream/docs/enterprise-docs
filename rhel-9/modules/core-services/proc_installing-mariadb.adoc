:_mod-docs-content-type: PROCEDURE
[id="installing-mariadb_{context}"]
= Installing MariaDB

[role="_abstract"]
RHEL 9.0 provides *MariaDB 10.5* as the initial version of this Application Stream, which can be installed easily as an RPM package. Additional *MariaDB* versions are provided as modules with a shorter life cycle in minor releases of RHEL 9.

RHEL 9.4 introduced *MariaDB 10.11* as the `mariadb:10.11` module stream.


[NOTE]
====
By design, it is impossible to install more than one version (stream) of the same module in parallel. Therefore, you must choose only one of the available streams from the `mariadb` module. You can use different versions of the *MariaDB* database server in containers, see xref:running-multiple-mariadb-versions-in-containers_{context}[Running multiple MariaDB versions in containers].

The *MariaDB* and *MySQL* database servers cannot be installed in parallel in RHEL 9 due to conflicting RPM packages. You can use the *MariaDB* and *MySQL* database servers in parallel in containers, see xref:running-multiple-mysql-mariadb-versions-in-containers_assembly_using-mysql[Running multiple MySQL and MariaDB versions in containers].
====

To install *MariaDB*, use the following procedure.

.Procedure

. Install *MariaDB* server packages:
+
.. For *MariaDB 10.5* from the RPM package:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install mariadb-server*
----
+
.. For *MariaDB 10.11* by selecting stream (version) `11` from the `mariadb` module and specifying the server profile, for example:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} module install mariadb:10.11/server*
----


. Start the `mariadb` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl start mariadb.service*
----

. Enable the `mariadb` service to start at boot:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl enable mariadb.service*
----

// The mariadb-secure-installation step removed because it is useless in MariaDB >=10.5. See BZ#2159717


////
TODO use when multiple module streams are available
[IMPORTANT]
====
If you want to upgrade from an earlier `mariadb` stream within RHEL 9, follow both procedures described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/managing_software_with_the_dnf_tool/index#proc_switching-to-a-later-stream_assembly_managing-versions-of-application-stream-content[Switching to a later stream] and in TBD migration chapter.
====
////

