:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-tls-on-a-mysql-server_{context}"]
= Configuring TLS on a MySQL server

[role="_abstract"]
To improve security, enable TLS support on the *MySQL* server. As a result, clients can transmit data with the server using TLS encryption.


.Prerequisites

* You installed the *MySQL* server.

* The [systemitem]`mysqld` service is running.

* The following files in Privacy Enhanced Mail (PEM) format exist on the server and are readable by the [systemitem]`mysql` user:
+
** The private key of the server: [filename]`/etc/pki/tls/private/server.example.com.key.pem`
** The server certificate: [filename]`/etc/pki/tls/certs/server.example.com.crt.pem`
** The Certificate Authority (CA) certificate [filename]`/etc/pki/tls/certs/ca.crt.pem`

* The subject distinguished name (DN) or the subject alternative name (SAN) field in the server certificate matches the server's host name.


.Procedure

. Create the [filename]`/etc/my.cnf.d/mysql-server-tls.cnf` file:

.. Add the following content to configure the paths to the private key, server and CA certificate:
+
[literal,subs="+quotes,attributes"]
----
[mysqld]
ssl_key = __/etc/pki/tls/private/server.example.com.key.pem__
ssl_cert = __/etc/pki/tls/certs/server.example.com.crt.pem__
ssl_ca = __/etc/pki/tls/certs/ca.crt.pem__
----

.. If you have a Certificate Revocation List (CRL), configure the *MySQL* server to use it:
+
[literal,subs="+quotes,attributes"]
----
*ssl_crl = __/etc/pki/tls/certs/example.crl.pem__*
----

.. Optional: Reject connection attempts without encryption. To enable this feature, append:
+
[literal,subs="+quotes,attributes"]
----
*require_secure_transport = on*
----

.. Optional: Set the TLS versions the server should support. For example, to support TLS 1.2 and TLS 1.3, append:
+
[literal,subs="+quotes,attributes"]
----
*tls_version = __TLSv1.2__,__TLSv1.3__*
----
+
By default, the server supports TLS 1.1, TLS 1.2, and TLS 1.3.

. Restart the [systemitem]`mysqld` service:
+
[literal,subs="+quotes,attributes"]
----
# **systemctl restart mysqld.service**
----



.Verification

To simplify troubleshooting, perform the following steps on the *MySQL* server before you configure the local client to use TLS encryption: 

. Verify that *MySQL* now has TLS encryption enabled: 
+
[literal,subs="+quotes,attributes"]
....
# **mysql -u root -p -h <MySQL_server_hostname> -e "SHOW session status LIKE 'Ssl_cipher';"**
+---------------+------------------------+
| Variable_name | Value                  |
+---------------+------------------------+
| Ssl_cipher    | TLS_AES_256_GCM_SHA384 |
+---------------+------------------------+
....

. If you configured the *MySQL* server to only support specific TLS versions, display the `tls_version` variable: 
+
[literal,subs="+quotes,attributes"]
....
# **mysql -u root -p -e "SHOW GLOBAL VARIABLES LIKE 'tls_version';"**
+---------------+-----------------+
| Variable_name | Value           |
+---------------+-----------------+
| tls_version   | TLSv1.2,TLSv1.3 |
+---------------+-----------------+
....

. Verify that the server uses the correct CA certificate, server certificate, and private key files:
+
[literal,subs="+quotes,attributes"]
....
# **mysql -u root -e "SHOW GLOBAL VARIABLES WHERE Variable_name REGEXP '{caret}ssl_ca|{caret}ssl_cert|{caret}ssl_key';"**
+-----------------+-------------------------------------------------+
| Variable_name   | Value                                           |
+-----------------+-------------------------------------------------+
| ssl_ca          | /etc/pki/tls/certs/ca.crt.pem                   |
| ssl_capath      |                                                 |
| ssl_cert        | /etc/pki/tls/certs/server.example.com.crt.pem   |
| ssl_key         | /etc/pki/tls/private/server.example.com.key.pem |
+-----------------+-------------------------------------------------+
....




[role="_additional-resources"]
.Additional resources
ifdef::databaseservers-title[]
* xref:proc_placing-the-ca-certificate-server-certificate-and-private-key-on-the-mysql-server_assembly_setting-up-tls-encryption-on-a-mysql-server[Placing the CA certificate, server certificate, and private key on the MySQL server]
endif::[]

ifndef::databaseservers-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/using-databases#proc_placing-the-ca-certificate-server-certificate-and-private-key-on-the-mysql-server_assembly_setting-up-tls-encryption-on-a-mysql-server[Placing the CA certificate, server certificate, and private key on the MySQL server]
endif::[]
