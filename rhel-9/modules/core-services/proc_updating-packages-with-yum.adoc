
:_mod-docs-content-type: PROCEDURE

[id="proc_updating-packages-with-yum_{context}"]
= Updating packages

[role="_abstract"]
You can use *{PackageManagerName}* to update a single package, a package group, or all packages and their dependencies at once.

IMPORTANT: When applying updates to the kernel, [command]`dnf` always installs a new kernel regardless of whether you are using the [command]`dnf upgrade` or [command]`dnf install` command. Note that this only applies to packages identified by using the `installonlypkgs` *DNF* configuration option. Such packages include, for example, the `kernel`, `kernel-core`, and `kernel-modules` packages.

// Comment out until applicable again IMPORTANT: Do not use the [command]`dnf upgrade` command to update module streams to a later version. To update module streams to a later version, follow the procedure described in xref:proc_switching-to-a-later-stream_assembly_managing-versions-of-application-stream-content[Switching to a later stream].

.Procedure

*  Depending on your scenario, use one of the following options to apply updates: 

** To update all packages and their dependencies, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} upgrade*
....

** To update a single package, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} upgrade _<package_name>_*
....

** To update packages only from a specific package group, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} group upgrade _<group_name>_*
....

// Comment out until applicable again IMPORTANT: Do not use the [command]`dnf upgrade` command to update module streams to a later version. To update module streams to a later version, follow the procedure described in xref:proc_switching-to-a-later-stream_assembly_managing-versions-of-application-stream-content[Switching to a later stream].

include::../../rhel-8/common-content/snip_grub-updated.adoc[leveloffset=+1]
