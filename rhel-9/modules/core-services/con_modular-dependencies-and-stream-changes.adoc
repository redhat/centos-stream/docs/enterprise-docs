
:_mod-docs-content-type: CONCEPT


[id="con_modular-dependencies-and-stream-changes_{context}"]
= Modular dependencies and stream changes

[role="_abstract"]
Traditionally, packages providing content depend on further packages, and usually specify the desired dependency versions. For packages contained in modules, this mechanism applies as well, but the grouping of packages and their particular versions into modules and streams provides further constraints. Additionally, module streams can declare dependencies on streams of other modules, independent of the packages contained and provided by them.

After any operations with packages or modules, the whole dependency tree of all underlying installed packages must satisfy all the conditions that the packages declare. Additionally, all module stream dependencies must be satisfied. For example, disabling a module stream can require disabling other module streams. No packages will be removed automatically.

Note that the following actions can cause subsequent automatic operations:

* Enabling a module stream can result in enabling further module streams.
* Installing a module stream profile or installing packages from a stream can result in enabling further module streams and installing further packages.
* Removing a package can result in removing further packages. If these packages were provided by modules, the module streams remain enabled in preparation for further installation, even if no packages from these streams are installed any more. This mirrors the behavior of an unused [application]*DNF* repository.

////
Commented out until again applicable
It is not possible to enable a module stream when another stream of the same module is already enabled. To switch streams, follow the procedure in
xref:proc_switching-to-a-later-stream_{context}[Switching to a later stream].

Alternatively, reset the module, and then enable the new stream. Removing all packages installed from a stream before switching to a different stream prevents the system from reaching states where packages could be installed with no repository or stream providing them.

Resetting module does not automatically change any installed packages. Removing the packages provided by the previous stream and any packages that depend on them is an explicit manual operation.

////
