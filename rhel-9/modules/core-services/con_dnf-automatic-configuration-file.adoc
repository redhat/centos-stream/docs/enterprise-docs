
:_mod-docs-content-type: CONCEPT


[id="con_dnf-automatic-configuration-file_{context}"]
= DNF Automatic configuration file

[role="_abstract"]
By default, [application]*DNF Automatic* uses `/etc/dnf/automatic.conf` as its configuration file to define its behavior.

The configuration file is separated into the following topical sections:

* `[commands]`
+
Sets the mode of operation of [application]*DNF Automatic*.
+
WARNING: Settings of the operation mode from the `[commands]` section are overridden by settings used by a systemd timer unit for all timer units except `dnf-automatic.timer`.


* `[emitters]`
+
Defines how the results of [application]*DNF Automatic* are reported.

* `[command]`
+
Defines the command emitter configuration.

* `[command_email]`
+
Provides the email emitter configuration for an external command used to send email.

* `[email]`
+
Provides the email emitter configuration.

* `[base]`
+
Overrides settings from the main configuration file of [application]*DNF*.

With the default settings of the `/etc/dnf/automatic.conf` file, [application]*DNF Automatic* checks for available updates, downloads them, and reports the results to standard output.


[role="_additional-resources"]
.Additional resources
* `dnf-automatic(8)` man page on your system
* xref:ref_overview-of-the-systemd-timer-units-included-in-the-dnf-automatic-package_{context}[Overview of the systemd timer units included in the dnf-automatic package]

