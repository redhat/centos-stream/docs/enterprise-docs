:_mod-docs-content-type: CONCEPT

[id="con_modules_{context}"]
= Modules

[role="_abstract"]
A module is a set of RPM packages that represent a component. A typical module contains the following package types:

* Packages with an application
* Packages with the application-specific dependency libraries
* Packages with documentation for the application
* Packages with helper utilities

