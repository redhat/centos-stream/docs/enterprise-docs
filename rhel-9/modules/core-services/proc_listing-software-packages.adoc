:_mod-docs-content-type: PROCEDURE

[id="proc_listing-software-packages_{context}"]
= Listing software packages

[role="_abstract"]
You can use *{PackageManagerName}* to display a list of packages and their versions that are available in the repositories. If required, you can filter this list and, for example, only list packages for which updates are available.

 
.Procedure

* List the latest versions of all available packages, including architectures, version numbers, and the repository they where installed from:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} list --all*
...
zlib.x86_64         1.2.11-39.el9   @rhel-9-for-x86_64-baseos-rpms
zlib.i686           1.2.11-39.el9   rhel-9-for-x86_64-baseos-rpms
zlib-devel.i686     2.11-39.el9     rhel-9-for-x86_64-appstream-rpms
zlib-devel.x86_64   1.2.11-39.el9   rhel-9-for-x86_64-appstream-rpms
...
....
+
The `@` sign in front of a repository indicates that the package in this line is currently installed.
+
Alternatively, to display all available packages, including version numbers and architectures, enter:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} repoquery*
...
zlib-0:1.2.11-35.el9_1.i686
zlib-0:1.2.11-35.el9_1.x86_64
zlib-0:1.2.11-39.el9.i686
zlib-0:1.2.11-39.el9.x86_64
zlib-devel-0:1.2.11-39.el9.i686
zlib-devel-0:1.2.11-39.el9.x86_64
...
....
+
Optionally, you can filter the output by using other options instead of `--all`, for example:
+
** Use `--installed` to list only installed packages.
** Use `--available` to list all available packages.
** Use `--upgrades` to list packages for which newer versions are available.

+
[NOTE]
====
You can filter the results by appending global expressions as arguments. For more details, see xref:proc_specifying-global-expressions-in-yum-input_{context}[Specifying global expressions in DNF input].
====

