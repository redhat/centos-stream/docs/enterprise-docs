:_mod-docs-content-type: PROCEDURE
[id="configuring-mariadb-galera-cluster_{context}"]
= Deploying MariaDB Galera Cluster

[role="_abstract"]
You can deploy the *MariaDB Galera Cluster* packages and update the configuration. To form a new cluster, you must bootstrap the first node of the cluster.

.Prerequisites

* All of the nodes in the cluster have xref:assembly_setting-up-tls-encryption-on-a-mariadb-server_using-mariadb[TLS set up].

* All certificates on all nodes must have the `Extended Key Usage` field set to:
+
[subs="+quotes"]
----
TLS Web Server Authentication, TLS Web Client Authentication
----




.Procedure

. Install the *MariaDB Galera Cluster* packages:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install mariadb-server-galera*
----
+
As a result, the following packages are installed together with their dependencies:
+
** [package]`mariadb-server-galera`
** [package]`mariadb-server`
** [package]`galera`
+
For more information about these packages reqired to build *MariaDB Galera Cluster*, see xref:components-to-build-mariadb-galera-cluster_replicating-mariadb-with-galera[Components to build MariaDB Cluster].

. Update the *MariaDB* server replication configuration before the system is added to a cluster for the first time. The default configuration is distributed in the [filename]`/etc/my.cnf.d/galera.cnf` file. Before deploying *MariaDB Galera Cluster*, set the [option]`wsrep_cluster_address` option in the [filename]`/etc/my.cnf.d/galera.cnf` file on all nodes to start with the following string:
+
[literal,subs="+quotes,attributes"]
----
gcomm://
----
+
** For the initial node, it is possible to set [option]`wsrep_cluster_address` as an empty list:
+
[literal,subs="+quotes,attributes"]
----
wsrep_cluster_address="gcomm://"
----
+
** For all other nodes, set [option]`wsrep_cluster_address` to include an address to any node which is already a part of the running cluster. For example:
+
[literal,subs="+quotes,attributes"]
----
wsrep_cluster_address="gcomm://10.0.0.10"
----
+
For more information about how to set Galera Cluster address, see link:https://mariadb.com/kb/en/library/galera-cluster-address/[Galera Cluster Address].

. Enable the `wsrep` API on every node by setting the `wsrep_on=1` option in the `/etc/my.cnf.d/galera.cnf` configuration file.

. Add the `wsrep_provider_options` variable to the Galera configuration file with the TLS keys and certificates. For example:
+
[subs="+quotes"]
----
wsrep_provider_options="socket.ssl_cert=/etc/pki/tls/certs/source.crt;socket.ssl_key=/etc/pki/tls/private/source.key;socket.ssl_ca=/etc/pki/tls/certs/ca.crt” 
----

. Bootstrap a first node of a new cluster by running the following wrapper on that node:
+
[literal,subs="+quotes,attributes"]
----
# *galera_new_cluster*
----
+
This wrapper ensures that the *MariaDB* server daemon (`mariadbd`) runs with the [option]`--wsrep-new-cluster` option. This option provides the information that there is no existing cluster to connect to. Therefore, the node creates a new UUID to identify the new cluster.
+
[NOTE]
====
The `mariadb` service supports a systemd method for interacting with multiple *MariaDB* server processes. Therefore, in cases with multiple running *MariaDB* servers, you can bootstrap a specific instance by specifying the instance name as a suffix:

[literal,subs="+quotes,attributes"]
----
# *galera_new_cluster _mariadb@node1_*
----
====

. Connect other nodes to the cluster by running the following command on each of the nodes:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl start mariadb.service*
----
+
As a result, the node connects to the cluster, and synchronizes itself with the state of the cluster.

[role="_additional-resources"]
.Additional resources
* link:https://mariadb.com/kb/en/library/getting-started-with-mariadb-galera-cluster/[Getting started with MariaDB Galera Cluster]

// If you use the systemd service that supports the systemd service's method for interacting with multiple *MariaDB* server processes,  you can bootstrap a specific instance by specifying the instance name as a suffix.
