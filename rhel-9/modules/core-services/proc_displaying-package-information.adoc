:_mod-docs-content-type: PROCEDURE

[id="proc_displaying-package-information_{context}"]
= Displaying package information

[role="_abstract"]
You can query *{PackageManagerName}* repositories to display further details about a package, such as the following:

* Version
* Release
* Architecture
* Package size
* Description


.Procedure

* Display information about one or more available packages:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} info __<package_name>__*
....
+
This command displays the information for the currently installed package and, if available, its newer versions that are in the repository. Alternatively, use the following command to display the information for all packages with the specified name in the repository:
+
[literal,subs="+quotes,attributes"]
....
$ *{PackageManagerCommand} repoquery --info __<package_name>__*
....
+
[NOTE]
====
You can filter the results by appending global expressions as arguments. For details, see xref:proc_specifying-global-expressions-in-yum-input_{context}[Specifying global expressions in DNF input].
====

