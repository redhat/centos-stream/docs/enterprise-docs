:_mod-docs-content-type: REFERENCE

[id="an-example-spec-file-for-the-pello-program-written-in-python_{context}"]
.An example spec file for the pello program written in Python

// .SPEC file for the [package]`python3-pello` package
// =====================================================================
[source,specfile]
----
%global python3_pkgversion 3.11                                       <1>

Name:           python-pello                                          <2>
Version:        1.0.2
Release:        1%{?dist}
Summary:        Example Python library

License:        MIT
URL:            https://github.com/fedora-python/Pello
Source:         %{url}/archive/v%{version}/Pello-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python%{python3_pkgversion}-devel                     <3>

# Build dependencies needed to be specified manually
BuildRequires:  python%{python3_pkgversion}-setuptools

# Test dependencies needed to be specified manually
# Also runtime dependencies need to be BuildRequired manually to run tests during build
BuildRequires:  python%{python3_pkgversion}-pytest >= 3


%global _description %{expand:
Pello is an example package with an executable that prints Hello World! on the command line.}

%description %_description

%package -n python%{python3_pkgversion}-pello                         <4>
Summary:        %{summary}

%description -n python%{python3_pkgversion}-pello %_description


%prep
%autosetup -p1 -n Pello-%{version}


%build
# The macro only supported projects with setup.py
%py3_build                                                            <5>


%install
# The macro only supported projects with setup.py
%py3_install


%check                                                                <6>
%{pytest}


# Note that there is no %%files section for the unversioned python module
%files -n python%{python3_pkgversion}-pello
%doc README.md
%license LICENSE.txt
%{_bindir}/pello_greeting

# The library files needed to be listed manually
%{python3_sitelib}/pello/

# The metadata files needed to be listed manually
%{python3_sitelib}/Pello-*.egg-info/
----
// =====================================================================

<1> By defining the `python3_pkgversion` macro, you set which Python version this package will be built for. To build for the default Python version 3.9, either set the macro to its default value `3` or remove the line entirely.
<2> When packaging a Python project into RPM, always add the `python-` prefix to the original name of the project. The original name here is `pello` and, therefore, the name of the Source RPM (SRPM) is `python-pello`.
<3> The `BuildRequires` directive specifies what packages are required to build and test this package. In `BuildRequires`, always include items providing tools necessary for building Python packages: `python3-devel` (or `python3.11-devel` or `python3.12-devel`) and the relevant projects needed by the specific software that you package, for example, `python3-setuptools` (or `python3.11-setuptools` or `python3.12-setuptools`) or the runtime and testing dependencies needed to run the tests in the `%check` section.
<4> When choosing a name for the binary RPM (the package that users will be able to install), add a versioned Python prefix. Use the `python3-` prefix for the default Python 3.9, the `python3.11-` prefix for Python 3.11, or the `python3.12-` prefix for Python 3.12. You can use the `%{python3_pkgversion}` macro, which evaluates to `3` for the default Python version 3.9 unless you set it to an explicit version, for example, `3.11` (see footnote 1).
<5> The `%py3_build` and `%py3_install` macros run the `setup.py build` and `setup.py install` commands, respectively, with additional arguments to specify installation locations, the interpreter to use, and other details.
<6> The `%check` section should run the tests of the packaged project. The exact command depends on the project itself, but it is possible to use the `%pytest` macro to run the `pytest` command in an RPM-friendly way. 
