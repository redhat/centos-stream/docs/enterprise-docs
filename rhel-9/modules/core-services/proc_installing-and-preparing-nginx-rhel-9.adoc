:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="installing-and-preparing-nginx_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Installing and preparing NGINX
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
In {RHEL} {ProductNumber},different versions to NGINX are provided by Application Streams. By using the default configuration, NGINX runs as a web server on port [systemitem]`80` and provides content from the [filename]`/usr/share/nginx/html/` directory.

.Prerequisites

* RHEL 9 is installed.
* The host is subscribed to the Red Hat Customer Portal.
* The [systemitem]`firewalld` service is enabled and started.


.Procedure

. Install the [package]`nginx` package:
+
* To install NGINX 1.20 as the initial version of this Application Stream from an RPM package:	
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install nginx*
----
+
NOTE: If you have previously enabled an NGINX module stream, this command installs the NGINX version from the enabled stream.
+
* To install an alternate later version of NGINX from a module stream:
+
.. Display the available NGINX module streams:
+ 	
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} module list nginx*
...
rhel-AppStream
Name        Stream        Profiles        Summary
nginx       1.22          common [d]      nginx webserver
...
Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
----
+
.. Enable the selected stream: 	
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} module enable nginx:__stream_version__*
----
+
.. Install the nginx package:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install nginx*
----

. Open the ports on which NGINX should provide its service in the firewall. For example, to open the default ports for HTTP (port 80) and HTTPS (port 443) in [systemitem]`firewalld`, enter:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --permanent --add-port={80/tcp,443/tcp}*
# *firewall-cmd --reload*
----

. Enable the [systemitem]`nginx` service to start automatically when the system boots:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl enable nginx*
----

. Optional: Start the [systemitem]`nginx` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl start nginx*
----
+
If you do not want to use the default configuration, skip this step, and configure NGINX accordingly before you start the service.



.Verification

. Use the [systemitem]`{PackageManagerCommand}` utility to verify that the [package]`nginx` package is installed.
+
* In case of the NGINX 1.20 RPM package:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} list installed nginx*
Installed Packages
nginx.x86_64    1:1.20.1-4.el9       @rhel-AppStream
----
+
* In case of a selected NGINX module stream:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} list installed nginx*
Installed Packages
nginx.x86_64    1:1.22.1-3.module+el9.2.0+17617+2f289c6c    @rhel-AppStream
----

. Ensure that the ports on which NGINX should provide its service are opened in the firewalld:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --list-ports*
80/tcp 443/tcp
----

. Verify that the [systemitem]`nginx` service is enabled:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl is-enabled nginx*
enabled
----


[role="_additional-resources"]
.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/[Securing networks]
