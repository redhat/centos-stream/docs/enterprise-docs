
:_mod-docs-content-type: PROCEDURE

[id="proc_managing-yum-plug-ins_{context}"]
= Managing DNF plug-ins

[role="_abstract"]
Every installed plug-in can have its own configuration file in the `/etc/dnf/plugins/` directory. Name plug-in configuration files in this directory `_<plug-in_name>_.conf`. By default, plug-ins are typically enabled. To disable a plug-in in one of these configuration files, add the following to the file:

[literal,subs="+quotes"]
....
[main]
enabled=False
....

