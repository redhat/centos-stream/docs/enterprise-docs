:_mod-docs-content-type: CONCEPT

[id="con_application-streams_{context}"]
= Application Streams

[role="_abstract"]
{RH} provides multiple versions of user-space components as Application Streams, and they are updated more frequently than the core operating system packages. This provides more flexibility to customize {ProductName} ({ProductShortName}) without impacting the underlying stability of the platform or specific deployments.

Application Streams are available in the following formats:

* RPM format
* Modules, which are an extension to the RPM format
* Software Collections

{ProductShortName} {ProductNumber} improves Application Streams experience by providing initial Application Stream versions as RPMs, which you can install by using the `{PackageManagerCommand} install` command.

Starting with {ProductShortName} 9.1, {RH} provides additional Application Stream versions as modules with a shorter life cycle.

[IMPORTANT]
====
Each Application Stream has its own life cycle, and it can be the same or shorter than the life cycle of {ProductShortName} {ProductNumber}. See link:https://access.redhat.com/support/policy/updates/rhel-app-streams-life-cycle[Red Hat Enterprise Linux Application Streams Life Cycle].

Always determine which version of an Application Stream you want to install, and make sure to review the {ProductShortName} Application Stream life cycle first.
====


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/articles/rhel9-abi-compatibility[Red Hat Enterprise Linux 9: Application Compatibility Guide]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/package_manifest/index[Package manifest]
* link:https://access.redhat.com/support/policy/updates/rhel-app-streams-life-cycle[Red Hat Enterprise Linux Application Streams Life Cycle]

