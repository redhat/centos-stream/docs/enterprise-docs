:_mod-docs-content-type: CONCEPT

[id="basic-concepts-for-ostree-mirrors_{context}"]
= Basic concepts for OSTree

Basic terms that OSTree and `rpm-ostree` use during image updates.

`rpm-ostree`:: The technology on the edge device that handles how the OSTree commits are assembled on the device. It works as a hybrid between an image and a package system. With the `rpm-ostree` technology, you can make atomic upgrades and rollbacks to your system.

OSTree:: OSTree is a technology that enables you to create commits and download bootable file system trees. You can also use it to deploy the trees and manage the boot loader configuration.

Commit:: An OSTree commit contains a full operating system that is not directly bootable. To boot the system, you must deploy it, for example, with a RHEL Installable image.

Reference:: It is also known as `ref`. An OSTree ref is similar to a Git branch and it is a name. The following reference names examples are valid:

* `rhel/9/x86_64/edge`
* `ref-name`
* `app/org.gnome.Calculator/x86_64/stable`
* `ref-name-2`

By default, image builder specifies `rhel/{ProductNumber}/$ARCH/edge` as a path. The "$ARCH" value is determined by the host machine. 

Parent::  The `parent` argument is an OSTree commit that you can provide to build a new commit with image builder. You can use the `parent` argument to specify an existing `ref` that retrieves a parent commit for the new commit that you are building. You must specify the parent commit as a ref value to be resolved and pulled, for example `rhel/{ProductNumber}/x86_64/edge`. You can use the `--parent` commit for the RHEL for Edge Commit (`.tar`) and RHEL for Edge Container (`.tar`) image types. 

Remote:: The http or https endpoint that hosts the OSTree content. This is analogous to the baseurl for a yum repository.

Static delta:: Static deltas are a collection of updates generated between two OSTree commits. This enables the system client to fetch a smaller amount of files, which are larger in size. The static deltas updates are more network efficient because, when updating an ostree-based host, the system client will only fetch the objects from the new OSTree commit which do not exist on the system. Typically, the  new OSTree commit contains many small files, which requires multiple TCP connections.

Summary:: The summary file is a concise way of enumerating refs, checksums, and available static deltas in an OSTree repo. You can check the state of all the refs and static deltas available in an Ostree repo. However, you must generate the summary file every time a new ref, commit, or static-delta is added to the OSTree repo.

