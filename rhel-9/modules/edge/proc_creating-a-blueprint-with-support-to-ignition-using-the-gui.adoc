:_mod-docs-content-type: PROCEDURE

[id="proc_creating-a-blueprint-with-support-to-ignition-using-the-cli_{context}"]
= Creating a blueprint with support to Ignition using the CLI

When building a simplified installer image, you can customize your blueprint by adding the `customizations.ignition` section to it. With that, you can create either a simplified installer image or a raw image that you can use for the bare metal platforms. The `customizations.ignition` customization in the blueprint enables the configuration files to be used in `edge-simplified-installer` ISO and `edge-raw-image` images. 

* For the `edge-simplified-installer` ISO image, you can customize the blueprint to embed an Ignition configuration file that will be included in the ISO image. For example:
+
[subs="quotes,attributes"]
----
[customizations.ignition.embedded]
config = "eyJ --- BASE64 STRING TRIMMED --- 19fQo="
----
+
You must provide a `base64` encoded Ignition configuration file.

* For both the `edge-simplified-installer` ISO image and also the `edge-raw-image`,  you can customize the blueprint, by defining a URL that will be fetched to obtain the Ignition configuration at the first boot. For example:
+
[subs="quotes,attributes"]
----
[customizations.ignition.firstboot]
url = "http://your_server/ignition_configuration.ig"
----
+
You must enter a URL that points to the Ignition configuration that will be fetched during the first boot. 

To customize your blueprint for a Simplified RHEL for Edge image with support to Ignition configuration, follow the steps:

.Prerequisites

* If using the `[customizations.ignition.embedded]` customization, you must create an Ignition configuration file.
* If using the `[customizations.ignition.firstboot]` customization, you must have created a container whose URL  points to the Ignition configuration that will be fetched during the first boot. 
* The blueprint customization `[customizations.ignition.embedded]` section enables `coreos-installer-dracut` to define `-ignition-url`|`-ignition-file` based on the presence of the osbuild's file.

.Procedure

. Create a plain text file in the Tom’s Obvious, Minimal Language (TOML) format, with the following content:
+
[subs="quotes,attributes"]
----
name = "simplified-installer-blueprint"
description = "Blueprint with Ignition for the simplified installer image"
version = "0.0.1"
packages = []
modules = []
groups = []
distro = ""

[customizations.ignition.embedded]
config = "eyJ --- BASE64 STRING TRIMMED --- 19fQo="
----
+
Where:

* The `name` is the name and `description` is the description for your blueprint.
* The `version` is the version number according to the Semantic Versioning scheme.
* The `modules` and `packages` describe the package name and matching version glob to be installed into the image.
For example, the package `name = "tmux"` and the matching version glob is `version = "3.3a"`. Notice that currently there are no differences between packages and modules.
* The `groups` are packages groups to be installed into the image. For example `groups = "anaconda-tools"` group package. If you do not know the modules and groups, leave them empty.
+
WARNING: If you want to create a user with Ignition, you cannot use the FDO customizations to create a user at the same time. You can create users using Ignition and copy configuration files using FDO. But if you are creating users, create them using Ignition or FDO, but not both at the same time.

 
. Push (import) the blueprint to the image builder server:
+
[subs="quotes,attributes"]
----
# composer-cli blueprints push _blueprint-name_.toml
----

. List the existing blueprints to check whether the created blueprint is successfully pushed and exists.
+
[subs="quotes,attributes"]
----
# composer-cli blueprints show _blueprint-name_
----

. Check whether the components and versions listed in the blueprint and their dependencies are valid:
+
[subs="quotes,attributes"]
----
# composer-cli blueprints depsolve _blueprint-name_
----

.Next

* You can use the blueprint you created to build your Simplified Installer image. See xref:creating-a-rhel-for-edge-simplified-installer-image-using-image-builder_building-and-provisioning-simplified-installer-images[Creating a RHEL for Edge Simplified Installer image using image builder CLI].

.Additional resources
* link:https://coreos.github.io/butane/config-r4e-v1_0/[RHEL for Edge Specification v1.0.0]

