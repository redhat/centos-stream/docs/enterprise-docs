:_mod-docs-content-type: PROCEDURE

[id="creating-a-blueprint-for-edge-ami-images_{context}"]
= Creating a blueprint for Edge AMI images

[role="_abstract"]
Create a blueprint for the `edge-ami` image and customize it with the `customizations.ignition` section. With that, you can create your image and when booting the image, inject the user configuration.

.Prerequisites

* You have created an Ignition configuration file. For example:
+
[subs="quotes,attributes"]
----
{
   "ignition":{
      "version":"3.3.0"
   },
   "passwd":{
      "users":[
         {
            "groups":[
               "wheel"
            ],
            "name":"core",
            "passwordHash":"$6$jfuNnO9t1Bv7N"
         }
      ]
   }
}
----
+
For more details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/composing_installing_and_managing_rhel_for_edge_images/index#proc_creating-an-ignition-configuration-file_assembly_using-the-ignition-tool-for-the-rhel-for-edge-simplified-installer-images[Creating an Ignition configuration file].

.Procedure

. Create a blueprint in the Tom’s Obvious, Minimal Language (TOML) format, with the following content:
+
[subs="quotes,attributes"]
----
name = "ami-edge-image"
description = "Blueprint for Edge AMI image"
version = "0.0.1"
packages = ["cloud-init"]
modules = []
groups = []
distro = ""

[[customizations.user]]
name = "admin"
password = "admin"
groups = ["wheel"]

[customizations.ignition.firstboot]
url = http://_&lt;IP_address&gt;_:8080/_config.ig_
----
+
Where:

* The `name` is the name and `description` is the description for your blueprint.
* The `version` is the version number according to the Semantic Versioning scheme.
* The `modules` and `packages` describe the package name and matching version glob to be installed into the image.
For example, the package `name = "open-vm-tools"`. Notice that currently there are no differences between packages and modules.
* The `groups` are packages groups to be installed into the image. For example `groups = "wheel"`. If you do not know the modules and groups, leave them empty.
* The `customizations.user` creates a username and password to log in to the VM.
* The `customizations.ignition.firstboot` contains the URL where the Ignition configuration file is being served.
+
NOTE: By default, the `open-vm-tools` package is not included in the `edge-vsphere` image. If you need this package, you must include it in the blueprint customization.

. Import the blueprint to the image builder server:
+
[subs="quotes,attributes"]
----
# composer-cli blueprints push _&lt;blueprint-name&gt;_.toml
----

. List the existing blueprints to check whether the created blueprint is successfully pushed and exists:
+
[subs="quotes,attributes"]
----
# composer-cli blueprints show _&lt;blueprint-name&gt;_
----

. Check whether the components and versions listed in the blueprint and their dependencies are valid:
+
[subs="quotes,attributes"]
----
# composer-cli blueprints depsolve _&lt;blueprint-name&gt;_
----

[role="_additional-resources"]
.Next steps

* Use the blueprint you created to build your `edge-ami` image. 

