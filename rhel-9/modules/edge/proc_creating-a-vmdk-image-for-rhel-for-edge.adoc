:_mod-docs-content-type: PROCEDURE

[id="creating-a-vmdk-image-for-rhel-for-edge_{context}"]
= Creating a VMDK image for RHEL for Edge

[role="_abstract"]
To create a RHEL for Edge `.vmdk` image, use the 'edge-vsphere' image type in the RHEL image builder command-line interface.

.Prerequisites

* You created a blueprint for the `.vmdk` image. 
* You served an OSTree repository of the commit to embed it in the image. For example, `\http://10.0.2.2:8080/repo`. For more details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/composing_installing_and_managing_rhel_for_edge_images/installing-rpm-ostree-images_composing-installing-managing-rhel-for-edge-images#setting-up-a-web-server-to-install-rhel-for-edge-image_installing-rpm-ostree-images[Setting up a web server to install RHEL for Edge image].

.Procedure

. Start the compose of a `.vmdk` image:
+
[subs="quotes,attributes"]
----
# composer-cli compose start start-ostree _&lt;blueprint-name&gt;_ edge-vsphere --_&lt;url&gt;_ 
----
The --_&lt;url&gt;_ is the URL of your repo, for example: `\http://10.88.0.1:8080/repo`.
+
A confirmation that the composer process has been added to the queue appears. It also shows a Universally Unique Identifier (UUID) number for the image created. Use the UUID number to track your build. Also, keep the UUID number handy for further tasks.

. Check the image compose status:
+
[subs="quotes,attributes"]
----
# composer-cli compose status
----
+
The output displays the status in the following format:
+
[subs="quotes,attributes"]
----
$ _&lt;UUID&gt;_ RUNNING date _&lt;blueprint-name&gt;_  _&lt;blueprint-version&gt;_ edge-vsphere
----
. After the compose process finishes, download the resulting image file:
+
[subs="quotes,attributes"]
----
# composer-cli compose image _&lt;UUID&gt;_
----

[role="_additional-resources"]
.Next steps

* Upload the `.vmdk` image to vSphere.



