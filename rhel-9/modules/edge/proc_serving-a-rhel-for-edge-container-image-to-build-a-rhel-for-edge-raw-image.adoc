:_newdoc-version: 2.15.0
:_template-generated: 2023-11-29
:_mod-docs-content-type: PROCEDURE

[id="serving-a-rhel-for-edge-container-image-to-build-a-rhel-for-edge-raw-image_{context}"]
= Serving a RHEL for Edge Container image to build a RHEL for Edge Raw image

[role="_abstract"]
Create a RHEL for Edge Container image to serve it to a running container.

.Prerequisites

* You have created a RHEL for Edge Minimal Raw image and downloaded it.

.Procedure

. Create a blueprint for the `rhel-edge-container` image type, for example: 
+
[subs="quotes,attributes"]
----
name = "rhel-edge-container-no-users"
description = ""
version = "0.0.1"
----

. Build a `rhel-edge-container` image:
+
[subs="quotes,attributes"]
----
# composer-cli compose start-ostree _&lt;rhel-edge-container-no-users&gt;_ rhel-edge-container
----

. Check if the image is ready:
+
[subs="quotes,attributes"]
----
# composer-cli compose status
----

. Download the `rhel-edge-container` image as a `.tar` file:
+
[subs="quotes,attributes"]
----
# composer-cli compose image _&lt;UUID&gt;_
----

. Import the RHEL for Edge Container into Podman:
+
[subs="quotes,attributes"]
----
$ skopeo copy oci-archive:_&lt;UUID&gt;_-container.tar \
containers-storage:localhost/rfe-93-mirror:latest
----

. Start the container and make it available by using the port 8080:
+
[subs="quotes,attributes"]
----
$ podman run -d --rm --name _&lt;rfe-93-mirror&gt;_ -p 8080:8080 localhost/_&lt;rfe-93-mirror&gt;_
----

. Create a blueprint for the `edge-raw-image` image type, for example: 
+
[subs="quotes,attributes"]
----
name = "_&lt;edge-raw&gt;_"
description = ""
version = "0.0.1"
[[customizations.user]]
name = "admin"
password = "admin"
groups = ["wheel"]
----

. Build a RHEL for Edge Raw image  by serving the RHEL Edge Container to it:
+
[subs="quotes,attributes"]
----
# composer-cli compose start-ostree edge-raw edge-raw-image \
--url http://10.88.0.1:8080/repo
----

. Download the RHEL for Edge Raw image as a `.raw` file:
+
[subs="quotes,attributes"]
----
# composer-cli compose image _&lt;UUID&gt;_
----

. Decompress the RHEL for Edge Raw image:
+
[subs="quotes,attributes"]
----
# xz --decompress _&lt;UUID&gt;_>-image.raw.xz
----

