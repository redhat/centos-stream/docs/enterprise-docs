:_mod-docs-content-type: PROCEDURE

[id="creating-a-rhel-for-edge-minimal-raw-image-using-image-builder-cli_{context}"]
= Creating a Minimal Raw image by using RHEL image builder CLI

[role="_abstract"]
Create a RHEL for Edge Minimal Raw image with the RHEL image builder command-line interface.

.Prerequisites

* You created a blueprint for the RHEL for Edge Minimal Raw image.

.Procedure

. Build the image.
+
[subs="quotes,attributes"]
----
# composer-cli compose start _&lt;blueprint_name&gt;_ minimal-raw
----
+

* `_&lt;blueprint_name&gt;_` is the RHEL for Edge blueprint name
* `minimal-raw` is the image type

. Check the image compose status.
+
[subs="quotes,attributes"]
----
# composer-cli compose status
----
+
The output displays the status in the following format:
+
[subs="quotes,attributes"]
----
# _&lt;UUID&gt;_ RUNNING date _&lt;blueprint_name&gt;_ blueprint-version minimal-raw
----

[role="_additional-resources"]
.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/composing_installing_and_managing_rhel_for_edge_images/composing-a-rhel-for-edge-image-using-image-builder-command-line_composing-installing-managing-rhel-for-edge-images[Composing a RHEL for Edge image using image builder command-line]
