:_mod-docs-content-type: PROCEDURE

[id="proc_creating-an-ignition-configuration-file_{context}"]
= Creating an Ignition configuration file

The `Butane` tool is the preferred option to create an Ignition configuration file. `Butane` consumes a `Butane Config YAML` file and produces an `Ignition Config` in the JSON format. The JSON file is used by a system on its first boot. The `Ignition Config` applies the configuration in the image, such as user creation, and `systemd` units installation. 

.Prerequisites

* You have installed the Butane tool version v0.17.0: 
+
[subs="quotes,attributes"]
----
$ sudo dnf/yum install -y butane
----

.Procedure

. Create a `Butane Config` file and save it in the `.bu` format. You must specify the `variant` entry as `r4e`, and the `version` entry as `1.0.0`,  for RHEL for Edge images. The butane `r4e` variant on version 1.0.0 targets Ignition spec version `3.3.0`.
The following is a Butane Config  YAML file example:
+
[subs="quotes,attributes"]
----
variant: r4e
version: 1.0.0
ignition:
  config:
    merge:
      - source: http://192.168.122.1:8000/sample.ign
passwd:
  users:
    - name: core
      groups:
        - wheel
      password_hash: password_hash_here
      ssh_authorized_keys:
        - ssh-ed25519 some-ssh-key-here
storage:
  files:
    - path: /etc/NetworkManager/system-connections/enp1s0.nmconnection
      contents:
        inline: |
          [connection]
          id=enp1s0
          type=ethernet
          interface-name=enp1s0
          [ipv4]
          address1=192.168.122.42/24,192.168.122.1
          dns=8.8.8.8;
          dns-search=
          may-fail=false
          method=manual
      mode: 0600
    - path: /usr/local/bin/startup.sh
      contents:
        inline: |
          #!/bin/bash
          echo "Hello, World!"
      mode: 0755
systemd:
  units:
    - name: hello.service
      contents: |
        [Unit]
        Description=A hello world
        [Install]
        WantedBy=multi-user.target
      enabled: true
    - name: fdo-client-linuxapp.service
      dropins:
        - name: log_trace.conf
          contents: |
            [Service]
            Environment=LOG_LEVEL=trace
----

. Run the following command to consume the `Butane Config YAML` file and generate an Ignition Config in the JSON format:
+
[subs="quotes,attributes"]
----
$ ./path/butane example.bu
{"ignition":{"config":{"merge":[{"source":"http://192.168.122.1:8000/sample.ign"}]},"timeouts":{"httpTotal":30},"version":"3.3.0"},"passwd":{"users":[{"groups":["wheel"],"name":"core","passwordHash":"password_hash_here","sshAuthorizedKeys":["ssh-ed25519 some-ssh-key-here"]}]},"storage":{"files":[{"path":"/etc/NetworkManager/system-connections/enp1s0.nmconnection","contents":{"compression":"gzip","source":"data:;base64,H4sIAAAAAAAC/0yKUcrCMBAG3/csf/ObUKQie5LShyX5SgPNNiSr0NuLgiDzNMPM8VBFtHzoQjkxtPp+ITsrGLahKYyyGtoqEYNKwfeZc32OC0lKDb179rfg/HVyPgQ3hv8w/v0WT0k7T+7D/S1Dh7S4MRU5h1XyzqvsHVRg25G4iD5kp1cAAAD//6Cvq2ihAAAA"},"mode":384},{"path":"/usr/local/bin/startup.sh","contents":{"source":"data:;base64,IyEvYmluL2Jhc2gKZWNobyAiSGVsbG8sIFdvcmxkISIK"},"mode":493}]},"systemd":{"units":[{"contents":"[Unit]\nDescription=A hello world\n[Install]\nWantedBy=multi-user.target","enabled":true,"name":"hello.service"},{"dropins":[{"contents":"[Service]\nEnvironment=LOG_LEVEL=trace\n","name":"log_trace.conf"}],"name":"fdo-client-linuxapp.service"}]}}
----
+
After you run the `Butane Config YAML` file to check and generate the `Ignition Config JSON` file, you might get warnings when using unsupported fields, like partitions, for example. You can fix those fields and rerun the check.

You have now an Ignition JSON configuration file that you can use to customize your blueprint.


[role="_additional-resources"]
.Additional resources
* link:https://coreos.github.io/butane/config-r4e-v1_0/[RHEL for Edge Specification v1.0.0]

