:_mod-docs-content-type: PROCEDURE

[id="proc_creating-a-blueprint-in-the-gui-with-support-to-ignition_{context}"]
= Creating a blueprint in the GUI with support to Ignition

When building a Simplified Installer image, you can customize your blueprint by entering the following details in the Ignition page of the blueprint:

* `Firstboot URL` - You must enter a URL that points to the Ignition configuration that will be fetched during the first boot. It can be used for both the raw image and simplified installer image.

* `Embedded Data` - You must provide the `base64` encoded `Ignition Configuration` file. It can be used only for the Simplified Installer image.

To customize your  blueprint for a simplified RHEL for Edge image with support to Ignition configuration using the Ignition blueprint customization, follow the steps:

.Prerequisites

* You have opened the image builder app from web console in a browser. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/composing_a_customized_rhel_system_image/index#accessing-composer-gui-in-the-rhel-8-web-console_creating-system-images-with-composer-web-console-interface[Accessing the image builder GUI in the RHEL web console].

* To fully support the embedded section, `coreos-installer-dracut` has to be able to define `-ignition-url`|`-ignition-file` based on the presence of the OSBuild's file.


.Procedure

. Click btn:[Create Blueprint] in the upper-right corner.
+
A dialog wizard with fields for the blueprint name and description opens.

* On the `Details` page:
** Enter the name of the blueprint and, optionally, its description. Click btn:[Next].

* On the `Ignition` page, complete the following steps:
** On the `Firstboot URL` field, enter the URL that points to the Ignition configuration to be fetched during the first boot.
** On the `Embedded Data` field, drag or upload the `base64` encoded `Ignition Configuration` file. Click btn:[Next].

* Review the image details and click btn:[Create].

The image builder dashboard view opens, listing the existing blueprints.

.Next

* You can use the blueprint you created to build your Simplified Installer image. See xref:creating-a-rhel-for-edge-simplified-installer-image-using-image-builder_building-and-provisioning-simplified-installer-images[Creating a RHEL for Edge Simplified Installer image using image builder CLI].


