:_mod-docs-content-type: PROCEDURE

[id="using-heatmaps-in-grafana_{context}"]
= Using heatmaps in Grafana

You can use heatmaps in Grafana to view histograms of your data over time, identify trends and patterns in your data, and see how they change over time. Each column within a heatmap represents a single histogram with different colored cells representing the different densities of observation of a given value within that histogram.


IMPORTANT: This specific workflow is for the heatmaps in Grafana version 10 and later on RHEL9.

.Prerequisites

* PCP Redis is configured. For more information see xref:configuring-pcp-redis_setting-up-graphical-representation-of-pcp-metrics[Configuring PCP Redis].
* The Grafana server is accessible. For more information see xref:accessing-the-grafana-web-ui_setting-up-graphical-representation-of-pcp-metrics[Accessing the Grafana Web UI].
* The PCP Redis data source is configured. For more information see xref:creating-panel-and-alerts-in-pcp-redis-data-source_setting-up-graphical-representation-of-pcp-metrics[Creating panels and alerts in PCP Redis data source].


.Procedure

. In the menu, select menu:Dashboards[New Dashboard > + Add visualization].
. In the *Select data source* pane, select *pcp-redis-datasource*.
. In the *Query* tab, in the text field below *A*, enter a metric, for example, `kernel.all.load` to visualize the kernel load graph.
. From the *Time series* drop-down menu on the right, select *Heatmap*.
. Optional: In the *Panel Options* dropdown menu, add a *Panel Title* and *Description*.
. In the *Heatmap* dropdown menu, under the *Calculate from data* setting, click *Yes*.
+
.Heatmap
image:grafana_heatmap.png[A configured Grafana heatmap]
. Optional: In the *Colors* dropdown menu, change the *Scheme* from the default *Orange* and select the number of steps (color shades).
. Optional: In the *Tooltip* dropdown menu, click the toggle to display a cell’s position within its specific histogram when hovering your cursor over a cell in the heatmap. For example:
+
.Show histogram (Y Axis) cell display
image:grafana_histogram.png[A cell's specific position within its histogram]
