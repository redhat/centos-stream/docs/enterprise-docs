// Module included in the following assemblies:
//reviewing-a-system-using-tuna-interface
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="tuning-cpus-using-tuna-tool_{context}"]
= Tuning CPUs by using the tuna tool

[role="_abstract"]
The `tuna` tool commands can target individual CPUs. By using the `tuna` tool, you can perform the following actions:

`Isolate CPUs`::
All tasks running on the specified CPU move to the next available CPU. Isolating a CPU makes this CPU unavailable by removing it from the affinity mask of all threads.

`Include CPUs`::
Allows tasks to run on the specified CPU.

`Restore CPUs`::
Restores the specified CPU to its previous configuration.


.Prerequisites

* The `tuna` tool is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/reviewing-a-system-using-tuna-interface_monitoring-and-managing-system-status-and-performance#installing-tuna-tool_reviewing-a-system-using-tuna-interface[Installing the tuna tool].

.Procedure

. List all CPUs and specify the list of CPUs to be affected by the command:
+
[literal,subs="+quotes"]
----
# *ps ax | awk 'BEGIN { ORS="," }{ print $1 }'*
PID,1,2,3,4,5,6,8,10,11,12,13,14,15,16,17,19
----
. Display the thread list in the `tuna` interface:
+
[literal,subs="+quotes"]
----
# *tuna show_threads -t 'thread_list from above cmd'*
----
+
. Specify the list of CPUs to be affected by a command:
+
[literal,subs="+quotes"]
----
# *tuna [_command_] --cpus _cpu_list_ *
----
+
The _cpu_list_ argument is a list of comma-separated CPU numbers, for example, `--cpus _0,2_`.
+
To add a specific CPU to the current _cpu_list_, use, for example, `--cpus +0`.

. Depending on your scenario, perform one of the following actions:

** To isolate a CPU, enter:
+
[literal,subs="+quotes"]
----
# *tuna isolate --cpus cpu_list*
----

** To include a CPU, enter:
+
[literal,subs="+quotes"]
----
# *tuna include --cpus cpu_list*
----

. To use a system with four or more processors, make all `ssh` threads run on CPU _0_ and _1_ and all `http` threads on CPU _2_ and _3_:
+
[literal,subs="+quotes"]

----
# *tuna move --cpus 0,1 -t ssh\**
# *tuna move --cpus 2,3 -t http\**
----


.Verification

* Display the current configuration and verify that the changes were applied:
+
[literal,subs="+quotes",options="nowrap", role=white-space-pre]
----
# *tuna show_threads -t ssh\**

pid   SCHED_  rtpri  affinity   voluntary   nonvoluntary   cmd 
855   OTHER   0      0,1        23           15            sshd

# *tuna show_threads -t http\**
pid   SCHED_  rtpri  affinity   voluntary   nonvoluntary   cmd 
855   OTHER   0       2,3        23           15           http

----




[role="_additional-resources"]
.Additional resources
* `/proc/cpuinfo` file
* `tuna(8)` man page on your system
