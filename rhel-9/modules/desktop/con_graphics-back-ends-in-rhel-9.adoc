:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_overview-of-gnome-environments.adoc

[id="graphics-back-ends-in-rhel_{context}"]
= Graphics back ends in RHEL 9

[role="_abstract"]
In RHEL 9, you can choose between two protocols to build a graphical user interface:

Wayland::
The *Wayland* protocol uses *GNOME Shell* as its compositor and display server, which is further referred to as *GNOME Shell on Wayland*.

X11::
The *X11* protocol uses *X.Org* as the display server. Displaying graphics based on this protocol works the same way as in RHEL 7, where this was the only option.

New installations of RHEL 9 automatically select *GNOME Shell on Wayland*. However, you can switch to *X.Org*, or select the required combination of GNOME environment and display server.

.X11 applications

Client applications need to be ported to the *Wayland* protocol or use a graphical toolkit that has a *Wayland* backend, such as GTK, to be able to work natively with the compositor and display server based on *Wayland*.

Legacy *X11* applications that cannot be ported to *Wayland* automatically use *Xwayland* as a proxy between the *X11* legacy clients and the *Wayland* compositor. *Xwayland* functions both as an *X11* server and a *Wayland* client. The role of *Xwayland* is to translate the *X11* protocol into the *Wayland* protocol and reversely, so that *X11* legacy applications can work with the display server based on *Wayland*.

On *GNOME Shell on Wayland*, *Xwayland* starts automatically at login, which ensures that most *X11* legacy applications work as expected when using *GNOME Shell on Wayland*. However, the *X11* and *Wayland* protocols are different, and certain clients that rely on features specific to *X11* might behave differently under *Xwayland*. For such specific clients, you can switch to the *X.Org* display server.

.Input devices

RHEL 9 uses a unified input stack, `libinput`, which manages all common device types, such as mice, touchpads, touchscreens, tablets, trackballs and pointing sticks. This unified stack is used both by the *X.Org* and by the *GNOME Shell on Wayland* compositor.

*GNOME Shell on Wayland* uses `libinput` directly for all devices, and no switchable driver support is available. Under *X.Org*, `libinput` is implemented as the *X.Org* `libinput` driver, and you can optionally enable the legacy *X.Org* `evdev` driver if `libinput` does not support your input device.


[role="_additional-resources"]
.Additional resources
* You can find the current list of environments for which Wayland is not available in the [filename]`/usr/lib/udev/rules.d/61-gdm.rules` file.
* For additional information about the *Wayland* project, see link:https://wayland.freedesktop.org/[Wayland documentation].
