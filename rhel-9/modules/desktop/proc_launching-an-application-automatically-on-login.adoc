////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

[id="proc_launching-an-application-automatically-on-login_{context}"]
= Launching an application automatically on login

You can set applications to launch automatically on login using the *Tweaks* tool. *Tweaks* is a tool to customize the GNOME Shell environment for a particular user.

.Prerequisites

* You have installed `gnome-tweaks` on your system. For more details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/administering_the_system_using_the_gnome_desktop_environment/assembly_installing-software-in-gnome_administering-the-system-using-the-gnome-desktop-environment[Installing software in GNOME]
* You have installed the application that you want to launch at login.

.Procedure

. Open *Tweaks*. For more details see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/getting_started_with_the_gnome_desktop_environment/assembly_launching-applications-in-gnome_getting-started-with-the-gnome-desktop-environment[Launching applications in GNOME].
. Select *Startup Applications* in the left side bar.
+
image:tweaks-select-startup-applications.png[Select Startup Applications]
. Click the plus sign button (btn:[+]).
+
image:tweaks-plus-button.png[Click plus sign button]
. Select an application from the list of available applications and click btn:[Add].
+
image:tweaks-add-app.png[Add application]

.Verification
. Open *Tweaks*.
. Select *Startup Applications* in the left side bar.
. List of applications launched at start will be present in the center section.
+
image:tweaks-list-of-apps.png[Select Startup Applications]

[role="_additional-resources"]
.Additional resources
* For more information about lauching applications, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/getting_started_with_the_gnome_desktop_environment/assembly_launching-applications-in-gnome_getting-started-with-the-gnome-desktop-environment[Launching applications in GNOME]
