:_mod-docs-content-type: PROCEDURE

[id="managing-gnome-shell-extensions-via-command-line_{context}"]
= Managing GNOME Shell extensions by using the command line

The `gnome-extensions` utility is a command-line tool that allows you to manage GNOME Shell extensions from the terminal. It provides various commands to list, install, enable, disable, remove, and get information about extensions.

Each GNOME Shell extension has a UUID (Universally Unique Identifier). You can find the UUID of an extension on its GNOME Shell Extensions website page.

.Procedure

* To list the installed GNOME Shell extensions, use:
+
----
$ gnome-extensions list
----

* To install a GNOME Shell extension, use:
+
----
$ gnome-extensions install <UUID>
----

* To enable a GNOME Shell extension, use:
+
----
$ gnome-extensions enable <UUID>
----

* To show information about a GNOME Shell extension, use:
+
----
$ gnome-extensions info <UUID>
----

* To disable a GNOME Shell extension, use:
+
----
$ gnome-extensions disable <UUID>
----

* To remove a GNOME Shell extension, use:
+
----
$ gnome-extensions uninstall <UUID>
----


Replace the `<UUIDs>` with the unique identifier assigned to the GNOME Shell extension you want to install.

[role="_additional-resources"]
.Additional resources
* The `gnome-extensions --help` page.