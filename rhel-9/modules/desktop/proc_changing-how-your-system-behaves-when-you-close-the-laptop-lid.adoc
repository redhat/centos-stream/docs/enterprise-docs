:_mod-docs-content-type: PROCEDURE

[id="changing-how-your-system-behaves-when-you-close-the-laptop-lid_{context}"]
= Changing how your system behaves when you close the laptop lid

When you close the lid of your laptop, it suspends by default to save battery. You can customize this behavior according to your preferences.

WARNING::  Some laptops can overheat if they are left running with the lid closed, especially in confined spaces. Consider whether changing this setting is safe for your laptop, especially if you intend to keep the laptop running with the lid closed for extended periods of time.

.Prerequisites
* Administrative access.

.Procedure

. Open the `/etc/systemd/logind.conf` configuration file.
. Look for the line that says `HandleLidSwitch=suspend`.
. If the line starts with the `#` symbol, remove it to enable the setting.
. Replace `suspend` with one of the following options:
+
--
* `poweroff` to shut down the computer.
* `lock` to lock the screen.
* `ignore` to do nothing.
--
+
For example,  to lock the screen upon closing the lid, use this setting:
+
----
HandleLidSwitch=lock
----
. Save your changes and close the editor.