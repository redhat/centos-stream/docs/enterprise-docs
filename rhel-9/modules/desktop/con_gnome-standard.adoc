:_mod-docs-content-type: CONCEPT

:experimental:

[id="gnome-standard_{context}"]
= GNOME Standard

[role="_abstract"]
The GNOME Standard user interface includes these major components:

Top bar::
The horizontal bar at the top of the screen provides access to some of the basic functions of GNOME Standard, such as the *Activities Overview*, clock and calendar, system status icons, and the *system menu*.

System menu::
image:gnome-system-menu.png[System menu]
+
The *system menu* is located in the upper-right corner, and provides the following functionality:

** Updating settings
** Controlling the sound volume
** Accessing your Wi-Fi connection
** Switching the user
** Logging out
** Turning off the computer

Activities Overview::
The *Activities Overview* features windows and applications views that let you run applications and windows and switch between them.
+
The *search entry* at the top allows for searching various items available on the desktop, including applications, documents, files, and configuration tools.
+
The horizontal bar on the bottom contains a list of favorite and running applications. You can add or remove applications from the default list of favorites.

Message tray::
The *message tray* provides access to pending notifications. The *message tray* shows when you press kbd:[Super+M].

.The GNOME Standard desktop
image:gnome-standard-9.png[]
