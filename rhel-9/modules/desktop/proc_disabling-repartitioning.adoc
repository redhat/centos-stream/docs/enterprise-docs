:_mod-docs-content-type: PROCEDURE

[id="disabling-repartitioning_{context}"]
= Disabling repartitioning

You can override the default system settings that control disk management.

IMPORTANT: Avoid modifying the `/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy` file directly. Any changes you make will be replaced during the next package update.

.Prerequisites
* Administrative access.

.Procedure

. Copy the `/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy` file under the `/etc/share/polkit-1/actions/` directory:
+
----
# cp /usr/share/polkit-1/actions/org.freedesktop.udisks2.policy /etc/share/polkit-1/actions/org.freedesktop.udisks2.policy
----

. In the `/etc/polkit-1/actions/org.freedesktop.udisks2.policy` file, delete any actions that you do not need and add the following lines:
+
[source,xml]
----
<action id="org.freedesktop.udisks2.modify-device">
  <message>Authentication is required to modify the disks settings</message>
     <defaults>
        <allow_any>no</allow_any>
        <allow_inactive>no</allow_inactive>
        <allow_active>yes</allow_active>
      </defaults>
 </action>
----
+
If you want to restrict access only to the root user, replace `<allow_any>no</allow_any>` with `<allow_any>auth_admin</allow_any>`.