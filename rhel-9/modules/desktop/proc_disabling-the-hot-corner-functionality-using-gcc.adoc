:_mod-docs-content-type: PROCEDURE
[id="disabling-hot-corner-using-gcc_{context}"]
= Disabling hot corner using Settings

To disable the hot corner functionality using the [application]*Settings* application, follow this procedure.

NOTE: This procedure disables the hot corner functionality for a *single* user.

.Procedure

. Open the [application]*Settings* application by clicking the gear button.

. In the [application]*Settings* application, go to *Multitasking*.

. In the *General* section, disable the btn:[Hot Corner] button.
+
.Disabling hot corner using the *Settings* application
image:disabling-hot-corner-using-settings.png[]

