:_mod-docs-content-type: PROCEDURE

[id="gnome-screen-recording_{context}"]
= GNOME Screen Recording

GNOME Screen Recording is a built-in feature in the GNOME desktop environment that allows users to record their desktop or specific application activities. The recordings are saved as video files in the WebM format.

.Procedure

. To start the recording, press the kbd:[Ctrl+Alt+Shift+R] keyboard shortcut.
+
Once the recording begins, a red circle indicator appears in the upper-right corner of the screen, indicating that the recording is active.

. To stop the recording, press the same kbd:[Ctrl+Alt+Shift+R] keyboard shortcut again.
+
The red circle indicator disappears, signaling the end of the recording.


The recorded video files are saved in the `~/Videos` directory.
The filenames of recorded videos start with `Screencast` and include the date and time of the recording.