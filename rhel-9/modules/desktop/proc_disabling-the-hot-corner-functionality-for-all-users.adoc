:_mod-docs-content-type: PROCEDURE
[id="disabling-the-hot-corner-functionality-for-all-users-gcc_{context}"]
= Disabling the hot corner functionality for all users

To disable the hot corner functionality for all users, you need to create a [systemitem]`dconf` profile.

.Procedure

. Create the user profile in the [filename]`/etc/dconf/profile/user` file.
+
----
user-db:user
system-db:local
----

. Create a file in the [filename]`/etc/dconf/db/local.d/` directory, for example [filename]`/etc/dconf/db/local.d/00-interface`, with the following content:
+
----
# Specify the dconf path
[org/gnome/desktop/interface]

# GSettings key names and their corresponding values
enable-hot-corners='FALSE'
----

. Create a file in the [filename]`/etc/dconf/db/local.d/locks` directory, for example [filename]`/etc/dconf/db/local.d/locks/00-interface`, with the following content:
+
----
# Prevent users from changing values for the following keys:
/org/gnome/desktop/interface/enable-hot-corners
----
+
The configuration file locks down the [filename]`/org/gnome/desktop/interface/enable-hot-corners` key for all users. This key controls whether the hot corner is enabled.

. Update the system databases for the changes to take effect.
+
----
# dconf update
----

. Ensure that all users log out. The changes take effect when users log back in.

