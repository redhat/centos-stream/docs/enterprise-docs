:_mod-docs-content-type: PROCEDURE

[id="adding-enterprise-user-in-gnome_{context}"]
= Adding enterprise users in GNOME

You can add an enterprise user to GNOME using Settings.

.Prerequisites

* Administrative access.
* xref:configuring-enterprise-credentials-in-gnome_{context}[You have configured Enterprise credentials].

.Procedure

. Open *Settings*.
// need to verify the UI
. Click *Users*.
. Select the btn:[Unlock] button and enter your password.
. Click *Add User*.
. Choose *Enterprise Login*
. Enter the domain, username, and password for your Enterprise account.
. Click btn:[Add].
+
Depending on the domain configuration, you might need to enter administrator credentials.