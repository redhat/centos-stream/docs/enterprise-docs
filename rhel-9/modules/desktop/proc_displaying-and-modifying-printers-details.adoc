:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_modifying-printer-settings.adoc

[id="displaying-and-modifying-printers-details_{context}"]
= Displaying and modifying printer's details

[role="_abstract"]
To maintain a configuration of a printer, use the [application]*Settings* application:

.Procedure

. Click the settings (⚙️) button on the right to display a settings menu for the selected printer:
+
image::gnome-control-center-printer-settings-9.png[]

. Click *Printer Details* to display and modify selected printer's settings:
+
image::gnome-control-center-printer-details-9.png[]
+
In this menu, you can select the following actions:
+
--
Search for Drivers::
GNOME Control Center communicates with [application]*PackageKit* that searches for a suitable driver suitable in available repositories.
Select from Database::
This option enables you to select a suitable driver from databases that have already been installed on the system.
Install PPD File::
This option enables you to select from a list of available postscript printer description (PPD) files that can be used as a driver for your printer.
--
+
image::gnome-control-center-printer-details-more-9.png[]
