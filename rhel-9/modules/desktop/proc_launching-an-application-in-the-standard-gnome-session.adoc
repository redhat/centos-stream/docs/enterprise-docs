:_module-type: PROCEDURE

[id="proc_launching-an-application-in-the-standard-gnome-session_{context}"]
= Launching an application in the standard GNOME session

[role="_abstract"]
This procedure launches a graphical application in the GNOME desktop environment.

.Prerequisites

* You are using the standard GNOME session. 

.Procedure

. Open the *Activities Overview* screen using either of the following ways:
+
** Click *Activities* in the top panel.
** Press the kbd:[Super] key, which is usually labeled with the Windows logo, kbd:[⌘], or kbd:[🔍].

. Find the application using either of the following ways:

** Click the *Show Applications* icon in the bottom horizontal bar.
+
image::launching-applications-standard.png[Applications overview in GNOME]

** Type the name of the required application in the search entry. 

. Click the application in the displayed list.

// .Verification
// 
// . Start each step with an active verb.
// 
// . Include one command or action per step.
// 
// . Use an unnumbered bullet (*) if the procedure includes only one step.

// [role="_additional-resources"]
// .Additional resources
// * A bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

