:_mod-docs-content-type: PROCEDURE

// Module included in the following assemblies:
//
// assembly_modifying-printer-settings.adoc

[id="setting-the-default-printer_{context}"]
= Setting the default printer

[role="_abstract"]
You can set the selected printer as the default printer.

.Procedure

. Click the settings (⚙️) button on the right to display a settings menu for the selected printer:
+
image::gnome-control-center-printer-settings-9.png[]

. Click *Use Printer by Default* to set the selected printer as the default printer:
+
image::gnome-control-center-default-printer-9.png[]
