:_mod-docs-content-type: PROCEDURE

:experimental:

[id="connecting-to-a-shared-desktop-using-gnome_{context}"]
= Connecting to a shared desktop using GNOME

This procedure connects to a remote desktop session using the *Connections* application.
It connects to the graphical session of the user that is currently logged in on the server.

.Prerequisites

* A user is logged into the GNOME graphical session on the server.
* The desktop sharing is enabled on the server.

.Procedure

. Install the *Connections* application on the client:
+
[subs="quotes"]
----
# *dnf install gnome-connections*
----

. Launch the *Connections* application.

. Click the btn:[+] button to open a new connection.
+
image:gnome-connections-1.png[]

. Enter the IP address of the server.
+
image:gnome-connections-2.png[]

. Choose the connection type based on the operating system you want to connect to.

. Click btn:[Connect].


.Verification

. On the client, check that you can see the shared server desktop.
. On the server, a screen sharing indicator appears on the right side of the top panel:
+
image:screen-sharing-indicator-9.png[]
+
You can control the screen sharing in the system menu.

