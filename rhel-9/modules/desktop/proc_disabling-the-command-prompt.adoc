:_mod-docs-content-type: PROCEDURE

[id="disabling-the-command-prompt_{context}"]
= Disabling the command prompt

Disabling the command prompt can simplify user interactions with the system, prevent inexperienced users from executing potentially harmful commands that might cause system instability or data loss, and  reduce the risk of unauthorized changes to system settings or configurations.

.Prerequisites

* Administrative access.

.Procedure

. Create a plain text `/etc/dconf/db/local.d/00-lockdown` keyfile in the `/etc/dconf/db/local.d/` directory with the following content:
+
----
[org/gnome/desktop/lockdown]

# Disable command prompt
disable-command-line=true
----

. Create a new file under the `/etc/dconf/db/local.d/locks/` directory and list the keys or subpaths you want to lock down:
+
----
# Lock command prompt
/org/gnome/desktop/lockdown/disable-command-line
----

. Apply the changes to the system databases:
+
----
# dconf update
----

. For this settings to take effect, users needs to log out and log back in.