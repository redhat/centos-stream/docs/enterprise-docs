:_mod-docs-content-type: REFERENCE

[id="the-timerlat-tracer-options_{context}"]
= The timerlat tracer options 

The `timerlat` tracer is built on top of `osnoise` tracer. Therefore, you can set the options in the `/osnoise/config` directory to trace and capture information for thread scheduling latencies. 

.timerlat options
cpus:: Sets CPUs for a `timerlat` thread to execute on.
timerlat_period_us:: Sets the duration period of the `timerlat` thread in microseconds.
stop_tracing_us:: Stops the system tracing if a timer latency at the `irq` context is more than the configured value. Writing 0 disables this option.
stop_tracing_total_us:: Stops the system tracing if the total noise is more than the configured value. Writing 0 disables this option.
print_stack:: Saves the stack of the interrupt requests (IRQs) occurrence. The stack saves the IRQs occurrence after the thread context event, or if the IRQs handler is more than the configured value.





