:_mod-docs-content-type: PROCEDURE

[id="configuring-the-timerlat-tracer-to-measure-scheduling-latency_{context}"]
= Configuring the timerlat tracer to measure scheduling latency

You can configure the `timerlat` tracer by adding `timerlat` in the `curret_tracer` file of the tracing system. The `current_tracer` file is generally mounted in the `/sys/kernel/tracing` directory. The `timerlat` tracer measures the interrupt requests (IRQs) and saves the trace output for analysis when a thread latency is more than 100 microseconds.


.Procedure

. List the current tracer: 
+
[literal,subs="+quotes,attributes"]
----
# *cat /sys/kernel/tracing/current_tracer*
nop
----
+
The `no operations` (`nop`) is the default tracer.
. Add the `timerlat` tracer in the `current_tracer` file of the tracing system:
+
[literal,subs="+quotes,attributes"]
----
# *cd /sys/kernel/tracing/*
# *echo timerlat > current_tracer*
----
. Generate a tracing output:
+
[literal,subs="+quotes,attributes"]
----
# *cat trace*
# *tracer: timerlat*
----


.Verification

* Enter the following command to check if `timerlat` is enabled as the current tracer:
+
[literal,subs="+quotes,attributes"]
----
# *cat /sys/kernel/tracing/current_tracer*
timerlat
----




