:_mod-docs-content-type: REFERENCE

[id="the-rtla-osnoise-top-tracer-options_{context}"]
= The rtla-osnoise-top tracer options

By using the `rtla osnoise top --help` command, you can view the help usage on the available options for the `rtla-osnoise-top` tracer. 

.Options for `rtla-osnoise-top` 
-a, --auto us:: Sets the automatic trace mode. This mode sets some commonly used options while debugging the system. It is equivalent to use `-s` `us` `-T` `1` and `-t`.
-p, --period us:: Sets the `osnoise` tracer duration period in microseconds.
-r, --runtime us:: Sets the `osnoise` tracer runtime in microseconds.
-s, --stop us:: Stops the trace if a single sample is more than the argument in microseconds. With `-t`, the command saves the trace to the output.
-S, --stop-total us:: Stops the trace if the total sample is more than the argument in microseconds. With `-T`, the command saves a trace to the output.
-T, --threshold us:: Specifies the minimum delta between two time reads to be considered noise. The default threshold is 5 us.
-q, --quiet:: Prints only a summary at the end of a run.
-c, --cpus cpu-list:: Sets the `osnoise` tracer to run the sample threads on the assigned `cpu-list`.
-d, --duration time[s|m|h|d]:: Sets the duration of a run.
-D, --debug:: Prints debug information.
-t, --trace[=file]:: Saves the stopped trace to `[file|osnoise_trace.txt]` file.
-e, --event sys:event:: Enables an event in the trace (`-t`) session. The argument can be a specific event, for example `-e sched:sched_switch`, or all events of a system group, such as  `-e sched` system group. 
--filter _<filter>_:: Filters the previous `-e sys:event` system event with a filter expression. 
--trigger _<trigger>_:: Enables a trace event trigger to the previous `-e sys:event` system event. 
-P, --priority o:prio|r:prio|f:prio|d:runtime:period:: Sets the scheduling parameters to the `osnoise` tracer threads. 
-h, --help:: Prints the help menu.






