:_newdoc-version: 2.17.0
:_template-generated: 2024-05-23
:_mod-docs-content-type: PROCEDURE

[id="configuring-network-interfaces-on-a-network-bond-by-using-nmcli_{context}"]
= Configuring network interfaces on a network bond by using nmcli

To configure a network bond on the command line, use the `nmcli` utility.

.Prerequisites

* Two or more physical devices are installed on the server, and they are not configured in any `NetworkManager` connection profile.						

.Procedure

. Create a bond interface:
+
[literal,subs="+quotes,attributes"]
----
# **nmcli connection add type bond con-name bond0 ifname bond0 bond.options "mode=active-backup"**
----
This command creates a bond named `bond0` that uses the `active-backup` mode.

. Assign the Ethernet interfaces to the bond:
+
[literal,subs="+quotes,attributes"]
----
# **nmcli connection add type ethernet slave-type bond con-name bond0-port1 ifname enp7s0 master bond0**
# **nmcli connection add type ethernet slave-type bond con-name bond0-port2 ifname enp8s0 master bond0**
----
These commands create profiles for `enp7s0` and `enp8s0`, and add them to the `bond0` connection.

. Configure the IPv4 settings:

* To use DHCP, no action is required.
* To set a static IPv4 address, network mask, default gateway, and DNS server to the `bond0` connection, enter:
+
[literal,subs="+quotes,attributes"]
----
# **nmcli connection modify bond0 ipv4.addresses `192.0.2.1/24` ipv4.gateway `192.0.2.254` ipv4.dns `192.0.2.253`  ipv4.dns-search `example.com` ipv4.method `manual`**
----

. Configure the IPv6 settings:

* To use stateless address autoconfiguration (SLAAC), no action is required.

* To set a static IPv6 address, network mask, default gateway, and DNS server to the `bond0` connection, enter:
+
[literal,subs="+quotes,attributes"]
----
# **nmcli connection modify bond0 ipv6.addresses `2001:db8:1::1/64` ipv6.gateway `2001:db8:1::fffe` ipv6.dns `2001:db8:1::fffd` ipv6.dns-search `example.com` ipv6.method `manual`**
----

. Optional: If you want to set any parameters on the bond ports, use the following command:
+
[literal,subs="+quotes,attributes"]
----
# **nmcli connection modify bond0-port1 bond-port.<parameter> <value>**
----

. Configure that Red Hat Enterprise Linux enables all ports automatically when the bond is enabled:
+
[literal,subs="+quotes,attributes"]
----
# **nmcli connection modify bond0 connection.autoconnect-{NM_ports} 1**
----

. Activate the bridge:
+
[literal,subs="+quotes,attributes"]
----
# **nmcli connection up bond0**
----

.Verification

. Temporarily remove the network cable from the host.
+
Note that there is no method to properly test link failure events using software utilities. Tools that deactivate connections, such as nmcli, show only the bonding driver's ability to handle port configuration changes and not actual link failure events.

. Display the status of the bond:
+
[literal,subs="+quotes,attributes"]
----
# **cat /proc/net/bonding/bond0**
----
