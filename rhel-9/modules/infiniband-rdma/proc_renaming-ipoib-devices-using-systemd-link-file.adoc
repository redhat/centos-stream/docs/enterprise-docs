:_mod-docs-content-type: PROCEDURE

:experimental:

[id="renaming-ipoib-devices-using-systemd-link-file_{context}"]
= Renaming IPoIB devices by using systemd link file

[role="_abstract"]
By default, the kernel names Internet Protocol over InfiniBand (IPoIB) devices, for example, `ib0`, `ib1`, and so on. To avoid conflicts, create a `systemd` link file to create persistent and meaningful names such as `mlx4_ib0`.

.Prerequisites

* You have installed an InfiniBand device.

.Procedure

. Display the hardware address of the device `ib0`:
+
[literal,subs="+quotes,attributes"]
....
# **ip addr show ib0**

7: ib0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65520 qdisc fq_codel state UP group default qlen 256
    link/infiniband 80:00:0a:28:fe:80:00:00:00:00:00:00:f4:52:14:03:00:7b:e1:b1 brd 00:ff:ff:ff:ff:12:40:1b:ff:ff:00:00:00:00:00:00:ff:ff:ff:ff
    altname ibp7s0
    altname ibs2
    inet 172.31.0.181/24 brd 172.31.0.255 scope global dynamic noprefixroute ib0
       valid_lft 2899sec preferred_lft 2899sec
    inet6 fe80::f652:1403:7b:e1b1/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
....

. For naming the interface with MAC address `80:00:0a:28:fe:80:00:00:00:00:00:00:f4:52:14:03:00:7b:e1:b1` to `mlx4_ib0`, create the `/etc/systemd/network/70-custom-ifnames.link` file with following contents: 
+
[literal,subs="+quotes,attributes"]
....
**[Match]**
**MACAddress=__80:00:0a:28:fe:80:00:00:00:00:00:00:f4:52:14:03:00:7b:e1:b1__**

**[Link]**
**Name=__mlx4_ib0__**
....
+
This link file matches a MAC address and renames the network interface to the name set in the `Name` parameter.

.Verification

. Reboot the host:
+
[literal,subs="+quotes,attributes"]
....
# **reboot**
....

. Verify that the device with the MAC address you specified in the link file has been assigned to `mlx4_ib0`:
+
[literal,subs="+quotes,attributes"]
....
# **ip addr show mlx4_ib0**

7: mlx4_ib0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65520 qdisc fq_codel state UP group default qlen 256
    link/infiniband 80:00:0a:28:fe:80:00:00:00:00:00:00:f4:52:14:03:00:7b:e1:b1 brd 00:ff:ff:ff:ff:12:40:1b:ff:ff:00:00:00:00:00:00:ff:ff:ff:ff
    altname ibp7s0
    altname ibs2
    inet 172.31.0.181/24 brd 172.31.0.255 scope global dynamic noprefixroute mlx4_ib0
       valid_lft 2899sec preferred_lft 2899sec
    inet6 fe80::f652:1403:7b:e1b1/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
....

[role="_additional-resources"]
.Additional resources
* `systemd.link(5)` man page on your system
