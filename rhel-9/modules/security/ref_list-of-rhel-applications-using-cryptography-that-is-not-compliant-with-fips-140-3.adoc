:_mod-docs-content-type: REFERENCE
:experimental:

[id="ref_list-of-rhel-applications-using-cryptography-that-is-not-compliant-with-fips-140-3_{context}"]
= List of RHEL applications using cryptography that is not compliant with FIPS 140-3

To pass all relevant cryptographic certifications, such as FIPS 140-3, use libraries from the core cryptographic components set. These libraries, except from `libgcrypt`, also follow the {ProductShortName} system-wide cryptographic policies.

See the link:https://access.redhat.com/articles/3655361[RHEL core cryptographic components] Red&nbsp;Hat Knowledgebase article for an overview of the core cryptographic components, the information on how are they selected, how are they integrated into the operating system, how do they support hardware security modules and smart cards, and how do cryptographic certifications apply to them.

.List of RHEL 9 applications using cryptography that is not compliant with FIPS 140-3
Bacula:: Implements the CRAM-MD5 authentication protocol.
Cyrus SASL:: Uses the SCRAM-SHA-1 authentication method.
Dovecot:: Uses SCRAM-SHA-1.
Emacs:: Uses SCRAM-SHA-1.
FreeRADIUS:: Uses MD5 and SHA-1 for authentication protocols.
Ghostscript:: Custom cryptography implementation (MD5, RC4, SHA-2, AES) to encrypt and decrypt documents.
GRUB:: Supports legacy firmware protocols requiring SHA-1 and includes the `libgcrypt` library.
iPXE:: Implements TLS stack.
Kerberos:: Preserves support for SHA-1 (interoperability with Windows).
Lasso:: The `lasso_wsse_username_token_derive_key()` key derivation function (KDF) uses SHA-1.
MariaDB, MariaDB Connector:: The `mysql_native_password` authentication plugin uses SHA-1.
MySQL:: `mysql_native_password` uses SHA-1.
OpenIPMI:: The RAKP-HMAC-MD5 authentication method is not approved for FIPS usage and does not work in FIPS mode.
Ovmf (UEFI firmware), Edk2, shim:: Full cryptographic stack (an embedded copy of the OpenSSL library).
Perl:: Uses HMAC, HMAC-SHA1, HMAC-MD5, SHA-1, SHA-224,….
Pidgin:: Implements DES and RC4 ciphers.
PKCS #12 file processing (OpenSSL, GnuTLS, NSS, Firefox, Java):: All uses of PKCS #12 are not FIPS-compliant, because the Key Derivation Function (KDF) used for calculating the whole-file HMAC is not FIPS-approved. As such, PKCS #12 files are considered to be plain text for the purposes of FIPS compliance. For key-transport purposes, wrap PKCS #12 (.p12) files using a FIPS-approved encryption scheme.
Poppler:: Can save PDFs with signatures, passwords, and encryption based on non-allowed algorithms if they are present in the original PDF (for example MD5, RC4, and SHA-1).
PostgreSQL:: Implements Blowfish, DES, and MD5. A KDF uses SHA-1.
QAT Engine:: Mixed hardware and software implementation of cryptographic primitives (RSA, EC, DH, AES,…)
Ruby:: Provides insecure MD5 and SHA-1 library functions.
Samba:: Preserves support for RC4 and DES (interoperability with Windows).
Syslinux:: BIOS passwords use SHA-1.
SWTPM::	Explicitly disables FIPS mode in its OpenSSL usage.
Unbound:: DNS specification requires that DNSSEC resolvers use a SHA-1-based algorithm in DNSKEY records for validation.
Valgrind:: AES, SHA hashes.footnote:[Re-implements in software hardware-offload operations, such as AES-NI or SHA-1 and SHA-2 on ARM.]
zip:: Custom cryptography implementation (insecure PKWARE encryption algorithm) to encrypt and decrypt archives using a password.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/compliance/fips[FIPS - Federal Information Processing Standards] section on the link:https://access.redhat.com/en/compliance[Product compliance] Red&nbsp;Hat Customer Portal page
* link:https://access.redhat.com/articles/3655361[RHEL core cryptographic components] (Red&nbsp;Hat Knowledgebase)
