
:_mod-docs-content-type: REFERENCE

[id="ref_changes-to-system-management_{context}"]
= Notable changes to system management
////
Duplicate this module and rename to fit your topic grouping or SST. For example, "NetworkManager". Make sure to update the id attribute and title above.
Replace the template Consideration items with your actual Consideration items. Add as many items as needed.
////

.Data Encryption Standard (DES) algorithm is not available for net-snmp communication in Red Hat Enterprise Linux 9

In previous versions of RHEL, DES was used as an encryption algorithm for secure communication between net-snmp clients and servers. In RHEL 9, the DES algorithm isn’t supported by the OpenSSL library. The algorithm is marked as insecure and the DES support for net-snmp has therefore been removed.

.The ABRT tool has been removed

The Automatic Bug Reporting Tool (ABRT) for detecting and reporting application crashes is not available in RHEL 9.

As a replacement, use the `systemd-coredump` tool to log and store core dumps, which are automatically generated files after a program crashes.

.The `hidepid=n` mount option is not supported in RHEL 9 `systemd`

The mount option `hidepid=n`, which controls who can access information in `/proc/[pid]` directories, is not compatible with `systemd` infrastructure provided in RHEL 9.

In addition, using this option might cause certain services started by `systemd` to produce SELinux AVC denial messages and prevent other operations from being completed.

.The `dump` utility from the `dump` package has been removed

The `dump` utility used for backup of file systems has been deprecated in Red Hat Enterprise Linux 8 and is not available in RHEL 9.

In RHEL 9, Red Hat recommends using the `tar`, or `dd` as a backup tool for ext2, ext3, and ext4 file systems. The `dump` utility will be a part of the EPEL 9 repository.

Note that the `restore` utility from the `dump` package remains available and supported in RHEL 9 and is available as the `restore` package.

.RHEL 9 does not contain ReaR crontab

The `/etc/cron.d/rear` crontab in the `rear` package, which runs `rear mkrescue` after the disk layout changes, has been removed in RHEL 9.

If you relied on the `/etc/cron.d/rear` crontab to run `rear mkrescue`, you can manually configure periodic runs of ReaR instead.

[NOTE]
====
The `rear` package in RHEL contains the following examples for scheduling jobs:

* the `/usr/share/doc/rear/rear.cron` example crontab
* the `/usr/share/doc/rear/rear.{service,timer}` example systemd unit

Do not use these examples without site-specific modifications or other actions to take updated backups for system recovery. You must take regular backups in addition to re-creating the rescue image. The steps to take a backup depend on the local configuration. If you run the `rear mkrescue` command without taking an updated backup at the same time, the system recovery process would use a previous backup that might be inconsistent with the saved layout.
====
