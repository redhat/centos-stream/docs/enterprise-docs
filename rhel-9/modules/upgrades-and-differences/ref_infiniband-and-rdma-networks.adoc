:_module-type: REFERENCE

[id="ref_infiniband-and-rdma-networks_{context}"]
= Infiniband and RDMA networks

.The `ibdev2netdev` script has been removed from RHEL 9

`ibdev2netdev` was a helper utility that was able to display all the associations between network devices and Remote Direct Memory Access (RDMA) adapter ports. Previously, Red Hat was including `ibdev2netdev` in the `rdma-core` package. From Red Hat Enterprise Linux 9, `ibdev2netdev` has been removed and replaced by the `rdmatool` utility. Now, the `iproute` package includes `rdmatool`.

//BZ#1955402
