:_mod-docs-content-type: REFERENCE
[id="known-issues-from-8.8_{context}"]
= Known issues for the RHEL 8.8 to RHEL 9.2 upgrade

[role="_abstract"]
The following are Known Issues you may encounter when upgrading from RHEL 8.8 to RHEL 9.2.

* Network teaming currently does not work when the in-place upgrade is performed while Network Manager is disabled or not installed.

* If your RHEL 8 system uses a device driver that is provided by Red Hat but is not available in RHEL 9, `Leapp` inhibits the upgrade. However, if the RHEL 8 system uses a third-party device driver that `Leapp` does not have data for in the `/etc/leapp/files/device_driver_deprecation_data.json` file, `Leapp` does not detect such a driver and proceeds with the upgrade. Consequently, the system might fail to boot after the upgrade.

* If the name of a third-party package (not signed by Red Hat) installed on your system is the same as the name of a package provided by Red Hat, the in-place upgrade fails. To work around this problem, choose one of the following options prior to upgrading:
+
a. Remove the third-party package
b. Replace the third-party package with the package provided by Red Hat

* In RHEL 8, you can manage Virtual Data Optimizer (VDO) volumes by using either the VDO manager or the Logical Volume Manager (LVM). In RHEL 9, it is only possible to manage VDO volumes by using LVM. To continue using VDO-managed volumes on RHEL 9, import those volumes to LVM-managed VDO volumes before the upgrade. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deduplicating_and_compressing_logical_volumes_on_rhel/proc_importing-existing-vdo-volumes-to-lvm_deduplicating-and-compressing-logical-volumes-on-rhel[Importing existing VDO volumes to LVM].

* The in-place upgrade might fail on systems with Software Redundant Array of Independent Disks (RAID). (BZ#link:https://bugzilla.redhat.com/show_bug.cgi?id=1957192[1957192])

* During the in-place upgrade, the `Leapp` utility usually preserves the network interface controller (NIC) names between RHEL 8 and RHEL 9. However, on some systems, such as systems with network bonding, the NIC names might need to be updated between RHEL 8 and RHEL 9. On those systems, perform the following steps:
.. Set the `LEAPP_NO_NETWORK_RENAMING=1` environment variable to prevent the Leapp utility from incorrectly preserving the original RHEL 8 NIC names. 
.. Perform the in-place upgrade.
.. Verify that your network is working correctly. If needed, manually update the network configuration.
+
(BZ#link:https://bugzilla.redhat.com/show_bug.cgi?id=1919382[1919382]) 
////
//Replaced by KI below to cover more scenarios after 8.8 GA.
* The in-place upgrade might be stopped before the upgrade is performed because the `Leapp` utility incorrectly detects that there is not enough free disk space. If your system contains partitions formatted with the XFS filesystem without ftype attributes, you can work around this issue by changing the default size in the `LEAPP_OVL_SIZE` environment variable to account for, at minimum, the specified missing disk space inside the container. It is recommended to increase the default size to greater than the specified missing disk space to prevent repeated error messages. For example, if the `Leapp` utility detects that an additional 400 MB is needed, increase the default size from 2048 MB to at least 2500 MB. 
+
[NOTE]
====
This workaround can require a large amount of free space in the `/var` partition. 
====
If this workaround does not resolve the issue, or if your system does not contain these partitions without ftype attributes, contact Red Hat support. 
(BZ#link:https://bugzilla.redhat.com/show_bug.cgi?id=1832730[1832730])
////
* The in-place upgrade might fail if there is not enough available disk space. The error messages and logs might contain misleading or invalid information about the problem and resolution. For more information about resolving this issue, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/7016238[leapp fails with "There is not enough space on the file system hosting /var/lib/leapp directory to extract the packages"].
(BZ#link:https://bugzilla.redhat.com/show_bug.cgi?id=1832730[1832730], BZ#link:https://bugzilla.redhat.com/show_bug.cgi?id=2210300[2210300])

//[fixed in 9.2] * The in-place upgrade breaks networking on IBM Z with RoCE Express adapters. RHEL 9.0 uses Predictable Interface Names for RoCE Express adapters. These are different from the names available in the RHEL 8.6 distribution. Therefore, existing networking configurations of RoCE Express adapters will break with the RHEL 9.0 release if you perform an in-place upgrade from RHEL 8 to RHEL 9.

//[fixed in 9.2] * RHEL 8.7 introduced new features that are not available in the RHEL 9.0 release. Consequently, the system or applications might be broken if any of such features are required. To work around these problems, either update the system setup to be compatible with RHEL 9.0, or, after upgrading to RHEL 9.0, manually update the system to RHEL 9.1, which contains the missing features.

* If your system boots by using BIOS, the in-place upgrade fails when upgrading the GRUB boot loader if the boot disk's embedding area does not contain enough space for the core image installation. This results in a broken system, and can occur when the disk has been partitioned manually, for example using the RHEL 6 `fdisk` utility. To verify whether this issue affects you, perform the following steps:
.. Determine which sector starts the first partition on the disk with the installed boot loader:
+
[subs="quotes,attributes"]
----
# *fdisk -l*
----
+
The standard partitioning, which ensures enough space for the core image, starts on sector 2048. 
.. Determine whether the starting sector provides enough space. The RHEL 9.0 core image requires at least 36 KiB. For example, if the sector size is the standard 512 bytes, then starting on sector 73 or lower would not provide enough space. 
+
[NOTE]
====
The RHEL 9 core image might be larger than 36 KiB and require a higher starting sector. Always verify how much space the current RHEL 9 core requires.
====
.. If the embedding area does not contain enough storage space, perform a fresh installation of the RHEL 9 system instead of performing an in-place upgrade.
+
(BZ#link:https://bugzilla.redhat.com/show_bug.cgi?id=2181380[2181380])

* After the in-place upgrade, SSH keys are no longer auto-generated if the system meets the following conditions:
** The system is on a cloud.
** The cloud-init package is installed.
** The ssh_genkeytypes configuration is set to ~ in the /etc/cloud/cloud.cfg file, which is the default.
+
This issue prevents the system from connecting by using SSH if the original keys have been removed. For more information on preventing this issue, see the Red Hat  Knowledgebase solution link:https://access.redhat.com/solutions/6988034[Unable to SSH to new Virtual Machine after upgrading the template to RHEL 8.7 or 9].
(BZ#link:https://bugzilla.redhat.com/show_bug.cgi?id=2210012[2210012])

* If too many file systems are mounted, the pre-upgrade process might fail with the following error message:
+
[subs="quotes,attributes"]
----
OperationalError: unable to open database file
----
+
If this issue occurs, complete the following steps: 
+
. Unmount any file systems that are not related to the system partitions and are not needed during the upgrade process. 
. Comment out the unmounted file systems’ entries in the `/etc/fstab` file to prevent them from being mounted during the upgrade process. 
. Restore the original file system configuration after the upgrade.
+
(link:https://issues.redhat.com/browse/RHEL-3320[RHEL-3320])

* In-place upgrades using Red Hat Update Infrastructure (RHUI) on Amazon Web Services (AWS) with the ARM architecture are currently experiencing issues. Use the Red Hat Subscription Manager (RHSM) instead. (link:https://issues.redhat.com/browse/RHEL-38909[RHEL-38909])

* In-place upgrades on Microsoft Azure by using RHUI virtual machines might fail. To prevent this issue, verify that you have installed the `leapp-rhui-azure-1.0.0-10.el8.noarch` package version before upgrading.

* By default, the `logrotate` is not active after the upgrade. The `logrotate` was previously managed by `cron` in RHEL 8 and earlier. In RHEL 9, it is managed by `systemd`. To activate `logrotate` persistently, run the following command after the upgrade:
+
[subs="+quotes,attributes"]
----
# *systemctl enable --now logrotate.timer*
----

* If you use an HTTP proxy, Red Hat Subscription Manager must be configured to use such a proxy, or the `subscription-manager` command must be executed with the `--proxy <hostname>` option. Otherwise, an execution of the `subscription-manager` command fails. If you use the `--proxy` option instead of the configuration change, the upgrade process fails because [application]`Leapp` is unable to detect the proxy. To prevent this problem from occurring, manually edit the `rhsm.conf` file. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/57669[How to configure HTTP Proxy for Red Hat Subscription Management]. (BZ#link:https://bugzilla.redhat.com/show_bug.cgi?id=1689294[1689294])

* For systems that require a proxy to access RHEL 9 content, you usually need to configure the use of the proxy by DNF in the `/etc/dnf/dnf.conf` configuration file. If the current DNF configuration is not compatible with the DNF version on the target system, specify the valid target configuration in the `/etc/leapp/files/dnf.conf` configuration file. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/node/7073762[How does Leapp work with a proxy?]

* The upgrade might fail with a `MountError` message when all of the following conditions are met: 	
** A mount point that is defined in `/etc/fstab` contains underscore characters.
** Another mount point defined in `/etc/stab` has the same name if the underscore character is replaced with a forward slash.
+
For example, having both `/var/tmp` and `/var_tmp` mount points defined in `/etc/fstab` causes the upgrade to fail. 
+
To prevent this problem, unmount the mount point that contains underscore characters, and comment that mount point out from the `/etc/fstab` file before the upgrade. You can restore the configuration after the upgrade. 
+
(link:https://issues.redhat.com/browse/RHEL-62737[RHEL-62737])
