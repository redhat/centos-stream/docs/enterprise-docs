
:_module-type: REFERENCE

[id="ref_considerations-security-selinux_{context}"]
= SELinux

.Support for disabling SELinux through `/etc/selinux/config` has been removed
// https://bugzilla.redhat.com/show_bug.cgi?id=1935783
With the RHEL 9.0 release, support for disabling SELinux through the `SELINUX=disabled` option in the `/etc/selinux/config` file has been removed from the kernel. When you disable SELinux only through `/etc/selinux/config`, the system starts with SELinux enabled but with no policy loaded, and SELinux security hooks remain registered in the kernel. This means that SELinux disabled by using `/etc/selinux/config` still requires some system resources, and you should instead disable SELinux by using the kernel command line in all performance-sensitive scenarios.

Furthermore, the Anaconda installation program and the corresponding man pages have been updated to reflect this change. This change also enables read-only-after-initialization protection for the Linux Security Module (LSM) hooks.

If your scenario requires disabling SELinux, add the `selinux=0` parameter to your kernel command line.

See the link:https://fedoraproject.org/wiki/Changes/Remove_Support_For_SELinux_Runtime_Disable[Remove support for SELinux run-time disable] Fedora wiki page for more information.

.Additional services confined in the SELinux policy
// https://bugzilla.redhat.com/show_bug.cgi?id=2080443
The RHEL 9.3 release added additional rules to the SELinux policy that confine the following `systemd` services:

* `qat`
* `systemd-pstore`
* `boothd`
* `fdo-manufacturing-server`
* `fdo-rendezvous-server`
* `fdo-client-linuxapp`
* `fdo-owner-onboarding-server`

As a result, these services do not run with the `unconfined_service_t` SELinux label anymore, and run successfully in SELinux enforcing mode.

.The `glusterd` SELinux module moved to a separate `glusterfs-selinux` package
// https://issues.redhat.com/browse/RHEL-1548
With this update, the `glusterd` SELinux module is maintained in the separate `glusterfs-selinux` package. The module is therefore no longer part of the `selinux-policy` package. For any actions that concern the `glusterd` module, install and use the `glusterfs-selinux` package.
