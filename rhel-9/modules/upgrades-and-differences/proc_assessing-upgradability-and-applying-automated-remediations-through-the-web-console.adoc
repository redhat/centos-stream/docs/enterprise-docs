:_mod-docs-content-type: PROCEDURE
// Module included in the following modules (= context is still "upgrading-from-rhel-8-to-rhel-9"):
//

:experimental:

[id="assessing-upgradability-and-applying-automated-remediations-through-the-web-console_{context}"]
= Assessing upgradability of RHEL 8.10 to RHEL 9.4 and 9.5 and applying automated remediations through the web console

[role="_abstract"]
Identify potential problems in the pre-upgrade phase before the upgrade and apply automated remediations by using the web console.


.Prerequisites

* You have completed the steps listed in
ifdef::upgrading-to-rhel-9[]
xref:assembly_preparing-for-the-upgrade_{context}[Preparing for the upgrade].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#assembly_preparing-for-the-upgrade_upgrading-from-rhel-8-to-rhel-9[Preparing for the upgrade].
endif::[]

* You are logged in to root with the unconfined SELinux role. If you are using `sudo`, you must use the `-r unconfined_r -t unconfined_t` options when running each leapp command, for example: 
+
[subs="quotes,attributes"]
----
$ *sudo -r unconfined_r -t unconfined_t leapp preupgrade*
----

.Procedure
. Install the `cockpit-leapp` plug-in:
+
[subs="quotes,attributes"]
----
# *dnf install cockpit-leapp*
----
+

. Log in to the web console as `root` or as a user that has permissions to enter administrative commands with `sudo`. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_systems_using_the_rhel_8_web_console/index[Managing systems using the RHEL 8 web console] for more information about the web console.


. On your RHEL 8 system, perform the pre-upgrade phase either from the command line or from the web console terminal:
+
[subs="quotes,attributes"]
----
# *leapp preupgrade*
----
+
* If you are using link:https://access.redhat.com/articles/4977891#repos[custom repositories] from the `/etc/yum.repos.d/` directory for the upgrade, enable the selected repositories as follows:
+
[subs="+quotes,attributes"]
----
# *leapp preupgrade --enablerepo _<repository_id1>_ --enablerepo _<repository_id2>_ ...*
----

* If you are link:https://access.redhat.com/articles/4977891#upgrade-without-rhsm-rhel9[upgrading without RHSM] or by using RHUI, add the `--no-rhsm` option.

* If you have an link:https://access.redhat.com/articles/rhel-eus[Extended Upgrade Support (EUS)], Advanced Update Support (AUS), or link:https://access.redhat.com/solutions/3082481[Update Services for SAP Solutions (E4S)] subscription, add the `--channel _<channel>_` option. Replace _<channel>_ with the channel name, for example, `eus`, `aus`, or `e4s`. 
Note that SAP HANA customers should perform the in-place upgrade using the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_for_sap_solutions/9/html-single/upgrading_sap_environments_from_rhel_8_to_rhel_9/index[Upgrading SAP environments from RHEL 8 to RHEL 9] guide.

* If you are using RHEL for Real Time or the Real Time for Network Functions Virtualization (NFV) in your Red Hat OpenStack Platform, enable the deployment by using the `--enablerepo` option. For example:
+
[subs="+quotes,attributes"]
----
# *leapp preupgrade --enablerepo rhel-9-for-x86_64-rt-rpms*
----
+
For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/14/html/advanced_overcloud_customization/realtime-compute[Configuring Real-Time Compute].

. In the web console, select *Upgrade Report* from the navigation menu to review all reported problems. *Inhibitor* problems prevent you from upgrading until you have resolved them. To view a problem in detail, select the row to open the Detail pane.
+
[id="image_web_console_report"]
.In-place upgrade report in the web console
image::leapp-web-console-report.png[In-place upgrade report in the web console]
+
The report contains the following risk factor levels:

High:: Very likely to result in a deteriorated system state.
Medium:: Can impact both the system and applications.
Low:: Should not impact the system but can have an impact on applications.
Info:: Informational with no expected impact to the system or applications.

. In certain configurations, the `Leapp` utility generates true or false questions that you must answer manually. If the Upgrade Report contains a *Missing required answers in the answer file* row, complete the following steps:
.. Select the *Missing required answers in the answer file* row to open the *Detail* pane. The default answer is stated at the end of the remediation command.
.. To confirm the default answer, select *Add to Remediation Plan* to execute the remediation later or *Run Remediation* to execute the remediation immediately. 
.. To select the non-default answer instead, execute the `leapp answer` command in the terminal, specifying the question you are responding to and your confirmed answer.
+
[subs="+quotes,attributes"]
----
# *leapp answer --section _<question_section>_._<field_name>_=_<answer>_*
----
+
For example, to confirm a `True` response to the question *Are all VDO devices, if any, successfully converted to LVM management?*, execute the following command:
+
[subs="quotes,attributes"]
----
# *leapp answer --section check_vdo.confirm=True*
----
+
[NOTE]
====
You can also manually edit the `/var/log/leapp/answerfile` file, uncomment the confirm line of the file by deleting the `#` symbol, and confirm your answer as `True` or `False`. For more information, see the 
ifdef::upgrading-to-rhel-9[]
xref:example-leapp_answerfile[Leapp answerfile example].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#troubleshooting_upgrading-from-rhel-8-to-rhel-9[Leapp answerfile example in the Troubleshooting tips section].
endif::[]
====

. Some problems have remediation commands that you can run to automatically resolve the problems. You can run remediation commands individually or all together in the remediation command.
.. To run a single remediation command, open the *Detail* pane for the problem and click *Run Remediation*. 
.. To add a remediation command to the remediation plan, open the *Detail* pane for the problem and click *Add to Remediation Plan*.
+
[id="image_detail_pane"]
.Detail pane
image::leapp-detail-pane.png[Detail pane,width=53%]

.. To run the remediation plan containing all added remediation commands, click the *Remediation plan* link in the top right corner above the report. Click *Execute Remediation Plan* to execute all listed commands.

. After reviewing the report and resolving all reported problems, repeat steps 3-7 to rerun the report to verify that you have resolved all critical issues.

