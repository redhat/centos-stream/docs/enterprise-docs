
:_mod-docs-content-type: REFERENCE

[id="ref_changes-to-the-web-console_{context}"]
= Changes to the RHEL web console

.Remote root login disabled on new installations of RHEL 9.2 and later
Due to security reasons, on new installations of RHEL 9.2 and newer, it is not possible to connect to the web console from a remote machine as a root user.

To enable the remote root login:

. As a root user, open the `/etc/cockpit/disallowed-users` file in a text editor.
. Remove the `root` user line from the file.
. Save your changes.

