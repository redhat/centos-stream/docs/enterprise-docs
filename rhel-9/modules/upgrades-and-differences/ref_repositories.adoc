////
Base the file name and the ID on the module title. For example:
* file name: ref-my-reference-a.adoc
* ID: [id="ref-my-reference-a_{context}"]
* Title: = My reference A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix ref- or ref_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: REFERENCE

[id="ref_repositories_{context}"]
= Repositories
//draft: https://docs.google.com/document/d/1pyMhKEkc--ieaJUkg6bL8Z08Gv1jFkDPy3FaCDP7Xhc/edit#heading=h.mhq9qqyi2dt

[role="_abstract"]
Red Hat Enterprise Linux 9 is distributed through two main repositories:

* BaseOS
* AppStream

Both repositories are required for a basic RHEL installation, and are available with all RHEL subscriptions.

Content in the BaseOS repository is intended to provide the core set of the underlying OS functionality that provides the foundation for all installations. This content is available in the RPM format and is subject to support terms similar to those in previous releases of RHEL. For more information, see the link:https://access.redhat.com/node/490973[Scope of Coverage Details] document.

Content in the AppStream repository includes additional user-space applications, runtime languages, and databases in support of the varied workloads and use cases.

In addition, the CodeReady Linux Builder repository is available with all RHEL subscriptions. It provides additional packages for use by developers. Packages included in the CodeReady Linux Builder repository are unsupported.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/package_manifest/index[Package manifest]



