:_mod-docs-content-type: REFERENCE
[id="troubleshooting-tips_{context}"]
= Troubleshooting tips

[role="_abstract"]
You can refer to the following troubleshooting tips.

*Pre-upgrade phase*

* Verify that your system meets all conditions listed in
ifdef::upgrading-to-rhel-9[]
xref:planning-an-upgrade-to-rhel-9_upgrading-from-rhel-8-to-rhel-9[Planning an upgrade].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/upgrading_from_rhel_8_to_rhel_9/index#planning-an-upgrade-to-rhel-9_upgrading-from-rhel-8-to-rhel-9[Planning an upgrade].
endif::[]

* Make sure you have followed all steps described in
ifdef::upgrading-to-rhel-9[]
xref:assembly_preparing-for-the-upgrade_upgrading-from-rhel-8-to-rhel-9[Preparing for the upgrade]
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/upgrading_from_rhel_8_to_rhel_9/index#assembly_preparing-for-the-upgrade_upgrading-from-rhel-9-to-rhel-8[Preparing for the upgrade]
endif::[]
for example, your system does not use more than one Network Interface Card (NIC) with a name based on the prefix used by the kernel (`eth`).
* Make sure you have answered all questions required by [application]`Leapp` in the `/var/log/leapp/answerfile` file. If any answers are missing, [application]`Leapp` inhibits the upgrade. For example:
+
** Are there no VDO devices on the system?
+

+
* Make sure you have resolved all problems identified in the pre-upgrade report, located at `/var/log/leapp/leapp-report.txt`. To achieve this, you can also use the web console, as described in
ifdef::upgrading-to-rhel-9[]
xref:assessing-upgradability-and-applying-automated-remediations-through-the-web-console_upgrading-from-rhel-8-to-rhel-9[Assessing upgradability and applying automated remediations through the web console].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/upgrading_from_rhel_8_to_rhel_9/index#assessing-upgradability-and-applying-automated-remediations-through-the-web-console_upgrading-from-rhel-8-to-rhel-9[Assessing upgradability and applying automated remediations through the web console].
endif::[]


[id="example-leapp_answerfile"]
//TODO: fix {context}
.Leapp answerfile

====

The following is an example of an unedited `/var/log/leapp/answerfile` file that has one unanswered question:

[subs="quotes,attributes"]
----
[check_vdo]
# Title:              None
# Reason:             Confirmation
# ============================= check_vdo.confirm =============================
# Label:              Are all VDO devices, if any, successfully converted to LVM management?
# Description:        Enter True if no VDO devices are present on the system or all VDO devices on the system have been successfully converted to LVM management. Entering True will circumvent check of failures and undetermined devices. Recognized VDO devices that have not been converted to LVM management can still block the upgrade despite the answer.All VDO devices must be converted to LVM management before upgrading.
# Reason:             To maximize safety all block devices on a system that meet the criteria as possible VDO devices are checked to verify that, if VDOs, they have been converted to LVM management. If the devices are not converted and the upgrade proceeds the data on unconverted VDO devices will be inaccessible. In order to perform checking the 'vdo' package must be installed. If the 'vdo' package is not installed and there are any doubts the 'vdo' package should be installed and the upgrade process re-run to check for unconverted VDO devices. If the check of any device fails for any reason an upgrade inhibiting report is generated. This may be problematic if devices are dynamically removed from the system subsequent to having been identified during device discovery. If it is certain that all VDO devices have been successfully converted to LVM management this dialog may be answered in the affirmative which will circumvent block device checking.
# Type:               bool
# Default:            None
# Available choices: True/False
# Unanswered question. Uncomment the following line with your answer
# confirm =



----

The `Label` field specifies the question that requires an answer. In this example, the question is *Are all VDO devices, if any, successfully converted to LVM management?*

To answer the question, uncomment the last line and enter an answer of `True` or `False`. In this example, the selected answer is `True`:

[subs="quotes,attributes"]
----
[check_vdo]
...
# Available choices: True/False
# Unanswered question. Uncomment the following line with your answer
confirm = *True*
----

====

*Download phase*

* If a problem occurs during downloading RPM packages, examine transaction debug data located in the `/var/log/leapp/dnf-debugdata/` directory.
+
[NOTE]
====
The `/var/log/leapp/dnf-debugdata/` directory is empty or does not exist if no transaction debug data was produced. This might occur when the required repositories are not available.
====

*Initramfs phase*

* During this phase, potential failures redirect you to the Dracut shell. Check the Journal log:
+
[subs="quotes,attributes"]
----
# *journalctl*
----
+
Alternatively, restart the system from the Dracut shell using the [command]`reboot` command and check the `/var/log/leapp/leapp-upgrade.log` file.

*Post-upgrade phase*

* If your system seems to be successfully upgraded but booted with the old RHEL 8 kernel, restart the system and check the kernel version of the default entry in GRUB.
* Make sure you have followed the recommended steps in
ifdef::upgrading-to-rhel-9[]
xref:verifying-the-post-upgrade-state_upgrading-from-rhel-8-to-rhel-9[Verifying the post-upgrade state].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/upgrading_from_rhel_8_to_rhel_9/index#verifying-the-post-upgrade-state_upgrading-from-rhel-8-to-rhel-9[Verifying the post-upgrade state].
endif::[]
* If your application or a service stops working or behaves incorrectly after you have switched SELinux to enforcing mode, search for denials using the [application]*ausearch*, [application]*journalctl*, or [application]*dmesg* utilities:
+
[subs="quotes,attributes"]
----
# *ausearch -m AVC,USER_AVC -ts boot*
# *journalctl -t setroubleshoot*
# *dmesg | grep -i -e selinux -e type=1400*
----
+
--
The most common problems are caused by incorrect labeling. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/troubleshooting-problems-related-to-selinux_using-selinux[Troubleshooting problems related to SELinux] for more details.
--
