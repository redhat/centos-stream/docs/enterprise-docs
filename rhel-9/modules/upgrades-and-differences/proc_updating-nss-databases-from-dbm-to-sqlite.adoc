:_module-type: PROCEDURE

[id="proc_updating-nss-databases-from-dbm-to-sqlite_{context}"]
= Updating NSS databases from DBM to SQLite

[role="_abstract"]
Many applications automatically convert the NSS database format from DBM to SQLite after you set the `NSS_DEFAULT_DB_TYPE` environment variable to the `sql` value on the system. You can ensure that all databases are converted by using the `certutil` tool.

[NOTE]
====
Convert your NSS databases stored in the DBM format before you upgrade to RHEL 9. In other words, perform the following steps on RHEL systems (6, 7, and 8) from which you want to upgrade to RHEL 9.
====

.Prerequisites

* The `nss-tools` package is installed on your system.

.Procedure

. Set `NSS_DEFAULT_DB_TYPE` to `sql` on the system:
+
[subs="quotes"]
----
# *export NSS_DEFAULT_DB_TYPE=sql*
----

. Use the conversion command in every directoryfootnote:[RHEL contains a system-wide NSS database in the `/etc/pki/nssdb` directory. Other locations depend on applications you use. For example, Libreswan stores its database in the `/etc/ipsec.d/` directory and Firefox uses the `/home/&lt;username&gt;/.mozilla/firefox/` directory.] that contains NSS database files in the DBM format, for example:
+
[subs="quotes"]
----
# *certutil -K -X -d _/etc/ipsec.d/_*
----
+
Note that you have to provide a password or a path to a password file as a value of the `-f` option if your database file is password-protected, for example:
+
[subs="quotes"]
----
# *certutil -K -X -f _/etc/ipsec.d/nsspassword_ -d _/etc/ipsec.d/_*
----

[role="_additional-resources"]
.Additional resources
* `certutil(1)` man page on your system
