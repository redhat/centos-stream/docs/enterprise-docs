
:_mod-docs-content-type: PROCEDURE

[id="proc_preparing-a-satellite-system-for-the-upgrade_{context}"]
= Preparing a Satellite-registered system for the upgrade

[role="_abstract"]
This procedure describes the steps that are necessary to prepare a system that is registered to Satellite for the upgrade to RHEL 9. There steps are performed on the Satellite Server.


[IMPORTANT]
====
Users on Satellite systems must complete the preparatory steps described both in this procedure and in
ifdef::upgrading-to-rhel-9[]
xref:preparing-a-rhel-8-system-for-the-upgrade_{context}[Preparing a RHEL 8 system for the upgrade].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#preparing-a-rhel-8-system-for-the-upgrade_upgrading-from-rhel-8-to-rhel-9[Preparing a RHEL 8 system for the upgrade].
endif::[]
====

.Prerequisites
* You have administrative privileges for the Satellite Server.

.Procedure

. Verify that Satellite is on a version in full or maintenance support. For more information, see link:https://access.redhat.com/support/policy/updates/satellite[Red Hat Satellite Product Life Cycle].

. Import a subscription manifest with RHEL 9 repositories into Satellite Server. For more information, see the Managing Red Hat Subscriptions chapter in the Managing Content Guide for the particular version of link:https://access.redhat.com/documentation/en-us/red_hat_satellite/[Red Hat Satellite], for example, for link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/managing_content/managing_red_hat_subscriptions_content-management[version 6.12].

. Enable and synchronize all required RHEL 8 and RHEL 9 repositories on the Satellite Server with the latest updates for the source and target OS versions. Required repositories must be available in the Content View and enabled in the associated activation key.
+
[NOTE]
====
For RHEL 9 repositories, enable the target OS version, for example, RHEL 9.4, of each repository. If you enable only the RHEL 9 version of the repositories, the in-place upgrade is inhibited.
====
+
For example, for the Intel architecture without an Extended Update Support (EUS) subscription, enable at minimum the following repositories:
+
* Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)
+
rhel-8-for-x86_64-appstream-rpms
+
x86_64 _<source_os_version>_
* Red Hat Enterprise Linux 8 for x86_64 - BaseOS (RPMs)
+
rhel-8-for-x86_64-baseos-rpms
+
x86_64 _<source_os_version>_
+
* Red Hat Enterprise Linux 9 for x86_64 - AppStream (RPMs)
+
rhel-9-for-x86_64-appstream-rpms
+
x86_64 _<target_os_version>_
* Red Hat Enterprise Linux 9 for x86_64 - BaseOS (RPMs)
+
rhel-9-for-x86_64-baseos-rpms
+
x86_64 _<target_os_version>_
+
Replace _<source_os_version>_ and _<target_os_version>_ with the source OS version and target OS version respectively, for example, 8.10 and 9.4.
+
For other architectures, see
ifdef::upgrading-to-rhel-9[]
xref:appendix-rhel-8-repositories_{context}[RHEL 8 repositories] and xref:appendix_rhel-9-repositories_{context}[RHEL 9 repositories].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#appendix-rhel-8-repositories_upgrading-from-rhel-8-to-rhel-9[RHEL 8 repositories] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#appendix-rhel-9-repositories_upgrading-from-rhel-8-to-rhel-9[RHEL 9 repositories].
endif::[]
+
For more information, see the _Importing Content_ chapter in the _Managing Content Guide_ for the particular version of link:https://access.redhat.com/documentation/en-us/red_hat_satellite/[Red Hat Satellite], for example, for link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/managing_content/importing_content_content-management[version 6.12].

. Attach the content host to a Content View containing the required RHEL 8 and RHEL 9 repositories.
+
For more information, see the _Managing Content Views_ chapter in the _Managing Content Guide_ for the particular version of link:https://access.redhat.com/documentation/en-us/red_hat_satellite/[Red Hat Satellite], for example, for link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.12/html/managing_content/managing_content_views_content-management[version 6.12].

.Verification

. Verify that the correct RHEL 8 and RHEL 9 repositories have been added to the correct Content View on Satellite Server.
.. In the Satellite web UI, navigate to *Content > Lifecycle > Content Views* and click the name of the Content View.
.. Click the *Repositories* tab and verify that the repositories appear as expected.
+
[NOTE]
====
You can also verify that the repositories have been added to the Content View using the following commands:
[subs="quotes,attributes"]
----
# *hammer repository list --search 'content_label ~ rhel-8' --content-view _&lt;content_view_name&gt;_ --organization _&lt;organization&gt;_ --lifecycle-environment _&lt;lifecycle_environment&gt;_*
# *hammer repository list --search 'content_label ~ rhel-9' --content-view _&lt;content_view_name&gt;_ --organization _&lt;organization&gt;_ --lifecycle-environment _&lt;lifecycle_environment&gt;_*
----

Replace _&lt;content_view_name&gt;_ with the name of the Content View, _&lt;organization&gt;_ with the organization, and _<lifecycle_environement_> with the name of the lifecycle environment..
====
. Verify that the correct RHEL 9 repositories are enabled in the activation key associated with the Content View:
.. In Satellite web UI navigate to *Content > Lifecycle > Activation Keys* and click the name of the activation key.
.. Click the *Repository Sets* tab and verify that the statuses of the required repositories are `Enabled`.
. . Verify that all expected RHEL 8 repositories are enabled in the host. For example:
+
[subs="quotes,attributes"]
----
# *subscription-manager repos --list-enabled | grep "^Repo ID"*
Repo ID:   rhel-8-for-x86_64-baseos-rpms
Repo ID:   rhel-8-for-x86_64-appstream-rpms
----
