:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// enterprise/assemblies/assembly_upgrading-from-rhel-8-to-rhel-9.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_preparing-a-rhel-8-system-for-the-upgrade.adoc[leveloffset=+1]

[id="preparing-a-rhel-8-system-for-the-upgrade_{context}"]
= Preparing a RHEL 8 system for the upgrade

[role="_abstract"]
This procedure describes the steps that are necessary before performing an in-place upgrade to RHEL 9 by using the [application]`Leapp` utility.

If you do not plan to use Red Hat Subscription Manager (RHSM) during the upgrade process, follow instructions in link:https://access.redhat.com/articles/4977891#upgrade-without-rhsm-rhel9[Upgrading to RHEL 9 without Red Hat Subscription Manager].

.Prerequisites

* The system meets conditions listed in
ifdef::upgrading-to-rhel-9[]
xref:planning-an-upgrade-to-rhel-9_{context}[Planning an upgrade].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#planning-an-upgrade-to-rhel-9_upgrading-from-rhel-8-to-rhel-9[Planning an upgrade].
endif::[]
* If the system has been previously upgraded from RHEL 7 to RHEL 8, ensure that all required post-upgrade steps have been completed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/upgrading_from_rhel_7_to_rhel_8/index#performing-post-upgrade-tasks-rhel-7-to-rhel-8_upgrading-from-rhel-7-to-rhel-8[Performing post-upgrade tasks] in the Upgrading from RHEL 7 to RHEL 8 guide.

.Procedure

. Optional: Review the best practices in link:https://access.redhat.com/articles/7012979[The best practices and recommendations for performing RHEL Upgrade using Leapp] Knowledgebase article.

. Ensure your system has been successfully registered to the Red Hat Content Delivery Network (CDN) or Red Hat Satellite by using the Red Hat Subscription Manager.

. If you have registered your system to Satellite Server, complete the steps in
ifdef::upgrading-to-rhel-9[]
xref:proc_preparing-a-satellite-system-for-the-upgrade_{context}[Preparing a Satellite-registered system for the upgrade]
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#proc_preparing-a-satellite-system-for-the-upgrade_upgrading-from-rhel-8-to-rhel-9[Preparing a Satellite-registered system for the upgrade]
endif::[]
to ensure that your system meets the requirements for the upgrade.
+
[IMPORTANT]
====
If your system is registered to Satellite Server, you must complete the steps in 
ifdef::upgrading-to-rhel-9[]
xref:proc_preparing-a-satellite-system-for-the-upgrade_{context}[Preparing a Satellite-registered system for the upgrade]
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#proc_preparing-a-satellite-system-for-the-upgrade_upgrading-from-rhel-8-to-rhel-9[Preparing a Satellite-registered system for the upgrade]
endif::[]
for the upgrade before proceeding with the steps in this procedure to prevent issues from occurring.
====

. Optional: Unmount non-system OS file systems that are not required for the upgrade, such as file systems containing only data files unrelated to the system itself, and comment them out from the `/etc/fstab` file. This can reduce the amount of time needed for the upgrade process and prevent potential issues related to third-party applications that are not migrated properly during the upgrade by custom or third-party actors.

. Verify that the system is subscribed using subscription-manager:
.. If your system is registered by using an account with link:https://access.redhat.com/articles/simple-content-access[Simple Content Access] (SCA) enabled, verify that the `Content Access Mode is set to Simple Content Access` message appears:
+
[subs="quotes,attributes"]
----
# *subscription-manager status*
+-------------------------------------------+
   System Status Details
+-------------------------------------------+
Overall Status: Disabled
Content Access Mode is set to Simple Content Access. This host has access to content, regardless of subscription status.
System Purpose Status: Disabled
----
.. If your system is registered by using an account with SCA disabled, verify that the Red Hat Linux Server subscription is attached, the product name is `Server`, and the status is `Subscribed`. For example:
+
[subs="quotes,attributes"]
----
# *subscription-manager list --installed*
+-------------------------------------------+
    	  Installed Product Status
+-------------------------------------------+
Product Name:   Red Hat Enterprise Linux for x86_64
Product ID:     479
Version:        8.10
Arch:           x86_64
Status:         Subscribed
----

. Ensure you have appropriate repositories enabled. The following command enables the Base and AppStream repositories for the 64-bit Intel architecture; for other architectures, see
ifdef::upgrading-to-rhel-9[]
xref:appendix-rhel-8-repositories_{context}[RHEL 8 repositories].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#appendix-rhel-8-repositories_upgrading-from-rhel-8-to-rhel-9[RHEL 8 repositories].
endif::[]
+
[subs="quotes,attributes"]
----
# *subscription-manager repos --enable  rhel-8-for-x86_64-baseos-rpms --enable rhel-8-for-x86_64-appstream-rpms*
----
+
[NOTE]
====
Optionally, you can enable the CodeReady Linux Builder (also known as Optional) or Supplementary repositories. For more information about repository IDs, see
ifdef::upgrading-to-rhel-9[]
xref:appendix-rhel-8-repositories_{context}[RHEL 8 repositories].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#appendix-rhel-8-repositories_upgrading-from-rhel-8-to-rhel-9[RHEL 8 repositories].
endif::[]
For more information about the content of these repositories, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/package_manifest[Package manifest].
====

. Set the system release version:
.. For systems subscribed using RHSM, lock the system to the desired source OS version:
+
[subs="+quotes,attributes"]
----
# *subscription-manager release --set _<source_os_version>_*
----
.. If you are upgrading by using Red Hat Update Infrastructure (RHUI) on a public cloud, set the  expected system release version manually:
+
[subs="+quotes,attributes"]
----
# *rhui-set-release --set _<source_os_version>_*
----
+
[IMPORTANT]
====
If the `rhui-set-release` command is not available on your system, you can set the expected system release version by updating the `/etc/dnf/vars/release` file:
[subs="+quotes,attributes"]
----
# *echo "_<source_os_version>_" > /etc/dnf/vars/releasever*
----
====
+
Replace _source_os_version_ with the source OS version, for example `8.8`.

. Optional: To use custom repositories, see the link:https://access.redhat.com/articles/4977891#repos-config[Configuring custom repositories] Knowledgebase article.

. If you use the `dnf versionlock` plugin to lock packages to a specific version, clear the lock by running:
+
[subs="quotes,attributes"]
----
# *dnf versionlock clear*
----
+
For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/98873[How to restrict {PackageManagerCommand} to install or upgrade a package to a fixed specific package version?].

. If you are upgrading by using Red Hat Update Infrastructure (RHUI) on a public cloud, enable required RHUI repositories and install required RHUI packages to ensure your system is ready for upgrade:
+
.. For AWS:
+
[subs="quotes,attributes"]
----
# *dnf config-manager --set-enabled rhui-client-config-server-8*
# *dnf -y install leapp-rhui-aws*
----
+
.. For Microsoft Azure:
+
[subs="quotes,attributes"]
----
# *dnf config-manager --set-enabled rhui-microsoft-azure-rhel8*
# *dnf -y install rhui-azure-rhel8 leapp-rhui-azure*
----
+
////
[NOTE]
====
If you locked the Azure virtual machine (VM) to a minor release, remove the version lock. For more information, see link:https://docs.microsoft.com/en-us/azure/virtual-machines/workloads/redhat/redhat-rhui#switch-a-rhel-7x-vm-back-to-non-eus-remove-a-version-lock[Switch a RHEL 7.x VM back to non-EUS].
====
////
.. For Google Cloud Platform, follow the link:https://access.redhat.com/articles/6981918[Leapp RHUI packages for Google Cloud Platform (GCP)] Knowledgebase article.

. Install the [application]`Leapp` utility:

+
[subs="quotes,attributes"]
----
# *dnf install leapp-upgrade*
----
+
Note that you need up-to-date `leapp` and `leapp-repository` packages, which differ based on which version of RHEL 8 is installed. The following are the current up-to-date package versions:
+
[NOTE]
====
The `leapp-repository` package contains the `leapp-upgrade-el8toel9` RPM package.
====
** *RHEL 8.8*: version `0.15.1` the `leapp package` and version `0.18.0` of the `leapp-repository` package
** *RHEL 8.10*: `leapp` version `0.18.0` of the `leapp` package and version `0.21.0` of the `leapp-repository` package
+
[NOTE]
====
If your system does not have internet access, download the following packages from the link:https://access.redhat.com/downloads/content/479/ver=/rhel---8/8.5/x86_64/packages[Red Hat Customer Portal]:

* `leapp`
* `leapp-deps`
* `python3-leapp`
* `leapp-upgrade-el8toel9`
* `leapp-upgrade-el8toel9-deps`

====

. Update all packages to the latest RHEL 8 version and reboot:

+
[subs="quotes,attributes"]
----
# *dnf update*
# *reboot*
----
+

. The latest release of the `leapp-upgrade-el8toel9` package contains all required data files. If you have replaced these data files with older versions, remove all JSON files in the `/etc/leapp/files` directory and reinstall the `leapp-upgrade-el8toel9` package to ensure your data files are up-to-date.
+
////
. Ensure you have access to the latest version of additional required data files, including RPM package changes, RPM repository mapping, and unsupported drivers and devices.
.. If you are using RHSM for the upgrade, the system has access to cloud.redhat.com, and you have not downloaded an earlier version of the required data files, no further action is required from you. The data files are automatically downloaded from cloud.redhat.com.
.. If you are accessing Red Hat CDN using a proxy server, define the `$LEAPP_PROXY_HOST` environment variable to access the latest version of required data files.
.. If needed, download the data files attached to the Knowledgebase article link:https://access.redhat.com/articles/3664871[Leapp utility metadata in-place upgrades of RHEL for disconnected upgrades] and place them in the `/etc/leapp/files/` directory. This is necessary for a successful upgrade in the following scenarios:
... You are upgrading on a public cloud by using RHUI. If you do not have a Red Hat subscription or Red Hat Customer Portal account, create a no-cost RHEL developer subscription so that you can access the Knowledgebase article and download required data packages. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/4078831[How do I get a no-cost Red Hat Enterprise Linux Developer Subscription or renew it?]
... Your system does not have internet access.
... You are using RHSM for the upgrade and you previously downloaded an older version of the required data files but did not perform the upgrade, for example to create automated scripts. You can also delete your older version of the data files to start the automatic download of the latest file version.
////

. Optional: Review, remediate, and then remove the `rpmnew` and `rpmsave` files. For more information, see link:https://access.redhat.com/solutions/60263[What are rpmnew & rpmsave files?] 

. Temporarily disable antivirus software to prevent the upgrade from failing.

. Ensure that any configuration management system does not interfere with the in-place upgrade process:
* If you use a configuration management system with a client-server architecture, such as *Puppet*, *Salt*, or *Chef*, disable the system before running the `leapp preupgrade` command. Do not enable the configuration management system until after the upgrade is complete to prevent issues during the upgrade.
* If you use a configuration management system with agentless architecture, such as *Ansible*, do not execute the configuration and deployment file, such as an Ansible playbook, during the in-place upgrade as described in
ifdef::upgrading-to-rhel-9[]
xref:performing-the-upgrade_{context}[Performing the upgrade].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#performing-the-upgrade_upgrading-from-rhel-8-to-rhel-9[Performing the upgrade].
endif::[]
+
Automation of the pre-upgrade and upgrade process by using a configuration management system is not supported by Red Hat. For more information, see link:https://access.redhat.com/articles/6313281[Using configuration management systems to automate parts of the Leapp pre-upgrade and upgrade process on Red Hat Enterprise Linux].

. Ensure your system does not use more than one Network Interface Card (NIC) with a name based on the prefix used by the kernel (`eth`). For more information about migrating to another naming scheme before an in-place upgrade to RHEL 9, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/4067471[How to perform an in-place upgrade to RHEL 8 when using kernel NIC names on RHEL 7]. The process for migrating naming schemes is the same for both the RHEL 7 to RHEL 8 upgrade and the RHEL 8 to RHEL 9 upgrade.

. If your NSS database was created in RHEL 7 or earlier, verify that the database has been converted from the DBM database format to SQLite. For more information, see
ifdef::upgrading-to-rhel-9[]
xref:proc_updating-nss-databases-from-dbm-to-sqlite_applying-security-policies[Updating NSS databases from DBM to SQLite].
endif::[]
ifndef::upgrading-to-rhel-9[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/upgrading_from_rhel_8_to_rhel_9/index#proc_updating-nss-databases-from-dbm-to-sqlite_applying-security-policies[Updating NSS databases from DBM to SQLite].
endif::[]

. RHEL 9 does not support the legacy `network-scripts` package, which was deprecated in RHEL 8. Before upgrading, move your custom network scripts and write a NetworkManager dispatcher script that executes your existing custom scripts. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/6900331[Migrating custom network scripts to NetworkManager dispatcher scripts].

. If you are upgrading using an ISO image, verify that the ISO image contains the target OS version, for example, RHEL 9.4, and is saved to a persistent local mount point to ensure that the `Leapp` utility can access the image throughout the upgrade process. 

. Ensure that you have a full system backup or a virtual machine snapshot. You should be able to get your system to the pre-upgrade state if you follow standard disaster recovery procedures within your environment. You can use the following backup options:
* Create a full backup of your system by using the Relax-and-Recover (ReaR) utility. For more information, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/assembly_recovering-and-restoring-a-system_configuring-basic-system-settings[ReaR documentation] and the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/2115051[What is Relax and Recover (ReaR) and how can I use it for disaster recovery?]. 
* Create a snapshot of your system by using link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html-single/configuring_and_managing_logical_volumes/index[LVM snapshots] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/configuring-raid-logical-volumes_configuring-and-managing-logical-volumes#splitting-and-merging-a-raid-image_configuring-raid-logical-volumes[RAID splitting]. In case of upgrading a virtual machine, you can create a snapshot of the whole VM. You can also manage snapshot and rollback boot entries by using the Boom utility. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/3750001[What is BOOM and how to install it?] and the link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/configuring_and_managing_logical_volumes/managing-system-upgrades-with-snapshots_configuring-and-managing-logical-volumes[Managing system upgrades with snapshots] guide.
+
[NOTE]
====
Because LVM snapshots do not create a full backup of your system, you might not be able to recover your system after certain upgrade failures. As a result, it is safer to create a full backup by using the ReaR utility. 
====
