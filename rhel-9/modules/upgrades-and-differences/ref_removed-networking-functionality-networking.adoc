:_module-type: REFERENCE

[id="ref_removed-functionality-networking_{context}"]
= Removed functionality

.RHEL 9 does not contain the legacy network scripts

RHEL 9 does not contain the `network-scripts` package that provided the deprecated legacy network scripts in RHEL 8. To configure network connections in RHEL 9, use NetworkManager. For details, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/[Configuring and managing networking] documentation.
// No BZ available


.The unsupported `xt_u32` Netfilter module has been removed

RHEL 8 contained the unsupported `xt_u32` module, which enabled `iptables` users to match arbitrary 32 bits in the packet header or payload. This module has been removed from RHEL 9. As a replacement, use the `nftables` packet filtering framework. If no native match exists in `nftables`, use the raw payload matching feature of `nftables`. For details, see the `raw payload expression` section in the `nft(8)` man page on your system.
// BZ#2061288


.Data Encryption Standard (DES) algorithm is not available for net-snmp communication in Red Hat Enterprise Linux 9

In previous versions of RHEL, DES was used as an encryption algorithm for secure communication between net-snmp clients and servers. In RHEL 9, the DES algorithm isn’t supported by the OpenSSL library. The algorithm is marked as insecure and hence the DES support for net-snmp has been removed.
//https://bugzilla.redhat.com/show_bug.cgi?id=1964963
