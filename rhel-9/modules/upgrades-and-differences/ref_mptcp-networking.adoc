:_module-type: REFERENCE

[id="ref_mptcp-networking_{context}"]
= MPTCP

.The mptcpd service is available

With this update the `mptcpd` service is available for usage. It is a user space based `MPTCP` path manager with integrated `mptcpize` tool.

The `mptcpd` service provides the simplified automatic configuration of the `MPTCP`paths. It benefits with better reliability of the `MPTCP` socket in case of network failure or reconfiguration.

Now you can use the `mptcpize` tool to enable the `MPTCP` protocol on the existing `systemd` units without additional external dependencies.
//BZ#1953962

