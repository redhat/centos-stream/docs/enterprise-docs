
:_module-type: REFERENCE

[id="ref_changes-to-kernel_{context}"]
= Notable changes to kernel
////
Duplicate this module and rename to fit your topic grouping or SST. For example, "NetworkManager". Make sure to update the id attribute and title above.
Replace the template Consideration items with your actual Consideration items. Add as many items as needed.
////

.The 64k page size kernel

In addition to the RHEL 9 for ARM kernel which supports 4k pages, Red Hat now offers an optional kernel package that supports 64k pages: `kernel-64k`.

The 64k page size kernel is a useful option for large datasets on ARM platforms. It enables better performance for some types of memory- and CPU-intensive operations.

You must choose page size on 64-bit ARM architecture systems at the time of installation. You can install `kernel-64k` only by Kickstart by adding the `kernel-64k` package to the package list in the `Kickstart` file.

For more information about installing `kernel-64k`, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/automatically_installing_rhel/index#installing-kernel-64k-on-arm-using-the-command-line_rhel-installer[Installing Kernel-64k on ARM].


.`cgroup-v2` enabled by default in RHEL 9

The control groups version 2 (`cgroup-v2`) feature implements a single hierarchy model that simplifies the management of control groups. Also, it ensures that a process can only be a member of a single control group at a time. Deep integration with `systemd` improves the end-user experience when configuring resource control on a RHEL system.

Development of new features is mostly done for `cgroup-v2`, which has some features that are missing in `cgroup-v1`. Similarly, `cgroup-v1` contains some legacy features that are missing in `cgroup-v2`. Also, the control interfaces are different. Therefore, third party software with direct dependency on `cgroup-v1` may not run properly in the `cgroup-v2` environment.

To use `cgroup-v1`, you need to add the following parameters to the kernel command-line:

----
systemd.unified_cgroup_hierarchy=0
systemd.legacy_systemd_cgroup_controller
----

NOTE: Both `cgroup-v1` and `cgroup-v2` are fully enabled in the kernel. There is no default control group version from the kernel point of view, and is decided by `systemd` to mount at startup.



.Kernel changes potentially affecting third party kernel modules

Linux distributions with a kernel version prior to 5.9 supported exporting GPL functions as non-GPL functions. As a result, users could link proprietary functions to GPL kernel functions through the `shim` mechanism.
With this release, the RHEL kernel incorporates upstream changes that enhance the ability of RHEL to enforce GPL by rebuffing `shim`.

IMPORTANT: Partners and independent software vendors (ISVs) should test their kernel modules with an early version of RHEL 9 to ensure their compliance with GPL.



.Core scheduling is supported in RHEL 9

With the core scheduling functionality users can prevent tasks that should not trust each other from sharing the same CPU core. Likewise, users can define groups of tasks that can share a CPU core.

These groups can be specified:

* To improve security by mitigating some cross-Symmetric Multithreading (SMT) attacks

* To isolate tasks that need a whole core. For example for tasks in real-time environments, or for tasks that rely on specific processor features such as Single Instruction, Multiple Data (SIMD) processing

For more information, see link:https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/core-scheduling.html[Core Scheduling].



.The `kernelopts` environment variable has been removed in RHEL 9

In RHEL 8, the kernel command-line parameters for systems using the GRUB boot loader were defined in the `kernelopts` environment variable. This variable was stored in the `/boot/grub2/grubenv` file for each kernel boot entry. However, storing the kernel command-line parameters using `kernelopts` was not robust. Therefore, Red Hat removed `kernelopts` and the kernel command-line parameters are now stored in the Boot Loader Specification (BLS) snippet, instead of in the `/boot/loader/entries/<pass:quotes[_KERNEL_BOOT_ENTRY_]>.conf` file.



.Red Hat protects kernel symbols only for minor releases

Red Hat guarantees that a kernel module will continue to load in all future updates within an Extended Update Support (EUS) release, only if you compile the kernel module using protected kernel symbols. There is no kernel Application Binary Interface (ABI) guarantee between minor releases of RHEL 9.
