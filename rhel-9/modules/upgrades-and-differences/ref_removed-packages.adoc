// Module included in the following assemblies:
// assembly_changes-to-packages.adoc
// include::modules/upgrades-and-differences/ref_removed-packages.adoc[leveloffset=+1]

:_mod-docs-content-type: REFERENCE

[id="removed-packages_{context}"]
= Removed packages

The following packages are part of RHEL 8 but are not distributed with RHEL 9:

[options="header", cols="3,7"]
|====
|Package|Note

|abrt |

|abrt-addon-ccpp |

|abrt-addon-kerneloops |

|abrt-addon-pstoreoops |

|abrt-addon-vmcore |

|abrt-addon-xorg |

|abrt-cli |

|abrt-console-notification |

|abrt-dbus |

|abrt-desktop |

|abrt-gui |

|abrt-gui-libs |

|abrt-libs |

|abrt-tui |

|adobe-source-sans-pro-fonts-3.02803.el9.noarch.rpm |

|alsa-plugins-pulseaudio |

|alsa-sof-firmware-debug |

|amanda |

|amanda-client |

|amanda-libs |

|amanda-server |

|ant-apache-log4j |

|ant-contrib |

|ant-contrib-javadoc |

|ant-javadoc |

|ant-manual |

|antlr-C++ |

|antlr-javadoc |

|antlr-manual |

|antlr3 |

|antlr32 |

|aopalliance |

|aopalliance |

|aopalliance-javadoc |

|apache-commons-beanutils-javadoc |

|apache-commons-cli-javadoc |

|apache-commons-codec-javadoc |

|apache-commons-collections-javadoc |

|apache-commons-collections-testframework |

|apache-commons-compress-javadoc |

|apache-commons-exec |

|apache-commons-exec-javadoc |

|apache-commons-io-javadoc |

|apache-commons-jxpath |

|apache-commons-jxpath |

|apache-commons-jxpath-javadoc |

|apache-commons-lang-javadoc |

|apache-commons-lang3-javadoc |

|apache-commons-logging-javadoc |

|apache-commons-net-javadoc |

|apache-commons-parent |

|apache-ivy |

|apache-ivy-javadoc |

|apache-parent |

|apache-resource-bundles |

|apache-sshd |

|apiguardian |

|aqute-bnd-javadoc |

|arpwatch | 

|aspnetcore-runtime-3.0 |

|aspnetcore-runtime-3.1 |

|aspnetcore-runtime-5.0 |

|aspnetcore-targeting-pack-3.0 |

|aspnetcore-targeting-pack-3.1 |

|aspnetcore-targeting-pack-5.0 |

|assertj-core-javadoc |

|atinject-javadoc |

|atinject-tck |

|authd |

|auto |

|autoconf213 |

|autogen |

|autogen-libopts |

|autogen-libopts-devel |

|avahi-ui | 

|avahi-ui-devel | 

|avahi-ui-gtk3 | 

|awscli |

|base64coder |

|bash-doc | 

|batik |

|batik-css |

|batik-util |

|bcel-javadoc |

|bea-stax |

|bea-stax-api |

|beust-jcommander-javadoc |

|bind-export-devel |

|bind-export-libs |

|bind-pkcs11 | Instead of the `named-pkcs11` service, append `-E pkcs11` to `named.service`. Use `pkcs11-tool` from the `opensc` package to manage `pkcs11` tokens or stored keys.

|bind-pkcs11-devel |

|bind-pkcs11-libs |

|bind-pkcs11-utils |

|bind-sdb |

|bind-sdb-chroot |

|bitmap-console-fonts | 

|bitmap-fixed-fonts | 

|bitmap-fonts-compat | 

|bitmap-lucida-typewriter-fonts |

|bluez-hid2hci |

|bnd-maven-plugin |

|boost-jam |

|boost-signals |

|bouncycastle |

|bpg-algeti-fonts |

|bpg-chveulebrivi-fonts |

|bpg-classic-fonts |

|bpg-courier-fonts |

|bpg-courier-s-fonts |

|bpg-dedaena-block-fonts |

|bpg-dejavu-sans-fonts |

|bpg-elite-fonts |

|bpg-excelsior-caps-fonts |

|bpg-excelsior-condenced-fonts |

|bpg-excelsior-fonts |

|bpg-fonts-common |

|bpg-glaho-fonts |

|bpg-gorda-fonts |

|bpg-ingiri-fonts |

|bpg-irubaqidze-fonts |

|bpg-mikhail-stephan-fonts |

|bpg-mrgvlovani-caps-fonts |

|bpg-mrgvlovani-fonts |

|bpg-nateli-caps-fonts |

|bpg-nateli-condenced-fonts |

|bpg-nateli-fonts |

|bpg-nino-medium-cond-fonts |

|bpg-nino-medium-fonts |

|bpg-sans-fonts |

|bpg-sans-medium-fonts |

|bpg-sans-modern-fonts |

|bpg-sans-regular-fonts |

|bpg-serif-fonts |

|bpg-serif-modern-fonts |

|bpg-ucnobi-fonts |

|brlapi-java |

|bsf-javadoc |

|bsh |

|bsh-javadoc |

|bsh-manual |

|buildnumber-maven-plugin |

|byaccj |

|byaccj-debuginfo |

|byaccj-debugsource |

|cal10n |

|cal10n-javadoc |

|cbi-plugins |

|cdi-api-javadoc |

|cdparanoia |

|cdparanoia-devel |

|cdparanoia-libs |

|cdrdao |

|celt051 |

|celt051-devel |

|cgdcbxd | 

|cglib-javadoc |

|clutter-devel |

|clutter-doc |

|clutter-gst3-devel |

|clutter-gtk-devel |

|cmirror |

|codehaus-parent |

|codemodel |

|cogl-devel |

|cogl-doc |

|compat-exiv2-026 |

|compat-guile18 |

|compat-guile18-devel |

|compat-hwloc1 |

|compat-libpthread-nonshared |

|compat-libtiff3 |

|compat-openssl10 |

|compat-sap-c++-10 |

|compat-sap-c++-11 |

|compat-sap-c++-9 |

|crash-ptdump-command |

|ctags |

|ctags-etags |

|culmus-keteryg-fonts | 

|culmus-shofar-fonts | 

|custodia |

|cyrus-imapd-vzic |

|dbus-c++ |

|dbus-c++-devel |

|dbus-c++-glib |

|dbxtool |

|dejavu-fonts-common | 

|dhcp-libs |

|directory-maven-plugin |

|directory-maven-plugin-javadoc |

|dirsplit |

|dleyna-connector-dbus |

|dleyna-core |

|dleyna-renderer |

|dleyna-server |

|dnf-plugin-spacewalk |

|dnssec-trigger |

|dnssec-trigger-panel |

|dotnet | 

|dotnet-apphost-pack-3.0 |

|dotnet-apphost-pack-3.1 |

|dotnet-apphost-pack-5.0 |

|dotnet-build-reference-packages |

|dotnet-host-fxr-2.1 |

|dotnet-hostfxr-3.0 |

|dotnet-hostfxr-3.1 |

|dotnet-hostfxr-5.0 |

|dotnet-runtime-2.1 |

|dotnet-runtime-3.0 |

|dotnet-runtime-3.1 |

|dotnet-runtime-5.0 |

|dotnet-sdk-2.1 |

|dotnet-sdk-2.1.5xx |

|dotnet-sdk-3.0 |

|dotnet-sdk-3.1 |

|dotnet-sdk-3.1-source-built-artifacts |

|dotnet-sdk-5.0 |

|dotnet-sdk-5.0-source-built-artifacts |

|dotnet-targeting-pack-3.0 |

|dotnet-targeting-pack-3.1 |

|dotnet-targeting-pack-5.0 |

|dotnet-templates-3.0 |

|dotnet-templates-3.1 |

|dotnet-templates-5.0 |

|dotnet5.0-build-reference-packages |

|dptfxtract |

|drpm |

|drpm-devel |

|dump | The `dump` package providing the `dump` utility has been removed. You can use the `tar`, `dd`, or `bacula` backup utility instead.

|dvd+rw-tools |

|dyninst-static |

|easymock-javadoc |

|eclipse-ecf |

|eclipse-ecf-core |

|eclipse-ecf-runtime |

|eclipse-emf |

|eclipse-emf-core |

|eclipse-emf-runtime |

|eclipse-emf-xsd |

|eclipse-equinox-osgi |

|eclipse-jdt |

|eclipse-license |

|eclipse-p2-discovery |

|eclipse-pde |

|eclipse-platform |

|eclipse-swt |

|ed25519-java |

|ee4j-parent |

|elfutils-devel-static |

|elfutils-libelf-devel-static |

|elinks | 

|emacs-terminal | 

|emoji-picker | 

|enca |

|enca-devel |

|environment-modules-compat |

|evemu | 

|evemu-libs | 

|evince-browser-plugin |

|exec-maven-plugin |

|exec-maven-plugin-javadoc |

|farstream02 |

|felix-gogo-command |

|felix-gogo-runtime |

|felix-gogo-shell |

|felix-osgi-compendium |

|felix-osgi-compendium-javadoc |

|felix-osgi-core |

|felix-osgi-core-javadoc |

|felix-osgi-foundation |

|felix-osgi-foundation-javadoc |

|felix-parent |

|felix-scr |

|felix-utils-javadoc |

|file-roller |

|fipscheck |

|fipscheck-devel |

|fipscheck-lib |

|fonts-tweak-tool |

|forge-parent |

|freeradius-mysql |

|freeradius-perl |

|freeradius-postgresql |

|freeradius-sqlite |

|freeradius-unixODBC |

|frei0r-devel |

|frei0r-plugins |

|frei0r-plugins-opencv |

|fuse-sshfs |

|fusesource-pom |

|future |

|gamin |

|gamin-devel |

|gavl |

|gcc-toolset-10 |

|gcc-toolset-10-annobin |

|gcc-toolset-10-binutils |

|gcc-toolset-10-binutils-devel |

|gcc-toolset-10-build |

|gcc-toolset-10-dwz |

|gcc-toolset-10-dyninst |

|gcc-toolset-10-dyninst-devel |

|gcc-toolset-10-elfutils |

|gcc-toolset-10-elfutils-debuginfod-client |

|gcc-toolset-10-elfutils-debuginfod-client-devel |

|gcc-toolset-10-elfutils-devel |

|gcc-toolset-10-elfutils-libelf |

|gcc-toolset-10-elfutils-libelf-devel |

|gcc-toolset-10-elfutils-libs |

|gcc-toolset-10-gcc |

|gcc-toolset-10-gcc-c++ |

|gcc-toolset-10-gcc-gdb-plugin |

|gcc-toolset-10-gcc-gfortran |

|gcc-toolset-10-gcc-plugin-devel |

|gcc-toolset-10-gdb |

|gcc-toolset-10-gdb-doc |

|gcc-toolset-10-gdb-gdbserver |

|gcc-toolset-10-libasan-devel |

|gcc-toolset-10-libatomic-devel |

|gcc-toolset-10-libitm-devel |

|gcc-toolset-10-liblsan-devel |

|gcc-toolset-10-libquadmath-devel |

|gcc-toolset-10-libstdc++-devel |

|gcc-toolset-10-libstdc++-docs |

|gcc-toolset-10-libtsan-devel |

|gcc-toolset-10-libubsan-devel |

|gcc-toolset-10-ltrace |

|gcc-toolset-10-make |

|gcc-toolset-10-make-devel |

|gcc-toolset-10-perftools |

|gcc-toolset-10-runtime |

|gcc-toolset-10-strace |

|gcc-toolset-10-systemtap |

|gcc-toolset-10-systemtap-client |

|gcc-toolset-10-systemtap-devel |

|gcc-toolset-10-systemtap-initscript |

|gcc-toolset-10-systemtap-runtime |

|gcc-toolset-10-systemtap-sdt-devel |

|gcc-toolset-10-systemtap-server |

|gcc-toolset-10-toolchain |

|gcc-toolset-10-valgrind |

|gcc-toolset-10-valgrind-devel |

|gcc-toolset-11 |

|gcc-toolset-11-annobin-annocheck |

|gcc-toolset-11-annobin-docs |

|gcc-toolset-11-annobin-plugin-gcc |

|gcc-toolset-11-binutils |

|gcc-toolset-11-binutils-devel |

|gcc-toolset-11-build |

|gcc-toolset-11-dwz |

|gcc-toolset-11-dyninst |

|gcc-toolset-11-dyninst-devel |

|gcc-toolset-11-elfutils |

|gcc-toolset-11-elfutils-debuginfod-client |

|gcc-toolset-11-elfutils-debuginfod-client-devel |

|gcc-toolset-11-elfutils-devel |

|gcc-toolset-11-elfutils-libelf |

|gcc-toolset-11-elfutils-libelf-devel |

|gcc-toolset-11-elfutils-libs |

|gcc-toolset-11-gcc |

|gcc-toolset-11-gcc-c++ |

|gcc-toolset-11-gcc-gdb-plugin |

|gcc-toolset-11-gcc-gfortran |

|gcc-toolset-11-gcc-plugin-devel |

|gcc-toolset-11-gdb |

|gcc-toolset-11-gdb-doc |

|gcc-toolset-11-gdb-gdbserver |

|gcc-toolset-11-libasan-devel |

|gcc-toolset-11-libatomic-devel |

|gcc-toolset-11-libgccjit |

|gcc-toolset-11-libgccjit-devel |

|gcc-toolset-11-libgccjit-docs |

|gcc-toolset-11-libitm-devel |

|gcc-toolset-11-liblsan-devel |

|gcc-toolset-11-libquadmath-devel |

|gcc-toolset-11-libstdc++-devel |

|gcc-toolset-11-libstdc++-docs |

|gcc-toolset-11-libtsan-devel |

|gcc-toolset-11-libubsan-devel |

|gcc-toolset-11-ltrace |

|gcc-toolset-11-make |

|gcc-toolset-11-make-devel |

|gcc-toolset-11-perftools |

|gcc-toolset-11-runtime |

|gcc-toolset-11-strace |

|gcc-toolset-11-systemtap |

|gcc-toolset-11-systemtap-client |

|gcc-toolset-11-systemtap-devel |

|gcc-toolset-11-systemtap-initscript |

|gcc-toolset-11-systemtap-runtime |

|gcc-toolset-11-systemtap-sdt-devel |

|gcc-toolset-11-systemtap-server |

|gcc-toolset-11-toolchain |

|gcc-toolset-11-valgrind |

|gcc-toolset-11-valgrind-devel |

|gcc-toolset-12-annobin-annocheck | 

|gcc-toolset-12-annobin-docs | 

|gcc-toolset-12-annobin-plugin-gcc | 

|gcc-toolset-12-binutils-devel | 

|gcc-toolset-12-binutils-gold | 

|gcc-toolset-9 |

|gcc-toolset-9-annobin |

|gcc-toolset-9-binutils |

|gcc-toolset-9-binutils-devel |

|gcc-toolset-9-build |

|gcc-toolset-9-dwz |

|gcc-toolset-9-dyninst |

|gcc-toolset-9-dyninst-devel |

|gcc-toolset-9-dyninst-doc |

|gcc-toolset-9-dyninst-static |

|gcc-toolset-9-dyninst-testsuite |

|gcc-toolset-9-elfutils |

|gcc-toolset-9-elfutils-devel |

|gcc-toolset-9-elfutils-libelf |

|gcc-toolset-9-elfutils-libelf-devel |

|gcc-toolset-9-elfutils-libs |

|gcc-toolset-9-gcc |

|gcc-toolset-9-gcc-c++ |

|gcc-toolset-9-gcc-gdb-plugin |

|gcc-toolset-9-gcc-gfortran |

|gcc-toolset-9-gcc-plugin-devel |

|gcc-toolset-9-gdb |

|gcc-toolset-9-gdb-doc |

|gcc-toolset-9-gdb-gdbserver |

|gcc-toolset-9-libasan-devel |

|gcc-toolset-9-libatomic-devel |

|gcc-toolset-9-libitm-devel |

|gcc-toolset-9-liblsan-devel |

|gcc-toolset-9-libquadmath-devel |

|gcc-toolset-9-libstdc++-devel |

|gcc-toolset-9-libstdc++-docs |

|gcc-toolset-9-libtsan-devel |

|gcc-toolset-9-libubsan-devel |

|gcc-toolset-9-ltrace |

|gcc-toolset-9-make |

|gcc-toolset-9-make-devel |

|gcc-toolset-9-perftools |

|gcc-toolset-9-runtime |

|gcc-toolset-9-strace |

|gcc-toolset-9-systemtap |

|gcc-toolset-9-systemtap-client |

|gcc-toolset-9-systemtap-devel |

|gcc-toolset-9-systemtap-initscript |

|gcc-toolset-9-systemtap-runtime |

|gcc-toolset-9-systemtap-sdt-devel |

|gcc-toolset-9-systemtap-server |

|gcc-toolset-9-toolchain |

|gcc-toolset-9-valgrind |

|gcc-toolset-9-valgrind-devel |

|GConf2 |

|GConf2-devel |

|gegl |

|genwqe-tools |

|genwqe-vpd |

|genwqe-zlib |

|genwqe-zlib-devel |

|geoipupdate |

|geronimo-annotation |

|geronimo-annotation |

|geronimo-annotation-javadoc |

|geronimo-jms |

|geronimo-jms-javadoc |

|geronimo-jpa |

|geronimo-jpa-javadoc |

|geronimo-parent-poms |

|gfbgraph |

|gflags |

|gflags-devel |

|glassfish-annotation-api |

|glassfish-annotation-api |

|glassfish-annotation-api-javadoc |

|glassfish-el |

|glassfish-fastinfoset |

|glassfish-jaxb-core |

|glassfish-jaxb-txw2 |

|glassfish-jsp |

|glassfish-jsp-api |

|glassfish-jsp-api |

|glassfish-jsp-api-javadoc |

|glassfish-legal |

|glassfish-master-pom |

|glassfish-servlet-api |

|glassfish-servlet-api |

|glassfish-servlet-api-javadoc |

|glew-devel |

|glib2-fam |

|glog |

|glog-devel |

|gmock |

|gmock-devel |

|gnome-abrt |

|gnome-boxes |

|gnome-menus-devel |

|gnome-online-miners |

|gnome-shell-extension-dash-to-panel |

|gnome-shell-extension-disable-screenshield |

|gnome-shell-extension-horizontal-workspaces |

|gnome-shell-extension-no-hot-corner |

|gnome-shell-extension-window-grouper |

|gnome-themes-standard |

|gnu-free-fonts-common |

|gnu-free-mono-fonts |

|gnu-free-sans-fonts |

|gnu-free-serif-fonts |

|gnuplot |

|gnuplot-common |

|gnuplot-doc |

|google-droid-kufi-fonts | 

|google-gson |

|google-guice-javadoc |

|google-noto-kufi-arabic-fonts | 

|google-noto-naskh-arabic-fonts | 

|google-noto-naskh-arabic-ui-fonts | 

|google-noto-nastaliq-urdu-fonts | 

|google-noto-sans-balinese-fonts | 

|google-noto-sans-bamum-fonts | 

|google-noto-sans-batak-fonts | 

|google-noto-sans-buginese-fonts | 

|google-noto-sans-buhid-fonts | 

|google-noto-sans-canadian-aboriginal-fonts | 

|google-noto-sans-cham-fonts | 

|google-noto-sans-cuneiform-fonts | 

|google-noto-sans-cypriot-fonts | 

|google-noto-sans-gothic-fonts | 

|google-noto-sans-gurmukhi-ui-fonts | 

|google-noto-sans-hanunoo-fonts | 

|google-noto-sans-inscriptional-pahlavi-fonts | 

|google-noto-sans-inscriptional-parthian-fonts | 

|google-noto-sans-javanese-fonts | 

|google-noto-sans-lepcha-fonts | 

|google-noto-sans-limbu-fonts | 

|google-noto-sans-linear-b-fonts | 

|google-noto-sans-lisu-fonts | 

|google-noto-sans-mandaic-fonts | 

|google-noto-sans-meetei-mayek-fonts | 

|google-noto-sans-mongolian-fonts | 

|google-noto-sans-myanmar-fonts | 

|google-noto-sans-myanmar-ui-fonts | 

|google-noto-sans-new-tai-lue-fonts | 

|google-noto-sans-ogham-fonts | 

|google-noto-sans-ol-chiki-fonts | 

|google-noto-sans-old-italic-fonts | 

|google-noto-sans-old-persian-fonts | 

|google-noto-sans-oriya-fonts | 

|google-noto-sans-oriya-ui-fonts | 

|google-noto-sans-phags-pa-fonts | 

|google-noto-sans-rejang-fonts | 

|google-noto-sans-runic-fonts | 

|google-noto-sans-samaritan-fonts | 

|google-noto-sans-saurashtra-fonts | 

|google-noto-sans-sundanese-fonts | 

|google-noto-sans-syloti-nagri-fonts | 

|google-noto-sans-syriac-eastern-fonts |

|google-noto-sans-syriac-estrangela-fonts |

|google-noto-sans-syriac-western-fonts |

|google-noto-sans-tagalog-fonts | 

|google-noto-sans-tagbanwa-fonts | 

|google-noto-sans-tai-le-fonts | 

|google-noto-sans-tai-tham-fonts | 

|google-noto-sans-tai-viet-fonts | 

|google-noto-sans-tibetan-fonts |

|google-noto-sans-tifinagh-fonts | 

|google-noto-sans-ui-fonts |

|google-noto-sans-yi-fonts | 

|google-noto-serif-bengali-fonts | 

|google-noto-serif-devanagari-fonts | 

|google-noto-serif-gujarati-fonts | 

|google-noto-serif-kannada-fonts | 

|google-noto-serif-malayalam-fonts | 

|google-noto-serif-tamil-fonts | 

|google-noto-serif-telugu-fonts | 

|gphoto2 |

|gsl-devel |

|gssntlmssp |

|gtest |

|gtest-devel |

|gtkmm24 |

|gtkmm24-devel |

|gtkmm24-docs |

|gtksourceview3 |

|gtksourceview3-devel |

|gtkspell |

|gtkspell-devel |

|guava20-javadoc |

|guava20-testlib |

|guice-assistedinject |

|guice-bom |

|guice-extensions |

|guice-grapher |

|guice-jmx |

|guice-jndi |

|guice-multibindings |

|guice-parent |

|guice-servlet |

|guice-testlib |

|guice-throwingproviders |

|guile |

|guile-devel |

|gutenprint-libs-ui |

|gutenprint-plugin |

|gvfs-afc |

|gvfs-afp |

|gvfs-archive |

|hamcrest-core |

|hamcrest-core |

|hamcrest-demo |

|hamcrest-javadoc |

|hawtjni |

|hawtjni |

|hawtjni |

|hawtjni-javadoc |

|hawtjni-runtime |

|hawtjni-runtime |

|HdrHistogram |

|HdrHistogram-javadoc |

|highlight-gui |

|hplip-gui |

|hspell | 

|httpcomponents-client-cache |

|httpcomponents-client-javadoc |

|httpcomponents-core-javadoc |

|httpcomponents-project |

|hwloc-plugins |

|hyphen-fo |

|hyphen-grc |

|hyphen-hsb |

|hyphen-ia |

|hyphen-is |

|hyphen-ku |

|hyphen-mi |

|hyphen-mn |

|hyphen-sa |

|hyphen-tk |

|ibus-sayura |

|ibus-table-devel | 

|ibus-table-tests | 

|ibus-typing-booster-tests | 

|icedax |

|icu4j |

|idm-console-framework |

|ilmbase-devel |

|ima-evm-utils0 |

|imake | 

|intel-gpu-tools |

|ipython |

|isl |

|isl-devel |

|isorelax |

|isorelax-javadoc |

|istack-commons-runtime |

|istack-commons-tools |

|ivy-local |

|iwl3945-firmware |

|iwl4965-firmware |

|iwl6000-firmware |

|jacoco |

|jaf |

|jaf-javadoc |

|jakarta-commons-httpclient-demo |

|jakarta-commons-httpclient-javadoc |

|jakarta-commons-httpclient-manual |

|jakarta-oro-javadoc |

|janino |

|jansi-javadoc |

|jansi-native |

|jansi-native |

|jansi-native-javadoc |

|jarjar |

|java-1.8.0-ibm |

|java-1.8.0-ibm-demo |

|java-1.8.0-ibm-devel |

|java-1.8.0-ibm-headless |

|java-1.8.0-ibm-jdbc |

|java-1.8.0-ibm-plugin |

|java-1.8.0-ibm-src |

|java-1.8.0-ibm-webstart |

|java-1.8.0-openjdk-accessibility |

|java-1.8.0-openjdk-accessibility-fastdebug |

|java-1.8.0-openjdk-accessibility-slowdebug |

|java-atk-wrapper |

|java_cup |

|java_cup-javadoc |

|java_cup-manual |

|javacc |

|javacc-demo |

|javacc-javadoc |

|javacc-manual |

|javacc-maven-plugin |

|javacc-maven-plugin-javadoc |

|javaewah |

|javamail-javadoc |

|javapackages-local |

|javaparser |

|javapoet |

|javassist |

|javassist |

|javassist-javadoc |

|javassist-javadoc |

|jaxen |

|jaxen-demo |

|jaxen-javadoc |

|jboss-annotations-1.2-api |

|jboss-interceptors-1.2-api |

|jboss-interceptors-1.2-api |

|jboss-interceptors-1.2-api-javadoc |

|jboss-logmanager |

|jboss-parent |

|jctools |

|jdepend-demo |

|jdepend-javadoc |

|jdependency |

|jdependency-javadoc |

|jdom |

|jdom-demo |

|jdom-javadoc |

|jdom2 |

|jdom2-javadoc |

|jetty |

|jetty-continuation |

|jetty-http |

|jetty-io |

|jetty-security |

|jetty-server |

|jetty-servlet |

|jetty-util |

|jffi |

|jflex |

|jflex-javadoc |

|jgit |

|jline |

|jline |

|jline-javadoc |

|jmc |

|jmc-core-javadoc |

|jnr-netdb |

|jolokia-jvm-agent |

|js-uglify |

|jsch-javadoc |

|json_simple |

|jsoup-javadoc |

|jsr-305-javadoc |

|jss-javadoc |

|jtidy |

|jul-to-slf4j |

|junit-javadoc |

|junit-manual |

|jvnet-parent |

|jzlib-demo |

|jzlib-javadoc |

|khmeros-fonts-common | 

|kmod-redhat-oracleasm |

|kurdit-unikurd-web-fonts |

|kyotocabinet-libs |

|ldapjdk-javadoc |

|lensfun |

|lensfun-devel |

|lftp-scripts |

|libaec |

|libaec-devel |

|libappindicator-gtk3 |

|libappindicator-gtk3-devel |

|libasan6 |

|libatomic-static |

|libavc1394 |

|libblocksruntime |

|libcacard |

|libcacard-devel |

|libcgroup |

|libcgroup-pam | 

|libcgroup-tools |

|libchamplain |

|libchamplain-devel |

|libchamplain-gtk |

|libcroco |

|libcroco-devel |

|libcxl |

|libcxl-devel |

|libdap |

|libdap-devel |

|libdazzle-devel |

|libdbusmenu |

|libdbusmenu-devel |

|libdbusmenu-doc |

|libdbusmenu-gtk3 |

|libdbusmenu-gtk3-devel |

|libdnet |

|libdnet-devel |

|libdv |

|libdv-devel |

|libdwarf | The `libdwarf` package is not included in RHEL 9. The `elfutils` package provides similar functionality.

|libdwarf-devel |

|libdwarf-static |

|libdwarf-tools |

|libeasyfc |

|libeasyfc-gobject |

|libepubgen-devel |

|libertas-sd8686-firmware |

|libertas-usb8388-firmware |

|libertas-usb8388-olpc-firmware |

|libev-libevent-devel | 

|libev-source | 

|libgdither |

|libGLEW |

|libgovirt |

|libguestfs-benchmarking |

|libguestfs-gfs2 |

|libguestfs-java |

|libguestfs-java-devel |

|libguestfs-javadoc |

|libguestfs-tools |

|libguestfs-tools-c |

|libhugetlbfs |

|libhugetlbfs-devel |

|libhugetlbfs-utils |

|libicu-doc | 

|libIDL |

|libIDL-devel |

|libidn | The `libidn` package (which implements the IDNA 2003 standard) is not included in RHEL 9. You can migrate applications to `libidn2`, which implements the IDNA 2008 standard and has a different feature set to `libidn`.

|libidn-devel |

|libiec61883 |

|libiec61883-devel |

|libindicator-gtk3 |

|libindicator-gtk3-devel |

|libiscsi-devel |

|libkkc |

|libkkc-common |

|libkkc-data |

|liblogging |

|libmalaga |

|libmcpp |

|libmetalink |

|libmodulemd1 | The `libmodulemd1` package has been removed and is replaced by the `libmodulemd` package.

|libmongocrypt |

|libmpcdec |

|libmpcdec-devel |

|libmtp-devel |

|libmusicbrainz5 |

|libmusicbrainz5-devel |

|libnice | 

|libnice-devel | 

|libnice-gstreamer1 | 

|liboauth |

|liboauth-devel |

|libocxl-docs |

|libpfm-static |

|libpng12 |

|libpsm2-compat | 

|libpurple |

|libpurple-devel |

|libraw1394 |

|libraw1394-devel |

|libreport-plugin-mailx |

|libreport-plugin-rhtsupport |

|libreport-plugin-ureport |

|libreport-rhel |

|libreport-rhel-bugzilla |

|librpmem | The `librpmem` package has been removed. Use the `librpma` package instead.

|librpmem-debug |

|librpmem-devel |

|libsass |

|libsass-devel |

|libselinux-python |

|libslirp-devel |

|libsqlite3x |

|libssh2-docs |

|libtar |

|libtpms-devel |

|libunwind |

|libusal |

|libvarlink |

|libverto-libevent |

|libvirt-admin |

|libvirt-bash-completion |

|libvirt-daemon-driver-storage-gluster |

|libvirt-daemon-driver-storage-iscsi-direct |

|libvirt-gconfig |

|libvirt-gobject |

|libvirt-wireshark |

|libvmem |

|libvmem-debug |

|libvmem-devel |

|libvmmalloc |

|libvmmalloc-debug |

|libvmmalloc-devel |

|libvncserver |

|libwmf |

|libwmf-devel |

|libwmf-lite |

|libXNVCtrl |

|libXNVCtrl-devel | 

|libXvMC |

|libXvMC-devel | 

|libXxf86misc |

|libXxf86misc-devel |

|libyami |

|log4j-over-slf4j |

|log4j12 |

|log4j12 |

|log4j12-javadoc |

|log4j12-javadoc |

|lohit-malayalam-fonts |

|lohit-nepali-fonts |

|lucene |

|lucene-analysis |

|lucene-analyzers-smartcn |

|lucene-queries |

|lucene-queryparser |

|lucene-sandbox |

|lz4-java |

|lz4-java-javadoc |

|mailman |

|make-devel |

|malaga |

|malaga-suomi-voikko |

|marisa |

|marisa-devel | 

|maven-antrun-plugin |

|maven-antrun-plugin-javadoc |

|maven-archiver-javadoc |

|maven-artifact |

|maven-artifact-manager |

|maven-artifact-resolver-javadoc |

|maven-artifact-transfer-javadoc |

|maven-assembly-plugin |

|maven-assembly-plugin-javadoc |

|maven-cal10n-plugin |

|maven-clean-plugin |

|maven-clean-plugin-javadoc |

|maven-common-artifact-filters-javadoc |

|maven-compiler-plugin-javadoc |

|maven-dependency-analyzer |

|maven-dependency-analyzer-javadoc |

|maven-dependency-plugin |

|maven-dependency-plugin-javadoc |

|maven-dependency-tree-javadoc |

|maven-doxia |

|maven-doxia-core |

|maven-doxia-javadoc |

|maven-doxia-logging-api |

|maven-doxia-module-apt |

|maven-doxia-module-confluence |

|maven-doxia-module-docbook-simple |

|maven-doxia-module-fml |

|maven-doxia-module-latex |

|maven-doxia-module-rtf |

|maven-doxia-module-twiki |

|maven-doxia-module-xdoc |

|maven-doxia-module-xhtml |

|maven-doxia-modules |

|maven-doxia-sink-api |

|maven-doxia-sitetools |

|maven-doxia-sitetools-javadoc |

|maven-doxia-test-docs |

|maven-doxia-tests |

|maven-enforcer-javadoc |

|maven-failsafe-plugin |

|maven-file-management-javadoc |

|maven-filtering-javadoc |

|maven-hawtjni-plugin |

|maven-install-plugin |

|maven-install-plugin-javadoc |

|maven-invoker |

|maven-invoker-javadoc |

|maven-invoker-plugin |

|maven-invoker-plugin-javadoc |

|maven-jar-plugin-javadoc |

|maven-javadoc |

|maven-local |

|maven-model |

|maven-monitor |

|maven-parent |

|maven-plugin-build-helper-javadoc |

|maven-plugin-bundle-javadoc |

|maven-plugin-descriptor |

|maven-plugin-registry |

|maven-plugin-testing-javadoc |

|maven-plugin-testing-tools |

|maven-plugin-tools-ant |

|maven-plugin-tools-beanshell |

|maven-plugin-tools-javadoc |

|maven-plugin-tools-javadocs |

|maven-plugin-tools-model |

|maven-plugins-pom |

|maven-profile |

|maven-project |

|maven-remote-resources-plugin-javadoc |

|maven-reporting-api |

|maven-reporting-api-javadoc |

|maven-reporting-impl |

|maven-reporting-impl-javadoc |

|maven-resolver-api |

|maven-resolver-api |

|maven-resolver-connector-basic |

|maven-resolver-connector-basic |

|maven-resolver-impl |

|maven-resolver-impl |

|maven-resolver-javadoc |

|maven-resolver-spi |

|maven-resolver-spi |

|maven-resolver-test-util |

|maven-resolver-transport-classpath |

|maven-resolver-transport-file |

|maven-resolver-transport-http |

|maven-resolver-transport-wagon |

|maven-resolver-transport-wagon |

|maven-resolver-util |

|maven-resolver-util |

|maven-resources-plugin-javadoc |

|maven-scm |

|maven-script |

|maven-script-ant |

|maven-script-beanshell |

|maven-script-interpreter |

|maven-script-interpreter-javadoc |

|maven-settings |

|maven-shade-plugin |

|maven-shade-plugin-javadoc |

|maven-shared |

|maven-shared-incremental-javadoc |

|maven-shared-io-javadoc |

|maven-shared-utils-javadoc |

|maven-source-plugin-javadoc |

|maven-surefire-javadoc |

|maven-surefire-report-parser |

|maven-surefire-report-plugin |

|maven-test-tools |

|maven-toolchain |

|maven-verifier-javadoc |

|maven-wagon-file |

|maven-wagon-file |

|maven-wagon-ftp |

|maven-wagon-http |

|maven-wagon-http |

|maven-wagon-http-lightweight |

|maven-wagon-http-shared |

|maven-wagon-http-shared |

|maven-wagon-javadoc |

|maven-wagon-provider-api |

|maven-wagon-provider-api |

|maven-wagon-providers |

|maven2 |

|maven2 |

|maven2-javadoc |

|meanwhile |

|mercurial |

|mercurial-hgk |

|mesa-libGLES-devel | 

|mesa-vdpau-drivers |

|metis |

|metis-devel |

|mingw32-bzip2 |

|mingw32-bzip2-static |

|mingw32-cairo |

|mingw32-expat |

|mingw32-fontconfig |

|mingw32-freetype |

|mingw32-freetype-static |

|mingw32-gstreamer1 |

|mingw32-harfbuzz |

|mingw32-harfbuzz-static |

|mingw32-icu |

|mingw32-libjpeg-turbo |

|mingw32-libjpeg-turbo-static |

|mingw32-libpng |

|mingw32-libpng-static |

|mingw32-libtiff |

|mingw32-libtiff-static |

|mingw32-openssl |

|mingw32-readline |

|mingw32-spice-vdagent |

|mingw32-sqlite |

|mingw32-sqlite-static |

|mingw64-adwaita-icon-theme |

|mingw64-bzip2 |

|mingw64-bzip2-static |

|mingw64-cairo |

|mingw64-expat |

|mingw64-fontconfig |

|mingw64-freetype |

|mingw64-freetype-static |

|mingw64-gstreamer1 |

|mingw64-harfbuzz |

|mingw64-harfbuzz-static |

|mingw64-icu |

|mingw64-libjpeg-turbo |

|mingw64-libjpeg-turbo-static |

|mingw64-libpng |

|mingw64-libpng-static |

|mingw64-libtiff |

|mingw64-libtiff-static |

|mingw64-nettle |

|mingw64-openssl |

|mingw64-readline |

|mingw64-spice-vdagent |

|mingw64-sqlite |

|mingw64-sqlite-static |

|mockito-javadoc |

|modello |

|modello-javadoc |

|mojo-parent |

|mongo-c-driver |

|motif-static |

|mousetweaks |

|mozjs52 |

|mozjs52-devel |

|mozjs60 |

|mozjs60-devel |

|mozvoikko |

|msv-javadoc |

|msv-manual |

|munge-maven-plugin |

|munge-maven-plugin-javadoc |

|mythes-lb | 

|mythes-mi |

|mythes-ne |

|nafees-web-naskh-fonts |

|nbd-3.21-2.el9 |

|nbdkit-gzip-plugin |

|nbdkit-plugin-python-common |

|nbdkit-plugin-vddk |

|nbdkit-tar-plugin |

|ncompress | The `ncompress` package has been removed. You can use a different compressing tool, such as `gzip`, `zlib`, or `zstd`.

|ncurses-compat-libs |

|netcf |

|netcf-devel |

|netcf-libs |

|network-scripts | 

|network-scripts-ppp |

|nkf |

|nodejs-devel |

|nodejs-packaging |

|nss-pam-ldapd | The `nss-pam-ldapd` package has been removed. You can use SSSD instead.

|nss_nis |

|objectweb-asm-javadoc |

|objectweb-pom |

|objenesis-javadoc |

|ocaml-bisect-ppx |

|ocaml-camlp4 |

|ocaml-camlp4-devel |

|ocaml-lwt-5.3.0-7.el9 |

|ocaml-mmap-1.1.0-16.el9 |

|ocaml-ocplib-endian-1.1-5.el9 |

|ocaml-ounit-2.2.2-15.el9 |

|ocaml-result-1.5-7.el9 |

|ocaml-seq-0.2.2-4.el9 |

|openblas-Rblas | 

|opencryptoki-tpmtok |

|opencv-contrib |

|opencv-core |

|opencv-devel |

|OpenEXR-devel |

|openhpi |

|openhpi-libs |

|OpenIPMI-perl |

|openssh-cavs |

|openssh-ldap |

|openssl-ibmpkcs11 |

|os-maven-plugin |

|os-maven-plugin-javadoc |

|osgi-annotation-javadoc |

|osgi-compendium-javadoc |

|osgi-core-javadoc |

|overpass-mono-fonts | 

|owasp-java-encoder-javadoc |

|pakchois |

|pandoc |

|pandoc-common |

|paps-libs |

|paranamer |

|paratype-pt-sans-caption-fonts | 

|parfait |

|parfait-examples |

|parfait-javadoc |

|pcp-parfait-agent |

|pcsc-lite-doc |

|perl-B-Debug |

|perl-B-Lint |

|perl-Class-Factory-Util |

|perl-Class-ISA |

|perl-DateTime-Format-HTTP |

|perl-DateTime-Format-Mail |

|perl-File-CheckTree |

|perl-homedir |

|perl-libxml-perl |

|perl-Locale-Codes |

|perl-Mozilla-LDAP |

|perl-NKF |

|perl-Object-HashBase-tools |

|perl-Package-DeprecationManager |

|perl-Pod-LaTeX |

|perl-Pod-Plainer |

|perl-prefork |

|perl-String-CRC32 |

|perl-SUPER |

|perl-Sys-Virt |

|perl-tests |

|perl-YAML-Syck |

|phodav-2.5-4.el9 |

|php-recode |

|php-xmlrpc |

|pidgin |

|pidgin-devel |

|pidgin-sipe |

|pinentry-emacs |

|pinentry-gtk |

|pipewire0.2-devel |

|pipewire0.2-libs |

|platform-python-coverage |

|plexus-ant-factory |

|plexus-ant-factory-javadoc |

|plexus-archiver-javadoc |

|plexus-bsh-factory |

|plexus-bsh-factory-javadoc |

|plexus-build-api-javadoc |

|plexus-cipher-javadoc |

|plexus-classworlds-javadoc |

|plexus-cli |

|plexus-cli-javadoc |

|plexus-compiler-extras |

|plexus-compiler-javadoc |

|plexus-compiler-pom |

|plexus-component-api |

|plexus-component-api-javadoc |

|plexus-component-factories-pom |

|plexus-components-pom |

|plexus-containers-component-javadoc |

|plexus-containers-component-metadata |

|plexus-containers-container-default |

|plexus-containers-javadoc |

|plexus-i18n |

|plexus-i18n-javadoc |

|plexus-interactivity |

|plexus-interactivity-api |

|plexus-interactivity-javadoc |

|plexus-interactivity-jline |

|plexus-interpolation-javadoc |

|plexus-io-javadoc |

|plexus-languages-javadoc |

|plexus-pom |

|plexus-resources-javadoc |

|plexus-sec-dispatcher-javadoc |

|plexus-utils-javadoc |

|plexus-velocity |

|plexus-velocity-javadoc |

|plymouth-plugin-throbgress |

|pmreorder | 

|postgresql-test-rpm-macros |

|powermock |

|powermock-api-easymock |

|powermock-api-mockito |

|powermock-api-support |

|powermock-common |

|powermock-core |

|powermock-javadoc |

|powermock-junit4 |

|powermock-reflect |

|powermock-testng |

|prometheus-jmx-exporter |

|prometheus-jmx-exporter-openjdk11 |

|ptscotch-mpich |

|ptscotch-mpich-devel |

|ptscotch-mpich-devel-parmetis |

|ptscotch-openmpi |

|ptscotch-openmpi-devel |

|purple-sipe |

|pygobject2-doc |

|pygtk2 |

|pygtk2-codegen |

|pygtk2-devel |

|pygtk2-doc |

|python-nose-docs |

|python-nss-doc |

|python-podman-api |

|python-psycopg2-doc |

|python-pymongo-doc |

|python-redis |

|python-schedutils |

|python-slip |

|python-sphinx-locale |

|python-sqlalchemy-doc |

|python-varlink |

|python-virtualenv-doc |

|python2-backports |

|python2-backports-ssl_match_hostname |

|python2-bson |

|python2-coverage |

|python2-docs |

|python2-docs-info |

|python2-funcsigs |

|python2-gluster |

|python2-ipaddress |

|python2-iso8601 |

|python2-mock |

|python2-nose |

|python2-numpy-doc |

|python2-psycopg2-debug |

|python2-psycopg2-tests |

|python2-pymongo |

|python2-pymongo-gridfs |

|python2-pytest-mock |

|python2-sqlalchemy |

|python2-tools |

|python2-virtualenv |

|python3-avahi | 

|python3-bson |

|python3-click |

|python3-coverage |

|python3-cpio |

|python3-custodia |

|python3-docs |

|python3-evdev | 

|python3-flask |

|python3-gevent |

|python3-html5lib |

|python3-hypothesis |

|python3-iso8601 |

|python3-itsdangerous |

|python3-javapackages |

|python3-jwt |

|python3-mock |

|python3-networkx-core |

|python3-nose |

|python3-nss |

|python3-openipmi | The `python3-openipmi` package is no longer provided. `python3-pyghmi` has been introduced in order to provide a simpler Python API for the IPMI protocol, but the API is not compatible with the one of `python3-openipmi`.

|python3-pexpect |

|python3-pillow |

|python3-pillow-devel |

|python3-pillow-doc |

|python3-pillow-tk |

|python3-ptyprocess |

|python3-pydbus |

|python3-pymongo |

|python3-pymongo-gridfs |

|python3-pyOpenSSL |

|python3-reportlab |

|python3-schedutils |

|python3-scons |

|python3-semantic_version |

|python3-slip |

|python3-slip-dbus |

|python3-sqlalchemy | The `python3-sqlalchemy` package has been removed. Customers must use Python connectors for MySQL or PostgreSQL directly. Python 3 database connector for MySQL is available in the `python3-PyMySQL` package. Python 3 database connector for PostgreSQL is available in the `python3-psycopg2` package.

|python3-sure |

|python3-syspurpose |

|python3-unittest2 |

|python3-virtualenv | Use the `venv` module in Python 3 instead.

|python3-webencodings |

|python3-werkzeug |

|python3-whoosh |

|python38-asn1crypto |

|python38-atomicwrites |

|python38-more-itertools |

|python38-numpy-doc |

|python38-psycopg2-doc |

|python38-psycopg2-tests |

|python39-more-itertools |

|python39-numpy-doc |

|python39-psycopg2-doc |

|python39-psycopg2-tests |

|python39-pybind11 |

|python39-pybind11-devel |

|qdox-javadoc |

|qemu-kvm-block-gluster |

|qemu-kvm-block-iscsi |

|qemu-kvm-block-ssh |

|qemu-kvm-device-display-virtio-gpu-gl | 

|qemu-kvm-device-display-virtio-gpu-pci-gl |

|qemu-kvm-device-display-virtio-vga-gl |

|qemu-kvm-hw-usbredir |

|qemu-kvm-tests |

|qemu-kvm-ui-spice |

|qpdf |

|qpdf-doc |

|qperf | The `qperf` package has been removed. You can use the `perftest` or `iperf3` package instead.

|qpid-proton |

|qrencode |

|qrencode-devel |

|qrencode-libs |

|qt5-qtcanvas3d |

|qt5-qtcanvas3d-examples |

|rarian |

|rarian-compat |

|re2c |

|recode |

|redhat-lsb |

|redhat-lsb-core |

|redhat-lsb-cxx |

|redhat-lsb-desktop |

|redhat-lsb-languages |

|redhat-lsb-printing |

|redhat-lsb-submod-multimedia |

|redhat-lsb-submod-security |

|redhat-menus |

|redhat-support-lib-python |

|redhat-support-tool |

|reflections |

|regexp-javadoc |

|relaxngDatatype |

|resteasy-javadoc | 

|rhsm-gtk |

|rpm-plugin-prioreset |

|rpmemd |

|rubygem-abrt |

|rubygem-abrt-doc |

|rubygem-bson |

|rubygem-bson-doc |

|rubygem-bundler-doc | 

|rubygem-mongo |

|rubygem-mongo-doc |

|rubygem-net-telnet | 

|rubygem-xmlrpc | 

|s390utils-cmsfs | The `s390utils-cmsfs` package has been removed and is replaced by the `s390utils-cmsfs-fuse` package.

|samyak-devanagari-fonts |

|samyak-fonts-common |

|samyak-gujarati-fonts |

|samyak-malayalam-fonts |

|samyak-odia-fonts |

|samyak-tamil-fonts |

|sane-frontends | The `sane-frontends` package has been removed. Its functionality is covered by the `scanimage` or `xsane` package.

|sanlk-reset |

|sat4j |

|scala |

|scotch |

|scotch-devel |

|SDL_sound |

|selinux-policy-minimum |

|shim-ia32 | 

|shrinkwrap |

|sil-padauk-book-fonts | 

|sisu-inject |

|sisu-inject |

|sisu-javadoc |

|sisu-mojos |

|sisu-mojos-javadoc |

|sisu-plexus |

|sisu-plexus |

|skkdic |

|slf4j-ext |

|slf4j-javadoc |

|slf4j-jcl |

|slf4j-log4j12 |

|slf4j-manual |

|slf4j-sources |

|SLOF |

|smc-anjalioldlipi-fonts |

|smc-dyuthi-fonts |

|smc-fonts-common |

|smc-kalyani-fonts |

|smc-raghumalayalam-fonts |

|smc-suruma-fonts |

|softhsm-devel |

|sonatype-oss-parent |

|sonatype-plugins-parent |

|sos-collector |

|sparsehash-devel |

|spax | The `spax` package has been removed. You can use the `tar` and `cpio` commands instead.

|spec-version-maven-plugin |

|spec-version-maven-plugin-javadoc |

|spice-0.14.3-4.el9 |

|spice-client-win-x64 |

|spice-client-win-x86 |

|spice-glib |

|spice-glib-devel |

|spice-gtk |

|spice-gtk-tools |

|spice-gtk3 |

|spice-gtk3-devel |

|spice-gtk3-vala |

|spice-parent |

|spice-qxl-wddm-dod |

|spice-qxl-xddm |

|spice-server |

|spice-server-devel |

|spice-streaming-agent |

|spice-vdagent-win-x64 |

|spice-vdagent-win-x86 |

|star |

|stax-ex |

|stax2-api |

|stringtemplate |

|stringtemplate4 |

|subscription-manager-initial-setup-addon |

|subscription-manager-migration |

|subscription-manager-migration-data |

|subversion-javahl |

|SuperLU |

|SuperLU-devel |

|swtpm-devel |

|swtpm-tools-pkcs11 |

|system-storage-manager |

|systemd-tests | 

|tcl-brlapi |

|testng |

|testng-javadoc |

|thai-scalable-laksaman-fonts | 

|tibetan-machine-uni-fonts |

|timedatex | The `timedatex` package has been removed. The `systemd` package provides the `systemd-timedated` service, which replaces `timedatex`.

|torque | 

|torque-devel | 

|torque-libs | 

|tpm-quote-tools |

|tpm-tools |

|tpm-tools-pkcs11 |

|treelayout |

|trousers |

|trousers-devel |

|trousers-lib |

|tuned-profiles-compat |

|tuned-profiles-nfv-host-bin |

|tuned-utils-systemtap |

|tycho |

|uglify-js |

|unbound-devel |

|univocity-output-tester |

|usbguard-notifier |

|utf8cpp |

|uthash |

|uthash-devel | 

|velocity-demo |

|velocity-javadoc |

|velocity-manual |

|vinagre |

|vino |

|virt-dib |

|virt-p2v-maker |

|vm-dump-metrics-devel |

|voikko-tools | 

|vorbis-tools | 

|weld-parent |

|woodstox-core |

|wqy-microhei-fonts |

|wqy-unibit-fonts |

|xalan-j2-demo |

|xalan-j2-javadoc |

|xalan-j2-manual |

|xalan-j2-xsltc |

|xbean-javadoc |

|xdelta |

|xerces-j2-demo |

|xerces-j2-javadoc |

|xinetd |

|xml-commons-apis-javadoc |

|xml-commons-apis-manual |

|xml-commons-resolver-javadoc |

|xmlgraphics-commons |

|xmlstreambuffer |

|xmlunit-javadoc |

|xmvn-api |

|xmvn-bisect |

|xmvn-connector-aether |

|xmvn-connector-ivy |

|xmvn-install |

|xmvn-javadoc |

|xmvn-parent-pom |

|xmvn-resolve |

|xmvn-subst |

|xmvn-tools-pom |

|xorg-sgml-doctools |

|xorg-x11-apps |

|xorg-x11-docs |

|xorg-x11-drv-ati |

|xorg-x11-drv-intel |

|xorg-x11-drv-nouveau |

|xorg-x11-drv-qxl |

|xorg-x11-drv-vesa |

|xorg-x11-server-Xspice |

|xorg-x11-xkb-utils-devel |

|xpp3 |

|xsane-gimp |

|xsom |

|xz-java-javadoc |

|yajl-devel |

|yp-tools |

|ypbind |

|ypserv |

|yum-rhn-plugin |

|zsh-html | 

|====



////
//alternative with a release column - but it shouldn't happen:
[options="header", cols="3,2,7"]
|====
|Package|Removed since|Note
|<input package - sort case-insensitively>|<release>|<migration note>
|====
////
