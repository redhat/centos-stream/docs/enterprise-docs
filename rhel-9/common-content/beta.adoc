// Module included in the following assemblies:
//
// All of them!...maybe...but only in beta


[preface]
[id="con_beta-warning"]
= RHEL Beta release

Red Hat provides Red Hat Enterprise Linux Beta access to all subscribed Red Hat accounts. The purpose of Beta access is to:

* Provide an opportunity to customers to test major features and capabilities prior to the general availability release and provide feedback or report issues.
* Provide Beta product documentation as a preview. Beta product documentation is under development and is subject to substantial change.

Note that Red Hat does not support the usage of RHEL Beta releases in production use cases. For more information, see link:https://access.redhat.com/solutions/21531[What does Beta mean in Red Hat Enterprise Linux and can I upgrade a RHEL Beta installation to a General Availability (GA) release?] (Red Hat Knowledgebase).
