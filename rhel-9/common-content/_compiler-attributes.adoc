// Our docinfo.xml
:docinfo:

// AsciiDoc rendering
:source-highlighter: prettify
:attribute-missing: warn
:experimental:

// Guide directory structure
:imagesdir: images
:filesdir: files

// special chars
:cpp: C++

// Platforms
:rh: Red{nbsp}Hat
:rhel: {rh} Enterprise{nbsp}Linux
:mac: macOS
:fed: Fedora
:msw: Microsoft Windows
:ocp: OpenShift Container Platform
:osd: OpenShift Dedicated
:rosa: Red Hat OpenShift Service on AWS (ROSA)

:rhocp: {rh} {ocp}
:rhcc: {rh} Container Catalog
:rhcc-url: registry.access.redhat.com
:rhcp: {rh} Customer Portal
:eap: {rh} Enterprise{nbsp}Application{nbsp}Platform
:rhsm: Red{nbsp}Hat Subscription Management
:rhio-url: registry.redhat.io

// OpenShift versions
:ocp-ver: 3.11
:ocp4-ver: 4.5
:ocp3-ver: 3.11
:osd-ver: 3.11

// prod naming, versions
:prod: {rh} Developer Tools
:prod-ver: 1

// devstudio naming
//:devstudio: Developer{nbsp}Studio
//:rhdevstudio: {rh} {devstudio}
//:devstudio-ver: 12.11
//:rhdevstudio-ver: {rhdevstudio}{nbsp}{devstudio-ver}

// OpenJDK naming & version
:java-ver: Java{nbsp}8
:jdk: OpenJDK
:jdk-ver: 1.8.0_131

// CDK naming, version
:rhcdk: {rh} Container{nbsp}Development{nbsp}Kit
:cdk-ver: 3.10
:rhcdk-ver: {rhcdk}{nbsp}{cdk-ver}

// Developer Toolset
:dts: {rh} Developer{nbsp}Toolset
:dts-ver: 11.0 Beta
:dts-ver-short: 11 Beta
:rhscl: {rh} Software{nbsp}Collections

// RH docs
:acc-rh-com-docs: https://access.redhat.com/documentation/en-us

// RHSM docs
:rhsm-docs-url: {acc-rh-com-docs}/red_hat_subscription_management

// devstudio guide names and URLs
//:rhdevstudio-rn: {rhdevstudio-ver} Release Notes
//:rhdevstudio-rn-url: {acc-rh-com-docs}/red_hat_jboss_developer_studio/{devstudio-ver}/html-single/release_notes_and_known_issues/
//:rhdevstudio-ig: {rhdevstudio-ver} Installation Guide
//:rhdevstudio-ig-url: {acc-rh-com-docs}/red_hat_jboss_developer_studio/{devstudio-ver}/html-single/installation_guide/
//:rhdevstudio-gs-cloud: Getting Started with Container and Cloud-based Development
//:rhdevstudio-gs-cloud-url: {acc-rh-com-docs}/red_hat_jboss_developer_studio/{devstudio-ver}/html-single/getting_started_with_container_and_cloud-based_development/
//:rhdevstudio-gs-cloud-paged-url: {acc-rh-com-docs}/red_hat_jboss_developer_studio/{devstudio-ver}/html/getting_started_with_container_and_cloud-based_development/

:!isbeta:
// define (no !) or undefine (line starts with :!)

ifdef::isbeta[]
:dashbeta: -beta
endif::[]
ifndef::isbeta[]
:dashbeta:
endif::[]


