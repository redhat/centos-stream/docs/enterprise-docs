:comp: Go
:comp-ver-short: 1.20
//use comp-ver for the overall title of the document
:comp-ver: {comp-ver-short}.6
:comp-ver-rhel-7: {comp-ver-short}.6
:comp-ver-rhel-8: {comp-ver-short}.4
:comp-ver-rhel-9: {comp-ver-short}.6
:prev-comp-ver: 1.18
//:comp-ver-minor: 1.19.6
:comp-pkg: go-toolset-{comp-ver-short}
:comp-module: go-toolset

:PKGVER: 8

// Define which versions of RHEL require published documentation.
// 
// To disable RHEL 7 rendering, use `:publish-rhel-7!:`.
// To enable RHEL 8 rendering, use `:publish-rhel-8:`.
:publish-rhel-7:
:publish-rhel-8:
:publish-rhel-9:

//:dashbeta: -beta
:dashbeta:

// version per https://errata.devel.redhat.com/advisory/43014/builds
//
//use :same-versions: to enable the not version-specific attributes in the components table (i.e. when the published versions don't differ):
:same-versions!: 

:golang-ver: {comp-ver}
:golang-ver-rhel-7: {comp-ver-rhel-7}
:golang-ver-rhel-8: {comp-ver-rhel-8}
:golang-ver-rhel-9: {comp-ver-rhel-9}

:delve-ver: 1.9.0
:delve-ver-rhel-7: 1.9.0
:delve-ver-rhel-8: 1.9.0
:delve-ver-rhel-9: 1.9.0

//https://docs.engineering.redhat.com/pages/viewpage.action?pageId=146918125
:rhdt: {prod}

// RH docs
:acc-rh-com-docs: https://access.redhat.com/documentation/en-us

// DTS User Guide docs URL
// NOTE: Makes use of {dashbeta} defined above, thus its placement here.
:dts-ug-docs-url: {acc-rh-com-docs}/red_hat_developer_toolset/{dts-ver-short}{dashbeta}/html/user_guide

//OpenShift Container Platform
:ocp-ver: 4.12