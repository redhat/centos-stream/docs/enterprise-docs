// Include shared compiler toolset attributes
include::common-content/_compiler-attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// Include toolset-specific attributes
include::toolset-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= Using {comp} {comp-ver} Toolset

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:rust-toolset:


// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::assemblies/assembly_rust-toolset.adoc[leveloffset=+1]

include::assemblies/assembly_the-cargo-build-tool.adoc[leveloffset=+1]

include::assemblies/assembly_the-rustfmt-formatting-tool.adoc[leveloffset=+1]

ifdef::publish-rhel-8[]

include::assemblies/assembly_container-images-with-rust-toolset.adoc[leveloffset=+1]

endif::publish-rhel-8[]

include::assemblies/assembly_changes-in-rust-toolset.adoc[leveloffset=+1]