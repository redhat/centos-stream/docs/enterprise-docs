// Show the table of contents
:toc:
// The name of the title
:ProjectName: Using LLVM Toolset
// The subtitle of the title
:Subtitle: A demo title containing several types of includes.
// The abstract of the title
:Abstract:  This title serves as a template for new titles in the RHEL 9 product documentation set.
// The name of the title for the purposes of {context}
:ProjectNameID: using-llvm-toolset
// The following are not required
//:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
