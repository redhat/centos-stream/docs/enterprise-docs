// Show the table of contents
:toc:
// The name of the title
:ProjectName: Packaging and distributing software
// The subtitle of the title
:Subtitle: Packaging software by using the RPM package management system
// The abstract of the title
:Abstract: Package software into an RPM package by using the RPM package manager. Prepare source code for packaging, package software, and investigate advanced packaging scenarios, such as packaging Python projects or RubyGems into RPM packages.
// The name of the title for the purposes of {context}
:ProjectNameID: packaging-and-distributing-software

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
