// Show the table of contents
:toc:
// The name of the title
:ProjectName: Boot options for RHEL Installer
// The subtitle of the title
:Subtitle: Customizing the installation program's behavior by specifying boot options
// The abstract of the title
:Abstract: {ProductShortName} installation system includes a range of boot options for administrators. By using these options, you can modify the default behavior of the installation program by enabling or disabling certain functions to customize the installation program.
// The name of the title for the purposes of {context}
:ProjectNameID: boot-options-for-rhel-installer

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
