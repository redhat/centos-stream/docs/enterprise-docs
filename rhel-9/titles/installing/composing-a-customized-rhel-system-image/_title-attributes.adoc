// Show the table of contents
:toc:

:ProjectName: Composing a customized RHEL system image

:Subtitle: Creating customized system images with RHEL image builder on {RHEL9}

:Abstract: RHEL image builder is a tool for creating deployment-ready customized system images: installation disks, virtual machines, cloud vendor-specific images, and others. By using RHEL image builder, you can create these images faster if compared to manual procedures, because it eliminates the specific configurations required for each output type. This document describes how to set up RHEL image builder and create images with it.
// The name of the title for the purposes of {context}
:ProjectNameID: composing-a-customized-rhel-system-image

:ProjectVersion: 0.1

// Product-specific
:RHEL: Red{nbsp}Hat Enterprise{nbsp}Linux
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
