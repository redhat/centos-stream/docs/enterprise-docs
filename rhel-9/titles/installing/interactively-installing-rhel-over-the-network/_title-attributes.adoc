// Show the table of contents
:toc:

// The name of the title
:ProjectName: Interactively installing RHEL over the network

// The subtitle of the title
:Subtitle: Installing RHEL on several systems using network resources or on a headless system with the graphical installer

// The abstract of the title
:Abstract: You can install RHEL by using the graphical installer over your local network. Use this method to install RHEL on one or a few systems if you prefer the graphical interface during the installation and your systems have no peripherals, such as a display. The installation source is a server in your local network or the Red Hat content delivery network (CDN).

// The name of the title for the purposes of {context}
:ProjectNameID: rhel-installer

// The following are not required
:ProjectVersion: 0.1

