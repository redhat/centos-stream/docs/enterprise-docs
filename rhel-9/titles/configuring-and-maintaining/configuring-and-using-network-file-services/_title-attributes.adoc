// Show the table of contents
:toc:
// The name of the title
:ProjectName: Configuring and using network file services
// The subtitle of the title
:Subtitle: A guide to configuring and using network file services in {RHEL} 9.
// The abstract of the title
:Abstract:  This document describes how to configure and run network file services on {RHEL} 9, including Samba server and NFS server.
// The name of the title for the purposes of {context}
:ProjectNameID: configuring-and-using-network-file-services

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:RHEL: Red{nbsp}Hat Enterprise{nbsp}Linux
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
