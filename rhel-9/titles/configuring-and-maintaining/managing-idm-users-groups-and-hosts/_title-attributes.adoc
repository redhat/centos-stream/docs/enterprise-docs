// Show the table of contents
:toc:
// The name of the title
:ProjectName: Managing IdM users, groups, hosts, and access control rules
// The subtitle of the title
:Subtitle: Configuring users and hosts, managing them in groups, and controlling access with host-based and role-based access control rules
// The abstract of the title
//:Abstract: The abstract is directly in docinfo.xml, because within an attribute, multiple paragraphs are not supported.
// The name of the title for the purposes of {context}
:ProjectNameID: managing-users-groups-hosts

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
