// Show the table of contents
:toc:

// The name of the title
:ProjectName: Configuring InfiniBand and RDMA networks

// The subtitle of the title
:Subtitle: Configuring and managing high-speed network protocols and RDMA hardware

// The abstract of the title
:Abstract: You can configure and manage Remote Directory Memory Access (RDMA) networks and InfiniBand hardware at an enterprise level by using various protocols. These include RDMA over Converged Ethernet (RoCE), the software implementation of RoCE (Soft-RoCE), the IP networks protocol such as iWARP, and the Network File System over RDMA (NFSoRDMA) protocol as a native support on RDMA-supported hardware. For low-latency and high-throughput connections, you can configure IP over InfiniBand (IPoIB).

// The name of the title for the purposes of {context}
:ProjectNameID: configuring-infiniband-and-rdma-networks

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
