include::common-content/_attributes.adoc[]

include::_title-attributes.adoc[]

[id="{ProjectNameID}"]
= {ProjectName}

:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

:cloud-content-aws:

// include::common-content/beta.adoc[leveloffset=+1]

// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_introducing-rhel-on-public-cloud-platforms.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_creating-and-uploading-aws-ami-images.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_deploying-a-virtual-machine-on-aws.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-a-red-hat-high-availability-cluster-on-aws.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-the-opentelemetry-collector-for-rhel-on-cloud.adoc[leveloffset=+1]
