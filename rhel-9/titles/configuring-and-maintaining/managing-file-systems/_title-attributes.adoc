// Show the table of contents
:toc:
// The name of the title
:ProjectName: Managing file systems
// The subtitle of the title
:Subtitle: Creating, modifying, and administering file systems in {RHEL9}
// The abstract of the title
//:Abstract: The abstract is added to the docinfo.xml file because within an attribute, multiple paragraphs and markups are not supported. 
// The name of the title for the purposes of {context}
:ProjectNameID: managing-file-systems

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
