// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:customizing-the-gnome-desktop-environment:


include::rhel-8/common-content/snip_3d-acceleration-with-gnome.adoc[]

// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_enabling-desktop-icons.adoc[leveloffset=+1]

include::assemblies/assembly_setting-up-startup-applications-in-gnome.adoc[leveloffset=+1]

include::modules/desktop/proc_enabling-automatic-login.adoc[leveloffset=+1]

include::modules/desktop/proc_enabling-automatic-suspension.adoc[leveloffset=+1]

include::modules/desktop/proc_enabling-automatic-screen-lock.adoc[leveloffset=+1]

include::modules/desktop/proc_changing-how-your-system-behaves-when-you-close-the-laptop-lid.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-screen-rotation.adoc[leveloffset=+1]

include::assemblies/assembly_customizing-desktop-appearance-and-branding.adoc[leveloffset=+1]

include::assemblies/assembly_restricting-the-desktop-session.adoc[leveloffset=+1]

include::assemblies/assembly_restricting-the-session-to-a-single-application.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-sound-in-gnome.adoc[leveloffset=+1]

include::assemblies/assembly_enabling-accessibility-for-visually-impaired-users.adoc[leveloffset=+1]

include::assemblies/assembly_special-characters-in-gnome.adoc[leveloffset=+1]

include::modules/desktop/proc_managing-gnome-shell-extensions-via-command-line.adoc[leveloffset=+1]

include::assemblies/assembly_enabling-and-enforcing-gnome-shell-extensions.adoc[leveloffset=+1]
