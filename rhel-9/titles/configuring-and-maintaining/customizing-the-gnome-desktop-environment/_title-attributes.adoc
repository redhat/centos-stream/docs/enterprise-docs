// Show the table of contents
:toc:
// The name of the title
:ProjectName: Customizing the GNOME desktop environment
// The subtitle of the title
:Subtitle: Customizing the GNOME desktop environment on Red Hat Enterprise Linux 9
// The abstract of the title
:Abstract: This document describes how to customize GNOME, which is the only desktop environment available in RHEL 9. It covers the instructions for users and system administrators for configuring GNOME to meet various use cases.
// The name of the title for the purposes of {context}
:ProjectNameID: customizing-the-gnome-desktop-environment

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}9

