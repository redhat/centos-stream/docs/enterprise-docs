// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included rhel-8/assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included rhel-8/assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:mailserver-title:

// include::common-content/beta.adoc[leveloffset=+1]

// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-and-maintaining-a-dovecot-imap-and-pop3-server.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_mail-transport-agent.adoc[leveloffset=+1]

