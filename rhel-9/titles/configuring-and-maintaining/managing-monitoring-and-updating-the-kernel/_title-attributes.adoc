// Show the table of contents
:toc:
// The name of the title
:ProjectName: Managing, monitoring, and updating the kernel
// The subtitle of the title
:Subtitle: A guide to managing the Linux kernel on Red Hat Enterprise Linux 9
// The abstract of the title
:Abstract: As a system administrator, you can configure the Linux kernel to optimize the operating system. Changes to the Linux kernel can improve system performance, security, and stability, as well as your ability to audit the system and troubleshoot problems.
// The name of the title for the purposes of {context}
:ProjectNameID: managing-monitoring-and-updating-the-kernel

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
