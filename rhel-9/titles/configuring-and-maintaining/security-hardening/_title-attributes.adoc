// Show the table of contents
:toc:
// The name of the title
:ProjectName: Security hardening
// The subtitle of the title
:Subtitle: Enhancing security of Red Hat Enterprise Linux 9 systems
// The abstract of the title
:Abstract: Learn the processes and practices for securing Red{nbsp}Hat Enterprise{nbsp}Linux servers and workstations against local and remote intrusion, exploitation, and malicious activity. By using these approaches and tools, you can create a more secure computing environment for the data center, workplace, and home.
// The name of the title for the purposes of {context}
:ProjectNameID: security-hardening

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}9
