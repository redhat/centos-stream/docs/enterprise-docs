:_mod-docs-content-type: CONCEPT
[preface]
[id="beta-changes_{context}"]
= Beta changes

// When on a Beta branch, copy this file to your title's directory and add links to the highlighted content.
//
// Only reference major documentation updates and new stories in this module. Lists of fixed DDFs, CLP, MS4, or other internal updates are not needed here.

The following list highlights parts of the documentation that have been newly added or updated as part of this Beta preview:

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9-beta/html-single/security_hardening/index#assembly_ensuring-system-integrity-with-keylime_security-hardening[Chapter 8. Ensuring system integrity with Keylime] is updated with instructions on how to configure the new Keylime runtime policy that combines the allow and exclude lists.
