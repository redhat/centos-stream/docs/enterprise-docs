include::common-content/_attributes.adoc[]

include::_title-attributes.adoc[]

[id="{ProjectNameID}"]
= {ProjectName}

:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

:cloud-content-azure:

// include::common-content/beta.adoc[leveloffset=+1]

// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_introducing-rhel-on-public-cloud-platforms.adoc[leveloffset=+1]

include::rhel-8/modules/composer/proc_pushing-vhd-imaged-to-azure-cloud.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_deploying-a-virtual-machine-on-microsoft-azure.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-rhel-high-availability-on-azure.adoc[leveloffset=+1]