// Show the table of contents
:toc:
// The name of the title
:ProjectName: Using systemd unit files to customize and optimize your system
// The subtitle of the title
:Subtitle: Optimize system performance and extend configuration with systemd
// The abstract of the title
:Abstract: Modify the systemd unit files and extend the default configuration, examine the system boot performance and optimize systemd to shorten the boot time.
// The name of the title for the purposes of {context}
:ProjectNameID: working-with-systemd

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
