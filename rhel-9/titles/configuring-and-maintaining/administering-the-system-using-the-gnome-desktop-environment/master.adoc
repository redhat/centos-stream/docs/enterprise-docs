// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:administering-the-system-using-the-gnome-desktop-environment:


include::rhel-8/common-content/snip_3d-acceleration-with-gnome.adoc[]

// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_installing-software-in-gnome.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_installing-applications-using-flatpak.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_registering-the-system-for-updates-using-gnome.adoc[leveloffset=+1]

include::modules/core-services/proc_changing-the-language-using-desktop-gui.adoc[leveloffset=+1]

include::modules/desktop/proc_changing-how-your-system-behaves-when-you-close-the-laptop-lid.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_changing-the-power-button-behavior.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_displaying-the-system-security-classification.adoc[leveloffset=+1]

include::rhel-8/modules/desktop/proc_setting-a-default-desktop-session-for-all-users.adoc[leveloffset=+1]

include::assemblies/assembly_setting-up-a-printer-in-gnome.adoc[leveloffset=+1]

include::assemblies/assembly_modifying-printer-settings-in-gnome.adoc[leveloffset=+1]

include::assemblies/assembly_browsing-files-on-a-network-share.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_managing-storage-volumes-in-gnome.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_troubleshooting-volume-management-in-gnome.adoc[leveloffset=+1]

include::rhel-8/modules/filesystems-and-storage/proc_configuring-gnome-to-store-user-settings-on-home-directories-hosted-on-an-nfs-share.adoc[leveloffset=+1][leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-file-associations.adoc[leveloffset=+1]

