// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:tuning-performance-in-idm:

// Use the IdM-specific "Making..inclusive" module
// include::rhel-8/common-content/making-open-source-more-inclusive-idm.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

// RHEL 9 assembly: include::assemblies/assembly_mixing-unrelated-items.adoc[leveloffset=+1]
// RHEL 9 module:   include::modules/_examples/con_the-metamorphosis.adoc[leveloffset=+1]
// RHEL 8 module:   include::rhel-8/modules/security/con_system-wide-crypto-policies.adoc[leveloffset=+1]
// RHEL 8 assembly: include::rhel-8/assemblies/assembly_protecting-systems-against-intrusive-usb-devices.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/con_important-considerations-when-tuning-idm.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/con_hardware-recommendations.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/ref_idm-server-performance-recommendations.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/ref_failover-load-balancing-high-availability.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_optimizing-the-replica-topology.adoc[leveloffset=+1]
//       con_guidelines-for-determining-the-appropriate-number-of-replicas-fin.adoc[leveloffset=+1]
//       con_guidelines-for-connecting-the-replicas-in-a-topology-fin.adoc[leveloffset=+1]
//       ref_replica-topology-examples-fin.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_adjusting-the-search-size-and-time-limit.adoc[leveloffset=+1]
//       proc_adjusting-the-search-size-and-time-limit-in-the-command-line.adoc[leveloffset=+1]
//       proc_adjusting-the-search-size-and-time-limit-in-the-web-ui.adoc

include::rhel-8/assemblies/assembly_adjusting-idm-directory-server-performance.adoc[leveloffset=+1]
//       proc_adjusting-the-entry-cache-size.adoc
//       proc_adjusting-the-database-index-cache-size.adoc
//       proc_re-enabling-database-and-entry-cache-auto-sizing.adoc
//       proc_adjusting-the-dn-cache-size.adoc
//       proc_adjusting-the-normalized-dn-cache-size.adoc
//       proc_adjusting-the-maximum-message-size.adoc
//       proc_adjusting-the-maximum-number-of-file-descriptors.adoc
//       proc_adjusting-the-connection-backlog-size.adoc
//       proc_adjusting-the-maximum-number-of-database-locks.adoc
//       proc_adjusting-the-input-output-block-timeout.adoc
//       proc_adjusting-the-idle-connection-timeout.adoc
//       proc_adjusting-replication-release-timeout.adoc

include::rhel-8/assemblies/assembly_adjusting-the-performance-of-the-kdc.adoc[leveloffset=+1]
//       proc_adjusting-the-length-of-the-kdc-listen-queue.adoc
//       ref_options-controlling-kdc-behavior-per-realm.adoc
//       proc_adjusting-kdc-settings-per-realm.adoc
//       proc_adjusting-the-number-of-krb5kdc-processes.adoc

include::rhel-8/assemblies/assembly_tuning-sssd-performance-for-large-idm-ad-trust-deployments.adoc[leveloffset=+1]
//       proc_tuning-sssd-in-idm-servers-for-large-idm-ad-trust-deployments.adoc
//       proc_tuning-the-ipa-extdom-plugin-config-timeout-on-idm-servers.adoc
//       proc_tuning-the-ipa-extdom-plugin-buffer-size-on-idm-servers.adoc
//       proc_tuning-sssd-in-idm-clients-for-large-idm-ad-trust-deployments.adoc
//       proc_mounting-the-sssd-cache-in-tmpfs.adoc
//       ref_options-for-tuning-sssd-in-servers-and-clients.adoc

include::rhel-8/assemblies/assembly_tuning-the-wsgi-processes.adoc[leveloffset=+1]
