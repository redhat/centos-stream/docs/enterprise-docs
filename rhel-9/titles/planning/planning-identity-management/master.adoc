// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:planning-identity-management:

// include::common-content/beta.adoc[leveloffset=+1]

// include::common-content/making-open-source-more-inclusive-idm.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

// RHEL 9 assembly - include::assemblies/assembly_mixing-unrelated-items.adoc[leveloffset=+1]
// RHEL 9 module --- include::modules/_examples/con_the-metamorphosis.adoc[leveloffset=+1]
// RHEL 8 module --- include::rhel-8/modules/security/con_system-wide-crypto-policies.adoc[leveloffset=+1]
// RHEL 8 assembly - include::rhel-8/assemblies/assembly_protecting-systems-against-intrusive-usb-devices.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_overview-of-identity-management-and-access-control.adoc[leveloffset=+1]

:context: {ProjectNameID}
include::rhel-8/modules/identity-management/ref_failover-load-balancing-high-availability.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_planning-the-replica-topology-fin.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_planning-your-dns-services-and-host-names.adoc[leveloffset=+1]

include::assemblies/assembly_planning-your-ca-services.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_planning-integration-with-ad-fin.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_planning-a-cross-forest-trust-between-ipa-and-ad.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_backing-up-and-restoring-idm.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_backing-up-and-restoring-idm-servers-using-ansible-playbooks.adoc[leveloffset=+1]

include::rhel-8/modules/identity-management/ref_idm-integration-with-other-red-hat-products.adoc[leveloffset=+1]

include::rhel-8/assemblies/assembly_configuring-single-sign-on-for-the-rhel-8-web-console-in-the-idm-domain.adoc[leveloffset=+1]
//       proc_creating-an-ansible-inventory-file-for-idm.adoc
//       proc_using-ansible-to-create-a-backup-of-an-idm-server.adoc
//       proc_using-ansible-to-create-a-backup-of-an-idm-server-on-your-ansible-controller.adoc
//       proc_using-ansible-to-copy-a-backup-of-an-idm-server-to-your-ansible-controller.adoc
//       proc_using-ansible-to-copy-a-backup-of-an-idm-server-from-your-ansible-controller-to-the-idm-server.adoc
//       proc_using-ansible-to-remove-a-backup-from-an-idm-server.adoc
//       proc_using-ansible-to-restore-an-idm-server-from-a-backup-stored-on-the-server.adoc
//       proc_using-ansible-to-restore-an-idm-server-from-a-backup-stored-on-your-ansible-controller.adoc

include::rhel-8/modules/identity-management/ref_idm-directory-server-rfc-support.adoc[leveloffset=+1]
