// Show the table of contents
:toc:
// The name of the title
:ProjectName: Considerations in adopting RHEL 9
// The subtitle of the title
:Subtitle: Key differences between RHEL 8 and RHEL 9
// The abstract of the title
:Abstract:  This document provides an overview of changes in RHEL 9 since RHEL 8 to help you evaluate an upgrade to RHEL 9.
// The name of the title for the purposes of {context}
:ProjectNameID: considerations-in-adopting-RHEL-9

// The following are not required
:ProjectVersion: 0.1

// Product-specific
//:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
