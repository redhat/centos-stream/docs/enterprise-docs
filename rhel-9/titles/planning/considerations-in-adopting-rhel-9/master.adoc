// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_title-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
//:choose-your-title-id:


// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

[id="preface_{context}"]
== Preface

This document provides an overview of differences between two major versions of {RHEL}: RHEL 8 and RHEL 9. It provides a list of changes relevant for evaluating an upgrade to RHEL 9 rather than an exhaustive list of all alterations.

For details regarding RHEL 9 usage, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9[RHEL 9 product documentation].

For guidance regarding an in-place upgrade from RHEL 8 to RHEL 9, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/upgrading_from_rhel_8_to_rhel_9/index[Upgrading from RHEL 8 to RHEL 9].

For information about major differences between RHEL 7 and RHEL 8, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/considerations_in_adopting_rhel_8/index[Considerations in adopting RHEL 8].

Capabilities and limits of Red Hat Enterprise Linux 9 as compared to other versions of the system are available in the Knowledgebase article link:https://access.redhat.com/articles/rhel-limits[Red Hat Enterprise Linux technology capabilities and limits].

Information regarding the Red Hat Enterprise Linux life cycle is provided in the link:https://access.redhat.com/support/policy/updates/errata/[Red Hat Enterprise Linux Life Cycle] document.

The link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/package_manifest/[Package manifest] document provides a package listing for RHEL 9, including licenses and application compatibility levels.

Application compatibility levels are explained in the link:https://access.redhat.com/articles/rhel9-abi-compatibility[Red Hat Enterprise Linux 9: Application Compatibility Guide] document.

//Generic chapters:
include::modules/upgrades-and-differences/ref_architectures.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_repositories.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_application-streams.adoc[leveloffset=+1]

//Subsystem-specific chapters:

include::assemblies/assembly_cloud.adoc[leveloffset=+1]

include::assemblies/assembly_containers.adoc[leveloffset=+1]

include::assemblies/assembly_compilers-and-development-tools.adoc[leveloffset=+1]

include::assemblies/assembly_desktop.adoc[leveloffset=+1]

include::assemblies/assembly_dynamic-programming-languages-web-servers-database-servers.adoc[leveloffset=+1]

include::assemblies/assembly_edge.adoc[leveloffset=+1]

include::assemblies/assembly_file-systems-and-storage.adoc[leveloffset=+1]

include::assemblies/assembly_hardware-enablement.adoc[leveloffset=+1]

include::assemblies/assembly_high-availability-and-clusters.adoc[leveloffset=+1]

include::assemblies/assembly_identity-management.adoc[leveloffset=+1]

include::assemblies/assembly_infrastructure-services.adoc[leveloffset=+1]

include::assemblies/assembly_installer-and-image-creation.adoc[leveloffset=+1]

include::assemblies/assembly_kernel.adoc[leveloffset=+1]

include::assemblies/assembly_networking.adoc[leveloffset=+1]

include::assemblies/assembly_performance.adoc[leveloffset=+1]

include::assemblies/assembly_security.adoc[leveloffset=+1]

include::assemblies/assembly_shells-and-command-line-tools.adoc[leveloffset=+1]

include::assemblies/assembly_software-management.adoc[leveloffset=+1]

include::assemblies/assembly_subscription-management.adoc[leveloffset=+1]

include::assemblies/assembly_system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_virtualization.adoc[leveloffset=+1]

include::assemblies/assembly_the-web-console.adoc[leveloffset=+1]


//include::assemblies/assembly_dotnet.adoc[leveloffset=+1]

//include::assemblies/assembly_internationalization.adoc[leveloffset=+1]

//include::assemblies/assembly_rhel-for-sap-solutions.adoc[leveloffset=+1]
include::assemblies/assembly_changes-to-packages.adoc[leveloffset=+1]
////
[id='related-information-{context}']
== Related information
////


