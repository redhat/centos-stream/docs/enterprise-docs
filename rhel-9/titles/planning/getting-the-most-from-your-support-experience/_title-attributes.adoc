// Show the table of contents
:toc:
// The name of the title
:ProjectName: Getting the most from your Support experience
// The subtitle of the title
:Subtitle: Gathering troubleshooting information from RHEL servers with the sos utility
// The abstract of the title
:Abstract: Collect configuration, diagnostic, and troubleshooting data with the sos utility and provide those files to Red Hat Technical Support. The Support team can analyze and investigate this data to resolve your service requests reported in your support case.
// The name of the title for the purposes of {context}
:ProjectNameID: getting-the-most-from-your-support-experience

// The following are not required
:ProjectVersion: 0.1

// Product-specific
:PRODUCT: Red{nbsp}Hat Enterprise{nbsp}Linux 9
