:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_scanning-the-system-for-configuration-compliance-and-vulnerabilities.adoc

[id="scanning-container-and-container-images-for-vulnerabilities_{context}"]
= Scanning container and container images for vulnerabilities

[role="_abstract"]
Use this procedure to find security vulnerabilities in a container or a container image.

ifeval::[{ProductNumber} == 8]
[NOTE]
The `oscap-podman` command is available from {ProductShortName} 8.2. For {ProductShortName} 8.1 and 8.0, use the workaround described in the link:https://access.redhat.com/articles/4392051[Using OpenSCAP for scanning containers in {ProductShortName} 8] Knowledgebase article.
endif::[]

.Prerequisites

* The `openscap-utils` and `bzip2` packages are installed.

.Procedure

. Download the latest RHSA OVAL definitions for your system:
+
[subs="quotes,attributes"]
----
ifeval::[{ProductNumber} == 8]
# *wget -O - https://www.redhat.com/security/data/oval/v2/RHEL8/rhel-8.oval.xml.bz2 | bzip2 --decompress > rhel-8.oval.xml*
endif::[]

ifeval::[{ProductNumber} == 9]
# *wget -O - https://www.redhat.com/security/data/oval/v2/RHEL9/rhel-9.oval.xml.bz2 | bzip2 --decompress > rhel-9.oval.xml*
endif::[]
----
. Get the ID of a container or a container image, for example:
+
[subs="quotes,attributes"]
----
# *podman images*
REPOSITORY                            TAG      IMAGE ID       CREATED       SIZE
ifeval::[{ProductNumber} == 8]
registry.access.redhat.com/ubi8/ubi   latest   096cae65a207   7 weeks ago   239 MB
endif::[]
ifeval::[{ProductNumber} == 9]
registry.access.redhat.com/ubi9/ubi   latest   096cae65a207   7 weeks ago   239 MB
endif::[]
----
. Scan the container or the container image for vulnerabilities and save results to the _vulnerability.html_ file:
+
[subs="quotes,attributes"]
----
ifeval::[{ProductNumber} == 8]
# *oscap-podman _096cae65a207_ oval eval --report _vulnerability.html_ rhel-8.oval.xml*
endif::[]

ifeval::[{ProductNumber} == 9]
# *oscap-podman _096cae65a207_ oval eval --report _vulnerability.html_ rhel-9.oval.xml*
endif::[]
----
+
Note that the `oscap-podman` command requires root privileges, and the ID of a container is the first argument.

.Verification

* Check the results in a browser of your choice, for example:
+
[subs="quotes,attributes"]
----
$ *firefox _vulnerability.html_ &amp;*
----

[role="_additional-resources"]
.Additional resources
* For more information, see the `oscap-podman(8)` and `oscap(8)` man pages.
