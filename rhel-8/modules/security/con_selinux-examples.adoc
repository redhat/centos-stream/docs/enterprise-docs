:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// assembly_getting-started-with-selinux.adoc

[id="selinux-examples_{context}"]
= SELinux examples

The following examples demonstrate how SELinux increases security:

* The default action is deny. If an SELinux policy rule does not exist to allow access, such as for a process opening a file, access is denied.

* SELinux can confine Linux users. A number of confined SELinux users exist in the SELinux policy. Linux users can be mapped to confined SELinux users to take advantage of the security rules and mechanisms applied to them. For example, mapping a Linux user to the SELinux `user_u` user, results in a Linux user that is not able to run unless configured otherwise set user ID (setuid) applications, such as [command]`sudo` and [command]`su`.

* Increased process and data separation. The concept of SELinux _domains_ allows defining which processes can access certain files and directories. For example, when running SELinux, unless otherwise configured, an attacker cannot compromise a Samba server, and then use that Samba server as an attack vector to read and write to files used by other processes, such as MariaDB databases.

* SELinux helps mitigate the damage made by configuration mistakes. Domain Name System (DNS) servers often replicate information between each other in a zone transfer. Attackers can use zone transfers to update DNS servers with false information. When running the Berkeley Internet Name Domain (BIND) as a DNS server in {ProductShortName}, even if an administrator forgets to limit which servers can perform a zone transfer, the default SELinux policy prevent updates for zone files footnote:[Text files that include DNS information, such as hostname to IP address mappings.] that use zone transfers, by the BIND `named` daemon itself, and by other processes.

* Without SELinux, an attacker can misuse a vulnerability to path traversal on an Apache web server and access files and directories stored on the file system by using special elements such as `../`. If an attacker attempts an attack on a server running with SELinux in enforcing mode, SELinux denies access to files that the `httpd` process must not access. SELinux cannot block this type of attack completely but it effectively mitigates it.

* SELinux in enforcing mode successfully prevents exploitation of kernel NULL pointer dereference operators on non-SMAP platforms (CVE-2019-9213). Attackers use a vulnerability in the `mmap` function, which does not check mapping of a null page, for placing arbitrary code on this page.

* The `deny_ptrace` SELinux boolean and SELinux in enforcing mode protect systems from the *PTRACE_TRACEME* vulnerability (CVE-2019-13272). Such configuration prevents scenarios when an attacker can get `root` privileges.

* The `nfs_export_all_rw` and `nfs_export_all_ro` SELinux booleans provide an easy-to-use tool to prevent misconfigurations of Network File System (NFS) such as accidental sharing `/home` directories.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/articles/6964380[SELinux as a security pillar of an operating system - Real-world benefits and examples] (Red&nbsp;Hat Knowledgebase)
* link:https://access.redhat.com/articles/7047896[SELinux hardening with Ansible] (Red&nbsp;Hat Knowledgebase)
* link:https://github.com/fedora-selinux/selinux-playbooks[selinux-playbooks] Github repository with Ansible playbooks for SELinux hardening
