:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
// assembly_using-the-system-wide-cryptographic-policies

[id="system-wide-crypto-policies_{context}"]
= System-wide cryptographic policies

When a system-wide policy is set up, applications in {ProductShortName} follow it and refuse to use algorithms and protocols that do not meet the policy, unless you explicitly request the application to do so. That is, the policy applies to the default behavior of applications when running with the system-provided configuration but you can override it if required.

{ProductShortName} {ProductNumber} contains the following predefined policies:

`DEFAULT`::
The default system-wide cryptographic policy level offers secure settings for current threat models. It allows the TLS 1.2 and 1.3 protocols, as well as the IKEv2 and SSH2 protocols. The RSA keys and Diffie-Hellman parameters are accepted if they are at least 2048 bits long.

`LEGACY`::
ifeval::[{ProductNumber} == 8]
Ensures maximum compatibility with Red{nbsp}Hat Enterprise{nbsp}Linux 5 and earlier; it is less secure due to an increased attack surface. In addition to the `DEFAULT` level algorithms and protocols, it includes support for the TLS 1.0 and 1.1 protocols. The algorithms DSA, 3DES, and RC4 are allowed, while RSA keys and Diffie-Hellman parameters are accepted if they are at least 1023 bits long.
endif::[]

ifeval::[{ProductNumber} == 9]
Ensures maximum compatibility with Red{nbsp}Hat Enterprise{nbsp}Linux 6 and earlier; it is less secure due to an increased attack surface. SHA-1 is allowed to be used as TLS hash, signature, and algorithm. CBC-mode ciphers are allowed to be used with SSH. Applications using GnuTLS allow certificates signed with SHA-1. It allows the TLS 1.2 and 1.3 protocols, as well as the IKEv2 and SSH2 protocols. The RSA keys and Diffie-Hellman parameters are accepted if they are at least 2048 bits long.
endif::[]

`FUTURE`::
ifeval::[{ProductNumber} == 8]
A stricter forward-looking security level intended for testing a possible future policy. This policy does not allow the use of SHA-1 in signature algorithms. It allows the TLS 1.2 and 1.3 protocols, as well as the IKEv2 and SSH2 protocols. The RSA keys and Diffie-Hellman parameters are accepted if they are at least 3072 bits long. If your system communicates on the public internet, you might face interoperability problems.
endif::[]

ifeval::[{ProductNumber} == 9]
A stricter forward-looking security level intended for testing a possible future policy. This policy does not allow the use of SHA-1 in DNSSec or as an HMAC. SHA2-224 and SHA3-224 hashes are rejected. 128-bit ciphers are disabled. CBC-mode ciphers are disabled except in Kerberos. It allows the TLS 1.2 and 1.3 protocols, as well as the IKEv2 and SSH2 protocols. The RSA keys and Diffie-Hellman parameters are accepted if they are at least 3072 bits long. If your system communicates on the public internet, you might face interoperability problems.
endif::[]
+
[IMPORTANT]
====
Because a cryptographic key used by a certificate on the Customer Portal API does not meet the requirements by the `FUTURE` system-wide cryptographic policy, the `redhat-support-tool` utility does not work with this policy level at the moment.

To work around this problem, use the `DEFAULT` cryptographic policy while connecting to the Customer Portal API.
====

`FIPS`::
Conforms with the FIPS 140 requirements. The `fips-mode-setup` tool, which switches the {ProductShortName} system into FIPS mode, uses this policy internally. Switching to the `FIPS` policy does not guarantee compliance with the FIPS 140 standard. You also must re-generate all cryptographic keys after you set the system to FIPS mode. This is not possible in many scenarios.
+
RHEL also provides the `FIPS:OSPP` system-wide subpolicy, which contains further restrictions for cryptographic algorithms required by the Common Criteria (CC) certification. The system becomes less interoperable after you set this subpolicy. For example, you cannot use RSA and DH keys shorter than 3072 bits, additional SSH algorithms, and several TLS groups. Setting `FIPS:OSPP` also prevents connecting to Red&nbsp;Hat Content Delivery Network (CDN) structure. Furthermore, you cannot integrate Active Directory (AD) into the IdM deployments that use `FIPS:OSPP`, communication between RHEL hosts using `FIPS:OSPP` and AD domains might not work, or some AD accounts might not be able to authenticate.
+
[NOTE]
====
Your system is not CC-compliant after you set the `FIPS:OSPP` cryptographic subpolicy. The only correct way to make your RHEL system compliant with the CC standard is by following the guidance provided in the `cc-config` package. See link:https://access.redhat.com/en/compliance/common-criteria[Common Criteria] section on the link:https://access.redhat.com/en/compliance[Product compliance] Red&nbsp;Hat Customer Portal page for a list of certified RHEL versions, validation reports, and links to CC guides.
====

Red Hat continuously adjusts all policy levels so that all libraries provide secure defaults, except when using the `LEGACY` policy. Even though the `LEGACY` profile does not provide secure defaults, it does not include any algorithms that are easily exploitable. As such, the set of enabled algorithms or acceptable key sizes in any provided policy may change during the lifetime of {ProductName}.

Such changes reflect new security standards and new security research. If you must ensure interoperability with a specific system for the whole lifetime of {ProductName}, you should opt-out from the system-wide cryptographic policies for components that interact with that system or re-enable specific algorithms using custom cryptographic policies.

The specific algorithms and ciphers described as allowed in the policy levels are available only if an application supports them:

ifeval::[{ProductNumber} == 8]
.Cipher suites and protocols enabled in the cryptographic policies
[cols="h,d,d,d,d"]
|===
|| `LEGACY` | `DEFAULT` | `FIPS` | `FUTURE`

| IKEv1
| no
| no
| no
| no

| 3DES
| yes
| no
| no
| no

| RC4
| yes
| no
| no
| no

| DH
| min. 1024-bit
| min. 2048-bit
| min. 2048-bitfootnote:[You can use only Diffie-Hellman groups defined in RFC 7919 and RFC 3526.]
| min. 3072-bit

| RSA
| min. 1024-bit
| min. 2048-bit
| min. 2048-bit
| min. 3072-bit

| DSA
| yes
| no
| no
| no

| TLS v1.0
| yes
| no
| no
| no

| TLS v1.1
| yes
| no
| no
| no

| SHA-1 in digital signatures
| yes
| yes
| no
| no

| CBC mode ciphers
| yes
| yes
| yes
| nofootnote:[CBC ciphers are disabled for TLS. In a non-TLS scenario, `AES-128-CBC` is disabled but `AES-256-CBC` is enabled. To disable also `AES-256-CBC`, apply a custom subpolicy.]

| Symmetric ciphers with keys < 256 bits
| yes
| yes
| yes
| no

| SHA-1 and SHA-224 signatures in certificates
| yes
| yes
| yes
| no

|===
endif::[]

ifeval::[{ProductNumber} == 9]
.Cipher suites and protocols enabled in the cryptographic policies
[cols="h,d,d,d,d"]
|===
|| `LEGACY` | `DEFAULT` | `FIPS` | `FUTURE`

| IKEv1
| no
| no
| no
| no

| 3DES
| no
| no
| no
| no

| RC4
| no
| no
| no
| no

| DH
| min. 2048-bit
| min. 2048-bit
| min. 2048-bit
| min. 3072-bit

| RSA
| min. 2048-bit
| min. 2048-bit
| min. 2048-bit
| min. 3072-bit

| DSA
| no
| no
| no
| no

| TLS v1.1 and older
| no
| no
| no
| no

| TLS v1.2 and newer
| yes
| yes
| yes
| yes

| SHA-1 in digital signatures and certificates
| yes
| no
| no
| no

| CBC mode ciphers
| yes
| nofootnote:[CBC ciphers are disabled for SSH]
| nofootnote:[CBC ciphers are disabled for all protocols except Kerberos]
| nofootnote:[CBC ciphers are disabled for all protocols except Kerberos]

| Symmetric ciphers with keys < 256 bits
| yes
| yes
| yes
| no

|===
endif::[]

[role="_additional-resources"]
.Additional resources
* `crypto-policies(7)` and `update-crypto-policies(8)` man pages on your system
* link:https://access.redhat.com/en/compliance[Product compliance] (Red&nbsp;Hat Customer Portal)
