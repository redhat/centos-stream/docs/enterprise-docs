:_mod-docs-content-type: PROCEDURE

[id="securing-the-rpcbind-service_{context}"]
= Securing the rpcbind service

The `rpcbind` service is a dynamic port-assignment daemon for remote procedure calls (RPC) services such as Network Information Service (NIS) and Network File System (NFS). Because it has weak authentication mechanisms and can assign a wide range of ports for the services it controls, it is important to secure `rpcbind`.

You can secure `rpcbind` by restricting access to all networks and defining specific exceptions using firewall rules on the server.

[NOTE]
====
ifeval::[{ProductNumber} == 8]
* The `rpcbind` service is required on `NFSv2` and `NFSv3` servers.
endif::[]

ifeval::[{ProductNumber} == 9]
* The `rpcbind` service is required on `NFSv3` servers.
endif::[]


* The `rpcbind` service is not required on `NFSv4`.
====

.Prerequisites

* The `rpcbind` package is installed.
* The `firewalld` package is installed and the service is running.

.Procedure

. Add firewall rules, for example:
+
** Limit TCP connection and accept packages only from the `192.168.0.0/24` host via the `111` port:
+
[subs=quotes]
----
# *firewall-cmd --add-rich-rule='rule family="ipv4" port port="111" protocol="tcp" source address="192.168.0.0/24" invert="True" drop'*
----
+
** Limit TCP connection and accept packages only from local host via the `111` port:
+
[subs=quotes]
----
# *firewall-cmd --add-rich-rule='rule family="ipv4" port port="111" protocol="tcp" source address="127.0.0.1" accept'*
----
+
** Limit UDP connection and accept packages only from the `192.168.0.0/24` host via the `111` port:
+
[subs=quotes]
----
# *firewall-cmd --permanent --add-rich-rule='rule family="ipv4" port port="111" protocol="udp" source address="192.168.0.0/24" invert="True" drop'*
----
+
To make the firewall settings permanent, use the `--permanent` option when adding firewall rules.

. Reload the firewall to apply the new rules:
+
[subs=quotes]
----
# *firewall-cmd --reload*
----

.Verification

* List the firewall rules:
+
[subs=quotes]
----
# *firewall-cmd --list-rich-rule*
rule family="ipv4" port port="111" protocol="tcp" source address="192.168.0.0/24" invert="True" drop
rule family="ipv4" port port="111" protocol="tcp" source address="127.0.0.1" accept
rule family="ipv4" port port="111" protocol="udp" source address="192.168.0.0/24" invert="True" drop
----

[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* For more information about `NFSv4-only` servers, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/deploying_different_types_of_servers/deploying-an-nfs-server_deploying-different-types-of-servers#configuring-an-nfsv4-only-server_deploying-an-nfs-server[Configuring an NFSv4-only server].
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/using-and-configuring-firewalld_securing-networks[Using and configuring firewalld]
endif::[]
ifeval::[{ProductNumber} == 9]
* For more information about `NFSv4-only` servers, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_using_network_file_services/deploying-an-nfs-server_configuring-and-using-network-file-services#configuring-an-nfsv4-only-server_deploying-an-nfs-server[Configuring an NFSv4-only server]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_firewalls_and_packet_filters/using-and-configuring-firewalld_firewall-packet-filters[Using and configuring firewalld]
endif::[]
