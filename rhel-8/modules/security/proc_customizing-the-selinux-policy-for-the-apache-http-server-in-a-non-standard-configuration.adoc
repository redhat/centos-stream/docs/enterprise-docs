:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_configuring-selinux-for-applications-and-services-with-non-standard-configurations

[id="customizing-the-selinux-policy-for-the-apache-http-server-in-a-non-standard-configuration_{context}"]
= Customizing the SELinux policy for the Apache HTTP server in a non-standard configuration

You can configure the Apache HTTP server to listen on a different port and to provide content in a non-default directory. To prevent consequent SELinux denials, follow the steps in this procedure to adjust your system's SELinux policy.

.Prerequisites

* The `httpd` package is installed and the Apache HTTP server is configured to listen on TCP port 3131 and to use the `/var/test_www/` directory instead of the default `/var/www/` directory.
* The [package]`policycoreutils-python-utils` and [package]`setroubleshoot-server` packages are installed on your system.

.Procedure

. Start the `httpd` service and check the status:
+
[subs="quotes"]
----
# *systemctl start httpd*
# *systemctl status httpd*
…
httpd[14523]: (13)Permission denied: AH00072: make_sock: could not bind to address [::]:3131
…
systemd[1]: Failed to start The Apache HTTP Server.
…
----
. The SELinux policy assumes that `httpd` runs on port 80:
+
[subs="quotes"]
----
# *semanage port -l | grep http*
http_cache_port_t              tcp      8080, 8118, 8123, 10001-10010
http_cache_port_t              udp      3130
http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
pegasus_http_port_t            tcp      5988
pegasus_https_port_t           tcp      5989
----
. Change the SELinux type of port 3131 to match port 80:
+
[subs="quotes"]
----
# *semanage port -a -t http_port_t -p tcp 3131*
----
. Start `httpd` again:
+
[subs="quotes"]
----
# *systemctl start httpd*
----
. However, the content remains inaccessible:
+
[subs="quotes"]
----
# *wget localhost:3131/index.html*
…
HTTP request sent, awaiting response... 403 Forbidden
…
----
+
Find the reason with the `sealert` tool:
+
[subs="macros,quotes"]
----
# *sealert -l "pass:[*]"*
...
SELinux is preventing httpd from getattr access on the file /var/test_www/html/index.html.
…
----
. Compare SELinux types for the standard and the new path using the `matchpathcon` tool:
+
[subs="quotes"]
----
# *matchpathcon /var/www/html /var/test_www/html*
/var/www/html       system_u:object_r:httpd_sys_content_t:s0
/var/test_www/html  system_u:object_r:var_t:s0
----

. Change the SELinux type of the new `/var/test_www/html/` content directory to the type of the default `/var/www/html` directory:
+
[subs="quotes"]
----
# *semanage fcontext -a -e /var/www /var/test_www*
----
. Relabel the `/var` directory recursively:
+
[subs="quotes"]
----
# *restorecon -Rv /var/*
...
Relabeled /var/test_www/html from unconfined_u:object_r:var_t:s0 to unconfined_u:object_r:httpd_sys_content_t:s0
Relabeled /var/test_www/html/index.html from unconfined_u:object_r:var_t:s0 to unconfined_u:object_r:httpd_sys_content_t:s0
----

.Verification

. Check that the `httpd` service is running:
+
[subs="quotes,attributes"]
----
# *systemctl status httpd*
…
Active: active (running)
…
systemd[1]: Started The Apache HTTP Server.
httpd[14888]: Server configured, listening on: port 3131
…
----
. Verify that the content provided by the Apache HTTP server is accessible:
+
[subs="quotes,attributes"]
----
# *wget localhost:3131/index.html*
…
HTTP request sent, awaiting response... 200 OK
Length: 0 [text/html]
Saving to: ‘index.html’
…
----

[role="_additional-resources"]
.Additional resources
* `semanage(8)`, `matchpathcon(8)`, and `sealert(8)` man pages on your system
