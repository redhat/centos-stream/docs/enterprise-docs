:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_scanning-the-system-for-configuration-compliance-and-vulnerabilities.adoc

[id="creating-a-remediation-bash-script-for-a-later-application_{context}"]
= Creating a remediation Bash script for a later application

[role="_abstract"]
Use this procedure to create a Bash script containing remediations that align your system with a security profile such as HIPAA. Using the following steps, you do not do any modifications to your system, you only prepare a file for later application.

.Prerequisites

* The `scap-security-guide` package is installed on your {ProductShortName} system.

.Procedure

. Use the `oscap` command to scan the system and to save the results to an XML file. In the following example, `oscap` evaluates the system against the `hipaa` profile:
+
[subs="quotes,attributes"]
----
ifeval::[{ProductNumber} == 8]
# *oscap xccdf eval --profile hipaa --results _&lt;hipaa-results.xml&gt;_ /usr/share/xml/scap/ssg/content/ssg-rhel8-ds.xml*
endif::[]

ifeval::[{ProductNumber} == 9]
# *oscap xccdf eval --profile hipaa --results _&lt;hipaa-results.xml&gt;_ /usr/share/xml/scap/ssg/content/ssg-rhel9-ds.xml*
endif::[]
----

. Find the value of the result ID in the file with the results:
+
[subs="quotes,attributes"]
----
# *oscap info _&lt;hipaa-results.xml&gt;_*
----

. Generate a Bash script based on the results file generated in step 1:
+
[subs="quotes,attributes"]
----
# *oscap xccdf generate fix --fix-type bash --result-id _&lt;xccdf_org.open-scap_testresult_xccdf_org.ssgproject.content_profile_hipaa&gt;_ --output _&lt;hipaa-remediations.sh&gt;_ _&lt;hipaa-results.xml&gt;_*
----
. The `_&lt;hipaa-remediations.sh&gt;_` file contains remediations for rules that failed during the scan performed in step 1. After reviewing this generated file, you can apply it with the `./_&lt;hipaa-remediations.sh&gt;_` command when you are in the same directory as this file.

.Verification

* In a text editor of your choice, review that the `_&lt;hipaa-remediations.sh&gt;_` file contains rules that failed in the scan performed in step 1.

[role="_additional-resources"]
.Additional resources
* `scap-security-guide(8)`, `oscap(8)`, and `bash(1)` man pages on your system
