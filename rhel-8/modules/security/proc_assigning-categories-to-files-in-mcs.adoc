:_mod-docs-content-type: PROCEDURE

[id="proc_assigning-categories-to-files-in-mcs_{context}"]
= Assigning categories to files in MCS

You need administrative privileges to assign categories to users. Users can then assign categories to files. To modify the categories of a file, users must have access rights to that file. Users can only assign a file to a category that is assigned to them.

[NOTE]
====
The system combines category access rules with conventional file access permissions. For example, if a user with a category of `bigfoot` uses Discretionary Access Control (DAC) to block access to a file by other users, other `bigfoot` users cannot access that file. A user assigned to all available categories still may not be able to access the entire file system.
====

.Prerequisites

* The SELinux mode is set to `enforcing`.

* The SELinux policy is set to `targeted` or `mls`.

* The `policycoreutils-python-utils` package is installed.

* Access and permissions to a Linux user that is:
** Assigned to an SELinux user.
** Assigned to the category to which you want to assign the file. For additional information, see
ifdef::using-selinux[]
xref:proc_assigning-categories-to-users-in-mcs_assembly_using-multi-category-security-mcs-for-data-confidentiality[Assigning categories to users in MCS]
endif::[]
ifndef::using-selinux[]
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/using_selinux/index#proc_assigning-categories-to-users-in-mcs_assembly_using-multi-category-security-mcs-for-data-confidentiality[Assigning categories to users in MCS]
endif::[]
.

* Access and permissions to the file you want to add to the category.

* For verification purposes: Access and permissions to a Linux user not assigned to this category

.Procedure

* Add categories to a file:
+
[subs="quotes"]
----
$ *chcat -- +_&lt;category1&gt;_,+_&lt;category2&gt;_  _&lt;path/to/file1&gt;_*
----
+
Use category numbers `c0` to `c1023` or category labels as defined in the `setrans.conf` file. For additional information, see
ifdef::using-selinux[]
xref:proc_defining-category-labels-in-mcs_assembly_using-multi-category-security-mcs-for-data-confidentiality[Defining category labels in MCS]
endif::[]
ifndef::using-selinux[]
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/using_selinux/index#proc_defining-category-labels-in-mcs_assembly_using-multi-category-security-mcs-for-data-confidentiality[Defining category labels in MCS]
endif::[]
.
+
You can remove categories from a file by using the same syntax:
+
[subs="quotes"]
----
$ *chcat -- -_&lt;category1&gt;_,-_&lt;category2&gt;_ _&lt;path/to/file1&gt;_*
----
+
[NOTE]
When removing a category, you must specify `--` on the command line before using the `-_&lt;category&gt;_` syntax. Otherwise, the `chcat` command misinterprets the category removal as a command option.

.Verification

. Display the security context of the file to verify that it has the correct categories:
+
[subs="quotes"]
----
$ *ls -lZ _&lt;path/to/file&gt;_*
-rw-r--r--  _&lt;LinuxUser1&gt;_ _&lt;Group1&gt;_ root:object_r:user_home_t:_&lt;sensitivity&gt;_:_&lt;category&gt;_ _&lt;path/to/file&gt;_
----
+
The specific security context of the file may differ.

. Optional: Attempt to access the file when logged in as a Linux user not assigned to the same category as the file:
+
[subs="quotes"]
----
$ *cat _&lt;path/to/file&gt;_*
cat: _&lt;path/to/file&gt;_: Permission Denied
----

[role="_additional-resources"]
.Additional resources
* `semanage(8)` and `chcat(8)` man pages on your system
