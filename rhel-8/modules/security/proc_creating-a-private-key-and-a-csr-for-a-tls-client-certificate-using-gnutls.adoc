:_mod-docs-content-type: PROCEDURE

[id="creating-a-private-key-and-a-csr-for-a-tls-client-certificate-using-gnutls{context}"]
= Creating a private key and a CSR for a TLS client certificate using GnuTLS

To obtain the certificate, you must create a private key and a certificate signing request (CSR) for your client first.

.Procedure

. Generate a private key on your client system, for example:
+
[subs="quotes"]
----
$ *certtool --generate-privkey --sec-param High --outfile _&lt;example-client.key&gt;_*
----

. Optional: Use a text editor of your choice to prepare a configuration file that simplifies creating your CSR, for example:
+
[subs="quotes"]
----
$ *vim _&lt;example_client.cnf&gt;_*
signing_key
encryption_key

tls_www_client

cn = "client.example.com"
email = "client@example.com"
----
+

. Create a CSR using the private key you created previously:
+
[subs="quotes"]
----
$ *certtool --generate-request --template _&lt;example-client.cfg&gt;_ --load-privkey _&lt;example-client.key&gt;_ --outfile _&lt;example-client.crq&gt;_*
----
+
If you omit the `--template` option, the `certtool` utility prompts you for additional information, for example:
+
[subs="quotes"]
----
Generating a PKCS #10 certificate request...
Country name (2 chars): _&lt;US&gt;_
State or province name: _&lt;Washington&gt;_
Locality name: _&lt;Seattle&gt;_
Organization name: _&lt;Example Organization&gt;_
Organizational unit name:
Common name: _&lt;server.example.com&gt;_
----

.Next steps

* Submit the CSR to a CA of your choice for signing. Alternatively, for an internal use scenario within a trusted network, use your private CA for signing. See
ifdef::securing-networks[]
xref:using-a-private-ca-to-issue-certificates-for-csrs-with-gnutls_creating-and-managing-tls-keys-and-certificates[]
endif::[]
ifndef::securing-networks[]
the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using-a-private-ca-to-issue-certificates-for-csrs-with-gnutls_creating-and-managing-tls-keys-and-certificates[Using a private CA to issue certificates for CSRs with GnuTLS] section
endif::[]
for more information.  


.Verification

. Check that the human-readable parts of the certificate match your requirements, for example:
+
[subs="quotes"]
----
$ *certtool --certificate-info --infile _&lt;example-client.crt&gt;_*
Certificate:
…
            X509v3 Extended Key Usage: 
                TLS Web Client Authentication
            X509v3 Subject Alternative Name: 
                email:client@example.com
…
----

[role="_additional-resources"]
.Additional resources
* `certtool(1)` man page on your system

