:_mod-docs-content-type: CONCEPT
:experimental:
// included in assembly_writing-a-custom-selinux-policy.adoc

[id="custom-selinux-policies-and-related-tools_{context}"]
= Custom SELinux policies and related tools

An SELinux security policy is a collection of SELinux rules. A policy is a core component of SELinux and is loaded into the kernel by SELinux user-space tools. The kernel enforces the use of an SELinux policy to evaluate access requests on the system. By default, SELinux denies all requests except for requests that correspond to the rules specified in the loaded policy.

Each SELinux policy rule describes an interaction between a process and a system resource:
----
ALLOW apache_process apache_log:FILE READ;
----
You can read this example rule as: _The *Apache* process can *read* its *logging file*._ In this rule, `apache_process` and `apache_log` are *labels*. An SELinux security policy assigns labels to processes and defines relations to system resources. This way, a policy maps operating-system entities to the SELinux layer.

SELinux labels are stored as extended attributes of file systems, such as `ext2`. You can list them using the `getfattr` utility or a `ls -Z` command, for example:
[subs="quotes"]
----
$ *ls -Z /etc/passwd*
system_u:object_r:passwd_file_t:s0 /etc/passwd
----
Where `system_u` is an SELinux user, `object_r` is an example of the SELinux role, and `passwd_file_t` is an SELinux domain.

The default SELinux policy provided by the `selinux-policy` packages contains rules for applications and daemons that are parts of {ProductShortName}{nbsp}{ProductNumber} and are provided by packages in its repositories. Applications not described in this distribution policy are not confined by SELinux. To change this, you have to modify the policy using a policy module, which contains additional definitions and rules.

In {ProductShortName}{nbsp}{ProductNumber}, you can query the installed SELinux policy and generate new policy modules using the `sepolicy` tool. Scripts that `sepolicy` generates together with the policy modules always contain a command using the `restorecon` utility. This utility is a basic tool for fixing labeling problems in a selected part of a file system.


[role="_additional-resources"]
.Additional resources
* `sepolicy(8)` and `getfattr(1)` man pages on your system
* link:https://access.redhat.com/articles/6999267[Quick start to write a custom SELinux policy] Knowledgebase article
