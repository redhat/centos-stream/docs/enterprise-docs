:_mod-docs-content-type: PROCEDURE

[id="proc_allowing-mls-users-to-edit-files-on-lower-levels_{context}"]
= Allowing MLS users to edit files on lower levels

By default, MLS users cannot write to files which have a sensitivity level below the lower value of the clearance range. If your scenario requires allowing users to edit files on lower levels, you can do so by creating a local SELinux module. However, writing to a file will increase its sensitivity level to the lower value of the user's current range.

.Prerequisites

* The SELinux policy is set to `mls`.
* The SELinux mode is set to `enforcing`.
* The `policycoreutils-python-utils` package is installed.
* The `setools-console` and `audit` packages for verification.

.Procedure

. Optional: Switch to permissive mode for easier troubleshooting.
+
[subs="quotes,attributes"]
----
# *setenforce 0*
----

. Open a new `.cil` file with a text editor, for example `~/local_mlsfilewrite.cil`, and insert the following custom rule:
+
----
(typeattributeset mlsfilewrite (_staff_t_))
----
+
You can replace `_staff_t_` with a different SELinux type. By specifying SELinux type here, you can control which SELinux roles can edit lower-level files.
+
To keep your local modules better organized, use the `local_` prefix in the names of local SELinux policy modules.

. Install the policy module:
+
[subs="quotes,attributes"]
----
# *semodule -i ~/_local_mlsfilewrite_.cil*
----
+
[NOTE]
====
To remove the local policy module, use `semodule -r ~/_local_mlsfilewrite_`. Note that you must refer to the module name without the `.cil` suffix.
====

. Optional: If you previously switched back to permissive mode, return to enforcing mode:
+
[subs="quotes,attributes"]
----
# *setenforce 1*
----

.Verification

. Find the local module in the list of installed SELinux modules:
+
[subs="quotes,attributes"]
----
# *semodule -lfull | grep "local_mls"*
400 local_mlsfilewrite  cil
----
+
Because local modules have priority `400`, you can list them also by using the `semodule -lfull | grep -v ^100` command.

. Log in as a user assigned to the type defined in the custom rule, for example, `_staff_t_`.

. Attempt to write to a file with a lower sensitivity level. This increases the file’s classification level to the user’s clearance level.
+
[IMPORTANT]
====
The files you use for verification should not contain any sensitive information in case the configuration is incorrect and the user actually can access the files without authorization.
====
