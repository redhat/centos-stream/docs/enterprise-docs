:_mod-docs-content-type: PROCEDURE

[id="securing-user-accounts-for-ftp_{context}"]
= Securing user accounts for FTP

FTP transmits usernames and passwords unencrypted over insecure networks for authentication. You can improve the security of FTP by denying system users access to the server from their user accounts.

Perform as many of the following steps as applicable for your configuration.

.Procedure

* Disable all user accounts in the `vsftpd` server, by adding the following line to the `/etc/vsftpd/vsftpd.conf` file:
+
----
local_enable=NO
----

* Disable FTP access for specific accounts or specific groups of accounts, such as the `root` user and users with `sudo` privileges, by adding the usernames to the `/etc/pam.d/vsftpd` PAM configuration file.

* Disable user accounts, by adding the usernames to the `/etc/vsftpd/ftpusers` file.
