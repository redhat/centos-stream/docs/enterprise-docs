:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="using-the-nbde_server-system-role-for-setting-up-multiple-tang-servers_{context}"]
= Using the `nbde_server` RHEL system role for setting up multiple Tang servers

By using the `nbde_server` system role, you can deploy and manage a Tang server as part of an automated disk encryption solution. This role supports the following features:

* Rotating Tang keys
* Deploying and backing up Tang keys


.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]


.Procedure

. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
----
---
- name: Deploy a Tang server
  hosts: tang.server.example.com
  tasks:
  - name: Install and configure periodic key rotation
    ansible.builtin.include_role:
        name: rhel-system-roles.nbde_server
    vars:
      nbde_server_rotate_keys: yes
      nbde_server_manage_firewall: true
      nbde_server_manage_selinux: true
----
+
This example playbook ensures deploying of your Tang server and a key rotation.
+
The settings specified in the example playbook include the following:
+
`nbde_server_manage_firewall: true`:: Use the `firewall` system role to manage ports used by the `nbde_server` role.
`nbde_server_manage_selinux: true`:: Use the `selinux` system role to manage ports used by the `nbde_server` role.
+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.nbde_server/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
----
$ *ansible-playbook --syntax-check ~/playbook.yml*
----
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
----
$ *ansible-playbook ~/playbook.yml*
----

.Verification

* On your NBDE client, verify that your Tang server works correctly by using the following command. The command must return the identical message you pass for encryption and decryption:
+
[subs="+quotes"]
----
# *ansible managed-node-01.example.com -m command -a 'echo test | clevis encrypt tang '{"url":"_<tang.server.example.com>_"}' -y | clevis decrypt'*
test
----


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.nbde_server/README.md` file
* `/usr/share/doc/rhel-system-roles/nbde_server/` directory
