:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_scanning-the-system-for-configuration-compliance-and-vulnerabilities.adoc

[id="viewing-profiles-for-configuration-compliance_{context}"]
= Viewing profiles for configuration compliance

Before you decide to use profiles for scanning or remediation, you can list them and check their detailed descriptions using the `oscap info` subcommand.

.Prerequisites

* The `openscap-scanner` and `scap-security-guide` packages are installed.

.Procedure

. List all available files with security compliance profiles provided by the SCAP Security Guide project:
+
[subs="+quotes,attributes"]
----
ifeval::[{ProductNumber} == 8]
$ *ls /usr/share/xml/scap/ssg/content/*
ssg-firefox-cpe-dictionary.xml  ssg-rhel6-ocil.xml
ssg-firefox-cpe-oval.xml        ssg-rhel6-oval.xml
…
ssg-rhel6-ds-1.2.xml          ssg-rhel8-oval.xml
ssg-rhel8-ds.xml              ssg-rhel8-xccdf.xml
…
endif::[]

ifeval::[{ProductNumber} == 9]
$ *ls /usr/share/xml/scap/ssg/content/*
ssg-rhel9-ds.xml
endif::[]
----

. Display detailed information about a selected data stream using the [command]`oscap info` subcommand. XML files containing data streams are indicated by the `-ds` string in their names. In the `Profiles` section, you can find a list of available profiles and their IDs:
+
[subs="+quotes,attributes"]
----
ifeval::[{ProductNumber} == 8]
$ *oscap info /usr/share/xml/scap/ssg/content/ssg-rhel8-ds.xml*
Profiles:
…
  Title: Health Insurance Portability and Accountability Act (HIPAA)
    Id: xccdf_org.ssgproject.content_profile_hipaa
  Title: PCI-DSS v3.2.1 Control Baseline for Red Hat Enterprise Linux 8
    Id: xccdf_org.ssgproject.content_profile_pci-dss
  Title: OSPP - Protection Profile for General Purpose Operating Systems
    Id: xccdf_org.ssgproject.content_profile_ospp
…
endif::[]

ifeval::[{ProductNumber} == 9]
$ *oscap info /usr/share/xml/scap/ssg/content/ssg-rhel9-ds.xml*
Profiles:
…
  Title: Australian Cyber Security Centre (ACSC) Essential Eight
    Id: xccdf_org.ssgproject.content_profile_e8
  Title: Health Insurance Portability and Accountability Act (HIPAA)
    Id: xccdf_org.ssgproject.content_profile_hipaa
  Title: PCI-DSS v3.2.1 Control Baseline for Red Hat Enterprise Linux 9
    Id: xccdf_org.ssgproject.content_profile_pci-dss
…
endif::[]
----
. Select a profile from the data stream file and display additional details about the selected profile. To do so, use [command]`oscap info` with the `--profile` option followed by the last section of the ID displayed in the output of the previous command. For example, the ID of the HIPPA profile is `xccdf_org.ssgproject.content_profile_hipaa`, and the value for the [option]`--profile` option is `hipaa`:
+
[subs="+quotes,attributes"]
----
$ *oscap info --profile hipaa /usr/share/xml/scap/ssg/content/ssg-rhel{ProductNumber}-ds.xml*
…
Profile
	Title: Health Insurance Portability and Accountability Act (HIPAA)

	Description: The HIPAA Security Rule establishes U.S. national standards to protect individuals’ electronic personal health information that is created, received, used, or maintained by a covered entity.
…
----

[role="_additional-resources"]
.Additional resources
* `scap-security-guide(8)` man page on your system
* link:https://access.redhat.com/articles/6999111[OpenSCAP memory consumption problems]
