:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="proc_setting-a-custom-cryptographic-policy-using-the-crypto-policies-system-role_{context}"]
= Enhancing security with the `FUTURE` cryptographic policy using the `crypto_policies` RHEL system role

You can use the `crypto_policies` RHEL system role to configure the `FUTURE` policy on your managed nodes. This policy helps to achieve for example:

* Future-proofing against emerging threats: anticipates advancements in computational power.
* Enhanced security: stronger encryption standards require longer key lengths and more secure algorithms.
* Compliance with high-security standards: for example in healthcare, telco, and finance the data sensitivity is high, and availability of strong cryptography is critical.

Typically, `FUTURE` is suitable for environments handling highly sensitive data, preparing for future regulations, or adopting long-term security strategies.

[WARNING]
====
Legacy systems or software does not have to support the more modern and stricter algorithms and protocols enforced by the `FUTURE` policy. For example, older systems might not support TLS 1.3 or larger key sizes. This could lead to compatibility problems.

Also, using strong algorithms usually increases the computational workload, which could negatively affect your system performance.
====


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
----
---
- name: Configure cryptographic policies
  hosts: managed-node-01.example.com
  tasks:
    - name: Configure the FUTURE cryptographic security policy on the managed node
      ansible.builtin.include_role:
        name: rhel-system-roles.crypto_policies
      vars:
        - crypto_policies_policy: _FUTURE_
        - crypto_policies_reboot_ok: true
----
+
The settings specified in the example playbook include the following:
+
--
`crypto_policies_policy: _FUTURE_`:: Configures the required cryptographic policy (`FUTURE`) on the managed node. It can be either the base policy or a base policy with some sub-policies. The specified base policy and sub-policies have to be available on the managed node. The default value is `null`. It means that the configuration is not changed and the `crypto_policies` RHEL system role will only collect the Ansible facts.

`crypto_policies_reboot_ok: _true_`:: Causes the system to reboot after the cryptographic policy change to make sure all of the services and applications will read the new configuration files. The default value is `false`.
--
+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.crypto_policies/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
----
$ *ansible-playbook --syntax-check ~/playbook.yml*
----
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
----
$ *ansible-playbook ~/playbook.yml*
----


include::common-content/snip_fips-ospp.adoc[leveloffset=+1]

.Verification

. On the control node, create another playbook named, for example, `verify_playbook.yml`:
+
[source,yaml,subs="+quotes"]
----
---
- name: Verification
  hosts: managed-node-01.example.com
  tasks:
    - name: Verify active cryptographic policy
      ansible.builtin.include_role:
        name: rhel-system-roles.crypto_policies
    - name: Display the currently active cryptographic policy
      ansible.builtin.debug:
        var: crypto_policies_active
----
+
The settings specified in the example playbook include the following:
+
`crypto_policies_active`:: An exported Ansible fact that contains the currently active policy name in the format as accepted by the `crypto_policies_policy` variable.


. Validate the playbook syntax:
+
[subs="+quotes"]
----
$ *ansible-playbook --syntax-check ~/verify_playbook.yml*
----

. Run the playbook:
+
[subs="+quotes,macros"]
----
$ *ansible-playbook ~/verify_playbook.yml*
TASK [debug] pass:[**************************]
ok: [host] => {
    "crypto_policies_active": "FUTURE"
}
----
+
The `crypto_policies_active` variable shows the active policy on the managed node.


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.crypto_policies/README.md` file
* `/usr/share/doc/rhel-system-roles/crypto_policies/` directory
* `update-crypto-policies(8)` and `crypto-policies(7)` manual pages
