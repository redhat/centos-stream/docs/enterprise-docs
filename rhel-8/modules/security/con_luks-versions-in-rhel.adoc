:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
// assembly_encrypting-block-devices-using-luks.adoc

[id="luks-versions-in-rhel_{context}"]
= LUKS versions in RHEL

In {RHEL}, the default format for LUKS encryption is LUKS2. The old LUKS1 format remains fully supported and it is provided as a format compatible with earlier {RHEL} releases. LUKS2 re-encryption is considered more robust and safe to use as compared to LUKS1 re-encryption.

The LUKS2 format enables future updates of various parts without a need to modify binary structures. Internally it uses JSON text format for metadata, provides redundancy of metadata, detects metadata corruption, and automatically repairs from a metadata copy.


ifeval::[{ProductNumber} == 8]
[IMPORTANT]
====
Do not use LUKS2 in systems that support only LUKS1 because LUKS2 and LUKS1 use different commands to encrypt the disk. Using the wrong command for a LUKS version might cause data loss.

.Encryption commands depending on the LUKS version
[options="header"]
|====
| LUKS version | Encryption command
| LUKS2 | `cryptsetup reencrypt`
| LUKS1 | `cryptsetup-reencrypt`
|====
====
endif::[]

ifeval::[{ProductNumber} >= 9]
IMPORTANT: Do not use LUKS2 in systems that support only LUKS1.

Since {RHEL} 9.2, you can use the `cryptsetup reencrypt` command for both the LUKS versions to encrypt the disk.
endif::[]


.Online re-encryption
The LUKS2 format supports re-encrypting encrypted devices while the devices are in use. For example, you do not have to unmount the file system on the device to perform the following tasks:

* Changing the volume key
* Changing the encryption algorithm
+
When encrypting a non-encrypted device, you must still unmount the file system. You can remount the file system after a short initialization of the encryption.
+
The LUKS1 format does not support online re-encryption.

.Conversion
In certain situations, you can convert LUKS1 to LUKS2. The conversion is not possible specifically in the following scenarios:

* A LUKS1 device is marked as being used by a Policy-Based Decryption (PBD) Clevis solution. The `cryptsetup` tool does not convert the device when some `luksmeta` metadata are detected.

* A device is active. The device must be in an inactive state before any conversion is possible.
