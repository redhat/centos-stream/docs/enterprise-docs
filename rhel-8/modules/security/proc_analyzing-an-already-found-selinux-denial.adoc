:_mod-docs-content-type: PROCEDURE
:experimental:
// included in troubleshooting-problems-related-to-selinux

[id="analyzing-an-already-found-selinux-denial_{context}"]
= Analyzing SELinux denial messages

[role="_abstract"]
After xref:identifying-selinux-denials_troubleshooting-problems-related-to-selinux[identifying] that SELinux is blocking your scenario, you might need to analyze the root cause before you choose a fix.

.Prerequisites

* The [package]`policycoreutils-python-utils` and [package]`setroubleshoot-server` packages are installed on your system.

.Procedure

. List more details about a logged denial using the [command]`sealert` command, for example:
+
[subs="quotes,macros"]
----
$ *sealert -l "pass:[*]"*
SELinux is preventing /usr/bin/passwd from write access on the file
/root/test.

pass:[*****  Plugin leaks (86.2 confidence) suggests *****************************]

If you want to ignore passwd trying to write access the test file,
because you believe it should not need this access.
Then you should report this as a bug.
You can generate a local policy module to dontaudit this access.
Do
# ausearch -x /usr/bin/passwd --raw | audit2allow -D -M my-passwd
# semodule -X 300 -i my-passwd.pp

pass:[*****  Plugin catchall (14.7 confidence) suggests **************************]

...

Raw Audit Messages
type=AVC msg=audit(1553609555.619:127): avc:  denied  { write } for
pid=4097 comm="passwd" path="/root/test" dev="dm-0" ino=17142697
scontext=unconfined_u:unconfined_r:passwd_t:s0-s0:c0.c1023
tcontext=unconfined_u:object_r:admin_home_t:s0 tclass=file permissive=0

...

Hash: passwd,passwd_t,admin_home_t,file,write
----

. If the output obtained in the previous step does not contain clear suggestions:

** Enable full-path auditing to see full paths to accessed objects and to make additional Linux Audit event fields visible:
+
[subs="quotes,attributes"]
----
# *auditctl -w /etc/shadow -p w -k shadow-write*
----
** Clear the `setroubleshoot` cache:
+
[subs="quotes,attributes"]
----
# *rm -f /var/lib/setroubleshoot/setroubleshoot.xml*
----
** Reproduce the problem.
** Repeat step 1.
+
After you finish the process, disable full-path auditing:
+
[subs="quotes,attributes"]
----
# *auditctl -W /etc/shadow -p w -k shadow-write*
----
+
. If `sealert` returns only `catchall` suggestions or suggests adding a new rule using the `audit2allow` tool, match your problem with examples listed and explained in xref:selinux-denials-in-the-audit-log_troubleshooting-problems-related-to-selinux[SELinux denials in the Audit log].

[role="_additional-resources"]
.Additional resources
* `sealert(8)` man page on your system
