:_mod-docs-content-type: PROCEDURE

[id="using-a-private-ca-to-issue-certificates-for-csrs-with-gnutls_{context}"]
= Using a private CA to issue certificates for CSRs with GnuTLS

To enable systems to establish a TLS-encrypted communication channel, a certificate authority (CA) must provide valid certificates to them. If you have a private CA, you can create the requested certificates by signing certificate signing requests (CSRs) from the systems.

.Prerequisites

* You have already configured a private CA. See
ifdef::securing-networks[]
xref:creating-a-private-ca-using-gnutls_creating-and-managing-tls-keys-and-certificates[]
endif::[]
ifndef::securing-networks[]
the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/creating-a-private-ca-using-gnutls_creating-and-managing-tls-keys-and-certificates[Creating a private CA using GnuTLS] section
endif::[]
for more information.
* You have a file containing a CSR. You can find an example of creating the CSR in
ifdef::securing-networks[]
xref:creating-a-private-key-and-a-csr-for-a-tls-server-certificate-using-gnutls_creating-and-managing-tls-keys-and-certificates[]
endif::[]
ifndef::securing-networks[]
the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/creating-a-private-key-and-a-csr-for-a-tls-server-certificate-using-gnutls_creating-and-managing-tls-keys-and-certificates[Creating a private key and a CSR for a TLS server certificate using GnuTLS] section
endif::[]
.

.Procedure

. Optional: Use a text editor of your choice to prepare an GnuTLS configuration file for adding extensions to certificates, for example:
+
[subs="quotes"]
----
$ *vi _&lt;server-extensions.cfg&gt;_*
honor_crq_extensions
ocsp_uri = "http://ocsp.example.com"

----

. Use the `certtool` utility to create a certificate based on a CSR, for example:
+
[subs="quotes"]
----
$ *certtool --generate-certificate --load-request _&lt;example-server.crq&gt;_ --load-ca-privkey _&lt;ca.key&gt;_ --load-ca-certificate _&lt;ca.crt&gt;_ --template _&lt;server-extensions.cfg&gt;_ --outfile _&lt;example-server.crt&gt;_*
----



[role="_additional-resources"]
.Additional resources
* `certtool(1)` man page on your system

