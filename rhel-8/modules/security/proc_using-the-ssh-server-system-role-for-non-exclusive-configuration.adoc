:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE

[id="proc_using-the-ssh-server-system-role-for-non-exclusive-configuration_{context}"]
= Using the `sshd` RHEL system role for non-exclusive configuration

By default, applying the `sshd` RHEL system role overwrites the entire configuration. This may be problematic if you have previously adjusted the configuration, for example, with a different RHEL system role or a playbook. To apply the `sshd` RHEL system role for only selected configuration options while keeping other options in place, you can use the non-exclusive configuration.

You can apply a non-exclusive configuration:

* In {ProductShortName} 8 and earlier by using a configuration snippet.
* In {ProductShortName} 9 and later by using files in a drop-in directory. The default configuration file is already placed in the drop-in directory as `/etc/ssh/sshd_config.d/00-ansible_system_role.conf`.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:

** For managed nodes that run RHEL 8 or earlier:
+
[source,yaml,subs="+quotes"]
....
---
- name: Non-exclusive sshd configuration
  hosts: managed-node-01.example.com
  tasks:
    - name: Configure SSHD to accept environment variables
      ansible.builtin.include_role:
        name: rhel-system-roles.sshd
      vars:
        sshd_config_namespace: _<my-application>_
        sshd:
          # Environment variables to accept
          AcceptEnv:
            LANG
            LS_COLORS
            EDITOR
....

** For managed nodes that run RHEL 9 or later:
+
[source,yaml,subs="+quotes"]
....
- name: Non-exclusive sshd configuration
  hosts: managed-node-01.example.com
  tasks:
    - name: Configure sshd to accept environment variables
      ansible.builtin.include_role:
        name: rhel-system-roles.sshd
      vars:
        sshd_config_file: /etc/ssh/sshd_config.d/_<42-my-application>_.conf
        sshd:
          # Environment variables to accept
          AcceptEnv:
            LANG
            LS_COLORS
            EDITOR
....
+
The settings specified in the example playbooks include the following:
+
--
`sshd_config_namespace: _<my-application>_`:: The role places the configuration that you specify in the playbook to configuration snippets in the existing configuration file under the given namespace. You need to select a different namespace when running the role from different context.

`sshd_config_file: /etc/ssh/sshd_config.d/_<42-my-application>_.conf`:: In the `sshd_config_file` variable, define the `.conf` file into which the `sshd` system role writes the configuration options. Use a two-digit prefix, for example `_42-_` to specify the order in which the configuration files will be applied.

`AcceptEnv:`:: Controls which environment variables the OpenSSH server (`sshd`) will accept from a client:
+
* `LANG`: defines the language and locale settings.
* `LS_COLORS`: defines the displaying color scheme for the `ls` command in the terminal.
* `EDITOR`: specifies the default text editor for the command-line programs that need to open an editor.
--
+
For details about the role variables and the OpenSSH configuration options used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.sshd/README.md` file and the `sshd_config(5)` manual page on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification
* Verify the configuration on the SSH server:

** For managed nodes that run RHEL 8 or earlier:
+
[subs="+quotes"]
....
# *cat /etc/ssh/sshd_config*
...
# BEGIN sshd system role managed block: namespace _<my-application>_
Match all
  AcceptEnv LANG LS_COLORS EDITOR
# END sshd system role managed block: namespace _<my-application>_
....

** For managed nodes that run RHEL 9 or later:
+
[subs="+quotes"]
....
# *cat /etc/ssh/sshd_config.d/42-my-application.conf*
# Ansible managed
#
AcceptEnv LANG LS_COLORS EDITOR
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.sshd/README.md` file
* `/usr/share/doc/rhel-system-roles/sshd/` directory
* `sshd_config(5)` manual page