:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_protecting-systems-against-intrusive-usb-devices.adoc

[id="installing-usbguard_{context}"]
= Installing USBGuard

[role="_abstract"]
Use this procedure to install and initiate the USBGuard framework.

.Procedure

. Install the `usbguard` package:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install usbguard*
----
. Create an initial rule set:
+
[subs="quotes,attributes"]
----
# *usbguard generate-policy > /etc/usbguard/rules.conf*
----
. Start the `usbguard` daemon and ensure that it starts automatically on boot:
+
[subs="quotes,attributes"]
----
# *systemctl enable --now usbguard*
----

.Verification

. Verify that the `usbguard` service is running:
+
[subs="quotes,attributes"]
----
# *systemctl status usbguard*
● usbguard.service - USBGuard daemon
   Loaded: loaded (/usr/lib/systemd/system/usbguard.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2019-11-07 09:44:07 CET; 3min 16s ago
     Docs: man:usbguard-daemon(8)
 Main PID: 6122 (usbguard-daemon)
    Tasks: 3 (limit: 11493)
   Memory: 1.2M
   CGroup: /system.slice/usbguard.service
           └─6122 /usr/sbin/usbguard-daemon -f -s -c /etc/usbguard/usbguard-daemon.conf

Nov 07 09:44:06 localhost.localdomain systemd[1]: Starting USBGuard daemon...
Nov 07 09:44:07 localhost.localdomain systemd[1]: Started USBGuard daemon.
----

. List USB devices recognized by USBGuard:
+
[subs="quotes,attributes"]
----
# *usbguard list-devices*
4: allow id 1d6b:0002 serial "0000:02:00.0" name "xHCI Host Controller" hash...
----

[role="_additional-resources"]
.Additional resources
* `usbguard(1)` and `usbguard-daemon.conf(5)` man pages.
