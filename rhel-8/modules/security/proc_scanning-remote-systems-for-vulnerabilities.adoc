:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_scanning-the-system-for-configuration-compliance-and-vulnerabilities.adoc

[id="scanning-remote-systems-for-vulnerabilities_{context}"]
= Scanning remote systems for vulnerabilities

You can check remote systems for vulnerabilities with the OpenSCAP scanner by using the `oscap-ssh` tool over the SSH protocol.

.Prerequisites

* The `openscap-utils` and `bzip2` packages are installed on the system you use for scanning.
* The `openscap-scanner` package is installed on the remote systems.
* The SSH server is running on the remote systems.

.Procedure

. Download the latest RHSA OVAL definitions for your system:
+
[subs="+quotes,attributes"]
----
# *wget -O - https://www.redhat.com/security/data/oval/v2/RHEL{ProductNumber}/rhel-{ProductNumber}.oval.xml.bz2 | bzip2 --decompress > rhel-{ProductNumber}.oval.xml*
----

. Scan a remote system for vulnerabilities and save the results to a file:
+
[subs="+quotes,attributes"]
----
# *oscap-ssh _<username>_@_<hostname>_ _<port>_ oval eval --report _<scan-report.html>_ rhel-{ProductNumber}.oval.xml*
----
+
Replace:

* `_<username>_@_<hostname>_` with the user name and host name of the remote system.
* `_<port>_` with the port number through which you can access the remote system, for example, `22`.
* `_<scan-report.html>_` with the file name where `oscap` saves the scan results.

[role="_additional-resources"]
.Additional resources
* `oscap-ssh(8)`
* link:https://www.redhat.com/security/data/oval/v2/RHEL{ProductNumber}/[Red Hat OVAL definitions]
* link:https://access.redhat.com/articles/6999111[OpenSCAP memory consumption problems] 
