:_mod-docs-content-type: REFERENCE
:experimental:

[id="ref_ssh-system-role-variables_{context}"]
= How the `ssh` RHEL system role maps settings from a playbook to the configuration file

[role="_abstract"]
In the `ssh` RHEL system role playbook, you can define the parameters for the client SSH configuration file.

If you do not specify these settings, the role produces a global `ssh_config` file that matches the {ProductShortName} defaults.

In all the cases, booleans correctly render as `yes` or `no` in the final configuration on your managed nodes. You can use lists to define multi-line configuration items. For example:

----
LocalForward:
  - 22 localhost:2222
  - 403 localhost:4003
----

renders as:

----
LocalForward 22 localhost:2222
LocalForward 403 localhost:4003
----

NOTE: The configuration options are case sensitive.


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.ssh/README.md` file
* `/usr/share/doc/rhel-system-roles/ssh/` directory

