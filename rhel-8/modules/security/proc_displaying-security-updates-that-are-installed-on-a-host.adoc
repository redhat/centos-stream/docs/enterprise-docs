:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_identifying-security-updates.adoc


[id="displaying-security-updates-that-are-installed-on-a-host_{context}"]
= Displaying security updates that are installed on a host

You can list installed security updates for your system by using the `{PackageManagerCommand}` utility.

.Procedure

* List all security updates which are installed on the host:
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} updateinfo list security --installed*
…
RHSA-2019:1234 Important/Sec. libssh2-1.8.0-7.module+el8+2833+c7d6d092
RHSA-2019:4567 Important/Sec. python3-libs-3.6.7.1.el8.x86_64
RHSA-2019:8901 Important/Sec. python3-libs-3.6.8-1.el8.x86_64
…
----
+
If multiple updates of a single package are installed, `{PackageManagerCommand}` lists all advisories for the package. In the previous example, two security updates for the `python3-libs` package have been installed since the system installation.
