:_mod-docs-content-type: PROCEDURE
:experimental:
// included in switching-the-system-to-fips-mode

[id="proc_installing-the-system-with-fips-mode-enabled_{context}"]
= Installing the system with FIPS mode enabled
[[proc_installing-the-system-with-fips-mode-enabled_assembly_installing-the-system-in-fips-mode]]

To enable the cryptographic module self-checks mandated by the Federal Information Processing Standard (FIPS) 140, enable FIPS mode during the system installation.

[IMPORTANT]
====
Only enabling FIPS mode during the {ProductShortName} installation ensures that the system generates all keys with FIPS-approved algorithms and continuous monitoring tests in place.
====

[WARNING]
====
After you complete the setup of FIPS mode, you cannot switch off FIPS mode without putting the system into an inconsistent state. If your scenario requires this change, the only correct way is a complete re-installation of the system.
====

.Procedure

. Add the `fips=1` option to the kernel command line during the system installation.

. During the software selection stage, do not install any third-party software.

. After the installation, the system starts in FIPS mode automatically.


.Verification

* After the system starts, check that FIPS mode is enabled:
+
[subs="quotes"]
----
$ *fips-mode-setup --check*
FIPS mode is enabled.
----


[role="_additional-resources"]
.Additional resources

* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/interactively_installing_rhel_from_installation_media/optional-customizing-boot-options_rhel-installer[Editing boot options]
