:_module-type: REFERENCE

// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_deploying-systems-that-are-compliant-with-a-security-profile-immediately-after-an-installation.adoc

//:ProductNumberLink: 9

[id="ref_profiles-not-compatible-with-server-with-gui_{context}"]
= Profiles not compatible with Server with GUI

[role="_abstract"]
Certain security profiles provided as part of the *SCAP Security Guide* are not compatible with the extended package set included in the *Server with GUI* base environment. Therefore, do not select *Server with GUI* when installing systems compliant with one of the following profiles:

.Profiles not compatible with Server with GUI
ifeval::[{ProductNumberLink} == 9]
[cols=4*,options=header]
|====
|Profile name|Profile ID|Justification|Notes
|[DRAFT] CIS Red Hat Enterprise Linux 9 Benchmark for Level 2 - Server|`xccdf_org.ssgproject.content_profile_**cis**`|Packages `xorg-x11-server-Xorg`, `xorg-x11-server-common`, `xorg-x11-server-utils`, and `xorg-x11-server-Xwayland` are part of the *Server with GUI* package set, but the policy requires their removal.|
|[DRAFT] CIS Red Hat Enterprise Linux 9 Benchmark for Level 1 - Server|	`xccdf_org.ssgproject.content_profile_**cis_server_l1**`|Packages `xorg-x11-server-Xorg`, `xorg-x11-server-common`, `xorg-x11-server-utils`, and `xorg-x11-server-Xwayland` are part of the *Server with GUI* package set, but the policy requires their removal.|
|DISA STIG for Red Hat Enterprise Linux 9	|`xccdf_org.ssgproject.content_profile_**stig**`|Packages `xorg-x11-server-Xorg`, `xorg-x11-server-common`, `xorg-x11-server-utils`, and `xorg-x11-server-Xwayland` are part of the *Server with GUI* package set, but the policy requires their removal.|To install a {ProductShortName} system as a *Server with GUI* aligned with DISA STIG, you can use the *DISA STIG with GUI* profile link:https://bugzilla.redhat.com/show_bug.cgi?id=1648162[BZ#1648162]
|====

endif::[]

ifeval::[{ProductNumberLink} == 8]
[cols=4*,options=header]
|====
|Profile name|Profile ID|Justification|Notes
|CIS Red Hat Enterprise Linux 8 Benchmark for Level 2 - Server|`xccdf_org.ssgproject.content_profile_**cis**`|Packages `xorg-x11-server-Xorg`, `xorg-x11-server-common`, `xorg-x11-server-utils`, and `xorg-x11-server-Xwayland` are part of the *Server with GUI* package set, but the policy requires their removal.|
|CIS Red Hat Enterprise Linux 8 Benchmark for Level 1 - Server|	`xccdf_org.ssgproject.content_profile_**cis_server_l1**`|Packages `xorg-x11-server-Xorg`, `xorg-x11-server-common`, `xorg-x11-server-utils`, and `xorg-x11-server-Xwayland` are part of the *Server with GUI* package set, but the policy requires their removal.|
|Unclassified Information in Non-federal Information Systems and Organizations (NIST 800-171)|	`xccdf_org.ssgproject.content_profile_**cui**`|The `nfs-utils` package is part of the *Server with GUI* package set, but the policy requires its removal.|
|Protection Profile for General Purpose Operating Systems	|`xccdf_org.ssgproject.content_profile_**ospp**`	|The `nfs-utils` package is part of the *Server with GUI* package set, but the policy requires its removal.|
|DISA STIG for Red Hat Enterprise Linux 8	|`xccdf_org.ssgproject.content_profile_**stig**`|Packages `xorg-x11-server-Xorg`, `xorg-x11-server-common`, `xorg-x11-server-utils`, and `xorg-x11-server-Xwayland` are part of the *Server with GUI* package set, but the policy requires their removal.|To install a RHEL system as a *Server with GUI* aligned with DISA STIG in RHEL version 8.4 and later, you can use the *DISA STIG with GUI* profile.
|====
endif::[]
