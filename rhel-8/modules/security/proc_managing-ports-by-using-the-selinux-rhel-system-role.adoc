:_newdoc-version: 2.15.1
:_template-generated: 2024-01-11
:_mod-docs-content-type: PROCEDURE

[id="managing-ports-by-using-the-selinux-rhel-system-role_{context}"]
= Managing ports by using the `selinux` RHEL system role

[role="_abstract"]
You can automate managing port access in SELinux consistently across multiple systems by using the `selinux` {RHELSystemRoles}. This might be useful, for example, when configuring an Apache HTTP server to listen on a different port. You can do this by creating a playbook with the `selinux` {RHELSystemRoles} that assigns the `http_port_t` SELinux type to a specific port number. After you run the playbook on the managed nodes, specific services defined in the SELinux policy can access this port.

You can automate managing port access in SELinux either by using the `seport` module, which is quicker than using the entire role, or by using the `selinux` {RHELSystemRoles}, which is more useful when you also make other changes in SELinux configuration. The methods are equivalent, in fact the `selinux` {RHELSystemRoles} uses the `seport` module when configuring ports. Each of the methods has the same effect as entering the command `semanage port -a -t http_port_t -p tcp _<port_number>_` on the managed node.


.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]
* Optional: To verify port status by using the `semanage` command, the `policycoreutils-python-utils` package must be installed.

.Procedure

* To configure just the port number without making other changes, use the `seport` module:
+
[source,yaml,subs="+quotes"]
....
- name: Allow Apache to listen on tcp port _<port_number>_
  community.general.seport:
    ports: _<port_number>_
    proto: tcp
    setype: http_port_t
    state: present
....
+
Replace `_<port_number>_` with the port number to which you want to assign the `http_port_t` type.

* For more complex configuration of the managed nodes that involves other customizations of SELinux, use the `selinux` {RHELSystemRoles}. Create a playbook file, for example, `~/playbook.yml`, and add the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Modify SELinux port mapping example
  hosts: all
  vars:
    # Map tcp port _<port_number>_ to the 'http_port_t' SELinux port type
    selinux_ports:
      - ports: _<port_number>_
        proto: tcp
        setype: http_port_t
        state: present
 
  tasks:
    - name: Include selinux role
      ansible.builtin.include_role:
        name: rhel-system-roles.selinux
....
+
Replace `_<port_number>_` with the port number to which you want to assign the `http_port_t` type.


.Verification

* Verify that the port is assigned to the `http_port_t` type:
+
[subs="+quotes"]
----
# *semanage port --list | grep http_port_t*
http_port_t                	tcp  	_<port_number>_, 80, 81, 443, 488, 8008, 8009, 8443, 9000
----


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.selinux/README.md` file
* `/usr/share/doc/rhel-system-roles/selinux/` directory