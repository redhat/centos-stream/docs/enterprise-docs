:_newdoc-version: 2.18.2
:_template-generated: 2024-06-28
:_mod-docs-content-type: PROCEDURE

[id="finding-the-correct-selinux-type-for-managing-access-to-non-standard-directories_{context}"]
= Finding the correct SELinux type for managing access to non-standard directories

If you need to set access-control rules that the default SELinux policy does not cover, start by searching for a boolean that matches your use case. If you cannot find a suitable boolean, you can use a matching SELinux type or even create a local policy module.

.Prerequisites

* The `selinux-policy-doc` and `setools-console` packages are installed on your system.

.Procedure

. List all SELinux-related topics and limit the results to a component you want to configure. For example:
+
[subs="+quotes"]
----
# *man -k selinux | grep samba*
samba_net_selinux (8) - Security Enhanced Linux Policy for the samba_net processes
samba_selinux (8)    - Security Enhanced Linux Policy for the smbd processes
…
----
+
In the man page that corresponds to your scenario, find the related SELinux booleans, port types, and file types.
+
Note that the `man -k selinux` or `apropos selinux` commands are available only after you install the `selinux-policy-doc` package.

. Optional: You can display the default mapping of processes on default locations by using the `semanage fcontext -l` command, for example:
+
[subs="+quotes,macros"]
----
# *semanage fcontext -l | grep samba*
…
/var/cache/samba(/.pass:[*])?                             all files          system_u:object_r:samba_var_t:s0
…
/var/spool/samba(/.pass:[*])?                             all files          system_u:object_r:samba_spool_t:s0
…
----

. Use the `sesearch` command to display rules in the default SELinux policy. You can find the type and boolean to use by listing the corresponding rule, for example:
+
[subs="+quotes"]
----
$ *sesearch -A | grep samba | grep httpd*
…
allow httpd_t cifs_t:dir { getattr open search }; [ use_samba_home_dirs && httpd_enable_homedirs ]:True
…
----

. An SELinux boolean might be the most straightforward solution for your configuration problem. You can display all available booleans and their values by using the `getsebool -a` command, for example:
+
[subs="+quotes"]
----
$ *getsebool -a | grep homedirs*
git_cgi_enable_homedirs --> off
git_system_enable_homedirs --> off
httpd_enable_homedirs --> off
mock_enable_homedirs --> off
mpd_enable_homedirs --> off
openvpn_enable_homedirs --> on
ssh_chroot_rw_homedirs --> off
----

. You can verify that the selected boolean does exactly what you want by using the `sesearch` command, for example:
+
[subs="+quotes"]
----
$ *sesearch -A | grep httpd_enable_homedirs*
…
allow httpd_suexec_t autofs_t:dir { getattr open search }; [ use_nfs_home_dirs && httpd_enable_homedirs ]:True
allow httpd_suexec_t autofs_t:dir { getattr open search }; [ use_samba_home_dirs && httpd_enable_homedirs ]:True
…
----

. If no boolean matches your scenario, find an SELinux type that suits your case. You can find a type for your files by querying a corresponding rule from the default policy by using `sesearch`, for example:
+
[subs="+quotes"]
----
$ *sesearch -A -s httpd_t -c file -p read*
…
allow httpd_t httpd_t:file { append getattr ioctl lock open read write };
allow httpd_t httpd_tmp_t:file { append create getattr ioctl link lock map open read rename setattr unlink write };
…
----

. If none of the previous solutions cover your scenario, you can add a custom rule to the SELinux policy. See the
ifdef::using-selinux[]
xref:proc_creating-a-local-selinux-policy-module_troubleshooting-problems-related-to-selinux[Creating a local SELinux policy module]
endif::[]
ifndef::using-selinux[]
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/troubleshooting-problems-related-to-selinux_using-selinux#proc_creating-a-local-selinux-policy-module_troubleshooting-problems-related-to-selinux[Creating a local SELinux policy module]
endif::[]
section for more information.


[role="_additional-resources"]
.Additional resources

* SELinux-related man pages provided by the `man -k selinux` command
* `sesearch(1)`, `semanage-fcontext(8)`, `semanage-boolean(8)`, and `getsebool(8)` man pages on your system
