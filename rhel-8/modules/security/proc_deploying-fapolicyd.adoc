:_mod-docs-content-type: PROCEDURE

// included in assembly_blocking-and-allowing-applications-using-fapolicyd.adoc

[id="proc_deploying-fapolicyd_{context}"]
= Deploying fapolicyd

When deploying the `fapolicyd` application allowlisting framework, you can either try your configuration in permissive mode first or directly enable the service in the default configuration.

.Procedure

. Install the `fapolicyd` package:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install fapolicyd*
----

. Optional: To try your configuration first, change mode to permissive.
.. Open the `/etc/fapolicyd/fapolicyd.conf` file in a text editor of your choice, for example:
+
[subs="quotes"]
----
# *vi /etc/fapolicyd/fapolicyd.conf*
----
.. Change the value of the `permissive` option from `0` to `1`, save the file, and exit the editor:
+
----
permissive = 1
----
+
Alternatively, you can debug your configuration by using the `fapolicyd --debug-deny --permissive` command before you start the service. See the xref:ref_troubleshooting-problems-related-to-fapolicyd_{context}[Troubleshooting problems related to fapolicyd] section for more information.

. Enable and start the `fapolicyd` service:
+
[subs="quotes"]
----
# *systemctl enable --now fapolicyd*
----

. If you enabled permissive mode through `/etc/fapolicyd/fapolicyd.conf`:
.. Set the Audit service for recording `fapolicyd` events:
+
[subs="quotes"]
----
# *auditctl -w /etc/fapolicyd/ -p wa -k fapolicyd_changes*
# *service try-restart auditd*
----
.. Use your applications.
.. Check Audit logs for `fanotify` denials, for example:
+
[subs="quotes"]
----
# *ausearch -ts recent -m fanotify*
----
.. When debugged, disable permissive mode by changing the corresponding value back to `permissive = 0`, and restart the service:
+
[subs="quotes"]
----
# *systemctl restart fapolicyd*
----


.Verification

. Verify that the `fapolicyd` service is running correctly:
+
[subs="+quotes"]
----
# *systemctl status fapolicyd*
● fapolicyd.service - File Access Policy Daemon
     Loaded: loaded (/usr/lib/systemd/system/fapolicyd.service; enabled; preset: disabled)
     Active: active (running) since Tue 2024-10-08 05:53:50 EDT; 11s ago
…
Oct 08 05:53:51 machine1.example.com fapolicyd[4974]: Loading trust data from rpmdb backend
Oct 08 05:53:51 machine1.example.com fapolicyd[4974]: Loading trust data from file backend
Oct 08 05:53:51 machine1.example.com fapolicyd[4974]: Starting to listen for events
----
. Log in as a user without root privileges, and check that `fapolicyd` is working, for example:
+
[subs="quotes,attributes"]
----
$ *cp /bin/ls /tmp*
$ */tmp/ls*
bash: /tmp/ls: Operation not permitted
----
