:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_identifying-security-updates.adoc


[id="displaying-available-security-updates_{context}"]
= Displaying security updates that are not installed on a host

You can list all available security updates for your system by using the `{PackageManagerCommand}` utility.

.Prerequisite

* A Red Hat subscription is attached to the host.

.Procedure

* List all available security updates which have not been installed on the host:
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} updateinfo list updates security*
…
RHSA-2019:0997 Important/Sec. platform-python-3.6.8-2.el8_0.x86_64
RHSA-2019:0997 Important/Sec. python3-libs-3.6.8-2.el8_0.x86_64
RHSA-2019:0990 Moderate/Sec.  systemd-239-13.el8_0.3.x86_64
…
----
