:_mod-docs-content-type: PROCEDURE
// included in assembly_confining-administrator-users.adoc

[id="confining-an-administrator-using-sudo-and-the-sysadm_r-role_{context}"]
= Confining an administrator by using sudo and the sysadm_r role

[role="_abstract"]
You can map a specific user with administrative privileges to the `staff_u` SELinux user, and configure `sudo` so that the user can gain the `sysadm_r` SELinux administrator role. This role allows the user to perform administrative tasks without SELinux denials. When the user logs in, the session runs in the `staff_u:staff_r:staff_t` SELinux context, but when the user enters a command by using `sudo`, the session changes to the `staff_u:sysadm_r:sysadm_t` context.

By default, all Linux users in {RHEL}, including users with administrative privileges, are mapped to the unconfined SELinux user `unconfined_u`. You can improve the security of the system by assigning users to SELinux confined users. This is useful to conform with the link:https://rhel7stig.readthedocs.io/en/latest/medium.html#v-71971-the-operating-system-must-prevent-non-privileged-users-from-executing-privileged-functions-to-include-disabling-circumventing-or-altering-implemented-security-safeguards-countermeasures-rhel-07-020020[V-71971 Security Technical Implementation Guide].

.Prerequisites

* The `root` user runs unconfined. This is the {RHEL} default.

.Procedure

. Map a new or existing user to the `staff_u` SELinux user:

.. To map a new user, add a new user to the `wheel` user group and map the user to the `staff_u` SELinux user:
+
[subs="quotes,attributes"]
----
# *adduser -G wheel -Z staff_u _&lt;example_user&gt;_*
----

.. To map an existing user, add the user to the `wheel` user group and map the user to the `staff_u` SELinux user:
+
[subs="quotes,attributes"]
----
# *usermod -G wheel -Z staff_u _&lt;example_user&gt;_*
----

. Restore the context of the user's home directory:
+
[subs="quotes,attributes"]
----
# *restorecon -R -F -v /home/_&lt;example_user&gt;_*
----

. To allow `_&lt;example_user&gt;_` to gain the SELinux administrator role, create a new file in the `/etc/sudoers.d/` directory, for example:
+
[subs="quotes,attributes"]
----
# *visudo -f /etc/sudoers.d/_&lt;example_user&gt;_*
----

. Add the following line to the new file:
+
[subs="quotes"]
----
*_&lt;example_user&gt;_  ALL=(ALL)  TYPE=sysadm_t ROLE=sysadm_r    ALL*
----

.Verification

. Check that `_&lt;example_user&gt;_` is mapped to the `staff_u` SELinux user:
+
[subs="quotes,attributes"]
----
# *semanage login -l | grep _&lt;example_user&gt;_*
_&lt;example_user&gt;_     staff_u    s0-s0:c0.c1023   *
----

. Log in as `_&lt;example_user&gt;_`, for example, using SSH, and switch to the `root` user:
+
[subs="quotes,attributes"]
----
[_&lt;example_user&gt;_@localhost ~]$ *sudo -i*
[sudo] password for _&lt;example_user&gt;_:
----

. Show the `root` security context:
+
[subs="quotes,attributes"]
----
# *id -Z*
staff_u:sysadm_r:sysadm_t:s0-s0:c0.c1023
----

. Try an administrative task, for example, restarting the `sshd` service:
+
[subs="quotes,attributes"]
----
# *systemctl restart sshd*
----
+
If there is no output, the command finished successfully.
+
If the command does not finish successfully, it prints the following message:
+
[subs="quotes,attributes"]
----
Failed to restart sshd.service: Access denied
See system logs and 'systemctl status sshd.service' for details.
----
