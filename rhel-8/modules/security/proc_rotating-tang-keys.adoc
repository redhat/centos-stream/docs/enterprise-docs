:_mod-docs-content-type: PROCEDURE
:experimental:
// included in configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption

[id="rotating-tang-keys_{context}"]
= Rotating Tang server keys and updating bindings on clients

For security reasons, rotate your Tang server keys and update existing bindings on clients periodically. The precise interval at which you should rotate them depends on your application, key sizes, and institutional policy.

Alternatively, you can rotate Tang keys by using the `nbde_server` {ProductShortName} system role. See
ifdef::security-hardening[]
xref:using-the-nbde_server-system-role-for-setting-up-multiple-tang-servers_configuring-nbde-by-using-rhel-system-roles[Using the nbde_server system role for setting up multiple Tang servers] for more information.
endif::[]
ifndef::security-hardening[]
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/configuring-nbde-by-using-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles#using-the-nbde_server-system-role-for-setting-up-multiple-tang-servers_configuring-nbde-by-using-rhel-system-roles[Using the nbde_server system role for setting up multiple Tang servers] for more information.
endif::[]

.Prerequisites

* A Tang server is running.
* The `clevis` and `clevis-luks` packages are installed on your clients.
ifeval::[{ProductNumber} == 8]
* Note that `clevis luks list`, `clevis luks report`, and `clevis luks regen` have been introduced in {ProductShortName} 8.2.
endif::[]

ifeval::[{ProductNumber} == 9]
endif::[]

.Procedure

. Rename all keys in the `/var/db/tang` key database directory to have a leading `.` to hide them from advertisement. Note that the file names in the following example differs from unique file names in the key database directory of your Tang server:
+
[subs="quotes,attributes"]
----
# *cd _/var/db/tang_*
# *ls -l*
-rw-r--r--. 1 root root 349 Feb  7 14:55 UV6dqXSwe1bRKG3KbJmdiR020hY.jwk
-rw-r--r--. 1 root root 354 Feb  7 14:55 y9hxLTQSiSB5jSEGWnjhY8fDTJU.jwk
# *mv UV6dqXSwe1bRKG3KbJmdiR020hY.jwk .UV6dqXSwe1bRKG3KbJmdiR020hY.jwk*
# *mv y9hxLTQSiSB5jSEGWnjhY8fDTJU.jwk .y9hxLTQSiSB5jSEGWnjhY8fDTJU.jwk*
----

. Check that you renamed and therefore hid all keys from the Tang server advertisement:
+
[subs="quotes,attributes"]
----
# *ls -l*
total 0
----

. Generate new keys using the `/usr/libexec/tangd-keygen` command in `/var/db/tang` on the Tang server:
+
[subs="quotes,attributes"]
----
# */usr/libexec/tangd-keygen _/var/db/tang_*
# *ls _/var/db/tang_*
3ZWS6-cDrCG61UPJS2BMmPU4I54.jwk zyLuX6hijUy_PSeUEFDi7hi38.jwk
----

. Check that your Tang server advertises the signing key from the new key pair, for example:
+
[subs="quotes,attributes"]
----
# *tang-show-keys _7500_*
3ZWS6-cDrCG61UPJS2BMmPU4I54
----
+
////
Tang immediately picks up all changes. No restart is required. At this point, new client bindings pick up the new keys and old clients can continue to use the old keys.
////

. On your NBDE clients, use the `clevis luks report` command to check if the keys advertised by the Tang server remains the same. You can identify slots with the relevant binding using the `clevis luks list` command, for example:
+
[subs="quotes,attributes"]
----
# *clevis luks list -d _/dev/sda2_*
1: tang '{"url":"_http://tang.srv_"}'
# *clevis luks report -d _/dev/sda2_ -s _1_*
...
Report detected that some keys were rotated.
Do you want to regenerate luks metadata with "clevis luks regen -d /dev/sda2 -s 1"? [ynYN]
----

. To regenerate LUKS metadata for the new keys either press `y` to the prompt of the previous command, or use the `clevis luks regen` command:
+
[subs="quotes,attributes"]
----
# *clevis luks regen -d _/dev/sda2_ -s _1_*
----

.  When you are sure that all old clients use the new keys, you can remove the old keys from the Tang server, for example:
+
[subs="macros,quotes"]
----
# *cd _/var/db/tang_*
# *rm .pass:[*].jwk*
----

[WARNING]
====
Removing the old keys while clients are still using them can result in data loss. If you accidentally remove such keys, use the `clevis luks regen` command on the clients, and provide your LUKS password manually.
====

[role="_additional-resources"]
.Additional resources
* `tang-show-keys(1)`, `clevis-luks-list(1)`, `clevis-luks-report(1)`, and `clevis-luks-regen(1)` man pages on your system
