:_mod-docs-content-type: PROCEDURE

[id="securing-postgresql-by-limiting-access-to-authenticated-local-users_{context}"]
= Securing PostgreSQL by limiting access to authenticated local users


PostgreSQL is an object-relational database management system (DBMS). In {ProductName}, PostgreSQL is provided by the `postgresql-server` package.

You can reduce the risks of attacks by configuring client authentication. The `pg_hba.conf` configuration file stored in the database cluster's data directory controls the client authentication. Follow the procedure to configure PostgreSQL for host-based authentication.

.Procedure

. Install PostgreSQL:
+
[subs="quotes"]
----
# *yum install postgresql-server*
----

. Initialize a database storage area using one of the following options:
.. Using the `initdb` utility:
+
[subs = "quotes"]
----
$ *initdb -D _/home/postgresql/db1/_*
----
+
The `initdb` command with the `-D` option creates the directory you specify if it does not already exist, for example `_/home/postgresql/db1/_`. This directory then contains all the data stored in the database and also the client authentication configuration file.

.. Using the `postgresql-setup` script:
+
[subs = "quotes"]
----
$ *postgresql-setup --initdb*
----
By default, the script uses the `/var/lib/pgsql/data/` directory. This script helps system administrators with basic database cluster administration.

. To allow any authenticated local users to access any database with their usernames, modify the following line in the `pg_hba.conf` file:
+
----
local   all             all                                     trust
----
+
This can be problematic when you use layered applications that create database users and no local users. If you do not want to explicitly control all user names on the system, remove the `local`  line entry from the `pg_hba.conf` file.

. Restart the database to apply the changes:
+
[subs = "quotes"]
----
# *systemctl restart postgresql*
----
+
The previous command updates the database and also verifies the syntax of the configuration file.
