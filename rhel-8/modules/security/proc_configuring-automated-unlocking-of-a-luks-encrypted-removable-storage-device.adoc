:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_configuring-automated-unlocking-of-a-luks-encrypted-removable-storage-device.adoc[leveloffset=+1]

[id="configuring-automated-unlocking-of-a-luks-encrypted-removable-storage-device_{context}"]
= Configuring automated unlocking of a LUKS-encrypted removable storage device

[role="_abstract"]
You can set up an automated unlocking process of a LUKS-encrypted USB storage device.

.Procedure

. To automatically unlock a LUKS-encrypted removable storage device, such as a USB drive, install the [package]`clevis-udisks2` package:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} install clevis-udisks2*
----

. Reboot the system, and then perform the binding step using the [command]`clevis luks bind` command as described in
ifdef::security-hardening[]
xref:configuring-manual-enrollment-of-volumes-using-clevis_configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption[Configuring manual enrollment of LUKS-encrypted volumes],
endif::[]
ifndef::security-hardening[]
_Configuring manual enrollment of LUKS-encrypted volumes_,
endif::[]
for example:
+
[subs="quotes,attributes"]
----
# *clevis luks bind -d _/dev/sdb1_ tang '{"url":"_http://tang.srv_"}'*
----

. The LUKS-encrypted removable device can be now unlocked automatically in your GNOME desktop session. The device bound to a Clevis policy can be also unlocked by the [command]`clevis luks unlock` command:
+
[subs="quotes,attributes"]
----
# *clevis luks unlock -d _/dev/sdb1_*
----

You can use an analogous procedure when using a TPM 2.0 policy instead of a Tang server.

[role="_additional-resources"]
.Additional resources
* `clevis-luks-unlockers(7)` man page on your system
