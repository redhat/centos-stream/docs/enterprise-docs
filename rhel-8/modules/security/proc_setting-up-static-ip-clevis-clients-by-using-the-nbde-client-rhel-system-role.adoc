:_newdoc-version: 2.18.3
:_template-generated: 2024-09-19
:_mod-docs-content-type: PROCEDURE

[id="setting-up-static-ip-clevis-clients-by-using-the-nbde-client-rhel-system-role_{context}"]
= Setting up static-IP Clevis clients by using the `nbde_client` RHEL system role

The `nbde_client` RHEL system role supports only scenarios with Dynamic Host Configuration Protocol (DHCP). On an NBDE client with static IP configuration, you must pass your network configuration as a kernel boot parameter.

Typically, administrators want to reuse a playbook and not maintain individual playbooks for each host to which Ansible assigns static IP addresses during early boot. In this case, you can use variables in the playbook and provide the settings in an external file. As a result, you need only one playbook and one file with the settings.

.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]

* A volume that is already encrypted by using LUKS.

.Procedure

. Create a file with the network settings of your hosts, for example, `static-ip-settings-clients.yml`, and add the values you want to dynamically assign to the hosts: 
+
[source,yaml,subs="+quotes"]
----
clients:
  managed-node-01.example.com:
    ip_v4: 192.0.2.1
    gateway_v4: 192.0.2.254
    netmask_v4: 255.255.255.0
    interface: enp1s0
  managed-node-02.example.com:
    ip_v4: 192.0.2.2
    gateway_v4: 192.0.2.254
    netmask_v4: 255.255.255.0
    interface: enp1s0
----

. Create a playbook file, for example, `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
----
- name: Configure clients for unlocking of encrypted volumes by Tang servers
  hosts: managed-node-01.example.com,managed-node-02.example.com
  vars_files:
    - ~/static-ip-settings-clients.yml
  tasks:
    - name: Create NBDE client bindings 
      ansible.builtin.include_role:
        name: rhel-system-roles.network
      vars:
        nbde_client_bindings:
          - device: /dev/rhel/root
            encryption_key_src: /etc/luks/keyfile
            servers:
              - http://server1.example.com
              - http://server2.example.com
          - device: /dev/rhel/swap
            encryption_key_src: /etc/luks/keyfile
            servers:
              - http://server1.example.com
              - http://server2.example.com

    - name: Configure a Clevis client with static IP address during early boot
      ansible.builtin.include_role:
        name: rhel-system-roles.bootloader
      vars:
        bootloader_settings: 
          - kernel: ALL
            options:
              - name: ip
                value: "{{ clients[inventory_hostname]['ip_v4'] }}::{{ clients[inventory_hostname]['gateway_v4'] }}:{{ clients[inventory_hostname]['netmask_v4'] }}::{{ clients[inventory_hostname]['interface'] }}:none"
----
+
This playbook reads certain values dynamically for each host listed in the `~/static-ip-settings-clients.yml` file.
+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file on the control node. 


. Validate the playbook syntax:
+
[subs="+quotes"]
----
$ *ansible-playbook --syntax-check ~/playbook.yml*
----
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.

. Run the playbook:
+
[subs="+quotes"]
----
$ *ansible-playbook ~/playbook.yml*
----

[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.nbde_client/README.md` file
* `/usr/share/doc/rhel-system-roles/nbde_client/` directory
* link:https://www.redhat.com/sysadmin/network-confi-initrd[Looking forward to Linux network configuration in the initial ramdisk (initrd)] (Red&nbsp;Hat Enable Sysadmin)

