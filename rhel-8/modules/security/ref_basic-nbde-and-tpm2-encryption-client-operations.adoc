:_mod-docs-content-type: REFERENCE
:experimental:
// Module included in the following assemblies:
//
// configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption

[id="basic-nbde-and-tpm2-encryption-client-operations_{context}"]
= Basic NBDE and TPM2 encryption-client operations

[role="_abstract"]
The Clevis framework can encrypt plain-text files and decrypt both ciphertexts in the JSON Web Encryption (JWE) format and LUKS-encrypted block devices. Clevis clients can use either Tang network servers or Trusted Platform Module 2.0 (TPM 2.0) chips for cryptographic operations.

The following commands demonstrate the basic functionality provided by Clevis on examples containing plain-text files. You can also use them for troubleshooting your NBDE or Clevis+TPM deployments.

.Encryption client bound to a Tang server

* To check that a Clevis encryption client binds to a Tang server, use the [command]`clevis encrypt tang` sub-command:
+
[subs="quotes, macros, attributes"]
----
$ *clevis encrypt tang '{"url":"_pass:[http://tang.srv]:port_"}' &lt; _input-plain.txt_ &gt; _secret.jwe_*
The advertisement contains the following signing keys:

_OsIk0T-E2l6qjfdDiwVmidoZjA

Do you wish to trust these keys? [ynYN] y
----
+
Change the `_\http://tang.srv:port_` URL in the previous example to match the URL of the server where [package]`tang` is installed. The `_secret.jwe_` output file contains your encrypted cipher text in the JWE format. This cipher text is read from the `_input-plain.txt_` input file.
+
Alternatively, if your configuration requires a non-interactive communication with a Tang server without SSH access, you can download an advertisement and save it to a file:
+
[subs="quotes,attributes"]
----
$ *curl -sfg _http://tang.srv:port_/adv -o _adv.jws_*
----
+
Use the advertisement in the `_adv.jws_` file for any following tasks, such as encryption of files or messages:
+
[subs="quotes,attributes"]
----
$ *echo 'hello' | clevis encrypt tang '{"url":"_http://tang.srv:port_","adv":"_adv.jws_"}'*
----

* To decrypt data, use the [command]`clevis decrypt` command and provide the cipher text (JWE):
+
[subs="quotes, macros, attributes"]
----
$ *clevis decrypt &lt; _secret.jwe_ &gt; _output-plain.txt_*
----

.Encryption client using TPM 2.0

* To encrypt using a TPM 2.0 chip, use the [command]`clevis encrypt tpm2` sub-command with the only argument in form of the JSON configuration object:
+
[subs="quotes, macros, attributes"]
----
$ *clevis encrypt tpm2 '{}' &lt; _input-plain.txt_ &gt; _secret.jwe_*
----
+
To choose a different hierarchy, hash, and key algorithms, specify configuration properties, for example:
+
[subs="quotes, macros, attributes"]
----
$ *clevis encrypt tpm2 '{"hash":"sha256","key":"rsa"}' &lt; _input-plain.txt_ &gt; _secret.jwe_*
----

* To decrypt the data, provide the ciphertext in the JSON Web Encryption (JWE) format:
+
[subs="quotes, macros, attributes"]
----
$ *clevis decrypt &lt; _secret.jwe_ &gt; _output-plain.txt_*
----

The pin also supports sealing data to a Platform Configuration Registers (PCR) state. That way, the data can only be unsealed if the PCR hashes values match the policy used when sealing.

For example, to seal the data to the PCR with index 0 and 7 for the SHA-256 bank:

[subs="quotes, macros, attributes"]
----
$ *clevis encrypt tpm2 '{"pcr_bank":"sha256","pcr_ids":"0,7"}' &lt; _input-plain.txt_ &gt; _secret.jwe_*
----

[WARNING]
====
Hashes in PCRs can be rewritten, and you no longer can unlock your encrypted volume. For this reason, add a strong passphrase that enable you to unlock the encrypted volume manually even when a value in a PCR changes.

If the system cannot automatically unlock your encrypted volume after an upgrade of the [package]`shim-x64` package, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/6175492[Clevis TPM2 no longer decrypts LUKS devices after a restart].
====

[role="_additional-resources"]
.Additional resources
* `clevis-encrypt-tang(1)`, `clevis-luks-unlockers(7)`, `clevis(1)`, and `clevis-encrypt-tpm2(1)` man pages on your system
* `clevis`, `clevis decrypt`, and `clevis encrypt tang` commands without any arguments show the built-in CLI help, for example:
+
[subs="quotes,macros,attributes"]
----
$ *clevis encrypt tang*
Usage: clevis encrypt tang CONFIG &lt; PLAINTEXT &gt; JWE
...
----
