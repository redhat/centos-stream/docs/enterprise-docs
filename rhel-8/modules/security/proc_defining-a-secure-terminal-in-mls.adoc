:_mod-docs-content-type: PROCEDURE

[id="proc_defining-a-secure-terminal-in-mls_{context}"]
= Defining a secure terminal in MLS

The SELinux policy checks the type of the terminal from which a user is connected, and allows running of certain SELinux applications, for example `newrole`, only from secure terminals. Attempting this from a non-secure terminal produces an error: `Error: you are not allowed to change levels on a non secure terminal;`.

The `/etc/selinux/mls/contexts/securetty_types` file defines secure terminals for the Multi-Level Security (MLS) policy.

Default contents of the file:

----
console_device_t
sysadm_tty_device_t
user_tty_device_t
staff_tty_device_t
auditadm_tty_device_t
secureadm_tty_device_t
----

[WARNING]
====
Adding terminal types to the list of secure terminals can expose your system to security risks.
====

.Prerequisites

* SELinux policy is set to `mls`.
* You are connected from an already secure terminal, or SELinux is in permissive mode.
* You have security administration rights, which means that you are assigned to either:
** The `secadm_r` role.
** If the `sysadm_secadm` module is enabled, to the `sysadm_r` role. The `sysadm_secadm` module is enabled by default.
* The `policycoreutils-python-utils` package is installed.

.Procedure

. Determine the current terminal type:
+
[subs="quotes,attributes,macros"]
----
# *ls -Z ++`tty`++*
root:object_r:**user_devpts_t**:s0 /dev/pts/0
----
+
In this example output, `user_devpts_t` is the current terminal type.

. Add the relevant SELinux type on a new line in the `/etc/selinux/mls/contexts/securetty_types` file.

. Optional: Switch SELinux to enforcing mode:
+
[subs="quotes,attributes"]
----
# *setenforce 1*
----

.Verification

* Log in from the previously insecure terminal you have added to the `/etc/selinux/mls/contexts/securetty_types` file.


[role="_additional-resources"]
.Additional resources
* `securetty_types(5)` man page on your system
