:_mod-docs-content-type: REFERENCE
[id="ref_selinux-roles-in-mls_{context}"]
= SELinux roles in MLS

[role="_abstract"]
The SELinux policy maps each Linux user to an SELinux user. This allows Linux users to inherit the restrictions of SELinux users.

IMPORTANT: The MLS policy does not contain the `unconfined` module, including unconfined users, types, and roles. As a result, users that would be unconfined, including `root`, cannot access every object and perform every action they could in the targeted policy.

You can customize the permissions for confined users in your SELinux policy according to specific needs by adjusting the booleans in policy. You can determine the current state of these booleans by using the `semanage boolean -l` command. To list all SELinux users, their SELinux roles, and MLS/MCS levels and ranges, use the `semanage user -l` command as `root`.


.Roles of SELinux users in MLS
[options="header"]
|===
|User|Default role|Additional roles
|`guest_u`|`guest_r`|
|`xguest_u`|`xguest_r`|
|`user_u`|`user_r`|
.4+|`staff_u` .4+|`staff_r`|`auditadm_r`
|`secadm_r`
|`sysadm_r`
|`staff_r`
|`sysadm_u`
|`sysadm_r`|
.4+|`root` .4+|`staff_r`|`auditadm_r`
|`secadm_r`
|`sysadm_r`
|`system_r`
|`system_u`|`system_r`|
|===

Note that `system_u` is a special user identity for system processes and objects, and `system_r` is the associated role. Administrators must never associate this `system_u` user and the `system_r` role to a Linux user. Also, `unconfined_u` and `root` are unconfined users. For these reasons, the roles associated to these SELinux users are not included in the following table Types and access of SELinux roles.

Each SELinux role corresponds to an SELinux type and provides specific access rights.

.Types and access of SELinux roles in MLS

[options="header"]
|======
|Role|Type|Login using X Window System|`su` and `sudo`|Execute in home directory and `/tmp` (default)|Networking
|`guest_r`|`guest_t`|no|no|yes|no
|`xguest_r`|`xguest_t`|yes|no|yes|web browsers only (Firefox, GNOME Web)
|`user_r`|`user_t`|yes|no|yes|yes
|`staff_r`|`staff_t`|yes|only `sudo`|yes|yes
|`auditadm_r`|`auditadm_t`||yes|yes|yes
|`secadm_r`|`secadm_t`||yes|yes|yes
|`sysadm_r`|`sysadm_t`|only when the `xdm_sysadm_login` boolean is `on`|yes|yes|yes
|======

* By default, the `sysadm_r` role has the rights of the `secadm_r` role, which means a user with the `sysadm_r` role can manage the security policy. If this does not correspond to your use case, you can separate the two roles by disabling the `sysadm_secadm` module in the policy. For additional information, see
ifdef::using-selinux[]
xref:proc_separating-system-administration-from-security-administration-in-mls_using-multi-level-security-mls[Separating system administration from security administration in MLS].
endif::[]
ifndef::using-selinux[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/index#proc_separating-system-administration-from-security-administration-in-mls_using-multi-level-security-mls[Separating system administration from security administration in MLS].
endif::[]


* Non-login roles `dbadm_r`, `logadm_r`, and  `webadm_r` can be used for a subset of administrative tasks. By default, these roles are not associated with any SELinux user.
