:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
[id="limiting-su-access_{context}"]

= Limiting su access to a privileged user group

[role="_abstract"]
Because `su` is a powerful tool, you can limit its use to privileged users. Follow these steps to limit `su` access to members of the `wheel` administrative group.

.Prerequisites
* You are logged in as the `root` user.
* `user1` is a member of the `wheel` group.

.Procedure

. Open the Pluggable Authentication Module (PAM) configuration file for `su`, for example, in the `vi` text editor:

+
[subs="quotes,attributes"]
----
# *vi /etc/pam.d/su*
----

. Remove the comment character `#` from the following line:
+
----
#auth           required        pam_wheel.so use_uid
----

. Save changes, and exit the file. Only users in the `wheel` group now have `su` privileges.

.Verification

To verify that users in the `wheel` group have `su` privileges:

. Switch to `user1` who is a member of the `wheel` group:
+
[subs="quotes,attributes"]
----
# *su - user1*
----

. As `user1`, switch to the `root` user, and enter the `root` password:
+
[subs="quotes,attributes"]
----
$ *su -*
----

. If `user1` has `su` privileges, you have become the `root` user.
+
If, instead, you receive the following message, `user1` does not have the necessary authorizations:
+
----
su: Permission denied
----
  