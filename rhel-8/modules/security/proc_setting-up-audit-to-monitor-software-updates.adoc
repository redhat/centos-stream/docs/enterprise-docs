:_module-type: PROCEDURE

[id="proc_setting-up-audit-to-monitor-software-updates_{context}"]
= Setting up Audit to monitor software updates

[role="_abstract"]
ifeval::[{ProductNumber} == 8]
In {ProductShortName} 8.6 and later versions, you can use the pre-configured rule `44-installers.rules` to configure Audit to monitor the following utilities that install software:

* `dnf` footnote:[Because `dnf` is a symlink in {ProductShortName}, the path in the `dnf` Audit rule must include the target of the symlink. To receive correct Audit events, modify the `44-installers.rules` file by changing the `path=/usr/bin/dnf` path to `/usr/bin/dnf-3`.]
* `yum`
* `pip`
* `npm`
* `cpan`
* `gem`
* `luarocks`

By default, `rpm` already provides audit `SOFTWARE_UPDATE` events when it installs or updates a package. You can list them by entering `ausearch -m SOFTWARE_UPDATE` on the command line.

In {ProductShortName} 8.5 and earlier versions, you can manually add rules to monitor utilities that install software into a `.rules` file within the `/etc/audit/rules.d/` directory.

[NOTE]
====
Pre-configured rule files cannot be used on systems with the `ppc64le` and `aarch64` architectures.
====
endif::[]

ifeval::[{ProductNumber} == 9]
You can use the pre-configured rule `44-installers.rules` to configure Audit to monitor the following utilities that install software:

* `dnf` footnote:[Because `dnf` is a symlink in {ProductShortName}, the path in the `dnf` Audit rule must include the target of the symlink. To receive correct Audit events, modify the `44-installers.rules` file by changing the `path=/usr/bin/dnf` path to `/usr/bin/dnf-3`.]
* `yum`
* `pip`
* `npm`
* `cpan`
* `gem`
* `luarocks`

To monitor the `rpm` utility, install the `rpm-plugin-audit` package. Audit will then generate `SOFTWARE_UPDATE` events when it installs or updates a package. You can list these events by entering `ausearch -m SOFTWARE_UPDATE` on the command line.

[NOTE]
====
Pre-configured rule files cannot be used on systems with the `ppc64le` and `aarch64` architectures.
====
endif::[]


.Prerequisites

* `auditd` is configured in accordance with the settings provided in
ifdef::security_hardening[]
xref:configuring-auditd-for-a-secure-environment_auditing-the-system[]
endif::[]
ifndef::security_hardening[]
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/auditing-the-system_security-hardening#configuring-auditd-for-a-secure-environment_auditing-the-system[Configuring auditd for a secure environment]
endif::[]
.

.Procedure
ifeval::[{ProductNumber} == 8]
. On {ProductShortName} 8.6 and later, copy the pre-configured rule file `44-installers.rules` from the `/usr/share/audit/sample-rules/` directory to the `/etc/audit/rules.d/` directory:
endif::[]
ifeval::[{ProductNumber} == 9]
. Copy the pre-configured rule file `44-installers.rules` from the `/usr/share/audit/sample-rules/` directory to the `/etc/audit/rules.d/` directory:
endif::[]
+
[subs="quotes"]
----
# *cp /usr/share/audit/sample-rules/44-installers.rules /etc/audit/rules.d/*
----
ifeval::[{ProductNumber} == 8]
On {ProductShortName} 8.5 and earlier, create a new file in the `/etc/audit/rules.d/` directory named `44-installers.rules`, and insert the following rules:
+
----
-a always,exit -F perm=x -F path=/usr/bin/dnf-3 -F key=software-installer
-a always,exit -F perm=x -F path=/usr/bin/yum -F
----
+
You can add additional rules for other utilities that install software, for example `pip` and `npm`, using the same syntax.
endif::[]

. Load the audit rules:
+
[subs="quotes"]
----
# *augenrules --load*
----

.Verification

. List the loaded rules:
+
[subs="quotes"]
----
# *auditctl -l*
-p x-w /usr/bin/dnf-3 -k software-installer
-p x-w /usr/bin/yum -k software-installer
-p x-w /usr/bin/pip -k software-installer
-p x-w /usr/bin/npm -k software-installer
-p x-w /usr/bin/cpan -k software-installer
-p x-w /usr/bin/gem -k software-installer
-p x-w /usr/bin/luarocks -k software-installer
----

. Perform an installation, for example:
+
[subs="quotes,attributes"]
----
# *{PackageManagerCommand} reinstall -y vim-enhanced*
----

. Search the Audit log for recent installation events, for example:
+
[subs="quotes",options="nowrap",role="white-space-pre"]
----
# *ausearch -ts recent -k software-installer*
&#8211;&#8211;&#8211;&#8211;
time->Thu Dec 16 10:33:46 2021
type=PROCTITLE msg=audit(1639668826.074:298): proctitle=2F7573722F6C6962657865632F706C6174666F726D2D707974686F6E002F7573722F62696E2F646E66007265696E7374616C6C002D790076696D2D656E68616E636564
type=PATH msg=audit(1639668826.074:298): item=2 name="/lib64/ld-linux-x86-64.so.2" inode=10092 dev=fd:01 mode=0100755 ouid=0 ogid=0 rdev=00:00 obj=system_u:object_r:ld_so_t:s0 nametype=NORMAL cap_fp=0 cap_fi=0 cap_fe=0 cap_fver=0 cap_frootid=0
type=PATH msg=audit(1639668826.074:298): item=1 name="/usr/libexec/platform-python" inode=4618433 dev=fd:01 mode=0100755 ouid=0 ogid=0 rdev=00:00 obj=system_u:object_r:bin_t:s0 nametype=NORMAL cap_fp=0 cap_fi=0 cap_fe=0 cap_fver=0 cap_frootid=0
type=PATH msg=audit(1639668826.074:298): item=0 name="/usr/bin/dnf" inode=6886099 dev=fd:01 mode=0100755 ouid=0 ogid=0 rdev=00:00 obj=system_u:object_r:rpm_exec_t:s0 nametype=NORMAL cap_fp=0 cap_fi=0 cap_fe=0 cap_fver=0 cap_frootid=0
type=CWD msg=audit(1639668826.074:298): cwd="/root"
type=EXECVE msg=audit(1639668826.074:298): argc=5 a0="/usr/libexec/platform-python" a1="/usr/bin/dnf" a2="reinstall" a3="-y" a4="vim-enhanced"
type=SYSCALL msg=audit(1639668826.074:298): arch=c000003e syscall=59 success=yes exit=0 a0=55c437f22b20 a1=55c437f2c9d0 a2=55c437f2aeb0 a3=8 items=3 ppid=5256 pid=5375 auid=0 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=pts0 ses=3 comm="dnf" exe="/usr/libexec/platform-python3.6" subj=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023 key="software-installer"
----
