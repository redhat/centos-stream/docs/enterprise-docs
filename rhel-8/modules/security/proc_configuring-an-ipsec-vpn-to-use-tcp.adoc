:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_setting-up-an-ipsec-vpn.adoc

[id="proc_configuring-an-ipsec-vpn-to-use-tcp_{context}"]
= Configuring an IPsec VPN to use TCP

[role="_abstract"]
Libreswan supports TCP encapsulation of IKE and IPsec packets as described in RFC 8229. With this feature, you can establish IPsec VPNs on networks that prevent traffic transmitted via UDP and Encapsulating Security Payload (ESP). You can configure VPN servers and clients to use TCP either as a fallback or as the main VPN transport protocol. Because TCP encapsulation has bigger performance costs, use TCP as the main VPN protocol only if UDP is permanently blocked in your scenario.

.Prerequisites

* A xref:configuring-a-remote-access-vpn_{context}[remote-access VPN] is already configured.

.Procedure

. Add the following option to the `/etc/ipsec.conf` file in the `config setup` section:
+
----
listen-tcp=yes
----
. To use TCP encapsulation as a fallback option when the first attempt over UDP fails, add the following two options to the client's connection definition:
+
----
enable-tcp=fallback
tcp-remoteport=4500
----
+
Alternatively, if you know that UDP is permanently blocked, use the following options in the client's connection configuration:
+
----
enable-tcp=yes
tcp-remoteport=4500
----


[role="_additional-resources"]
.Additional resources
* link:https://tools.ietf.org/html/rfc8229[IETF RFC 8229: TCP Encapsulation of IKE and IPsec Packets]
