:_mod-docs-content-type: REFERENCE
:experimental:
// included in assembly_configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption.adoc

[id="deploying-high-availability-nbde-systems_{context}"]
= Deploying high-availability NBDE systems

[role="_abstract"]
Tang provides two methods for building a high-availability deployment:

Client redundancy (recommended):: Clients should be configured with the ability to bind to multiple Tang servers. In this setup, each Tang server has its own keys and clients can decrypt by contacting a subset of these servers. Clevis already supports this workflow through its `sss` plug-in. Red Hat recommends this method for a high-availability deployment.

Key sharing:: For redundancy purposes, more than one instance of Tang can be deployed. To set up a second or any subsequent instance, install the `tang` packages and copy the key directory to the new host using `rsync` over `SSH`. Note that Red Hat does not recommend this method because sharing keys increases the risk of key compromise and requires additional automation infrastructure.

[discrete]
== High-available NBDE using Shamir's Secret Sharing

Shamir's Secret Sharing (SSS) is a cryptographic scheme that divides a secret into several unique parts. To reconstruct the secret, a number of parts is required. The number is called threshold and SSS is also referred to as a thresholding scheme.

Clevis provides an implementation of SSS. It creates a key and divides it into a number of pieces. Each piece is encrypted using another pin including even SSS recursively. Additionally, you define the threshold `t`. If an NBDE deployment decrypts at least `t` pieces, then it recovers the encryption key and the decryption process succeeds. When Clevis detects a smaller number of parts than specified in the threshold, it prints an error message.

[discrete]
=== Example 1: Redundancy with two Tang servers

The following command decrypts a LUKS-encrypted device when at least one of two Tang servers is available:
[subs=+quotes]
----
# *clevis luks bind -d _/dev/sda1_ sss '{"t":1,"pins":{"tang":[{"url":"_http://tang1.srv_"},{"url":"_http://tang2.srv_"}]}}'*
----

The previous command used the following configuration scheme:
----
{
    "t":1,
    "pins":{
        "tang":[
            {
                "url":"http://tang1.srv"
            },
            {
                "url":"http://tang2.srv"
            }
        ]
    }
}
----
In this configuration, the SSS threshold `t` is set to `1` and the `clevis luks bind` command successfully reconstructs the secret if at least one from two listed `tang` servers is available.

[discrete]
=== Example 2: Shared secret on a Tang server and a TPM device

The following command successfully decrypts a LUKS-encrypted device when both the `tang` server and the `tpm2` device are available:
[subs=+quotes]
----
# *clevis luks bind -d _/dev/sda1_ sss '{"t":2,"pins":{"tang":[{"url":"_http://tang1.srv_"}], "tpm2": {"pcr_ids":"0,7"}}}'*
----
The configuration scheme with the SSS threshold 't' set to '2' is now:
----
{
    "t":2,
    "pins":{
        "tang":[
            {
                "url":"http://tang1.srv"
            }
        ],
        "tpm2":{
            "pcr_ids":"0,7"
        }
    }
}
----

[role="_additional-resources"]
.Additional resources
* `tang(8)` (section `High Availability`), `clevis(1)` (section `Shamir's Secret Sharing`), and `clevis-encrypt-sss(1)` man pages on your system
