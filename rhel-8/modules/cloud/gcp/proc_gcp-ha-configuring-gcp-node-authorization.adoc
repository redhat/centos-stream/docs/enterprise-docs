:_mod-docs-content-type: PROCEDURE
[id="gcp-ha-configuring-gcp-node-authorization_{context}"]
= Configuring GCP node authorization

Configure cloud SDK tools to use your account credentials to access GCP.


.Procedure

Enter the following command on each node to initialize each node with your project ID and account credentials.

[subs="+quotes"]
----
# *gcloud-ra init*
----
