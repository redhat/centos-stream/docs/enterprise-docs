:_mod-docs-content-type: PROCEDURE
[id="gcp-ha-installing-rhel-ha-packages-and-agents_{context}"]
= Installing HA packages and agents

On each of your nodes, you need to install the High Availability packages and agents to be able to configure a Red Hat High Availability cluster on Google Cloud Platform (GCP). 

// TODO to achieve what, exactly?

.Procedure

. In the Google Cloud Console, select *Compute Engine* and then select *VM instances*.

. Select the instance, click the arrow next to *SSH*, and select the *View* gcloud command option.

. Paste this command at a command prompt for passwordless access to the instance.

. Enable sudo account access and register with Red Hat Subscription Manager.

. Enable a Subscription Pool ID (or use the [command]`--auto-attach` command).

. Disable all repositories.
+
[subs="+quotes,attributes"]
----
# *subscription-manager repos --disable=**
----

. Enable the following repositories.
+
[subs="+quotes,attributes"]
----
# *subscription-manager repos --enable=rhel-{ProductNumber}-server-rpms*
# *subscription-manager repos --enable=rhel-{ProductNumber}-for-x86_64-highavailability-rpms*
----

. Install `pcs pacemaker`, the fence agents, and the resource agents.
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install -y pcs pacemaker fence-agents-gce resource-agents-gcp*
----

. Update all packages.
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} update -y*
----
