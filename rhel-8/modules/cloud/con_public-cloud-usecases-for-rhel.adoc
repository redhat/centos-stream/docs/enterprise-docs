:_mod-docs-content-type: CONCEPT


[id="public-cloud-usecases-for-rhel_{context}"]
= Public cloud use cases for RHEL
 
// TODO: Write up more specific use cases for cloud instances - highlight what workloads are best run in public clouds.

// TODO2: Tone down the buzzwordese?

Deploying on a public cloud provides many benefits, but might not be the most efficient solution in every scenario. If you are evaluating whether to migrate your RHEL deployments to the public cloud, consider whether your use case will benefit from the advantages of the public cloud.  

*Beneficial use cases*

* Deploying public cloud instances is very effective for flexibly increasing and decreasing the active computing power of your deployments, also known as _scaling up_ and _scaling down_. Therefore, using {ProductShortName} on public cloud is recommended in the following scenarios:

** Clusters with high peak workloads and low general performance requirements. Scaling up and down based on your demands can be highly efficient in terms of resource costs.

** Quickly setting up or expanding your clusters. This avoids high upfront costs of setting up local servers.

* Cloud instances are not affected by what happens in your local environment. Therefore, you can use them for backup and disaster recovery.

*Potentially problematic use cases*

* You are running an existing environment that cannot be adjusted. Customizing a cloud instance to fit the specific needs of an existing deployment may not be cost-effective in comparison with your current host platform.

* You are operating with a hard limit on your budget. Maintaining your deployment in a local data center typically provides less flexibility but more control over the maximum resource costs than the public cloud does.

.Next steps
* xref:obtaining-rhel-for-public-cloud-deployments_introducing-rhel-on-public-cloud-platforms[Obtaining RHEL for public cloud deployments]


[role="_additional-resources"]
.Additional resources

* link:https://www.redhat.com/en/blog/should-i-migrate-my-application-cloud-heres-how-decide[Should I migrate my application to the cloud? Here's how to decide.]
