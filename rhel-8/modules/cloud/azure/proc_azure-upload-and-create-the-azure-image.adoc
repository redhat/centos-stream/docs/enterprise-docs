:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_deploying-a-virtual-machine-on-microsoft-azure


[id="upload-image-to-azure_{context}"]
= Uploading and creating an Azure image

Complete the following steps to upload the `VHD` file to your container and create an Azure custom image.

[NOTE]
====
The exported storage connection string does not persist after a system reboot. If any of the commands in the following steps fail, export the connection string again.
//See xref:create-azure-resources-for-ha_{context}[Creating Resources in Microsoft Azure] for the steps for obtaining and exporting a connection string.
====

.Procedure

. Upload the `VHD` file to the storage container. It may take several minutes. To get a list of storage containers, enter the [command]`az storage container list` command.
+
[subs="+quotes,attributes"]
----
$ *az storage blob upload \*
    *--account-name <storage-account-name> --container-name <container-name> \*
    *--type page --file <path-to-vhd> --name <image-name>.vhd*
----
+
Example:
+
[subs="+quotes"]
----
[clouduser@localhost]$ *az storage blob upload \*
*--account-name azrhelclistact --container-name azrhelclistcont \*
*--type page --file rhel-image-{ProductNumber}.vhd --name rhel-image-{ProductNumber}.vhd*

Percent complete: %100.0
----

. Get the URL for the uploaded `VHD` file to use in the following step.
+
[subs="+quotes,attributes"]
----
$ *az storage blob url -c <container-name> -n <image-name>.vhd*
----
+
Example:
+
[subs="+quotes,+attributes"]
----
$ *az storage blob url -c azrhelclistcont -n rhel-image-{ProductNumber}.vhd
"https://azrhelclistact.blob.core.windows.net/azrhelclistcont/rhel-image-{ProductNumber}.vhd"*
----

. Create the Azure custom image.
+
[subs="+quotes,attributes"]
----
$ *az image create -n <image-name> -g <resource-group> -l <azure-region> --source <URL> --os-type linux*
----
+
[NOTE]
====
The default hypervisor generation of the VM is V1. You can optionally specify a V2 hypervisor generation by including the option `--hyper-v-generation V2`. Generation 2 VMs use a UEFI-based boot architecture. See link:https://docs.microsoft.com/en-us/azure/virtual-machines/linux/generation-2[Support for generation 2 VMs on Azure] for information about generation 2 VMs.

The command may return the error "Only blobs formatted as VHDs can be imported." This error may mean that the image was not aligned to the nearest 1 MB boundary before it was converted to `VHD`.
====
+
Example:
+
[subs="+quotes,+attributes"]
----
$ *az image create -n rhel{ProductNumber} -g azrhelclirsgrp2 -l southcentralus --source https://azrhelclistact.blob.core.windows.net/azrhelclistcont/rhel-image-{ProductNumber}.vhd --os-type linux*
----
