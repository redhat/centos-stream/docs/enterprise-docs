:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
// assembly_understanding-base-images-azure.adoc
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-reference-a.adoc
// * ID: [id='my-reference-a']
// * Title: = My reference A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="azure-vm-configuration-settings_{context}"]
= Azure VM configuration settings

Azure VMs must have the following configuration settings. Some of these settings are enabled during the initial VM creation. Other settings are set when provisioning the VM image for Azure. Keep these settings in mind as you move through the procedures. Refer to them as necessary.

.VM configuration settings
[options="header"]
|====
|Setting|Recommendation
|ssh|ssh must be enabled to provide remote access to your Azure VMs.
|dhcp|The primary virtual adapter should be configured for dhcp (IPv4 only).
|Swap Space|Do not create a dedicated swap file or swap partition. You can configure swap space with the Windows Azure Linux Agent (WALinuxAgent).
|NIC|Choose *virtio* for the primary virtual network adapter.
|encryption|For custom images, use Network Bound Disk Encryption (NBDE) for full disk encryption on Azure.
|====
