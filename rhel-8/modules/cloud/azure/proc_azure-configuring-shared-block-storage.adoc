:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// enterprise/assemblies/assembly_configuring-rhel-high-availability-on-azure.adoc


[id="azure-configuring-shared-block-storage_{context}"]
= Configuring shared block storage


To configure shared block storage for a {RH} High Availability cluster with Microsoft Azure Shared Disks, use the following procedure. Note that this procedure is optional, and the steps below assume three Azure VMs (a three-node cluster) with a 1 TB shared disk.

[NOTE]
====
This is a stand-alone sample procedure for configuring block storage. The procedure assumes that you have not yet created your cluster.
====

.Prerequisites

* You must have installed the Azure CLI on your host system and created your SSH key(s).
* You must have created your cluster environment in Azure, which includes creating the following resources. Links are to the Microsoft Azure documentation.
** link:https://docs.microsoft.com/en-us/azure/virtual-network/quick-create-cli#create-a-resource-group-and-a-virtual-network[Resource group]
** https://docs.microsoft.com/en-us/azure/virtual-network/quick-create-cli#create-a-resource-group-and-a-virtual-network[Virtual network]
** https://docs.microsoft.com/en-us/azure/virtual-network/manage-network-security-group[Network security group(s)]
** https://docs.microsoft.com/en-us/azure/virtual-network/manage-network-security-group#view-details-of-a-network-security-group[Network security group rules]
** https://docs.microsoft.com/en-us/azure/virtual-network/virtual-network-manage-subnet[Subnet(s)]
** https://docs.microsoft.com/en-us/azure/load-balancer/quickstart-load-balancer-standard-public-portal?tabs=option-1-create-load-balancer-standard[Load balancer (optional)]
** https://docs.microsoft.com/en-us/cli/azure/storage/account?view=azure-cli-latest#az_storage_account_create[Storage account]
** https://docs.microsoft.com/en-us/cli/azure/ppg?view=azure-cli-latest#az_ppg_create[Proximity placement group]
** https://docs.microsoft.com/en-us/cli/azure/vm/availability-set?view=azure-cli-latest#az_vm_availability_set_create[Availability set]


.Procedure

. Create a shared block volume by using the Azure command link:https://docs.microsoft.com/en-us/cli/azure/disk?view=azure-cli-latest#az_disk_create[`az disk create`].
+
[subs="+quotes,attributes"]
----
$ *az disk create -g <resource_group> -n <shared_block_volume_name> --size-gb <disk_size> --max-shares <number_vms> -l* <location>
----
+
For example, the following command creates a shared block volume named `shared-block-volume.vhd` in the resource group `sharedblock` within the Azure Availability Zone `westcentralus`.
+
[subs="+quotes,attributes"]
----
$ *az disk create -g sharedblock-rg -n shared-block-volume.vhd --size-gb 1024 --max-shares 3 -l westcentralus*

{
  "creationData": {
    "createOption": "Empty",
    "galleryImageReference": null,
    "imageReference": null,
    "sourceResourceId": null,
    "sourceUniqueId": null,
    "sourceUri": null,
    "storageAccountId": null,
    "uploadSizeBytes": null
  },
  "diskAccessId": null,
  "diskIopsReadOnly": null,
  "diskIopsReadWrite": 5000,
  "diskMbpsReadOnly": null,
  "diskMbpsReadWrite": 200,
  "diskSizeBytes": 1099511627776,
  "diskSizeGb": 1024,
  "diskState": "Unattached",
  "encryption": {
    "diskEncryptionSetId": null,
    "type": "EncryptionAtRestWithPlatformKey"
  },
  "encryptionSettingsCollection": null,
  "hyperVgeneration": "V1",
  "id": "/subscriptions/12345678910-12345678910/resourceGroups/sharedblock-rg/providers/Microsoft.Compute/disks/shared-block-volume.vhd",
  "location": "westcentralus",
  "managedBy": null,
  "managedByExtended": null,
  "maxShares": 3,
  "name": "shared-block-volume.vhd",
  "networkAccessPolicy": "AllowAll",
  "osType": null,
  "provisioningState": "Succeeded",
  "resourceGroup": "sharedblock-rg",
  "shareInfo": null,
  "sku": {
    "name": "Premium_LRS",
    "tier": "Premium"
  },
  "tags": {},
  "timeCreated": "2020-08-27T15:36:56.263382+00:00",
  "type": "Microsoft.Compute/disks",
  "uniqueId": "cd8b0a25-6fbe-4779-9312-8d9cbb89b6f2",
  "zones": null
}
----

. Verify that you have created the shared block volume by using the Azure command link:https://docs.microsoft.com/en-us/cli/azure/disk?view=azure-cli-latest#az_disk_show[`az disk show`].
+
[subs="+quotes,attributes"]
----
$ *az disk show -g <resource_group> -n <shared_block_volume_name>*
----
+
For example, the following command shows details for the shared block volume `shared-block-volume.vhd` within the resource group `sharedblock-rg`.
+
[subs="+quotes,attributes"]
----
$ *az disk show -g sharedblock-rg -n shared-block-volume.vhd*

{
  "creationData": {
    "createOption": "Empty",
    "galleryImageReference": null,
    "imageReference": null,
    "sourceResourceId": null,
    "sourceUniqueId": null,
    "sourceUri": null,
    "storageAccountId": null,
    "uploadSizeBytes": null
  },
  "diskAccessId": null,
  "diskIopsReadOnly": null,
  "diskIopsReadWrite": 5000,
  "diskMbpsReadOnly": null,
  "diskMbpsReadWrite": 200,
  "diskSizeBytes": 1099511627776,
  "diskSizeGb": 1024,
  "diskState": "Unattached",
  "encryption": {
    "diskEncryptionSetId": null,
    "type": "EncryptionAtRestWithPlatformKey"
  },
  "encryptionSettingsCollection": null,
  "hyperVgeneration": "V1",
  "id": "/subscriptions/12345678910-12345678910/resourceGroups/sharedblock-rg/providers/Microsoft.Compute/disks/shared-block-volume.vhd",
  "location": "westcentralus",
  "managedBy": null,
  "managedByExtended": null,
  "maxShares": 3,
  "name": "shared-block-volume.vhd",
  "networkAccessPolicy": "AllowAll",
  "osType": null,
  "provisioningState": "Succeeded",
  "resourceGroup": "sharedblock-rg",
  "shareInfo": null,
  "sku": {
    "name": "Premium_LRS",
    "tier": "Premium"
  },
  "tags": {},
  "timeCreated": "2020-08-27T15:36:56.263382+00:00",
  "type": "Microsoft.Compute/disks",
  "uniqueId": "cd8b0a25-6fbe-4779-9312-8d9cbb89b6f2",
  "zones": null
}
----

. Create three network interfaces by using the Azure command link:https://docs.microsoft.com/en-us/cli/azure/network/nic?view=azure-cli-latest#az_network_nic_create[`az network nic create`]. Run the following command three times by using a different `<nic_name>` for each.
+
[subs="+quotes,attributes"]
----
$ *az network nic create \*
    *-g <resource_group> -n <nic_name> --subnet <subnet_name> \*
    *--vnet-name <virtual_network> --location <location> \*
    *--network-security-group <network_security_group> --private-ip-address-version IPv4*
----
+
For example, the following command creates a network interface with the name `shareblock-nodea-vm-nic-protected`.
+
[subs="+quotes,attributes"]
----
$ *az network nic create \*
    *-g sharedblock-rg -n sharedblock-nodea-vm-nic-protected --subnet sharedblock-subnet-protected \*
    *--vnet-name sharedblock-vn --location westcentralus \*
    *--network-security-group sharedblock-nsg --private-ip-address-version IPv4*
----

. Create three VMs and attach the shared block volume by using the Azure command link:https://docs.microsoft.com/en-us/cli/azure/vm?view=azure-cli-latest#az_vm_create[`az vm create`]. Option values are the same for each VM except that each VM has its own `<vm_name>`, `<new_vm_disk_name>`, and `<nic_name>`.
+
[subs="+quotes,attributes"]
----
$ *az vm create \*
    *-n <vm_name> -g <resource_group> --attach-data-disks <shared_block_volume_name> \*
    *--data-disk-caching None --os-disk-caching ReadWrite --os-disk-name <new-vm-disk-name> \* 
    *--os-disk-size-gb <disk_size> --location <location> --size <virtual_machine_size> \*
    *--image <image_name> --admin-username <vm_username> --authentication-type ssh \*
    *--ssh-key-values <ssh_key> --nics <nic_name> --availability-set <availability_set> --ppg <proximity_placement_group>*
----
+
For example, the following command creates a VM named `sharedblock-nodea-vm`.
+
[subs="+quotes,attributes"]
----
$ *az vm create \*
*-n sharedblock-nodea-vm -g sharedblock-rg --attach-data-disks shared-block-volume.vhd \*
*--data-disk-caching None --os-disk-caching ReadWrite --os-disk-name sharedblock-nodea-vm.vhd \* 
*--os-disk-size-gb 64 --location westcentralus --size Standard_D2s_v3 \*
*--image /subscriptions/12345678910-12345678910/resourceGroups/sample-azureimagesgroupwestcentralus/providers/Microsoft.Compute/images/sample-azure-rhel-{ProductNumber}.3.0-20200713.n.0.x86_64 --admin-username sharedblock-user --authentication-type ssh \*
*--ssh-key-values @sharedblock-key.pub --nics sharedblock-nodea-vm-nic-protected --availability-set sharedblock-as --ppg sharedblock-ppg*

{
  "fqdns": "",
  "id": "/subscriptions/12345678910-12345678910/resourceGroups/sharedblock-rg/providers/Microsoft.Compute/virtualMachines/sharedblock-nodea-vm",
  "location": "westcentralus",
  "macAddress": "00-22-48-5D-EE-FB",
  "powerState": "VM running",
  "privateIpAddress": "198.51.100.3",
  "publicIpAddress": "",
  "resourceGroup": "sharedblock-rg",
  "zones": ""
}
----

.Verification

. For each VM in your cluster, verify that the block device is available by using the [command]`ssh` command with your VM's IP address.
+
[subs="+quotes,attributes",options="nowrap", role=white-space-pre]
----
# *ssh _<ip_address>_ "hostname ; lsblk -d | grep ' 1T '"*
----
+
For example, the following command lists details including the host name and block device for the VM IP `198.51.100.3`.
+
[subs="quotes",options="nowrap", role=white-space-pre]
----
# *ssh 198.51.100.3 "hostname ; lsblk -d | grep ' 1T '"*

nodea
sdb    8:16   0    1T  0 disk
----
. Use the [command]`ssh` command to verify that each VM in your cluster uses the same shared disk.
+
[subs="+quotes",options="nowrap", role=white-space-pre]
----
# *ssh _<ip_address>_ "hostname ; lsblk -d | grep ' 1T ' | awk '{print \$1}' | xargs -i udevadm info --query=all --name=/dev/{} | grep '^E: ID_SERIAL='"*
----
+
For example, the following command lists details including the host name and shared disk volume ID for the instance IP address `198.51.100.3`.
+
[subs="quotes",options="nowrap", role=white-space-pre]
----
# *ssh 198.51.100.3 "hostname ; lsblk -d | grep ' 1T ' | awk '{print \$1}' | xargs -i udevadm info --query=all --name=/dev/{} | grep '^E: ID_SERIAL='"*

nodea
E: ID_SERIAL=3600224808dd8eb102f6ffc5822c41d89
----

After you have verified that the shared disk is attached to each VM, you can configure resilient storage for the cluster.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_configuring-gfs2-in-a-cluster-configuring-and-managing-high-availability-clusters#proc_configuring-gfs2-in-a-cluster.adoc-configuring-gfs2-cluster[Configuring a GFS2 file system in a cluster]

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_gfs2_file_systems/index[Configuring GFS2 file systems]
