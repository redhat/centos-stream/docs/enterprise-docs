:_mod-docs-content-type: PROCEDURE
[id="azure-create-a-fencing-device-in-ha_{context}"]
= Creating a fencing device

Complete the following steps to configure fencing. Complete these commands from any node in the cluster

.Prerequisites

You need to set the cluster property `stonith-enabled` to `true`.


.Procedure

. Identify the Azure node name for each RHEL VM. You use the Azure node names to configure the fence device.
+
[subs="+quotes,attributes",options="nowrap", role=white-space-pre]
----
# *fence_azure_arm \*
    *-l _<AD-Application-ID>_ -p _<AD-Password>_ \*
    *--resourceGroup _<MyResourceGroup>_ --tenantId _<Tenant-ID>_ \*
    *--subscriptionId _<Subscription-ID>_ -o list*
----
+
Example:
+
[subs="+quotes,attributes",options="nowrap", role=white-space-pre]
----
[root@node01 clouduser]# *fence_azure_arm \*
*-l e04a6a49-9f00-xxxx-xxxx-a8bdda4af447 -p z/a05AwCN0IzAjVwXXXXXXXEWIoeVp0xg7QT//JE=*
*--resourceGroup azrhelclirsgrp --tenantId 77ecefb6-cff0-XXXX-XXXX-757XXXX9485* 
*--subscriptionId XXXXXXXX-38b4-4527-XXXX-012d49dfc02c -o list*

node01,
node02,
node03,
----

. View the options for the Azure ARM STONITH agent.
+
[literal,subs="quotes"]
--
# *pcs stonith describe fence_azure_arm*
--
+
Example:
+
[literal,subs="quotes"]
--
# *pcs stonith describe fence_apc*
Stonith options:
password: Authentication key
password_script: Script to run to retrieve password
--
+
[WARNING]
====
For fence agents that provide a method option, do not specify a value of cycle as it is not supported and can cause data corruption.
====
+
Some fence devices can fence only a single node, while other devices can fence multiple nodes. The parameters you specify when you create a fencing device depend on what your fencing device supports and requires.
+
You can use the `pcmk_host_list` parameter when creating a fencing device to specify all of the machines that are controlled by that fencing device.
+
You can use `pcmk_host_map` parameter when creating a fencing device to map host names to the specifications that comprehends the fence device.

. Create a fence device.
+
[subs="+quotes"]
----
# *pcs stonith create _clusterfence_ fence_azure_arm*
----

. To ensure immediate and complete fencing, disable ACPI Soft-Off on all cluster nodes. For information about disabling ACPI Soft-Off, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/configuring_and_managing_high_availability_clusters/index#proc_configuring-acpi-for-fence-devices-configuring-fencing[Disabling ACPI for use with integrated fence device].

.Verification

. Test the fencing agent for one of the other nodes.

+
[subs="+quotes"]
----
# *pcs stonith fence _azurenodename_*
----
+
Example:
+
[subs="+quotes,attributes"]
----
[root@node01 clouduser]# *pcs status*
Cluster name: newcluster
Stack: corosync
Current DC: node01 (version 1.1.18-11.el7-2b07d5c5a9) - partition with quorum
Last updated: Fri Feb 23 11:44:35 2018
Last change: Fri Feb 23 11:21:01 2018 by root via cibadmin on node01

3 nodes configured
1 resource configured

Online: [ node01 node03 ]
OFFLINE: [ node02 ]

Full list of resources:

  clusterfence  (stonith:fence_azure_arm):  Started node01

Daemon Status:
  corosync: active/disabled
  pacemaker: active/disabled
  pcsd: active/enabled
----
+

. Start the node that was fenced in the previous step.
+
[subs="+quotes,verbatim,macros,attributes"]
----
# *pcs cluster start _<hostname>_*
----

. Check the status to verify the node started.
+
[subs="+quotes,verbatim,macros,attributes"]
----
# *pcs status*
----
+
Example:
+
[subs="quotes,attributes"]
----
[root@node01 clouduser]# *pcs status*
Cluster name: newcluster
Stack: corosync
Current DC: node01 (version 1.1.18-11.el7-2b07d5c5a9) - partition with quorum
Last updated: Fri Feb 23 11:34:59 2018
Last change: Fri Feb 23 11:21:01 2018 by root via cibadmin on node01

3 nodes configured
1 resource configured

Online: [ node01 node02 node03 ]

Full list of resources:

clusterfence    (stonith:fence_azure_arm):  Started node01

Daemon Status:
  corosync: active/disabled
  pacemaker: active/disabled
  pcsd: active/enabled
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/15575[Fencing in a Red Hat High Availability Cluster] (Red Hat Knowledgebase)
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_high_availability_clusters/assembly_configuring-fencing-configuring-and-managing-high-availability-clusters#ref_general-fence-device-properties-configuring-fencing[General properties of fencing devices]
