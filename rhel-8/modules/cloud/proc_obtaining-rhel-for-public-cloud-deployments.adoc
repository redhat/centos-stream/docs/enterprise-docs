:_mod-docs-content-type: REFERENCE

[id="obtaining-rhel-for-public-cloud-deployments_{context}"]
= Obtaining RHEL for public cloud deployments

To deploy a {ProductShortName} system in a public cloud environment, you need to:

. Select the optimal cloud provider for your use case, based on your requirements and the current offer on the market.
+
The cloud providers currently certified for running {ProductShortName} instances are:
+
* link:https://aws.amazon.com/[Amazon Web Services (AWS)]
ifdef::cloud-init-title[]
** For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/deploying_rhel_{ProductNumber}_on_amazon_web_services/index[Deploying RHEL {ProductNumber} on Amazon Web Services].
endif::[]
* link:https://cloud.google.com/[Google Cloud Platform (GCP)]
ifdef::cloud-init-title[]
** For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/deploying_rhel_{ProductNumber}_on_google_cloud_platform/index[Deploying RHEL {ProductNumber} on Google Cloud Platform].
endif::[]
* link:https://azure.microsoft.com/en-us/[Microsoft Azure]
ifdef::cloud-init-title[]
** For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/deploying_rhel_{ProductNumber}_on_microsoft_azure/index[Deploying RHEL {ProductNumber} on Microsoft Azure].
endif::[]
+
ifdef::cloud-content-aws[]
[NOTE]
====
This document specifically talks about deploying {ProductShortName} on AWS.
====
endif::cloud-content-aws[]
ifdef::cloud-content-gcp[]
[NOTE]
====
This document specifically talks about deploying {ProductShortName} on GCP.
====
endif::cloud-content-gcp[]
ifdef::cloud-content-azure[]
[NOTE]
====
This document specifically talks about deploying {ProductShortName} on Microsoft Azure.
====
endif::cloud-content-azure[]

. Create a {ProductShortName} cloud instance on your chosen cloud platform. For more information, see xref:methods-for-creating-rhel-cloud-instances_introducing-rhel-on-public-cloud-platforms[Methods for creating RHEL cloud instances].


. To keep your {ProductShortName} deployment up-to-date, use link:https://access.redhat.com/products/red-hat-update-infrastructure[Red Hat Update Infrastructure] (RHUI).
+
//or link:https://www.redhat.com/en/technologies/management/satellite[Red Hat Satellite] <= RHUI SHOULD BE OUR RECOMMENDED WAY TO GO (?)

[role="_additional-resources"]
.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_update_infrastructure[RHUI documentation]
// * link:https://access.redhat.com/documentation/en-us/red_hat_satellite[Red Hat Satellite documentation]
* link:https://www.redhat.com/en/products/open-hybrid-cloud[Red Hat Open Hybrid Cloud]
