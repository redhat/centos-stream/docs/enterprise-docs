// Currently included in the following assemblies:
//
// Setting up IP address resources  on AWS (context: configuring-aws-network-resource-agents)

:_content-type: PROCEDURE

[id="creating-a-cluster-resource-to-manage-a-private-ip-address-limited-to-a-single-aws-availability-zone_{context}"]
= Creating an IP address resource to manage a private IP address limited to a single AWS Availability Zone

To ensure that high-availability (HA) clients on AWS can access a {ProductShortName} {ProductNumber} node that uses a a private IP address that can only move in a single AWS Availability Zone (AZ), configure an _AWS Secondary Private IP Address_ (`awsvip`) resource to use a virtual IP address.

You can complete the following procedure on any node in the cluster.

.Prerequisites

* You have a previously configured cluster.
* Your cluster nodes have access to the RHEL HA repositories. For more information, see xref:aws-installing-rhel-ha-packages-and-agents_configuring-a-red-hat-high-availability-cluster-on-aws[Installing the High Availability packages and agents].
* You have set up the AWS CLI. For instructions, see xref:installing-aws-command-line-interface_deploying-a-virtual-machine-on-aws[Installing the AWS CLI].

.Procedure

ifeval::[{ProductNumber} == 8]
. Install the `resource-agents` package.
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install resource-agents*
----
endif::[]

ifeval::[{ProductNumber} == 9]
. Install the `resource-agents-cloud` package.
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install resource-agents-cloud*
----
+
// Also obtainable from https://access.redhat.com/downloads/content/resource-agents-cloud/4.10.0-23.el9_1.3/x86_64/fd431d51/package
endif::[]

. Optional: View the `awsvip` description. This shows the options and default operations for this agent.
+
[subs="+quotes,attributes"]
----
# *pcs resource describe awsvip*
----
+
// TODO example output for this scenario?

. Create a Secondary Private IP address with an unused private IP address in the `VPC CIDR` block. In addition, create a resource group that the Secondary Private IP address will belong to.
+
[subs="+quotes,attributes"]
----
# *pcs resource create <resource-id> awsvip secondary_private_ip=_<Unused-IP-Address>_ --group _<group-name>_*
----
+
Example:
+
[subs="+quotes,attributes"]
----
[root@ip-10-0-0-48 ~]# *pcs resource create privip awsvip secondary_private_ip=10.0.0.68 --group networking-group*
----

. Create a virtual IP resource. This is a VPC IP address that can be rapidly remapped from the fenced node to the failover node, masking the failure of the fenced node within the subnet. Ensure that the virtual IP belongs to the same resource group as the Secondary Private IP address you created in the previous step.
+
[subs="+quotes,attributes"]
----
# *pcs resource create <resource-id> IPaddr2 ip=_<secondary-private-IP>_ --group _<group-name>_*
----
+
Example: 
+
[subs="+quotes,attributes"]
----
root@ip-10-0-0-48 ~]# *pcs resource create vip IPaddr2 ip=10.0.0.68 --group networking-group*
----
+
// What is the IPaddr2 doing there?


.Verification

* Display the status of the cluster to verify that the required resources are running.
+
[subs="+quotes,attributes"]
----
# *pcs status*
----
+
The following output shows an example running cluster where the `vip` and `privip` resources have been started as a part of the `networking-group` resource group:
+
[subs="+quotes,attributes"]
----
[root@ip-10-0-0-48 ~]# *pcs status*
Cluster name: newcluster
Stack: corosync
Current DC: ip-10-0-0-46 (version 1.1.18-11.el7-2b07d5c5a9) - partition with quorum
Last updated: Fri Mar  2 22:34:24 2018
Last change: Fri Mar  2 22:14:58 2018 by root via cibadmin on ip-10-0-0-46

3 nodes configured
3 resources configured

Online: [ ip-10-0-0-46 ip-10-0-0-48 ip-10-0-0-58 ]

Full list of resources:

clusterfence    (stonith:fence_aws):    Started ip-10-0-0-46
 *Resource Group: networking-group*
     *privip (ocf::heartbeat:awsvip):    Started ip-10-0-0-48*
     *vip    (ocf::heartbeat:IPaddr2):   Started ip-10-0-0-58*

Daemon Status:
  corosync: active/disabled
  pacemaker: active/disabled
  pcsd: active/enabled
----
+
// TODO? Add an example failure, and how to amend it?