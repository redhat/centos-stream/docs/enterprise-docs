:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_uploading-the-rhel-image-to-aws

[id="importing-your-image-as-a-snapshot_{context}"]
= Importing your image as a snapshot

To launch a RHEL instance in the Amazon Elastic Cloud Compute (EC2) service, you require an Amazon Machine Image (AMI). To create an AMI of your system, you must first upload a snapshot of your RHEL system image to EC2.

.Procedure

. Create a file to specify a bucket and path for your image. Name the file `containers.json`. In the sample that follows, replace `s3-bucket-name` with your bucket name and `s3-key` with your key. You can get the key for the image by using the Amazon S3 Console.
+
[subs="+attributes"]
----
{
    "Description": "rhel-{ProductNumber}.0-sample.raw",
    "Format": "raw",
    "UserBucket": {
        "S3Bucket": "s3-bucket-name",
        "S3Key": "s3-key"
    }
}
----

. Import the image as a snapshot. This example uses a public Amazon S3 file; you can use the link:https://console.aws.amazon.com/s3/[Amazon S3 Console] to change permissions settings on your bucket.
+
[subs="+quotes,attributes"]
----
$ *aws ec2 import-snapshot --disk-container file://containers.json*
----
+
The terminal displays a message such as the following. Note the `ImportTaskID` within the message.
+
[subs="+attributes"]
----
{
    "SnapshotTaskDetail": {
        "Status": "active",
        "Format": "RAW",
        "DiskImageSize": 0.0,
        "UserBucket": {
            "S3Bucket": "s3-bucket-name",
            "S3Key": "rhel-{ProductNumber}.0-sample.raw"
        },
        "Progress": "3",
        "StatusMessage": "pending"
    },
    "ImportTaskId": "import-snap-06cea01fa0f1166a8"
}
----

. Track the progress of the import by using the [command]`describe-import-snapshot-tasks` command. Include the `ImportTaskID`.
+
[subs="+quotes,attributes"]
----
$ *aws ec2 describe-import-snapshot-tasks --import-task-ids import-snap-06cea01fa0f1166a8*
----
+
The returned message shows the current status of the task. When complete, `Status` shows `completed`. Within the status, note the snapshot ID.

[role="_additional-resources"]
.Additional resources
 * link:https://console.aws.amazon.com/s3/[Amazon S3 Console]
 * link:https://docs.aws.amazon.com/vm-import/latest/userguide/vmimport-import-snapshot.html[Importing a Disk as a Snapshot Using VM Import/Export]
