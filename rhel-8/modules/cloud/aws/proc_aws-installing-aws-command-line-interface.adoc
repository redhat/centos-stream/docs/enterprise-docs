:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_deploying-a-virtual-machine-on-aws

[id="installing-aws-command-line-interface_{context}"]
= Installing the AWS CLI

Many of the procedures required to manage HA clusters in AWS include using the AWS CLI.

.Prerequisites

* You have created an AWS Access Key ID and an AWS Secret Access Key, and have access to them. For instructions and details, see link:https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html#cli-quick-configuration[Quickly Configuring the AWS CLI].


.Procedure


. Install the link:https://aws.amazon.com/cli/[AWS command line tools] by using the [command]`{PackageManagerCommand}` command.
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install awscli*
----

. Use the [command]`aws --version` command to verify that you installed the AWS CLI.
+
[subs="+quotes,attributes"]
----
$ *aws --version*
aws-cli/1.19.77 Python/3.6.15 Linux/5.14.16-201.fc34.x86_64 botocore/1.20.77
----

. Configure the AWS command line client according to your AWS access details.
+
[subs="+quotes,attributes"]
----
$ *aws configure*
AWS Access Key ID [None]:
AWS Secret Access Key [None]:
Default region name [None]:
Default output format [None]:
----

[role="_additional-resources"]
.Additional resources
 * link:https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html#cli-quick-configuration[Quickly Configuring the AWS CLI]
 * link:https://aws.amazon.com/cli/[AWS command line tools]
