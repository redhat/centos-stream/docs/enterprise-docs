:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_uploading-the-rhel-image-to-aws

[id="converting-and-pushing-your-image-to-s3_{context}"]
= Converting and pushing your image to S3

By using the `qemu-img` command, you can convert your image, so that you can push it to S3. The samples are representative; they convert an image formatted in the `qcow2` file format to `raw` format. Amazon accepts images in `OVA`, `VHD`, `VHDX`, `VMDK`, and `raw` formats. See link:https://docs.aws.amazon.com/vm-import/latest/userguide/how-vm-import-export-works.html[How VM Import/Export Works] for more information about image formats that Amazon accepts.

// TODO: What is "samples are representative" supposed to mean here? Does it imply that this is one of the several possible ways? If so, that should be more explicit, or maybe not said at all (given that all our docs is "opinionated advice").

.Procedure

. Run the [command]`qemu-img` command to convert your image. For example:
+
[subs="+quotes,attributes"]
----
# *qemu-img convert -f qcow2 -O raw rhel-{ProductNumber}.0-sample.qcow2 rhel-{ProductNumber}.0-sample.raw*
----

. Push the image to S3.
+
[subs="+quotes,attributes"]
----
$ *aws s3 cp rhel-{ProductNumber}.0-sample.raw s3://s3-bucket-name*
----
+
[NOTE]
====
This procedure could take a few minutes. After completion, you can check that your image uploaded successfully to your S3 bucket by using the link:https://console.aws.amazon.com/s3/[AWS S3 Console].
====

[role="_additional-resources"]
.Additional resources
 * link:https://docs.aws.amazon.com/vm-import/latest/userguide/how-vm-import-export-works.html[How VM Import/Export Works]
 * link:https://console.aws.amazon.com/s3/[AWS S3 Console]
