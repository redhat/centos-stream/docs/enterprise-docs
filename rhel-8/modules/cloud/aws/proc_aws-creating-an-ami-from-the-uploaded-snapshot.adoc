:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_uploading-the-rhel-image-to-aws


[id="creating-an-ami-from-the-uploaded-snapshot_{context}"]
= Creating an AMI from the uploaded snapshot

To launch a RHEL instance in Amazon Elastic Cloud Compute (EC2) service, you require an Amazon Machine Image (AMI). To create an AMI of your system, you can use a RHEL system snapshot that you previously uploaded.

.Procedure

. Go to the AWS EC2 Dashboard.
+
//link?

. Under *Elastic Block Store*, select *Snapshots*.

. Search for your snapshot ID (for example, `snap-0e718930bd72bcda0`).

. Right-click on the snapshot and select *Create image*.

. Name your image.

. Under *Virtualization type*, choose *Hardware-assisted virtualization*.

. Click *Create*. In the note regarding image creation, there is a link to your image.

. Click on the image link. Your image shows up under *Images>AMIs*.
+
[NOTE]
====
Alternatively, you can use the AWS CLI [command]`register-image` command to create an AMI from a snapshot. See link:https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ec2/register-image.html[register-image] for more information. An example follows.

[subs="+quotes,attributes"]
----
$ *aws ec2 register-image \*
    *--name "myimagename" --description "myimagedescription" --architecture x86_64 \*
    *--virtualization-type hvm --root-device-name "/dev/sda1" --ena-support \*
    *--block-device-mappings "{\"DeviceName\": \"/dev/sda1\",\"Ebs\": {\"SnapshotId\": \"snap-0ce7f009b69ab274d\"}}"*
----

You must specify the root device volume `/dev/sda1` as your `root-device-name`. For conceptual information about device mapping for AWS, see link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/block-device-mapping-concepts.html#block-device-mapping-ex[Example block device mapping].
====
