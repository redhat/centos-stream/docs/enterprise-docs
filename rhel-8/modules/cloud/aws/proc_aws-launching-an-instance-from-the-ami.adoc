:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_uploading-the-rhel-image-to-aws
//
// <List assemblies here, each on a new line>

[id="launching-an-instance-from-the-ami_{context}"]
= Launching an instance from the AMI

To launch and configure an Amazon Elastic Compute Cloud (EC2) instance, use an Amazon Machine Image (AMI).

.Procedure

. From the AWS EC2 Dashboard, select *Images* and then *AMIs*.

. Right-click on your image and select *Launch*.

. Choose an *Instance Type* that meets or exceeds the requirements of your workload.
+
See link:https://aws.amazon.com/ec2/instance-types/[Amazon EC2 Instance Types] for information about instance types.

. Click *Next: Configure Instance Details*.
.. Enter the *Number of instances* you want to create.
.. For *Network*, select the VPC you created when link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html#create-a-vpc[setting up your AWS environment]. Select a subnet for the instance or create a new subnet.
.. Select *Enable* for Auto-assign Public IP.
+
[NOTE]
====
These are the minimum configuration options necessary to create a basic instance. Review additional options based on your application requirements.
====

. Click *Next: Add Storage*. Verify that the default storage is sufficient.

. Click *Next: Add Tags*.
+
[NOTE]
====
Tags can help you manage your AWS resources. See link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html?icmpid=docs_ec2_console[Tagging Your Amazon EC2 Resources] for information about tagging.
====

. Click *Next: Configure Security Group*. Select the security group you created when link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html#create-a-base-security-group[setting up your AWS environment].

. Click *Review and Launch*. Verify your selections.

. Click *Launch*. You are prompted to select an existing key pair or create a new key pair. Select the key pair you created when link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html#create-a-key-pair[setting up your AWS environment].
+
[NOTE]
====
Verify that the permissions for your private key are correct. Use the command options [command]`chmod 400 <keyname>.pem` to change the permissions, if necessary.
====

. Click *Launch Instances*.

. Click *View Instances*. You can name the instance(s).
+
You can now launch an SSH session to your instance(s) by selecting an instance and clicking *Connect*. Use the example provided for *A standalone SSH client*.
+
[NOTE]
====
Alternatively, you can launch an instance by using the AWS CLI. See link:https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2-instances.html[Launching, Listing, and Terminating Amazon EC2 Instances] in the Amazon documentation for more information.
====

[role="_additional-resources"]
.Additional resources
* link:https://console.aws.amazon.com/console/[AWS Management Console]
* link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html[Setting Up with Amazon EC2]
* link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Instances.html[Amazon EC2 Instances]
* link:https://aws.amazon.com/ec2/instance-types/[Amazon EC2 Instance Types]
