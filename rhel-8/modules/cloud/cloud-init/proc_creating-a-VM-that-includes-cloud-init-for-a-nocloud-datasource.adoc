:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// enterprise/titles/configuring-and-maintaining/configuring-and-managing-cloud-init-for-rhel-8/assemblies/assembly_configuring-cloud-init.adoc

[id="creating-a-vm-that-includes-cloud-init-for-a-nocloud-datasource_{context}"]
= Creating a virtual machine that includes cloud-init for a NoCloud datasource

To create a new virtual machine (VM) that includes `cloud-init`, create a `meta-data` file and a `user-data` file.

* The `meta-data` file includes instance details.
* The `user-data` file includes information to create a user and grant access.

Include these files in a new ISO image, and attach the ISO file to a new VM created from a KVM Guest Image. In this scenario, the datasource is NoCloud.

.Procedure

. Create a directory named `cloudinitiso` and set is as your working directory:
+
[subs="+quotes"]
----
$ *mkdir cloudinitiso*
$ *cd cloudinitiso*
----

. Create the `meta-data` file and add the following information:
+
[subs="+quotes"]
----
instance-id: citest
local-hostname: citest-1
----

. Create the `user-data` file and add the following information:
+
[subs="+quotes"]
----
#cloud-config
password: cilogon
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ssh-rsa AAA...fhHQ== sample@redhat.com
----
+
[NOTE]
====
The last line of the `user-data` file references an SSH public key. Find your SSH public keys in `~/.ssh/id_rsa.pub`. When trying this sample procedure, modify the line to include one of your public keys.
====

. Use the `genisoimage` command to create an ISO image that includes `user-data` and `meta-data`:
+
[subs="+quotes"]
----
# *genisoimage -output ciiso.iso -volid cidata -joliet -rock user-data meta-data*

I: -input-charset not specified, using utf-8 (detected in locale settings)
Total translation table size: 0
Total rockridge attributes bytes: 331
Total directory bytes: 0
Path table size(bytes): 10
Max brk space used 0
183 extents written (0 MB)
----

. Download a KVM Guest Image from the Red Hat Customer Portal to the `/var/lib/libvirt/images` directory.

. Create a new VM from the KVM Guest Image using the `virt-install` utility and attach the downloaded image to the existing image:
+
[subs="+quotes, attributes"]
----
# *virt-install \*
    *--memory 4096 \*
    *--vcpus 4 \*
    *--name mytestcivm \*
    *--disk /var/lib/libvirt/images/rhel-8.1-x86_64-kvm.qcow2,device=disk,bus=virtio,format=qcow2 \*
    *--disk /home/sample/cloudinitiso/ciiso.iso,device=cdrom \*
    *--os-type Linux \*
    *--os-variant rhel{ProductNumber}.0 \*
    *--virt-type kvm \*
    *--graphics none \*
    *--import*
----

. Log on to your image with username `cloud-user` and password `cilogon`:
+
[subs="+quotes"]
----
citest-1 login: cloud-user
Password:
[cloud-user@citest-1 ~]$
----

.Verification

* Check the `cloud-init` status to confirm that the utility has completed its defined tasks:
+
[subs="+quotes"]
----
[cloud-user@citest-1 instance]$ *cloud-init status*
status: done
----

* The `cloud-init` utility creates the `cloud-init` directory layout under `/var/lib/cloud` when it runs, and it updates or changes certain directory contents based upon the directives you have specified.
+
For example, you can confirm that the datasource is `NoCloud` by checking the datasource file.
+
[subs="+quotes"]
----
$ *cd /var/lib/cloud/instance*
$ *cat datasource*
DataSourceNoCloud: DataSourceNoCloud [seed=/dev/sr0][dsmode=net]
----

* `cloud-init` copies user-data into `/var/lib/cloud/instance/user-data.txt`:
+
[subs="+quotes"]
----
$ *cat user-data.txt*
#cloud-config
password: cilogon
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ssh-rsa AAA...fhHQ== sample@redhat.com
----

[NOTE]
====
For OpenStack, the link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.0/html/creating_and_managing_instances/index[Creating and managing instances] includes information for configuring an instance using `cloud-init`. See link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/17.0/html/creating_and_managing_instances/assembly_creating-a-customized-instance_instances[Creating a customized instance] for specific procedures.
====

[role="_additional-resources"]
.Additional resources
* link:https://cloudinit.readthedocs.io/en/22.4.2/topics/datasources/nocloud.html[Upstream documentation for the NoCloud data source]
