:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// enterprise/titles/configuring-and-maintaining/configuring-and-managing-cloud-init-for-rhel-8/assemblies/assembly_configuring-cloud-init.adoc

[id="setting-up-a-static-networking-configuration-with-cloud-init_{context}"]
= Setting up a static networking configuration with cloud-init

You can set up network configuration with `cloud-init` by adding a `network-interfaces` section to the metadata.

{ProductName} provides its default networking service through `NetworkManager`, a dynamic network control and configuration daemon that keeps network devices and connections up and active when they are available.

Your datasource might provide a network configuration. For details, see the `cloud-init` section link:https://cloudinit.readthedocs.io/en/latest/topics/network-config.html#network-configuration-sources[Network Configuration Sources].

If you do not specify network configuration for `cloud-init` and have not disabled network configuration, `cloud-init` tries to determine if any attached devices have a connection. If it finds a connected device, it generates a network configuration that issues a DHCP request on the interface. Refer to the `cloud-init` documentation section link:https://cloudinit.readthedocs.io/en/latest/topics/network-config.html#fallback-network-configuration[Fallback Network Configuration] for more information.

.Procedure

The following example adds a static networking configuration.

include::reuse/include_directives.adoc[tag=where-to-add-directives-user-data]

+
. Add a `network-interfaces` section.
+
[options="nowrap"]
----
network:
  version: 1
  config:
    - type: physical
      name: eth0
      subnets:
        - type: static
          address: 192.0.2.1/24
          gateway: 192.0.2.254
----

[NOTE]
====
You can disable a network configuration by adding the following information to your metadata.

[subs="+quotes,attributes"]
----
network:
  config: disabled
----
====

[role="_additional-resources"]
.Additional resources
* link:https://cloudinit.readthedocs.io/en/latest/topics/network-config.html[Network Configuration]
* link:https://cloudinit.readthedocs.io/en/latest/reference/datasources/nocloud.html[NoCloud]
