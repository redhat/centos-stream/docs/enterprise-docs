:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
// enterprise/assemblies/assembly_introduction-to-cloud-init.adoc
// <List assemblies here, each on a new line>

[id="cloud-init-operates-in-stages_{context}"]
= cloud-init operates in stages

//Stages of the cloud-init utility
// This is a bit of a wonky title -> update?

During system boot, the `cloud-init` utility operates in five stages that determine whether `cloud-init` runs and where it finds its datasources, among other tasks. The stages are as follows:

. *Generator stage*: By using the `systemd` service, this phase determines whether to run `cloud-init` utility at the time of boot. 

. *Local stage*: `cloud-init` searches local datasources and applies network configuration, including the DHCP-based fallback mechanism.

. *Network stage*: `cloud-init` processes user data by running modules listed under `cloud_init_modules` in the `/etc/cloud/cloud.cfg` file. You can add, remove, enable, or disable modules in the `cloud_init_modules` section.

. *Config stage*: `cloud-init` runs modules listed under `cloud_config_modules` section in the `/etc/cloud/cloud.cfg` file. You can add, remove, enable, or disable modules in the `cloud_config_modules` section.

. *Final stage*: `cloud-init` runs modules and configurations included in the `cloud_final_modules` section of the `/etc/cloud/cloud.cfg` file. It can include the installation of specific packages, as well as triggering configuration management plug-ins and user-defined scripts. You can add, remove, enable, or disable modules in the `cloud_final_modules` section.

[role="_additional-resources"]
.Additional resources
*  link:https://cloudinit.readthedocs.io/en/latest/topics/boot.html#boot-stages[Boot Stages of cloud-init]
