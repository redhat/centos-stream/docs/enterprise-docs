:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// enterprise/titles/configuring-and-maintaining/configuring-and-managing-cloud-init-for-rhel-8/assemblies/assembly_configuring-cloud-init.adoc
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="changing-a-default-user-name-with-cloud-init_{context}"]
= Changing a default user name with cloud-init

You can change the default user name to something other than `cloud-user`.

.Procedure

include::reuse/include_directives.adoc[tag=where-to-add-directives-user-data]

+
. Add the line `user: <username>`, replacing <username> with the new default user name:
+
[options="nowrap"]
----
#cloud-config
user: username
password: mypassword
chpasswd: {expire: False}
ssh_pwauth: True
ssh_authorized_keys:
  - ssh-rsa AAA...SDvz user1@yourdomain.com
  - ssh-rsa AAB...QTuo user2@yourdomain.com
----
