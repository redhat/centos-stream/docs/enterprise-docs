// included in assembly tuning-cpu-frequency-to-optimize-energy-consumption
:_mod-docs-content-type: CONCEPT

[id="intel-p-state-cpufreq-governors_{context}"]
= Intel P-state CPUfreq governors

[role="_abstract"]
By default, the Intel P-state driver operates in active mode with or without Hardware p-state (HWP) depending on whether the CPU supports HWP.

Using the `cpupower frequency-info --governor` command as root, you can view the available CPUfreq governors.

[NOTE]
====
The functionality of `performance` and `powersave` Intel P-state CPUfreq governors is different compared to core CPUfreq governors of the same names.
====

The Intel P-state driver can operate in the following three different modes:

`Active mode with hardware-managed P-states`::
When active mode with HWP is used, the Intel P-state driver instructs the CPU to perform the P-state selection. The driver can provide frequency hints. However, the final selection depends on CPU internal logic.
In active mode with HWP, the Intel P-state driver provides two P-state selection
algorithms:

* `performance`: With the `performance` governor, the driver instructs internal CPU logic to be performance-oriented. The range of allowed P-states is restricted to the upper boundary of the range that the driver is allowed to use.

* `powersave`: With the `powersave` governor, the driver instructs internal CPU logic to be powersave-oriented.

`Active mode without hardware-managed P-states`::
When active mode without HWP is used, the Intel P-state driver provides two P-state selection algorithms:

* `performance`: With the `performance` governor, the driver chooses the maximum P-state it is allowed to use.

* `powersave`: With the `powersave` governor, the driver chooses P-states proportional to the current CPU utilization. The behavior is similar to the `ondemand` CPUfreq core governor.

`Passive mode`::
When the `passive` mode is used, the Intel P-state driver functions the same as the traditional CPUfreq scaling driver. All available generic CPUFreq core governors can be used.
