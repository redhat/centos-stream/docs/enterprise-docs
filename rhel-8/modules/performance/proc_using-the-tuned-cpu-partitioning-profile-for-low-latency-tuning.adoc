:_module-type: PROCEDURE

[id="using-the-tuned-cpu-partitioning-profile-for-low-latency-tuning_{context}"]
= Using the TuneD cpu-partitioning profile for low-latency tuning

[role="_abstract"]
This procedure describes how to tune a system for low-latency using the TuneD’s `cpu-partitioning` profile. It uses the example of a low-latency application that can use `cpu-partitioning` and the CPU layout as mentioned in the xref:cpu-partitioning_{context}[cpu-partitioning] figure.

The application in this case uses:

* One dedicated reader thread that reads data from the network will be pinned to CPU 2.
* A large number of threads that process this network data will be pinned to CPUs 4-23.
* A dedicated writer thread that writes the processed data to the network will be pinned to CPU 3.


.Prerequisites

* You have installed the `cpu-partitioning` TuneD profile by using the `{PackageManagerCommand} install tuned-profiles-cpu-partitioning` command as root.


.Procedure

. Edit `/etc/tuned/cpu-partitioning-variables.conf` file and add the following information:
+
----
# All isolated CPUs:
isolated_cores=2-23
# Isolated CPUs without the kernel’s scheduler load balancing:
no_balance_cores=2,3
----

. Set the `cpu-partitioning` TuneD profile:
+
-----
# tuned-adm profile cpu-partitioning
-----

. Reboot
+
After rebooting, the system is tuned for low-latency, according to the isolation in the cpu-partitioning figure. The application can use taskset to pin the reader and writer threads to CPUs 2 and 3, and the remaining application threads on CPUs 4-23.


[role="_additional-resources"]
.Additional resources
* `tuned-profiles-cpu-partitioning(7)` man page on your system
