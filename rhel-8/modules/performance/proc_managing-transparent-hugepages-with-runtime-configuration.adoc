:_newdoc-version: 2.18.3
:_template-generated: 2025-02-05
:_mod-docs-content-type: PROCEDURE

[id="managing-transparent-hugepages-with-runtime-configuration_{context}"]
= Managing transparent hugepages with runtime configuration

Transparent hugepages (THP) can be managed at runtime to optimize memory usage. The runtime configuration is not persistent across system reboots.

.Procedure

. Check the status of THP:
+
[subs="+quotes,verbatim,normal"]
----
$ *cat /sys/kernel/mm/transparent_hugepage/enabled* 
----

. Configure THP.
** Enabling THP:
+
[subs="+quotes,verbatim,normal"]
----
$ *echo always > /sys/kernel/mm/transparent_hugepage/enabled*
----

** Disabling THP:
+
[subs="+quotes,verbatim,normal"]
----
$ *echo never > /sys/kernel/mm/transparent_hugepage/enabled*
----

** Setting THP to `madvise`:
+
[subs="+quotes,verbatim,normal"]
----
$ *echo madvise > /sys/kernel/mm/transparent_hugepage/enabled*
----
+
To prevent applications from allocating more memory resources than necessary, disable the system-wide transparent hugepages and only enable them for the applications that explicitly request it through the `madvise` system call.
+
[NOTE]
====
Sometimes, providing low latency to short-lived allocations has higher priority than immediately achieving the best performance with long-lived allocations. In such cases, you can disable direct compaction while leaving THP enabled.

Direct compaction is a synchronous memory compaction during the huge page allocation. Disabling direct compaction provides no guarantee of saving memory, but can decrease the risk of higher latencies during frequent page faults. Also, disabling direct compaction allows synchronous compaction of Virtual Memory Areas (VMAs) highlighted in `madvise` only. Note that if the workload benefits significantly from THP, the performance decreases. Disable direct compaction:

$ echo never > /sys/kernel/mm/transparent_hugepage/defrag
====

[role="_additional-resources"]
.Additional resources

* `madvise(2)` man page on your system.
