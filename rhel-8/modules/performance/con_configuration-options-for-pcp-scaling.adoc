// Module included in the following assemblies:
//setting-up-pcp
// <List assemblies here, each on a new line
:_mod-docs-content-type: CONCEPT
[id="configuration-options-for-pcp-scaling_{context}"]
= Configuration options for PCP scaling

[role="_abstract"]
The following are the configuration options, which are required for scaling:

`sysctl and rlimit settings`::
When archive discovery is enabled, `pmproxy` requires four descriptors for every `pmlogger` that it is monitoring or log-tailing, along with the additional file descriptors for the service logs and `pmproxy` client sockets, if any.
Each `pmlogger` process uses about 20 file descriptors for the remote `pmcd` socket, archive files, service logs, and others. In total, this can exceed the default 1024 soft limit on a system running around 200 `pmlogger` processes. The `pmproxy` service in `pcp-5.3.0` and later automatically increases the soft limit to the hard limit. On earlier versions of PCP, tuning is required if a high number of `pmlogger` processes are to be deployed, and this can be accomplished by increasing the soft or hard limits for `pmlogger`. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/1346533[How to set limits (ulimit) for services run by systemd].

`Local Archives`::
The `pmlogger` service stores metrics of local and remote `pmcds` in the `/var/log/pcp/pmlogger/` directory. To control the logging interval of the local system, update the `/etc/pcp/pmlogger/control.d/_configfile_` file and add `-t _X_` in the arguments, where _X_ is the logging interval in seconds.
To configure which metrics should be logged, execute `pmlogconf /var/lib/pcp/config/pmlogger/config._clienthostname_`.  This command deploys a configuration file with a default set of metrics, which can optionally be further customized.
To specify retention settings, that is when to purge old PCP archives, update the `/etc/sysconfig/pmlogger_timers` file and specify `PMLOGGER_DAILY_PARAMS="-E -k _X_"`, where _X_ is the amount of days to keep PCP archives.

`Redis`::
The `pmproxy` service sends logged metrics from `pmlogger` to a Redis instance. The following are the available two options to specify the retention settings in the `/etc/pcp/pmproxy/pmproxy.conf` configuration file:
* `stream.expire` specifies the duration when stale metrics should be removed, that is metrics which were not updated in a specified amount of time in seconds.
* `stream.maxlen` specifies the maximum number of metric values for one metric per host. This setting should be the retention time divided by the logging interval, for example 20160 for 14 days of retention and 60s logging interval (60*60*24*14/60)


[role="_additional-resources"]
.Additional resources
* `pmproxy(1)`, `pmlogger(1)`, and `sysctl(8)` man pages on your system
