// Module included in the following assemblies:
//setting-up-pcp
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="deploying-a-minimal-pcp-setup_{context}"]
= Deploying a minimal PCP setup

[role="_abstract"]
The minimal PCP setup collects performance statistics on {RHEL}. The setup involves adding the minimum number of packages on a production system needed to gather data for further analysis.

You can analyze the resulting `tar.gz` file and the archive of the `pmlogger` output using various PCP tools and compare them with other sources of performance information.

.Prerequisites


* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].

.Procedure

. Update the `pmlogger` configuration:
+
----
# pmlogconf -r /var/lib/pcp/config/pmlogger/config.default
----

. Start the `pmcd` and `pmlogger` services:
+
----
# systemctl start pmcd.service

# systemctl start pmlogger.service
----

. Execute the required operations to record the performance data.

. Stop the `pmcd` and `pmlogger` services:
+
----
# systemctl stop pmcd.service

# systemctl stop pmlogger.service
----

. Save the output and save it to a `tar.gz` file named based on the host name and the current date and time:
+
[subs=quotes]
----
# cd /var/log/pcp/pmlogger/

# tar -czf $(hostname).$(date +%F-%Hh%M).pcp.tar.gz $(hostname)
----
+
Extract this file and analyze the data using PCP tools.


[role="_additional-resources"]
.Additional resources
* `pmlogconf(1)`, `pmlogger(1)`, and `pmcd(1)` man pages on your system

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#system-services-distributed-with-pcp_setting-up-pcp[System services and tools distributed with PCP]
