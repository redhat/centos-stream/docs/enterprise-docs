// Module included in the following assemblies:
//logging-performance-data-with-pmlogger
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="modifying-the-pmlogger-configuration-file-with-pmlogconf_{context}"]
= Modifying the pmlogger configuration file with pmlogconf

[role="_abstract"]
When the `pmlogger` service is running, PCP logs a default set of metrics on the host.

Use the `pmlogconf` utility to check the default configuration. If the `pmlogger` configuration file does not exist, `pmlogconf` creates it with a default metric values.

.Prerequisites


* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].

.Procedure

. Create or modify the `pmlogger` configuration file:
+
----
# pmlogconf -r /var/lib/pcp/config/pmlogger/config.default
----

. Follow `pmlogconf` prompts to enable or disable groups of related performance metrics and to control the logging interval for each enabled group.

[role="_additional-resources"]
.Additional resources
* `pmlogconf(1)` and `pmlogger(1)` man pages on your system

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#system-services-distributed-with-pcp_setting-up-pcp[System services and tools distributed with PCP]
