// Module included in the following assemblies:
//setting-up-graphical-representation-of-pcp-metrics
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="installing-pcp-vector_{context}"]
= Installing PCP Vector

[role="_abstract"]
Install a `pcp vector` data source to show live, on-host metrics from the real-time `pmwebapi` interfaces. This data source is intended for on-demand performance monitoring of an individual host and includes container support.

.Prerequisites

. PCP is configured. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#setting-up-pcp-with-pcp-zeroconf_setting-up-graphical-representation-of-pcp-metrics[Setting up PCP with pcp-zeroconf].

. The `grafana-server` is configured. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#setting-up-a-grafana-server_setting-up-graphical-representation-of-pcp-metrics[Setting up a grafana-server].

.Procedure

. Install the `pcp-pmda-bcc` package:
+
[literal,subs="+quotes,attributes"]
....
# {PackageManagerCommand} install pcp-pmda-bcc
....

. Install the `bcc` PMDA:
+
----
# cd /var/lib/pcp/pmdas/bcc
# ./Install
[Wed Apr  1 00:27:48] pmdabcc(22341) Info: Initializing, currently in 'notready' state.
[Wed Apr  1 00:27:48] pmdabcc(22341) Info: Enabled modules:
[Wed Apr  1 00:27:48] pmdabcc(22341) Info: ['biolatency', 'sysfork',
[...]
Updating the Performance Metrics Name Space (PMNS) ...
Terminate PMDA if already installed ...
Updating the PMCD control file, and notifying PMCD ...
Check bcc metrics have appeared ... 1 warnings, 1 metrics and 0 values
----


[role="_additional-resources"]
.Additional resources
* `pmdabcc(1)` man page on your system
