// Module included in the following assemblies:
//logging-performance-data-with-pmlogger
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="setting-up-the-central-server-to-collect-data_{context}"]
= Setting up a central server to collect data

[role="_abstract"]
This procedure describes how to create a central server to collect metrics from clients running PCP.

.Prerequisites


* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].
* Client is configured for metrics collection. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/logging-performance-data-with-pmlogger_monitoring-and-managing-system-status-and-performance#setting-up-a-client-system-for-metrics-collection_logging-performance-data-with-pmlogger[Setting up a client system for metrics collection].


.Procedure

. Install the `pcp-system-tools` package:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install pcp-system-tools
----
+
. Create the `/etc/pcp/pmlogger/control.d/remote` file with the following content:
+
ifeval::[{ProductNumber} == 8]
[literal,subs="quotes"]
....
# DO NOT REMOVE OR EDIT THE FOLLOWING LINE
$version=1.1

_192.168.4.13_ n n PCP_ARCHIVE_DIR/rhel7u4a -r -T24h10m -c config.rhel7u4a
_192.168.4.14_ n n PCP_ARCHIVE_DIR/rhel6u10a -r -T24h10m -c config.rhel6u10a
_192.168.4.62_ n n PCP_ARCHIVE_DIR/rhel8u1a -r -T24h10m -c config.rhel8u1a
....
+
Replace _192.168.4.13_, _192.168.4.14_ and _192.168.4.62_  with the client IP addresses.
+
NOTE: In {RH} Enterpirse Linux 8.0, 8.1 and 8.2 use the following format for remote hosts in the control file: PCP_LOG_DIR/pmlogger/__host_name__.
endif::[]
+
ifeval::[{ProductNumber} == 9]
[literal,subs="quotes"]
....
# DO NOT REMOVE OR EDIT THE FOLLOWING LINE
$version=1.1

_192.168.4.13_ n n PCP_ARCHIVE_DIR/rhel7u4a -r -T24h10m -c config.rhel7u4a
_192.168.4.14_ n n PCP_ARCHIVE_DIR/rhel6u10a -r -T24h10m -c config.rhel6u10a
_192.168.4.62_ n n PCP_ARCHIVE_DIR/rhel8u1a -r -T24h10m -c config.rhel8u1a
_192.168.4.69_ n n PCP_ARCHIVE_DIR/rhel9u3a -r -T24h10m -c config.rhel9u3a
....
+
Replace _192.168.4.13_, _192.168.4.14_, _192.168.4.62_ and _192.168.4.69_  with the client IP addresses.
endif::[]


. Enable the `pmcd` and `pmlogger` services:
+
----
# systemctl enable pmcd pmlogger
# systemctl restart pmcd pmlogger
----

.Verification

* Ensure that you can access the latest archive file from each directory:
+
----
# for i in /var/log/pcp/pmlogger/rhel*/*.0; do pmdumplog -L $i; done
Log Label (Log Format Version 2)
Performance metrics from host rhel6u10a.local
  commencing Mon Nov 25 21:55:04.851 2019
  ending     Mon Nov 25 22:06:04.874 2019
Archive timezone: JST-9
PID for pmlogger: 24002
Log Label (Log Format Version 2)
Performance metrics from host rhel7u4a
  commencing Tue Nov 26 06:49:24.954 2019
  ending     Tue Nov 26 07:06:24.979 2019
Archive timezone: CET-1
PID for pmlogger: 10941
[..]
----
+
The archive files from the `/var/log/pcp/pmlogger/` directory can be used for further analysis and graphing.

[role="_additional-resources"]
.Additional resources
* `pmlogger(1)` man page on your system

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#system-services-distributed-with-pcp_setting-up-pcp[System services and tools distributed with PCP]

* `/var/lib/pcp/config/pmlogger/config.default` file
