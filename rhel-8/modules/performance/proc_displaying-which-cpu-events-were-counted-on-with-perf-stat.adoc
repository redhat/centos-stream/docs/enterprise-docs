:_mod-docs-content-type: PROCEDURE
[id="displaying-which-cpu-events-were-counted-on-with-perf-stat_{context}"]
= Displaying which CPU events were counted on with perf stat

[role="_abstract"]
You can use `perf stat` to display which CPU events were counted on by disabling CPU count aggregation. You must count events in system-wide mode by using the `-a` flag in order to use this functionality.

.Prerequisites

* You have the `perf` user space tool installed as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-perf_monitoring-and-managing-system-status-and-performance#installing-perf_getting-started-with-perf[Installing perf].


.Procedure

* Count the events with CPU count aggregation disabled:
+
[subs=+quotes]
----
# perf stat -a -A sleep _seconds_
----
+
The previous example displays counts of a default set of common hardware and software events recorded over a time period of `_seconds_` seconds, as dictated by using the `sleep` command, over each individual CPU in ascending order, starting with `CPU0`. As such, it may be useful to specify an event such as cycles:
+
[subs=+quotes]
----
# perf stat -a -A -e cycles sleep _seconds_
----
