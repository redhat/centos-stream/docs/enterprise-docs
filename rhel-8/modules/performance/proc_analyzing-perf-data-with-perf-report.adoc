:_mod-docs-content-type: PROCEDURE
[id="analyzing-perf-data-with-perf-report_{context}"]
= Analyzing perf.data with perf report

[role="_abstract"]
You can use `perf report` to display and analyze a [file]`perf.data` file.

.Prerequisites

* You have the `perf` user space tool installed as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-perf_monitoring-and-managing-system-status-and-performance#installing-perf_getting-started-with-perf[Installing perf].

* There is a [file]`perf.data` file in the current directory.
* If the [file]`perf.data` file was created with root access, you need to run `perf report` with root access too.

.Procedure

* Display the contents of the [file]`perf.data` file for further analysis:
+
----
# perf report
----
+
This command displays output similar to the following:
+
[subs=+quotes]
----
Samples: 2K of event 'cycles', Event count (approx.): 235462960
Overhead  Command          Shared Object                     Symbol
   2.36%  kswapd0          [kernel.kallsyms]                 [k] page_vma_mapped_walk
   2.13%  sssd_kcm         libc-2.28.so                      [.] __memset_avx2_erms
   2.13%  perf             [kernel.kallsyms]                 [k] smp_call_function_single
   1.53%  gnome-shell      libc-2.28.so                      [.] __strcmp_avx2
   1.17%  gnome-shell      libglib-2.0.so.0.5600.4           [.] g_hash_table_lookup
   0.93%  Xorg             libc-2.28.so                      [.] __memmove_avx_unaligned_erms
   0.89%  gnome-shell      libgobject-2.0.so.0.5600.4        [.] g_object_unref
   0.87%  kswapd0          [kernel.kallsyms]                 [k] page_referenced_one
   0.86%  gnome-shell      libc-2.28.so                      [.] __memmove_avx_unaligned_erms
   0.83%  Xorg             [kernel.kallsyms]                 [k] alloc_vmap_area
   0.63%  gnome-shell      libglib-2.0.so.0.5600.4           [.] g_slice_alloc
   0.53%  gnome-shell      libgirepository-1.0.so.1.0.0      [.] g_base_info_unref
   0.53%  gnome-shell      ld-2.28.so                        [.] _dl_find_dso_for_object
   0.49%  kswapd0          [kernel.kallsyms]                 [k] vma_interval_tree_iter_next
   0.48%  gnome-shell      libpthread-2.28.so                [.] __pthread_getspecific
   0.47%  gnome-shell      libgirepository-1.0.so.1.0.0      [.] 0x0000000000013b1d
   0.45%  gnome-shell      libglib-2.0.so.0.5600.4           [.] g_slice_free1
   0.45%  gnome-shell      libgobject-2.0.so.0.5600.4        [.] g_type_check_instance_is_fundamentally_a
   0.44%  gnome-shell      libc-2.28.so                      [.] malloc
   0.41%  swapper          [kernel.kallsyms]                 [k] apic_timer_interrupt
   0.40%  gnome-shell      ld-2.28.so                        [.] _dl_lookup_symbol_x
   0.39%  kswapd0          [kernel.kallsyms]                 [k] __raw_callee_save___pv_queued_spin_unlock
----

[role="_additional-resources"]
.Additional resources
* `perf-report(1)` man page on your system
