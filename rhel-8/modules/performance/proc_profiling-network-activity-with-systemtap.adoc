:_mod-docs-content-type: PROCEDURE

[id="profiling-network-activity-with-systemtap_{context}"]
= Profiling network activity with SystemTap

[role="_abstract"]
You can use the `nettop.stp` example SystemTap script to profile network activity. The script tracks which processes are generating network traffic on the system, and provides the following information about each process:

PID ::
The ID of the listed process.
UID ::
User ID. A user ID of 0 refers to the root user.
DEV ::
Which ethernet device the process used to send or receive data (for example, eth0, eth1).
XMIT_PK ::
The number of packets transmitted by the process.
RECV_PK ::
The number of packets received by the process.
XMIT_KB ::
The amount of data sent by the process, in kilobytes.
RECV_KB ::
The amount of data received by the service, in kilobytes.

.Prerequisites

* You have installed SystemTap as described in xref:installing-systemtap_getting-started-with-systemtap[Installing SystemTap].


.Procedure

* Run the `nettop.stp` script:
+
[subs=+quotes]
----
# stap  --example nettop.stp
----
+
The `nettop.stp` script  provides network profile sampling every 5 seconds.
+
Output of the `nettop.stp` script looks similar to the following:
+
[literal,subs="+quotes,attributes"]
----
[...]
  PID   UID DEV     XMIT_PK RECV_PK XMIT_KB RECV_KB COMMAND
    0     0 eth0          0       5       0       0 swapper
11178     0 eth0          2       0       0       0 synergyc
  PID   UID DEV     XMIT_PK RECV_PK XMIT_KB RECV_KB COMMAND
 2886     4 eth0         79       0       5       0 cups-polld
11362     0 eth0          0      61       0       5 firefox
    0     0 eth0          3      32       0       3 swapper
 2886     4 lo            4       4       0       0 cups-polld
11178     0 eth0          3       0       0       0 synergyc
  PID   UID DEV     XMIT_PK RECV_PK XMIT_KB RECV_KB COMMAND
    0     0 eth0          0       6       0       0 swapper
 2886     4 lo            2       2       0       0 cups-polld
11178     0 eth0          3       0       0       0 synergyc
 3611     0 eth0          0       1       0       0 Xorg
  PID   UID DEV     XMIT_PK RECV_PK XMIT_KB RECV_KB COMMAND
    0     0 eth0          3      42       0       2 swapper
11178     0 eth0         43       1       3       0 synergyc
11362     0 eth0          0       7       0       0 firefox
 3897     0 eth0          0       1       0       0 multiload-apple
----
