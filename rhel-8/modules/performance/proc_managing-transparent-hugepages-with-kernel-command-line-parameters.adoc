:_newdoc-version: 2.18.3
:_template-generated: 2025-02-05
:_mod-docs-content-type: PROCEDURE

[id="managing-transparent-hugepages-with-kernel-command-line-parameters_{context}"]
= Managing transparent hugepages with kernel command line parameters

You can manage transparent hugepages (THP) at boot time by modifying kernel parameters. This configuration is persistent across system reboots.

.Prerequisite

* You have root permissions on the system.

.Procedure

. Get the current kernel command line parameters:
+
ifeval::[{ProductNumber} == 9]
[subs="+quotes,verbatim,normal"]
----
# *grubby --info=$(grubby --default-kernel)*
kernel="/boot/vmlinuz-5.14.0-284.11.1.el9_2.x86_64"
args="ro crashkernel=1G-4G:192M,4G-64G:256M,64G-:512M resume=UUID=_XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXXX_ console=tty0 console=ttyS0"
root="UUID=_XXXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX_"
initrd="/boot/initramfs-5.14.0-284.11.1.el9_2.x86_64.img"
title="Red Hat Enterprise Linux (5.14.0-284.11.1.el9_2.x86_64) 9.2 (Plow)"
id="_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX_-5.14.0-284.11.1.el9_2.x86_64"
----
endif::[]
ifeval::[{ProductNumber} == 8]
[subs="+quotes,verbatim,normal"]
----
# *grubby --info=$(grubby --default-kernel)*
kernel="/boot/vmlinuz-4.18.0-553.el8_10.x86_64"
args="ro crashkernel=1G-4G:192M,4G-64G:256M,64G-:512M resume=UUID=_XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXXX_ console=tty0 console=ttyS0"
root="UUID=_XXXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX_"
initrd="/boot/initramfs-4.18.0-553.el8_10.x86_64.img"
title="Red Hat Enterprise Linux (4.18.0-553.el8_10.x86_64) 8.10 (Ootpa)"
id="_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX_-4.18.0-553.el8_10.x86_64"
----
endif::[]

. Configure THP by adding kernel parameters.
** To enable THP:
+
[subs="+quotes,verbatim,normal"]
----
# *grubby --args="transparent_hugepage=always" --update-kernel=DEFAULT*
----

** To disable THP:
+
[subs="+quotes,verbatim,normal"]
----
# *grubby --args="transparent_hugepage=never" --update-kernel=DEFAULT*
----

** To set THP to `madvise`:
+
[subs="+quotes,verbatim,normal"]
----
# *grubby --args="transparent_hugepage=madvise" --update-kernel=DEFAULT*
----

. Reboot the system for changes to take effect:
+
[subs="+quotes,verbatim,normal"]
----
# *reboot*
----

.Verification
* To verify the status of THP, view the following files:
+
[subs="+quotes,verbatim,normal"]
----
# *cat /sys/kernel/mm/transparent_hugepage/enabled*
always madvise [never]
----
+
[subs="+quotes,verbatim,normal"]
----
# *grep AnonHugePages: /proc/meminfo*
AnonHugePages:         0 kB
----
+
[subs="+quotes,verbatim,normal"]
----
# *grep nr_anon_transparent_hugepages /proc/vmstat*
nr_anon_transparent_hugepages 0
----