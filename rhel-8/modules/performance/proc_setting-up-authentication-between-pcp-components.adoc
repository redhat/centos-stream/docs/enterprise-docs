// Module included in the following assemblies:
//setting-up-graphical-representation-of-pcp-metrics
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="setting-up-authentication-between-pcp-components_{context}"]
= Setting up authentication between PCP components

[role="_abstract"]
You can setup authentication using the `scram-sha-256` authentication mechanism, which is supported by PCP through the Simple Authentication Security Layer (SASL) framework.

ifeval::[{ProductNumber} == 8]
[NOTE]
====
From {RHEL} 8.3, PCP supports the `scram-sha-256` authentication mechanism.
====
endif::[]

.Procedure

. Install the `sasl` framework for the `scram-sha-256` authentication mechanism:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install cyrus-sasl-scram cyrus-sasl-lib
----

. Specify the supported authentication mechanism and the user database path in the `pmcd.conf` file:
+
----
# vi /etc/sasl2/pmcd.conf

mech_list: scram-sha-256

sasldb_path: /etc/pcp/passwd.db
----

. Create a new user:
+
[subs=quotes]
----
# useradd -r _metrics_
----
+
Replace _metrics_ by your user name.

. Add the created user in the user database:
+
[subs=quotes]
----
# saslpasswd2 -a pmcd _metrics_

Password:
Again (for verification):
----
+
To add the created user, you are required to enter the _metrics_ account password.

. Set the permissions of the user database:
+
----
# chown root:pcp /etc/pcp/passwd.db
# chmod 640 /etc/pcp/passwd.db
----

. Restart the `pmcd` service:
+
----
# systemctl restart pmcd
----

.Verification

* Verify the `sasl` configuration:
+
[literal,subs="quotes,attributes"]
....
# pminfo -f -h "pcp://127.0.0.1?username=_metrics_" disk.dev.read
Password:
disk.dev.read
inst [0 or "sda"] value 19540
....

[role="_additional-resources"]
.Additional resources
* `saslauthd(8)`, `pminfo(1)`, and `sha256` man pages on your system
* link:https://access.redhat.com/solutions/5041891[How can I setup authentication between PCP components, like PMDAs and pmcd in RHEL 8.2?] (Red Hat Knowledgebase)
