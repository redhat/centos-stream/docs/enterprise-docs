// Module included in the following assemblies:
//configuring-huge-pages
// <List assemblies here, each on a new line>
:_mod-docs-content-type: CONCEPT
[id="available-hugepage-features_{context}"]
= Available huge page features

[role="_abstract"]
With {RHEL} {ProductNumber}, you can use huge pages for applications that work with big data sets, and improve the performance of such applications.

The following are the huge page methods, which are supported in {ProductShortName} {ProductNumber}:

`HugeTLB pages`::
HugeTLB pages are also called static huge pages. There are two ways of reserving HugeTLB pages:

* At boot time: It increases the possibility of success because the memory has not yet been significantly fragmented. However, on NUMA machines, the number of pages is automatically split among the NUMA nodes.

For more information about parameters that influence HugeTLB page behavior at boot time, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/configuring-huge-pages_monitoring-and-managing-system-status-and-performance#parameters-for-reserving-hugetlb-pages-at-boot-time_configuring-huge-pages[Parameters for reserving HugeTLB pages at boot time] and how to use these parameters to configure HugeTLB pages at boot time, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/configuring-huge-pages_monitoring-and-managing-system-status-and-performance#configuring-hugetlb-at-boot-time_configuring-huge-pages[Configuring HugeTLB at boot time].

* At run time: It allows you to reserve the huge pages per NUMA node. If the run-time reservation is done as early as possible in the boot process, the probability of memory fragmentation is lower.

For more information about parameters that influence HugeTLB page behavior at run time, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/monitoring_and_managing_system_status_and_performance/configuring-huge-pages_monitoring-and-managing-system-status-and-performance#parameters-for-reserving-hugetlb-pages-at-run-time_configuring-huge-pages[Parameters for reserving HugeTLB pages at run time] and how to use these parameters to configure HugeTLB pages at run time, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/monitoring_and_managing_system_status_and_performance/configuring-huge-pages_monitoring-and-managing-system-status-and-performance#configuring-hugetlb-at-run-time_configuring-huge-pages[Configuring HugeTLB at run time].

`Transparent HugePages (THP)`::
With THP, the kernel automatically assigns huge pages to processes, and therefore there is no need to manually reserve the static huge pages. The following are the two modes of operation in THP:

* `system-wide`: Here, the kernel tries to assign huge pages to a process whenever it is possible to allocate the huge pages and the process is using a large contiguous virtual memory area.

* `per-process`: Here, the kernel only assigns huge pages to the memory areas of individual processes which you can specify using the `madvise`() system call.
+
[NOTE]
====
The THP feature only supports `2 MB` pages.
====
+


For more information about parameters that influence HugeTLB page behavior at boot time, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/configuring-huge-pages_monitoring-and-managing-system-status-and-performance#enabling-transparent-hugepages_configuring-huge-pages[Enabling transparent hugepages] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/configuring-huge-pages_monitoring-and-managing-system-status-and-performance#disabling-transparent-hugepages_configuring-huge-pages[Disabling transparent hugepages].
