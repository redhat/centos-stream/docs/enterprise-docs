// Module included in the following assemblies:
//monitoring-performance-with-performance-co-pilot
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="visually-tracing-pcp-log-archives-with-pcp-charts_{context}"]
= Visually tracing PCP log archives with the PCP Charts application

[role="_abstract"]
After recording metric data, you can replay the PCP log archives as graphs. The metrics are sourced from one or more live hosts with alternative options to use metric data from PCP log archives as a source of historical data. To customize the *PCP Charts* application interface to display the data from the performance metrics, you can use line plot, bar graphs, or utilization graphs.


Using the *PCP Charts* application, you can:

* Replay the data in the *PCP Charts* application application and use graphs to visualize the retrospective data alongside live data of the system.
* Plot performance metric values into graphs.
* Display multiple charts simultaneously.


.Prerequisites


* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].
* Logged performance data with the `pmlogger`. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/logging-performance-data-with-pmlogger_monitoring-and-managing-system-status-and-performance[Logging performance data with pmlogger].


* Install the `pcp-gui` package:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install pcp-gui
----

.Procedure

. Launch the *PCP Charts* application from the command line:
+
----
# pmchart
----
+
.PCP Charts application
image::pmchart_started.png[]
+
The `pmtime` server settings are located at the bottom. The *start* and *pause* button allows you to control:

* The interval in which PCP polls the metric data
* The date and time for the metrics of historical data

. Click *File* and then *New Chart* to select metric from both the local machine and remote machines by specifying their host name or address. Advanced configuration options include the ability to manually set the axis values for the chart, and to manually choose the color of the plots.

. Record the views created in the *PCP Charts* application:
+
Following are the options to take images or record the views created in the *PCP Charts* application:

* Click *File* and then *Export* to save an image of the current view.
* Click *Record* and then *Start* to start a recording. Click *Record* and then *Stop* to stop the recording. After stopping the recording, the recorded metrics are archived to be viewed later.

. Optional: In the *PCP Charts* application, the main configuration file, known as the *view*, allows the metadata associated with one or more charts to be saved. This metadata describes all chart aspects, including the metrics used and the chart columns. Save the custom *view* configuration by clicking *File* and then *Save View*, and load the *view* configuration later.
+
The following example of the *PCP Charts* application view configuration file describes a stacking chart graph showing the total number of bytes read and written to the given XFS file system `loop1`:
+
----
#kmchart
version 1

chart title "Filesystem Throughput /loop1" style stacking antialiasing off
    plot legend "Read rate"   metric xfs.read_bytes   instance  "loop1"
    plot legend "Write rate"  metric xfs.write_bytes  instance  "loop1"
----

[role="_additional-resources"]
.Additional resources
* `pmchart(1)` and `pmtime(1)` man pages on your system

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#system-services-distributed-with-pcp_setting-up-pcp[System services and tools distributed with PCP]
