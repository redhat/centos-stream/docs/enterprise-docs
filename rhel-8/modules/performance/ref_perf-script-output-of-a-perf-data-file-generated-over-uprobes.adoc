:_mod-docs-content-type: REFERENCE
[id="ref_perf-script-output-of-a-perf-data-file-generated-over-uprobes_{context}"]
= Perf script output of data recorded over uprobes

[role="_abstract"]
A common method to analyze data collected using uprobes is using the `perf script` command to read a `perf.data` file and display a detailed trace of the recorded workload.

In the perf script example output:

* A uprobe is added to the function *isprime()* in a program called *my_prog*
* *a* is a function argument added to the uprobe. Alternatively, *a* could be an arbitrary variable visible in the code scope of where you add your uprobe:
[literal,subs="+quotes,attributes"]
----
# perf script
    my_prog  1367 [007] 10802159.906593: probe_my_prog:isprime: (400551) a=2
    my_prog  1367 [007] 10802159.906623: probe_my_prog:isprime: (400551) a=3
    my_prog  1367 [007] 10802159.906625: probe_my_prog:isprime: (400551) a=4
    my_prog  1367 [007] 10802159.906627: probe_my_prog:isprime: (400551) a=5
    my_prog  1367 [007] 10802159.906629: probe_my_prog:isprime: (400551) a=6
    my_prog  1367 [007] 10802159.906631: probe_my_prog:isprime: (400551) a=7
    my_prog  1367 [007] 10802159.906633: probe_my_prog:isprime: (400551) a=13
    my_prog  1367 [007] 10802159.906635: probe_my_prog:isprime: (400551) a=17
    my_prog  1367 [007] 10802159.906637: probe_my_prog:isprime: (400551) a=19
----
