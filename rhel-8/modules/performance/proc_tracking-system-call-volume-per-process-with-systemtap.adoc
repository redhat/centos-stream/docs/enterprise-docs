:_mod-docs-content-type: PROCEDURE

[id="proc_tracking-system-call-volume-per-process-with-systemtap_{context}"]
= Tracking system call volume per process with SystemTap


[role="_abstract"]
You can use the *syscalls_by_proc.stp* SystemTap script to see which processes are performing the highest volume of system calls.
It displays 20 processes performing the most of system calls.

.Prerequisites

* You have installed SystemTap as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-systemtap_monitoring-and-managing-system-status-and-performance#installing-systemtap_getting-started-with-systemtap[Installing Systemtap].


.Procedure
* Run the *syscalls_by_proc.stp* script:
+
----
# stap --example syscalls_by_proc.stp
----
+
Output of the *syscalls_by_proc.stp* script looks similar to the following:
+
----
Collecting data... Type Ctrl-C to exit and display results
#SysCalls  Process Name
1577       multiload-apple
692        synergyc
408        pcscd
376        mixer_applet2
299        gnome-terminal
293        Xorg
206        scim-panel-gtk
95         gnome-power-man
90         artsd
85         dhcdbd
84         scim-bridge
78         gnome-screensav
66         scim-launcher
[...]
----
