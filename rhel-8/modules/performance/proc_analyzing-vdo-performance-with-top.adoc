:_newdoc-version: 2.15.0
:_template-generated: 2023-11-1
:_mod-docs-content-type: PROCEDURE

[id="analyzing-vdo-performance-with-top_{context}"]
= Analyzing VDO performance with top

You can examine the performance of VDO threads by using the `top` utility.

[NOTE]
====
Tools such as `top` cannot differentiate between productive CPU cycles and cycles stalled due to cache or memory delays. These tools interpret cache contention and slow memory access as actual work. Moving threads between nodes can appear like reduced CPU utilization while increasing operations per second.
====

.Procedure

. Display the individual threads:
+
[subs="+quotes"]
----
$ *top -H*
----

. Press the kbd:[f] key to display the fields manager.

. Use the kbd:[(↓)] key to navigate to the `P = Last Used Cpu (SMP)` field.

. Press the spacebar to select the `P = Last Used Cpu (SMP)` field.

. Press the kbd:[q] key to close the fields manager. The `top` utility now displays the CPU load for individual cores and indicates which CPU each process or thread recently used. You can switch to per-CPU statistics by pressing kbd:[1].


[role="_additional-resources"]
.Additional resources
* `top(1)` man page on your system
* xref:interpretation-of-the-top-results_identifying-performance-bottlenecks[Interpretation of the `top` results]