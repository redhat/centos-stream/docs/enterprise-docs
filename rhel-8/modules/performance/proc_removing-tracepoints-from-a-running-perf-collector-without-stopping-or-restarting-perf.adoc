:_mod-docs-content-type: PROCEDURE
[id="removing-tracepoints-from-a-running-perf-collector-without-stopping-or-restarting-perf_{context}"]
= Removing tracepoints from a running perf collector without stopping or restarting perf

[role="_abstract"]
Remove tracepoints from a running `perf` collector using the control pipe interface to reduce the scope of data you are collecting without having to stop `perf` and losing performance data.

.Prerequisites

* You have the `perf` user space tool installed as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-perf_monitoring-and-managing-system-status-and-performance#installing-perf_getting-started-with-perf[Installing perf].
* You have added tracepoints to a running `perf` collector via the control pipe interface. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/turning-tracepoints-on-and-off-without-stopping-or-restarting-perf_monitoring-and-managing-system-status-and-performance#adding-tracepoints-to-a-running-perf-collector-without-stopping-or-restarting-perf_turning-tracepoints-on-and-off-without-stopping-or-restarting-perf[Adding tracepoints to a running perf collector without stopping or restarting perf].

.Procedure

* Remove the tracepoint:
+
[subs=+quotes]
----
# echo 'disable _sched:sched_process_fork_' > control
----
+
NOTE: This example assumes you have previously loaded scheduler events into the control file and enabled the tracepoint `sched:sched_process_fork`.

+

This command triggers `perf` to scan the current event list in the control file for the declared event. If the event is present, the tracepoint is disabled and the following message appears in the terminal used to configure the control pipe:
+
[subs=+quotes]
----
event sched:sched_process_fork disabled
----
