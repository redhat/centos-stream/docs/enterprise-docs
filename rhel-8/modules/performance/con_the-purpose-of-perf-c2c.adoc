// Module included in the following assemblies:
//detecting-false-sharing
// <List assemblies here, each on a new line>
:_mod-docs-content-type: CONCEPT
[id="the-purpose-of-perf-c2c_{context}"]
= The purpose of perf c2c

[role="_abstract"]
The `c2c` subcommand of the `perf` tool enables Shared Data Cache-to-Cache (C2C) analysis. You can use the `perf c2c` command to inspect cache-line contention to detect both true and false sharing.

Cache-line contention occurs when a processor core on a Symmetric Multi Processing (SMP) system modifies data items on the same cache line that is in use by other processors. All other processors using this cache-line must then invalidate their copy and request an updated one. This can lead to degraded performance.

The `perf c2c` command provides the following information:

* Cache lines where contention has been detected
* Processes reading and writing the data
* Instructions causing the contention
* The Non-Uniform Memory Access (NUMA) nodes involved in the contention
