:_mod-docs-content-type: PROCEDURE

[id="configuring-secure-connections-for-pcp-monitoring-components_{context}"]
= Configuring secure connections for PCP monitoring components

//RHEL9.2 and on only

Configure your PCP monitoring components to participate in secure PCP protocol exchanges.

.Prerequisites

* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].
* The private client key is stored in the `~/.pcp/tls/client.key` file. If you use a different path, adapt the corresponding steps of the procedure.
+
For details about creating a private key and certificate signing request (CSR), as well as how to request a certificate from a certificate authority (CA), see your CA's documentation.
* The TLS client certificate is stored in the `~/.pcp/tls/client.crt` file. If you use a different path, adapt the corresponding steps of the procedure.
* The CA certificate is stored in the `/etc/pcp/tls/ca.crt` file. If you use a different path, adapt the corresponding steps of the procedure.

.Procedure

. Create a TLS configuration file with the following information:
+ 
[subs=+quotes]
---- 
$ home=`echo ~`
$ cat > ~/.pcp/tls.conf << END
tls-ca-cert-file = /etc/pcp/tls/ca.crt
tls-key-file = $home/.pcp/tls/client.key
tls-cert-file = $home/.pcp/tls/client.crt
END
---- 

. Establish the secure connection:
+ 
[subs=+quotes]
---- 
$ export PCP_SECURE_SOCKETS=enforce
$ export PCP_TLSCONF_PATH=~/.pcp/tls.conf
----


.Verification

* Verify the secure connection is configured:
+
[subs=+quotes]
----
$ pminfo --fetch --host pcps://localhost kernel.all.load

kernel.all.load
    inst [1 or "1 minute"] value 1.26
    inst [5 or "5 minute"] value 1.29
    inst [15 or "15 minute"] value 1.28
----


