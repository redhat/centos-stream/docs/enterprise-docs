// Module included in the following assemblies:
//reviewing-a-system-using-tuna-interface
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="installing-tuna-tool_{context}"]
= Installing the tuna tool

[role="_abstract"]
The `tuna` tool is designed to be used on a running system. This allows application-specific measurement tools to see and analyze system performance immediately after changes have been made.


.Procedure

* Install the `tuna` tool:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install tuna*
----

.Verification

* Display the available `tuna` CLI options:
+
[literal,subs="+quotes"]
----
# *tuna -h*
----

[role="_additional-resources"]
.Additional resources
* `tuna(8)` man page on your system
