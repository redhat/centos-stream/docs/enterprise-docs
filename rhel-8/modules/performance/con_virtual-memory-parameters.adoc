// Module included in the following assemblies:
//configuring-an-operating-system-to-optimize-memory-access
// <List assemblies here, each on a new line>
:_mod-docs-content-type: CONCEPT
[id="virtual-memory-parameters_{context}"]
= Virtual memory parameters

[role="_abstract"]
The virtual memory parameters are listed in the `/proc/sys/vm` directory.

The following are the available virtual memory parameters:

`vm.dirty_ratio`::
Is a percentage value. When this percentage of the total system memory is modified, the system begins writing the modifications to the disk. The default value is `20` percent.

`vm.dirty_background_ratio`::
A percentage value. When this percentage of total system memory is modified, the system begins writing the modifications to the disk in the background. The default value is `10` percent.

`vm.overcommit_memory`::
Defines the conditions that determine whether a large memory request is accepted or denied.The default value is `0`.
+
By default, the kernel performs checks if a virtual memory allocation request fits into the present amount of memory (total + swap) and rejects only large requests. Otherwise virtual memory allocations are granted, and this means they allow memory overcommitment.
+
Setting the `overcommit_memory` parameter’s value:

* When this parameter is set to `1`, the kernel performs no memory overcommit handling. This increases the possibility of memory overload, but improves performance for memory-intensive tasks.

* When this parameter is set to `2`, the kernel denies requests for memory equal to or larger than the sum of the total available swap space and the percentage of physical RAM specified in the `overcommit_ratio`. This reduces the risk of overcommitting memory, but is recommended only for systems with swap areas larger than their physical memory.

`vm.overcommit_ratio`::
Specifies the percentage of physical RAM considered when `overcommit_memory` is set to `2`. The default value is `50`.

`vm.max_map_count`::
Defines the maximum number of memory map areas that a process can use. The default value is `65530`. Increase this value if your application needs more memory map areas.

`vm.min_free_kbytes`::
Sets the size of the reserved free pages pool. It is also responsible for setting the `min_page`, `low_page`, and `high_page` thresholds that govern the behavior of the Linux kernel's page reclaim algorithms. It also specifies the minimum number of kilobytes to keep free across the system. This  calculates a  specific value for each low memory zone, each of which is assigned a number of reserved free pages in proportion to their size.
+
Setting the `vm.min_free_kbytes` parameter’s value:

* Increasing the parameter value effectively reduces the application working set usable memory. Therefore, you might want to use it for only kernel-driven workloads, where driver buffers need to be allocated in atomic contexts.

* Decreasing the parameter value might render the kernel unable to service system requests, if memory becomes heavily contended in the system.
+
[WARNING]
====
Extreme values can be detrimental to the system’s performance. Setting the `vm.min_free_kbytes` to an extremely low value prevents the system from reclaiming memory effectively, which can result in system crashes and failure to service interrupts or other kernel services. However, setting `vm.min_free_kbytes` too high considerably increases system reclaim activity, causing allocation latency due to a false direct reclaim state. This might cause the system to enter an out-of-memory state immediately.
====
+
The `vm.min_free_kbytes` parameter also sets a page reclaim watermark, called `min_pages`. This watermark is used as a factor when determining the two other memory watermarks, `low_pages`, and `high_pages`, that govern page reclaim algorithms.

`/proc/_PID_/oom_adj`::
In the event that a system runs out of memory, and the `panic_on_oom` parameter is set to `0`, the `oom_killer` function kills processes, starting with the process that has the highest `oom_score`, until the system recovers.
+
The `oom_adj` parameter determines the `oom_score` of a process. This parameter is set per process identifier. A value of `-17` disables the `oom_killer` for that process. Other valid values range from `-16` to `15`.

[NOTE]
====
Processes created by an adjusted process inherit the `oom_score` of that process.
====

`vm.swappiness`::
The swappiness value, ranging from `0` to `200`, controls the degree to which the system favors reclaiming memory from the anonymous memory pool, or the page cache memory pool.
+
Setting the `swappiness` parameter’s value:

* Higher values favor file-mapped driven workloads while swapping out the less actively accessed processes’ anonymous mapped memory of RAM. This is useful for file-servers or streaming applications that depend on data, from files in the storage, to reside on memory to reduce I/O latency for the service requests.

* Low values favor anonymous-mapped driven workloads while reclaiming the page cache (file mapped memory). This setting is useful for applications that do not depend heavily on the file system information, and heavily utilize dynamically allocated and private memory, such as mathematical and number crunching applications, and few hardware virtualization supervisors like QEMU.
+
The default value of the `vm.swappiness` parameter is `60`.
+
ifeval::[{ProductNumber} >= 9]
[WARNING]
====
Setting the `vm.swappiness` to `0` aggressively avoids swapping anonymous memory out to a disk, this increases the risk of processes being killed by the `oom_killer` function when under memory or I/O intensive workloads.
====
endif::[]
ifeval::[{ProductNumber} == 8]
[WARNING]
====
* Setting the `vm.swappiness` to `0` aggressively avoids swapping anonymous memory out to a disk, this increases the risk of processes being killed by the `oom_killer` function when under memory or I/O intensive workloads.

* If you are using `cgroupsV1`, the per-cgroup swappiness value exclusive to `cgroupsV1` will result in the system-wide swappiness configured by the `vm.swappiness` parameter having little-to-no effect on the swap behavior of the system. This issue might lead to unexpected and inconsistent swap behavior.
+
In such cases, consider using the `vm.force_cgroup_v2_swappiness` parameter.
+
For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/6785021[Premature swapping with swappiness=0 while there is still plenty of pagecache to be reclaimed].
====

`force_cgroup_v2_swappiness`::
This control is used to deprecate the per-cgroup swappiness value available only in `cgroupsV1`. Most of all system and user processes are run within a cgroup. Cgroup swappiness values default to 60. This can lead to effects where systems swappiness value has little effect on the swap behavior of their system. If a user does not care about the per-cgroup swappiness feature they can configure their system with `force_cgroup_v2_swappiness=1` to have more consistent swappiness behavior across their whole system.
endif::[]


[role="_additional-resources"]
.Additional resources
* `sysctl(8)` man page on your system

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/index#setting-memory-related-kernel-parameters_configuring-an-operating-system-to-optimize-memory-access[Setting memory-related kernel parameters]
