:_module-type: CONCEPT

[id="tuned-cpu-partitioning-profile_{context}"]
= TuneD cpu-partitioning profile

[role="_abstract"]
For tuning {RHEL} {ProductNumber} for latency-sensitive workloads, {RH} recommends to use the `cpu-partitioning` TuneD profile.

Prior to {RHEL} {ProductNumber}, the low-latency {RH} documentation described the numerous low-level steps needed to achieve low-latency tuning. In {RHEL} {ProductNumber}, you can perform low-latency tuning more efficiently by using the `cpu-partitioning` TuneD profile. This profile is easily customizable according to the requirements for individual low-latency applications.

The following figure is an example to demonstrate how to use the `cpu-partitioning` profile. This example uses the CPU and node layout.

[id='cpu-partitioning_{context}']
.Figure cpu-partitioning

image::cpu-partitioning.png[]

You can configure the cpu-partitioning profile in the `/etc/tuned/cpu-partitioning-variables.conf` file using the following configuration options:

Isolated CPUs with load balancing::

In the cpu-partitioning figure, the blocks numbered from 4 to 23, are the default isolated CPUs. The kernel scheduler’s process load balancing is enabled on these CPUs. It is designed for low-latency processes with multiple threads that need the kernel scheduler load balancing.
+
You can configure the cpu-partitioning profile in the `/etc/tuned/cpu-partitioning-variables.conf` file using the `isolated_cores=cpu-list` option, which lists CPUs to isolate that will use the kernel scheduler load balancing.
+
The list of isolated CPUs is comma-separated or you can specify a range using a dash, such as `3-5`. This option is mandatory. Any CPU missing from this list is automatically considered a housekeeping CPU.

Isolated CPUs without load balancing::

In the cpu-partitioning figure, the blocks numbered 2 and 3, are the isolated CPUs that do not provide any additional kernel scheduler process load balancing.
+
You can configure the cpu-partitioning profile in the `/etc/tuned/cpu-partitioning-variables.conf` file using the `no_balance_cores=cpu-list` option, which lists CPUs to isolate that will not use the kernel scheduler load balancing.
+
Specifying the `no_balance_cores` option is optional, however any CPUs in this list must be a subset of the CPUs listed in the `isolated_cores` list.
+
Application threads using these CPUs need to be pinned individually to each CPU.

Housekeeping CPUs::

Any CPU not isolated in the `cpu-partitioning-variables.conf` file is automatically considered a housekeeping CPU.  On the housekeeping CPUs, all services, daemons, user processes, movable kernel threads, interrupt handlers, and kernel timers are permitted to execute.


[role="_additional-resources"]
.Additional resources
* `tuned-profiles-cpu-partitioning(7)` man page on your system
