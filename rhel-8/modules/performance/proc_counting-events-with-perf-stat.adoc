:_mod-docs-content-type: PROCEDURE
[id="counting-events-with-perf-stat_{context}"]
= Counting events with perf stat

[role="_abstract"]
You can use `perf stat` to count hardware and software event occurrences during command execution and generate statistics of these counts. By default, `perf stat` operates in per-thread mode.

.Prerequisites

* You have the `perf` user space tool installed as described in xref:installing-perf_getting-started-with-perf[Installing perf].


.Procedure

* Count the events.

** Running the [command]`perf stat` command without root access will only count events occurring in the user space:
+
----
$ perf stat ls
----
+
.Output of perf stat ran without root access
====
----
Desktop  Documents  Downloads  Music  Pictures  Public  Templates  Videos

 Performance counter stats for 'ls':

              1.28 msec task-clock:u               #    0.165 CPUs utilized
                 0      context-switches:u         #    0.000 M/sec
                 0      cpu-migrations:u           #    0.000 K/sec
               104      page-faults:u              #    0.081 M/sec
         1,054,302      cycles:u                   #    0.823 GHz
         1,136,989      instructions:u             #    1.08  insn per cycle
           228,531      branches:u                 #  178.447 M/sec
            11,331      branch-misses:u            #    4.96% of all branches

       0.007754312 seconds time elapsed

       0.000000000 seconds user
       0.007717000 seconds sys
----
====
+
As you can see in the previous example, when `perf stat` runs without root access the event names are followed by `:u`, indicating that these events were counted only in the user-space.

** To count both user-space and kernel-space events, you must have root access when running [command]`perf stat`:
+
----
# perf stat ls
----
+
.Output of perf stat ran with root access
====
----
Desktop  Documents  Downloads  Music  Pictures  Public  Templates  Videos

 Performance counter stats for 'ls':

              3.09 msec task-clock                #    0.119 CPUs utilized
                18      context-switches          #    0.006 M/sec
                 3      cpu-migrations            #    0.969 K/sec
               108      page-faults               #    0.035 M/sec
         6,576,004      cycles                    #    2.125 GHz
         5,694,223      instructions              #    0.87  insn per cycle
         1,092,372      branches                  #  352.960 M/sec
            31,515      branch-misses             #    2.89% of all branches

       0.026020043 seconds time elapsed

       0.000000000 seconds user
       0.014061000 seconds sys
----
====

*** By default, `perf stat` operates in per-thread mode. To change to CPU-wide event counting, pass the `-a` option to `perf stat`. To count CPU-wide events, you need root access:
+
----
# perf stat -a ls
----

[role="_additional-resources"]
.Additional resources
* `perf-stat(1)` man page on your system
