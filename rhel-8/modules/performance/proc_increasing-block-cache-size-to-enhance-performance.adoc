:_mod-docs-content-type: PROCEDURE

[id="increasing-block-cache-size-to-enhance-performance_{context}"]
= Increasing block map cache size to enhance performance
[[increasing-block-cache-size_optimizing-vdo-performance]]

You can enhance read and write performance by increasing the cache size for your LVM-VDO volume.

If you have extended read and write latencies or a significant volume of data read from storage that does not align with application requirements, you might need to adjust the cache size.

[WARNING]
====
When you increase a block map cache, the cache uses the amount of memory that you specified, plus an additional 15% of memory. Larger cache sizes use more RAM and affect overall system stability.
====

The following example shows how to change the cache size from 128Mb to 640Mb in your system.

.Procedure

. Check the current cache size of your LVM-VDO volume:
+
[subs="+quotes"]
----
# *lvs -o vdo_block_map_cache_size*
  VDOBlockMapCacheSize    
               128.00m                                     
               128.00m
----

. Deactivate the LVM-VDO volume:
+
[subs="+quotes"]
----
# *lvchange -an vg_name/vdo_volume*
----

. Change the LVM-VDO setting:
+
[subs="+quotes"]
----
# *lvchange --vdosettings "block_map_cache_size_mb=640" vg_name/vdo_volume*
----
+
Replace `640` with your new cache size in megabytes.
+
[NOTE]
====
The cache size must be a multiple of 4096, within the range of 128MB to 16TB, and at least 16MB per logical thread. Changes take effect the next time the LVM-VDO device is started. Already running devices are not affected.
====

. Activate the LVM-VDO volume:
+
[subs="+quotes"]
----
# *lvchange -ay vg_name/vdo_volume*
----

.Verification

* Check the current LVM-VDO volume configuration:
+
[subs="+quotes"]
----
# *lvs -o vdo_block_map_cache_size vg_name/vdo_volume*
  VDOBlockMapCacheSize
               640.00m
----

[role="_additional-resources"]
.Additional resources
* `lvchange(8)` man page on your system