// Module included in the following assemblies:
//reviewing-a-system-using-tuna-interface
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="tuning-irqs-using-tuna-tool_{context}"]
= Tuning IRQs using tuna tool

[role="_abstract"]
The `/proc/interrupts` file records the number of interrupts per IRQ, the type of interrupt, and the name of the device that is located at that IRQ.

This procedure describes how to tune the IRQs using the `tuna` tool.

.Prerequisites

* The tuna tool is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/reviewing-a-system-using-tuna-interface_monitoring-and-managing-system-status-and-performance#installing-tuna-tool_reviewing-a-system-using-tuna-interface[Installing tuna tool].

.Procedure

* To view the current IRQs and their affinity:
+
----
# tuna --show_irqs
# users            affinity
0 timer                   0
1 i8042                   0
7 parport0                0
----

* To specify the list of IRQs to be affected by a command:
+
[subs=quotes]
----
# tuna --irqs=_irq_list_ [_command_]
----
+
The _irq_list_ argument is a list of comma-separated IRQ numbers or user-name patterns.
+
Replace [_command_] with, for example, `--spread`.

* To move an interrupt to a specified CPU:
+
[subs=quotes]
----
# tuna --irqs=_128_ --show_irqs
   # users            affinity
 128 iwlwifi           0,1,2,3

# tuna --irqs=_128_ --cpus=3 --move
----
+
Replace _128_ with the irq_list argument and _3_ with the cpu_list argument.
+
The _cpu_list_ argument is a list of comma-separated CPU numbers, for example, `--cpus=_0,2_`. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/reviewing-a-system-using-tuna-interface_monitoring-and-managing-system-status-and-performance#tuning-cpus-using-tuna-tool_reviewing-a-system-using-tuna-interface[Tuning CPUs using tuna tool].


.Verification

* Compare the state of the selected IRQs before and after moving any interrupt to a specified CPU:
+
[subs=quotes]
----
# tuna --irqs=_128_ --show_irqs
   # users            affinity
 128 iwlwifi                 3
----

[role="_additional-resources"]
.Additional resources
* `/procs/interrupts` file
* `tuna(8)` man page on your system
