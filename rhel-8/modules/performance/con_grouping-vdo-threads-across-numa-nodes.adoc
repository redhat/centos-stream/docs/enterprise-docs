:_newdoc-version: 2.15.0
:_template-generated: 2023-11-1

:_mod-docs-content-type: CONCEPT

[id="grouping-vdo-threads-across-numa-nodes_{context}"]
= Grouping VDO threads across NUMA nodes

Accessing memory across NUMA nodes is slower than local memory access. On Intel processors where cores share the last-level cache within a node, cache problems are more significant when data is shared between nodes than when it is shared within a single node. While many VDO kernel threads manage exclusive data structures, they often exchange messages about I/O requests. VDO threads being spread across multiple nodes or the scheduler reassigning threads between nodes might cause contention, that is multiple nodes competing for the same resources.

You can enhance VDO performance by grouping certain threads on the same NUMA nodes.

Group related threads together on one NUMA node::

* I/O acknowledgment (`ackQ`) threads
* Higher-level I/O submission threads:
** User-mode threads handling direct I/O
** Kernel page cache flush thread

Optimize device access::

* If device access timing varies across NUMA nodes, run `bioQ` threads on the node closest to the storage device controllers

Minimize contention::

* Run I/O submissions and storage device interrupt processing on the same node as `logQ` or `physQ` threads.
* Run other VDO-related work on the same node.
* If one node cannot handle all VDO work, consider memory contention when moving threads to other nodes. For example, move the device that interrupts handling and `bioQ` threads to another node.