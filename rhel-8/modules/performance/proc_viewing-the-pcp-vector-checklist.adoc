// Module included in the following assemblies:
//setting-up-graphical-representation-of-pcp-metrics
// <List assemblies here, each on a new line>
:experimental:
:_mod-docs-content-type: PROCEDURE
[id="viewing-the-pcp-vector-checklist_{context}"]
= Viewing the PCP Vector Checklist

[role="_abstract"]
The PCP Vector data source displays live metrics and uses the `pcp` metrics. It analyzes data for individual hosts.

After adding the PCP Vector data source, you can view the dashboard with an overview of useful metrics and view the related troubleshooting or reference links in the checklist.

.Prerequisites

. The PCP Vector is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#installing-pcp-vector_setting-up-graphical-representation-of-pcp-metrics[Installing PCP Vector].

. The `grafana-server` is accessible. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#accessing-the-grafana-web-ui_setting-up-graphical-representation-of-pcp-metrics[Accessing the Grafana web UI].

.Procedure

. Log into the Grafana web UI.

. In the Grafana *Home* page, click *Add your first data source*.

. In the *Add data source* pane, type vector in the *Filter by name or type* text box and then click *PCP Vector*.

. In the *Data Sources / PCP Vector* pane, perform the following:

.. Add `++http://localhost:44322++` in the *URL* field and then click btn:[Save & Test].

.. Click menu:Dashboards tab[Import > PCP Vector: Host Overview] to see a dashboard with an overview of any useful metrics.
+
.PCP Vector: Host Overview
image::pcp-vector-host-overview.png[]

. From the menu, hover over the &nbsp;&nbsp; image:pcp-plugin-in-grafana.png[] &nbsp;&nbsp; *Performance Co-Pilot* plugin and then click *PCP Vector Checklist*.
+
In the PCP checklist, click &nbsp;&nbsp; image:pcp-vector-checklist-troubleshooting-doc.png[] &nbsp;&nbsp; help or &nbsp;&nbsp; image:pcp-vector-checklist-warning.png[] &nbsp;&nbsp; warning icon to view the related troubleshooting or reference links.
+
.Performance Co-Pilot / PCP Vector Checklist
image::pcp-vector-checklist.png[]
