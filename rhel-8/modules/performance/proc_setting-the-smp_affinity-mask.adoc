// Module included in the following assemblies:
//configuring-an-operating-system-to-optimize-cpu-utilization
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="setting-the-smp_affinity-mask_{context}"]
= Setting the smp_affinity mask

[role="_abstract"]
The `smp_affinity` value is stored as a hexadecimal bit mask representing all processors in the system. Each bit configures a different CPU. The least significant bit is CPU 0.

The default value of the mask is `f`, which means that an interrupt request can be handled on any processor in the system. Setting this value to 1 means that only processor 0 can handle the interrupt.


.Procedure

. In binary, use the value 1 for CPUs that handle the interrupts. For example, to set CPU 0 and CPU 7 to handle interrupts, use `0000000010000001` as the binary code:
+
.Binary Bits for CPUs
[cols="3,16*"]
|===
|CPU | 15| 14| 13| 12| 11| 10| 9| 8| 7| 6| 5| 4| 3| 2| 1| 0
|Binary | 0| 0| 0| 0| 0| 0| 0| 0| 1| 0| 0| 0| 0| 0| 0| 1
|===

. Convert the binary code to hexadecimal:
+
For example, to convert the binary code using Python:
+
----
>>> hex(int('0000000010000001', 2))

'0x81'
----
+
On systems with more than 32 processors, you must delimit the `smp_affinity` values for discrete 32 bit groups. For example, if you want only the first 32 processors of a 64 processor system to service an interrupt request, use `0xffffffff,00000000`.

. The interrupt affinity value for a particular interrupt request is stored in the associated `/proc/irq/irq_number/smp_affinity` file. Set the `smp_affinity` mask in this file:
+
----
# echo mask > /proc/irq/irq_number/smp_affinity
----

[role="_additional-resources"]
.Additional resources
* `journalctl(1)`, `irqbalance(1)`, and `taskset(1)` man pages on your system
