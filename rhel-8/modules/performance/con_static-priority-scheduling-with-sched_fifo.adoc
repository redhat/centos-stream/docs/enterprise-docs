// Module included in the following assemblies:
//tuning-scheduling-policy
// <List assemblies here, each on a new line>
:_mod-docs-content-type: CONCEPT
[id="static-priority-scheduling-with-SCHED_FIFO_{context}"]
= Static priority scheduling with SCHED_FIFO

[role="_abstract"]
The `SCHED_FIFO`, also called static priority scheduling, is a realtime policy that defines a fixed priority for each thread. This policy allows administrators to improve event response time and reduce latency. It is recommended to not execute this policy for an extended period of time for time sensitive tasks.

When `SCHED_FIFO` is in use, the scheduler scans the list of all the `SCHED_FIFO` threads in order of priority and schedules the highest priority thread that is ready to run. The priority level of a `SCHED_FIFO` thread can be any integer from `1` to `99`, where `99` is treated as the highest priority. {RH} recommends starting with a  lower number and increasing priority only when you identify latency issues.

[WARNING]
====
Because realtime threads are not subject to time slicing, {RH} does not recommend setting a priority as 99. This keeps your process at the same priority level as migration and watchdog threads; if your thread goes into a computational loop and these threads are blocked, they will not be able to run. Systems with a single processor will eventually hang in this situation.
====

Administrators can limit `SCHED_FIFO` bandwidth to prevent realtime application programmers from initiating realtime tasks that monopolize the processor.

The following are some of the parameters used in this policy:

`/proc/sys/kernel/sched_rt_period_us`::
This parameter defines the time period, in microseconds, that is considered to be one hundred percent of the processor bandwidth. The default value is `1000000 μs`, or `1 second`.

`/proc/sys/kernel/sched_rt_runtime_us`::
This parameter defines the time period, in microseconds, that is devoted to running real-time threads. The default value is `950000 μs`, or `0.95 seconds`.
