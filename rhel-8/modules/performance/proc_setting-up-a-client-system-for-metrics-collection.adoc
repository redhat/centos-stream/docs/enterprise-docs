// Module included in the following assemblies:
//logging-performance-data-with-pmlogger
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE
[id="setting-up-a-client-system-for-metrics-collection_{context}"]
= Setting up a client system for metrics collection

[role="_abstract"]
This procedure describes how to set up a client system so that a central server can collect metrics from clients running PCP.

.Prerequisites


* PCP is installed. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#installing-and-enabling-pcp_setting-up-pcp[Installing and enabling PCP].

.Procedure

. Install the `pcp-system-tools` package:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install pcp-system-tools
----

. Configure an IP address for `pmcd`:
+
[subs=quotes]
----
# echo "-i _192.168.4.62_" >>/etc/pcp/pmcd/pmcd.options
----
+
Replace _192.168.4.62_ with the IP address, the client should listen on.
+
By default, `pmcd` is listening on the localhost.

. Configure the firewall to add the public `zone` permanently:
+
----
# firewall-cmd --permanent --zone=public --add-port=44321/tcp
success

# firewall-cmd --reload
success
----

. Set an SELinux boolean:
+
----
# setsebool -P pcp_bind_all_unreserved_ports on
----

. Enable the `pmcd` and `pmlogger` services:
+
----
# systemctl enable pmcd pmlogger
# systemctl restart pmcd pmlogger
----

.Verification

* Verify if the `pmcd` is correctly listening on the configured IP address:
+
[literal,subs="quotes,attributes"]
....
# ss -tlp | grep 44321
LISTEN   0   5     127.0.0.1:44321   0.0.0.0:*   users:(("pmcd",pid=151595,fd=6))
LISTEN   0   5  _192.168.4.62:44321_   0.0.0.0:*   users:(("pmcd",pid=151595,fd=0))
LISTEN   0   5         [::1]:44321      [::]:*   users:(("pmcd",pid=151595,fd=7))
....

[role="_additional-resources"]
.Additional resources
* `pmlogger(1)`, `firewall-cmd(1)`, `ss(8)`, and `setsebool(8)` man pages on your system

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-pcp_monitoring-and-managing-system-status-and-performance#system-services-distributed-with-pcp_setting-up-pcp[System services and tools distributed with PCP]

* `/var/lib/pcp/config/pmlogger/config.default` file
