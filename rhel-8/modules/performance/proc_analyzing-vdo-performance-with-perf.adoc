:_newdoc-version: 2.15.0
:_template-generated: 2023-11-1
:_mod-docs-content-type: PROCEDURE

[id="analyzing-vdo-performance-with-perf_{context}"]
= Analyzing VDO performance with perf

You can check the CPU performance of VDO by using the `perf` utility.

.Prerequisites

* The `perf` package is installed.

.Procedure

. Display the performance profile:
+
[subs="+quotes"]
----
# *perf top*
----

. Analyze the CPU performance by interpreting `perf` results:
+
.Interpreting `perf` results
[options="header"]
|===
| Values | Description | Suggestions

| `kvdo:bioQ` threads spend excessive cycles acquiring spin locks
| Too much contention might be occurring in the device driver below VDO
| Reduce the number of `kvdo:bioQ` threads

| High CPU usage
| Contention between NUMA nodes.

Check counters such as `stalled-cycles-backend`, `cache-misses`, and `node-load-misses` if they are supported by your processor. High miss rates might cause stalls, resembling high CPU usage in other tools, indicating possible contention.
| Implement CPU affinity for the VDO kernel threads or IRQ affinity for interrupt handlers to restrict processing work to a single node.

|===

[role="_additional-resources"]
.Additional resources
* `perf-top(1)` man page on your system

