:_newdoc-version: 2.18.3
:_template-generated: 2024-07-04
:_mod-docs-content-type: PROCEDURE

[id="selecting-a-package_{context}"]
= Selecting a package

[role="_abstract"]
Customize the blueprint with packages and modules.

* The `name` attribute is a required string and can be an exact match, or a filesystem glob that uses `*` for wildcards, and `?` for character matching.
* The `version` attribute is an optional string can be an exact match or a filesystem glob of the version that uses `*` for wildcards, and `?` for character matching. If you do not enter a version, the system uses the latest version in the repositories. 

When you use a virtual provides as the package name, the version glob must be `*`. Consequently, you will be unable to freeze the blueprint, because the provides will expand into multiple packages with their own names and versions.

NOTE: Currently, there are no differences between packages and modules in `osbuild-composer`. Both are treated as an RPM package dependency. 
.Procedure

* Customize your blueprint with a package:
+
[subs="quotes,attributes"]
----
[[packages]]
name = "_package_name_"
----
+
Replace `_package_name_` with the name of the group. For example, the `tmux-2.9a`, and the `openssh-server-8.*` packages.
+
[subs="quotes,attributes"]
----
[[packages]]
name = "tmux"
version = "2.9a"
----

[[packages]]
name = "openssh-server"
version = "8.*"
