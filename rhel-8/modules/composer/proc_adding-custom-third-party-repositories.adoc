:_mod-docs-content-type: PROCEDURE

[id="proc_adding-custom-third-party-repositories_{context}"]
= Adding custom third-party repositories to RHEL image builder

You can add custom third-party sources to your repositories and manage these repositories by using the `composer-cli`. 

.Prerequisites

* You have the URL of the custom third-party repository.

.Procedure

. Create a repository source file, such as `/root/repo.toml`. For example:
+
[literal]
----
id = "k8s"
name = "Kubernetes"
type = "yum-baseurl"
url = "https://server.example.com/repos/company_internal_packages/"
check_gpg = false
check_ssl = false
system = false
----
+
The `type` field accepts the following valid values: `yum-baseurl`, `yum-mirrorlist`, and `yum-metalink`.
. Save the file in the TOML format.

. Add the new third-party source to RHEL image builder:
+
[subs="quotes,attributes"]
----
$ *composer-cli sources add [replaceable]__&lt;file-name&gt;__.toml*
----

.Verification

. Check if the new source was successfully added:
+
[subs="quotes,attributes"]
----
$ *composer-cli sources list*
----

. Check the new source content:
+
[subs="quotes,attributes"]
----
$ *composer-cli sources info [replaceable]__&lt;source_id&gt;__*
----

