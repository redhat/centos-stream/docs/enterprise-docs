:_mod-docs-content-type: CONCEPT

[id="con_the-openscap-blueprint-customization_{context}"]
= The OpenSCAP blueprint customization

With the OpenSCAP support for blueprint customization, you can generate blueprints from the`scap-security-guide` content for specific security profiles and then use the blueprints to build your own pre-hardened images. 

Creating  a customized blueprint with OpenSCAP involves the following high level steps:

* Modify the mount points and configure the file system layout according to your specific requirements. 
* In the blueprint, Select the OpenSCAP profile. This configures the image to trigger the remediation during the image build in accordance with the selected profile. Also during the image build, OpenSCAP applies a pre-first-boot remediation.


To use the `OpenSCAP` blueprint customization in your image blueprints, you need to provide the following information:

* The data stream path to the `datastream` remediation instructions. The data stream files from `scap-security-guide` package are located in the `/usr/share/xml/scap/ssg/content/` directory.
ifeval::[{ProductNumber} == 9]
* The `profile_id` of the required security profile. The value of the `profile_id` field accepts both the long and short forms, for example, the following are acceptable: `cis` or `xccdf_org.ssgproject.content_profile_cis`. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/security_hardening/scanning-the-system-for-configuration-compliance-and-vulnerabilities_security-hardening#scap-security-guide-profiles-supported-in-rhel-9_scanning-the-system-for-configuration-compliance-and-vulnerabilities[SCAP Security Guide profiles supported in RHEL 9] for more details.
endif::[]

ifeval::[{ProductNumber} == 8]
* The `profile_id` of the required security profile. The value of the `profile_id` field accepts both the long and short forms, for example, the following are acceptable: `cis` or `xccdf_org.ssgproject.content_profile_cis`. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html/security_hardening/scanning-the-system-for-configuration-compliance-and-vulnerabilities_security-hardening#scap-security-guide-profiles-supported-in-rhel_scanning-the-system-for-configuration-compliance-and-vulnerabilities[SCAP Security Guide profiles supported in RHEL 8] for more details.
endif::[]

The following example is a snippet with the `OpenSCAP` remediation stage:

[subs="quotes,attributes"]
----
[customizations.openscap]
# If you want to use the data stream from the 'scap-security-guide' package
# the 'datastream' key could be omitted.
# datastream = "/usr/share/xml/scap/ssg/content/ssg-rhel{ProductNumber}-ds.xml"
profile_id = "xccdf_org.ssgproject.content_profile_cis"
----

You can find more details about the `SCAP` source data stream from the `scap-security-guide` package, including the list of security profiles it provides, by using the command:

[subs="quotes,attributes"]
----
# *oscap info /usr/share/xml/scap/ssg/content/ssg-rhel{ProductNumber}-ds.xml*
----

For your convenience the `OpenSCAP` tool can generate the hardening blueprint for any profile available in `scap-security-guide` data streams.

For example, the command:

[subs="quotes,attributes"]
----
# *oscap xccdf generate fix --profile=cis --fix-type=blueprint /usr/share/xml/scap/ssg/content/ssg-rhel{ProductNumber}-ds.xml*
----
generates a blueprint for CIS profile similar to:

[subs="quotes,attributes"]
----
# Blueprint for CIS Red Hat Enterprise Linux {ProductNumber} Benchmark for Level 2 - Server
#
# Profile Description:
# This profile defines a baseline that aligns to the "Level 2 - Server"
# configuration from the Center for Internet Security® Red Hat Enterprise
# Linux {ProductNumber} Benchmark™, v3.0.0, released 2023-10-30.
# This profile includes Center for Internet Security®
# Red Hat Enterprise Linux {ProductNumber} CIS Benchmarks™ content.
#
# Profile ID:  xccdf_org.ssgproject.content_profile_cis
# Benchmark ID:  xccdf_org.ssgproject.content_benchmark_RHEL-{ProductNumber}
# Benchmark Version:  0.1.74
# XCCDF Version:  1.2

name = "hardened_xccdf_org.ssgproject.content_profile_cis"
description = "CIS Red Hat Enterprise Linux {ProductNumber} Benchmark for Level 2 - Server"
version = "0.1.74"

[customizations.openscap]
profile_id = "xccdf_org.ssgproject.content_profile_cis"
# If your hardening data stream is not part of the 'scap-security-guide' package
# provide the absolute path to it (from the root of the image filesystem).
# datastream = "/usr/share/xml/scap/ssg/content/ssg-xxxxx-ds.xml"

[[customizations.filesystem]]
mountpoint = "/home"
size = 1073741824

[[customizations.filesystem]]
mountpoint = "/tmp"
size = 1073741824

[[customizations.filesystem]]
mountpoint = "/var"
size = 3221225472

[[customizations.filesystem]]
mountpoint = "/var/tmp"
size = 1073741824

[[packages]]
name = "aide"
version = "\*"

[[packages]]
name = "libselinux"
version = "*"

[[packages]]
name = "audit"
version = "*"

[customizations.kernel]
append = "audit_backlog_limit=8192 audit=1"

[customizations.services]
enabled = ["auditd","crond","firewalld","systemd-journald","rsyslog"]
disabled = []
masked = ["nfs-server","rpcbind","autofs","bluetooth","nftables"]
----

NOTE: Do not use this exact blueprint snippet for image hardening. It does not reflect a complete profile. As Red Hat constantly updates and refines security requirements for each profile in the `scap-security-guide` package, it makes sense to always re-generate the initial template using the most up-to-date version of the data stream provided for your system.

Now you can customize the blueprint or use it as it is to build an image.

RHEL image builder generates the necessary configurations for the `osbuild` stage based on your blueprint customization. Additionally, RHEL image builder adds two packages to the image:

* `openscap-scanner` - the `OpenSCAP` tool.
* `scap-security-guide` - the package which contains the remediation and evaluation instructions.
+
NOTE: The remediation stage uses the `scap-security-guide` package for the datastream because this package is installed on the image by default. If you want to use a different datastream, add the necessary package to the blueprint, and specify the path to the datastream in the `oscap` configuration.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/security_hardening/index#scap-security-guide-profiles-supported-in-rhel{ProductNumber}_scanning-the-system-for-configuration-compliance-and-vulnerabilities[SCAP Security Guide profiles supported in RHEL {ProductNumber}]
