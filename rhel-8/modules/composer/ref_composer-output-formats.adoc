:_mod-docs-content-type: REFERENCE

[id="composer-output-formats_{context}"]
= RHEL image builder output formats

[role="_abstract"]
RHEL image builder can create images in multiple output formats shown in the following table. 

.RHEL image builder output formats
[options="header",cols="2,1,1"]
|====
| Description | CLI name | File extension
| QEMU Image | `qcow2` | [filename]`.qcow2`
| Disk Archive | `tar` | [filename]`.tar`
| Amazon Web Services | `raw` | [filename]`.raw`
| Microsoft Azure | `vhd` | [filename]`.vhd`
| Google Cloud Platform | `gce`| [filename]`.tar.gz`
| VMware vSphere | `vmdk` | [filename]`.vmdk`
| VMware vSphere | `ova` | [filename]`.ova`
| Openstack | `openstack` | [filename]`.qcow2`
| RHEL for Edge Commit  | `edge-commit` | [filename]`.tar`
| RHEL for Edge Container | `edge-container` | [filename]`.tar`
| RHEL for Edge Installer | `edge-installer` | [filename]`.iso`
| RHEL for Edge Raw Image | `edge-raw-image` | [filename]`.raw.xz`
| RHEL for Edge Simplified Installer | `edge-simplified-installer` | [filename]`.iso`
| RHEL for Edge AMI | `edge-ami` | [filename]`.ami` 
| RHEL for Edge VMDK | `edge-vsphere` | [filename]`.vmdk` 
| RHEL Installer | `image-installer` | [filename]`.iso`
| Oracle Cloud Infrastructure | [filename]`.oci` | [filename]`.qcow2`
|====

To check the supported types, run the command:
[subs="quotes,attributes"]
----
# composer-cli compose types
----

// https://github.com/osbuild/osbuild-composer/blob/main/internal/distro/rhel8/package_sets.go
// Composer can create system images for multiple architectures, but which are added to the system?
