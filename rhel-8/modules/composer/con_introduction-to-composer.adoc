:_mod-docs-content-type: CONCEPT
[id="introduction-to-composer_{context}"]
= What is image builder?
// CON describing composer - what it is and how to get it

[role="_abstract"]
You can use image builder to create customized system images of {ProductShortName}, including system images prepared for deployment on cloud platforms. Image builder automatically handles the setup details for each output type and is therefore easier to use and faster to work with than manual methods of image creation. You can access the image builder functionality on the command line by using the [command]`composer-cli` tool, or a graphical user interface in the RHEL web console.

NOTE: From RHEL 8.3 onward, the `osbuild-composer` back end replaces `lorax-composer`. The new service provides REST APIs for image building. 




