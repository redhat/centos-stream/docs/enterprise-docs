:_mod-docs-content-type: PROCEDURE

[id="proc_building-an-image-and-pulling-the-container-into-the-image_{context}"]
= Building an image and pulling the container into the image

After you have created the container image, you can build your customized image and pull the container image into it. For that, you must specify a *container customization* in the blueprint, and the *container name* for the final image. During the build process, the container image is fetched and placed in the local Podman container storage.


.Prerequisites

* You created a container image and pushed it into your local `quay.io` container registry instance. See xref:proc_pushing-a-container-artifact-directly-to-a-container-registry_assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image[Pushing a container artifact directly to a container registry].
* You have access to link:https://access.redhat.com/terms-based-registry/#/[registry.access.redhat.com].
* You have a container `manifest ID`. 
* You have the `qemu-kvm` and `qemu-img` packages installed. 

.Procedure

. Create a blueprint to build a `qcow2` image. The blueprint must contain the "_[[containers]]_" customization.
+
[subs="quotes,attributes"]
----
name = "image"
description = "A qcow2 image with a container"
version = "0.0.1"
distro = "rhel-90"
[[packages]]
name = "podman"
version = "*"
[[containers]]
source = "registry.access.redhat.com/ubi9:8080/osbuild/container/container-image@sha256:manifest-ID-from-Repository-tag: tag-version"
name =  "source-name"
tls-verify = true
----

. Push the blueprint:
+
[subs="quotes,attributes"]
----
# composer-cli blueprints push [replaceable]__blueprint-image__.toml
----

. Build the container image:
+
[subs="quotes,attributes"]
----
# composer-cli start compose [replaceable]__image__ _qcow2_
----
+
- _image_ is the blueprint name.
- `qcow2` is the image type.
+
NOTE: Building the image takes time because it checks the container on `quay.io` registry.
+
. To check the status of the compose:
+
[subs="quotes,attributes"]
----
# composer-cli compose status
----
+
A finished compose shows the *FINISHED* status value. To identify your compose in the list, use its UUID.

. After the compose process is finished, download the resulting image file to your default download location: 
+
[subs="quotes,attributes"]
----
# composer-cli compose image _UUID_
----
+
Replace UUID with the UUID value shown in the previous steps.
+
You can use the `qcow2` image you created and downloaded to create a VM.

.Verification

From the resulting `qcow2` image that you downloaded, perform the following steps:

. Start the `qcow2` image in a VM. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/composing_a_customized_rhel_system_image/index#creating-a-virtual-machine-from-a-kvm-guest-image_preparing-and-deploying-kvm-guest-images-with-image-builder[Creating a virtual machine from a KVM guest image].

. The `qemu` wizard opens. Login in to the `qcow2` image. 
.. Enter the username and password. These can be the username and password you set up in the `.qcow2` blueprint in the  "customizations.user" section, or created at boot time with `cloud-init`.

. Run the container image and open a shell prompt inside the container:
+
[subs="quotes,attributes"]
----
# podman run -it registry.access.redhat.com/ubi9:8080/osbuild/_repository_ /bin/bash/
----
+
`registry.access.redhat.com` is the target registry, `osbuild` is the organization and `repository` is the location to push the container when it finishes building.

. Check that the packages you added to the blueprint are available:
+
[subs="quotes,attributes"]
----
# type -a nginx
----
+
The output shows you the `nginx` package path.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/RegistryAuthentication[Red Hat Container Registry Authentication]
* link:https://access.redhat.com/documentation/en-us/openshift_container_platform/3.11/html/configuring_clusters/install-config-configuring-red-hat-registry[Accessing and Configuring the Red Hat Registry]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html/managing_containers/finding_running_and_building_containers_with_podman_skopeo_and_buildah[Basic Podman commands]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/building_running_and_managing_containers/index#proc_running-skopeo-in-a-container_assembly_running-skopeo-buildah-and-podman-in-a-container[Running Skopeo in a container]

