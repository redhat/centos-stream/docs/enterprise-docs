:_mod-docs-content-type: PROCEDURE

[id="creating-customized-kvm-guest-images-with-image-builder_{context}"]
= Creating customized KVM guest images by using RHEL image builder

[role="_abstract"]
You can create a  customized `.qcow2` KVM guest image by using RHEL image builder. The following procedure shows the steps on the GUI, but you can also use the CLI.

.Prerequisites

* You must be in the `root` or `weldr` group to access the system.
* The `cockpit-composer` package is installed.
* On a RHEL system, you have opened the RHEL image builder dashboard of the web console.
* You have created a blueprint. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_a_customized_rhel_system_image/creating-system-images-with-composer-web-console-interface_composing-a-customized-rhel-system-image#creating-a-composer-blueprint-in-the-web-console-interface_creating-system-images-with-composer-web-console-interface[Creating a blueprint in the web console interface].

.Procedure

. Click the blueprint name you created.

. Select the tab *Images*.

. Click *Create Image* to create your customized image.
The *Create Image* window opens.

. From the *Type* drop-down menu list, select `QEMU Image(.qcow2)`.

. Set the size that you want the image to be when instantiated and click *Create*.

. A small pop-up on the upper right side of the window informs you that the image creation has been added to the queue.
After the image creation process is complete, you can see the *Image build complete* status.


.Verification

* Click the breadcrumbs icon and select the *Download* option. RHEL image builder downloads the KVM guest image `.qcow2` file at your default download location.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_a_customized_rhel_system_image/creating-system-images-with-composer-web-console-interface_composing-a-customized-rhel-system-image#creating-a-composer-blueprint-in-the-web-console-interface_creating-system-images-with-composer-web-console-interface[Creating a blueprint in the web console interface]
