:experimental:

:_mod-docs-content-type: PROCEDURE

[id="uploading-images-to-alibaba_{context}"]
= Uploading customized RHEL images to Alibaba

[role="_abstract"]
You can upload a customized `AMI` image you created by using RHEL image builder to the Object Storage Service (OSS).

.Prerequisites

* Your system is set up for uploading Alibaba images. See xref:preparing-uploading-images-to-alibaba_creating-cloud-images-with-composer[Preparing for uploading images to Alibaba].
* You have created an `ami` image by using RHEL image builder. 
* You have a bucket. See link:https://www.alibabacloud.com/help/doc-detail/31885.htm?spm=a2c63.p38356.b99.19.5c3f465a0WnfaV[Creating a bucket].
* You have an link:https://account.alibabacloud.com/register/intl_register.htm?spm=a2c63.p38356.879954.7.2ce96962qvmvAi[active Alibaba Account].
* You activated link:https://www.alibabacloud.com/help/doc-detail/31884.htm?spm=a2c63.p38356.879954.10.7c0a64baufqGup#task-njz-hf4-tdb[OSS].


.Procedure

. Log in to the link:https://oss.console.aliyun.com/?spm=a2c63.p38356.879954.10.2171455fhuA3H5[OSS console].

. In the Bucket menu on the left, select the bucket to which you want to upload an image.

. In the upper right menu, click the kbd:[Files] tab.

. Click btn:[Upload]. A dialog window opens on the right side. Configure the following:

* *Upload To*: Choose to upload the file to the *Current* directory or to a *Specified* directory.
* *File ACL*: Choose the type of permission of the uploaded file.

. Click btn:[Upload].

. Select the image you want to upload to the OSS Console..

. Click btn:[Open].

[role="_additional-resources"]
.Additional resources
* link:https://www.alibabacloud.com/help/doc-detail/31886.htm?spm=a2c63.p38356.b99.20.454c5dc4qRcnad[Upload an object.]
* link:https://www.alibabacloud.com/help/doc-detail/25542.htm?spm=a2c63.p38356.879954.20.7c0a64batcdSMD#ImportImage[Creating an instance from custom images.]
* link:https://www.alibabacloud.com/help/doc-detail/48226.htm[Importing images.]
