:_newdoc-version: 2.18.2
:_template-generated: 2024-06-24
:_mod-docs-content-type: PROCEDURE

[id="using-satellite-cv-as-repositories-to-build-images-in-rhel-image-builder_{context}"]
= Using Satellite CV as repositories to build images in RHEL image builder

Configure RHEL image builder to use Satellite's content views (CV)
as repositories to build your custom images. 

.Prerequisites

* You have integrated Satellite with RHEL web console. See link:https://docs.redhat.com/en/documentation/red_hat_satellite/6.15/html/managing_hosts/host_management_and_monitoring_using_cockpit_managing-hosts?extIdCarryOver=true&intcmp=701f20000012ngPAAQ&sc_cid=701f2000001Css5AAC#Enabling_Cockpit_on_Server_managing-hosts[Enabling the RHEL web console on Satellite]

.Procedure

. In the Satellite web UI, navigate to *Content > Products*, select your *Product* and click the repository you want to use. 

. Search for the secured URL (HTTPS) in the Published field and copy it.

. Use the URL that you copied as a baseurl for the Red Hat image builder repository. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/composing_a_customized_rhel_system_image/managing-repositories_composing-a-customized-rhel-system-image#proc_adding-custom-third-party-repositories_managing-repositories[Adding custom third-party repositories to RHEL image builder].


[role="_additional-resources"]
.Next steps

* Build the image. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/composing_a_customized_rhel_system_image/creating-system-images-with-composer-web-console-interface_composing-a-customized-rhel-system-image#creating-a-system-image-with-composer-in-the-web-console-interface_creating-system-images-with-composer-web-console-interface[Creating a system image by using RHEL image builder in the web console interface].



