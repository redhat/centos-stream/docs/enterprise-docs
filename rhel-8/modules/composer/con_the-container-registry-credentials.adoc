:_mod-docs-content-type: CONCEPT

[id="con_the-container-registry-credentials_{context}"]
= The Container registry credentials

The `osbuild-worker@.service` is a template service that can start multiple service instances. By default, the `osbuild-composer` service always starts with only one local `osbuild-worker`, specifically `osbuild-worker@1.service`. 
The `osbuild-worker` service is responsible for the communication with the container registry. 
To enable the service, set up the `/etc/osbuild-worker/osbuild-worker.toml` configuration file.

[NOTE]

====
After setting the `/etc/osbuild-worker/osbuild-worker.toml` configuration file, you must restart the `osbuild-worker` service, because it reads the `/etc/osbuild-worker/osbuild-worker.toml` configuration file only once, during the `osbuild-worker` service start.
====

To stop the service instance, restart the systemd service with the following command: 

[subs="+quotes,attributes"]
----
$ systemctl restart osbuild-worker@*
----

With that, you restart all the started instances of `osbuild-worker`, specifically `osbuild-worker@1.service`, the only service that might be running.

The `/etc/osbuild-worker/osbuild-worker.toml` configuration file has a containers section with an `auth_field_path` entry that is a string referring to a path of a `containers-auth.json` file to be used for accessing protected resources. The container registry credentials are only used to pull a container image from a registry, when embedding the container into the image.

For example:

[subs="+quotes,attributes"]
----
[containers]
auth_file_path = "/etc/osbuild-worker/containers-auth.json"
----

[role="_additional-resources"]
.Additional resources
* The `containers-auth.json` man page on your system

