:_mod-docs-content-type: PROCEDURE

[id="selecting-a-distribution_{context}"]
= Selecting a distribution

[role="_abstract"]
You can use the `distro` field to specify the distribution to use when composing your images or solving dependencies in the blueprint. If the `distro` field is left blank,  the blueprint automatically uses the host's operating system distribution. If you do not specify a distribution, the blueprint uses the host distribution. When you upgrade the host operating system, blueprints without a specified distribution build images by using the upgraded operating system version.

You can build images for older major versions on a newer system. For example, you can use a RHEL 10 host to create RHEL 9 and RHEL 8 images. However, you cannot build images for newer major versions on an older system.

[IMPORTANT]

====
You cannot build an operating system image that differs from the RHEL image builder host. For example, you cannot use a RHEL system to build Fedora or CentOS images.
====

* Customize the blueprint with the RHEL distribution to always build the specified RHEL image:
+
[subs="quotes,attributes"]
----
name = "_blueprint_name_"
description = "_blueprint_version_"
version = "0.1"
distro = "_different_minor_version_"
----
+
For example:
+
[subs="quotes,attributes"]
----
name = "tmux"
description = "tmux image with openssh"
version = "1.2.16"
distro = "rhel-9.5"
----

ifeval::[{ProductNumber} == 8]
Replace "_different_minor_version_" to build a different minor version, for example, if you want to build a RHEL 8.10 image, use `distro` = "rhel-810". On RHEL 8.10 image, you can build minor versions such as RHEL 8.9 and earlier releases. 
endif::[]

ifeval::[{ProductNumber} == 9]
Replace `"_different_minor_version_"` to build a different minor version, for example, if you want to build a RHEL 9.5 image, use `distro` = "rhel-95". On RHEL 9.3 image, you can build minor versions such as RHEL 9.3, RHEL 9.2, and earlier releases. 
endif::[]
