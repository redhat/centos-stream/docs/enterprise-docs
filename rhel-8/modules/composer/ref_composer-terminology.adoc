:_mod-docs-content-type: REFERENCE

[id="composer-terminology_{context}"]
= RHEL image builder terminology

[role="_abstract"]
RHEL image builder uses the following concepts:

Blueprint::
+
A blueprint is a description of a customized system image. It lists the packages and customizations that will be part of the system. You can edit blueprints with customizations and save them as a particular version. When you create a system image from a blueprint, the image is associated with the blueprint in the RHEL image builder interface.
+
Create blueprints in the TOML format.

Compose::
+
Composes are individual builds of a system image, based on a specific version of a particular blueprint. Compose as a term refers to the system image, the logs from its creation, inputs, metadata, and the process itself.

Customizations::
+
Customizations are specifications for the image that are not packages. This includes users, groups, and SSH keys.

