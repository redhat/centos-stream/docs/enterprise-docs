:_newdoc-version: 2.18.3
:_template-generated: 2024-07-18
:_mod-docs-content-type: PROCEDURE

[id="embedding-a-container_{context}"]
= Embedding a container

You can customize your blueprint to embed the latest RHEL container. The containers list contains objects with a source, and optionally, the `tls-verify` attribute. 

The container list entries describe the container images to be embedded into the image.

* `source` - Mandatory field. It is a reference to the container image at a registry. This example uses the `registry.access.redhat.com` registry. You can specify a tag version. The default tag version is latest.
* `name` - The name of the container in the local registry.
* `tls-verify` - Boolean field. The tls-verify boolean field controls the transport layer security. The default value is true.

The embedded containers do not start automatically. To start it, create `systemd` unit files or `quadlets` with the files customization.

* To embed a container from `registry.access.redhat.com/ubi9/ubi:latest` and a container from your host, add the following customization to your blueprint:
+
[subs="quotes,attributes"]
----
[[containers]]
source = "registry.access.redhat.com/ubi9/ubi:latest"
name =  "local-name"
tls-verify = true

[[containers]]
source = "localhost/test:latest"
local-storage = true
----

You can access protected container resources by using a `containers-auth.json` file. See link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html-single/composing_a_customized_rhel_system_image/index#con_the-container-registry-credentials_assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image[Container registry credentials].

