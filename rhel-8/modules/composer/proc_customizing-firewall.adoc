:_mod-docs-content-type: PROCEDURE

[id="customizing-firewall_{context}"]
= Customizing firewall

[role="_abstract"]
Set the firewall for the resulting system image. By default, the firewall blocks incoming connections, except for services that enable their ports explicitly, such as `sshd`. 

If you do not want to use the `[customizations.firewall]` or the `[customizations.firewall.services]`, either remove the attributes, or set them to an empty list []. If you only want to use the default firewall setup, you can omit the customization from the blueprint.

NOTE: The Google and OpenStack templates explicitly disable the firewall for their environment. You cannot override this behavior by setting the blueprint.

.Procedure

* Customize the blueprint with the following settings to open other ports and services:
+
[subs="quotes,attributes"]
----
[customizations.firewall]
ports = _["PORTS"]_
----
+
Where `ports` is an optional list of strings that contain ports or a range of ports and protocols to open. You can configure the ports by using the following format: `port:protocol` format. You can configure the port ranges by using the `portA-portB:protocol` format. For example:
+
[subs="quotes,attributes"]
----
[customizations.firewall]
ports = ["22:tcp", "80:tcp", "imap:tcp", "53:tcp", "53:udp", "30000-32767:tcp", "30000-32767:udp"]
----
+
You can use numeric ports, or their names from the `/etc/services` to enable or disable port lists.

* Specify which firewall services to enable or disable in the `customizations.firewall.service` section:
+
[subs="quotes,attributes"]
----
[customizations.firewall.services]
enabled = _["SERVICES"]_
disabled = _["SERVICES"]_
----
+
* You can check the available firewall services:
+
[subs="quotes,attributes"]
----
$ firewall-cmd --get-services
----
+
For example:
+
[subs="quotes,attributes"]
----
[customizations.firewall.services]
enabled = ["ftp", "ntp", "dhcp"]
disabled = ["telnet"]
----
+
NOTE: The services listed in `firewall.services` are different from the `service-names` available in the `/etc/services` file. 

