:_newdoc-version: 2.15.0
:_template-generated: 2024-3-15
:_mod-docs-content-type: PROCEDURE

[id="specifying-a-partition-mode_{context}"]
= Specifying a partition mode

[role="_abstract"]
Use the `partitioning_mode` variable to select how to partition the disk image that you are building. You can customize your image with the following supported modes:

* `auto-lvm`: It uses the raw partition mode, unless there are one or more filesystem customizations. In that case, it uses the LVM partition mode. 
* `lvm`: It always uses the LVM partition mode, even when there are no extra mountpoints. 
* `raw`: It uses raw partitions even when there are one or more mountpoints.

* You can customize your blueprint with the `partitioning_mode` variable by using the following customization:
+
[subs="quotes,attributes"]
----
[customizations]
partitioning_mode = "lvm"
----
