:_mod-docs-content-type: CONCEPT

[id="con_authentication-with-gcp_{context}"]
= How RHEL image builder sorts the authentication order of different GCP credentials

You can use several different types of credentials with RHEL image builder to authenticate with GCP. If RHEL image builder configuration is set to authenticate with GCP using multiple sets of credentials, it uses the credentials in the following order of preference:

. Credentials specified with the `composer-cli` command in the configuration file.
. Credentials configured in the `osbuild-composer` worker configuration.
. `Application Default Credentials` from the `Google GCP SDK` library, which tries to automatically find a way to authenticate by using the following options:
.. If the [replaceable]__GOOGLE_APPLICATION_CREDENTIALS__ environment variable is set, Application Default Credentials tries to load and use credentials from the file pointed to by the variable.
.. Application Default Credentials tries to authenticate by using the service account attached to the resource that is running the code. For example, Google Compute Engine VM.
+
NOTE: You must use the GCP credentials to determine which GCP project to upload the image to. Therefore, unless you want to upload all of your images to the same GCP project, you always must specify the credentials in the `gcp-config.toml` configuration file with the `composer-cli` command.


