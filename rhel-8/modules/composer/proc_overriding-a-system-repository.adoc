:_mod-docs-content-type: PROCEDURE

[id="overriding-a-system-repository_{context}"]

= Overriding a system repository

[role="_abstract"]
You can configure your own repository override for RHEL image builder in the `/etc/osbuild-composer/repositories` directory.

ifeval::[{ProductNumber} == 8]
NOTE: Prior to RHEL 8.5 release, the name of the repository overrides is `rhel-8.json`. Starting from RHEL 8.5, the names also respect the minor version: `rhel-84.json`, `rhel-85.json`, and so on.
endif::[]

.Prerequisites

* You have a custom repository that is accessible from your host system.

.Procedure

. Create the `/etc/osbuild-composer/repositories/` directory to store your repository overrides: 
+
[subs="quotes,attributes"]
----
$ sudo mkdir -p /etc/osbuild-composer/repositories
----

. Create a JSON file, using a name corresponding to your RHEL version. Alternatively, you can copy the file for your distribution from `/usr/share/osbuild-composer/` and modify its content. 
+
ifeval::[{ProductNumber} == 8]
For RHEL {ProductNumber}.9, use `/etc/osbuild-composer/repositories/rhel-{ProductNumber}9.json`.
endif::[]

ifeval::[{ProductNumber} == 9]
For RHEL {ProductNumber}.3, use `/etc/osbuild-composer/repositories/rhel-{ProductNumber}3.json`.
endif::[]


. Add the following structure to your JSON file. Specify only one of the following attributes, in the string format:  

* `baseurl` - The base URL of the repository. 
* `metalink` - The URL of a metalink file that contains a list of valid mirror repositories.
* `mirrorlist` - The URL of a mirrorlist file that contains a list of valid mirror repositories. The remaining fields, such as `gpgkey`, and `metadata_expire`, are optional. 
+
For example:
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
....
{
     "x86_64": [
        {
            "name": "baseos",
            "baseurl": "http://mirror.example.com/composes/released/RHEL-{ProductNumber}/{ProductNumber}.0/BaseOS/x86_64/os/",
            "gpgkey": "-----BEGIN PGP PUBLIC KEY BLOCK-----\n\n (…​)",
            "check_gpg": true            
        }
    ]
}
....
+
Alternatively, you can copy the JSON file for your distribution, by replacing `rhel-version.json` with your RHEL version, for example: rhel-{ProductNumber}.json. 
+
[subs="quotes,attributes"]
----
$  cp /usr/share/osbuild-composer/repositories/rhel-_version_.json /etc/osbuild-composer/repositories/
----

. Optional: Verify the JSON file:
+
[literal,subs="+quotes"]
----
$ json_verify < /etc/osbuild-composer/repositories/rhel-_version_.json
----

. Edit the `baseurl` paths in the `rhel-{ProductNumber}.json` file and save it. For example:
+
[subs="quotes,attributes"]
----
$ /etc/osbuild-composer/repositories/rhel-_version_.json
----

. Restart the `osbuild-composer.service`:
+
[subs="quotes,attributes"]
----
$ sudo systemctl restart osbuild-composer.service
----

.Verification

* Check if the repository points to the correct URLs: 
+
[subs="quotes,attributes"]
----
$ cat /etc/yum.repos.d/redhat.repo
----
+
You can see that the repository points to the correct URLs which are copied from the `/etc/yum.repos.d/redhat.repo` file. 

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/6957312[The latest RPMs version available in repository not visible for `osbuild-composer`.] (Red Hat Knowledgebase)
