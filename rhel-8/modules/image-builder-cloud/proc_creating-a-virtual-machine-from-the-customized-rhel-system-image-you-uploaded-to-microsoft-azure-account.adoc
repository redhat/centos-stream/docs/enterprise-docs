:_mod-docs-content-type: PROCEDURE

:experimental:

[id="creating-a-virtual-machine-from-the-customized-rhel-system-image-you-uploaded-to-microsoft-azure-account_{context}"]
= Creating a VM from the RHEL system image shared with your Microsoft Azure account

[role="_abstract"]
You can create a Virtual Machine (VM) from the image you shared with the Microsoft Azure Cloud account by using  Insights image builder.

.Prerequisites

* You must have a link:https://portal.azure.com/#create/Microsoft.StorageAccount[Microsoft Azure Storage Account] created.
* You must have uploaded the required image to the Microsoft Azure Cloud account.

.Procedure

. Click btn:[+ Create VM]. You are redirected to the *Create a virtual machine* dashboard.
+
. In the *Basic* tab under *Project Details*, your *Subscription* and the *Resource Group* are pre-set.
+
Optional: If you want to create a new resource Group:
+
.. Click btn:[Create new].
+
A pop-up prompts you to create the *Resource Group Name* container.
+
.. Insert a name and click btn:[OK].
+
If you want to keep the *Resource Group* that is already pre-set.
+
. Under *Instance Details*, insert:
.. *Virtual machine name*
.. *Region*
.. *Image* 
.. *Size*: Choose a VM size that better suits your needs.
+
Keep the remaining fields as in the default choice.
+
. Under *Administrator account*, enter the following details:
.. *Username*: the name of the account administrator.
.. *SSH public key source*: from the drop-down menu, select *Generate new key pair*.
.. *Key pair name*: insert a name for the key pair.
+
. Under *Inbound port rules*:
.. *Public inbound ports*: select *Allow selected ports*.
.. *Select inbound ports*: Use the default set *SSH (22)*.
+
. Click btn:[Review + Create]. You are redirected to the *Review + create* tab. You receive a confirmation that the validation passed.

. Review the details and click btn:[Create].
+
To change options, click btn:[Previous].
+
. A *Generates New Key Pair* pop-up opens. Click btn:[Download private key and create resources].
+
Save the key file in the *_yourKey_.pem* file format.
+
. After the deployment is complete, click btn:[Go to resource].
+
You are redirected to a new window with your VM details.

. Select the public IP address on the top right side of the page and copy it to your clipboard.

.Verification

Create an SSH connection to connect to the Virtual Machine you created. For that, follow the steps:

. Open a terminal.

. At your prompt, open an SSH connection to your virtual machine. Replace the IP address with the one from your VM, and replace the path to the `.pem` file with the path to where the key file was downloaded.
+
----
# ssh -i <yourKey.pem file location> <username>@<IP_address>
----
+
* Add the user name and replace the IP address with the one from your VM.
* Replace the path to the _.pem_ file with the path to where the key file was downloaded.
+
For example:
+
----
# ssh -i ./Downloads/yourKey.pem azureuser@10.111.12.123
----

. You are required to confirm if you want to continue to connect. Type `yes` to continue.
+
As a result, the output image you shared with the Microsoft Azure Storage account is started and ready to be provisioned.
+ 
NOTE: The default user is `azureuser` and the password is `azureuser`.


