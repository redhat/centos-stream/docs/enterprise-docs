:_mod-docs-content-type: PROCEDURE

:experimental:

[id="adding-additional-packages-during-the-image-creation_{context}"]
= Adding additional packages during the image creation

[role="_abstract"]
When creating a customized image using Insights image builder, you can add additional packages from the `BaseOS` and `AppStream` repositories. For that, follow the steps:

.Prerequisites

* You have an account on link:https://access.redhat.com/[Red Hat Customer Portal] with an Insights subscription.
* Access to the link:https://console.redhat.com/insights/image-builder/landing[Insights image builder] dashboard.
* You have already completed the following steps:
** Image output
** Target cloud environment
** Optionally, Registration


.Procedure

.  On the *Packages* page:

.. Type the name of the package you want to add to your image in the *Available packages* search bar.
+
Optionally, you can enter the first two letters of the package name to see the available package options. The packages are listed on the *Available packages* dual list box.

.. Click the package or packages you want to add.
... Click the btn:[>>] button to add all packages shown in the package search results to the *Chosen packages* dual list box.
+
Optionally, you can click the btn:[>] button to add only the selected packages.

.. After you have finished adding the additional packages, click btn:[Next].

. On the *Name image* page, enter a name for your image and click btn:[Next]. If you do not enter a name, you can find the image you created by its UUID.

. On the *Review* page, review the details about the image creation and click btn:[Create image].
+
After you complete the steps in the *Create image* wizard, the *Image Builder* dashboard is displayed. Insights image builder starts the compose of a RHEL image for the `x86_64` architecture. 
+
The Insights image builder Images dashboard opens. You can see details such as the Image UUID, the cloud target environment, the image operating system release and the status of the image creation.
+
NOTE: The image build, upload and cloud registration processes can take up to ten minutes to complete. 
