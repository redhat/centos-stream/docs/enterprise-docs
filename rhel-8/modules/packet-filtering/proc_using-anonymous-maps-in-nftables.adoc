:_mod-docs-content-type: PROCEDURE
:experimental:

[id="using-anonymous-maps-in-nftables_{context}"]
= Using anonymous maps in nftables

[role="_abstract"]
An anonymous map is a `{ __match_criteria__ : __action__ }` statement that you use directly in a rule. The statement can contain multiple comma-separated mappings.

The drawback of an anonymous map is that if you want to change the map, you must replace the rule. For a dynamic solution, use named maps as described in xref:using-named-maps-in-nftables_{context}[Using named maps in nftables].

For example, you can use an anonymous map to route both TCP and UDP packets of the IPv4 and IPv6 protocol to different chains to count incoming TCP and UDP packets separately.


.Procedure

. Create a new table:
+
[literal,subs="+quotes,attributes"]
----
# *nft add table inet example_table*
----

. Create the `tcp_packets` chain in `example_table`:
+
[literal,subs="+quotes,attributes"]
----
# *nft add chain inet example_table tcp_packets*
----

. Add a rule to `tcp_packets` that counts the traffic in this chain:
+
[literal,subs="+quotes,attributes"]
----
# *nft add rule inet example_table tcp_packets counter*
----

. Create the `udp_packets` chain in `example_table`
+
[literal,subs="+quotes,attributes"]
----
# *nft add chain inet example_table udp_packets*
----

. Add a rule to `udp_packets` that counts the traffic in this chain:
+
[literal,subs="+quotes,attributes"]
----
# *nft add rule inet example_table udp_packets counter*
----
. Create a chain for incoming traffic. For example, to create a chain named `incoming_traffic` in `example_table` that filters incoming traffic:
+
[literal,subs="+quotes,attributes"]
----
# *nft add chain inet example_table incoming_traffic { type filter hook input priority 0 \; }*
----

. Add a rule with an anonymous map to `incoming_traffic`:
+
[literal,subs="+quotes,attributes"]
----
# *nft add rule inet example_table incoming_traffic ip protocol vmap { tcp : jump tcp_packets, udp : jump udp_packets }*
----
+
The anonymous map distinguishes the packets and sends them to the different counter chains based on their protocol.

. To list the traffic counters, display `example_table`:
+
[literal,subs="+quotes,attributes"]
----
# *nft list table inet example_table*
table inet example_table {
  chain tcp_packets {
    *counter packets 36379 bytes 2103816*
  }

  chain udp_packets {
    *counter packets 10 bytes 1559*
  }

  chain incoming_traffic {
    type filter hook input priority filter; policy accept;
    ip protocol vmap { tcp : jump tcp_packets, udp : jump udp_packets }
  }
}
----
+
The counters in the `tcp_packets` and `udp_packets` chain display both the number of received packets and bytes.

