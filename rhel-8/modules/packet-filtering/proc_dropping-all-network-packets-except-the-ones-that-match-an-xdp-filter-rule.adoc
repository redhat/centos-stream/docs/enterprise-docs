:_mod-docs-content-type: PROCEDURE

// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="dropping-all-network-packets-except-the-ones-that-match-an-xdp-filter-rule_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Dropping all network packets except the ones that match an xdp-filter rule
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
You can use `xdp-filter` to allow only network packets:

* From and to a specific destination port
* From and to a specific IP address
* From and to specific MAC address

To do so, use the `deny` policy of `xdp-filter` which defines that the filter drops all network packets except the ones that match a particular rule. For example, use this method if you do not know the source IP addresses of packets you want to drop.

[WARNING]
====
If you set the default policy to `deny` when you load `xdp-filter` on an interface, the kernel immediately drops all packets from this interface until you create rules that allow certain traffic. To avoid being locked out from the system, enter the commands locally or connect through a different network interface to the host.
====



.Prerequisites

* The `xdp-tools` package is installed.
* You are logged in to the host either locally or using a network interface for which you do not plan to filter the traffic.
* A network driver that supports XDP programs.


.Procedure

. Load `xdp-filter` to process packets on a certain interface, such as `enp1s0`:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter load __enp1s0__ -p deny*
....
+
Optionally, use the `-f __feature__` option to enable only particular features, such as `tcp`, `ipv4`, or `ethernet`. Loading only the required features instead of all of them increases the speed of packet processing. To enable multiple features, separate them with a comma.
+
If the command fails with an error, the network driver does not support XDP programs.

. Add rules to allow packets that match them. For example:

** To allow packets to port `22`, enter:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter port __22__*
....
+
This command adds a rule that matches TCP and UDP traffic. To match only a particular protocol, pass the `-p __protocol__` option to the command.

** To allow packets to `192.0.2.1`, enter:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter ip __192.0.2.1__*
....
+
Note that `xdp-filter` does not support IP ranges.

** To allow packets to MAC address `00:53:00:AA:07:BE`, enter:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter ether __00:53:00:AA:07:BE__*
....

+
[IMPORTANT]
====
The `xdp-filter` utility does not support stateful packet inspection. This requires that you either do not set a mode using the `-m __mode__` option or you add explicit rules to allow incoming traffic that the machine receives in reply to outgoing traffic.
====



.Verification

* Use the following command to display statistics about dropped and allowed packets:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter status*
....

[role="_additional-resources"]
.Additional resources
* `xdp-filter(8)` man page on your system
* If you are a developer and you are interested in the code of `xdp-filter`, download and install the corresponding source RPM (SRPM) from the Red Hat Customer Portal.
