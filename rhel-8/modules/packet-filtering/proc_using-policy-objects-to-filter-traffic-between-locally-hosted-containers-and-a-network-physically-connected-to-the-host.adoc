:_mod-docs-content-type: PROCEDURE

[id="proc_using-policy-objects-to-filter-traffic-between-locally-hosted-containers-and-a-network-physically-connected-to-the-host_{context}"]
= Using policy objects to filter traffic between locally hosted containers and a network physically connected to the host

The policy objects feature allows users to filter traffic between Podman and firewalld zones.

[NOTE]
====
Red Hat recommends blocking all traffic by default and opening the selective services needed for the Podman utility.
====

.Procedure

. Create a new firewall policy:
[literal,subs="+quotes",options="nowrap"]
+
....
# *firewall-cmd --permanent --new-policy podmanToAny*
....

. Block all traffic from Podman to other zones and allow only necessary services on Podman:
[literal,subs="+quotes",options="nowrap"]
+
....
# *firewall-cmd --permanent --policy podmanToAny --set-target REJECT*
# *firewall-cmd --permanent --policy podmanToAny --add-service dhcp*
# *firewall-cmd --permanent --policy podmanToAny --add-service dns*
# *firewall-cmd --permanent --policy podmanToAny --add-service https*
....

. Create a new Podman zone:
[literal,subs="+quotes",options="nowrap"]
+
....
# *firewall-cmd --permanent --new-zone=podman*
....

. Define the ingress zone for the policy:
[literal,subs="+quotes",options="nowrap"]
+
....
# *firewall-cmd --permanent --policy podmanToHost --add-ingress-zone podman*
....

. Define the egress zone for all other zones:
[literal,subs="+quotes",options="nowrap"]
+
....
# *firewall-cmd --permanent --policy podmanToHost --add-egress-zone ANY*
....
+
Setting the egress zone to ANY means that you filter from Podman to other zones. If you want to filter to the host, then set the egress zone to HOST.


. Restart the firewalld service:
[literal,subs="+quotes",options="nowrap"]
+
....
# *systemctl restart firewalld*
....

.Verification

* Verify the Podman firewall policy to other zones:
[literal,subs="+quotes",options="nowrap"]
+
....
# *firewall-cmd --info-policy podmanToAny*
podmanToAny (active)
  ...
  target: REJECT
  ingress-zones: podman
  egress-zones: ANY
  services: dhcp dns https
  ...
....
