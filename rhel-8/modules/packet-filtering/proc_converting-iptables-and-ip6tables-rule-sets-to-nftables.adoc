:_mod-docs-content-type: PROCEDURE

[id="proc_converting-iptables-and-ip6tables-rule-sets-to-nftables_{context}"]
= Converting iptables and ip6tables rule sets to nftables
Use the `iptables-restore-translate` and `ip6tables-restore-translate` utilities to translate `iptables` and `ip6tables` rule sets to `nftables`.


.Prerequisites
* The `nftables` and `iptables` packages are installed.
* The system has `iptables` and `ip6tables` rules configured.


.Procedure

. Write the `iptables` and `ip6tables` rules to a file:
+
[literal,subs="+quotes"]
....
# **iptables-save >/root/iptables.dump**
# **ip6tables-save >/root/ip6tables.dump**
....

. Convert the dump files to `nftables` instructions:
+
[literal,subs="+quotes"]
....
# **iptables-restore-translate -f /root/iptables.dump > /etc/nftables/ruleset-migrated-from-iptables.nft**
# **ip6tables-restore-translate -f /root/ip6tables.dump > /etc/nftables/ruleset-migrated-from-ip6tables.nft**
....

. Review and, if needed, manually update the generated `nftables` rules.

. To enable the `nftables` service to load the generated files, add the following to the `/etc/sysconfig/nftables.conf` file:
+
[literal,subs="+quotes"]
....
**include "/etc/nftables/ruleset-migrated-from-iptables.nft"**
**include "/etc/nftables/ruleset-migrated-from-ip6tables.nft"**
....

. Stop and disable the `iptables` service:
+
[literal,subs="+quotes"]
....
# **systemctl disable --now iptables**
....
+
If you used a custom script to load the `iptables` rules, ensure that the script no longer starts automatically and reboot to flush all tables.

. Enable and start the `nftables` service:
+
[literal,subs="+quotes"]
....
# **systemctl enable --now nftables**
....


.Verification

* Display the `nftables` rule set:
+
[literal,subs="+quotes"]
....
# **nft list ruleset**
....


[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
ifdef::networking-title[]
* xref:automatically-loading-nftables-rules-when-the-system-boots_writing-and-executing-nftables-scripts[Automatically loading nftables rules when the system boots]
endif::[]
ifndef::networking-title[]
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/configuring_and_managing_networking/index#automatically-loading-nftables-rules-when-the-system-boots_writing-and-executing-nftables-scripts[Automatically loading nftables rules when the system boots]
endif::[]
endif::[]
ifeval::[{ProductNumber} >= 9]
ifdef::firewall-title[]
* xref:automatically-loading-nftables-rules-when-the-system-boots_writing-and-executing-nftables-scripts[Automatically loading nftables rules when the system boots]
endif::[]
ifndef::firewall-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_firewalls_and_packet_filters/getting-started-with-nftables_firewall-packet-filters#automatically-loading-nftables-rules-when-the-system-boots_writing-and-executing-nftables-scripts[Automatically loading nftables rules when the system boots]
endif::[]
endif::[]

