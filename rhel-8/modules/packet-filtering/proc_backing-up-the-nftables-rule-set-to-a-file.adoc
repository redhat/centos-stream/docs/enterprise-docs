:_mod-docs-content-type: PROCEDURE
:experimental:
[id="backing-up-the-nftables-rule-set-to-a-file_{context}"]
= Backing up the nftables rule set to a file

[role="_abstract"]
You can use the `nft` utility to back up the `nftables` rule set to a file.


.Procedure

* To backup `nftables` rules:
+
** In a format produced by `nft list ruleset` format:
+
[literal,subs="+quotes,attributes"]
----
# *nft list ruleset > _file_.nft*
----
** In JSON format:
+
[literal,subs="+quotes,attributes"]
----
# *nft -j list ruleset > _file_.json*
----
  
