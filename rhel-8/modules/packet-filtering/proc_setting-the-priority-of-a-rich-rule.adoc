:_mod-docs-content-type: PROCEDURE
:experimental:

[id="setting-the-priority-of-a-rich-rule_{context}"]
= Setting the priority of a rich rule

[role="_abstract"]
The following is an example of how to create a rich rule that uses the `priority` parameter to log all traffic that is not allowed or denied by other rules. You can use this rule to flag unexpected traffic.


.Procedure

* Add a rich rule with a very low precedence to log all traffic that has not been matched by other rules:
+
[literal,subs="+quotes,attributes"]
....
# *firewall-cmd --add-rich-rule='rule priority=32767 log prefix="UNEXPECTED: " limit value="5/m"'*
....
+
The command additionally limits the number of log entries to `5` per minute.


.Verification

* Display the `nftables` rule that the command in the previous step created:
+
[literal,subs="+quotes,attributes"]
....
# *nft list chain inet firewalld filter_IN_public_post*
table inet firewalld {
  chain filter_IN_public_post {
    log prefix "UNEXPECTED: " limit rate 5/minute
  }
}
....

