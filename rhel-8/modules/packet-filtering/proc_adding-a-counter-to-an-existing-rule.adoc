:_mod-docs-content-type: PROCEDURE
:experimental:
[id="adding-a-counter-to-an-existing-rule_{context}"]
= Adding a counter to an existing rule

[role="_abstract"]
To identify if a rule is matched, you can use a counter.

ifeval::[{ProductNumber} == 8]
ifdef::networking-title[]
* For more information about a procedure that adds a new rule with a counter, see xref:creating-a-rule-with-a-counter_{context}[Creating a rule with the counter].
endif::[]
ifndef::networking-title[]
* For more information about a procedure that adds a new rule with a counter, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/getting-started-with-nftables_configuring-and-managing-networking#creating-a-rule-with-a-counter_debugging-nftables-rules[Creating a rule with the counter] in `Configuring and managing networking`
endif::[]
endif::[]
ifeval::[{ProductNumber} == 9]
ifdef::firewall-title[]
* For more information about a procedure that adds a new rule with a counter, see xref:creating-a-rule-with-a-counter_{context}[Creating a rule with the counter].
endif::[]
ifndef::firewall-title[]
* For more information about a procedure that adds a new rule with a counter, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_firewalls_and_packet_filters/getting-started-with-nftables_firewall-packet-filters#creating-a-rule-with-a-counter_debugging-nftables-rules[Creating a rule with the counter].
endif::[]
endif::[]


.Prerequisites

* The rule to which you want to add the counter exists.


.Procedure

. Display the rules in the chain including their handles:
+
[literal,subs="+quotes,attributes"]
....
# *nft --handle list chain _inet_ _example_table_ _example_chain_*
table inet _example_table_ {
  chain _example_chain_ { # handle 1
    type filter hook input priority filter; policy accept;
    tcp dport ssh accept # *handle 4*
  }
}
....

. Add the counter by replacing the rule but with the `counter` parameter. The following example replaces the rule displayed in the previous step and adds a counter:
+
[literal,subs="+quotes,attributes"]
....
# *nft replace rule _inet_ _example_table_ _example_chain_ handle _4_ _tcp_ dport _22_ counter _accept_*
....
. To display the counter values:
+
[literal,subs="+quotes,attributes"]
....
# *nft list ruleset*
table _inet_ _example_table_ {
  chain _example_chain_ {
    type filter hook input priority filter; policy accept;
    tcp dport ssh *counter packets _6872_ bytes _105448565_* accept
  }
}
....
