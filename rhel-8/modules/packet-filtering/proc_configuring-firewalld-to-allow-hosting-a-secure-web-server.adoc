:_mod-docs-content-type: PROCEDURE

[id="configuring-firewalld-to-allow-hosting-a-secure-web-server_{context}"]
= Configuring firewalld to allow hosting a secure web server

Ports are logical services that enable an operating system to receive and distinguish network traffic and forward it to system services. The system services are represented by a daemon that listens on the port and waits for any traffic coming to this port.

Normally, system services listen on standard ports that are reserved for them. The [systemitem]`httpd` daemon, for example, listens on port 80. However, system administrators can directly specify the port number instead of the service name.

You can use the `firewalld` service to configure access to a secure web server for hosting your data.


.Prerequisites

* The `firewalld` service is running.


.Procedure

. Check the currently active firewall zone:
+
[literal,subs="+quotes"]
....
# **firewall-cmd --get-active-zones**
....

. Add the HTTPS service to the appropriate zone:
+
[literal,subs="+quotes"]
....
# **firewall-cmd --zone=__<zone_name>__ --add-service=https --permanent**
....


. Reload the firewall configuration:
+
[literal,subs="+quotes"]
....
# **firewall-cmd --reload**
....



.Verification

. Check if the port is open in `firewalld`:

** If you opened the port by specifying the port number, enter:
+
[literal,subs="+quotes"]
....
# **firewall-cmd --zone=__<zone_name>__ --list-all**
....

** If you opened the port by specifying a service definition, enter:
+
[literal,subs="+quotes"]
....
# **firewall-cmd --zone=__<zone_name>__ --list-services**
....

