:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
// assembly_controlling-ports-using-cli.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_closing-unused-or-unnecessary-ports-to-enhance-network-security.adoc[leveloffset=+1]

[id="closing-unused-or-unnecessary-ports-to-enhance-network-security_{context}"]
= Closing unused or unnecessary ports to enhance network security

[role="_abstract"]
When an open port is no longer needed, you can use the `firewalld` utility to close it.

[IMPORTANT]
====
Close all unnecessary ports to reduce the potential attack surface and minimize the risk of unauthorized access or exploitation of vulnerabilities.
====

.Procedure

. List all allowed ports:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --list-ports*
----
+
By default, this command lists the ports that are enabled in the default zone.
+
[NOTE]
====
This command will only give you a list of ports that are opened as ports. You will not be able to see any open ports that are opened as a service. For that case, consider using the `--list-all` option instead of `--list-ports`.
====


. Remove the port from the list of allowed ports to close it for the incoming traffic:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --remove-port=port-number/port-type*
----
+
This command removes a port from a zone. If you do not specify a zone, it will remove the port from the default zone.

. Make the new settings persistent:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --runtime-to-permanent*
----
+
Without specifying a zone, this command applies runtime changes to the permanent configuration of the default zone.

.Verification

. List the active zones and choose the zone you want to inspect:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --get-active-zones*
----

. List the currently open ports in the selected zone to check if the unused or unnecessary ports are closed:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --zone=__<zone_to_inspect>__ --list-ports*
----
