:_mod-docs-content-type: REFERENCE
:experimental:
// included in assembly_managing-icmp-requests.adoc

[id="configuring-the-icmp-filter-using-gui_{context}"]
= Configuring the ICMP filter using GUI

[role="_abstract"]
* To enable or disable an `ICMP` filter, start the *firewall-config* tool and select the network zone whose messages are to be filtered. Select the `ICMP Filter` tab and select the check box for each type of `ICMP` message you want to filter. Clear the check box to disable a filter. This setting is per direction and the default allows everything.

* To enable inverting the `ICMP Filter`, click the `Invert Filter` check box on the right. Only marked `ICMP` types are now accepted, all other are rejected. In a zone using the DROP target, they are dropped.

