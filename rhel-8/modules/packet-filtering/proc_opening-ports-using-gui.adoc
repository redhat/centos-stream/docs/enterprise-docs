:_mod-docs-content-type: PROCEDURE

:experimental:

[id="proc_opening-ports-using-gui_{context}"]
= Opening ports using GUI

[role="_abstract"]
To permit traffic through the firewall to a certain port, you can open the port in the GUI.


.Prerequisites

* You installed the `firewall-config` package


.Procedure

. Start the *firewall-config* tool and select the network zone whose settings you want to change.

. Select the `Ports` tab and click the btn:[Add] button on the right-hand side. The `Port and Protocol` window opens.

. Enter the port number or range of ports to permit.

. Select `tcp` or `udp` from the list.


