:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
// assembly_viewing-the-current-status-and-settings-of-firewalld.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_viewing-the-current-status-of-firewalld.adoc[leveloffset=+1]

[id="viewing-the-current-status-of-firewalld_{context}"]
= Viewing the current status of `firewalld`

[role="_abstract"]
The firewall service, `firewalld`, is installed on the system by default. Use the `firewalld` CLI interface to check that the service is running.

.Procedure

. To see the status of the service:
+
[literal,subs="+quotes"]
----
# *firewall-cmd --state*
----

. For more information about the service status, use the `systemctl status` sub-command:
+
[literal,subs="+quotes"]
----
# *systemctl status firewalld*
firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor pr
   Active: active (running) since Mon 2017-12-18 16:05:15 CET; 50min ago
     Docs: man:firewalld(1)
 Main PID: 705 (firewalld)
    Tasks: 2 (limit: 4915)
   CGroup: /system.slice/firewalld.service
           └─705 /usr/bin/python3 -Es /usr/sbin/firewalld --nofork --nopid
----


