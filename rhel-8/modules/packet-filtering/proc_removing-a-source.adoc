:_mod-docs-content-type: PROCEDURE
:experimental:
// included in assembly_using-and-configuring-firewalld.adoc

[id="removing-a-source_{context}"]
= Removing a source

[role="_abstract"]
When you remove a source from a zone, the traffic which originates from the source is no longer directed through the rules specified for that source. Instead, the traffic falls back to the rules and settings of the zone associated with the interface from which it originates, or goes to the default zone.

.Procedure

. List allowed sources for the required zone:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --zone=zone-name --list-sources*
----

. Remove the source from the zone permanently:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --zone=zone-name --remove-source=<source>*
----

. Make the new settings persistent:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --runtime-to-permanent*
----
  
