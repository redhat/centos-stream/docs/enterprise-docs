:_mod-docs-content-type: PROCEDURE
:experimental:

[id="controlling-traffic-with-predefined-services-using-gui_{context}"]
= Controlling traffic with predefined services using GUI

[role="_abstract"]
You can control the network traffic with predefined services using graphical user interface.


.Prerequisites

* You installed the `firewall-config` package


.Procedure

. To enable or disable a predefined or custom service:

.. Start the *firewall-config* tool and select the network zone whose services are to be configured.

.. Select the `Zones` tab and then the `Services` tab below.

.. Select the check box for each type of service you want to trust or clear the check box to block a service in the selected zone.

. To edit a service:

.. Start the *firewall-config* tool.

.. Select `Permanent` from the menu labeled `Configuration`. Additional icons and menu buttons appear at the bottom of the btn:[Services] window.

.. Select the service you want to configure.

The `Ports`, `Protocols`, and `Source Port` tabs enable adding, changing, and removing of ports, protocols, and source port for the selected service. The modules tab is for configuring *Netfilter* helper modules. The `Destination` tab enables limiting traffic to a particular destination address and Internet Protocol (`IPv4` or `IPv6`).

[NOTE]
====
It is not possible to alter service settings in the `Runtime` mode.
====
  
