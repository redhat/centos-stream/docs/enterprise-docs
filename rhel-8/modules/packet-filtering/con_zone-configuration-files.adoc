:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
// assembly_working-with-firewalld-zones.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/con_zone-configuration-files.adoc[leveloffset=+1]

[id="zone-configuration-files_{context}"]
= Zone configuration files

[role="_abstract"]

A `firewalld` zone configuration file contains the information for a zone. These are the zone description, services, ports, protocols, icmp-blocks, masquerade, forward-ports and rich language rules in an XML file format. The file name has to be `pass:attributes[{blank}]_zone-name_.xml` where the length of _zone-name_ is currently limited to 17 chars. The zone configuration files are located in the `/usr/lib/firewalld/zones/` and `/etc/firewalld/zones/` directories.

The following example shows a configuration that allows one service (`SSH`) and one port range, for both the `TCP` and `UDP` protocols:

[source,xml]
----
<?xml version="1.0" encoding="utf-8"?>
<zone>
  <short>My Zone</short>
  <description>Here you can describe the characteristic features of the zone.</description>
  <service name="ssh"/>
  <port protocol="udp" port="1025-65535"/>
  <port protocol="tcp" port="1025-65535"/>
</zone>
----


[role="_additional-resources"]
.Additional resources
* `firewalld.zone` manual page
