:_mod-docs-content-type: PROCEDURE

[id="proc_converting-single-iptables-and-ip6tables-rules-to-nftables_{context}"]
= Converting single iptables and ip6tables rules to nftables
{RHEL} provides the `iptables-translate` and `ip6tables-translate` utilities to convert an `iptables` or `ip6tables` rule into the equivalent one for `nftables`.


.Prerequisites

* The `nftables` package is installed.


.Procedure

* Use the `iptables-translate` or `ip6tables-translate` utility instead of `iptables` or `ip6tables` to display the corresponding `nftables` rule, for example:
+
[literal,subs="+quotes"]
....
# **iptables-translate __-A INPUT -s 192.0.2.0/24 -j ACCEPT__**
__nft add rule ip filter INPUT ip saddr 192.0.2.0/24 counter accept__
....
+
Note that some extensions lack translation support. In these cases, the utility prints the untranslated rule prefixed with the `#` sign, for example:
+
[literal,subs="+quotes"]
....
# **iptables-translate __-A INPUT -j CHECKSUM --checksum-fill__**
nft # __-A INPUT -j CHECKSUM --checksum-fill__
....


[role="_additional-resources"]
.Additional resources
* `iptables-translate --help`

