:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="configuring-masquerading-using-nftables_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Configuring masquerading using nftables

[role="_abstract"]
Masquerading enables a router to dynamically change the source IP of packets sent through an interface to the IP address of the interface. This means that if the interface gets a new IP assigned, `nftables` automatically uses the new IP when replacing the source IP.

Replace the source IP of packets leaving the host through the `ens3` interface to the IP set on `ens3`.

.Procedure

. Create a table:
+
[literal,subs="+quotes,attributes"]
....
# *nft add table nat*
....

. Add the `prerouting` and `postrouting` chains to the table:
+
[literal,subs="+quotes,attributes"]
....
# *nft add chain nat postrouting { type nat hook postrouting priority 100 \; }*
....
+
[IMPORTANT]
====
Even if you do not add a rule to the `prerouting` chain, the `nftables` framework requires this chain to match incoming packet replies.
====
+
Note that you must pass the `--` option to the `nft` command to prevent the shell from interpreting the negative priority value as an option of the `nft` command.

. Add a rule to the `postrouting` chain that matches outgoing packets on the `ens3` interface:
+
[literal,subs="+quotes,attributes"]
....
# *nft add rule nat postrouting oifname "__ens3__" masquerade*
....
