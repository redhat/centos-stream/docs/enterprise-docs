:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
// assembly_controlling-network-traffic-using-firewalld.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_controlling-traffic-with-predefined-services-using-cli.adoc[leveloffset=+1]

[id="controlling-traffic-with-predefined-services-using-the-cli_{context}"]
= Controlling traffic with predefined services using the CLI

[role="_abstract"]
The most straightforward method to control traffic is to add a predefined service to `firewalld`. This opens all necessary ports and modifies other settings according to the _service definition file_.

.Prerequisites

* The `firewalld` service is running.


.Procedure

. Check that the service in `firewalld` is not already allowed:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --list-services*
ssh dhcpv6-client
----
+
The command lists the services that are enabled in the default zone.

. List all predefined services in `firewalld`:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --get-services*
RH-Satellite-6 amanda-client amanda-k5-client bacula bacula-client bitcoin bitcoin-rpc bitcoin-testnet bitcoin-testnet-rpc ceph ceph-mon cfengine condor-collector ctdb dhcp dhcpv6 dhcpv6-client dns docker-registry ...
----
+
The command displays a list of available services for the default zone.

. Add the service to the list of services that `firewalld` allows:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --add-service=__<service_name>__*
----
+
The command adds the specified service to the default zone.

. Make the new settings persistent:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --runtime-to-permanent*
----
+
The command applies these runtime changes to the permanent configuration of the firewall. By default, it applies these changes to the configuration of the default zone.

.Verification

. List all permanent firewall rules:
+
[literal,subs="+quotes,attributes"]
....
# *firewall-cmd --list-all --permanent*
public
  target: default
  icmp-block-inversion: no
  interfaces: 
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  forward: no
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
....
+
The command displays complete configuration with the permanent firewall rules of the default firewall zone (`public`).


. Check the validity of the permanent configuration of the `firewalld` service.
+
[literal,subs="+quotes,attributes"]
....
# *firewall-cmd --check-config*
success
....
+
If the permanent configuration is invalid, the command returns an error with further details:
+
[literal,subs="+quotes,attributes"]
....
# *firewall-cmd --check-config*
Error: INVALID_PROTOCOL: 'public.xml': 'tcpx' not from {'tcp'|'udp'|'sctp'|'dccp'}
....
+
You can also manually inspect the permanent configuration files to verify the settings. The main configuration file is `/etc/firewalld/firewalld.conf`. The zone-specific configuration files are in the `/etc/firewalld/zones/` directory and the policies are in the `/etc/firewalld/policies/` directory.

