:_mod-docs-content-type: PROCEDURE

// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="dropping-network-packets-that-match-an-xdp-filter-rule_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Dropping network packets that match an xdp-filter rule
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
You can use `xdp-filter` to drop network packets:

* To a specific destination port
* From a specific IP address
* From a specific MAC address

The `allow` policy of `xdp-filter` defines that all traffic is allowed and the filter drops only network packets that match a particular rule. For example, use this method if you know the source IP addresses of packets you want to drop.




.Prerequisites

* The `xdp-tools` package is installed.
* A network driver that supports XDP programs.



.Procedure

. Load `xdp-filter` to process incoming packets on a certain interface, such as `enp1s0`:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter load __enp1s0__*
....
+
By default, `xdp-filter` uses the `allow` policy, and the utility drops only traffic that matches any rule.
+
Optionally, use the `-f __feature__` option to enable only particular features, such as `tcp`, `ipv4`, or `ethernet`. Loading only the required features instead of all of them increases the speed of packet processing. To enable multiple features, separate them with a comma.
+
If the command fails with an error, the network driver does not support XDP programs.

. Add rules to drop packets that match them. For example:

** To drop incoming packets to port `22`, enter:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter port __22__*
....
+
This command adds a rule that matches TCP and UDP traffic. To match only a particular protocol, use the `-p __protocol__` option.

** To drop incoming packets from `192.0.2.1`, enter:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter ip __192.0.2.1__ -m __src__*
....
+
Note that `xdp-filter` does not support IP ranges.

** To drop incoming packets from MAC address `00:53:00:AA:07:BE`, enter:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter ether __00:53:00:AA:07:BE__ -m __src__*
....



.Verification

* Use the following command to display statistics about dropped and allowed packets:
+
[literal,subs="+quotes,attributes"]
....
# *xdp-filter status*
....


[role="_additional-resources"]
.Additional resources
* `xdp-filter(8)` man page on your system
* If you are a developer and interested in the code of `xdp-filter`, download and install the corresponding source RPM (SRPM) from the Red Hat Customer Portal.
