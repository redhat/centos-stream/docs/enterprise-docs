:_mod-docs-content-type: CONCEPT

[id="con_basics-of-nftables-rules_{context}"]
= Basics of nftables rules

Rules define actions to perform on packets that pass a chain that contains this rule. If the rule also contains matching expressions, `nftables` performs the actions only if all previous expressions apply.

If you want to add a rule to a chain, the format to use depends on your firewall script:

* In scripts in native syntax, use:
+
[literal,subs="+quotes"]
....
table __<table_address_family>__ __<table_name>__ {
  chain __<chain_name>__ {
    type __<type>__ hook __<hook>__ priority __<priority>__ ; policy __<policy>__ ;
      **__<rule>__**
  }
}
....

* In shell scripts, use:
+
[literal,subs="+quotes"]
....
**nft add rule __<table_address_family>__ __<table_name>__ __<chain_name>__ __<rule>__**
....
+
This shell command appends the new rule at the end of the chain. If you prefer to add a rule at the beginning of the chain, use the `nft insert` command instead of `nft add`.

