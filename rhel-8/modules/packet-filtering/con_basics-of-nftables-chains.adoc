:_mod-docs-content-type: CONCEPT

[id="con_basics-of-nftables-chains_{context}"]
= Basics of nftables chains

Tables consist of chains which in turn are containers for rules. The following two rule types exists:

* *Base chain*: You can use base chains as an entry point for packets from the networking stack.
* *Regular chain*: You can use regular chains as a `jump` target to better organize rules.

If you want to add a base chain to a table, the format to use depends on your firewall script:

* In scripts in native syntax, use:
+
[literal,subs="+quotes"]
....
table __<table_address_family>__ __<table_name>__ {
  **chain __<chain_name>__ {**
    **type __<type>__ hook __<hook>__ priority __<priority>__**
    **policy __<policy>__ ;**
  **}**
}
....

* In shell scripts, use:
+
[literal,subs="+quotes"]
....
**nft add chain __<table_address_family>__ __<table_name>__ __<chain_name>__ { type __<type>__ hook __<hook>__ priority __<priority>__ \; policy __<policy>__ \; }**
....
+
To avoid that the shell interprets the semicolons as the end of the command, place the `\` escape character in front of the semicolons.

Both examples create *base chains*. To create a *regular chain*, do not set any parameters in the curly brackets.


[discrete]
== Chain types

The following are the chain types and an overview with which address families and hooks you can use them:

[options="header"]
[cols="1,2,2,4"]
|====
| Type | Address families | Hooks | Description
| `filter` | all | all | Standard chain type
| `nat` | `ip`, `ip6`, `inet` | `prerouting`, `input`, `output`, `postrouting` | Chains of this type perform native address translation based on connection tracking entries. Only the first packet traverses this chain type.
| `route` | `ip`, `ip6` | `output` | Accepted packets that traverse this chain type cause a new route lookup if relevant parts of the IP header have changed.
|====


[discrete]
== Chain priorities

The priority parameter specifies the order in which packets traverse chains with the same hook value. You can set this parameter to an integer value or use a standard priority name.

The following matrix is an overview of the standard priority names and their numeric values, and with which address families and hooks you can use them:

[options="header"]
[cols="2,2,3,3"]
|====
| Textual value | Numeric value | Address families |Hooks
| `raw` | `-300` | `ip`, `ip6`, `inet` | all
| `mangle` | `-150` | `ip`, `ip6`, `inet` | all
.2+| `dstnat` | `-100` | `ip`, `ip6`, `inet` | `prerouting`
| `-300` | `bridge` | `prerouting`
.2+| `filter` | `0` | `ip`, `ip6`, `inet`, `arp`, `netdev` | all
| `-200` | `bridge` | all
| `security` | `50` | `ip`, `ip6`, `inet` | all
.2+| `srcnat` | `100` |`ip`, `ip6`, `inet` | `postrouting`
| `300` | `bridge` | `postrouting`
| `out` | `100` | `bridge` | `output`
|====


[discrete]
== Chain policies

The chain policy defines whether `nftables` should accept or drop packets if rules in this chain do not specify any action. You can set one of the following policies in a chain:

* `accept` (default)
* `drop`

