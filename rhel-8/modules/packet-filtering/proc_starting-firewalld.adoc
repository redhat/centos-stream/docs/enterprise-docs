:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
// assembly_using-and-configuring-firewalld.adoc
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_starting-firewalld.adoc[leveloffset=+1]

[id="starting-firewalld_{context}"]
= Starting firewalld

[role="_abstract"]
.Procedure

. To start `firewalld`, enter the following command as `root`:
+
[literal,subs="+quotes"]
----
# *systemctl unmask firewalld*
# *systemctl start firewalld*
----
+
. To ensure `firewalld` starts automatically at system start, enter the following command as `root`:
+
[literal,subs="+quotes"]
----
# *systemctl enable firewalld*
----
  
