:_mod-docs-content-type: PROCEDURE

// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

[id="proc_viewing-firewalld-settings-using-cli_{context}"]
= Viewing firewalld settings using CLI
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
With the CLI client, it is possible to get different views of the current firewall settings. The `--list-all` option shows a complete overview of the `firewalld` settings.

`firewalld` uses zones to manage the traffic. If a zone is not specified by the `--zone` option, the command is effective in the default zone assigned to the active network interface and connection.

.Procedure

* To list all the relevant information for the default zone:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --list-all*
public
  target: default
  icmp-block-inversion: no
  interfaces:
  sources:
  services: ssh dhcpv6-client
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
----

* To specify the zone for which to display the settings, add the `--zone=pass:attributes[{blank}]_zone-name_pass:attributes[{blank}]` argument to the `firewall-cmd --list-all` command, for example:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --list-all --zone=home*
home
  target: default
  icmp-block-inversion: no
  interfaces:
  sources:
  services: ssh mdns samba-client dhcpv6-client
  ...
----

* To see the settings for particular information, such as services or ports, use a specific option. See the `firewalld` manual pages or get a list of the options using the command help:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --help*
----

* To see which services are allowed in the current zone:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --list-services*
ssh dhcpv6-client
----

[NOTE]
====
Listing the settings for a certain subpart using the CLI tool can sometimes be difficult to interpret. For example, you allow the `SSH` service and `firewalld` opens the necessary port (22) for the service. Later, if you list the allowed services, the list shows the `SSH` service, but if you list open ports, it does not show any. Therefore, it is recommended to use the `--list-all` option to make sure you receive a complete information.
====

