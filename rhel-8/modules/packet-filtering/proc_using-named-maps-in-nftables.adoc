:_mod-docs-content-type: PROCEDURE
:experimental:
[id="using-named-maps-in-nftables_{context}"]
= Using named maps in nftables

[role="_abstract"]
The `nftables` framework supports named maps. You can use these maps in multiple rules within a table. Another benefit over anonymous maps is that you can update a named map without replacing the rules that use it.

When you create a named map, you must specify the type of elements:

* `ipv4_addr` for a map whose match part contains an IPv4 address, such as `192.0.2.1`.
* `ipv6_addr` for a map whose match part contains an IPv6 address, such as `2001:db8:1::1`.
* `ether_addr` for a map whose match part contains a media access control (MAC) address, such as `52:54:00:6b:66:42`.
* `inet_proto` for a map whose match part contains an internet protocol type, such as `tcp`.
* `inet_service` for a map whose match part contains an internet services name port number, such as `ssh` or `22`.
* `mark` for a map whose match part contains a packet mark. A packet mark can be any positive 32-bit integer value (`0` to `2147483647`).
* `counter` for a map whose match part contains a counter value. The counter value can be any positive 64-bit integer value.
* `quota` for a map whose match part contains a quota value. The quota value can be any positive 64-bit integer value.

For example, you can allow or drop incoming packets based on their source IP address. Using a named map, you require only a single rule to configure this scenario while the IP addresses and actions are dynamically stored in the map.


.Procedure

. Create a table. For example, to create a table named `example_table` that processes IPv4 packets:
+
[literal,subs="+quotes,attributes"]
----
# *nft add table ip __example_table__*
----

. Create a chain. For example, to create a chain named `example_chain` in `example_table`:
+
[literal,subs="+quotes,attributes"]
----
# *nft add chain ip __example_table__ __example_chain__ { type filter hook __input__ priority 0 \; }*
----
+
[IMPORTANT]
====
To prevent the shell from interpreting the semicolons as the end of the command, you must escape the semicolons with a backslash.
====

. Create an empty map. For example, to create a map for IPv4 addresses:
+
[literal,subs="+quotes,attributes"]
----
# *nft add map ip __example_table__ __example_map__ { type ipv4_addr : verdict \; }*
----

. Create rules that use the map. For example, the following command adds a rule to `example_chain` in `example_table` that applies actions to IPv4 addresses which are both defined in `example_map`:
+
[literal,subs="+quotes,attributes"]
----
# *nft add rule __example_table__ __example_chain__ ip saddr vmap @__example_map__*
----

. Add IPv4 addresses and corresponding actions to `example_map`:
+
[literal,subs="+quotes,attributes"]
----
# *nft add element ip __example_table__ __example_map__ { 192.0.2.1 : accept, 192.0.2.2 : drop }*
----
+
This example defines the mappings of IPv4 addresses to actions. In combination with the rule created above, the firewall accepts packet from `192.0.2.1` and drops packets from `192.0.2.2`.

. Optional: Enhance the map by adding another IP address and action statement:
+
[literal,subs="+quotes,attributes"]
----
# *nft add element ip __example_table__ __example_map__ { 192.0.2.3 : accept }*
----

. Optional: Remove an entry from the map:
+
[literal,subs="+quotes,attributes"]
----
# *nft delete element ip __example_table__ __example_map__ { 192.0.2.1 }*
----

. Optional: Display the rule set:
+
[literal,subs="+quotes,attributes"]
----
# *nft list ruleset*
table ip example_table {
  map example_map {
    type ipv4_addr : verdict
    elements = { 192.0.2.2 : drop, 192.0.2.3 : accept }
  }

  chain example_chain {
    type filter hook input priority filter; policy accept;
    ip saddr vmap @example_map
  }
}
----
  
