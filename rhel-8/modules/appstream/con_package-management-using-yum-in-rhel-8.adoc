:_mod-docs-content-type: CONCEPT
[id="package-management-using-yum-in-rhel-8_{context}"]
= Software management tools in Red Hat Enterprise Linux 8

// User Story: As a sysadmin, I need to know what is available through the AppStream so I can determine what RPMs, SCLs, and modules to download and install.

// User Story: As a sysadmin, I need to know what is available through the AppStream so I can determine what RPMs, SCLs, and modules to download and install.

In {ProductName} ({ProductShortName}) {ProductNumber}, use *{PackageManagerName}* to manage software. *{PackageManagerName}* is based on the [application]*DNF* technology, which adds support for modular features.

NOTE: Upstream documentation identifies the technology as [application]*DNF*, and the tool is referred to as [application]*DNF*. As a result, some output returned by the new *YUM* tool in {ProductShortName} {ProductNumber} mentions [application]*DNF*. 

Although *{PackageManagerName}* is based on [application]*DNF*, it is compatible with *{PackageManagerName}* used in RHEL 7. For software installation, the [command]`{PackageManagerCommand}` command and most of its options work the same way in {ProductShortName} {ProductNumber} as they did in RHEL 7.

Selected [application]*{PackageManagerName}* plug-ins and utilities have been ported to the new [application]*DNF* back end and can be installed under the same names as in RHEL 7. Packages also provide compatibility symlinks. Therefore, you can find binaries, configuration files, and directories in usual locations.

NOTE: The legacy Python API provided by *{PackageManagerName}* in RHEL 7 is no longer available. You can migrate your plug-ins and scripts to the new DNF Python API provided by *{PackageManagerName}* in {ProductShortName} {ProductNumber}. For more information, see link:https://dnf.readthedocs.io/en/latest/api.html[DNF API Reference].

// docs note - we are forbidden from mentioning "yum v4" in any forms, use "yum based on dnf tech in rhel8" vs. "yum v3 from rhel7"
// also prefer YUM uppercase when referring to name, not tool
// resource http://dnf.readthedocs.io/en/latest/cli_vs_yum.html


// docs note - we are forbidden from mentioning "yum v4" in any forms, use "yum based on dnf tech in rhel8" vs. "yum v3 from rhel7"
// also prefer YUM uppercase when referring to name, not tool
// resource http://dnf.readthedocs.io/en/latest/cli_vs_yum.html
