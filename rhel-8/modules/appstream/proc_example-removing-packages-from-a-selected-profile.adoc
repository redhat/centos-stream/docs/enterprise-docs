:_mod-docs-content-type: PROCEDURE

[id="example-removing-packages-from-a-selected-profile_{context}"]

.Removing packages from a selected profile

====


The following is an example of how to remove packages and their dependencies that belong to the `devel` profile of the `php:7.3` module stream.

NOTE: The outputs in this example have been edited for brevity. Actual outputs might contain more information than shown here. 

.Procedure

. Install the `php:7.3` module stream, including all available profiles:
+
[subs="quotes,attributes"]
----
# *yum module install php:7.3/**
Updating Subscription Management repositories.
Last metadata expiration check: 0:08:41 ago on Tue Mar  3 11:32:05 2020.
Dependencies resolved.
=========================================================================
 Package          Arch   Version                              Repository                        Size
=========================================================================
Installing group/module packages:
 libzip           x86_64 1.5.2-1.module+el8.1.0+3189+a1bff096 rhel-8-for-x86_64-appstream-rpms  63 k
 php-cli          x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6 rhel-8-for-x86_64-appstream-rpms 3.0 M
 php-common       x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6 rhel-8-for-x86_64-appstream-rpms 663 k
 php-devel        x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6 rhel-8-for-x86_64-appstream-rpms 735 k
 php-fpm          x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6 rhel-8-for-x86_64-appstream-rpms 1.6 M
 php-json         x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6 rhel-8-for-x86_64-appstream-rpms  73 k
 php-mbstring     x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6 rhel-8-for-x86_64-appstream-rpms 610 k
 php-pear         noarch 1:1.10.9-1.module+el8.1.0+3189+a1bff096
                                                              rhel-8-for-x86_64-appstream-rpms 359 k
 php-pecl-zip     x86_64 1.15.4-1.module+el8.1.0+3189+a1bff096
                                                              rhel-8-for-x86_64-appstream-rpms  51 k
 php-process      x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6 rhel-8-for-x86_64-appstream-rpms  84 k
 php-xml          x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6 rhel-8-for-x86_64-appstream-rpms 188 k
Installing dependencies:
 autoconf         noarch 2.69-27.el8                          rhel-8-for-x86_64-appstream-rpms 710 k
...
Installing weak dependencies:
 perl-IO-Socket-IP
                  noarch 0.39-5.el8                           rhel-8-for-x86_64-appstream-rpms  47 k
...
Installing module profiles:
 php/common
 php/devel
 php/minimal
Enabling module streams:
 httpd                   2.4
 nginx                   1.14
 php                     7.3

Transaction Summary
=========================================================================
Install  73 Packages

Total download size: 76 M
Installed size: 220 M
Is this ok [y/N]: *y*
----
+

. Inspect the installed profiles:
+
[subs="quotes,attributes"]
----
$ *yum module info php*
...
Name             : php
Stream           : 7.3 [e] [a]
Version          : 8020020200715124551
Context          : ceb1cf90
Architecture     : x86_64
Profiles         : common [d] [i], devel [i], minimal [i]
Default profiles : common
Repo             : rhel-AppStream
....
Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled, [a]ctive
----
+
All profiles are installed as indicated in the output.

. Remove packages from the `devel` profile:
+
[subs="quotes,attributes"]
----
# *yum module remove php:7.3/devel*
Updating Subscription Management repositories.
Last metadata expiration check: 0:09:40 ago on Tue Mar  3 11:32:05 2020.
Dependencies resolved.
=========================================================================
 Package                Arch   Version                       Repository                         Size
=========================================================================
Removing:
 libzip                 x86_64 1.5.2-1.module+el8.1.0+3189+a1bff096
                                                             @rhel-8-for-x86_64-appstream-rpms 313 k
 php-devel              x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6
                                                             @rhel-8-for-x86_64-appstream-rpms 5.3 M
 php-pear               noarch 1:1.10.9-1.module+el8.1.0+3189+a1bff096
                                                             @rhel-8-for-x86_64-appstream-rpms 2.1 M
 php-pecl-zip           x86_64 1.15.4-1.module+el8.1.0+3189+a1bff096
                                                             @rhel-8-for-x86_64-appstream-rpms 119 k
 php-process            x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6
                                                             @rhel-8-for-x86_64-appstream-rpms 117 k
Removing unused dependencies:
 autoconf               noarch 2.69-27.el8                   @rhel-8-for-x86_64-appstream-rpms 2.2 M
...
Disabling module profiles:
 php/devel

Transaction Summary
=========================================================================
Remove  64 Packages

Freed space: 193 M
Is this ok [y/N]: *y*
----

. Inspect the installed profiles after the removal:
+
[subs="quotes,attributes"]
----
$ *yum module info php*
...
Name             : php
Stream           : 7.3 [e] [a]
Version          : 8020020200715124551
Context          : ceb1cf90
Architecture     : x86_64
Profiles         : common [d] [i], devel, minimal [i]
Default profiles : common
Repo             : rhel-AppStream
...
Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled, [a]ctive
----
+
All profiles except `devel` are currently installed. 
====
