:_mod-docs-content-type: PROCEDURE
[id="removing-installed-packages_{context}"]
= Removing installed packages

You can use *{PackageManagerName}* to remove a single package or multiple packages installed on your system. If any of the packages you choose to remove have unused dependencies, *{PackageManagerName}* uninstalls these dependencies as well. 

.Procedure

* Remove particular packages:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} remove _<package_name_1>_ _<package_name_2>_ ...*
----