:_mod-docs-content-type: PROCEDURE
[id="removing-packages-from-an-installed-profle_{context}"]
= Removing packages from an installed profile

When you remove packages installed with a profile, all packages with a name corresponding to the packages installed by the profile are removed. This includes their dependencies, with the exception of packages required by a different profile.

To remove all packages from a selected stream, complete the steps in xref:removing-all-packages-from-a-module-stream_removing-installed-modular-content[Removing all packages from a module stream]. 

.Prerequisites

* The selected profile is installed by using the `{PackageManagerCommand} module install _module_name_:__stream__/_profile_` command or as a default profile by using the `{PackageManagerCommand} install _module_name_:__stream__` command.


.Procedure

* Uninstall packages that belong to the selected profile:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} module remove _<module_name>_:__<stream>__/_<profile>_*
----
+
For example, to remove packages from the `devel` profile of the `php:7.3` module stream, enter:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} module remove php:7.3/devel*
(...)
Dependencies resolved.
=========================================================================
 Package                Arch   Version                       Repository                         Size
=========================================================================
*Removing:*
 libzip                 x86_64 1.5.2-1.module+el8.1.0+3189+a1bff096
                                                             @rhel-8-for-x86_64-appstream-rpms 313 k
 php-devel              x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6
                                                             @rhel-8-for-x86_64-appstream-rpms 5.3 M
 php-pear               noarch 1:1.10.9-1.module+el8.1.0+3189+a1bff096
                                                             @rhel-8-for-x86_64-appstream-rpms 2.1 M
 php-pecl-zip           x86_64 1.15.4-1.module+el8.1.0+3189+a1bff096
                                                             @rhel-8-for-x86_64-appstream-rpms 119 k
 php-process            x86_64 7.3.5-5.module+el8.1.0+4560+e0eee7d6
                                                             @rhel-8-for-x86_64-appstream-rpms 117 k
*Removing unused dependencies:*
 autoconf               noarch 2.69-27.el8                   @rhel-8-for-x86_64-appstream-rpms 2.2 M
...
Disabling module profiles:
 php/devel

Transaction Summary
=========================================================================
Remove  64 Packages

Freed space: 193 M
Is this ok [y/N]: *y*
----
+
WARNING: Check the list of packages under `Removing:` and `Removing unused dependencies:` before you proceed with the removal transaction. This transaction removes requested packages, unused dependencies, and dependent packages, which might result in the system failure. 

+
Alternatively, uninstall packages from all installed profiles within a stream:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} module remove _<module_name>_:__<stream>__*
----
+
NOTE: These operations will not remove packages from the stream that do not belong to any of the profiles.


.Verification

*  Verify that the correct profile was removed: 
+
[literal,subs="+quotes,attributes"]
----
$ *yum module info php*
...
Name             : php
Stream           : 7.3 [e] [a]
Version          : 8020020200715124551
Context          : ceb1cf90
Architecture     : x86_64
Profiles         : common [d] *[i]*, devel, minimal *[i]*
Default profiles : common
Repo             : rhel-AppStream
...
Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled, [a]ctive
----
+
All profiles except `devel` are currently installed (`[i]`). 




[role="_additional-resources"]
.Additional resources
* xref:modular-dependencies-and-stream-changes_managing-versions-of-appstream-content[Modular dependencies and stream changes]
