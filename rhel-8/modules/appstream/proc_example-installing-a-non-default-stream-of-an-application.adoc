:_mod-docs-content-type: PROCEDURE
[id="example-installing-a-non-default-stream-of-an-application_{context}"]
.Installing a non-default stream of an application

====

The following is an example of how to install an application from a non-default stream (version), namely, the [application]*PostgreSQL* server (the [package]`postgresql-server` package) in version `13`. The default stream provides version `10`.

.Procedure

. List modules that provide the [package]`postgresql-server` package to see which streams are available:
+
[subs="quotes,attributes"]
----
$ *yum module list postgresql*
Name        Stream  Profiles            Summary
postgresql  9.6     client, server [d]  PostgreSQL server and client module
postgresql  10 [d]  client, server [d]  PostgreSQL server and client module
postgresql  12      client, server [d]  PostgreSQL server and client module
postgresql  13      client, server [d]  PostgreSQL server and client module
postgresql  15      client, server [d]  PostgreSQL server and client module

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
----
+
The output shows that the `postgresql` module is available with streams `9.6`, `10`, `12`, `13`, and `15`. The default stream is `10`.

. Install the packages provided by the `postgresql` module in stream `13`:
+
[subs="quotes,attributes"]
----
# *yum module install postgresql:13*
...
Dependencies resolved.
===================================================================================================================
 Package                  Architecture  Version                                        Repository             Size
===================================================================================================================
Installing group/module packages:
 postgresql-server        x86_64        13.10-1.module+el8.7.0+18279+1ca8cf12          rhel-AppStream        5.6 M
Installing dependencies:
 libicu                   x86_64        60.3-2.el8_1                                   rhel                  8.8 M
 libpq                    x86_64        13.5-1.el8                                     rhel-AppStream        198 k
 postgresql               x86_64        13.10-1.module+el8.7.0+18279+1ca8cf12          rhel-AppStream        1.5 M
Installing module profiles:
 postgresql/server                                                                                                
Enabling module streams:
 postgresql                             13                                                                        

Transaction Summary
===================================================================================================================
Install  4 Packages

Total download size: 16 M
Installed size: 61 M
Is this ok [y/N]: y

...

Installed:
  libicu-60.3-2.el8_1.x86_64                                                                                       
  libpq-13.5-1.el8.x86_64                                                                                          
  postgresql-13.10-1.module+el8.7.0+18279+1ca8cf12.x86_64                                                          
  postgresql-server-13.10-1.module+el8.7.0+18279+1ca8cf12.x86_64                                                   

Complete!
----
+
Because the installation profile was not specified, the default profile `server` was used.

. Verify the installed version of [application]*PostgreSQL*:
+
[subs="quotes,attributes"]
----
$ *postgres --version*
postgres (PostgreSQL) 13.10
----

====
