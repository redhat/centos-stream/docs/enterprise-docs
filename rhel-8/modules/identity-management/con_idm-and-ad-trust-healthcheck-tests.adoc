:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_verifying-your-idm-and-ad-trust-configuration-with-the-healthcheck-tool.adoc[leveloffset=+1]

// This module can be included from assemblies using the following include statement:
// include::<path>/con_healthcheck-in-idm.adoc[leveloffset=+1]

[id="idm-and-ad-trust-healthcheck-tests_{context}"]
= IdM and AD trust Healthcheck tests

[role="_abstract"]
The Healthcheck tool includes several tests for testing the status of your Identity Management (IdM) and Active Directory (AD) trust.

To see all trust tests, run `ipa-healthcheck` with the `--list-sources` option:

[literal]
--
# ipa-healthcheck --list-sources
--

You can find all tests under the `ipahealthcheck.ipa.trust` source:

IPATrustAgentCheck:: This test checks the SSSD configuration when the machine is configured as a trust agent. For each domain in `/etc/sssd/sssd.conf` where `id_provider=ipa` ensure that `ipa_server_mode` is `True`.
IPATrustDomainsCheck:: This test checks if the trust domains match SSSD domains by comparing the list of domains in `sssctl domain-list` with the list of domains from `ipa trust-find` excluding the IPA domain.
IPATrustCatalogCheck:: This test resolves resolves an AD user, `Administrator@REALM`.
This populates the AD Global catalog and AD Domain Controller values in `sssctl domain-status` output.
+
For each trust domain look up the user with the id of the SID + 500 (the administrator) and then check the output of `sssctl domain-status <domain> --active-server` to ensure that the domain is active.
IPAsidgenpluginCheck:: This test verifies that the `sidgen` plugin is enabled in the IPA 389-ds instance.
The test also verifies that the `IPA SIDGEN` and `ipa-sidgen-task` plugins in `cn=plugins,cn=config` include the `nsslapd-pluginEnabled` option.

IPATrustAgentMemberCheck:: This test verifies that the current host is a member of `cn=adtrust agents,cn=sysaccounts,cn=etc,SUFFIX`.
IPATrustControllerPrincipalCheck:: This test verifies that the current host is a member of `cn=adtrust agents,cn=sysaccounts,cn=etc,SUFFIX`.
IPATrustControllerServiceCheck:: This test verifies that the current host starts the ADTRUST service in ipactl.
IPATrustControllerConfCheck:: This test verifies that `ldapi` is enabled for the passdb backend in the output of `net conf` list.
IPATrustControllerGroupSIDCheck:: This test verifies that the admins group's SID ends with 512 (Domain Admins RID).
IPATrustPackageCheck:: This test verifies that the `trust-ad` package is installed if the trust controller and AD trust are not enabled.

NOTE: Run these tests on all IdM servers when trying to find an issue.
