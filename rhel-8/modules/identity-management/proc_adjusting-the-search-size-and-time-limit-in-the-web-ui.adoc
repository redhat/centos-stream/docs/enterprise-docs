:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// searching-ipa-entries-from-the-command-line.adoc
// assembly_adjusting-the-search-size-and-time-limit.adoc

[id="adjusting-the-search-size-and-time-limit-in-the-web-ui_{context}"]
= Adjusting the search size and time limit in the Web UI

[role="_abstract"]
The following procedure describes adjusting global search size and time limits in the IdM Web UI.

// commented out the following 2 lines because the GUI procedure only controls global limits, not temporary per-search limits like the CLI procedure covers
//* Globally
//* For a specific entry


.Procedure

. Log in to the IdM Web UI.

. Click *IPA Server*.
+
image:web-ui-ipaserver.png[Screenshot of the IdM Web UI highlighting the "IPA Server" tab from the top menu]

. On the *IPA Server* tab, click *Configuration*.

. Set the required values in the *Search Options* area.
+
Default values are:

* Search size limit: 100 entries
* Search time limit: 2 seconds

. Click *Save* at the top of the page.
+
image:web-ui-search-limits.png[Screenshot of the IdM Web UI highlighting the Save button which is below the "Configuration" title at the top of the Configuration page]
