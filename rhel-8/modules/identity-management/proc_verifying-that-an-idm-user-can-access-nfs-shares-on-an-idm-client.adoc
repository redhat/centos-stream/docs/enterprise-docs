:_mod-docs-content-type: PROCEDURE
[id="verifying-that-an-idm-user-can-access-nfs-shares-on-an-idm-client_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Verifying that an IdM user can access NFS shares on an IdM client

[role="_abstract"]
As an {IPA} (IdM) system administrator, you can test if an IdM user that is a member of a specific group can access NFS shares when logged in to a specific IdM client.

In the example, the following scenario is tested:

* An IdM user named *idm_user* belonging to the *developers* group can read and write the contents of the files in the */devel/project* directory automounted on *idm-client.idm.example.com*, an IdM client located in the *raleigh* automount location.
//* Any IdM user belonging to the *developers* group can read the contents of the files in the */devel_ro/documentation* directory from a configured IdM client.

ifdef::ansible-automount[]

.Prerequisites
* You have xref:setting-up-an-nfs-server-with-kerberos-in-a-red-hat-identity-management-domain_{context}[set up an NFS server with Kerberos on an IdM host].
* You have xref:configuring-automount-locations-maps-and-keys-in-idm-by-using-ansible_{context}[configured automount locations, maps, and mount points in IdM] in which you configured how IdM users can access the NFS share.
* You have xref:using-ansible-to-add-idm-users-to-a-group-that-owns-nfs-shares_{context}[used Ansible to add IdM users to the developers group that owns the NFS shares].
//* You have xref:using-ansible-to-add-an-idm-client-to-an-automount-location_{context}[used Ansible to add the IdM client to the relevant automount location].
* You have xref:configuring-automount-on-an-idm-client_{context}[configured automount on the IdM client].

endif::[]

ifndef::ansible-automount[]

.Prerequisites
* You have xref:setting-up-an-nfs-server-with-kerberos-in-a-red-hat-identity-management-domain_{context}[set up an NFS server with Kerberos on an IdM host].
* You have xref:configuring-automount-locations-and-maps-in-idm-using-the-idm-cli_{context}[configured automount locations, maps, and mount points in IdM] in which you configured how IdM users can access the NFS share.
* You have xref:configuring-automount-on-an-idm-client_{context}[configured automount on the IdM client].

endif::[]


.Procedure

. Verify that the IdM user can access the `read-write` directory:

.. Connect to the IdM client as the IdM user:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ssh idm_user@idm-client.idm.example.com*
Password:
....

.. Obtain the ticket-granting ticket (TGT) for the IdM user:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *kinit idm_user*
....

.. Optional: View the group membership of the IdM user:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa user-show idm_user*
  User login: idm_user
  [...]
  Member of groups: developers, ipausers
....


.. Navigate to the */devel/project* directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd /devel/project*
....

.. List the directory contents:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ls*
rw_file
....

.. Add a line to the file in the directory to test the `write` permission:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *echo "idm_user can write into the file" > rw_file*
....

.. Optional: View the updated contents of the file:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cat rw_file*
this is a read-write file
idm_user can write into the file
....

+
The output confirms that *idm_user* can write into the file.

////
. Verify that the IdM user can access the `read-only` directory:


.. Navigate to the */devel_ro/documentation* directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd /devel_ro/documentation*
....


.. List the directory contents:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ls*
ro_file
....

.. View the contents of the file to test the `read` permission:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cat ro_file*
this is a read-only file
....

+
The output confirms that *idm_user* can read the file.
////
