:_mod-docs-content-type: PROCEDURE

[id="proc_replacing-the-web-server-and-ldap-server-certificates-if-they-have-expired-in-the-whole-idm-deployment_{context}"]
= Replacing the web server and LDAP server certificates if they have expired in the whole IdM deployment


[role="_abstract"]
{IPA} (IdM) uses the following service certificates:

* The LDAP (or `Directory`) server certificate
* The web (or `httpd`) server certificate
* The PKINIT certificate

In an IdM deployment without a CA, `certmonger` does not by default track IdM service certificates or notify of their expiration. If the IdM system administrator does not manually set up notifications for these certificates, or configure `certmonger` to track them, the certificates will expire without notice.

Follow this procedure to manually replace expired certificates for the `httpd` and LDAP services running on the *server.idm.example.com* IdM server.

NOTE: The HTTP and LDAP service certificates have different keypairs and subject names on different IdM servers. Therefore, you must renew the certificates on each IdM server individually.

.Prerequisites

* The HTTP and LDAP certificates have expired on _all_ IdM replicas in the topology. If not, see xref:proc_replacing-the-web-server-and-ldap-server-certificates-if-they-have-not-yet-expired-on-an-idm-replica_{context}[Replacing the web server and LDAP server certificates if they have not yet expired on an IdM replica].
* You have `root` access to the IdM server and replicas.
* You know the `Directory Manager` password.
* You have created backups of the following directories and files:
** `/etc/dirsrv/slapd-_IDM-EXAMPLE-COM_/`
** `/etc/httpd/alias`
** `/var/lib/certmonger`
** `/var/lib/ipa/certs/`


.Procedure

. Optional: Perform a backup of `/var/lib/ipa/private` and `/var/lib/ipa/passwds`.

. If you are not using the same CA to sign the new certificates or if the already installed CA certificate is no longer valid, update the information about the external CA in your local database with a file that contains a valid CA certificate chain of the external CA. The file is accepted in PEM and DER certificate, PKCS#7 certificate chain, PKCS#8 and raw private key and PKCS#12 formats.

+
--
.. Install the certificates available in _ca_certificate_chain_file.crt_ as additional CA certificates into IdM:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *ipa-cacert-manage install ca_certificate_chain_file.crt*
....

.. Update the local IdM certificate databases with certificates from _ca_certicate_chain_file.crt_:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *ipa-certupdate*
....

--

+
. Request the certificates for `httpd` and LDAP:

.. Create a certificate signing request (CSR) for the Apache web server running on your IdM instances to your third party CA using the `OpenSSL` utility.

+
* The creation of a new private key is optional. If you still have the original private key, you can use the `-in` option with the `openssl req` command to specify the input file name to read the request from:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *openssl req -new -nodes -in /var/lib/ipa/private/httpd.key -out /tmp/http.csr -addext 'subjectAltName = DNS:_server.idm.example.com_, otherName:1.3.6.1.4.1.311.20.2.3;UTF8:HTTP/_server.idm.example.com@IDM.EXAMPLE.COM_' -subj '/O=_IDM.EXAMPLE.COM/CN=server.idm.example.com_'*
....

+
* If you want to create a new key:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *openssl req -new -newkey rsa:2048 -nodes -keyout /var/lib/ipa/private/httpd.key -out /tmp/http.csr -addext 'subjectAltName = DNS:__server.idm.example.com__, otherName:1.3.6.1.4.1.311.20.2.3;UTF8:HTTP/__server.idm.example.com@IDM.EXAMPLE.COM__' -subj '/O=__IDM.EXAMPLE.COM__/CN=__server.idm.example.com__'*
....

.. Create a certificate signing request (CSR) for the LDAP server running on your IdM instances to your third party CA using the `OpenSSL` utility:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *openssl req -new -newkey rsa:2048 -nodes -keyout ~/ldap.key -out /tmp/ldap.csr -addext 'subjectAltName = DNS:__server.idm.example.com__, otherName:1.3.6.1.4.1.311.20.2.3;UTF8:ldap/__server.idm.example.com@IDM.EXAMPLE.COM__' -subj '/O=__IDM.EXAMPLE.COM__/CN=__server.idm.example.com__'*
....

+
.. Submit the CSRs, */tmp/http.csr* and *tmp/ldap.csr*, to the external CA, and obtain a certificate for `httpd` and a certificate for LDAP. The process differs depending on the service to be used as the external CA.


. Install the certificate for `httpd` :

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *cp /path/to/httpd.crt /var/lib/ipa/certs/*
....

. Install the LDAP certificate into an NSS database:

+
--
.. Optional: List the available certificates:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *certutil -d /etc/dirsrv/slapd-_IDM-EXAMPLE-COM_/ -L*
Certificate Nickname                                         Trust Attributes
                                                             SSL,S/MIME,JAR/XPI

*Server-Cert*                                                  u,u,u
....

+
The default certificate nickname is *Server-Cert*, but it is possible that a different name was applied.

.. Remove the old invalid certificate from the NSS database (`NSSDB`) by using the certificate nickname from the previous step:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *certutil -D -d /etc/dirsrv/slapd-_IDM-EXAMPLE-COM_/ -n 'Server-Cert' -f /etc/dirsrv/slapd-_IDM-EXAMPLE-COM_/pwdfile.txt*
....

.. Create a PKCS12 file to ease the import process into `NSSDB`:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *openssl pkcs12 -export -in ldap.crt -inkey ldap.key -out ldap.p12 -name Server-Cert*
....

.. Install the created PKCS#12 file into the `NSSDB`:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *pk12util -i ldap.p12 -d /etc/dirsrv/slapd-_IDM-EXAMPLE-COM_/ -k /etc/dirsrv/slapd-_IDM-EXAMPLE-COM_/pwdfile.txt*
....


.. Check that the new certificate has been successfully imported:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *certutil -L -d /etc/dirsrv/slapd-_IDM-EXAMPLE-COM_/*
....

--

+
. Restart the `httpd` service:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *systemctl restart httpd.service*
....


. Restart the `Directory` service:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *systemctl restart dirsrv@_IDM-EXAMPLE-COM_.service*
....

. Perform all the previous steps on all your IdM replicas. This is a prerequisite for establishing `TLS` connections between the replicas.

. Enroll the new certificates to LDAP storage:

.. Replace the Apache web server's old private key and certificate with the new key and the newly-signed certificate:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *ipa-server-certinstall -w --pin=password /var/lib/ipa/private/httpd.key /var/lib/ipa/certs/httpd.crt*
....


+
In the command above:

* The `-w` option specifies that you are installing a certificate into the web server.
* The `--pin` option specifies the password protecting the private key.

.. When prompted, enter the `Directory Manager` password.

.. Replace the LDAP server's old private key and certificate with the new key and the newly-signed certificate:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *ipa-server-certinstall -d --pin=password  /etc/dirsrv/slapd-_IDM-EXAMPLE-COM_/ldap.key /path/to/ldap.crt*
....

+
--
In the command above:

* The `-d` option specifies that you are installing a certificate into the LDAP server.
* The `--pin` option specifies the password protecting the private key.
--

+
.. When prompted, enter the `Directory Manager` password.

+
.. Restart the `httpd` service:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *systemctl restart httpd.service*
....

+
.. Restart the `Directory` service:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *systemctl restart dirsrv@_IDM-EXAMPLE-COM_.service*
....

. Execute the commands from the previous step on all the other affected replicas.

[role="_additional-resources"]
.Additional resources
* man `ipa-server-certinstall(1)`
* link:https://access.redhat.com/solutions/6765131[How do I manually renew Identity Management (IPA) certificates on RHEL 8 after they have expired? (CA-less IPA)] (Red Hat Knowledgebase)
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/convert-cert-formats-idm_configuring-and-managing-idm[Converting certificate formats to work with IdM]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/convert-cert-formats-idm_managing-certificates-in-idm[Converting certificate formats to work with IdM]
endif::[]
