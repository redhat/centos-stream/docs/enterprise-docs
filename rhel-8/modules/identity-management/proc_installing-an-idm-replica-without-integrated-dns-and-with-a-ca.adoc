:_mod-docs-content-type: PROCEDURE
[id="installing-an-idm-replica-without-integrated-dns-and-with-a-ca_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Installing an IdM replica without integrated DNS and with a CA

Follow this procedure to install an Identity Management (IdM) replica:

--
* Without integrated DNS
* With a certificate authority (CA)
--

[IMPORTANT]
====
When configuring a replica with a CA, the CA configuration of the replica must mirror the CA configuration of the other server.

For example, if the server includes an integrated IdM CA as the root CA, the new replica must also be installed with an integrated CA as the root CA. No other CA configuration is available in this case.

Including the  `--setup-ca` option in the `ipa-replica-install` command copies the CA configuration of the initial server.
====


.Prerequisites

* Ensure your system is xref:preparing-the-system-for-ipa-replica-installation_installing-identity-management[prepared for an IdM replica installation].


.Procedure

. Enter `ipa-replica-install` with the [option]`--setup-ca` option.
+
[subs="+quotes,attributes"]
----
# ipa-replica-install --setup-ca
----

. Add the newly created IdM DNS service records to your DNS server:

.. Export the IdM DNS service records into a file in the `nsupdate` format:

+
[subs="+quotes,attributes"]
----
$ ipa dns-update-system-records --dry-run --out dns_records_file.nsupdate
----

+
.. Submit a DNS update request to your DNS server using the `nsupdate` utility and the *dns_records_file.nsupdate* file. For more information, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/dns-updates-external#dns-update-external-nsupdate[Updating External DNS Records Using nsupdate] in RHEL 7 documentation. Alternatively, refer to your DNS server documentation for adding DNS records.


////
.Additional resources

* For details on the supported CA configurations, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/install-server#install-determine-ca[Determining What CA Configuration to Use].
//link:{planning-url}planning-the-installation-and-distribution-of-server-services[Planning the installation and distribution of server services] in.
// _{PlanningIPA}_.
////
