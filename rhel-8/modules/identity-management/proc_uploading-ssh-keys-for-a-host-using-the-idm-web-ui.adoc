
:_mod-docs-content-type: PROCEDURE

[id="uploading-ssh-keys-for-a-host-using-the-idm-web-ui_{context}"]
= Uploading SSH keys for a host using the IdM Web UI

Identity Management allows you to upload a public SSH key to a host entry. OpenSSH uses public keys to authenticate hosts.


.Prerequisites

* Administrator privileges for managing the IdM Web UI or User Administrator role.


.Procedure

. You can retrieve the key for your host from a `~/.ssh/known_hosts` file. For example:
+
[literal]
--
server.example.com,1.2.3.4 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEApvjBvSFSkTU0WQW4eOweeo0DZZ08F9Ud21xlLy6FOhzwpXFGIyxvXZ52+siHBHbbqGL5+14N7UvElruyslIHx9LYUR/pPKSMXCGyboLy5aTNl5OQ5EHwrhVnFDIKXkvp45945R7SKYCUtRumm0Iw6wq0XD4o+ILeVbV3wmcB1bXs36ZvC/M6riefn9PcJmh6vNCvIsbMY6S+FhkWUTTiOXJjUDYRLlwM273FfWhzHK+SSQXeBp/zIn1gFvJhSZMRi9HZpDoqxLbBB9QIdIw6U4MIjNmKsSI/ASpkFm2GuQ7ZK9KuMItY2AoCuIRmRAdF8iYNHBTXNfFurGogXwRDjQ==
--

+
You can also generate a host key. See xref:generating-ssh-keys_managing-public-ssh-keys[Generating SSH keys].

. Copy the public key from the key file. The full key entry has the form `host name,IP type key==`. Only the `key==` is required, but you can store the entire entry. To use all elements in the entry, rearrange the entry so it has the order `type key== [host name,IP]`.
+
[literal]
--
cat /home/user/.ssh/host_keys.pub
ssh-rsa AAAAB3NzaC1yc2E...tJG1PK2Mq++wQ== server.example.com,1.2.3.4
--

. Log into the IdM Web UI.

. Go to the `Identity>Hosts` tab.

. Click the name of the host to edit.

. In the `Host Settings` section, click the SSH public keys `Add` button.

. Paste the public key for the host into the `SSH public key` field.

. Click `Set`.

. Click `Save` at the top of the IdM Web UI window.


.Verification

* Under the `Hosts Settings` section, verify the key is listed under `SSH public keys`.
