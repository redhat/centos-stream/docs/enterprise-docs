:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_adjusting-idm-directory-server-performance.adoc

[id="adjusting-the-database-index-cache-size_{context}"]
= Adjusting the database index cache size

IMPORTANT: Red Hat recommends using the built-in cache auto-sizing feature for optimized performance. Only change this value if you need to purposely deviate from the auto-tuned values.

[role="_abstract"]
The `nsslapd-dbcachesize` attribute controls the amount of memory the database indexes use. This cache size has less of an impact on Directory Server performance than the entry cache size does, but if there is available RAM after the entry cache size is set, Red Hat recommends increasing the amount of memory allocated to the database cache.

The database cache is limited to 1.5 GB RAM because higher values do not improve performance.


[cols="h,1"]
|===
| Default value | `10000000`  _(10 MB)_
| Valid range | `500000 - 1610611911`  _(500 kB - 1.5GB)_
| Entry DN location | `cn=config,cn=ldbm database,cn=plugins,cn=config`
|===


.Prerequisites
* The LDAP Directory Manager password


.Procedure

. Disable automatic cache tuning, and set the database cache size. This example sets the database cache to 256 megabytes.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *dsconf -D "cn=Directory Manager" _ldap://server.example.com_ backend config set --cache-autosize=0 --dbcachesize=268435456*
....

. Restart the Directory Server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *systemctl restart dirsrv.target*
....

. Monitor the IdM directory server's performance. If it does not change in a desirable way, repeat this procedure and adjust `dbcachesize` to a different value, or re-enable cache auto-sizing.


.Verification

* Display the value of the `nsslapd-dbcachesize` attribute and verify it has been set to your desired value.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ldapsearch -D "cn=directory manager" -w _DirectoryManagerPassword_ -b "cn=config,cn=ldbm database,cn=plugins,cn=config" | grep nsslapd-dbcachesize*
nsslapd-dbcachesize: *2147483648*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_directory_server/11/html/configuration_command_and_file_reference/plug_in_implemented_server_functionality_reference#nsslapd_dbcachesize[nsslapd-dbcachesize] in Directory Server 11 documentation
* xref:re-enabling-database-and-entry-cache-auto-sizing_adjusting-idm-directory-server-performance[Re-enabling entry and database cache auto-sizing].
