:_mod-docs-content-type: CONCEPT

[id="supported-windows-platforms-for-direct-integration_{context}"]
= Supported Windows platforms for direct integration

You can directly integrate your RHEL system with Active Directory forests that use the following forest and domain functional levels:

* Forest functional level range: Windows Server 2008 - Windows Server 2016
* Domain functional level range: Windows Server 2008 - Windows Server 2016

Direct integration has been tested on the following supported operating systems:

ifeval::[{ProductNumber} == 8]
* Windows Server 2022 (RHEL 8.7 or later)
* Windows Server 2019
* Windows Server 2016
* Windows Server 2012 R2
endif::[]

ifeval::[{ProductNumber} == 9]
* Windows Server 2022 (RHEL 9.1 or later)
* Windows Server 2019
* Windows Server 2016
* Windows Server 2012 R2
endif::[]


[NOTE]
====
Windows Server 2019 and Windows Server 2022 do not introduce a new functional level. The highest functional level Windows Server 2019 and Windows Server 2022 use is Windows Server 2016.
====
