:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// include::assemblies/assembly_managing-user-accounts-in-the-idm-web-ui.adoc[leveloffset=+1]


[id="preserving-active-users-in-the-idm-web-ui_{context}"]
= Preserving active users in the IdM Web UI

[role="_abstract"]
Preserving user accounts enables you to remove accounts from the *Active users* tab, yet keeping these accounts in IdM.

Preserve the user account if the employee leaves the company.
If you want to disable user accounts for a couple of weeks or months (parental leave, for example), disable the account.
For details, see xref:disabling-user-accounts-in-the-web-ui_managing-user-accounts-using-the-idm-web-ui[Disabling user accounts in the Web UI].
The preserved accounts are not active and users cannot use them to access your internal network, however, the account stays in the database with all the data.

You can move the restored accounts back to the active mode.

NOTE: The list of users in the preserved state can provide a history of past user accounts.

.Prerequisites

* Administrator privileges for managing the IdM (Identity Management) Web UI or User Administrator role.

.Procedure

. Log in to the IdM Web UI.
+
ifeval::[{ProductNumber} == 8]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/accessing-the-ipa-web-ui-in-a-web-browser_configuring-and-managing-idm[Accessing the IdM Web UI in a web browser].
endif::[]


. Go to *Users -> Active users* tab.

. Click the check-box of the user accounts you want to preserve.

. Click on the *Delete* button.
+
image:idm-users-active-delete.png[A screenshot of the "Active Users" page displaying a table of users. The checkbox for the entry for the "preserved.user" account has been checked and the "Delete" button at the top is highlighted.]

. In the *Remove users* dialog box, switch the *Delete mode* radio button to *preserve*.

. Click on the *Delete* button.
+
image:idm-users-preserving.png[A screenshot of a pop-up window titled "Remove users." The contents say "Are you sure you want to delete selected entries?" and specifies "preserved.user" below. There is a label "Delete mode" with two radial options: "delete" and "preserve" (which is selected). There are "Delete" and "Cancel" buttons at the bottom right corner of the window.]

As a result, the user account is moved to *Preserved users*.

If you need to restore preserved users, see the xref:restoring-users-in-the-idm-web-ui_managing-user-accounts-using-the-idm-web-ui[Restoring users in the IdM Web UI].
