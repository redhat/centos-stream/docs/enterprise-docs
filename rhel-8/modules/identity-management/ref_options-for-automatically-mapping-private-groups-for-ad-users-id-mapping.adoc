[id="ref_options-for-automatically-mapping-private-groups-for-ad-users-id-mapping_{context}"]
= Options for automatically mapping private groups for AD users: ID mapping trusts

[role="_abstract"]
Each user in a Linux environment has a primary user group. {ProductName} (RHEL) uses a user private group (UPG) scheme: a UPG has the same name as the user for which it was created and that user is the only member of the UPG.

If you have allocated UIDs for your AD users, but GIDs were not added, you can configure SSSD to automatically map private groups for users based on their UID by adjusting the `auto_private_groups` setting for that ID range.

By default, the `auto_private_groups` option is set to `true` for `ipa-ad-trust` ID ranges used in an ID mapping trust. With this configuration, SSSD computes the UID and GID for an AD user based on its Security Identifier (SID). SSSD ignores any POSIX attributes in AD, such as `uidNumber`, `gidNumber`, and also ignores the `primaryGroupID`.

`auto_private_groups = true`::
SSSD always maps a private group with the GID set to match the UID, which is based on the SID of the AD user.
+
.SSSD behavior when the `auto_private_groups` variable is set to `true` for an ID mapping ID range
[options="header",adoc]
|====
|User configuration in AD|Output of `id username`

a|AD user entry where:

* SID maps to 7000
* `primaryGroupID` maps to 8000
|`# id aduser@AD-DOMAIN.COM`
`uid=7000(aduser@ad-domain.com) gid=7000(aduser@ad-domain.com) groups=7000(aduser@ad-domain.com), 8000(adgroup@ad-domain.com), ...`
|====

`auto_private_groups = false`::
If you set the `auto_private_groups` option to `false`, SSSD uses the `primaryGroupID` set in the AD entry as the GID number. The default value for `primaryGroupID` corresponds to the `Domain Users` group in AD.
+
.SSSD behavior when the `auto_private_groups` variable is set to `false` for an ID mapping ID range
[options="header",adoc]
|====
|User configuration in AD|Output of `id username`

a|AD user entry where:

* SID maps to 7000
* `primaryGroupID` maps to 8000
|`# id aduser@AD-DOMAIN.COM`
`uid=7000(aduser@ad-domain.com) gid=8000(adgroup@ad-domain.com) groups=8000(adgroup@ad-domain.com), ...`
|====
