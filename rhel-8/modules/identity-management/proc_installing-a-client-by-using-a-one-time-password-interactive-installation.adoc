:_mod-docs-content-type: PROCEDURE

[id="proc_installing-a-client-by-using-a-one-time-password-interactive-installation_{context}"]
= Installing a client by using a one-time password: Interactive installation

[role="_abstract"]
Follow this procedure to install an {IPA} (IdM) client interactively by using a one-time password to enroll the system into the domain.

.Prerequisites

* On a server in the domain, add the future client system as an IdM host. Use the [option]`--random` option with the [command]`ipa host-add` command to generate a one-time random password for the enrollment.
+
NOTE: The `ipa host-add <client_fqdn>` command requires that the client FQDN is resolvable through DNS. If it is not resolvable, provide the IdM client system's IP address using the `--ip address` option or alternatively, use the `--force` option.
+
[literal,subs="+quotes,attributes"]
....
$ *ipa host-add _client.example.com_ --random*
 --------------------------------------------------
 Added host "client.example.com"
 --------------------------------------------------
  Host name: client.example.com
  Random password: *W5YpARl=7M.n*
  Password: True
  Keytab: False
  Managed by: server.example.com
....

+
NOTE: The generated password will become invalid after you use it to enroll the machine into the IdM domain. It will be replaced with a proper host keytab after the enrollment is finished.

.Procedure

. Run the `ipa-client-install` utility on the system that you want to configure as an IdM client.
+
Use the [option]`--password` option to provide the one-time random password. Because the password often contains special characters, enclose it in single quotes (').
+
[literal,subs="+quotes,attributes"]
....
# *ipa-client-install --mkhomedir --password=__password__*
....
+
Add the [option]`--enable-dns-updates` option to update the DNS records with the IP address of the client system if either of the following conditions applies:
+
--
** The IdM server the client will be enrolled with was installed with integrated DNS
** The DNS server on the network accepts DNS entry updates with the GSS-TSIG protocol

//* If you want to disable storing Kerberos passwords in the {SSSD} (SSSD) cache, add the [option]`--no-krb5-offline-passwords` option. --no-krb5-offline-passwords  --no-krb5-offline-passwords
--
+
[literal,subs="+quotes,attributes"]
....
# *ipa-client-install --password 'W5YpARl=7M.n' --enable-dns-updates --mkhomedir*
....
+
Enabling DNS updates is useful if your client:

** has a dynamic IP address issued using the Dynamic Host Configuration Protocol
** has a static IP address but it has just been allocated and the IdM server does not know about it

. The installation script attempts to obtain all the required settings, such as DNS records, automatically.

** If the SRV records are set properly in the IdM DNS zone, the script automatically discovers all the other required values and displays them. Enter `yes` to confirm.
+
[subs="+quotes,attributes"]
....
Client hostname: client.example.com
Realm: EXAMPLE.COM
DNS Domain: example.com
IPA Server: server.example.com
BaseDN: dc=example,dc=com

Continue to configure the system with these values? [no]: *yes*
....

** To install the system with different values, enter `no`. Then run `ipa-client-install` again, and specify the required values by adding command-line options to `ipa-client-install`, for example:
+
--
*** `--hostname`
*** `--realm`
*** `--domain`
*** `--server`
*** `--mkhomedir`
--
+
[IMPORTANT]
====

The fully qualified domain name must be a valid DNS name:

* Only numbers, alphabetic characters, and hyphens (`-`) are allowed. For example, underscores are not allowed and can cause DNS failures.
* The host name must be all lower-case. No capital letters are allowed.

//For other recommended naming practices, see the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/sec-Securing_DNS_Traffic_with_DNSSEC.html#sec-Recommended_Naming_Practices++[{RHEL} Security Guide].

====

** If the script fails to obtain some settings automatically, it prompts you for the values.


. The installation script now configures the client. Wait for the operation to complete.
+
....
Client configuration complete.
....

//. Optional: Run the `ipa-client-automount` utility, which automatically configures NFS for {IPA}.

[role="_additional-resources"]
.Additional resources
* For details on how the client installation script searches for the DNS records, see the `DNS Autodiscovery` section in the `ipa-client-install`(1) man page.
