:_mod-docs-content-type: PROCEDURE
[id="proc_listing-overrides-on-a-host_{context}"]
= Listing overrides on a host

[role="_abstract"]
As an administrator, you can list all user and group overrides on a host to verify that the correct attributes have been overridden.

.Prerequisites
* `root` access
* Installed `sssd-tools`

.Procedure
* List all user overrides:
+
....
# sss_override user-find
user1@ldap.example.com::8000::::/bin/zsh:
user2@ldap.example.com::8001::::/bin/bash:
...
....

* List all group overrides:
+
....
# sss_override group-find
group1@ldap.example.com::7000
group2@ldap.example.com::7001
...
....
