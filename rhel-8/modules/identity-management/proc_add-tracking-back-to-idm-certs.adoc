:_mod-docs-content-type: PROCEDURE
[id="add-tracking-back-to-idm-certs_{context}"]
= Making certmonger resume tracking of IdM certificates on a CA replica

[role="_abstract"]
This procedure shows how to make `certmonger` resume the tracking of Identity Management (IdM) system certificates that are crucial for an IdM deployment with an integrated certificate authority after the tracking of certificates was interrupted. The interruption may have been caused by the IdM host being unenrolled from IdM during the renewal of the system certificates or by replication topology not working properly.
The procedure also shows how to make `certmonger` resume the tracking of the IdM service certificates, namely the `HTTP`, `LDAP` and `PKINIT` certificates.

.Prerequisites

* The host on which you want to resume tracking system certificates is an IdM server that is also an IdM certificate authority (CA) but not the IdM CA renewal server.


.Procedure

. Get the PIN for the subsystem CA certificates:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *grep 'internal=' /var/lib/pki/pki-tomcat/conf/password.conf*
....

. Add tracking to the subsystem CA certificates, replacing `[internal PIN]` in the commands below with the PIN obtained in the previous step:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *getcert start-tracking -d /etc/pki/pki-tomcat/alias -n "caSigningCert cert-pki-ca" -c 'dogtag-ipa-ca-renew-agent' -P [internal PIN] -B /usr/libexec/ipa/certmonger/stop_pkicad -C '/usr/libexec/ipa/certmonger/renew_ca_cert "caSigningCert cert-pki-ca"' -T caCACert*

# *getcert start-tracking -d /etc/pki/pki-tomcat/alias -n "auditSigningCert cert-pki-ca" -c 'dogtag-ipa-ca-renew-agent' -P [internal PIN] -B /usr/libexec/ipa/certmonger/stop_pkicad -C '/usr/libexec/ipa/certmonger/renew_ca_cert "auditSigningCert cert-pki-ca"' -T caSignedLogCert*

# *getcert start-tracking -d /etc/pki/pki-tomcat/alias -n "ocspSigningCert cert-pki-ca" -c 'dogtag-ipa-ca-renew-agent' -P [internal PIN] -B /usr/libexec/ipa/certmonger/stop_pkicad -C '/usr/libexec/ipa/certmonger/renew_ca_cert "ocspSigningCert cert-pki-ca"' -T caOCSPCert*

# *getcert start-tracking -d /etc/pki/pki-tomcat/alias -n "subsystemCert cert-pki-ca" -c 'dogtag-ipa-ca-renew-agent' -P [internal PIN] -B /usr/libexec/ipa/certmonger/stop_pkicad -C '/usr/libexec/ipa/certmonger/renew_ca_cert "subsystemCert cert-pki-ca"' -T caSubsystemCert*

# *getcert start-tracking -d /etc/pki/pki-tomcat/alias -n "Server-Cert cert-pki-ca" -c 'dogtag-ipa-ca-renew-agent' -P [internal PIN] -B /usr/libexec/ipa/certmonger/stop_pkicad -C '/usr/libexec/ipa/certmonger/renew_ca_cert "Server-Cert cert-pki-ca"' -T caServerCert*
....


. Add tracking for the remaining IdM certificates, the `HTTP`, `LDAP`, `IPA renewal agent` and `PKINIT` certificates:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *getcert start-tracking -f /var/lib/ipa/certs/httpd.crt -k /var/lib/ipa/private/httpd.key -p /var/lib/ipa/passwds/idm.example.com-443-RSA -c IPA -C /usr/libexec/ipa/certmonger/restart_httpd -T caIPAserviceCert*

# *getcert start-tracking -d /etc/dirsrv/slapd-IDM-EXAMPLE-COM -n "Server-Cert" -c IPA -p /etc/dirsrv/slapd-IDM-EXAMPLE-COM/pwdfile.txt -C '/usr/libexec/ipa/certmonger/restart_dirsrv "IDM-EXAMPLE-COM"' -T caIPAserviceCert*

# *getcert start-tracking -f /var/lib/ipa/ra-agent.pem -k /var/lib/ipa/ra-agent.key -c dogtag-ipa-ca-renew-agent -B /usr/libexec/ipa/certmonger/renew_ra_cert_pre -C /usr/libexec/ipa/certmonger/renew_ra_cert -T caSubsystemCert*

# *getcert start-tracking -f /var/kerberos/krb5kdc/kdc.crt -k /var/kerberos/krb5kdc/kdc.key -c dogtag-ipa-ca-renew-agent -B /usr/libexec/ipa/certmonger/renew_ra_cert_pre -C /usr/libexec/ipa/certmonger/renew_kdc_cert -T KDCs_PKINIT_Certs*
....


. Restart `certmonger`:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *systemctl restart certmonger*
....


. Wait for one minute after `certmonger` has started and then check the statuses of the new certificates:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *getcert list*
....

Note the following:

* If your IdM system certificates have all expired, see the Red{nbsp}Hat Knowledgebase solution link:https://access.redhat.com/solutions/3357261[How do I manually renew Identity Management (IPA) certificates on RHEL7/RHEL 8 after they have expired?] to manually renew IdM system certificates on the IdM CA server that is also the CA renewal server and the CRL publisher server. 

* Follow the procedure described in the Red{nbsp}Hat Knowledgebase solution link:https://access.redhat.com/solutions/3357331[How do I manually renew Identity Management (IPA) certificates on RHEL7 after they have expired?] to manually renew IdM system certificates on all the other CA servers in the topology.

//* If your IdM system certificates have all expired and your RHEL environment is RHEL 8.1 and later, follow the procedure described in the Red{nbsp}Hat Knowledgebase solution link:https://access.redhat.com/solutions/4172771[Fixing expired system certificates] to renew IdM system certificates on IdM CA servers and replicas in a more automated way using the `ipa-cert-fix` tool.
