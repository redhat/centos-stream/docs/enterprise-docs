:_mod-docs-content-type: PROCEDURE
// [[proc-add-certmapdata-to-user-cli_{context}]]
[id="proc-add-certmapdata-to-user-cli_{context}"]

= Adding certificate mapping data to a user entry in the IdM CLI

[role="_abstract"]
. Obtain the administrator's credentials:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *kinit admin*
....

. Choose one of the following options:

* If you have the certificate of `idm_user`, add the certificate to the user account using the `ipa user-add-cert` command:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *CERT=$(openssl x509 -in idm_user_cert.pem -outform der|base64 -w0)*
# *ipa user-add-certmapdata idm_user --certificate $CERT*
....

* If you do not have the certificate of `idm_user` but know the `Issuer` and the `Subject` of the user's certificate:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *ipa user-add-certmapdata idm_user --subject "O=EXAMPLE.ORG,CN=test" --issuer "CN=Smart Card CA,O=EXAMPLE.ORG"*
--------------------------------------------
Added certificate mappings to user "idm_user"
--------------------------------------------
  User login: idm_user
  Certificate mapping data: X509:<I>O=EXAMPLE.ORG,CN=Smart Card CA<S>CN=test,O=EXAMPLE.ORG
....

.Verification

If you have access to the whole certificate in the `.pem` format, verify that the user and certificate are linked:

--
. Use the `sss_cache` utility to invalidate the record of `idm_user` in the SSSD cache and force a reload of the `idm_user` information:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *sss_cache -u idm_user*
....

+
. Run the `ipa certmap-match` command with the name of the file containing the certificate of the IdM user:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *ipa certmap-match idm_user_cert.pem*
--------------
1 user matched
--------------
 Domain: IDM.EXAMPLE.COM
 User logins: idm_user
----------------------------
Number of entries returned 1
----------------------------
....

+
The output confirms that now you have certificate mapping data added to `idm_user` and that a corresponding mapping rule exists. This means that you can use any certificate that matches the defined certificate mapping data to authenticate as `idm_user`.
--
