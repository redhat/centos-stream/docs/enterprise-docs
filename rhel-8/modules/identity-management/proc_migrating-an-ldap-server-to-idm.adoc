[id="migrating-an-ldap-server-to-idm_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Migrating an LDAP server to IdM

You can migrate your authentication and authorization services from an LDAP server to {IPA} (IdM) using the [command]`ipa migrate-ds` command.

[WARNING]
====

This is a general migration procedure that may not work in every environment.

It is strongly recommended that you set up a test LDAP environment and test the migration process before attempting to migrate the real LDAP environment. When testing the environment, do the following:

. Create a test user in IdM and compare the output of migrated users to that of the test user.
. Compare the output of migrated users, as seen on IdM, to the source users, as seen on the original LDAP server.

For more guidance, see the *Verification* section below.

====

.Prerequisites

* You have administrator privileges to the LDAP directory.
* If IdM is already installed, you have administrator privileges to IdM.
* You are logged in as `root` on the RHEL system on which you are executing the procedure below.
* You have read and understood the following chapters:

** xref:considerations-in-migrating-from-ldap-to-idm_{context}[Considerations in migrating from LDAP to IdM].
** xref:planning-the-client-configuration-when-migrating-from-ldap-to-idm_{context}[Planning the client configuration when migrating from LDAP to IdM].
** xref:planning-password-migration-when-migrating-from-ldap-to-idm_{context}[Planning password migration when migrating from LDAP to IdM].
** xref:further-migration-considerations-and-requirements_{context}[Further migration considerations and requirements].
** xref:customizing-the-migration-from-ldap-to-idm_{context}[Customizing the migration from LDAP to IdM].


.Procedure

. If IdM is not yet installed: install the IdM server, including any custom LDAP directory schema, on a different machine from the one on which the existing LDAP directory is installed. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/index[Installing Identity Management].

+
[NOTE]
====

Custom user or group schemas have limited support in IdM. They can cause problems during the migration because of incompatible object definitions.

====

. For performance reasons, disable the compat plug-in:
+
[literal,subs="+quotes,verbatim"]
....
# *ipa-compat-manage disable*
....

+
For more information about the schema compat feature and the benefits of disabling it for the migration, see xref:the-schema-to-use-when-migrating-from-ldap-to-idm-and-the-schema-compat-feature_customizing-the-migration-from-ldap-to-idm[The schema to use when migrating from LDAP to IdM and the schema compat feature].


. Restart the IdM Directory Server instance:
+
[literal,subs="+quotes,verbatim"]
....
# *systemctl restart dirsrv.target*
....

. Configure the IdM server to allow migration:
+
[literal,subs="+quotes,verbatim"]
....
# *ipa config-mod --enable-migration=TRUE*
....

+
By setting `--enable-migration` to TRUE, you do the following:

+
** Allow pre-hashed passwords during an LDAP add operation.
** Configure SSSD to try the password migration sequence if the initial Kerberos authentication fails. For more information, see the Workflow section in xref:using-sssd_planning-password-migration-when-migrating-from-ldap-to-idm[Using SSSD when migrating passwords from LDAP to IdM].

. Run the IdM migration script, [command]`ipa migrate-ds`, with the options that are relevant for your use case. For more information, see xref:customizing-the-migration-from-ldap-to-idm_{context}[Customizing the migration from LDAP to IdM].

+
[literal,subs="+quotes,verbatim"]
....
# *ipa migrate-ds __--your-options__ ldap://ldap.example.com:389*
....

+
[NOTE]
====
If you did not disable the compat plug-in in one of the previous steps, add the [option]`--with-compat` option to [command]`ipa migrate-ds`:

[literal,subs="+quotes,verbatim"]
....
# *ipa migrate-ds __--your-options__ --with-compat ldap://ldap.example.com:389*
....
====

+
. Re-enable the compat plug-in:
+
[literal,subs="+quotes,verbatim"]
....
# *ipa-compat-manage enable*
....

. Restart the IdM Directory Server:
+
[literal,subs="+quotes,verbatim"]
....
# *systemctl restart dirsrv.target*
....

. When all users have had their passwords migrated, disable the migration mode:
+
[literal,subs="+quotes,verbatim"]
....
# *ipa config-mod --enable-migration=FALSE*
....

. Optional: When all of the users have been migrated, reconfigure non-SSSD clients to use Kerberos authentication, that is `pam_krb5`, instead of LDAP authentication, that is `pam_ldap`. For more information, see link:https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System-Level_Authentication_Guide/Configuring_a_Kerberos_5_Client.html[Configuring a Kerberos Client] in the RHEL 7 _System-level Authentication Guide_.

. Have users generate their hashed Kerberos passwords. Choose one of the methods described in xref:planning-password-migration-when-migrating-from-ldap-to-idm_{context}[Planning password migration when migrating from LDAP to IdM].
+
--
* If you decide on the xref:using-sssd_planning-password-migration-when-migrating-from-ldap-to-idm[SSSD method]:
+
** Move clients that have SSSD installed from the LDAP directory to the IdM directory, and enroll them as clients with IdM. This downloads the required keys and certificates.
+
On {RHEL} clients, this can be done using the `ipa-client-install` command. For example:
+
[literal,subs="+quotes,verbatim"]
....
# *ipa-client-install --enable-dns-update*
....
+
* If you decide on the xref:using-the-migration-web-page_planning-password-migration-when-migrating-from-ldap-to-idm[IdM migration web page] method:
+
** Instruct users to log into IdM using the migration web page:
+
[subs="+quotes"]
....
https://ipaserver.example.com/ipa/migration
....
--

+
. To monitor the user migration process, query the existing LDAP directory to see which user accounts have a password but do not yet have a Kerberos principal key.
+
[literal,subs="+quotes,verbatim"]
....
$ *ldapsearch -LL -x -D 'cn=Directory Manager' -w secret -b 'cn=users,cn=accounts,dc=example,dc=com' '(&(!(krbprincipalkey=*))(userpassword=*))' uid*
....
+
[NOTE]
====

Include the single quotes around the filter so that it is not interpreted by the shell.

====

. When the migration of all clients and users is complete, decommission the LDAP directory.

.Verification

. Create a test user in IdM by using the `ipa user-add` command. Compare the output of migrated users to that of the test user. Ensure that the migrated users contain the minimal set of attributes and object classes present on the test user. For example:

+
[literal,subs="+quotes,verbatim"]
....
$ *ipa user-show --all testing_user*
dn: uid=testing_user,cn=users,cn=accounts,dc=idm,dc=example,dc=com
User login: testing_user
First name: testing
Last name: user
Full name: testing user
Display name: testing user
Initials: tu
Home directory: /home/testing_user
GECOS: testing user
Login shell: /bin/sh
Principal name: testing_user@IDM.EXAMPLE.COM
Principal alias: testing_user@IDM.EXAMPLE.COM
Email address: testing_user@idm.example.com
UID: 1689700012
GID: 1689700012
Account disabled: False
Preserved user: False
Password: False
Member of groups: ipausers
Kerberos keys available: False
ipauniqueid: 843b1ac8-6e38-11ec-8dfe-5254005aad3e
mepmanagedentry: cn=testing_user,cn=groups,cn=accounts,dc=idm,dc=example,dc=com
objectclass: top, person, organizationalperson, inetorgperson, inetuser, posixaccount, krbprincipalaux, krbticketpolicyaux, ipaobject,
             ipasshuser, ipaSshGroupOfPubKeys, mepOriginEntry
....

. Compare the output of migrated users, as seen on IdM, to the source users, as seen on the original LDAP server. Ensure that imported attributes are not copied twice and that they have the correct values.


[role="_additional-resources"]
.Additional resources
* xref:migrating-from-ldap-to-idm-over-ssl_{context}[Migrating from LDAP to IdM over SSL]
