
:_mod-docs-content-type: CONCEPT

[id="con_support-for-trust-with-fips-mode_{context}"]
= Support for cross-forest trust with FIPS mode enabled

To establish a cross-forest trust with an Active Directory (AD) domain while FIPS mode is enabled, you must meet the following requirements:

* IdM servers are on RHEL 8.4.0 or later.
* You must authenticate with an AD administrative account when setting up a trust. You cannot establish a trust using a shared secret while FIPS mode is enabled.

[IMPORTANT]
====
RADIUS authentication is not FIPS-compliant as the RADIUS protocol uses the MD5 hash function to encrypt passwords between client and server and, in FIPS mode, OpenSSL disables the use of the MD5 digest algorithm. However, if the RADIUS server is running on the same host as the IdM server, you can work around the problem and enable MD5 by performing the steps described in the Red{nbsp}Hat Knowledgebase solution link:https://access.redhat.com/solutions/4650511[How to configure FreeRADIUS authentication in FIPS mode].
====

[role="_additional-resources"]
.Additional Resources
* For more information about FIPS mode in the RHEL operating system, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/assembly_installing-a-rhel-8-system-with-fips-mode-enabled_security-hardening[Installing the system in FIPS mode] in the _Security Hardening_ document.
* For more details about the FIPS 140-2 standard, see the link:https://csrc.nist.gov/publications/detail/fips/140/2/final[Security Requirements for Cryptographic Modules] on the National Institute of Standards and Technology (NIST) web site.
