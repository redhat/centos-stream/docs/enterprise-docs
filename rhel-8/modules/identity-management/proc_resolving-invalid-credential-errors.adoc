:_mod-docs-content-type: PROCEDURE
// This module included in the following Assemblies:
//
// assembly_troubleshooting-idm-replica-installation.adoc


[id="resolving-invalid-credential-errors_{context}"]
= Resolving invalid credential errors

[role="_abstract"]
If an IdM replica installation fails with an `Invalid credentials` error, the system clocks on the hosts might be out of sync with each other:

[literal,subs="+quotes,attributes,verbatim"]
....
[27/40]: setting up initial replication
Starting replication, please wait until this has completed.
Update in progress, 15 seconds elapsed
[ldap://server.example.com:389] reports: *Update failed! Status: [49  - LDAP error: Invalid credentials]*

[error] RuntimeError: Failed to start replication
Your system may be partly configured.
Run /usr/sbin/ipa-server-install --uninstall to clean up.

ipa.ipapython.install.cli.install_tool(CompatServerReplicaInstall): ERROR    Failed to start replication
ipa.ipapython.install.cli.install_tool(CompatServerReplicaInstall): ERROR    The ipa-replica-install command failed. See /var/log/ipareplica-install.log for more information
....

If you use the `--no-ntp` or `-N` options to attempt the replica installation while clocks are out of sync, the installation fails because services are unable to authenticate with Kerberos.

To resolve this issue, synchronize the clocks on both hosts and retry the installation process.


.Prerequisites

* You must have `root` privileges to change system time.


.Procedure

. Synchronize the system clocks manually or with `chronyd`.
+
Synchronizing manually::
Display the system time on the server and set the replica's time to match.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[user@server ~]$ *date*
Thu May 28 21:03:57 EDT 2020

[user@replica ~]$ sudo timedatectl set-time _'2020-05-28 21:04:00'_
....
* *Synchronizing with `chronyd`*:
+
See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/configuring-time-synchronization_configuring-basic-system-settings#using-chrony-to-configure-ntp_configuring-time-synchronization[Using the Chrony suite to configure NTP] to configure and set system time with `chrony` tools.

. Attempt the IdM replica installation again.

.Additional resources
* If you are unable to resolve a failing replica installation, and you have a Red Hat Technical Support subscription, open a Technical Support case at the link:https://access.redhat.com/support/cases/#/[Red Hat Customer Portal] and provide an `sosreport` of the replica and an `sosreport` of the server.
* The `sosreport` utility collects configuration details, logs and system information from a RHEL system. For more information about the `sosreport` utility, see the Red{nbsp}Hat Knowledgebase solution link:https://access.redhat.com/solutions/3592[What is an sosreport and how to create one in Red Hat Enterprise Linux?].
