:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-an-hbac-rule-in-idm-using-an-ansible-playbook_{context}"]
= Ensuring the presence of an HBAC rule in IdM using an Ansible playbook

[role="_abstract"]
Follow this procedure to ensure the presence of a host-based access control (HBAC) rule in {IPA} (IdM) using an Ansible playbook.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

* The users and user groups you want to use for your HBAC rule exist in IdM. See xref:managing-user-accounts-using-Ansible-playbooks_{ProjectNameID}[Managing user accounts using Ansible playbooks] and xref:ensuring-the-presence-of-IdM-groups-and-group-members-using-Ansible-playbooks_managing-user-groups-using-ansible-playbooks[Ensuring the presence of IdM groups and group members using Ansible playbooks] for details.

* The hosts and host groups to which you want to apply your HBAC rule exist in IdM. See xref:managing-hosts-using-Ansible-playbooks_{ProjectNameID}[Managing hosts using Ansible playbooks] and xref:managing-host-groups-using-Ansible-playbooks_{ProjectNameID}[Managing host groups using Ansible playbooks] for details.


.Procedure

. Create an inventory file, for example `inventory.file`, and define `ipaserver` in it:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Create your Ansible playbook file that defines the HBAC policy whose presence you want to ensure. To simplify this step, you can copy and modify the example in the `/usr/share/doc/ansible-freeipa/playbooks/hbacrule/ensure-hbacrule-allhosts-present.yml` file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to handle hbacrules
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  # Ensure idm_user can access client.idm.example.com via the sshd service
  - ipahbacrule:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: login
      user: idm_user
      host: client.idm.example.com
      hbacsvc:
      - sshd
      state: present
....

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory/inventory.file__ __path_to_playbooks_directory/ensure-new-hbacrule-present.yml__*
....




.Verification

. Log in to the IdM Web UI as administrator.

. Navigate to *Policy* → *Host-Based-Access-Control* → *HBAC Test*.

. In the *Who* tab, select idm_user.

. In the *Accessing* tab, select *client.idm.example.com*.

. In the *Via service* tab, select *sshd*.

. In the *Rules* tab, select *login*.

. In the *Run test* tab, click the *Run test* button. If you see ACCESS GRANTED, the HBAC rule is implemented successfully.

[role="_additional-resources"]
.Additional resources
* See the `README-hbacsvc.md`, `README-hbacsvcgroup.md`, and `README-hbacrule.md` files in the `/usr/share/doc/ansible-freeipa` directory.
* See the playbooks in the subdirectories of the `/usr/share/doc/ansible-freeipa/playbooks` directory.
