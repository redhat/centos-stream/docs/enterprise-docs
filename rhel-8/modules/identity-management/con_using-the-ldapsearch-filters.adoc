:_mod-docs-content-type: CONCEPT

[id="con_using-the-ldapsearch-filters_{context}"]
= Using the ldapsearch filters

The [command]`ldapsearch` filters allow you to narrow down the search results.

For example, you want the search result to contain all the entries with a common names set to _example_:

----
"(cn=example)"
----

In this case, the _equal sign (=)_ is the operator, and _example_ is the value.

.The `ldapsearch` filter operators
[subs=+quotes,options="header"]
|===
|Search type|Operator|Description

|Equality
|=
|Returns the entries with the exact match to the value. For example, _cn=example_.

|Substring
|_=string* string_
|Returns all entries with the substring match. For example, _cn=exa*l_. The asterisk (*) indicates zero (0) or more characters.

|Greater than or equal to
|>=
|Returns all entries with attributes that are greater than or equal to the value. For example, _uidNumber >= 5000_.

|Less than or equal to
|&lt;=
|Returns all entries with attributes that are less than or equal to the value. For example, _uidNumber &lt;= 5000_.

|Presence
|=*
|Returns all entries with one or more attributes. For example, _cn=*_.

|Approximate
|~=
|Returns all entries with the similar to the value attributes. For example, _l~=san fransico_ can return _l=san francisco_.
|===
[role="_additional-resources"]

You can use _boolean_ operators to combine multiple filters to the [command]`ldapsearch` command.

.The `ldapsearch` filter boolean operators
[subs=+quotes,options="header"]
|===
|Search type|Operator|Description

|AND
|&
|Returns all entries where all statements in the filters are true. For example, _(&(filter)(filter)(filter)...)_.

|OR
a|\|
|Returns all entries where at least one statement in the filters is true. For example, _(\|(filter)(filter)(filter)...)_.

|NOT
|!
|Returns all entries where the statement in the filter is not true. For example, _(!(filter))_.
|===
