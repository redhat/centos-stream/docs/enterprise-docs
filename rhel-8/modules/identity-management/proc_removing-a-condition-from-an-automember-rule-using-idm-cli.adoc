:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
[id="removing-a-condition-from-an-automember-rule-using-idm-cli_{context}"]
= Removing a condition from an automember rule using IdM CLI

[role="_abstract"]
Follow this procedure to remove a specific condition from an automember rule.

.Prerequisites

* You must be logged in as the administrator. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]


.Procedure

. Enter the [command]`ipa automember-remove-condition` command.

. When prompted, specify:

** *Automember rule*. This is the name of the rule from which you want to remove a condition.

** *Attribute Key*. This is the target entry attribute. For example, *uid* for users.

** *Grouping Type*. This specifies whether the condition you want to delete is for a user group or a host group. Enter *group* or *hostgroup*.

** *Inclusive regex* and *Exclusive regex*. These specify the conditions you want to remove. If you only want to specify one condition, press *Enter* when prompted for the other.

+
For example:

+
[literal, subs="+quotes,verbatim"]
--
$ *ipa automember-remove-condition*
Automember Rule: *user_group*
Attribute Key: *uid*
Grouping Type: *group*
[Inclusive Regex]: .*
[Exclusive Regex]:
-----------------------------------
Removed condition(s) from "user_group"
-----------------------------------
  Automember Rule: user_group
------------------------------
Number of conditions removed 1
------------------------------
--
