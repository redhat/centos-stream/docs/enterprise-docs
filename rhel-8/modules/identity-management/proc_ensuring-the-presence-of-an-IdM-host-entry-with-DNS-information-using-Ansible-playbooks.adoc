:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-an-IdM-host-entry-with-DNS-information-using-Ansible-playbooks_{context}"]
= Ensuring the presence of an IdM host entry with DNS information using Ansible playbooks

[role="_abstract"]
Follow this procedure to ensure the presence of host entries in {IPA} (IdM) using Ansible playbooks. The host entries are defined by their `fully-qualified domain names` (FQDNs) and their IP addresses.

[NOTE]
====
Without Ansible, host entries are created in IdM using the [command]`ipa host-add` command. The result of adding a host to IdM is the state of the host being present in IdM. Because of the Ansible reliance on idempotence, to add a host to IdM using Ansible, you must create a playbook in which you define the state of the host as present: *state: present*.
====

.Prerequisites

* You know the IdM administrator password.

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Create an inventory file, for example `inventory.file`, and define `ipaserver` in it:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....

. Create an Ansible playbook file with the `fully-qualified domain name` (FQDN) of the host whose presence in IdM you want to ensure. In addition, if the IdM server is configured to manage DNS and you know the IP address of the host, specify a value for the `ip_address` parameter. The IP address is necessary for the host to exist in the DNS resource records. To simplify this step, you can copy and modify the example in the `/usr/share/doc/ansible-freeipa/playbooks/host/host-present.yml` file. You can also include other, additional information:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Host present
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure host01.idm.example.com is present
    ipahost:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: host01.idm.example.com
      description: Example host
      ip_address: 192.168.0.123
      locality: Lab
      ns_host_location: Lab
      ns_os_version: CentOS 7
      ns_hardware_platform: Lenovo T61
      mac_address:
      - "08:00:27:E3:B1:2D"
      - "52:54:00:BD:97:1E"
      state: present
....

+
. Run the playbook:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i _path_to_inventory_directory/inventory.file_ _path_to_playbooks_directory/ensure-host-is-present.yml_*
....

[NOTE]
====
The procedure results in a host entry in the IdM LDAP server being created but not in enrolling the host into the IdM Kerberos realm. For that, you must deploy the host as an IdM client. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-client-using-an-ansible-playbook_installing-identity-management[Installing an Identity Management client using an Ansible playbook].
====


.Verification

. Log in to your IdM server as admin:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ssh admin@server.idm.example.com*
Password:
....



. Enter the `ipa host-show` command and specify the name of the host:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa host-show host01.idm.example.com*
  Host name: host01.idm.example.com
  Description: Example host
  Locality: Lab
  Location: Lab
  Platform: Lenovo T61
  Operating system: CentOS 7
  Principal name: host/host01.idm.example.com@IDM.EXAMPLE.COM
  Principal alias: host/host01.idm.example.com@IDM.EXAMPLE.COM
  MAC address: 08:00:27:E3:B1:2D, 52:54:00:BD:97:1E
  Password: False
  Keytab: False
  Managed by: host01.idm.example.com
....

The output confirms *host01.idm.example.com* exists in IdM.
