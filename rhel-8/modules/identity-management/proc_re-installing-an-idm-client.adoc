:_mod-docs-content-type: PROCEDURE
[id="re-installing-an-idm-client_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Re-installing an IdM client
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
Install a client on your renamed host following the procedure described in xref:assembly_installing-an-idm-client_{ProjectNameID}[Installing a client].
