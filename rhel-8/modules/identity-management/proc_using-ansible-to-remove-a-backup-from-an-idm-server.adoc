:_mod-docs-content-type: PROCEDURE
[id="proc_using-ansible-to-remove-a-backup-from-an-idm-server_{context}"]
= Using Ansible to remove a backup from an IdM server

[role="_abstract"]
You can use an Ansible playbook to remove a backup from an IdM server.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

.Procedure

. Navigate to the ~/MyPlaybooks/ directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ cd ~/MyPlaybooks/
....

. Make a copy of the `remove-backup-from-server.yml` file located in the `/usr/share/doc/ansible-freeipa/playbooks` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ cp /usr/share/doc/ansible-freeipa/playbooks/remove-backup-from-server.yml remove-backup-from-my-server.yml
....

. Open the `remove-backup-from-my-server.yml` file for editing.

. Adapt the file by setting the following variables:
.. Set the `hosts` variable to a host group from your inventory file. In this example, set it to the `ipaserver` host group.
.. Set the `ipabackup_name` variable to the name of the `ipabackup` to remove from your IdM server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to remove backup from IPA server
  hosts: *ipaserver*
  become: true

  vars:
    ipabackup_name: *ipa-full-2021-04-30-13-12-00*

  roles:
  - role: ipabackup
    state: absent
....

. Save the file.

. Run the Ansible playbook, specifying the inventory file and the playbook file:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ ansible-playbook --vault-password-file=password_file -v -i ~/MyPlaybooks/inventory remove-backup-from-my-server.yml
....

[NOTE]
====
To remove *all* IdM backups from the IdM server, set the `ipabackup_name` variable in the Ansible playbook to `all`:

[literal,subs="+quotes,attributes,verbatim"]
....
  vars:
    ipabackup_name: *all*
....

For an example, see the `remove-all-backups-from-server.yml` Ansible playbook in the `/usr/share/doc/ansible-freeipa/playbooks` directory.
====

[role="_additional-resources"]
.Additional resources
* The `README.md` file in the `/usr/share/doc/ansible-freeipa/roles/ipabackup` directory.
* The `/usr/share/doc/ansible-freeipa/playbooks/` directory.
