:_mod-docs-content-type: CONCEPT
// This module is included in the following assemblies:
//
// assembly_backing-up-and-restoring-idm.adoc
// assembly_recovering-from-data-loss-with-backups.adoc


[id="when-to-restore-from-idm-backup_{context}"]
= When to restore from an IdM backup

[role="_abstract"]
You can respond to several disaster scenarios by restoring from an IdM backup:

* *Undesirable changes were made to the LDAP content*: Entries were modified or deleted, replication carried out those changes throughout the deployment, and you want to revert those changes. Restoring a data-only backup returns the LDAP entries to the previous state without affecting the IdM configuration itself.

* *Total Infrastructure Loss, or loss of all CA instances*: If a disaster damages all Certificate Authority replicas, the deployment has lost the ability to rebuild itself by deploying additional servers. In this situation, restore a backup of a CA Replica and build new replicas from it.

* *An upgrade on an isolated server failed*: The operating system remains functional, but the IdM data is corrupted, which is why you want to restore the IdM system to a known good state. Red Hat recommends working with Technical Support to diagnose and troubleshoot the issue. If those efforts fail, restore from a full-server backup.
+
IMPORTANT: The preferred solution for hardware or upgrade failure is to rebuild the lost server from a replica. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_disaster_recovery_with_identity_management/recovering-a-single-server-with-replication_performing-disaster-recovery[Recovering a single server with replication].
