:_mod-docs-content-type: PROCEDURE
[id="deploying-an-IdM-replica-using-an-Ansible-playbook_{context}"]
= Deploying an IdM replica using an Ansible playbook

Complete this procedure to use an Ansible playbook to deploy an IdM replica.

.Prerequisites
* The managed node is a {RHEL} {ProductNumber} system with a static IP address and a working package manager.
* You have configured https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-replica-using-an-ansible-playbook_installing-identity-management#specifying-the-base-server-and-client-variables-for-installing-the-IdM-replica_replica-ansible[the inventory file for installing an IdM replica].
* You have configured https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-replica-using-an-ansible-playbook_installing-identity-management#specifying-the-credentials-for-installing-the-replica-using-an-ansible-playbook_replica-ansible[the authorization for installing the IdM replica].

.Procedure

* Run the Ansible playbook:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook -i ~/MyPlaybooks/inventory ~/MyPlaybooks/install-replica.yml*
....
