:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="adding-idm-host-groups-members-using-the-cli_{context}"]
= Adding IdM host group members using the CLI

[role="_abstract"]
You can add hosts as well as host groups as members to an IdM host group using a single command.

.Prerequisites

* Administrator privileges for managing IdM or User Administrator role.
* An active Kerberos ticket. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]

* _Optional_. Use the [command]`ipa hostgroup-find` command to find hosts and host groups.

.Procedure

. To add a member to a host group, use the [command]`ipa hostgroup-add-member` and provide the relevant information.
//* `Host-group`: the host group to which you want to add members
//* `[member host]` to add one or more hosts
//* `[member host group]` to add one or more host groups
You can specify the type of member to add using these options: +
* Use the `--hosts` option to add one or more hosts to an IdM host group. +
  For example, to add the host named _example_member_ to the group named _group_name_:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa hostgroup-add-member _group_name_ --hosts _example_member_
Host-group: group_name
Description: My host group
Member hosts: example_member
-------------------------
Number of members added 1
-------------------------
....

* Use the `--hostgroups` option to add one or more host groups to an IdM host group. +
  For example, to add the host group named _nested_group_ to the group named _group_name_:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa hostgroup-add-member _group_name_ --hostgroups _nested_group_
Host-group: group_name
Description: My host group
Member host-groups: nested_group
-------------------------
Number of members added 1
-------------------------
....
// If applicable, the command will return indirect members:
// Indirect Member host-groups:

* You can add multiple hosts and multiple host groups to an IdM host group in one single command using the following syntax: +
//  For example, to add the hosts named _host1_ and _host2_ as well as the host groups named _group1_ and _group2_ to the group named _group_name_, use the following syntax:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa hostgroup-add-member _group_name_ --hosts={_host1,host2_} --hostgroups={_group1,group2_}
....

IMPORTANT: When adding a host group as a member of another host group, do not create recursive groups. For example, if Group A is a member of Group B, do not add Group B as a member of Group A. Recursive groups can cause unpredictable behavior.
