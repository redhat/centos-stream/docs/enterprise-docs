:_mod-docs-content-type: CONCEPT

[id="comparing-hosts-and-users_{context}"]
= Enrollment and authentication of IdM hosts and users: comparison

[role="_abstract"]
There are many similarities between users and hosts in IdM, some of which can be observed during the enrollment stage as well as those that concern authentication during the deployment stage.

* The enrollment stage (xref:user-host-enrollment_{context}[User and host enrollment]):

** An administrator can create an LDAP entry for both a user and a host before the user or host actually join IdM: for the stage user, the command is `ipa stageuser-add`; for the host, the command is `ipa host-add`.
//** If the `ipa host-add` command is run with the `--random` option, it generates a one-time random password. Having access to the one-time password allows an ordinary user to join the host to IdM without the administrator's privileges. The user achieves this by running the `ipa-client-install` command using the `--password` option, supplying the one-time password generated previously.
** A file containing a _key table_ or, abbreviated, keytab, a symmetric key resembling to some extent a user password, is created during the execution of the `ipa-client-install` command on the host, resulting in the host joining the IdM realm. Analogically, a user is asked to create a password when they activate their account, therefore joining the IdM realm.
** While the user password is the default authentication method for a user, the keytab is the default authentication method for a host. The keytab is stored in a file on the host.

+
[id='user-host-enrollment_{context}']
.User and host enrollment
[cols="2,3,3"]
|===
|Action|User|Host


|Pre-enrollment
|$ *ipa stageuser-add _user_name_ [--password]*
|$ *ipa host-add _host_name_ [--random]*

|Activating the account
|$ *ipa stageuser-activate _user_name_*
|$ *ipa-client install [--password]* (must be run on the host itself)
|===


* The deployment stage (xref:user-host-start-session_{context}[User and host session authentication]):

** When a user starts a new session, the user authenticates using a password; similarly, every time it is switched on, the host authenticates by presenting its keytab file. The System Security Services Daemon (SSSD) manages this process in the background.
** If the authentication is successful, the user or host obtains a Kerberos ticket granting ticket (TGT).
** The TGT is then used to obtain specific tickets for specific services.

+
[id="user-host-start-session_{context}"]
.User and host session authentication
[cols="2,3,3"]
|===
||User|Host

|Default means of authentication
|*Password*
|*Keytabs*

|Starting a session (ordinary user)
|$ *kinit _user_name_*
|_[switch on the host]_

|The result of successful authentication
|*TGT* to be used to obtain access to specific services
|*TGT* to be used to obtain access to specific services
|===


TGTs and other Kerberos tickets are generated as part of the Kerberos services and policies defined by the server. The initial granting of a Kerberos ticket, the renewing of the Kerberos credentials, and even the destroying of the Kerberos session are all handled automatically by the IdM services.


[id="alternative-host-authentication_{context}"]
.Alternative authentication options for IdM hosts

Apart from keytabs, IdM supports two other types of machine authentication:

* SSH keys. The SSH public key for the host is created and uploaded to the host entry. From there, the System Security Services Daemon (SSSD) uses IdM as an identity provider and can work in conjunction with OpenSSH and other services to reference the public keys located centrally in IdM.
//This is described in !!!!!!!!!!!!!!!!!!!!!!!!!
//xref:host-keys[insert text if using].

//* Key tables (or _keytabs_, a symmetric key resembling to some extent a user password) and machine certificates.
//Managing Kerberos is covered in !!!!!!!!!!!!!!!!!!!!!!!
//xref:kerberos[insert text if using].

* Machine certificates. In this case, the machine uses an SSL certificate that is issued by the IdM server's certificate authority and then stored in IdM's Directory Server. The certificate is then sent to the machine to present when it authenticates to the server. On the client, certificates are managed by a service called
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_certificates_in_idm/using-certmonger_working-with-idm-certificates#certmonger_certmonger-for-issuing-renewing-service-certs[certmonger].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/using-certmonger_managing-certificates-in-idm#certmonger_certmonger-for-issuing-renewing-service-certs[certmonger].
endif::[]
