:_mod-docs-content-type: CONCEPT

[id="con_constrained-delegation-in-identity-management_{context}"]
= Constrained delegation in Identity Management

The Service for User to Proxy (`S4U2proxy`) extension provides a service that obtains a service ticket to another service on behalf of a user. This feature is known as *constrained delegation*. The second service is typically a proxy performing some work on behalf of the first service, under the authorization context of the user. Using constrained delegation eliminates the need for the user to delegate their full ticket-granting ticket (TGT).

{IPA} (IdM) traditionally uses the Kerberos `S4U2proxy` feature to allow the web server framework to obtain an LDAP service ticket on the user's behalf. The IdM-AD trust system also uses constrained delegation to obtain a `cifs` principal.

You can use the `S4U2proxy` feature to configure a web console client to allow an IdM user that has authenticated with a smart card to achieve the following:

* Run commands with superuser privileges on the RHEL host on which the web console service is running without being asked to authenticate again.
* Access a remote host using `SSH` and access services on the host without being asked to authenticate again.


[role="_additional-resources"]
.Additional resources
////
Optional. Delete if not used.
////
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/assembly_using-constrained-delegation-in-idm_configuring-and-managing-idm#proc_using-ansible-to-configure-a-web-console-to-allow-a-user-authenticated-with-a-smart-card-to-ssh-to-a-remote-host-without-being-asked-to-authenticate-again_assembly_using-constrained-delegation-in-idm[Using Ansible to configure a web console to allow a user authenticated with a smart card to SSH to a remote host without being asked to authenticate again]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/assembly_using-constrained-delegation-in-idm_configuring-and-managing-idm#proc_using-ansible-to-configure-a-web-console-to-allow-a-user-authenticated-with-a-smart-card-to-run-sudo-without-being-asked-to-authenticate-again_assembly_using-constrained-delegation-in-idm[Using Ansible to configure a web console to allow a user authenticated with a smart card to run sudo without being asked to authenticate again]
endif::[]
* link:https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-sfu/bde93b0e-f3c9-4ddf-9f44-e1453be7af5a[S4U2proxy]
* link:https://www.freeipa.org/page/V4/Service_Constraint_Delegation[Service constrained delegation]
