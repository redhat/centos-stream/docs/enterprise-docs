:_mod-docs-content-type: CONCEPT

[id="ad-server-discovery-and-affinity_{context}"]
= AD server discovery and affinity

Server discovery and affinity configuration affects which {AD} (AD) servers an {IPA} (IdM) client communicates with in a cross-forest trust between IdM and AD.

Configuring clients to prefer servers in the same geographical location helps prevent time lags and other problems that occur when clients contact servers from another, remote data center. To verify clients communicate with local servers, you must ensure that:

* Clients communicate with local IdM servers over LDAP and over Kerberos
* Clients communicate with local AD servers over Kerberos
* Embedded clients on IdM servers communicate with local AD servers over LDAP and over Kerberos

[discrete]
== Options for configuring LDAP and Kerberos on the IdM client for communication with local IdM servers

.When using IdM with integrated DNS
By default, clients use automatic service lookup based on the DNS records. In this setup, you can also use the _DNS locations_ feature to configure DNS-based service discovery.

To override the automatic lookup, you can disable the DNS discovery in one of the following ways:

* During the IdM client installation by providing failover parameters from the command line
* After the client installation by modifying the {SSSD} (SSSD) configuration

.When using IdM without integrated DNS
You must explicitly configure clients in one of the following ways:

* During the IdM client installation by providing failover parameters from the command line
* After the client installation by modifying the SSSD configuration

[discrete]
== Options for configuring Kerberos on the IdM client for communication with local AD servers

IdM clients are unable to automatically discover which AD servers to communicate with. To specify the AD servers manually, modify the `krb5.conf` file:

* Add the AD realm information
* Explicitly list the AD servers to communicate with

For example:

[literal,subs="+quotes,attributes,verbatim"]
....
[realms]
AD.EXAMPLE.COM = {
kdc = server1.ad.example.com
kdc = server2.ad.example.com
}
....

[discrete]
== Options for configuring embedded clients on IdM servers for communication with local AD servers over Kerberos and LDAP

The embedded client on an IdM server works also as a client of the AD server. It can automatically discover and use the appropriate AD site.

When the embedded client performs the discovery, it might first discover an AD server in a remote location. If the attempt to contact the remote server takes too long, the client might stop the operation without establishing the connection. Use the `dns_resolver_timeout` option in the `sssd.conf` file on the client to increase the amount of time for which the client waits for a reply from the DNS resolver. See the _sssd.conf(5)_ man page for details.

Once the embedded client has been configured to communicate with the local AD servers, the SSSD remembers the AD site the embedded client belongs to. Thanks to this, SSSD normally sends an LDAP ping directly to a local domain controller to refresh its site information. If the site no longer exists or the client has meanwhile been assigned to a different site, SSSD starts querying for SRV records in the forest and goes through a whole process of autodiscovery.

Using _trusted domain sections_ in `sssd.conf`, you can also explicitly override some of the information that is discovered automatically by default.

[role="_additional-resources"]
.Additional resources
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumber}/html-single/working_with_dns_in_identity_management/index[Working with DNS in Identity Management]