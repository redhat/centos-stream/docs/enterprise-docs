:_newdoc-version: 2.15.0
:_template-generated: 2024-6-28
:_mod-docs-content-type: PROCEDURE

[id="accessing-delegated-services_{context}"]
= Accessing delegated services

[role="_abstract"]
When a client has delegated authority, it can obtain a keytab for the principal on the local machine for both services and hosts.

With the `kinit` command, use the `-k` option to load a keytab and the `-t` option to specify the keytab. The principal format is `<principal>/hostname@REALM`. For a service, replace `<principal>` with the service name, for example HTTP. For a host, use `host` as the principal.

.Procedure

* To access a host:
+
[literal,subs="+quotes",options="nowrap"]
----
[root@server ~]# kinit -kt /etc/krb5.keytab host/ipa.example.com@EXAMPLE.COM
----

* To access a service:
+
[literal,subs="+quotes",options="nowrap"]
----
[root@server ~]# kinit -kt /etc/httpd/conf/krb5.keytab HTTP/ipa.example.com@EXAMPLE.COM
----
