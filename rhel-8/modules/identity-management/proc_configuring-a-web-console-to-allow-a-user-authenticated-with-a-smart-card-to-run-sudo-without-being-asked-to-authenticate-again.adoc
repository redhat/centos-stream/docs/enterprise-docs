
:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-a-web-console-to-allow-a-user-authenticated-with-a-smart-card-to-run-sudo-without-being-asked-to-authenticate-again_{context}"]
= Configuring a web console to allow a user authenticated with a smart card to run sudo without being asked to authenticate again

After you have logged in to a user account on the RHEL web console, as an {IPA} (IdM) system administrator you might need to run commands with superuser privileges. You can use the xref:con_constrained-delegation-in-identity-management_{context}[constrained delegation] feature to run `sudo` on the system without being asked to authenticate again.

Follow this procedure to configure a web console to use constrained delegation. In the example below, the web console session runs on the *myhost.idm.example.com* host.


.Prerequisites

* You have obtained an IdM `admin` ticket-granting ticket (TGT).
* The web console service is present in IdM.
* The *myhost.idm.example.com* host is present in IdM.
* You have link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_8_web_console/configuring_single_sign_on_for_the_rhel_8_web_console_in_the_idm_domain_system-management-using-the-rhel-8-web-console#enabling-admin-sudo-access-to-domain-administrators-on-the-idm-server_configuring-single-sign-on-for-the-web-console-in-the-idm-domain[enabled `admin` `sudo` access to domain administrators on the IdM server].
* The web console has created an `S4U2Proxy` Kerberos ticket in the user session. To verify that this is the case, log in to the web console as an IdM user, open the `Terminal` page, and enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *klist*
Ticket cache: FILE:/run/user/1894000001/cockpit-session-3692.ccache
Default principal: user@IDM.EXAMPLE.COM

Valid starting     Expires            Service principal
*07/30/21 09:19:06  07/31/21 09:19:06  HTTP/myhost.idm.example.com@IDM.EXAMPLE.COM*
07/30/21 09:19:06  07/31/21 09:19:06  krbtgt/IDM.EXAMPLE.COM@IDM.EXAMPLE.COM
        for client HTTP/myhost.idm.example.com@IDM.EXAMPLE.COM
....



.Procedure

. Create a list of the target hosts that can be accessed by the delegation rule:

.. Create a service delegation target:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa servicedelegationtarget-add cockpit-target*
....

.. Add the target host to the delegation target:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa servicedelegationtarget-add-member cockpit-target \*
  *--principals=host/myhost.idm.example.com@IDM.EXAMPLE.COM*
....

. Allow `cockpit` sessions to access the target host list by creating a service delegation rule and adding the `HTTP` service Kerberos principal to it:

.. Create a service delegation rule:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa servicedelegationrule-add cockpit-delegation*
....

.. Add the web console service to the delegation rule:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa servicedelegationrule-add-member cockpit-delegation \*
  *--principals=HTTP/myhost.idm.example.com@IDM.EXAMPLE.COM*
....

.. Add the delegation target to the delegation rule:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa servicedelegationrule-add-target cockpit-delegation \*
  *--servicedelegationtargets=cockpit-target*
....


. Enable `pam_sss_gss`, the PAM module for authenticating users over the Generic Security Service Application Program Interface (GSSAPI) in cooperation with the System Security Services Daemon (SSSD):

.. Open the `/etc/sssd/sssd.conf` file for editing.

.. Specify that `pam_sss_gss` can provide authentication for the `sudo` and `sudo -i` commands in IdM your domain:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[domain/idm.example.com]
*pam_gssapi_services = sudo, sudo-i*
....

.. Save and exit the file.

.. Open the `/etc/pam.d/sudo` file for editing.

.. Insert the following line to the top of the `#%PAM-1.0` list to allow, but not require, GSSAPI authentication for `sudo` commands:
+
[literal,subs="+quotes,attributes,verbatim"]
....
*auth sufficient pam_sss_gss.so*
....

.. Save and exit the file.

. Restart the `SSSD` service so that the above changes take effect immediately:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *systemctl restart sssd*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_systems_using_the_rhel_8_web_console/configuring-smart-card-authentication-with-the-web-console_system-management-using-the-rhel-8-web-console#logging-in-to-the-web-console-with-smart-cards_configuring-smart-card-authentication-with-the-web-console[Logging in to the web console with smart cards]
* xref:con_constrained-delegation-in-identity-management_{context}[Constrained delegation in Identity Management]
