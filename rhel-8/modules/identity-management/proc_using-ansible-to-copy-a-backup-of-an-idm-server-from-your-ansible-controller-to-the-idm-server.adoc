:_mod-docs-content-type: PROCEDURE
[id="proc_using-ansible-to-copy-a-backup-of-an-idm-server-from-your-ansible-controller-to-the-idm-server_{context}"]
= Using Ansible to copy a backup of an IdM server from your Ansible controller to the IdM server

[role="_abstract"]
You can use an Ansible playbook to copy a backup of an IdM server from your Ansible controller to the IdM server.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

.Procedure

. Navigate to the `~/MyPlaybooks/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ cd ~/MyPlaybooks/
....

. Make a copy of the `copy-backup-from-controller.yml` file located in the `/usr/share/doc/ansible-freeipa/playbooks` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ cp /usr/share/doc/ansible-freeipa/playbooks/copy-backup-from-controller.yml copy-backup-from-my-controller-to-my-server.yml
....

. Open the `copy-my-backup-from-my-controller-to-my-server.yml` file for editing.

. Adapt the file by setting the following variables:
.. Set the `hosts` variable to a host group from your inventory file. In this example, set it to the `ipaserver` host group.
.. Set the `ipabackup_name` variable to the name of the `ipabackup` on your Ansible controller to copy to the IdM server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to copy a backup from controller to the IPA server
  hosts: *ipaserver*
  become: true

  vars:
    ipabackup_name: *server.idm.example.com_ipa-full-2021-04-30-13-12-00*
    ipabackup_from_controller: true

  roles:
  - role: ipabackup
    state: copied
....

. Save the file.

. Run the Ansible playbook, specifying the inventory file and the playbook file:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ ansible-playbook --vault-password-file=password_file -v -i ~/MyPlaybooks/inventory copy-backup-from-my-controller-to-my-server.yml
....

[role="_additional-resources"]
.Additional resources
* The `README.md` file in the `/usr/share/doc/ansible-freeipa/roles/ipabackup` directory.
* The `/usr/share/doc/ansible-freeipa/playbooks/` directory.
