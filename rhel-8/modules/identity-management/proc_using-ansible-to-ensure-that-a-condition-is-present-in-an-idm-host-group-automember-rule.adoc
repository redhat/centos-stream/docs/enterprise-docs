:_mod-docs-content-type: PROCEDURE
[id="using-ansible-to-ensure-that-a-condition-is-present-in-an-idm-host-group-automember-rule_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using Ansible to ensure that a condition is present in an IdM host group automember rule
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

:my_domain_hosts: .*.idm.example.com
:external_domain_hosts: .*.example.org

[role="_abstract"]
Follow this procedure to use Ansible to ensure that a condition is present in an IdM host group automember rule. The example describes how to ensure that hosts with the `FQDN` of *{my_domain_hosts}* are members of the *primary_dns_domain_hosts* host group and hosts whose `FQDN` is *{external_domain_hosts}* are not members of the *primary_dns_domain_hosts* host group.

.Prerequisites

* You know the IdM `admin` password.
* The *primary_dns_domain_hosts* host group and automember host group rule exist in IdM.
* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Copy the `automember-hostgroup-rule-present.yml` Ansible playbook file located in the `/usr/share/doc/ansible-freeipa/playbooks/automember/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/automember/automember-hostgroup-rule-present.yml automember-hostgroup-rule-present-copy.yml*
....

. Open the `automember-hostgroup-rule-present-copy.yml` file for editing.

. Adapt the file by setting the following variables in the `ipaautomember` task section:

** Set the `ipaadmin_password` variable to the password of the IdM `admin`.
** Set the `name` variable to *primary_dns_domain_hosts*.
** Set the `automember_type` variable to *hostgroup*.
** Ensure that the `state` variable is set to `present`.
** Ensure that the `action` variable is set to `member`.
** Ensure that the `inclusive` `key` variable is set to `fqdn`.
** Set the corresponding `inclusive` `expression` variable to *{my_domain_hosts}*.
** Set the `exclusive` `key` variable to `fqdn`.
** Set the corresponding `exclusive` `expression` variable to *{external_domain_hosts}*.

+
--

This is the modified Ansible playbook file for the current example:

[subs="+quotes,attributes"]
....
---
- name: Automember user group rule member present
  hosts: ipaserver
  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure an automember condition for a user group is present
    ipaautomember:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: primary_dns_domain_hosts*
      *automember_type: hostgroup*
      *state: present*
      *action: member*
      inclusive:
        *- key: fqdn*
          *expression: {my_domain_hosts}*
      exclusive:
        - *key: fqdn*
          *expression: {external_domain_hosts}*
....

--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory automember-hostgroup-rule-present-copy.yml*
....

[role="_additional-resources"]
.Additional resources
ifndef::u-a-in-idm[]
* See xref:applying-automember-rules-to-existing-entries-using-idm-cli_automating-group-membership-using-idm-cli[Applying automember rules to existing entries using the IdM CLI].
* See xref:benefits-of-automatic-group-membership_automating-group-membership-using-idm-cli[Benefits of automatic group membership] and xref:automember-rules_automating-group-membership-using-idm-cli[Automember rules].
endif::[]
* See the `README-automember.md` file in the `/usr/share/doc/ansible-freeipa/` directory.
* See the `/usr/share/doc/ansible-freeipa/playbooks/automember` directory.
