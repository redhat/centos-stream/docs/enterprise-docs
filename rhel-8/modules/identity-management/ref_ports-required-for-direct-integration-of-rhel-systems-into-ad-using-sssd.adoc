:_newdoc-version: 2.18.3
:_template-generated: 2024-12-16

:_mod-docs-content-type: REFERENCE

[id="ports-required-for-direct-integration-of-rhel-systems-into-ad-using-sssd_{context}"]
= Ports required for direct integration of RHEL systems into AD using SSSD

The following ports must be open and accessible to the AD domain controllers and the RHEL host.

.Ports Required for Direct Integration of Linux Systems into AD Using SSSD
[options="header"]
|===
|Service|Port|Protocol|Notes
|DNS|53|UDP and TCP|
|LDAP|389|UDP and TCP|
|LDAPS|636|TCP|Optional
|Samba|445|UDP and TCP|For AD Group Policy Objects (GPOs)
|Kerberos|88|UDP and TCP|
|Kerberos|464|UDP and TCP|Used by `kadmin` for setting and changing a password
|LDAP Global Catalog|3268|TCP|If the [option]`id_provider = ad` option is being used
|LDAPS Global Catalog|3269|TCP|Optional
|NTP|123|UDP|Optional
|NTP|323|UDP|Optional
|===
