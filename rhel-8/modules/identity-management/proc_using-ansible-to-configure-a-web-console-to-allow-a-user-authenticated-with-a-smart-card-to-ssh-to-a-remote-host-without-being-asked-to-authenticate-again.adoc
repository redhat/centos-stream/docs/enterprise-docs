
:_mod-docs-content-type: PROCEDURE

[id="proc_using-ansible-to-configure-a-web-console-to-allow-a-user-authenticated-with-a-smart-card-to-ssh-to-a-remote-host-without-being-asked-to-authenticate-again_{context}"]
= Using Ansible to configure a web console to allow a user authenticated with a smart card to SSH to a remote host without being asked to authenticate again


After you have logged in to a user account on the RHEL web console, as an {IPA} (IdM) system administrator you might need to connect to remote machines by using the `SSH` protocol. You can use the link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/assembly_using-constrained-delegation-in-idm_configuring-and-managing-idm#con_constrained-delegation-in-identity-management_assembly_using-constrained-delegation-in-idm[constrained delegation] feature to use `SSH` without being asked to authenticate again.

Follow this procedure to use the `servicedelegationrule` and `servicedelegationtarget` `ansible-freeipa` modules to configure a web console to use constrained delegation. In the example below, the web console session runs on the *myhost.idm.example.com* host and it is being configured to access the *remote.idm.example.com* host by using `SSH` on behalf of the authenticated user.


.Prerequisites

* The IdM `admin` password.
* `root` access to *remote.idm.example.com*.
* The web console service is present in IdM.
* The *remote.idm.example.com* host is present in IdM.
* The web console has created an `S4U2Proxy` Kerberos ticket in the user session. To verify that this is the case, log in to the web console as an IdM user, open the `Terminal` page, and enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *klist*
Ticket cache: FILE:/run/user/1894000001/cockpit-session-3692.ccache
Default principal: user@IDM.EXAMPLE.COM

Valid starting     Expires            Service principal
*07/30/21 09:19:06  07/31/21 09:19:06  HTTP/myhost.idm.example.com@IDM.EXAMPLE.COM*
07/30/21 09:19:06  07/31/21 09:19:06  krbtgt/IDM.EXAMPLE.COM@IDM.EXAMPLE.COM
        for client HTTP/myhost.idm.example.com@IDM.EXAMPLE.COM
....

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....




. Create a `web-console-smart-card-ssh.yml` playbook with the following content:

.. Create a task that ensures the presence of a delegation target:
+
[literal,subs="+quotes,attributes"]
....
---
- name: Playbook to create a constrained delegation target
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure servicedelegationtarget web-console-delegation-target is present
    ipaservicedelegationtarget:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: web-console-delegation-target
....

.. Add a task that adds the target host to the delegation target:
+
[literal,subs="+quotes,attributes"]
....
  - name: Ensure servicedelegationtarget web-console-delegation-target member principal host/remote.idm.example.com@IDM.EXAMPLE.COM is present
    ipaservicedelegationtarget:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: web-console-delegation-target
      principal: host/remote.idm.example.com@IDM.EXAMPLE.COM
      action: member
....


.. Add a task that ensures the presence of a delegation rule:
+
[literal,subs="+quotes,attributes"]
....
  - name: Ensure servicedelegationrule delegation-rule is present
    ipaservicedelegationrule:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: web-console-delegation-rule
....



.. Add a task that ensures that the Kerberos principal of the web console client service is a member of the constrained delegation rule:
+
[literal,subs="+quotes,attributes"]
....
  - name: Ensure the Kerberos principal of the web console client service is added to the servicedelegationrule web-console-delegation-rule
    ipaservicedelegationrule:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: web-console-delegation-rule
      principal: HTTP/myhost.idm.example.com
      action: member
....




.. Add a task that ensures that the constrained delegation rule is associated with the web-console-delegation-target delegation target:
+
[literal,subs="+quotes,attributes"]
....
  - name: Ensure a constrained delegation rule is associated with a specific delegation target
    ipaservicedelegationrule:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: web-console-delegation-rule
      target: web-console-delegation-target
      action: member
....

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory web-console-smart-card-ssh.yml*
....

. Enable Kerberos authentication on *remote.idm.example.com*:

.. `SSH` to *remote.idm.example.com* as `root`.

.. Open the `/etc/ssh/sshd_config` file for editing.

.. Enable `GSSAPIAuthentication` by uncommenting the `GSSAPIAuthentication no` line and replacing it with `GSSAPIAuthentication yes`.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/configuring-smart-card-authentication-with-the-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console-with-smart-cards_configuring-smart-card-authentication-with-the-web-console[Logging in to the web console with smart cards]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/assembly_using-constrained-delegation-in-idm_configuring-and-managing-idm#con_constrained-delegation-in-identity-management_assembly_using-constrained-delegation-in-idm[Constrained delegation in Identity Management]
* `README-servicedelegationrule.md` and `README-servicedelegationtarget.md` in the `/usr/share/doc/ansible-freeipa/` directory
* Sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/servicedelegationtarget` and `/usr/share/doc/ansible-freeipa/playbooks/servicedelegationrule` directories
