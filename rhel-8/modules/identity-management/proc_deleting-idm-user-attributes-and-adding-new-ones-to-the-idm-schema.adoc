:_newdoc-version: 2.18.3
:_template-generated: 2024-11-11
:_mod-docs-content-type: PROCEDURE

[id="deleting-idm-user-attributes-and-adding-new-ones-to-the-idm-schema_{context}"]
= Deleting IdM user attributes and adding new ones to the IdM schema

You can not only xref:viewing-and-modifying-user-and-group-configuration-in-the-idm-cli_{context}[modify the current attributes in IdM] but also delete them or add new ones.

.Procedure

* To add a new attribute, use the `ipa config-mod` command with the `--addattr` option. For example, to add the `assistant` attribute available in the `organizationalperson` user object class: 

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa config-mod --addattr ipauserobjectclasses=assistant*
....


* To delete an attribute, use the `ipa config-mod` command with the `--delattr` option, for example:  

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa config-mod --delattr ipauserobjectclasses=assistant*
....
 

[NOTE]
====
The new attribute must be part of the user object classes that are currently configured.

For more information about how to add a new user object class, see xref:modifying-user-object-classes-in-the-idm-cli_{context}[Modifying user object classes in the IdM CLI]. 
====

[NOTE]
====
The general command to determine the LDAP attribute used by IdM for the specific option of a specific `ipa` command is `ipa show-mappings _<command>_`. Therefore, to find out what _user object classes_ are configured for IdM, enter, for example: `ipa show-mappings config-mod | grep userobjectclasses`. The output shows "`ipauserobjectclasses*`".

This means that

* To add an attribute to the list of user object classes, you must enter `ipa config-mod --addattr ipauserobjectclasses=_<newattribute>_`. 
* To delete an attribute, you must enter `ipa config-mod --delattr ipauserobjectclasses=_<existingattribute>_`. 

The sign immediately after the attribute name in the `ipa show-mappings` output indicates one of the following:

* The attribute is mandatory. This applies if no sign follows the attribute name (for singular) or if the *+* sign follows the attribute name (for multi-valued).
* The attribute is optional. This applies if the *?* sign follows the attribute name (for singular) or if the *** sign follows the attribute name (for multi-valued, which is the case with *ipauserobjectclasses* above).
====