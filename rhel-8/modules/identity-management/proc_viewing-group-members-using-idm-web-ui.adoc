:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="viewing-group-members-using-idm-web-ui_{context}"]
= Viewing group members using IdM Web UI

[role="_abstract"]
Follow this procedure to view members of a group using the IdM Web UI. You can view both direct and indirect group members. For more information, see xref:direct-and-indirect-group-members_{context}[Direct and indirect group members].

.Prerequisites

* You are logged in to the IdM Web UI.

.Procedure

. Select *Identity -> Groups*.
. Select *User Groups* in the left sidebar.
. Click the name of the group you want to view.
. Switch between *Direct Membership* and *Indirect Membership*.
+
image::groups-menu-clean.png[A screenshot showing radial buttons next to the "Direct Membership" and "Indirect Membership" options next to "Show Results."]
