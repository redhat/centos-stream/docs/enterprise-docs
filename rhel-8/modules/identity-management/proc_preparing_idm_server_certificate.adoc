:_mod-docs-content-type: PROCEDURE
:parent-context: {context}
// [[cert-idm-users-auth-preparing-the-server_{context}]]
[id="cert-idm-users-auth-preparing-the-server_{context}"]

= Configuring the Identity Management Server for Certificate Authentication in the Web UI

[role="_abstract"]
As an Identity{nbsp}Management (IdM) administrator, you can allow users to use certificates to authenticate to your IdM environment.

.Procedure
As the Identity{nbsp}Management administrator:
////
. Obtain administrator privileges:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# kinit admin
....
+
////
. On an Identity{nbsp}Management server, obtain administrator privileges and create a shell script to configure the server.
+
.. Run the [command]`ipa-advise config-server-for-smart-card-auth` command, and save its output to a file, for example [replaceable]`server_certificate_script.sh`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# kinit admin
# ipa-advise config-server-for-smart-card-auth > [replaceable]`server_certificate_script.sh`
....

+
.. Add execute permissions to the file using the `chmod` utility:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# chmod +x [replaceable]`server_certificate_script.sh`
....

. On all the servers in the Identity{nbsp}Management domain, run the [replaceable]`server_certificate_script.sh` script

.. with the path of the IdM Certificate Authority certificate, [filename]`/etc/ipa/ca.crt`, as input if the IdM CA is the only certificate authority that has issued the certificates of the users you want to enable certificate authentication for:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# [replaceable]`./server_certificate_script.sh` [filename]`/etc/ipa/ca.crt`
....

+
.. with the paths leading to the relevant CA certificates as input if different external CAs signed the certificates of the users who you want to enable certificate authentication for:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# [replaceable]`./server_certificate_script.sh` [replaceable]`/tmp/ca1.pem` [replaceable]`/tmp/ca2.pem`
....

[NOTE]
====
Do not forget to run the script on each new replica that you add to the system in the future if you want to have certificate authentication for users enabled in the whole topology.
====

////
deprecated because since RHEL7.4, the dbus package is installed by default:
+
. On all the servers in the IdM domain, make sure that the [package]*sssd-dbus* package is installed:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# {PackageManagerCommand} info sssd-dbus
....
////

:context: {parent-context}
