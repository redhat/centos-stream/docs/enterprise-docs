:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_migrating-from-nis-to-identity-managment.adoc


[id="proc_migrating-user-group-from-nis-to-idm_{context}"]
[[nis-import-groups]]
= Migrating user group from NIS to IdM

[role="_abstract"]
The NIS `group` map contains information about groups, such as group names, GIDs, or group members. Use this data to migrate NIS groups to {IPA} (IdM):

.Prerequisites
* You have root access on NIS server.
* xref:enabling-nis[NIS is enabled in IdM.]
* The NIS server is enrolled into IdM.

.Procedure
. Install the `yp-tools` package:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@nis-server ~]# {PackageManagerCommand} install yp-tools -y
....

. Create the `/root/nis-groups.sh` script with the following content on the NIS server:
+
[source,bash]
....
#!/bin/sh
# $1 is the NIS domain, $2 is the primary NIS server
ypcat -d $1 -h $2 group > /dev/shm/nis-map.group 2>&1

IFS=$'\n'
for line in $(cat /dev/shm/nis-map.group); do
	IFS=' '
	groupname=$(echo $line | cut -f1 -d:)
	# Not collecting encrypted password because we need cleartext password
	# to create kerberos key
	gid=$(echo $line | cut -f3 -d:)
	members=$(echo $line | cut -f4 -d:)

	# Now create this entry
	ipa group-add $groupname --desc=NIS_GROUP_$groupname --gid=$gid
	if [ -n "$members" ]; then
		ipa group-add-member $groupname --users={$members}
	fi
	ipa group-show $groupname
done

....

. Authenticate as the IdM `admin` user:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@nis-server ~]# kinit admin
....

. Run the script. For example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@nis-server ~]# sh /root/nis-groups.sh pass:quotes[_nisdomain_] pass:quotes[_nis-server.example.com_]
....
