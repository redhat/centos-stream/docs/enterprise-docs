:_mod-docs-content-type: REFERENCE
// This module is included in the following assemblies:
//
// assembly_granting-sudo-access-to-an-IdM-user-on-an-IdM-client.adoc


[id="ref_sssd-options-controlling-gssapi-authentication-for-pam-services_{context}"]
= SSSD options controlling GSSAPI authentication for PAM services

[role="_abstract"]
You can use the following options for the `/etc/sssd/sssd.conf` configuration file to adjust the GSSAPI configuration within the SSSD service.


pam_gssapi_services::
GSSAPI authentication with SSSD is disabled by default. You can use this option to specify a comma-separated list of PAM services that are allowed to try GSSAPI authentication using the `pam_sss_gss.so` PAM module. To explicitly disable GSSAPI authentication, set this option to `-`.


pam_gssapi_indicators_map::
This option only applies to Identity Management (IdM) domains. Use this option to list Kerberos authentication indicators that are required to grant PAM access to a service. Pairs must be in the format `_<PAM_service>_:_<required_authentication_indicator>_`.
+
Valid authentication indicators are:
+
* `otp` for two-factor authentication
* `radius` for RADIUS authentication
* `pkinit` for PKINIT, smart card, or certificate authentication
* `hardened` for hardened passwords


pam_gssapi_check_upn::
This option is enabled and set to `true` by default. If this option is enabled, the SSSD service requires that the user name matches the Kerberos credentials. If `false`, the `pam_sss_gss.so` PAM module authenticates every user that is able to obtain the required service ticket.


.Examples

The following options enable Kerberos authentication for the `sudo` and `sudo-i` services, requires that `sudo` users authenticated with a one-time password, and user names must match the Kerberos principal. Because these settings are in the `[pam]` section, they apply to all domains:

[literal,subs="+quotes,attributes,verbatim"]
....
[pam]
*pam_gssapi_services* = *_sudo_, _sudo-i_*
*pam_gssapi_indicators_map* = *_sudo:otp_*
*pam_gssapi_check_upn* = *_true_*
....

You can also set these options in individual `[domain]` sections to overwrite any global values in the `[pam]` section. The following options apply different GSSAPI settings to each domain:

For the `idm.example.com` domain::
* Enable GSSAPI authentication for the `sudo` and `sudo -i` services.
* Require certificate or smart card authentication authenticators for the `sudo` command.
* Require one-time password authentication authenticators for the `sudo -i` command.
* Enforce matching user names and Kerberos principals.

For the `ad.example.com` domain::
* Enable GSSAPI authentication only for the `sudo` service.
* Do not enforce matching user names and principals.

[literal,subs="+quotes,attributes,verbatim"]
....
[domain/*_idm.example.com_*]
pam_gssapi_services = *sudo, sudo-i*
pam_gssapi_indicators_map = *_sudo:pkinit_*, *_sudo-i:otp_*
pam_gssapi_check_upn = *true*
...

[domain/*_ad.example.com_*]
pam_gssapi_services = *sudo*
pam_gssapi_check_upn = *false*
...
....


[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/managing-kerberos-ticket-policies_configuring-and-managing-idm#kerberos-authentication-indicators_managing-kerberos-ticket-policies[Kerberos authentication indicators]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_idm_users_groups_hosts_and_access_control_rules/managing-kerberos-ticket-policies_managing-users-groups-hosts#kerberos-authentication-indicators_managing-kerberos-ticket-policies[Kerberos authentication indicators]
endif::[]
