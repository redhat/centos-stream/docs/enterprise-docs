:_mod-docs-content-type: PROCEDURE
[id="using-ansible-to-ensure-an-rbac-permission-with-an-attribute-is-present_{context}"]
= Using Ansible to ensure an RBAC permission with an attribute is present

[role="_abstract"]
As a system administrator of {IPA} (IdM), you can customize the IdM role-based access control (RBAC).

The following procedure describes how to use an Ansible playbook to ensure a permission is present in IdM so that it can be added to a privilege. The example describes how to ensure the following target state:

* The *MyPermission* permission exists.
* The *MyPermission* permission can only be used to add hosts.
* A user granted a privilege that contains the permission can do all of the following possible operations on a host entry:

** Write
** Read
** Search
** Compare
** Add
** Delete

* The host entries created by a user that is granted a privilege that contains the *MyPermission* permission can have a `description` value.

[NOTE]
====
The type of attribute that you can specify when creating or modifying a permission is not constrained by the IdM LDAP schema. However, specifying, for example, `attrs: car_licence` if the `object_type` is `host` later results in the `ipa: ERROR: attribute "car-license" not allowed` error message when you try to exercise the permission and add a specific car licence value to a host.
====


.Prerequisites

* On the control node:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Navigate to the *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Make a copy of the `permission-present.yml` file located in the `/usr/share/doc/ansible-freeipa/playbooks/permission/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/permission/permission-present.yml permission-present-with-attribute.yml*
....

. Open the `permission-present-with-attribute.yml` Ansible playbook file for editing.

. Adapt the file by setting the following variables in the `ipapermission` task section:

* Adapt the `name` of the task to correspond to your use case.
* Set the `ipaadmin_password` variable to the password of the IdM administrator.
* Set the `name` variable to the name of the permission.
* Set the `object_type` variable to `host`.
* Set the `right` variable to `all`.
* Set the `attrs` variable to `description`.

+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Permission present example
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - *name: Ensure that the "MyPermission" permission is present with an attribute*
    ipapermission:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: MyPermission*
      *object_type: host*
      *right: all*
      *attrs: description*
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory permission-present-with-attribute.yml*
....

[role="_additional-resources"]
.Additional resources
* See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/user-schema[User and group schema] in __Linux Domain Identity, Authentication and Policy Guide__ in RHEL 7.
