:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// planning-server-services-on-the-ipa-domain.adoc
// planning-the-replica-topology.adoc
// overview-of-identity-management-and-access-control.adoc

[id="introduction-to-ipa-servers-and-clients_{context}"]
= Introduction to IdM servers and clients

The {IPA} (IdM) domain includes the following types of systems:

IdM clients:: IdM clients are {RHEL} systems enrolled with the servers and configured to use the IdM services on these servers.
+
Clients interact with the IdM servers to access services provided by them. For example, clients use the Kerberos protocol to perform authentication and acquire tickets for enterprise single sign-on (SSO), use LDAP to get identity and policy information, and use DNS to detect where the servers and services are located and how to connect to them.


IdM servers:: IdM servers are {RHEL} systems that respond to identity, authentication, and authorization requests from IdM clients within an IdM domain. IdM servers are the central repositories for identity and policy information. They can also host any of the optional services used by domain members:

+
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_certificates_in_idm/cert-intro_working-with-idm-certificates#certificate_authorities_in_idm[Certificate authority] (CA): This service is present in most IdM deployments.
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/cert-intro_managing-certificates-in-idm#certificate_authorities_in_idm[Certificate authority] (CA): This service is present in most IdM deployments.
endif::[]
* Key Recovery Authority (KRA)
* DNS
* Active Directory (AD) trust controller
* Active Directory (AD) trust agent


+
IdM servers are also embedded IdM clients. As clients enrolled with themselves, the servers provide the same functionality as other clients.

To provide services for large numbers of clients, as well as for redundancy and availability, IdM allows deployment on multiple IdM servers in a single domain. It is possible to deploy up to 60 servers. This is the maximum number of IdM servers, also called replicas, that is currently supported in the IdM domain. 

When creating a replica, IdM clones the configuration of the existing server. A replica shares with the initial server its core configuration, including internal information about users, systems, certificates, and configured policies. 

NOTE:: A replica and the server it was created from are functionally identical, except for the _CA renewal_ and _CRL publisher_ roles. Therefore, the term *_server_* and *_replica_* are used interchangeably in RHEL IdM documentation, depending on the context.

However, different IdM servers can provide different services for the client, if so configured. Core components like Kerberos and LDAP are available on every server. Other services like CA, DNS, Trust Controller or Vault are optional. This means that different IdM servers can have distinct roles in the deployment.

If your IdM topology contains an integrated CA, one server has the role of the
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_certificates_in_idm/generating-crl-on-the-idm-ca-server_working-with-idm-certificates[Certificate revocation list (CRL) publisher server] and one server has the role of the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_certificates_in_idm/ipa-ca-renewal_working-with-idm-certificates[CA renewal server].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/generating-crl-on-the-idm-ca-server_managing-certificates-in-idm[Certificate revocation list (CRL) publisher server] and one server has the role of the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/ipa-ca-renewal_managing-certificates-in-idm[CA renewal server].
endif::[]

By default, the first CA server installed fulfills these two roles, but you can assign these roles to separate servers.

WARNING: The __CA renewal server__ is critical for your IdM deployment because it is the only system in the domain responsible for tracking CA subsystem
ifeval::["{ProductNumber}" == "8"]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_certificates_in_idm/cert-intro_working-with-idm-certificates[certificates and keys].
endif::[]
ifeval::["{ProductNumber}" == "9"]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/cert-intro_managing-certificates-in-idm[certificates and keys].
endif::[]
For details about how to recover from a disaster affecting your IdM deployment, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_disaster_recovery_with_identity_management/index[Performing disaster recovery with Identity Management].

NOTE::
ifeval::["{ProductNumber}" == "8"]
All IdM servers (for clients, see xref:supported-versions-of-rhel-for-installing-idm-clients_overview-of-idm-and-access-control[Supported versions of RHEL for installing IdM clients]) must be running on the same major and minor version of RHEL. Do not spend more than several days applying z-stream updates or upgrading the IdM servers in your topology. For details about how to apply Z-stream fixes and upgrade your servers, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html-single/installing_identity_management/index#updating_idm_packages[Updating IdM packages]. For details about how to migrate to IdM on RHEL 8, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html-single/migrating_to_identity_management_on_rhel_8/index#migrate-7-to-8_migrating[Migrating your IdM environment from RHEL 7 servers to RHEL 8 servers].
endif::[]

ifeval::["{ProductNumber}" == "9"]
All IdM servers (for clients, see xref:supported-versions-of-rhel-for-installing-idm-clients_overview-of-idm-and-access-control[Supported versions of RHEL for installing IdM clients]) must be running on the same major and minor version of RHEL. Do not spend more than several days applying z-stream updates or upgrading the IdM servers in your topology. For details about how to apply Z-stream fixes and upgrade your servers, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/installing_identity_management/index#updating_idm_packages[Updating IdM packages]. For details about how to migrate to IdM on RHEL 9, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/migrating_to_identity_management_on_rhel_9/index#assembly_migrating-your-idm-environment-from-rhel-8-servers-to-rhel-9-servers_migrating-to-idm-on-rhel-9[Migrating your IdM environment from RHEL 8 servers to RHEL 9 servers].
endif::[]