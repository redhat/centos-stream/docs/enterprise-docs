:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies:
//
// assembly_granting-sudo-access-to-an-IdM-user-on-an-IdM-client.adoc

[id="proc_troubleshooting-gssapi-authentication-for-sudo_{context}"]
= Troubleshooting GSSAPI authentication for sudo

[role="_abstract"]
If you are unable to authenticate to the `sudo` service with a Kerberos ticket from IdM, use the following scenarios to troubleshoot your configuration.

.Prerequisites

* You have enabled GSSAPI authentication for the `sudo` service. See xref:proc_enabling-gssapi-authentication-for-sudo-on-an-idm-client_granting-sudo-access-to-an-IdM-user-on-an-IdM-client[Enabling GSSAPI authentication for sudo on an IdM client].
* You need `root` privileges to modify the `/etc/sssd/sssd.conf` file and PAM files in the `/etc/pam.d/` directory.

.Procedure

* If you see the following error, the Kerberos service might not able to resolve the correct realm for the service ticket based on the host name:
+
[literal,subs="+quotes,attributes,verbatim"]
....
Server not found in Kerberos database
....
+
In this situation, add the hostname directly to `[domain_realm]` section in the `/etc/krb5.conf` Kerberos configuration file:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm-user@idm-client ~]$ cat /etc/krb5.conf
...

[domain_realm]
 .example.com = EXAMPLE.COM
 example.com = EXAMPLE.COM
 *server.example.com = EXAMPLE.COM*
....

* If you see the following error, you do not have any Kerberos credentials:
+
[literal,subs="+quotes,attributes,verbatim"]
....
No Kerberos credentials available
....
+
In this situation, retrieve Kerberos credentials with the `kinit` utility or authenticate with SSSD:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm-user@idm-client ~]$ *kinit idm-user@IDM.EXAMPLE.COM*
Password for *idm-user@idm.example.com*:
....

* If you see either of the following errors in the `/var/log/sssd/sssd_pam.log` log file, the Kerberos credentials do not match the username of the user currently logged in:
+
[literal,subs="+quotes,attributes,verbatim"]
....
User with UPN [_<UPN>_] was not found.

UPN [_<UPN>_] does not match target user [_<username>_].
....
+
In this situation, verify that you authenticated with SSSD, or consider disabling the `pam_gssapi_check_upn` option in the `/etc/sssd/sssd.conf` file:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm-user@idm-client ~]$ cat /etc/sssd/sssd.conf
...

*pam_gssapi_check_upn = false*
....

* For additional troubleshooting, you can enable debugging output for the `pam_sss_gss.so` PAM module.
** Add the `debug` option at the end of all `pam_sss_gss.so` entries in PAM files, such as `/etc/pam.d/sudo` and `/etc/pam.d/sudo-i`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idm-client ~]# cat /etc/pam.d/sudo
#%PAM-1.0
auth       sufficient   pam_sss_gss.so   *debug*
auth       include      system-auth
account    include      system-auth
password   include      system-auth
session    include      system-auth
....
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idm-client ~]# cat /etc/pam.d/sudo-i
#%PAM-1.0
auth       sufficient   pam_sss_gss.so   *debug*
auth       include      sudo
account    include      sudo
password   include      sudo
session    optional     pam_keyinit.so force revoke
session    include      sudo
....

** Try to authenticate with the `pam_sss_gss.so` module and review the console output. In this example, the user did not have any Kerberos credentials.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm-user@idm-client ~]$ sudo ls -l /etc/sssd/sssd.conf
pam_sss_gss: Initializing GSSAPI authentication with SSSD
pam_sss_gss: Switching euid from 0 to 1366201107
pam_sss_gss: Trying to establish security context
pam_sss_gss: SSSD User name: idm-user@idm.example.com
pam_sss_gss: User domain: idm.example.com
pam_sss_gss: User principal:
pam_sss_gss: Target name: host@idm.example.com
pam_sss_gss: Using ccache: KCM:
pam_sss_gss: Acquiring credentials, principal name will be derived
pam_sss_gss: Unable to read credentials from [KCM:] [maj:0xd0000, min:0x96c73ac3]
pam_sss_gss: GSSAPI: Unspecified GSS failure.  Minor code may provide more information
pam_sss_gss: *GSSAPI: No credentials cache found*
pam_sss_gss: Switching euid from 1366200907 to 0
pam_sss_gss: *System error [5]: Input/output error*
....
