:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// planning-your-ca-services.adoc
// planning-server-services-on-the-ipa-domain.adoc
[id="ca-services-available-in-an-ipa-server_{context}"]
= CA Services available in an IdM server

You can install an {IPA} (IdM) server with an integrated IdM certificate authority (CA) or without a CA.

//If you choose the integrated CA, the IdM server's CA signing certificate must be signed by a _root CA_ (the highest CA in the CA hierarchy). The root CA can be the {IPA} CA itself or an externally-hosted CA, which can be for example a different CA operated within your organization or a CA operated by a third party.

[id='{context}-comparing-ipa-with-integrated-ca-and-without-a-ca']
[cols="1,2a,2a"]
.Comparing IdM with integrated CA and without a CA
|===
||Integrated CA|Without a CA

|Overview:
|IdM uses its own public key infrastructure (PKI) service with a _CA signing certificate_ to create and sign the certificates in the IdM domain.

* If the root CA is the integrated CA, IdM uses a self-signed CA certificate.
* If the root CA is an external CA, the integrated IdM CA is subordinate to the external CA. The CA certificate used by IdM is signed by the external CA, but all certificates for the IdM domain are issued by the integrated Certificate{nbsp}System instance.
* Integrated CA is also able to issue certificates for users, hosts, or services.

The external CA can be a corporate CA or a third-party CA.
|IdM does not set up its own CA, but uses signed host certificates from an external CA.

Installing a server without a CA requires you to request the following certificates from a third-party authority:

* An LDAP server certificate
* An Apache server certificate
* A PKINIT certificate
* Full CA certificate chain of the CA that issued the LDAP and Apache server certificates

|Limitations:
|If the integrated CA is subordinate to an external CA, the certificates issued within the IdM domain are potentially subject to restrictions set by the external CA for various certificate attributes, such as:

* The validity period.
* Constraints on what subject names can appear on certificates issued by the IDM CA or its subordinates..
* Constraints on whether the IDM CA can itself, issue subordinate CA certificates, or how "deep" the chain of subordinate certificates can go.
|Managing certificates outside of IdM causes many additional activities, such as :

* Creating, uploading, and renewing certificates is a manual process.
* The `certmonger` service does not track the IPA certificates (LDAP server, Apache server, and PKINIT certificates) and does not notify you when the certificates are about to expire. The administrators must manually set up notifications for externally issued certificates, or set tracking requests for those certificates if they want `certmonger` to track them.

|Works best for:
|Environments that allow you to create and use your own certificate infrastructure.
|Very rare cases when restrictions within the infrastructure do not allow you to install certificate services integrated with the server.
|===

NOTE: Switching from the self-signed CA to an externally-signed CA, or the other way around, as well as changing which external CA issues the IdM CA certificate, is possible even after the installation. It is also possible to configure an integrated CA even after an installation without a CA. For more details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-ipa-server-without-a-ca_installing-identity-management[Installing an IdM server: With integrated DNS, without a CA].

[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_certificates_in_idm/understanding-what-certificates-are-used-by-idm_working-with-idm-certificates[Understanding the certificates used internally by IdM]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/understanding-what-certificates-are-used-by-idm_managing-certificates-in-idm[Understanding the certificates used internally by IdM]
endif::[]
