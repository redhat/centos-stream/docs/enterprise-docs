:_mod-docs-content-type: PROCEDURE

[id="modifying-an-IdM-ID-view_{context}"]
= Modifying an IdM ID view

[role="_abstract"]
An ID view in {IPA} (IdM) overrides a POSIX attribute value associated with a specific IdM user. Follow this procedure to modify an existing ID view. Specifically, it describes how to modify an ID view to enable the user named *idm_user* to use the `/home/user_1234/` directory as the user home directory instead of `/home/idm_user/` on the *host1.idm.example.com* IdM client.

.Prerequisites

* You have root access to *host1.idm.example.com*.
* You are logged in as a user with the required privileges, for example *admin*.
* You have an ID view configured for *idm_user* that applies to the *host1* IdM client.

.Procedure

. As root, create the directory that you want *idm_user* to use on *host1.idm.example.com* as the user home directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@host1 /]# *mkdir /home/user_1234/*
....

. Change the ownership of the directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@host1 /]# *chown idm_user:idm_user /home/user_1234/*
....

. Display the ID view, including the hosts to which the ID view is currently applied. To display the ID view named `example_for_host1`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa idview-show example_for_host1 --all*
  dn: cn=example_for_host1,cn=views,cn=accounts,dc=idm,dc=example,dc=com
  ID View Name: example_for_host1
  User object override: idm_user
  Hosts the view applies to: host1.idm.example.com
  objectclass: ipaIDView, top, nsContainer
....

+
The output shows that the ID view currently applies to *host1.idm.example.com*.

. Modify the user override of the *example_for_host1* ID view. To override the user home directory:

* Enter the `ipa idoverrideuser-add` command
* Add the name of the ID view
* Add the user name, also called the anchor
* Add the `--homedir` option:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *ipa idoverrideuser-mod example_for_host1 idm_user --homedir=/home/user_1234*
-----------------------------
Modified a User ID override "idm_user"
-----------------------------
  Anchor to override: idm_user
  User login: user_1234
  Home directory: /home/user_1234/
....

+
For a list of the available options, run [command]`ipa idoverrideuser-mod --help`.


. To apply the new configuration to the *host1.idm.example.com* system immediately:

.. SSH to the system as root:

+
[literal,subs="+quotes,attributes,verbatim"]
....

$ *ssh root@host1*
Password:
....


.. Clear the SSSD cache:

+
[literal,subs="+quotes,attributes,verbatim"]
....
root@host1 ~]# *sss_cache -E*
....

.. Restart the SSSD daemon:

+
[literal,subs="+quotes,attributes,verbatim"]
....
root@host1 ~]# *systemctl restart sssd*
....


.Verification

. `SSH` to *host1* as *idm_user*:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@r8server ~]# *ssh idm_user@host1.idm.example.com*
Password:

Last login: Sun Jun 21 22:34:25 2020 from 192.168.122.229
[user_1234@host1 ~]$
....

. Print the working directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[user_1234@host1 ~]$ *pwd*
/home/user_1234/
....


[role="_additional-resources"]
.Additional resources
* xref:proc_defining-global-attributes-for-an-ad-user-by-modifying-the-default-trust-view_assembly_using-id-views-for-active-directory-users[Defining global attributes for an AD user by modifying the Default Trust View]
