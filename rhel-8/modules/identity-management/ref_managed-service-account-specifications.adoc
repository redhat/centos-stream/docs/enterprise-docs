
[id="ref_managed-service-account-specifications_{context}"]
= Managed Service Account specifications

[role="_abstract"]
The Managed Service Accounts (MSAs) that the `adcli` utility creates have the following specifications:

* They cannot have additional service principal names (SPNs). 
* By default, the Kerberos principal for the MSA is stored in a Kerberos keytab named `_<default_keytab_location>.<Active_Directory_domain>_`, like  `_/etc/krb5.keytab.production.example.com_`.
* MSA names are limited to 20 characters or fewer. The last 4 characters are a suffix of 3 random characters from number and upper- and lowercase ASCII ranges appended to the short host name you provide, using a `!` character as a separator.
For example, a host with the short name `myhost` receives an MSA with the following specifications:
+
[options="header",adoc]
|====
|Specification|Value
|Common name (CN) attribute|`myhost!A2c`
|NetBIOS name|`myhost!A2c$`
|sAMAccountName|`myhost!A2c$`
|Kerberos principal in the `production.example.com`
AD domain|`myhost!A2c$@PRODUCTION.EXAMPLE.COM`
|====
