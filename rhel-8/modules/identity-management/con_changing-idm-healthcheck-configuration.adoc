
:_mod-docs-content-type: CONCEPT

[id="con_changing-idm-healthcheck-configuration_{context}"]
= Changing IdM Healthcheck configuration

You can change Healthcheck settings by adding the desired command line options to the `/etc/ipahealthcheck/ipahealthcheck.conf` file. This can be useful when, for example, you configured a log rotation and want to ensure the logs are in a format suitable for automatic analysis, but do not want to set up a new timer.

ifeval::[{ProductNumber} == 8]
NOTE: This Healthcheck feature is only available on RHEL 8.7 and newer.
endif::[]

ifeval::[{ProductNumber} == 9]
NOTE: This Healthcheck feature is only available on RHEL 9.1 and newer.
endif::[]

After the modification, all logs that Healthcheck creates follow the new settings. These settings also apply to any manual execution of Healthcheck.

NOTE: When running Healthcheck manually, settings in the configuration file take precedence over options specified in the command line. For example, if `output_type` is set to `human` in the configuration file, specifying `json` on the command line has no effect. Any command line options you use that are not specified in the configuration file are applied normally.

[role="_additional-resources"]
.Additional resources

* xref:configuring-log-rotation-using-the-idm-healthcheck_{context}[Configuring log rotation using the IdM Healthcheck]
