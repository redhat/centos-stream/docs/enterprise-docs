:_mod-docs-content-type: PROCEDURE

[id="proc_assigning-dna-id-ranges-manually_{context}"]
= Assigning DNA ID ranges manually

In some cases, you may need to manually assign Distributed Numeric Assignment (DNA) ID ranges to existing replicas, for example to reassign a DNA ID range assigned to a non-functioning replica. For more information, see xref:con_manual-id-range-assignment_adjusting-id-ranges-manually[Manual ID range assignment].

When adjusting a DNA ID range manually, make sure that the newly adjusted range is included in the IdM ID range; you can check this using the [commmand]`ipa idrange-find` command. Otherwise, the command fails.

IMPORTANT:  Be careful not to create overlapping ID ranges. If any of the ID ranges you assign to servers or replicas overlap, it could result in two different servers assigning the same ID value to different entries.

.Prerequisites

* _Optional._ If you are recovering a DNA ID range from a non-functioning replica, first find the ID range using the commands described in xref:displaying-currently-assigned-dna-id-ranges_adjusting-id-ranges-manually[Displaying currently assigned DNA ID ranges].

.Procedure

* To define the current DNA ID range for a specified server, use [command]`ipa-replica-manage dnarange-set`:
+
[literal]
--
# ipa-replica-manage dnarange-set serverA.example.com 1250-1499
--
* To define the next DNA ID range for a specified server, use [command]`ipa-replica-manage dnanextrange-set`:
+
[literal]
--
# ipa-replica-manage dnanextrange-set serverB.example.com 1500-5000
--


.Verification

* You can check that the new DNA ranges are set correctly by using the commands described in xref:displaying-currently-assigned-dna-id-ranges_adjusting-id-ranges-manually[Displaying the currently assigned DNA ID ranges].
