
:_mod-docs-content-type: PROCEDURE

[id="proc_using-ansible-to-configure-idm-clients-for-smart-card-authentication_{context}"]
= Using Ansible to configure IdM clients for smart card authentication


[role="_abstract"]
Follow this procedure to use the `ansible-freeipa` `ipasmartcard_client` module to configure specific {IPA} (IdM) clients to permit IdM users to authenticate with a smart card. Run this procedure to enable smart card authentication for IdM users that use any of the following to access IdM:

* The `ssh` protocol
+
For details see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-and-importing-local-certificates-to-a-smart-card_managing-smart-card-authentication#configuring-ssh-access-using-smart-card-authentication_configuring-and-importing-local-certificates-to-a-smart-card[Configuring SSH access using smart card authentication].

* The console login
* The GNOME Display Manager (GDM)
* The `su` command

[NOTE]
====
This procedure is not required for authenticating to the IdM Web UI. Authenticating to the IdM Web UI involves two hosts, neither of which needs to be an IdM client:

* The machine on which the browser is running. The machine can be outside of the IdM domain.
* The IdM server on which `httpd` is running.
====


.Prerequisites

* Your IdM server has been configured for smart card authentication, as described in xref:proc_using-ansible-to-configure-the-idm-server-for-smart-card-authentication_{context}[Using Ansible to configure the IdM server for smart card authentication].
* You have root access to the IdM server and the IdM client.
* You have the root CA certificate, the IdM CA certificate, and all the intermediate CA certificates.
* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. If your CA certificates are stored in files of a different format, such as `DER`, convert them to `PEM` format:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *openssl x509 -in <filename>.der -inform DER -out <filename>.pem -outform PEM*
....

+
The IdM CA certificate is in `PEM` format and is located in the `/etc/ipa/ca.crt` file.

. Optional: Use the `openssl x509` utility to view the contents of the files in the `PEM` format to check that the `Issuer` and `Subject` values are correct:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *openssl x509 -noout -text -in root-ca.pem | more*
....

. On your Ansible control node, navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *cd ~/__MyPlaybooks__/*
....
. Create a subdirectory dedicated to the CA certificates:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *mkdir SmartCard/*
....

. For convenience, copy all the required certificates to the *~/MyPlaybooks/SmartCard/* directory, for example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# *cp /tmp/root-ca.pem ~/MyPlaybooks/SmartCard/*
# *cp /tmp/intermediate-ca.pem ~/MyPlaybooks/SmartCard/*
# *cp /etc/ipa/ca.crt ~/MyPlaybooks/SmartCard/ipa-ca.crt*
....


. In your Ansible inventory file, specify the following:

+
--
** The IdM clients that you want to configure for smart card authentication.
** The IdM administrator password.
** The paths to the certificates of the CAs in the following order:

*** The root CA certificate file
*** The intermediate CA certificates files
*** The IdM CA certificate file
--

+
The file can look as follows:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
*[ipaclients]*
ipaclient1.example.com
ipaclient2.example.com

[ipaclients:vars]
*ipaadmin_password=SomeADMINpassword*
*ipasmartcard_client_ca_certs=/home/<user_name>/MyPlaybooks/SmartCard/root-ca.pem,/home/<user_name>/MyPlaybooks/SmartCard/intermediate-ca.pem,/home/<user_name>/MyPlaybooks/SmartCard/ipa-ca.crt*
....


. Create an `install-smartcard-clients.yml` playbook with the following content:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
---
- name: Playbook to set up smart card authentication for an IdM client
  *hosts: ipaclients*
  become: true

  roles:
  - *role: ipasmartcard_client*
    state: present
....

. Save the file.

. Run the Ansible playbook. Specify the playbook and inventory files:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory install-smartcard-clients.yml*
....

+
The `ipasmartcard_client` Ansible role performs the following actions:

* It configures the smart card daemon.
* It sets the system-wide truststore.
* It configures the System Security Services Daemon (SSSD) to allow users to authenticate with either their user name and password or their smart card. For more details on SSSD profile options for smart card authentication, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/assembly_understanding-smart-card-authentication_managing-smart-card-authentication#con_smart-card-authentication-options-in-rhel_assembly_understanding-smart-card-authentication[Smart card authentication options in RHEL].


The clients listed in the *ipaclients* section of the inventory file are now configured for smart card authentication.

[NOTE]
====
If you have installed the IdM clients with the `--mkhomedir` option, remote users will be able to log in to their home directories. Otherwise, the default login location is the root of the directory structure, `/`.
====

[role="_additional-resources"]
.Additional resources
* Sample playbooks using the `ipasmartcard_server` role in the `/usr/share/doc/ansible-freeipa/playbooks/` directory
