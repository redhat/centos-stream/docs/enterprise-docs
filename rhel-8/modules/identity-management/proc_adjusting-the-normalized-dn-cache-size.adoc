:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_adjusting-idm-directory-server-performance.adoc

[id="adjusting-the-normalized-dn-cache-size_{context}"]
= Adjusting the normalized DN cache size

IMPORTANT: Red Hat recommends using the built-in cache auto-sizing feature for optimized performance. Only change this value if you need to purposely deviate from the auto-tuned values.

[role="_abstract"]
The `nsslapd-ndn-cache-max-size` attribute controls the size, in bytes, of the cache that stores normalized distinguished names (NDNs). Increasing this value will retain more frequently used DNs in memory.

[cols="h,1"]
|===
| Default value | `20971520`  _(20 MB)_
| Valid range | `0 - 2147483647`
| Entry DN location | `cn=config`
|===

.Prerequisites
* The LDAP Directory Manager password


.Procedure

. Ensure the NDN cache is enabled.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *dsconf -D "cn=Directory Manager" _ldap://server.example.com_ config get _nsslapd-ndn-cache-enabled_*
Enter password for cn=Directory Manager on ldap://server.example.com:
nsslapd-ndn-cache-enabled: *on*
....
+
If the cache is `off`, enable it with the following command.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *dsconf -D "cn=Directory Manager" _ldap://server.example.com_ config replace nsslapd-ndn-cache-enabled=on*
Enter password for cn=Directory Manager on ldap://server.example.com:
Successfully replaced "nsslapd-ndn-cache-enabled"
....

. Retrieve the current value of the `nsslapd-ndn-cache-max-size` parameter and make a note of it before making any adjustments, in case it needs to be restored. Enter the Directory Manager password when prompted.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *dsconf -D "cn=Directory Manager" _ldap://server.example.com_ config get _nsslapd-ndn-cache-max-size_*
Enter password for cn=Directory Manager on ldap://server.example.com:
nsslapd-ndn-cache-max-size: 20971520
....

. Modify the value of the `nsslapd-ndn-cache-max-size` attribute. This example increases the value to `41943040` (40 MB).
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *dsconf -D "cn=Directory Manager" _ldap://server.example.com_ config replace nsslapd-ndn-cache-max-size=_41943040_*
....

. Monitor the IdM directory server's performance. If it does not change in a desirable way, repeat this procedure and adjust `nsslapd-ndn-cache-max-size` to a different value, or re-enable cache auto-sizing.


.Verification

* Display the new value of the `nsslapd-ndn-cache-max-size` attribute and verify it has been set to your desired value.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *dsconf -D "cn=Directory Manager" _ldap://server.example.com_ config get nsslapd-ndn-cache-max-size*
Enter password for cn=Directory Manager on ldap://server.example.com:
nsslapd-ndn-cache-max-size: *41943040*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_directory_server/11/html/configuration_command_and_file_reference/core_server_configuration_reference#cnconfig-nsslapd_ndn_cache_max-size[nsslapd-ndn-cache-max-size] in Directory Server 11 documentation
