:_mod-docs-content-type: PROCEDURE
// Module included in the following titles/assemblies:
//
// assembly_managing-dns-forwarding-in-idm.adoc

[id="establishing-a-dns-global-forwarder-in-idm-using-ansible_{context}"]
= Establishing a DNS Global Forwarder in IdM using Ansible

[role="_abstract"]
Follow this procedure to use an Ansible playbook to establish a DNS Global Forwarder in IdM.

In the example procedure below, the IdM administrator creates a DNS global forwarder to a DNS server with an Internet Protocol (IP) v4 address of `8.8.6.6` and IPv6 address of `2001:4860:4860::8800` on port `53`.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* You know the IdM administrator password.

.Procedure

. Navigate to the `/usr/share/doc/ansible-freeipa/playbooks/dnsconfig` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ cd /usr/share/doc/ansible-freeipa/playbooks/dnsconfig
....

. Open your inventory file and make sure that the IdM server that you want to configure is listed in the `[ipaserver]` section. For example, to instruct Ansible to configure `server.idm.example.com`, enter:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....

. Make a copy of the `set-configuration.yml` Ansible playbook file. For example:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ cp set-configuration.yml establish-global-forwarder.yml
....

. Open the `establish-global-forwarder.yml` file for editing.

. Adapt the file by setting the following variables:
.. Change the `name` variable for the playbook to `Playbook to establish a global forwarder in IdM DNS`.
.. In the `tasks` section, change the `name` of the task to `Create a DNS global forwarder to 8.8.6.6 and 2001:4860:4860::8800`.
.. In the `forwarders` section of the `ipadnsconfig` portion:
... Change the first `ip_address` value to the IPv4 address of the global forwarder: `8.8.6.6`.
... Change the second `ip_address` value to the IPv6 address of the global forwarder: `2001:4860:4860::8800`.
... Verify the `port` value is set to `53`.
.. Change the `forward_policy` to `first`.

+
This the modified Ansible playbook file for the current example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to establish a global forwarder in IdM DNS
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Create a DNS global forwarder to 8.8.6.6 and 2001:4860:4860::8800
    ipadnsconfig:
      forwarders:
        - ip_address: 8.8.6.6
        - ip_address: 2001:4860:4860::8800
          port: 53
      forward_policy: first
      allow_sync_ptr: true
....

. Save the file.

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ ansible-playbook --vault-password-file=password_file -v -i inventory.file establish-global-forwarder.yml
....

[role="_additional-resources"]
.Additional resources
* The `README-dnsconfig.md` file in the `/usr/share/doc/ansible-freeipa/` directory
