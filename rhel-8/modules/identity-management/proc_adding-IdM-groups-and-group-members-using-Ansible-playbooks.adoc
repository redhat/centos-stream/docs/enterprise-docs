:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-IdM-groups-and-group-members-using-Ansible-playbooks_{context}"]
= Ensuring the presence of IdM groups and group members using Ansible playbooks

[role="_abstract"]
The following procedure describes ensuring the presence of IdM groups and group members - both users and user groups - using an Ansible playbook.

.Prerequisites

* You know the IdM administrator password.

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

* The users you want to reference in your Ansible playbook exist in IdM. For details on ensuring the presence of users using Ansible, see xref:managing-user-accounts-using-Ansible-playbooks_{ProjectNameID}[Managing user accounts using Ansible playbooks].


.Procedure

. Create an inventory file, for example `inventory.file`, and define `ipaserver` in it:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Create an Ansible playbook file with the necessary user and group information:

+
[source,java]
----
---
- name: Playbook to handle groups
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Create group ops with gid 1234
    ipagroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: ops
      gidnumber: 1234

  - name: Create group sysops
    ipagroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: sysops
      user:
      - idm_user

  - name: Create group appops
    ipagroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: appops

  - name: Add group members sysops and appops to group ops
    ipagroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: ops
      group:
      - sysops
      - appops
----

. Run the playbook:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i _path_to_inventory_directory/inventory.file_ _path_to_playbooks_directory/add-group-members.yml_*
....


.Verification

You can verify if the *ops* group contains *sysops* and *appops* as direct members and *idm_user* as an indirect member by using the `ipa group-show` command:

. Log into `ipaserver` as administrator:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ssh admin@server.idm.example.com*
Password:
[admin@server /]$
....

. Display information about _ops_:

+
[literal,subs="+quotes,attributes,verbatim"]
....
ipaserver]$ *ipa group-show ops*
  Group name: ops
  GID: 1234
  Member groups: sysops, appops
  Indirect Member users: idm_user
....

+
The *appops* and *sysops* groups - the latter including the *idm_user* user - exist in IdM.

[role="_additional-resources"]
.Additional resources
* See the `/usr/share/doc/ansible-freeipa/README-group.md` Markdown file.
