
:_mod-docs-content-type: CONCEPT

[id="con_the-role-of-security-and-relative-identifiers-in-idm-id-ranges_{context}"]
= The role of security and relative identifiers in IdM ID ranges


An {IPA} (IdM) ID range is defined by several parameters:

* The range name
* The first POSIX ID of the range
* The range size: the number of IDs in the range
* The first *relative identifier* (RID) of the corresponding *RID range*
* The first RID of the *secondary RID range*

You can view these values by using the `ipa idrange-show` command:


[literal,subs="+quotes,attributes"]
....
$ *ipa idrange-show IDM.EXAMPLE.COM_id_range*
  Range name: IDM.EXAMPLE.COM_id_range
  First Posix ID of the range: 196600000
  Number of IDs in the range: 200000
  First RID of the corresponding RID range: 1000
  First RID of the secondary RID range: 1000000
  Range type: local domain range
....


.Security identifiers

The data from the ID ranges of the local domain are used by the IdM server internally to assign unique *security identifiers* (SIDs) to IdM users and groups. The SIDs are stored in the user and group objects. A user’s SID consists of the following:

* The domain SID
* The user's *relative identifier* (RID), which is a four-digit 32-bit value appended to the domain SID

For example, if the domain SID is S-1-5-21-123-456-789 and the RID of a user from this domain is 1008, then the user has the SID of S-1-5-21-123-456-789-1008.

.Relative identifiers

The RID itself is computed in the following way:

Subtract the first POSIX ID of the range from the user's POSIX UID, and add the first RID of the corresponding RID range to the result. For example, if the UID of _idmuser_ is 196600008, the first POSIX ID is 196600000, and the first RID is 1000, then _idmuser_'s RID is 1008.

[NOTE]
====
The algorithm computing the user's RID checks if a given POSIX ID falls into the ID range allocated before it computes a corresponding RID. For example, if the first ID is 196600000 and the range size is 200000, then the POSIX ID of 1600000 is outside of the ID range and the algorithm does not compute a RID for it.
====

.Secondary relative identifiers

In IdM, a POSIX UID can be identical to a POSIX GID. This means that if _idmuser_ already exists with the UID of 196600008, you can still create a new _idmgroup_ group with the GID of 196600008.

However, a SID can define only one object, a user _or_ a group. The SID of S-1-5-21-123-456-789-1008 that has already been created for _idmuser_ cannot be shared with _idmgroup_. An alternative SID must be generated for _idmgroup_.

IdM uses a *secondary relative identifier*, or secondary RID, to avoid conflicting SIDs. This secondary RID consists of the following:

* The secondary RID base
* A range size; by default identical with the base range size

In the example above, the secondary RID base is set to 1000000. To compute the RID for the newly created _idmgroup_: subtract the first POSIX ID of the range from the user's POSIX UID, and add the first RID of the secondary RID range to the result. _idmgroup_ is therefore assigned the RID of 1000008. Consequently, the SID of _idmgroup_ is S-1-5-21-123-456-789-1000008.

IdM uses the secondary RID to compute a SID only if a user or a group object was previously created with a manually set POSIX ID. Otherwise, automatic assignment prevents assigning the same ID twice.

[role="_additional-resources"]
.Additional resources
* xref:proc_using-ansible-to-add-a-new-local-idm-id-range_{context}[Using Ansible to add a new local IdM ID range]
