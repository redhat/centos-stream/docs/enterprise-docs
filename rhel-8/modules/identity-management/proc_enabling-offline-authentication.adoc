:_mod-docs-content-type: PROCEDURE
[id="proc_enabling-offline-authentication_{context}"]
= Enabling offline authentication

[role="_abstract"]
SSSD does not cache user credentials by default. When processing authentication requests, SSSD always contacts the identity provider. If the provider is unavailable, user authentication fails.

To ensure that users can authenticate even when the identity provider is unavailable, you can enable credential caching by setting `cache_credentials` to `true` in the `/etc/sssd/sssd.conf` file. Cached credentials refer to passwords and the first authentication factor if two-factor authentication is used. Note that for
ifeval::[{ProductNumber} == 8]
smart card authentication, you do not need to set `cache_credentials` to true or set any additional configuration; it is expected to work offline as long as a successful online authentication is recorded in the cache. 
endif::[]
ifeval::[{ProductNumber} == 9]
passkey and smart card authentication, you do not need to set `cache_credentials` to true or set any additional configuration; they are expected to work offline as long as a successful online authentication is recorded in the cache. 
endif::[]


[IMPORTANT]
====
SSSD never caches passwords in plain text. It stores only a hash of the password.

While credentials are stored as a salted SHA-512 hash, this potentially poses a security risk in case an attacker manages to access the cache file and break a password using a brute force attack. Accessing a cache file requires privileged access, which is the default on RHEL.
====

.Prerequisites
* `root` access

.Procedure

. Open the `/etc/sssd/sssd.conf` file.

. In a domain section, add the `cache_credentials = true` setting:
+
[subs=+quotes]
....
[domain/_your-domain-name_]
cache_credentials = true
....

. _Optional, but recommended_: Configure a time limit for how long SSSD allows offline authentication if the identity provider is unavailable:
+
// the link should be updated once the PAM section is converted from RHEL7 to RHEL8
.. Configure the PAM service to work with SSSD.
+
See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_authentication_and_authorization_in_rhel/configuring-user-authentication-using-authselect_configuring-authentication-and-authorization-in-rhel[Configuring user authentication using authselect] for more details.

.. Use the `offline_credentials_expiration` option to specify the time limit.
+
Note that the limit is set in days.
+
For example, to specify that users are able to authenticate offline for 3 days since the last successful login, use:
+
....
[pam]
offline_credentials_expiration = 3
....

//For more details on `offline_credentials_expiration`, see the `sssd.conf(5)` man page on your system.

////
.Verification
////
[role="_additional-resources"]
.Additional resources
* `sssd.conf(5)` man page on your system
