:_mod-docs-content-type: CONCEPT

[id="cross-forest-and-external-trusts-between-idm-and-ad_{context}"]
= Cross-forest and external trusts between IdM and AD


[discrete]
== A cross-forest trust between IdM and AD

In a pure {AD} (AD) environment, a cross-forest trust connects two separate AD forest root domains. When you create a cross-forest trust between AD and IdM, the IdM domain presents itself to AD as a separate forest with a single domain. A trust relationship is then established between the AD forest root domain and the IdM domain. As a result, users from the AD forest can access the resources in the IdM domain.

IdM can establish a trust with one AD forest or multiple unrelated forests.

[NOTE]
====
Two separate Kerberos realms can be connected in a _cross-realm trust_.
//In this setup, a principal from one realm can request a ticket to a service in another Kerberos realm. Using this ticket, the principal can authenticate against resources on systems belonging to the other realm.
However, a Kerberos realm only concerns authentication, not other services and protocols involved in identity and authorization operations. Therefore, establishing a Kerberos cross-realm trust is not enough to enable users from one realm to access resources in another realm.
====

[discrete]
== An external trust to an AD domain

An external trust is a trust relationship between IdM and an Active Directory domain. While a forest trust always requires establishing a trust between IdM and the root domain of an Active Directory forest, an external trust can be established from IdM to any domain within a forest.


// An _AD forest_ is at the top of the organizational structure of {AD}. A forest is a collection of AD trees, which are collections of AD domains. The first domain created in a forest is the _root domain_ of the forest.

////
.Additional resources

* For details on the architecture of AD forests, see link:https://technet.microsoft.com/en-us/library/cc759073[What Are Domains and Forests?] on TechNet.
* For details on trust flow and transitivity, see link:https://technet.microsoft.com/en-us/library/cc773178[How Domain and Forest Trusts Work] on TechNet.
////
