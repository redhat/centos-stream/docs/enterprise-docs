:_mod-docs-content-type: PROCEDURE
[id="conf-idm-server-for-smart-card-auth_{context}"]
= Configuring the IdM server for smart card authentication

[role="_abstract"]

If you want to enable smart card authentication for users whose certificates have been issued by the certificate authority (CA) of the <EXAMPLE.ORG> domain that your {IPA} (IdM) CA trusts, you must obtain the following certificates so that you can add them when running the `ipa-advise` script that configures the IdM server:

* The certificate of the root CA that has either issued the certificate for the <EXAMPLE.ORG> CA directly, or through one or more of its sub-CAs. You can download the certificate chain from a web page whose certificate has been issued by the authority. For details, see Steps 1 - 4a in
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/dc-web-ui-auth_configuring-and-managing-idm#configuring-browser-for-cert-auth_dc-web-ui-auth[Configuring a browser to enable certificate authentication].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/dc-web-ui-auth_managing-certificates-in-idm#configuring-browser-for-cert-auth_dc-web-ui-auth[Configuring a browser to enable certificate authentication].
endif::[]

* The IdM CA certificate. You can obtain the CA certificate from the `/etc/ipa/ca.crt` file on the IdM server on which an IdM CA instance is running.
* The certificates of all of the intermediate CAs; that is, intermediate between the <EXAMPLE.ORG> CA and the IdM CA.

To configure an IdM server for smart card authentication:

. Obtain files with the CA certificates in the PEM format.
. Run the built-in `ipa-advise` script.
. Reload the system configuration.

.Prerequisites

* You have root access to the IdM server.
* You have the root CA certificate and all the intermediate CA certificates.

.Procedure

. Create a directory in which you will do the configuration:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@server]# *mkdir ~/SmartCard/*
....


. Navigate to the directory:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@server]# *cd ~/SmartCard/*
....


. Obtain the relevant CA certificates stored in files in PEM format. If your CA certificate is stored in a file of a different format, such as DER, convert it to PEM format. The IdM Certificate Authority certificate is in PEM format and is located in the `/etc/ipa/ca.crt` file.
+
Convert a DER file to a PEM file:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# openssl x509 -in <filename>.der -inform DER -out <filename>.pem -outform PEM
....

. For convenience, copy the certificates to the directory in which you want to do the configuration:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@server SmartCard]# *cp /tmp/rootca.pem ~/SmartCard/*
[root@server SmartCard]# *cp /tmp/subca.pem ~/SmartCard/*
[root@server SmartCard]# *cp /tmp/issuingca.pem ~/SmartCard/*
....

. Optional: If you use certificates of external certificate authorities, use the `openssl x509` utility to view the contents of the files in the `PEM` format to check that the `Issuer` and `Subject` values are correct:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@server SmartCard]# *openssl x509 -noout -text -in rootca.pem | more*
....


. Generate a configuration script with the in-built `ipa-advise` utility, using the administrator's privileges:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@server SmartCard]# *kinit admin*
[root@server SmartCard]# *ipa-advise config-server-for-smart-card-auth > config-server-for-smart-card-auth.sh*
....

+
The `config-server-for-smart-card-auth.sh` script performs the following actions:

* It configures the IdM Apache HTTP Server.
* It enables Public Key Cryptography for Initial Authentication in Kerberos (PKINIT) on the Key Distribution Center (KDC).
* It configures the IdM Web UI to accept smart card authorization requests.


. Execute the script, adding the PEM files containing the root CA and sub CA certificates as arguments:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@server SmartCard]# *chmod +x config-server-for-smart-card-auth.sh*
[root@server SmartCard]# *./config-server-for-smart-card-auth.sh rootca.pem subca.pem issuingca.pem*
Ticket cache:KEYRING:persistent:0:0
Default principal: \admin@IDM.EXAMPLE.COM
[...]
Systemwide CA database updated.
The ipa-certupdate command was successful
....

+
[NOTE]
====
Ensure that you add the root CA's certificate as an argument before any sub CA certificates and that the CA or sub CA certificates have not expired.
====

. Optional: If the certificate authority that issued the user certificate does not provide any Online Certificate Status Protocol (OCSP) responder, you may need to disable OCSP check for authentication to the IdM Web UI:

+
--
.. Set the `SSLOCSPEnable` parameter to `off` in the `/etc/httpd/conf.d/ssl.conf` file:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
*SSLOCSPEnable off*
....

+
.. Restart the Apache daemon (httpd) for the changes to take effect immediately:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@server SmartCard]# *systemctl restart httpd*
....

--

+
[WARNING]
====
Do not disable the OCSP check if you only use user certificates issued by the IdM CA. OCSP responders are part of IdM.
====

+
For instructions on how to keep the OCSP check enabled, and yet prevent a user certificate from being rejected by the IdM server if it does not contain the information about the location at which the CA that issued the user certificate listens for OCSP service requests, see the `SSLOCSPDefaultResponder` directive in http://httpd.apache.org/docs/trunk/en/mod/mod_ssl.html[Apache mod_ssl configuration options].


The server is now configured for smart card authentication.

[NOTE]
====

To enable smart card authentication in the whole topology, run the procedure on each IdM server.

====
