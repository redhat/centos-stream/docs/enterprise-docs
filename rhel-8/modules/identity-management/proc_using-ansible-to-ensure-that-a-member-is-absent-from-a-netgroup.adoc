:_mod-docs-content-type: PROCEDURE

[id="using-ansible-to-ensure-that-a-member-is-absent-from-a-netgroup_{context}"]
= Using Ansible to ensure that a member is absent from a netgroup


You can use an Ansible playbook to ensure that IdM users are members of a netgroup.
The example describes how to ensure that the *TestNetgroup1* group does not have the *user1* IdM user among its members.
 netgroup


.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** You have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
endif::[]
** You have stored your `ipaadmin_password` in the *secret.yml* Ansible vault.
* The *TestNetgroup1* netgroup exists.



.Procedure


. Create your Ansible playbook file *IdM-member-absent-from-a-netgroup.yml* with the following content:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to manage IPA netgroup.
  hosts: ipaserver
  become: no

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure netgroup user, "user1", is absent
    ipanetgroup:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: TestNetgroup1
      user: "user1"
      action: member
      state: absent
....

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory/inventory.file__ __path_to_playbooks_directory_/IdM-member-absent-from-a-netgroup.yml__*
....

[role="_additional-resources"]
.Additional resources
* xref:nis-in-idm_{context}[NIS in IdM]
* `/usr/share/doc/ansible-freeipa/README-netgroup.md`
* `/usr/share/doc/ansible-freeipa/playbooks/netgroup`
