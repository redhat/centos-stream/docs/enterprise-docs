[id="adding-the-idm-ca-service-to-an-idm-server-in-a-deployment-with-a-ca_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Adding the IdM CA service to an IdM server in a deployment with a CA

If your {IPA} (IdM) environment already has the IdM certificate authority (CA) service installed but a particular IdM server, _idmserver_, was installed as an IdM replica without a CA, you can add the CA service to _idmserver_ by using the `ipa-ca-install` command.

[NOTE]
====
This procedure is identical for both the following scenarios:

* The IdM CA is a root CA.
* The IdM CA is subordinate to an external, root CA.
====

.Prerequisites

* You have `root` permissions on _idmserver_.
* The IdM server is installed on _idmserver_.
* Your IdM deployment has a CA installed on another IdM server.
* You know the IdM `Directory Manager` password.

.Procedure

* On _idmserver_, install the IdM Certificate Server CA:

+
[literal,subs="+quotes,verbatim,macros"]
....
[root@idmserver ~] *ipa-ca-install*
....
