:_mod-docs-content-type: PROCEDURE
[id='installing-an-ipa-server-with-integrated-dns-non-interactive-installation_{context}']
= Non-interactive installation
//Installing an {IPA} server with integrated DNS:
//* With integrated {IPA} certificate authority (CA) as the root CA, which is the default CA configuration

[role="_abstract"]
The `ipa-server-install` installation script creates a log file at `/var/log/ipaserver-install.log`. If the installation fails, the log can help you identify the problem.


.Procedure

. Run the *ipa-server-install* utility with the options to supply all the required information. The minimum required options for non-interactive installation are:
+
--
* `--realm` to provide the Kerberos realm name
* `--ds-password` to provide the password for the Directory Manager (DM), the {DS} super user
* `--admin-password` to provide the password for `admin`, the {IPA} (IdM) administrator
* `--unattended` to let the installation process select default options for the host name and domain name
--
//
ifeval::["{assembly}" == "installing-server-with-dns"]
+
To install a server with integrated DNS, add also these options:
+
--
* `--setup-dns` to configure integrated DNS
* `--forwarder` or `--no-forwarders`, depending on whether you want to configure DNS forwarders or not
* `--auto-reverse` or `--no-reverse`, depending on whether you want to configure automatic detection of the reverse DNS zones that must be created in the IdM DNS or no reverse zone auto-detection
--
+
For example:
+
[literal,subs="+quotes,attributes"]
----
# *ipa-server-install --realm _IDM.EXAMPLE.COM_ --ds-password _DM_password_ --admin-password _admin_password_ --unattended --setup-dns --forwarder _{server1-ipv4}_ --no-reverse*
----

. After the installation script completes, update your DNS records in the following way:

.. Add DNS delegation from the parent domain to the IdM DNS domain. For example, if the IdM DNS domain is `_idm.example.com_`, add a name server (NS) record to the `example.com` parent domain.
+
IMPORTANT: Repeat this step each time after an IdM DNS server is installed.

.. Add an `_ntp._udp` service (SRV) record for your time server to your IdM DNS. The presence of the SRV record for the time server of the newly-installed IdM server in IdM DNS ensures that future replica and client installations are automatically configured to synchronize with the time server used by this primary IdM server.

endif::[]

//
ifeval::["{assembly}" == "installing-server-without-dns"]
+
[literal,subs="+quotes,attributes"]
----
# *ipa-server-install --realm _EXAMPLE.COM_ --ds-password _DM_password_ --admin-password _admin_password_ --unattended*
----
endif::[]
//

////
ifeval::["{assembly}" == "installing-server-with-dns"]

. After the installation script completes, add DNS delegation from the parent domain to the IdM DNS domain. For example, if the IdM DNS domain is `_idm.example.com_`, add a name server (NS) record to the `_example.com_` parent domain.
+
IMPORTANT: Repeat this step each time after an IdM DNS server is installed. !!!

endif::[]
////

