:_mod-docs-content-type: PROCEDURE
[id="ensuring-that-AD-UPNs-are-up-to-date-in-IdM_{context}"]
= Ensuring that AD UPNs are up-to-date in IdM

[role="_abstract"]
After you add or remove a User Principal Name (UPN) suffix in a trusted {AD} (AD) forest, refresh the information for the trusted forest on an IdM server.


.Prerequisites

* IdM administrator credentials.


.Procedure

* Enter the `ipa trust-fetch-domains` command. Note that a seemingly empty output is expected:

+
[literal,subs="+quotes,attributes"]
....
[root@ipaserver ~]# ipa trust-fetch-domains
Realm-Name: ad.example.com
-------------------------------
No new trust domains were found
-------------------------------
----------------------------
Number of entries returned 0
----------------------------
....


.Verification

* Enter the `ipa trust-show` command to verify that the server has fetched the new UPN. Specify the name of the AD realm when prompted:

+
[literal,subs="+quotes,attributes"]
....
[root@ipaserver ~]# *ipa trust-show*
Realm-Name: *ad.example.com*
  Realm-Name: ad.example.com
  Domain NetBIOS name: AD
  Domain Security Identifier: S-1-5-21-796215754-1239681026-23416912
  Trust direction: One-way trust
  Trust type: Active Directory domain
  *UPN suffixes: example.com*
....

The output shows that the `example.com` UPN suffix is now part of the `ad.example.com` realm entry.
