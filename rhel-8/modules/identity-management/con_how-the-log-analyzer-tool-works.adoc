:_mod-docs-content-type: CONCEPT


[id="con_how-the-log-analyzer-tool-works_{context}"]
= How the log analyzer tool works

Using the log parsing tool, you can track SSSD requests from start to finish across log files from multiple SSSD components. You run the analyzer tool using the `sssctl analyze` command.

The log analyzer tool helps you to troubleshoot NSS and PAM issues in SSSD and more easily review SSSD debug logs. You can extract and print SSSD logs related only to certain client requests across SSSD processes.

SSSD tracks user and group identity information (`id`, `getent`) separately from user authentication (`su`, `ssh`) information. The client ID (CID) in the NSS responder is independent of the CID in the PAM responder and you see overlapping numbers when analyzing NSS and PAM requests. Use the `--pam` option with the `sssctl analyze` command to review PAM requests.

[NOTE]
====
Requests returned from the SSSD memory cache are not logged and cannot be tracked by the log analyzer tool.
====


[role="_additional-resources"]
.Additional resources
* `sudo sssctl analyze request --help`
* `sudo sssctl analyze --help`
* `sssd.conf` and `sssctl` man pages on your system
