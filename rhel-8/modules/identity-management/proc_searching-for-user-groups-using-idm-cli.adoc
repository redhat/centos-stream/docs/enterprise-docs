:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="searching-for-user-groups-using-idm-cli_{context}"]
= Searching for user groups using IdM CLI

[role="_abstract"]
Follow this procedure to search for existing user groups using the IdM CLI.

.Procedure

* Display all user groups by using the [command]`ipa group-find` command. To specify a group type, add options to [command]`ipa group-find`:

** Display all POSIX groups using the [command]`ipa group-find --posix` command.

** Display all non-POSIX groups using the [command]`ipa group-find --nonposix` command.

** Display all external groups using the [command]`ipa group-find --external` command.
+
For more information about different group types, see xref:the-different-group-types-in-idm_{context}[The different group types in IdM].
