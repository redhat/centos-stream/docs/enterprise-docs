:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_managing-kerberos-ticket-policies.adoc

[id="idm-kerberos-ticket-policy-types_{context}"]
= IdM Kerberos ticket policy types

[role="_abstract"]
IdM Kerberos ticket policies implement the following ticket policy types:

Connection policy:: To protect Kerberized services with different levels of security, you can define connection policies to enforce rules based on which pre-authentication mechanism a client used to retrieve a ticket-granting ticket (TGT).
+
For example, you can require smart card authentication to connect to `client1.example.com`, and require two-factor authentication to access the `testservice` application on `client2.example.com`.
+
To enforce connection policies, associate _authentication indicators_ with services. Only clients that have the required authentication indicators in their service ticket requests are able to access those services. For more information, see xref:kerberos-authentication-indicators_managing-kerberos-ticket-policies[Kerberos authentication indicators].

Ticket lifecycle policy:: Each Kerberos ticket has a _lifetime_ and a potential _renewal age_: you can renew a ticket before it reaches its maximum lifetime, but not after it exceeds its maximum renewal age.
+
The default global ticket lifetime is one day (86400 seconds) and the default global maximum renewal age is one week (604800 seconds). To adjust these global values, see xref:configuring-global-ticket-lifecycle-policy_managing-kerberos-ticket-policies[Configuring the global ticket lifecycle policy].
+
You can also define your own ticket lifecycle policies:

* To configure different global ticket lifecycle values for each authentication indicator, see xref:configuring-global-ticket-policies-per-authentication-indicator_managing-kerberos-ticket-policies[Configuring global ticket policies per authentication indicator].
* To define ticket lifecycle values for a single user that apply regardless of the authentication method used, see xref:configuring-user-default-ticket-policy_managing-kerberos-ticket-policies[Configuring the default ticket policy for a user].
* To define individual ticket lifecycle values for each authentication indicator that only apply to a single user, see xref:configuring-individual-authentication-indicator-ticket-policies-for-a-user_managing-kerberos-ticket-policies[Configuring individual authentication indicator ticket policies for a user].
