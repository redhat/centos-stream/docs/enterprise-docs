:_mod-docs-content-type: PROCEDURE

[id="proc_deleting-a-sub-ca-from-the-idm-cli_{context}"]
= Deleting a sub-CA from the IdM CLI

[role="_abstract"]
Follow this procedure to delete lightweight sub-CAs from the IdM CLI.

[NOTE]
====
* If you delete a sub-CA, revocation checking for that sub-CA will no longer work. Only delete a sub-CA when there are no more certificates that were issued by that sub-CA whose `notAfter` expiration time is in the future.

* You should only disable sub-CAs while there are still non-expired certificates that were issued by that sub-CA. If all certificates that were issued by a sub-CA have expired, you can delete that sub-CA.

* You cannot disable or delete the IdM CA.
====

.Prerequisites

* Make sure you have obtained the administrator’s credentials.

.Procedure

. To display a list of sub-CAs and CAs, run the `ipa ca-find` command:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# ipa ca-find
-------------
3 CAs matched
-------------
  Name: ipa
  Description: IPA CA
  Authority ID: 5195deaf-3b61-4aab-b608-317aff38497c
  Subject DN: CN=Certificate Authority,O=IPA.TEST
  Issuer DN: CN=Certificate Authority,O=IPA.TEST

  Name: webclient-ca
  Authority ID: 605a472c-9c6e-425e-b959-f1955209b092
  Subject DN: CN=WEBCLIENT,O=IDM.EXAMPLE.COM
  Issuer DN: CN=Certificate Authority,O=IPA.TEST

 *Name: webserver-ca*
  Authority ID: 02d537f9-c178-4433-98ea-53aa92126fc3
  Subject DN: CN=WEBSERVER,O=IDM.EXAMPLE.COM
  Issuer DN: CN=Certificate Authority,O=IPA.TEST
----------------------------
Number of entries returned 3
----------------------------
....

. Run the `ipa ca-disable` command to disable your sub-CA, in this example, the `webserver-ca`:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# ipa ca-disable webserver-ca
--------------------------
Disabled CA "webserver-ca"
--------------------------
....

. Delete the sub-CA, in this example, the `webserver-ca`:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# ipa ca-del webserver-ca
-------------------------
Deleted CA "webserver-ca"
-------------------------
....

.Verification

* Run `ipa ca-find` to display the list of CAs and sub-CAs. The `webserver-ca` is no longer on the list.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# ipa ca-find
-------------
2 CAs matched
-------------
  Name: ipa
  Description: IPA CA
  Authority ID: 5195deaf-3b61-4aab-b608-317aff38497c
  Subject DN: CN=Certificate Authority,O=IPA.TEST
  Issuer DN: CN=Certificate Authority,O=IPA.TEST

  Name: webclient-ca
  Authority ID: 605a472c-9c6e-425e-b959-f1955209b092
  Subject DN: CN=WEBCLIENT,O=IDM.EXAMPLE.COM
  Issuer DN: CN=Certificate Authority,O=IPA.TEST
----------------------------
Number of entries returned 2
----------------------------
....
