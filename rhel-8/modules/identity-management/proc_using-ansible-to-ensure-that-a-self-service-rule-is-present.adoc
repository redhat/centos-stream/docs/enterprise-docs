:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

[id="using-ansible-to-ensure-that-a-self-service-rule-is-present_{context}"]
= Using Ansible to ensure that a self-service rule is present

[role="_abstract"]
The following procedure describes how to use an Ansible playbook to define self-service rules and ensure their presence on an {IPA} (IdM) server. In this example, the new *Users can manage their own name details* rule grants users the ability to change their own `givenname`, `displayname`, `title` and `initials` attributes. This allows them to, for example, change their display name or initials if they want to.

.Prerequisites

* On the control node:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Navigate to the *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Make a copy of the `selfservice-present.yml` file located in the `/usr/share/doc/ansible-freeipa/playbooks/selfservice/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/selfservice/selfservice-present.yml selfservice-present-copy.yml*
....

.  Open the `selfservice-present-copy.yml` Ansible playbook file for editing.

. Adapt the file by setting the following variables in the `ipaselfservice` task section:

* Set the `ipaadmin_password` variable to the password of the IdM administrator.
* Set the `name` variable to the name of the new self-service rule.
* Set the `permission` variable to a comma-separated list of permissions to grant: `read` and `write`.
* Set the `attribute` variable to a list of attributes that users can manage themselves: `givenname`, `displayname`, `title`, and `initials`.

+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Self-service present
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure self-service rule "Users can manage their own name details" is present
    ipaselfservice:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: "Users can manage their own name details"*
      *permission: read, write*
      *attribute:*
      *- givenname*
      *- displayname*
      *- title*
      *- initials*
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory selfservice-present-copy.yml*
....

[role="_additional-resources"]
.Additional resources
* xref:self-service-access-control-in-idm_{context}[Self-service access control in IdM]
* The `README-selfservice.md` file in the `/usr/share/doc/ansible-freeipa/` directory
* The `/usr/share/doc/ansible-freeipa/playbooks/selfservice` directory
