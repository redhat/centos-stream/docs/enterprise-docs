:_mod-docs-content-type: PROCEDURE
:experimental:
[id="revoking-certificates-with-the-integrated-IdM-CAs-using-IdM-WebUI_{context}"]
= Revoking certificates with the integrated IdM CAs using IdM WebUI

[role="_abstract"]
If you know you have lost the private key for your certificate, you must revoke the certificate to prevent its abuse. Complete this procedure to use the IdM WebUI to revoke a certificate issued by the IdM CA.

.Procedure

. Click `Authentication` > `Certificates` > `Certificates`.

. Click the serial number of the certificate to open the certificate information page.
+
[id="host-cert-list-revoke_{context}"]
.List of Certificates
+
image::host_cert_list.png[A screenshot of the "Certificates" page of the IdM Web UI displaying a table of certificates. The certificates are organized by their Serial Numbers and their Subject. The Serial Number "3" is highlighted for the third certificate in the table.]

. In the certificate information page, click menu:Actions[Revoke Certificate].

. Select the reason for revoking and click btn:[Revoke]. See xref:certificate-revocation-reasons_revoking-certificates[Certificate revocation reasons] for details.
