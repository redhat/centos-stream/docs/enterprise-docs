:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_enforcing-authentication-indicators-for-an-idm-service.adoc

[id="retrieving-a-kerberos-service-ticket-for-an-idm-service_{context}"]
= Retrieving a Kerberos service ticket for an IdM service

[role="_abstract"]
The following procedure describes retrieving a Kerberos service ticket for an IdM service. You can use this procedure to test Kerberos ticket policies, such as enforcing that certain Kerberos authentication indicators are present in a ticket-granting ticket (TGT).


.Prerequisites

* If the service you are working with is not an internal IdM service, you have created a corresponding _IdM service_ entry for it. See xref:creating-an-idm-service-entry-and-its-kerberos-keytab_enforcing-authentication-indicators-for-an-idm-service[Creating an IdM service entry and its Kerberos keytab].
* You have a Kerberos ticket-granting ticket (TGT).


.Procedure

* Use the `kvno` command with the `-S` option to retrieve a service ticket, and specify the name of the IdM service and the fully-qualified domain name of the host that manages it.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *kvno -S _testservice client.example.com_*
testservice/client.example.com@EXAMPLE.COM: kvno = 1
....

[NOTE]
====
If you need to access an IdM service and your current ticket-granting ticket (TGT) does not possess the required Kerberos authentication indicators associated with it, clear your current Kerberos credentials cache with the `kdestroy` command and retrieve a new TGT:

[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *kdestroy*
....

For example, if you initially retrieved a TGT by authenticating with a password, and you need to access an IdM service that has the `pkinit` authentication indicator associated with it, destroy your current credentials cache and re-authenticate with a smart card. See xref:kerberos-authentication-indicators_managing-kerberos-ticket-policies[Kerberos authentication indicators].
====

.Verification

*  Use the `klist` command to verify that the service ticket is in the default Kerberos credentials cache.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server etc]# *klist_*
Ticket cache: KCM:1000
Default principal: admin@EXAMPLE.COM

Valid starting       Expires              Service principal
04/01/2020 12:52:42  04/02/2020 12:52:39  krbtgt/EXAMPLE.COM@EXAMPLE.COM
*04/01/2020 12:54:07  04/02/2020 12:52:39  testservice/client.example.com@EXAMPLE.COM*
....
