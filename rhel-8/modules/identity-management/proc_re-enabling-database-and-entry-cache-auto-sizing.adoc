:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_adjusting-idm-directory-server-performance.adoc

:experimental:

[id="re-enabling-database-and-entry-cache-auto-sizing_{context}"]
= Re-enabling database and entry cache auto-sizing

IMPORTANT: Use the built-in cache auto-sizing feature for optimized performance. Do not set cache sizes manually.

[role="_abstract"]
By default, the IdM Directory Server automatically determines the optimal size for the database cache and entry cache. Auto-sizing sets aside a portion of free RAM and optimizes the size of both caches based on the hardware resources of the server when the instance starts.

Use this procedure to undo custom database cache and entry cache values and restore the cache auto-sizing feature to its default values.


[cols="h,1"]
|===
| `nsslapd-cache-autosize` | This settings controls how much free RAM is allocated for auto-sizing the database and entry caches. A value of `0` disables auto-sizing.
| Default value | `10`  _(10% of free RAM)_
| Valid range | `0 - 100`
| Entry DN location | `cn=config,cn=ldbm database,cn=plugins,cn=config`
|===

[cols="h,1"]
|===
| `nsslapd-cache-autosize-split` | This value sets the percentage of free memory determined by `nsslapd-cache-autosize` that is used for the database cache. The remaining percentage is used for the entry cache.
| Default value | `25`  _(25% for the database cache, 60% for the entry cache)_
| Valid range | `0 - 100`
| Entry DN location | `cn=config,cn=ldbm database,cn=plugins,cn=config`
|===


.Prerequisites
* You have previously disabled database and entry cache auto-tuning.


.Procedure

. Stop the Directory Server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *systemctl stop dirsrv.target*
....

. Backup the [filename]`/etc/dirsrv/_slapd-instance_name_/dse.ldif` file before making any further modifications.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *cp /etc/dirsrv/_slapd-instance_name_/dse.ldif \
     /etc/dirsrv/_slapd-instance_name_/dse.ldif.bak.$(date "+%F_%H-%M-%S")
....

. Edit the [filename]`/etc/dirsrv/_slapd-instance_name_/dse.ldif` file:
.. Set the percentage of free system RAM to use for the database and entry caches back to the default of 10% of free RAM.
+
[literal,subs="+quotes,attributes,verbatim"]
....
*nsslapd-cache-autosize: 10*
....
.. Set the percentage used from the free system RAM for the database cache to the default of 25%:
+
[literal,subs="+quotes,attributes,verbatim"]
....
*nsslapd-cache-autosize-split: 25*
....

. Save your changes to the [filename]`/etc/dirsrv/_slapd-instance_name_/dse.ldif` file.

. Start the Directory Server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *systemctl start dirsrv.target*
....


.Verification

* Display the values of the `nsslapd-cache-autosize` and `nsslapd-cache-autosize-split` attributes and verify they have been set to your desired values.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ldapsearch -D "cn=directory manager" -w _DirectoryManagerPassword_ -b "cn=config,cn=ldbm database,cn=plugins,cn=config" | grep nsslapd-cache-autosize
nsslapd-cache-autosize: *10*
nsslapd-cache-autosize-split: *25*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_directory_server/11/html/configuration_command_and_file_reference/plug_in_implemented_server_functionality_reference#nsslapd_cache_autosize[nsslapd-cache-autosize] in Directory Server 11 documentation
