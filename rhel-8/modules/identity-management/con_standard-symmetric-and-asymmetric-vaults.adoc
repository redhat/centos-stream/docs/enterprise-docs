:_mod-docs-content-type: CONCEPT
[id="standard-symmetric-and-asymmetric-vaults_{context}"]
= Standard, symmetric, and asymmetric vaults

[role="_abstract"]
Based on the level of security and access control, IdM classifies vaults into the following types:

Standard vaults::
+
Vault owners and vault members can archive and retrieve the secrets without having to use a password or key.

Symmetric vaults::
+
Secrets in the vault are protected with a symmetric key. Vault owners and members can archive and retrieve the secrets, but they must provide the vault password.

Asymmetric vaults::
+
Secrets in the vault are protected with an asymmetric key. Users archive the secret using a public key and retrieve it using a private key. Vault members can only archive secrets, while vault owners can do both, archive and retrieve secrets.
