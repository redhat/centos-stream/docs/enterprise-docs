:_mod-docs-content-type: PROCEDURE
// [[proc-add-certmapdata-no-cert-no-map-cli_{context}]]
[id="proc-add-certmapdata-no-cert-no-map-cli_{context}"]

= Adding a certificate to an AD user’s ID override in the IdM CLI

[role="_abstract"]
. Obtain the administrator's credentials:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *kinit admin*
....

. Store the certificate blob in a new variable called `CERT`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *CERT=$(openssl x509 -in /path/to/certificate -outform der|base64 -w0)*
....

. Add the certificate of `ad_user@ad.example.com` to the user account using the `ipa idoverrideuser-add-cert` command:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *ipa idoverrideuser-add-cert ad_user@ad.example.com --certificate $CERT*
....


.Verification

Verify that the user and certificate are linked:

. Use the `sss_cache` utility to invalidate the record of `ad_user@ad.example.com` in the SSSD cache and force a reload of the `ad_user@ad.example.com` information:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *sss_cache -u ad_user@ad.example.com*
....

. Run the `ipa certmap-match` command with the name of the file containing the certificate of the AD user:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *ipa certmap-match ad_user_cert.pem*
--------------
1 user matched
--------------
 Domain: AD.EXAMPLE.COM
 User logins: ad_user@ad.example.com
----------------------------
Number of entries returned 1
----------------------------
....


The output confirms that you have certificate mapping data added to `ad_user@ad.example.com` and that a corresponding mapping rule defined in xref:conf-certmap-ad-no-cert-no-map_conf-certmap-for-ad-map[Adding a certificate mapping rule if the AD user entry contains no certificate or mapping data] exists. This means that you can use any certificate that matches the defined certificate mapping data to authenticate as `ad_user@ad.example.com`.


ifdef::c-m-idm[]
[role="_additional-resources"]
.Additional resources
* xref:assembly_using-id-views-for-active-directory-users_configuring-and-managing-idm[Using ID views for Active Directory users]
endif::[]

ifdef::managing-users-groups-hosts[]
[role="_additional-resources"]
.Additional resources
* xref:assembly_using-id-views-for-active-directory-users_managing-users-groups-hosts[Using ID views for Active Directory users]
endif::[]

ifdef::working-with-idm-certificates[]
[role="_additional-resources"]
.Additional resources
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/assembly_using-id-views-for-active-directory-users_configuring-and-managing-idm[Using ID views for Active Directory users]
endif::[]

ifdef::managing-certificates-in-idm[]
[role="_additional-resources"]
.Additional resources
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_idm_users_groups_hosts_and_access_control_rules/assembly_using-id-views-for-active-directory-users_managing-users-groups-hosts[Using ID views for Active Directory users]
endif::[]
