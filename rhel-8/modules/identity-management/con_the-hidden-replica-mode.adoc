:_mod-docs-content-type: CONCEPT
[id="the-hidden-replica-mode_{context}"]
= The hidden replica mode

A *hidden replica* is an IdM server that has all services running and available. However, a hidden replica has no SRV records in DNS, and LDAP server roles are not enabled. Therefore, clients cannot use service discovery to detect hidden replicas.

By default, when you set up a replica, the installation program automatically creates service (SRV) resource records for it in DNS.
These records enable clients to auto-discover the replica and its services.
When installing a replica as hidden, add the `--hidden-replica` parameter to the `ipa-replica-install` command.

//Only display the following if this is rhel 8
ifeval::[{ProductNumber} == 8]
NOTE: The hidden replica feature, introduced in RHEL 8.1 as a Technology Preview, is fully supported starting with RHEL 8.2.
endif::[]

Hidden replicas are primarily designed for dedicated services that might disrupt clients.
For example, a full backup of IdM requires shutting down all IdM services on the server.
As no clients use a hidden replica, administrators can temporarily shut down the services on this host without affecting any clients.

Other use cases include high-load operations on the IdM API or the LDAP server, such as a mass import or extensive queries.

Before backing up a hidden replica, you must install all required server roles used in a cluster, especially the Certificate Authority role if the integrated CA is used. Therefore, restoring a backup from a hidden replica on a new host always results in a regular replica.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-ipa-replica_installing-identity-management[Installing an {IPA} replica]
* xref:backing-up-and-restoring-idm_{ProjectNameID}[Backing up and restoring IdM]
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/demoting-or-promoting-hidden-replicas_configuring-and-managing-idm[Demoting or promoting hidden replicas]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_replication_in_identity_management/demoting-or-promoting-hidden-replicas_managing-replication-in-idm[Demoting or promoting hidden replicas].
endif::[]
