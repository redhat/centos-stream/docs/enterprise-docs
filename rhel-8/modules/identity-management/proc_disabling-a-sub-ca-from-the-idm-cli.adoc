:_mod-docs-content-type: PROCEDURE

[id="proc_disabling-a-sub-ca-from-the-idm-cli_{context}"]
= Disabling a sub-CA from the IdM CLI

[role="_abstract"]
Follow this procedure to disable a sub-CA from the IdM CLI. If there are still non-expired certificates that were issued by a sub-CA, you should not delete it but you can disable it. If you delete the sub-CA, revocation checking for that sub-CA will no longer work.

.Prerequisites

* Make sure you have obtained the administrator’s credentials.

.Procedure

. Run the `ipa ca-find` command to determine the name of the sub-CA you are deleting:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@ipaserver ~]# ipa ca-find
-------------
3 CAs matched
-------------
  Name: ipa
  Description: IPA CA
  Authority ID: 5195deaf-3b61-4aab-b608-317aff38497c
  Subject DN: CN=Certificate Authority,O=IPA.TEST
  Issuer DN: CN=Certificate Authority,O=IPA.TEST

  Name: webclient-ca
  Authority ID: 605a472c-9c6e-425e-b959-f1955209b092
  Subject DN: CN=WEBCLIENT,O=IDM.EXAMPLE.COM
  Issuer DN: CN=Certificate Authority,O=IPA.TEST

 *Name: webserver-ca*
  Authority ID: 02d537f9-c178-4433-98ea-53aa92126fc3
  Subject DN: CN=WEBSERVER,O=IDM.EXAMPLE.COM
  Issuer DN: CN=Certificate Authority,O=IPA.TEST
----------------------------
Number of entries returned 3
----------------------------
....

. Run the `ipa ca-disable` command to disable your sub-CA, in this example, the `webserver-ca`:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
ipa ca-disable webserver-ca
--------------------------
Disabled CA "webserver-ca"
--------------------------
....
