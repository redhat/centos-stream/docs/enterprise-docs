:_mod-docs-content-type: PROCEDURE
[id="using-ansible-to-check-if-a-replication-agreement-exists-between-two-replicas_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using Ansible to check if a replication agreement exists between two replicas

[role="_abstract"]
Data stored on an {IPA} (IdM) server is replicated based on replication agreements: when two servers have a replication agreement configured, they share their data.
Replication agreements are always bilateral: the data is replicated from the first replica to the other one as well as from the other replica to the first one.

Follow this procedure to verify that replication agreements exist between multiple pairs of replicas in IdM. In contrast to xref:using-ansible-to-ensure-a-replication-agreement-exists-in-idm_{context}[Using Ansible to ensure a replication agreement exists in IdM], this procedure does not modify the existing configuration.


.Prerequisites

* Ensure that you understand the recommendations for designing your {IPA} (IdM) topology listed in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/planning-the-replica-topology_planning-identity-management#guidelines-for-connecting-idm-replicas-in-a-topology_planning-the-replica-topology[Connecting the replicas in a topology].
* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password` and that you have access to a file that stores the password protecting the *secret.yml* file.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Copy the `check-topologysegments.yml` Ansible playbook file provided by the `ansible-freeipa` package:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/topology/check-topologysegments.yml check-topologysegments-copy.yml*
....

. Open the `check-topologysegments-copy.yml` file for editing.

. Adapt the file by setting the following variables in the `vars` section:

* Indicate that the value of the `ipaadmin_password` variable is defined in the *secret.yml* Ansible vault file.
* For every topology segment, add a line in the `ipatopology_segments` section and set the following variables:
** Set the `suffix` variable to either `domain` or `ca`, depending on the type of segment you are adding.
** Set the `left` variable to the name of the IdM server that you want to be the left node of the replication agreement.
** Set the `right` variable to the name of the IdM server that you want to be the right node of the replication agreement.

. In the `tasks` section of the `check-topologysegments-copy.yml` file, ensure that the `state` variable is set to `present`.

+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Add topology segments
  hosts: ipaserver
  gather_facts: false

  vars:
    *ipaadmin_password: "{{ ipaadmin_password }}"*
    *ipatopology_segments:*
    - *{suffix: domain, left: replica1.idm.example.com, right: replica2.idm.example.com }*
    - *{suffix: domain, left: replica2.idm.example.com , right: replica3.idm.example.com }*
    - *{suffix: domain, left: replica3.idm.example.com , right: replica4.idm.example.com }*
    - *{suffix: domain+ca, left: replica4.idm.example.com , right: replica1.idm.example.com }*

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Check topology segment
    ipatopologysegment:
      ipaadmin_password: "{{ ipaadmin_password }}"
      suffix: "{{ item.suffix }}"
      name: "{{ item.name | default(omit) }}"
      left: "{{ item.left }}"
      right: "{{ item.right }}"
      state: *checked*
    loop: "{{ ipatopology_segments | default([]) }}"
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory check-topologysegments-copy.yml*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_managing-replication-topology_installing-identity-management#assembly_explaining-replication-agreements-topology-suffixes-and-topology-segments_assembly_managing-replication-topology[Explaining Replication Agreements, Topology Suffixes, and Topology Segments]
* `/usr/share/doc/ansible-freeipa/README-topology.md`
* Sample playbooks in `/usr/share/doc/ansible-freeipa/playbooks/topology`