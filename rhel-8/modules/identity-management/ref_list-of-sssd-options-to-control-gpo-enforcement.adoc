:_mod-docs-content-type: REFERENCE

[id="list-of-sssd-options-to-control-gpo-enforcement_{context}"]
= List of SSSD options to control GPO enforcement

You can set the following SSSD options to limit the scope of GPO rules.

.The `ad_gpo_access_control` option

You can set the `ad_gpo_access_control` option in the `/etc/sssd/sssd.conf` file to choose between three different modes in which GPO-based access control operates.

.Table of `ad_gpo_access_control` values
[options="header",cols="3,5"]
|====
|Value of +
ad_gpo_access_control|Behavior
|`enforcing`|GPO-based access control rules are evaluated and enforced. +
*This is the default setting in RHEL 8.*
|`permissive`|GPO-based access control rules are evaluated but *not* enforced; a `syslog` message is recorded every time access would be denied.  This is the default setting in RHEL 7. +
This mode is ideal for testing policy adjustments while allowing users to continue logging in.
|`disabled`|GPO-based access control rules are neither evaluated nor enforced.
|====


.The `ad_gpo_implicit_deny` option

The `ad_gpo_implicit_deny` option is set to `False` by default. In this default state, users are allowed access if applicable GPOs are not found. If you set this option to `True`, you must explicitly allow users access with a GPO rule.

You can use this feature to harden security, but be careful not to deny access unintentionally. Red Hat recommends testing this feature while `ad_gpo_access_control` is set to `permissive`.

The following two tables illustrate when a user is allowed or rejected access based on the allow and deny login rights defined on the AD server-side and the value of `ad_gpo_implicit_deny`.

.Login behavior with `ad_gpo_implicit_deny` set to `False` *(default)*
[options="header",cols="1,1,2"]
|====
|allow-rules|deny-rules|result
|missing|missing|all users are allowed
|missing|present|only users not in deny-rules are allowed
|present|missing|only users in allow-rules are allowed
|present|present|only users in allow-rules and not in deny-rules are allowed
|====

.Login behavior with `ad_gpo_implicit_deny` set to `True`
[options="header",cols="1,1,2"]
|====
|allow-rules|deny-rules|result
|missing|missing|no users are allowed
|missing|present|no users are allowed
|present|missing|only users in allow-rules are allowed
|present|present|only users in allow-rules and not in deny-rules are allowed
|====

[role="_additional-resources"]
.Additional resources
* xref:changing-the-gpo-access-control-mode_applying-group-policy-object-access-control-in-rhel[Changing the GPO access control mode]
* `sssd-ad(5)` man page on your system
