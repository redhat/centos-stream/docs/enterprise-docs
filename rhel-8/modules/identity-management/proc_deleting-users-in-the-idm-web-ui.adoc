:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// include::assemblies/assembly_managing-user-accounts-in-the-idm-web-ui.adoc[leveloffset=+1]

[id="deleting-users-in-the-idm-web-ui_{context}"]
= Deleting users in the IdM Web UI

[role="_abstract"]
Deleting users is an irreversible operation, causing the user accounts to be permanently deleted from the IdM database, including group memberships and passwords.
Any external configuration for the user, such as the system account and home directory, is not deleted, but is no longer accessible through IdM.

You can delete:

* Active users -- the IdM Web UI offers you with the options:
** Preserving users temporarily
+
For details, see the xref:preserving-active-users-in-the-idm-web-ui_managing-user-accounts-using-the-idm-web-ui[Preserving active users in the IdM Web UI].
** Deleting them permanently

* Stage users -- you can just delete stage users permanently.

* Preserved users -- you can delete preserved users permanently.

The following procedure describes deleting active users. Similarly, you can delete user accounts on:

 * The *Stage users* tab
 * The *Preserved users* tab

.Prerequisites

* Administrator privileges for managing the IdM Web UI or User Administrator role.

.Procedure

. Log in to the IdM Web UI.
+
ifeval::[{ProductNumber} == 8]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_identity_management/accessing-the-ipa-web-ui-in-a-web-browser_configuring-and-managing-idm[Accessing the IdM Web UI in a web browser].
endif::[]


. Go to *Users -> Active users* tab.
+
Alternatively, you can delete the user account in the *Users -> Stage users* or *Users -> Preserved users*.

. Click the *Delete* icon.

. In the *Remove users* dialog box, switch the *Delete mode* radio button to *delete*.

. Click on the *Delete* button.

The users accounts have been permanently deleted from IdM.
