:_newdoc-version: 2.18.3
:_template-generated: 2024-11-08

:_mod-docs-content-type: CONCEPT

[id="the-default-idm-user-attributes_{context}"]
= The default IdM user attributes

A user entry contains attributes. The values of certain attributes are set automatically, based on defaults, unless you set a specific value yourself. For other attributes, you have to set the values manually. Certain attributes, such as `First name`, require a value, whereas others, such as `Street address`, do not. As an administrator, you can configure the values generated or used by the default attributes. For more information, see the xref:tab_default-idm-user-attributes[Default IdM user attributes] table below. 

[id="tab_default-idm-user-attributes"]
.Default IdM user attributes
[cols="3", options="header"]
|===
| Web UI field | Command-line option | Required, optional, or default?

| User login
| *username*
| Required

| First name
| --first
| Required

| Last name
| --last
| Required

| Full name
| --cn
| Optional

| Display name
| --displayname
| Optional

| Initials
| --initials
| Default

| Home directory
| --homedir
| Default

| GECOS field
| --gecos
| Default

| Shell
| --shell
| Default

| Kerberos principal
| --principal
| Default

| Email address
| --email
| Optional

| Password
| --password
| Optional.
Note that the script prompts for a new password, rather than accepting a value with the argument.

| User ID number
| --uid
| Default

| Group ID number
| --gidnumber
| Default

| Street address
| --street
| Optional

| City
| --city
| Optional

| State/Province
| --state
| Optional

| Zip code
| --postalcode
| Optional

| Telephone number
| --phone
| Optional

| Mobile telephone number
| --mobile
| Optional

| Pager number
| --pager
| Optional

| Fax number
| --fax
| Optional

| Organizational unit
| --orgunit
| Optional

| Job title
| --title
| Optional

| Manager
| --manager
| Optional

| Car license
| --carlicense
| Optional

| 
| --noprivate
| Optional

| SSH Keys
| --sshpubkey
| Optional

| Additional attributes
| --addattr
| Optional

| Department Number
| --departmentnumber
| Optional

| Employee Number
| --employeenumber
| Optional

| Employee Type
| --employeetype
| Optional

| Preferred Language
| --preferredlanguage
| Optional
|===

You can also add any attributes available in the xref:tab-default-idm-user-object-classes_{context}[Default IdM user object classes], even if no Web UI or command-line argument for that attribute exists.