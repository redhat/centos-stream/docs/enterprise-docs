:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies:
//
// assembly_creating-encrypted-backups.adoc


[id="creating-gpg-key_{context}"]
= Creating a GPG2 key

[role="_abstract"]
The following procedure describes how to generate a GPG2 key to use with encryption utilities.


.Prerequisites

* You need `root` privileges.


.Procedure

. Install and configure the `pinentry` utility.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *{PackageManagerCommand} install pinentry*
[root@server ~]# *mkdir ~/.gnupg -m 700*
[root@server ~]# *echo "pinentry-program /usr/bin/pinentry-curses" >> ~/.gnupg/gpg-agent.conf*
....

. Create a `key-input` file used for generating a GPG keypair with your preferred details. For example:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# cat >key-input <<EOF
%echo Generating a standard key
Key-Type: _RSA_
Key-Length: _2048_
Name-Real: _GPG User_
Name-Comment: _first key_
Name-Email: _root@example.com_
Expire-Date: _0_
%commit
%echo Finished creating standard key
EOF
....

. Optional: By default, GPG2 stores its keyring in the `~/.gnupg` file. To use a custom keyring location, set the `GNUPGHOME` environment variable to a directory that is only accessible by root.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *export GNUPGHOME=_/root/backup_*

[root@server ~]# *mkdir -p $GNUPGHOME -m 700*
....

. Generate a new GPG2 key based on the contents of the `key-input` file.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *gpg2 --batch --gen-key key-input*
....

. Enter a passphrase to protect the GPG2 key. You use this passphrase to access the private key for decryption.
+
[literal,subs="+quotes,attributes,verbatim"]
....
┌──────────────────────────────────────────────────────┐
│ Please enter the passphrase to                       │
│ protect your new key                                 │
│                                                      │
│ Passphrase: *_<passphrase>_*                             │
│                                                      │
│	 <OK>                             <Cancel>         │
└──────────────────────────────────────────────────────┘
....

. Confirm the correct passphrase by entering it again.
+
[literal,subs="+quotes,attributes,verbatim"]
....
┌──────────────────────────────────────────────────────┐
│ Please re-enter this passphrase                      │
│                                                      │
│ Passphrase: *_<passphrase>_*                             │
│                                                      │
│	 <OK>                             <Cancel>         │
└──────────────────────────────────────────────────────┘
....

. Verify that the new GPG2 key was created successfully.
+
[literal,subs="+quotes,attributes,verbatim"]
....
gpg: keybox '/root/backup/pubring.kbx' created
gpg: Generating a standard key
gpg: /root/backup/trustdb.gpg: trustdb created
gpg: key BF28FFA302EF4557 marked as ultimately trusted
gpg: directory '/root/backup/openpgp-revocs.d' created
gpg: revocation certificate stored as '/root/backup/openpgp-revocs.d/8F6FCF10C80359D5A05AED67BF28FFA302EF4557.rev'
gpg: *Finished creating standard key*
....


.Verification
* List the GPG keys on the server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *gpg2 --list-secret-keys*
gpg: checking the trustdb
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
_/*root*/backup/pubring.kbx_
------------------------
sec   _rsa2048_ 2020-01-13 [SCEA]
      8F6FCF10C80359D5A05AED67BF28FFA302EF4557
uid           [ultimate] _GPG User (first key) <root@example.com>_
....

[role="_additional-resources"]
.Additional resources
* link:https://gnupg.org/[GNU Privacy Guard]
