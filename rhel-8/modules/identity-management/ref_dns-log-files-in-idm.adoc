:_mod-docs-content-type: REFERENCE

[id="ref_dns-log-files-in-idm_{context}"]
= DNS log files in IdM

The following table presents directories and files that DNS uses to log information in {IPA} (IdM).

.DNS log files

[options="header", cols="1,3", adoc]
|===
|Directory or File|Description
a|`/var/log/messages`|Includes DNS error messages and other system messages. DNS logging in this file is not enabled by default. To enable it, enter the [command]`# /usr/sbin/rndc querylog` command. The command results in the following lines being added to `var/log/messages`:

`Jun 26 17:37:33 r8server named-pkcs11[1445]: received control channel command 'querylog'`

`Jun 26 17:37:33 r8server named-pkcs11[1445]: query logging is now on`

To disable logging, run the command again.
|===
