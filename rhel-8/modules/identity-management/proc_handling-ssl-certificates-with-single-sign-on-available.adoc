:_mod-docs-content-type: PROCEDURE

[id="proc_handling-ssl-certificates-with-single-sign-on-available_{context}"]
= Requesting SSL certificates with single sign-on

[role="_abstract"]
After you disabled strict Kerberos principal checks on your IdM client, you can set up SSL-based services.
SSL-based services require a certificate with `dNSName` extension records that cover all system host names, because both original (A/AAAA) and CNAME records must be in the certificate. Currently, IdM only issues certificates to host objects in the IdM database.

Follow this procedure to create a host object for `ipa-client.example.com` in IdM and make sure the real IdM machine's host object is able to manage this host.


.Prerequisites

* You have disabled the strict checks on what Kerberos principal is used to target the Kerberos server.

.Procedure

. Create a new host object on the IdM server:
+
[literal]
--
[root@idm-server.idm.example.com ~]# ipa host-add idm-client.ad.example.com --force
--

+
Use the `--force` option, because the host name is a CNAME and not an A/AAAA record.

. On the IdM server, allow the IdM DNS host name to manage the Active Directory host entry in the IdM database:
+
[literal]
--
[root@idm-server.idm.example.com ~]# ipa host-add-managedby idm-client.ad.example.com \
      --hosts=idm-client.idm.example.com
--

. Your can now request an SSL certificate for your IdM client with the `dNSName` extension record for its host name within the Active Directory DNS domain:
+
[literal]
--
[root@idm-client.idm.example.com ~]# ipa-getcert request -r \
      -f /etc/httpd/alias/server.crt \
      -k /etc/httpd/alias/server.key \
      -N CN=`hostname --fqdn` \
      -D `hostname --fqdn` \
      -D idm-client.ad.example.com \
      -K host/idm-client.idm.example.com@IDM.EXAMPLE.COM \
      -U id-kp-serverAuth
--
