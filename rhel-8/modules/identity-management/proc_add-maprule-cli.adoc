:_mod-docs-content-type: PROCEDURE
// [[proc-add-maprule-cli_{context}]]
[id="proc-add-maprule-cli_{context}"]
= Adding a certificate mapping rule in the IdM CLI

[role="_abstract"]
. Obtain the administrator's credentials:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *kinit admin*
....

. Enter the mapping rule and the matching rule the mapping rule is based on. For example, to make IdM search for the `Issuer` and `Subject` entries in any certificate presented, and base its decision to authenticate or not on the information found in these two entries of the presented certificate, recognizing only certificates issued by the `Smart Card CA` of the `EXAMPLE.ORG` organization:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *ipa certmaprule-add `rule_name` --matchrule '<ISSUER>CN=Smart Card CA,O=EXAMPLE.ORG' --maprule '(ipacertmapdata=X509:<I>{issuer_dn!nss_x500}<S>{subject_dn!nss_x500})'*
-------------------------------------------------------
Added Certificate Identity Mapping Rule "rule_name"
-------------------------------------------------------
  Rule name: rule_name
  Mapping rule: (ipacertmapdata=X509:<I>{issuer_dn!nss_x500}<S>{subject_dn!nss_x500})
  Matching rule: <ISSUER>CN=Smart Card CA,O=EXAMPLE.ORG
  Enabled: TRUE
....

. The System Security Services Daemon (SSSD) periodically re-reads the certificate mapping rules. To force the newly-created rule to be loaded immediately, restart SSSD:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *systemctl restart sssd*
....

Now you have a certificate mapping rule set up that compares the type of data specified in the mapping rule that it finds on a smart card certificate with the certificate mapping data in your IdM user entries. Once it finds a match, it authenticates the matching user.
