:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="configuring-a-default-user-group-using-idm-web-ui_{context}"]
= Configuring a default user group using IdM Web UI

[role="_abstract"]
When you configure a default user group, new user entries that do not match any automember rule are automatically added to this default group.

.Prerequisites

* You are logged in to the IdM Web UI.
* You must be a member of the `admins` group.
* The target user group you want to set as default exists in IdM.

.Procedure

. Click *Identity -> Automember*, and select *User group rules*.
. In the *Default user group* field, select the group you want to set as the default user group.
+
image::automember-default-user-group.png[Setting a default user group]
