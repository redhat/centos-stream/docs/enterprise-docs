:_mod-docs-content-type: PROCEDURE
[id="configuring-smart-card-authentication-to-enable-user-password-authentication_{context}"]
= Configure your system to enable both smart card and password authentication

[role="_abstract"]
Follow this procedure to enable both smart card and password authentication on your system.

.Prerequisites

* The Smart card contains your certificate and private key.
* The card is inserted into the reader and connected to the computer.
* The `authselect` tool is installed on your system.

.Procedure

* Enter the following command to allow smart card and password authentication:
+
[literal]
....
# authselect select sssd with-smartcard --force
....

At this point, smart card authentication is enabled, however, password authentication will work if you forget your smart card at home.
