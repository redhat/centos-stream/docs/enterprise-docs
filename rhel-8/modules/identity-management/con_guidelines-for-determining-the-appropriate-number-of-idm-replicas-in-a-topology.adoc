:_mod-docs-content-type: CONCEPT

[id="guidelines-for-determining-the-appropriate-number-of-idm-replicas-in-a-topology_{context}"]
= Guidelines for determining the appropriate number of IdM replicas in a topology

[role="_abstract"]
Plan IdM topology to match your organization's requirements and ensure optimal performance and service availability.

Set up at least two replicas in each data center:: Deploy at least two replicas in each data center to ensure that if one server fails, the replica can  take over and handle requests.

Set up a sufficient number of servers to serve your clients:: One {IPA} (IdM) server can provide services to 2000 - 3000 clients. This assumes the clients query the servers multiple times a day, but not, for example, every minute. If you expect frequent queries, plan for more servers.

Set up a sufficient number of Certificate Authority (CA) replicas:: Only replicas with the CA role installed can replicate certificate data. If you use the IdM CA, ensure your environment has at least two CA replicas with certificate replication agreements between them.

Set up a maximum of 60 replicas in a single IdM domain:: Red{nbsp}Hat supports environments with up to 60 replicas.
