:_module-type: PROCEDURE

[id="proc_increasing-sssd-timeouts_{context}"]
= Increasing SSSD timeouts

[role="_abstract"]
If you are having issues authenticating with a smart card, check the `krb5_child.log` and the `p11_child.log` file for timeout entries similar to the following:

`krb5_child: Timeout for child [9607] reached.....consider increasing value of krb5_auth_timeout.`

If there is a timeout entry in the log file, try increasing the SSSD timeouts as outlined in this procedure.

.Prerequisites

* You have configured your IdM Server and client for smart card authentication.

.Procedure

. Open the `sssd.conf` file on the IdM client:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
# vim /etc/sssd/sssd.conf
----

. In your domain section, for example `[domain/idm.example.com]`, add the following option:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
krb5_auth_timeout = 60
----

. In the `[pam]` section, add the following:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
p11_child_timeout = 60
----

. Clear the SSSD cache:
+
[subs="+quotes",options="nowrap",role="white-space-pre"]
----
# sssctl cache-remove
SSSD must not be running. Stop SSSD now? (yes/no) [yes] yes
Creating backup of local data…
Removing cache files…
SSSD needs to be running. Start SSSD now? (yes/no) [yes] yes
----

Once you have increased the timeouts, try authenticating again using your smart card. See xref:proc_testing-smart-card-authentication_assembly_troubleshooting-authentication-with-smart-cards[Testing smart card authentication] for more details.
