:_mod-docs-content-type: CONCEPT

[id="con_required-steps-for-smart-card-authentication-in-idm_{context}"]
= Required steps for smart card authentication in IdM

You must ensure the following steps have been followed before you can authenticate with a smart card in Identity Management (IdM):

* Configure your IdM server for smart card authentication. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#conf-idm-server-for-smart-card-auth_configuring-idm-for-smart-card-auth[Configuring the IdM server for smart card authentication]

* Configure your IdM client for smart card authentication. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#conf-idm-client-for-smart-card-auth_configuring-idm-for-smart-card-auth[Configuring the IdM client for smart card authentication]

* Add the certificate to the user entry in IdM. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#proc-add-cert-idm-user-webui_configuring-idm-for-smart-card-auth[Adding a certificate to a user entry in the IdM Web UI]

* Store your keys and certificates on the smart card. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#storing-a-certificate-on-the-smart-card_configuring-idm-for-smart-card-auth[Storing a certificate on a smart card]
