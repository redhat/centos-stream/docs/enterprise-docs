:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_adjusting-idm-directory-server-performance.adoc

:experimental:

[id="adjusting-the-maximum-number-of-database-locks_{context}"]
= Adjusting the maximum number of database locks

[role="_abstract"]
Lock mechanisms control how many copies of Directory Server processes can run at the same time, and the `nsslapd-db-locks` parameter sets the maximum number of locks.

Increase the maximum number of locks if if you see the following error messages in the [filename]`/var/log/dirsrv/slapd-_instance_name_/errors` log file:

[literal,subs="+quotes,attributes,verbatim"]
....
libdb: Lock table is *out of available locks*
....


[cols="h,1"]
|===
| Default value | `50000` locks
| Valid range | `0 - 2147483647`
| Entry DN location | `cn=bdb,cn=config,cn=ldbm database,cn=plugins,cn=config`
|===

.Prerequisites

* The LDAP Directory Manager password

.Procedure

. Retrieve the current value of the `nsslapd-db-locks` parameter and make a note of it before making any adjustments, in case it needs to be restored.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ldapsearch -D "cn=directory manager" -w _DirectoryManagerPassword_ -b "cn=bdb,cn=config,cn=ldbm database,cn=plugins,cn=config" | grep nsslapd-db-locks*
nsslapd-db-locks: 50000
....

. Modify the value of the `locks` attribute. This example doubles the value to `100000` locks.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *dsconf -D "cn=Directory Manager" _ldap://server.example.com_ backend config set --locks=_100000_*
....

. Authenticate as the Directory Manager to make the configuration change.
+
[literal,subs="+quotes,attributes,verbatim"]
....
Enter password for cn=Directory Manager on _ldap://server.example.com_:
Successfully updated database configuration
....

. Restart the Directory Server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *systemctl restart dirsrv.target*
....


.Verification

* Display the value of the `nsslapd-db-locks` attribute and verify it has been set to your desired value.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ldapsearch -D "cn=directory manager" -w _DirectoryManagerPassword_ -b "cn=bdb,cn=config,cn=ldbm database,cn=plugins,cn=config" | grep nsslapd-db-locks*
nsslapd-db-locks: *100000*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_directory_server/11/html/configuration_command_and_file_reference/plug_in_implemented_server_functionality_reference#nsslapd_db_locks[nsslapd-db-locks] in Directory Server 11 documentation
