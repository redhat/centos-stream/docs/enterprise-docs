:_mod-docs-content-type: PROCEDURE

[id="proc_stopping-replication-between-two-servers-using-command-line_{context}"]
[[managing-topology-stop-cli]]
= Stopping replication between two servers using the CLI


[role="_abstract"]
You can terminate replication agreements from command line using the `ipa topology segment-del` command.

.Prerequisites
* You have the IdM administrator credentials.

.Procedure
. Optional. If you do not know the name of the specific replication segment that you want to remove, display all segments available. Use the [command]`ipa topologysegment-find` command. When prompted, provide the required topology suffix: `domain` or `ca`. For example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa topologysegment-find
Suffix name: pass:quotes[_domain_]
------------------
8 segments matched
------------------
  pass:quotes[_Segment name: new_segment_]
  Left node: server1.example.com
  Right node: server2.example.com
  Connectivity: both

...

----------------------------
Number of entries returned 8
----------------------------
....

+
Locate the required segment in the output.
 

. Remove the topology segment joining the two servers:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa topologysegment-del
Suffix name: pass:quotes[_domain_]
Segment name: pass:quotes[_new_segment_]
-----------------------------
Deleted segment "new_segment"
-----------------------------
....
+
Deleting the segment removes the replication agreement.


.Verification

* Verify that the segment is no longer listed:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa topologysegment-find
Suffix name: _domain_
------------------
7 segments matched
------------------
  Segment name: server2.example.com-to-server3.example.com
  Left node: server2.example.com
  Right node: server3.example.com
  Connectivity: both

...

----------------------------
Number of entries returned 7
----------------------------
....
