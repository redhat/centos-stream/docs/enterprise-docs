:_mod-docs-content-type: PROCEDURE
// Module included in the following titles/assemblies:
//
// assembly_managing-dns-forwarding-in-idm.adoc

[id="adding-a-global-forwarder-in-the-cli_{context}"]
= Adding a global forwarder in the CLI

[role="_abstract"]
Follow this procedure to add a global DNS forwarder by using the command line (CLI).

.Prerequisites

* You are logged in as IdM administrator.
* You know the Internet Protocol (IP) address of the DNS server to forward queries to.

.Procedure

* Use the `ipa dnsconfig-mod` command to add a new global forwarder. Specify the IP address of the DNS forwarder with the `--forwarder` option.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[user@server ~]$ *ipa dnsconfig-mod --forwarder=_10.10.0.1_*
Server will check DNS forwarder(s).
This may take some time, please wait ...
  Global forwarders: 10.10.0.1
  IPA DNS servers: server.example.com
....


.Verification

* Use the `dnsconfig-show` command to display global forwarders.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[user@server ~]$ ipa dnsconfig-show
  Global forwarders: 10.10.0.1
  IPA DNS servers: server.example.com
....
