:_newdoc-version: 2.17.0
:_template-generated: 2024-04-16
:_mod-docs-content-type: PROCEDURE

[id="setting-and-removing-kerberos-flags-from-the-command-line_{context}"]
= Setting and removing Kerberos flags from the command line

You can add or remove a Kerberos flag by using the command line. The `ipa service-mod` command uses the following command options for the flags:

* `--ok-as-delegate` for `OK_AS_DELEGATE`
* `--requires-pre-auth` for `REQUIRES_PRE_AUTH`
* `--ok-to-auth-as-delegate` for `OK_TO_AUTH_AS_DELEGATE`

By setting an option value to `1`, you enable a flag for a principle. By setting an option value to `0`, you disable the flag.

The following procedure enables and disables the `OK_AS_DELEGATE` flag for the `service/ipa.example.com@example.com` principal. 

.Procedure

* To add the `OK_AS_DELEGATE` flag for the `service/ipa.example.com@example.com` principle, run: 
+
[subs=+quotes]
----
$ *ipa service-mod _service/ipa.example.com@EXAMPLE.COM_ --ok-as-delegate=1*
----
* To remove the `OK_AS_DELEGATE` flag from the `service/ipa.example.com@example.com` principle, run: 
+
[subs=+quotes]
----
$ *ipa service-mod _service/ipa.example.com@EXAMPLE.COM_ --ok-as-delegate=0*
----