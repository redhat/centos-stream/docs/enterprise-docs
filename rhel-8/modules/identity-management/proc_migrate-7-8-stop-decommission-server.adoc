:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// rhel-8/assemblies/assembly_migrating-from-RHEL7-to-RHEL8.adoc

[id=stop-decommission-server_{context}]

= Stopping and decommissioning the RHEL 7 server

[role="_abstract"]
. Ensure that all data, including the latest changes, have been correctly migrated from `rhel7.example.com` to `rhel8.example.com`. For example:
.. Add a new user on `rhel7.example.com`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel7 ~]# *ipa user-add _random_user_*
First name: random
Last name: user
....

.. Check that the user has been replicated to `rhel8.example.com`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel8 ~]# *ipa user-find _random_user_*
--------------
1 user matched
--------------
  User login: random_user
  First name: random
  Last name: user
....

. Ensure that a Distributed Numeric Assignment (DNA) ID range is allocated to `rhel8.example.com`. Use one of the following methods:

** Activate the DNA plug-in on `rhel8.example.com` directly by creating another test user:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel8 ~]# *ipa user-add _another_random_user_*
First name: another
Last name: random_user
....

** Assign a specific DNA ID range to `rhel8.example.com`:

+
--
... On `rhel7.example.com`, display the IdM ID range:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel7 ~]# *ipa idrange-find*
----------------
3 ranges matched
----------------
  Range name: EXAMPLE.COM_id_range
  First Posix ID of the range: *196600000*
  Number of IDs in the range: *200000*
  First RID of the corresponding RID range: 1000
  First RID of the secondary RID range: 100000000
  Range type: local domain range
....

... On `rhel7.example.com`, display the allocated DNA ID ranges:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel7 ~]# *ipa-replica-manage dnarange-show*
rhel7.example.com: *196600026-196799999*
rhel8.example.com: *No range set*
....

... Reduce the DNA ID range allocated to `rhel7.example.com` so that a section becomes available to `rhel8.example.com`:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel7 ~]# *ipa-replica-manage dnarange-set rhel7.example.com 196600026-196699999*
....

... Assign the remaining part of the IdM ID range to `rhel8.example.com`:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel7 ~]# *ipa-replica-manage dnarange-set rhel8.example.com 196700000-196799999*
....
--

+
. Stop all IdM services on `rhel7.example.com` to force domain discovery to the new `rhel8.example.com` server.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@rhel7 ~]# *ipactl stop*
Stopping CA Service
Stopping pki-ca:                                           [  OK  ]
Stopping HTTP Service
Stopping httpd:                                            [  OK  ]
Stopping MEMCACHE Service
Stopping ipa_memcached:                                    [  OK  ]
Stopping DNS Service
Stopping named: .                                          [  OK  ]
Stopping KPASSWD Service
Stopping Kerberos 5 Admin Server:                          [  OK  ]
Stopping KDC Service
Stopping Kerberos 5 KDC:                                   [  OK  ]
Stopping Directory Service
Shutting down dirsrv:
    EXAMPLE-COM...                                         [  OK  ]
    PKI-IPA...                                             [  OK  ]
....

+
After this, the `ipa` utility will contact the new server through a remote procedure call (RPC).

+
. Remove the RHEL 7 server from the topology by executing the removal commands on the RHEL 8 server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_identity_management/uninstalling-an-ipa-server_installing-identity-management[Uninstalling an IdM server].

.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/adjusting-id-ranges-manually_configuring-and-managing-idm[Adjusting ID ranges manually]
