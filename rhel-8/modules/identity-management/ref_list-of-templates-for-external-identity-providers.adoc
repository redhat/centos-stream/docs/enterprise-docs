:_mod-docs-content-type: REFERENCE

[id="ref_list-of-templates-for-external-identity-providers_{context}"]
= List of templates for external identity providers

The following identity providers (IdPs) support OAuth 2.0 device authorization grant flow:

* Microsoft Identity Platform, including Azure AD
* Google
* GitHub
* Keycloak, including Red Hat Single Sign-On (SSO)
* Okta

When using the `ipa idp-add` command to create a reference to one of these external IdPs, you can specify the IdP type with the `--provider` option, which expands into additional options as described below:


`--provider=microsoft`::
Microsoft Azure IdPs allow parametrization based on the Azure tenant ID, which you can specify with the `--organization` option to the `ipa idp-add` command. If you need support for the live.com IdP, specify the option `--organization common`.
+
Choosing `--provider=microsoft` expands to use the following options. The value of the `--organization` option replaces the string `${ipaidporg}` in the table.
+
[options="header",cols="1,2"]
|====
|Option|Value
|`--auth-uri=URI` |`\https://login.microsoftonline.com/${ipaidporg}/oauth2/v2.0/authorize`
|`--dev-auth-uri=URI` |`\https://login.microsoftonline.com/${ipaidporg}/oauth2/v2.0/devicecode`
|`--token-uri=URI` |`\https://login.microsoftonline.com/${ipaidporg}/oauth2/v2.0/token`
|`--userinfo-uri=URI` |`\https://graph.microsoft.com/oidc/userinfo`
|`--keys-uri=URI` |`\https://login.microsoftonline.com/common/discovery/v2.0/keys`
|`--scope=STR` |`openid email`
|`--idp-user-id=STR` |`email`
|====



`--provider=google`::
Choosing `--provider=google` expands to use the following options:
+
[options="header",cols="1,2"]
|====
|Option|Value
|`--auth-uri=URI` |`\https://accounts.google.com/o/oauth2/auth`
|`--dev-auth-uri=URI` |`\https://oauth2.googleapis.com/device/code`
|`--token-uri=URI` |`\https://oauth2.googleapis.com/token`
|`--userinfo-uri=URI` |`\https://openidconnect.googleapis.com/v1/userinfo`
|`--keys-uri=URI` |`\https://www.googleapis.com/oauth2/v3/certs`
|`--scope=STR` |`openid email`
|`--idp-user-id=STR` |`email`
|====


`--provider=github`::
Choosing `--provider=github` expands to use the following options:
+
[options="header",cols="1,2"]
|====
|Option|Value
|`--auth-uri=URI` |`\https://github.com/login/oauth/authorize`
|`--dev-auth-uri=URI` |`\https://github.com/login/device/code`
|`--token-uri=URI` |`\https://github.com/login/oauth/access_token`
|`--userinfo-uri=URI` |`\https://openidconnect.googleapis.com/v1/userinfo`
|`--keys-uri=URI` |`\https://api.github.com/user`
|`--scope=STR` |`user`
|`--idp-user-id=STR` |`login`
|====


`--provider=keycloak`::
With Keycloak, you can define multiple realms or organizations. Since it is often a part of a custom deployment, both base URL and realm ID are required, and you can specify them with the `--base-url` and `--organization` options to the `ipa idp-add` command:
+
[literal,subs="+quotes,verbatim"]
....
[root@client ~]# ipa idp-add MySSO --provider keycloak \
    *--org main --base-url keycloak.domain.com:8443/auth* \
    --client-id _<your-client-id>_
....
+
Choosing `--provider=keycloak` expands to use the following options. The value you specify in the `--base-url` option replaces the string `${ipaidpbaseurl}` in the table, and the value you specify for the `--organization `option replaces the string `${ipaidporg}`.
+
[options="header",cols="1,2"]
|====
|Option|Value
|`--auth-uri=URI` |`\https://${ipaidpbaseurl}/realms/${ipaidporg}/protocol/openid-connect/auth`
|`--dev-auth-uri=URI` |`\https://${ipaidpbaseurl}/realms/${ipaidporg}/protocol/openid-connect/auth/device`
|`--token-uri=URI` |`\https://${ipaidpbaseurl}/realms/${ipaidporg}/protocol/openid-connect/token`
|`--userinfo-uri=URI` |`\https://${ipaidpbaseurl}/realms/${ipaidporg}/protocol/openid-connect/userinfo`
|`--scope=STR` |`openid email`
|`--idp-user-id=STR` |`email`
|====


`--provider=okta`::
After registering a new organization in Okta, a new base URL is associated with it. You can specify this base URL with the `--base-url` option to the `ipa idp-add` command:
+
[literal,subs="+quotes,verbatim"]
....
[root@client ~]# ipa idp-add MyOkta --provider okta --base-url dev-12345.okta.com --client-id _<your-client-id>_
....
+
Choosing `--provider=okta` expands to use the following options. The value you specify for the `--base-url` option replaces the string `${ipaidpbaseurl}` in the table.
+
[options="header",cols="1,2"]
|====
|Option|Value
|`--auth-uri=URI` |`\https://${ipaidpbaseurl}/oauth2/v1/authorize`
|`--dev-auth-uri=URI` |`\https://${ipaidpbaseurl}/oauth2/v1/device/authorize`
|`--token-uri=URI` |`\https://${ipaidpbaseurl}/oauth2/v1/token`
|`--userinfo-uri=URI` |`\https://${ipaidpbaseurl}/oauth2/v1/userinfo`
|`--scope=STR` |`openid email`
|`--idp-user-id=STR` |`email`
|====



[role="_additional-resources"]
.Additional resources
* link:https://freeipa.readthedocs.io/en/latest/designs/external-idp/idp-api.html#pre-populated-idp-templates[Pre-populated IdP templates]
