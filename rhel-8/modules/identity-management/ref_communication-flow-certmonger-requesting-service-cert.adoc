:_mod-docs-content-type: REFERENCE
[id="communication-flow-certmonger-requesting-service-cert_{context}"]
= Communication flow for certmonger requesting a service certificate

[role="_abstract"]
These diagrams show the stages of what happens when `certmonger` requests a service certificate from Identity Management (IdM) certificate authority (CA) server. The sequence consists of these diagrams:

* xref:{context}_certmonger-service-cert-1[Unencrypted communication]
* xref:{context}_certmonger-service-cert-2[Certmonger requesting a service certificate]
* xref:{context}_certmonger-service-cert-3[IdM CA issuing the service certificate]
* xref:{context}_certmonger-service-cert-4[Certmonger applying the service certificate]
* xref:{context}_certmonger-service-cert-5[Certmonger requesting a new certificate when the old one is nearing expiration]

ifdef::lightweight-sub-ca[]
In the diagrams, the `webserver-ca` sub-CA is represented by the generic `IdM CA server`.
endif::lightweight-sub-ca[]

//xref:{context}-certmonger-service-cert-1[insert text if using] shows four data centers, each with four servers. The servers are connected with replication agreements.

xref:{context}_certmonger-service-cert-1[Unencrypted communication]
 shows the initial situation: without an HTTPS certificate, the communication between the web server and the browser is unencrypted.

[#{context}_certmonger-service-cert-1]
.Unencrypted communication
image::84_RHEL_IdM_0420_1.png[A diagram displaying an IdM client running an Apache web server and the certmonger service. There are arrows between a browser and the Apache webserver showing it is connecting over an unencrypted HTTP connection. There is an inactive connection from the certmonger service to an IdM CA server.]
//{imagesdir}
{sp} +


xref:{context}_certmonger-service-cert-2[Certmonger requesting a service certificate]
shows the system administrator using `certmonger` to manually request an HTTPS certificate for the Apache web server. Note that when requesting a web server certificate, certmonger does not communicate directly with the CA. It proxies through IdM.


[#{context}_certmonger-service-cert-2]
.Certmonger requesting a service certificate
image::84_RHEL_IdM_0420_2.png[A diagram displaying an arrow between the certmonger service on the IdM client and the IdM CA server to show it is connecting via an ipa-getcert request.]
//{imagesdir}
{sp} +


xref:{context}_certmonger-service-cert-3[IdM CA issuing the service certificate]
shows an IdM CA issuing an HTTPS certificate for the web server.

[#{context}_certmonger-service-cert-3]
.IdM CA issuing the service certificate
image::84_RHEL_IdM_0420_3.png[A diagram displaying an arrow between the IdM CA server and the certmonger service on the IdM client - showing it is connecting and sending an HTTPS certificate.]
//{imagesdir}
{sp} +

xref:{context}_certmonger-service-cert-4[Certmonger applying the service certificate]
shows `certmonger` placing the HTTPS certificate in appropriate locations on the IdM client and, if instructed to do so, restarting the `httpd` service. The Apache server subsequently uses the HTTPS certificate to encrypt the traffic between itself and the browser.


[#{context}_certmonger-service-cert-4]
.Certmonger applying the service certificate
image::84_RHEL_IdM_0420_4.png[A diagram displaying an image of an HTTPS certificate assigned to the Apache web server and one assigned to the certmonger service. There are arrows between the browser and the Apache webserver showing that the connection is now an encrypted HTTPS connection. The connection between the certmonger service and the IdM CA server is inactive.]
//{imagesdir}
{sp} +


xref:{context}_certmonger-service-cert-5[Certmonger requesting a new certificate when the old one is nearing expiration]
 shows `certmonger` automatically requesting a renewal of the service certificate from the IdM CA before the expiration of the certificate. The IdM CA issues a new certificate.


[#{context}_certmonger-service-cert-5]
.Certmonger requesting a new certificate when the old one is nearing expiration
image::84_RHEL_IdM_0420_5.png[A diagram displaying an arrow from the certmonger service on the IdM client connecting to the IdM CA server to show it is performing an ipa-getcert request. An arrow from the IdM CA server to the Certmonger is labeled HTTPS certificate to show it is transferring an HTTPS certificate to the certmonger service.]
//{imagesdir}
{sp} +
