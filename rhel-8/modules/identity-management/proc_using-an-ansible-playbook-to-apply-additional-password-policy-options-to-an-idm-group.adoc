:_mod-docs-content-type: PROCEDURE

[id="using-an-ansible-playbook-to-apply-additional-password-policy-options-to-an-idm-group_{context}"]
= Using an Ansible playbook to apply additional password policy options to an IdM group

You can use an Ansible playbook to apply additional password policy options to strengthen the password policy requirements for a specific IdM group.
You can use the `maxrepeat`, `maxsequence`, `dictcheck` and `usercheck` password policy options for this purpose.
The example describes how to set the following requirements for the *managers* group:

* Users' new passwords do not contain the users' respective user names.
* The passwords contain no more than two identical characters in succession.
* Any monotonic character sequences in the passwords are not longer than 3 characters. This means that the system does not accept a password with a sequence such as *1234* or *abcd*.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** You have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
endif::[]
** You have stored your `ipaadmin_password` in the *secret.yml* Ansible vault.

* The group for which you are ensuring the presence of a password policy exists in IdM.

.Procedure

. Create your Ansible playbook file *manager_pwpolicy_present.yml* that defines the password policy whose presence you want to ensure. To simplify this step, copy and modify the following example:

+
[literal,subs="+quotes,attributes,verbatim"]
....

---
- name: Tests
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure presence of usercheck and maxrepeat pwpolicy for group managers
    ipapwpolicy:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: managers
      usercheck: True
      maxrepeat: 2
      maxsequence: 3
....

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory/inventory.file__ __path_to_playbooks_directory_/manager_pwpolicy_present.yml__*
....



.Verification

. Add a test user named *test_user*:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa user-add test_user*
First name: test
Last name: user
----------------------------
Added user "test_user"
----------------------------
....

. Add the test user to the *managers* group:

.. In the IdM Web UI, click menu:Identity[Groups > User Groups].

.. Click *managers*.

.. Click `Add`.

.. In the *Add users into user group 'managers'* page, check *test_user*.

.. Click the `>` arrow to move the user to the `Prospective` column.

.. Click `Add`.

. Reset the password for the test user:

.. Go to menu:Identity[Users].

.. Click *test_user*.

.. In the `Actions` menu, click `Reset Password`.

.. Enter a temporary password for the user.

. On the command line, try to obtain a Kerberos ticket-granting ticket (TGT) for the *test_user*:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *kinit test_user*
....

.. Enter the temporary password.

.. The system informs you that you must change your password. Enter a password that contains the user name of *test_user*:
+
[literal,subs="+quotes,attributes,verbatim"]
....
Password expired. You must change it now.
Enter new password:
Enter it again:
Password change rejected: Password not changed.
Unspecified password quality failure while trying to change password.
Please try again.
....

+
NOTE: Kerberos does not have fine-grained error password policy reporting and, in certain cases, does not provide a clear reason why a password was rejected.

+
.. The system informs you that the entered password was rejected. Enter a password that contains three or more identical characters in succession:
+
[literal,subs="+quotes,attributes,verbatim"]
....
Password change rejected: Password not changed.
Unspecified password quality failure while trying to change password.
Please try again.

Enter new password:
Enter it again:
....

+
.. The system informs you that the entered password was rejected. Enter a password that contains a monotonic character sequence longer than 3 characters. Examples of such sequences include *1234* and *fedc*:
+
[literal,subs="+quotes,attributes,verbatim"]
....
Password change rejected: Password not changed.
Unspecified password quality failure while trying to change password.
Please try again.

Enter new password:
Enter it again:
....

+
.. The system informs you that the entered password was rejected. Enter a password that meets the criteria of the *managers* password policy:
+
[literal,subs="+quotes,attributes,verbatim"]
....
Password change rejected: Password not changed.
Unspecified password quality failure while trying to change password.
Please try again.

Enter new password:
Enter it again:
....

. Verify that you have obtained a TGT, which is only possible after having entered a valid password:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *klist*
Ticket cache: KCM:0:33945
Default principal: test_user@IDM.EXAMPLE.COM

Valid starting       Expires              Service principal
07/07/2021 12:44:44  07/08/2021 12:44:44  krbtgt@IDM.EXAMPLE.COM@IDM.EXAMPLE.COM
....


[role="_additional-resources"]
.Additional resources
* xref:additional-password-policy-options-in-idm_{context}[Additional password policies in IdM]
* `/usr/share/doc/ansible-freeipa/README-pwpolicy.md`
* `/usr/share/doc/ansible-freeipa/playbooks/pwpolicy`
