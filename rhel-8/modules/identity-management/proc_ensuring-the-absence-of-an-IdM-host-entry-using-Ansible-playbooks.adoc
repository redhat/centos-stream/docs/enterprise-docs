:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-absence-of-an-IdM-host-entry-using-Ansible-playbooks_{context}"]
= Ensuring the absence of an IdM host entry using Ansible playbooks

[role="_abstract"]
Follow this procedure to ensure the absence of host entries in {IPA} (IdM) using Ansible playbooks.


.Prerequisites

* IdM administrator credentials

.Procedure

. Create an inventory file, for example `inventory.file`, and define `ipaserver` in it:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Create an Ansible playbook file with the `fully-qualified domain name` (FQDN) of the host whose absence from IdM you want to ensure. If your IdM domain has integrated DNS, use the `updatedns: true` option to remove the associated records of any kind for the host from the DNS.

+
To simplify this step, you can copy and modify the example in the `/usr/share/doc/ansible-freeipa/playbooks/host/delete-host.yml` file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Host absent
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Host host01.idm.example.com absent
    ipahost:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: host01.idm.example.com
      updatedns: true
      state: absent
....


+
. Run the playbook:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i _path_to_inventory_directory/inventory.file_ _path_to_playbooks_directory/ensure-host-absent.yml_*
....

[NOTE]
====
The procedure results in:

* The host not being present in the IdM Kerberos realm.
* The host entry not being present in the IdM LDAP server.

To remove the specific IdM configuration of system services, such as System Security Services Daemon (SSSD), from the client host itself, you must run the `ipa-client-install --uninstall` command on the client. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/uninstalling-an-ipa-client_installing-identity-management[Uninstalling an IdM client].
====

.Verification

. Log into `ipaserver` as admin:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ssh admin@server.idm.example.com*
Password:
[admin@server /]$
....

. Display information about _host01.idm.example.com_:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa host-show host01.idm.example.com*
ipa: ERROR: host01.idm.example.com: host not found
....


The output confirms that the host does not exist in IdM.
