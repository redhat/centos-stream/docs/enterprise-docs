[id="changing-your-user-password-or-resetting-another-users-password-in-idm-cli_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Changing your user password or resetting another user's password in IdM CLI
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

You can change your user password using the {IPA} (IdM) command-line interface (CLI). If you are an administrative user, you can use the CLI to reset another user's password.

.Prerequisites

* You have obtained a ticket-granting ticket (TGT) for an IdM user.
* If you are resetting another user's password, you must have obtained a TGT for an administrative user in IdM.

.Procedure

* Enter the [command]`ipa user-mod` command with the name of the user and the [option]`--password` option. The command will prompt you for the new password.

+
[literal,subs="+quotes,verbatim,macros"]
....

$ *ipa user-mod _idm_user_ --password*
Password:
Enter Password again to verify:
--------------------
Modified user "idm_user"
--------------------
...

....

[NOTE]
====
You can also use the `ipa passwd` _idm_user_ command instead of `ipa user-mod`.
====
