:_mod-docs-content-type: CONCEPT
[id="vaults-and-their-benefits_{context}"]
= Vaults and their benefits

[role="_abstract"]
A vault is a useful feature for those {IPA} (IdM) users who want to keep all their sensitive data stored securely but conveniently in one place. There are various types of vaults and you should choose which vault to use based on your requirements.

A vault is a secure location in (IdM) for storing, retrieving, sharing, and recovering a secret. A secret is security-sensitive data, usually authentication credentials, that only a limited group of people or entities can access. For example, secrets include:

* Passwords

* PINs

* Private SSH keys

A vault is comparable to a password manager. Just like a password manager, a vault typically requires a user to generate and remember one primary password to unlock and access any information stored in the vault. However, a user can also decide to have a standard vault. A standard vault does not require the user to enter any password to access the secrets stored in the vault.

[NOTE]
====
The purpose of vaults in IdM is to store authentication credentials that allow you to authenticate to external, non-IdM-related services.
====

Other important characteristics of the IdM vaults are:

* Vaults are only accessible to the vault owner and those IdM users that the vault owner selects to be the vault members. In addition, the IdM administrator has access to the vault.

* If a user does not have sufficient privileges to create a vault, an IdM administrator can create the vault and set the user as its owner.

* Users and services can access the secrets stored in a vault from any machine enrolled in the IdM domain.

* One vault can only contain one secret, for example, one file. However, the file itself can contain multiple secrets such as passwords, keytabs or certificates.


[NOTE]
====

Vault is only available from the IdM command line (CLI), not from the IdM Web UI.

====
