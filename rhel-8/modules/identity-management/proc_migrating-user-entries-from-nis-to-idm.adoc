:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_migrating-from-nis-to-identity-management.adoc


[id="proc_migrating-user-entries-from-nis-to-idm_{context}"]
[[nis-import-users]]
= Migrating user entries from NIS to IdM

[role="_abstract"]
The NIS `passwd` map contains information about users, such as names, UIDs, primary group, GECOS, shell, and home directory. Use this data to migrate NIS user accounts to {IPA} (IdM):

.Prerequisites

* You have root access on NIS server.
* xref:enabling-nis[NIS is enabled in IdM.]
* The NIS server is enrolled into IdM.
* You have link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/managing_idm_users_groups_hosts_and_access_control_rules/adjusting-id-ranges-manually_managing-users-groups-hosts#id-ranges_adjusting-id-ranges-manually[ID ranges] that can store UIDs of importing users.


.Procedure

. Install the `yp-tools` package:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@nis-server ~]# {PackageManagerCommand} install yp-tools -y
....
. On the NIS server create the `/root/nis-users.sh` script with the following content:
+
[source,bash]
....
#!/bin/sh
# $1 is the NIS domain, $2 is the primary NIS server
ypcat -d $1 -h $2 passwd > /dev/shm/nis-map.passwd 2>&1

IFS=$'\n'
for line in $(cat /dev/shm/nis-map.passwd) ; do
	IFS=' '
	username=$(echo $line | cut -f1 -d:)
	# Not collecting encrypted password because we need cleartext password
	# to create kerberos key
	uid=$(echo $line | cut -f3 -d:)
	gid=$(echo $line | cut -f4 -d:)
	gecos=$(echo $line | cut -f5 -d:)
	homedir=$(echo $line | cut -f6 -d:)
	shell=$(echo $line | cut -f7 -d:)

	# Now create this entry
	echo passw0rd1 | ipa user-add $username --first=NIS --last=USER \
	     --password --gidnumber=$gid --uid=$uid --gecos="$gecos" --homedir=$homedir \
	     --shell=$shell
	ipa user-show $username
done

....

. Authenticate as the IdM `admin` user:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@nis-server ~]# kinit admin
....

. Run the script. For example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
[root@nis-server ~]# sh /root/nis-users.sh pass:quotes[_nisdomain_] pass:quotes[_nis-server.example.com_]
....
+
[IMPORTANT]
====

This script uses hard-coded values for first name, last name, and sets the password to [option]`passw0rd1`. The user must change the temporary password at the next login.

====
