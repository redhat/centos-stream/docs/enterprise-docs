:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="adding-a-user-group-using-idm-cli_{context}"]
= Adding a user group using IdM CLI

[role="_abstract"]
Follow this procedure to add a user group using the IdM CLI.

.Prerequisites

* You must be logged in as the administrator. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]


.Procedure

* Add a user group by using the [command]`ipa group-add pass:quotes[_group_name_]` command. For example, to create group_a:
+
....
$ ipa group-add group_a
---------------------
Added group "group_a"
---------------------
  Group name: group_a
  GID: 1133400009
....
+
--
By default, [command]`ipa group-add` adds a POSIX user group. To specify a different group type, add options to [command]`ipa group-add`:

** [option]`--nonposix` to create a non-POSIX group

** [option]`--external` to create an external group
+
For details on group types, see xref:the-different-group-types-in-idm_{context}[The different group types in IdM].
--
+
You can specify a custom GID when adding a user group by using the [option]`--gid=pass:quotes[_custom_GID_]` option. If you do this, be careful to avoid ID conflicts. If you do not specify a custom GID, IdM automatically assigns a GID from the available ID range.
