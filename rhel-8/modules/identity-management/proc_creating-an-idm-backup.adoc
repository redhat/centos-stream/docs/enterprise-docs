:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies:
//
// assembly_backing-up-and-restoring-idm.adoc
// assembly_preparing-for-data-loss-with-idm-backups.adoc

[id="proc_creating-an-idm-backup_{context}"]
= Creating an IdM backup

[role="_abstract"]
Create a full-server and data-only backup in offline and online modes using the `ipa-backup` command.

.Prerequisites

* You must have `root` privileges to run the `ipa-backup` utility.

.Procedure

* To create a full-server backup in offline mode, use the `ipa-backup` utility without additional options.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ipa-backup*
Preparing backup on server.example.com
Stopping IPA services
Backing up ipaca in EXAMPLE-COM to LDIF
Backing up userRoot in EXAMPLE-COM to LDIF
Backing up EXAMPLE-COM
Backing up files
Starting IPA service
*Backed up to /var/lib/ipa/backup/ipa-full-2020-01-14-11-26-06*
*The ipa-backup command was successful*
....

* To create an offline data-only backup, specify the `--data` option.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ipa-backup --data*
....

* To create a full-server backup that includes IdM log files, use the `--logs` option.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ipa-backup --logs*
....

* To create a data-only backup while IdM services are running, specify both `--data` and `--online` options.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ipa-backup --data --online*
....

[NOTE]
====
If the backup fails due to insufficient space in the `/tmp` directory, use the `TMPDIR` environment variable to change the destination for temporary files created by the backup process:

[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *TMPDIR=/new/location ipa-backup*
....
====


.Verification
* Ensure the backup directory contains an archive with the backup.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@server ~]# *ls _/var/lib/ipa/backup/ipa-full-2020-01-14-11-26-06_*
header  *ipa-full.tar*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/3344471[ipa-backup command fails to finish] (Red Hat Knowledgebase)
