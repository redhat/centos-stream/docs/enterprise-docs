:_mod-docs-content-type: PROCEDURE

[id="uploading-ssh-keys-using-the-idm-cli_{context}"]
= Uploading SSH keys for a user using the IdM CLI

Identity Management allows you to upload a public SSH key to a user entry. The user who has access to the corresponding private SSH key can use SSH to log into an IdM machine without using Kerberos credentials.

.Prerequisites

* Administrator privileges for managing the IdM CLI or User Administrator role.


.Procedure

. Run the `ipa user-mod` command with the `--sshpubkey` option to upload the base64-encoded public key to the user entry.
+
[literal]
--
$ ipa user-mod user --sshpubkey="ssh-rsa AAAAB3Nza...SNc5dv== client.example.com"
--
+
Note in this example you upload the key type, the key, and the hostname identifier to the user entry.

. To upload multiple keys, use `--sshpubkey` multiple times. For example, to upload two SSH keys:
+
[literal]
--
--sshpubkey="AAAAB3Nza...SNc5dv==" --sshpubkey="RjlzYQo...ZEt0TAo="
--

. To use command redirection and point to a file that contains the key instead of pasting the key string manually, use the following command:
+
[literal]
--
ipa user-mod user --sshpubkey="$(cat ~/.ssh/id_rsa.pub)" --sshpubkey="$(cat ~/.ssh/id_rsa2.pub)"
--


.Verification

* Run the `ipa user-show` command to verify that the SSH public key is associated with the specified user:
+
[literal]
--
$ ipa user-show user
User login: user
  First name: user
  Last name: user
  Home directory: /home/user
  Login shell: /bin/sh
  Principal name: user@IPA.TEST
  Principal alias: user@IPA.TEST
  Email address: user@ipa.test
  UID: 1118800019
  GID: 1118800019
  SSH public key fingerprint: SHA256:qGaqTZM60YPFTngFX0PtNPCKbIuudwf1D2LqmDeOcuA
                              user@IPA.TEST (ssh-rsa)
  Account disabled: False
  Password: False
  Member of groups: ipausers
  Subordinate ids: 3167b7cc-8497-4ff2-ab4b-6fcb3cb1b047
  Kerberos keys available: False
--
