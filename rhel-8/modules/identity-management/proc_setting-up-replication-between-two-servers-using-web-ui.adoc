:_mod-docs-content-type: PROCEDURE
:experimental:
[id="proc_setting-up-replication-between-two-servers-using-web-ui_{context}"]
[[managing-topology-ui-set-up]]
= Setting up replication between two servers using the Web UI

[role="_abstract"]
Using the {IPA} (IdM) Web UI, you can choose two servers and create a new replication agreement between them.

.Prerequisites
* You are logged in as an IdM administrator.

.Procedure
. In the topology graph, hover your mouse over one of the server nodes.
+
.Domain or CA options
+
image::mng_top_domain_ca.png[]

. Click on the `domain` or the `ca` part of the circle depending on what type of topology segment you want to create.

. A new arrow representing the new replication agreement appears under your mouse pointer. Move your mouse to the other server node, and click on it.
+
.Creating a new segment
+
image::mng_top_drag.png[]

. In the `Add topology segment` window, click btn:[Add] to confirm the properties of the new segment.

The new topology segment between the two servers joins them in a replication agreement. The topology graph now shows the updated replication topology:

.New segment created

image::mng_top_three.png[]
