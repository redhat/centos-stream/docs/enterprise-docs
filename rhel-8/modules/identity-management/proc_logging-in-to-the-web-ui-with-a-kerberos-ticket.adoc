:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//


[id='logging-in-to-the-web-ui-with-a-kerberos-ticket-{context}']
= Logging in to the web UI using a Kerberos ticket

[role="_abstract"]
Follow this procedure to log in to the IdM Web UI using a Kerberos ticket-granting ticket (TGT).

The TGT expires at a predefined time. The default time interval is 24 hours and you can change it in the IdM Web UI.

After the time interval expires, you need to renew the ticket:

* Using the kinit command.

* Using IdM login credentials in the Web UI login dialog.


.Procedure

* Open the IdM Web UI.
+
If Kerberos authentication works correctly and you have a valid ticket, you will be automatically authenticated and the Web UI opens.
+
If the ticket is expired, it is necessary to authenticate yourself with credentials first. However, next time the IdM Web UI will open automatically without opening the login dialog.
+
If you see an error message `Authentication with Kerberos failed`, verify that your browser is configured for Kerberos authentication. See xref:configuring-the-browser-for-kerberos-authentication-login-web-ui-krb[Configuring the browser for Kerberos authentication].
+
image::firefox_kerb_auth_failed.png[A screenshot of the IdM Web UI log in screen displaying an error above the empty Username and Password fields. The error message says "Authentication with Kerberos failed."]
