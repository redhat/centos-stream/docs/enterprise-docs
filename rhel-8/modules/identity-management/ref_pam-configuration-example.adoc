:_newdoc-version: 2.15.0
:_template-generated: 2024-8-14

:_mod-docs-content-type: REFERENCE

[id="pam-configuration-example_{context}"]
= PAM configuration example

[role="_abstract"]
See an example of PAM configuration file with the detailed description.

.Annotated PAM configuration example
[literal,subs="+quotes,attributes,verbatim"]
----
#%PAM-1.0
auth		required	pam_securetty.so <1>
auth		required	pam_unix.so nullok <2>
auth		required	pam_nologin.so <3>
account		required	pam_unix.so <4>
password	required	pam_pwquality.so retry=3 <5>
password	required	pam_unix.so shadow nullok use_authtok <6>
session		required	pam_unix.so <7>
----
<1> This line ensures that the root login is allowed only from a terminal which is listed in the `/etc/securetty` file, if this file exists. If the terminal is not listed in the file, the login as root fails with a `Login incorrect` message.
<2> Prompts the user for a password and checks it against the information stored in `/etc/passwd` and, if it exists, `/etc/shadow`. The `nullok` argument allows a blank password.
<3> Checks if `/etc/nologin` file exists. If it exists, and the user is not root, authentication fails.
+
[NOTE]
====
In this example, all three `auth` modules are checked, even if the first `auth` module fails. This is a good security approach that prevents potential attacker from knowing at what stage their authentication failed.
====

<4> Verifies the user's account. For example, if shadow passwords have been enabled, the account type of the `pam_unix.so` module checks for expired accounts or if the user needs to change the password.
<5> Prompts for a new password if the current one has expired or when the user manually requests a password change. Then it checks the strength of the newly created password to ensure it meets quality requirements and is not easily determined by a dictionary-based password cracking program. The argument `retry=3` specifies that user has three attempts to create a strong password.
<6> The `pam_unix.so` module manages password changes. The `shadow` argument instructs the module to create shadow passwords when updating a user's password. The `nullok` argument allows the user to change their password from a blank password, otherwise a null password is treated as an account lock. The `use_authtok` argument accepts any password that was entered previously without prompting the user again. In this way, all new passwords must pass the `pam_pwquality.so` check for secure passwords before being accepted.
<7> The `pam_unix.so` module manages the session and logs the user name and service type at the beginning and end of each session. Logs are collected by the `systemd-journald` service and can be viewed by using the `journalctl` command. Logs are also stored in `/var/log/secure`. This module can be supplemented by stacking it with other session modules for additional functionality.
