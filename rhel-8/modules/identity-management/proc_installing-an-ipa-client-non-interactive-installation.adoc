:_mod-docs-content-type: PROCEDURE
[id="installing-ipa-client-non-interactive-install_{context}"]
= Installing a client: Non-interactive installation

[role="_abstract"]
For a non-interactive installation, you must provide all required information to the `ipa-client-install` utility using command-line options. The following sections describe the minimum required options for a non-interactive installation.

Options for the intended authentication method for client enrollment:: The available options are:
+
--
* [option]`--principal` and [option]`--password` to specify the credentials of a user authorized to enroll clients
* [option]`--random` to specify a one-time random password generated for the client
* [option]`--keytab` to specify the keytab from a previous enrollment
--

The option for unattended installation:: The [option]`--unattended` option lets the installation run without requiring user confirmation.
+
If the SRV records are set properly in the IdM DNS zone, the script automatically discovers all the other required values. If the script cannot discover the values automatically, provide them using command-line options, such as:

* [option]`--hostname` to specify a static fully qualified domain name (FQDN) for the client machine.
+
[IMPORTANT]
====

The FQDN must be a valid DNS name:

* Only numbers, alphabetic characters, and hyphens (-) are allowed. For example, underscores are not allowed and can cause DNS failures.
* The host name must be all lower-case. No capital letters are allowed.

//For other recommended naming practices, see the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/sec-Securing_DNS_Traffic_with_DNSSEC.html#sec-Recommended_Naming_Practices++[{RHEL} Security Guide].

====
* [option]`--domain` to specify the primary DNS domain of an existing IdM deployment, such as `example.com`. The name is a lowercase version of the IdM Kerberos realm name.
+
* [option]`--server` to specify the FQDN of the IdM server to connect to. When this option is used, DNS autodiscovery for Kerberos is disabled and a fixed list of KDC and Admin servers is configured. Under normal circumstances, this option is not needed as the list of servers is retrieved from the primary IdM DNS domain.
+
* [option]`--realm` to specify the Kerberos realm of an existing IdM deployment. Usually it is an uppercase version of the primary DNS domain used by the IdM installation. Under normal circumstances, this option is not needed as the realm name is retrieved from the IdM server.

An example of a basic *ipa-client-install* command for non-interactive installation:

[literal,subs="+quotes,attributes"]
----
# *ipa-client-install --password '_W5YpARl=7M.n_' --mkhomedir --unattended*
----
{nbsp} +

An example of an *ipa-client-install* command for non-interactive installation with more options specified:

[literal,subs="+quotes,attributes"]
----
# *ipa-client-install --password '_W5YpARl=7M.n_' --domain _{domain-name}_ --server _{server-host-name}_ --realm _{realm-name}_ --mkhomedir --unattended*
----

[role="_additional-resources"]
.Additional resources
* For a complete list of options accepted by [command]`ipa-client-install`, see the *ipa-client-install*(1) man page.
