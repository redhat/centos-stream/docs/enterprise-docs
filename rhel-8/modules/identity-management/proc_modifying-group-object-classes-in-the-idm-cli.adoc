:_newdoc-version: 2.18.3
:_template-generated: 2024-11-01
:_mod-docs-content-type: PROCEDURE

[id="modifying-group-object-classes-in-the-idm-cli_{context}"]
= Modifying group object classes in the IdM CLI


{IPA} (IdM) has the following default group object classes:

* top
* groupofnames
* nestedgroup
* ipausergroup
* ipaobject

This procedure describes how you can use the IdM Web UI to add additional group object classes for future {IPA} (IdM) user group entries. As a result, these entries will have different attributes than the current the group entries do. 

.Prerequisites
* You have enabled the `brace expansion` feature:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *set -o braceexpand*
....
* You are logged in as the IdM administrator. 

.Procedure
* Use the `ipa config-mod` command to modify the current schema. For example, to add `ipasshuser` and `employee` group object classes to the future user entries:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[bjensen@server ~]$ *ipa config-mod --groupobjectclasses={top,groupofnames,nestedgroup,ipausergroup,ipaobject,ipasshuser,employeegroup}*
....

+
The command adds all the default group object classes as well as the two new ones, `ipasshuser` and `employeegroup`.

+
IMPORTANT: If any group object classes required by IdM are not included, then subsequent attempts to add a group entry will fail with object class violations. 

+
NOTE: Instead of the comma-separated list inside curly braces with no spaces allowed that is used in the example above, you can use the `--groupobjectclasses` argument repeatedly.
