:_mod-docs-content-type: PROCEDURE
[id="verifying-the-trust-configuration-on-ad_{context}"]
= Verifying the trust configuration on AD

[role="_abstract"]
After configuring the trust, verify that:

* The {IPA} (IdM)-hosted services are resolvable from the {AD} (AD) server.
* AD services are resolvable from the AD server.

.Prerequisites

* You need to be logged in with administrator privileges.

.Procedure

. On the AD server, set the `nslookup.exe` utility to look up service records.
+
[literal]
--
C:\>nslookup.exe
> set type=SRV
--

. Enter the domain name for the Kerberos over UDP and LDAP over TCP service records.
+
[literal]
--
> _kerberos._udp.idm.example.com.
_kerberos._udp.idm.example.com.       SRV service location:
    priority                = 0
    weight                  = 100
    port                    = 88
    svr hostname   = server.idm.example.com
> _ldap._tcp.idm.example.com
_ldap._tcp.idm.example.com       SRV service location:
    priority                = 0
    weight                  = 100
    port                    = 389
    svr hostname   = server.idm.example.com
--

. Change the service type to TXT and run a DNS query for the TXT record with the IdM Kerberos realm name.
+
[literal]
--
C:\>nslookup.exe
> set type=TXT
> _kerberos.idm.example.com.
_kerberos.idm.example.com.        text =

    "IDM.EXAMPLE.COM"
--

. Run a DNS query for the MS DC Kerberos over UDP and LDAP over TCP service records.
+
[literal]
--
C:\>nslookup.exe
> set type=SRV
> _kerberos._udp.dc._msdcs.idm.example.com.
_kerberos._udp.dc._msdcs.idm.example.com.        SRV service location:
    priority = 0
    weight = 100
    port = 88
    svr hostname = server.idm.example.com
> _ldap._tcp.dc._msdcs.idm.example.com.
_ldap._tcp.dc._msdcs.idm.example.com.        SRV service location:
    priority = 0
    weight = 100
    port = 389
    svr hostname = server.idm.example.com
--
+
Active Directory only expects to discover domain controllers that can respond to AD-specific protocol requests, such as other AD domain controllers and IdM trust controllers. Use the `ipa-adtrust-install` tool to promote an IdM server to a trust controller, and you can verify which servers are trust controllers with the `ipa server-role-find --role 'AD trust controller'` command.

. Verify that AD services are resolvable from the AD server.
+
[literal]
--
C:\>nslookup.exe
> set type=SRV
--

. Enter the domain name for the Kerberos over UDP and LDAP over TCP service records.
+
[literal]
--
> _kerberos._udp.dc._msdcs.ad.example.com.
_kerberos._udp.dc._msdcs.ad.example.com. 	SRV service location:
    priority = 0
    weight = 100
    port = 88
    svr hostname = addc1.ad.example.com
> _ldap._tcp.dc._msdcs.ad.example.com.
_ldap._tcp.dc._msdcs.ad.example.com. 	SRV service location:
    priority = 0
    weight = 100
    port = 389
    svr hostname = addc1.ad.example.com
--
