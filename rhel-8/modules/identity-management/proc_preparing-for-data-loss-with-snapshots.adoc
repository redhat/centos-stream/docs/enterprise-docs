:_mod-docs-content-type: PROCEDURE
// Module included in the following titles/assemblies:
//
// title /planning/preparing-for-disaster-recovery-with-identity-management


[id="preparing-for-data-loss-with-snapshots_{context}"]
= Preparing for data loss with VM snapshots

[role="_abstract"]
Virtual machine (VM) snapshots are an integral component of a data recovery strategy, since they preserve the full state of an IdM server:

* Operating system software and settings
* IdM software and settings
* IdM customer data

Preparing a VM snapshot of an IdM Certificate Authority (CA) replica allows you to rebuild an entire IdM deployment after a disaster.


[WARNING]
====
If your environment uses the integrated CA, a snapshot of a replica _without a CA_ will not be sufficient for rebuilding a deployment, because certificate data will not be preserved.

Similarly, if your environment uses the IdM Key Recovery Authority (KRA), make sure you create snapshots of a KRA replica, or you might lose the storage key.

Red Hat recommends creating snapshots of a VM that has all of the IdM server roles installed which are in use in your deployment: CA, KRA, DNS.
====


.Prerequisites

* A hypervisor capable of hosting RHEL VMs.


.Procedure

. Configure at least one *CA replica* in the deployment to run inside a VM.

.. If IdM DNS or KRA are used in your environment, consider installing DNS and KRA services on this replica as well.

.. Optional: Configure this VM replica as a link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-ipa-replica_installing-identity-management#installing-an-idm-hidden-replica_install-replica[hidden replica].

. Periodically shutdown this VM, take a full snapshot of it, and bring it back online so it continues to receive replication updates. If the VM is a hidden replica, IdM Clients will not be disrupted during this procedure.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/certified-hypervisors[Which hypervisors are certified to run Red Hat Enterprise Linux?]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/planning_identity_management/planning-the-replica-topology_planning-identity-management#the-hidden-replica-mode_planning-the-replica-topology[The hidden replica mode]
