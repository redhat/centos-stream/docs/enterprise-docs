:_mod-docs-content-type: PROCEDURE

[id="setting-the-parameters-of-the-inventory-file-when-autodiscovery-is-not-possible-during-client-installation_{context}"]

= Setting the parameters of the inventory file when autodiscovery is not possible during client installation

To install an Identity Management client using an Ansible playbook, configure the target host parameters in an inventory file, for example `inventory/hosts`:

//* the <<info-about-host-ansible_{context},
* The information about the host, the IdM server and the IdM domain or the IdM realm
//* the <<info-about-authorization-ansible_{context},
* The authorization for the task

The inventory file can be in one of many formats, depending on the inventory plugins you have. The `INI-like` format is one of Ansible’s defaults and is used in the examples below.

[NOTE]
====
To use smart cards with the graphical user interface in RHEL, ensure that you include the `ipaclient_mkhomedir` variable in your Ansible playbook.
====

.Procedure

. Specify the fully-qualified hostname (FQDN) of the host to become an IdM client. The fully qualified domain name must be a valid DNS name:

+
--
** Only numbers, alphabetic characters, and hyphens (`-`) are allowed. For example, underscores are not allowed and can cause DNS failures.
** The host name must be all lower-case. No capital letters are allowed.
--

. Specify other options in the relevant sections of the `inventory/hosts` file:

** The FQDN of the servers in the `[ipaservers]` section to indicate which IdM server the client will be enrolled with
** One of the two following options:

*** The `ipaclient_domain` option in the `[ipaclients:vars]` section to indicate the DNS domain name of the IdM server the client will be enrolled with
*** The `ipaclient_realm` option in the `[ipaclients:vars]` section to indicate the name of the Kerberos realm controlled by the IdM server
+
.Example of an inventory hosts file with the client FQDN, the server FQDN and the domain defined
[subs="+quotes,attributes"]
----
[ipaclients]
*client.idm.example.com*

[ipaservers]
*server.idm.example.com*

[ipaclients:vars]
*ipaclient_domain=idm.example.com*
[...]
----


. Specify the credentials for enrolling the client. The following authentication methods are available:

** The *password of a user authorized to enroll clients*. This is the default option.

*** Use the Ansible Vault to store the password, and reference the Vault file from the playbook file, for example `install-client.yml`, directly:
+
.Example playbook file using principal from inventory file and password from an Ansible Vault file
[subs="+quotes,attributes"]
----
- name: Playbook to configure IPA clients with username/password
  hosts: ipaclients
  become: true
  vars_files:
  - *playbook_sensitive_data.yml*

  roles:
  - role: ipaclient
    state: present
----

** Less securely, the credentials of `admin` to be provided using the `ipaadmin_password` option in the `[ipaclients:vars]` section of the `inventory/hosts` file. Alternatively, to specify a different authorized user, use the `ipaadmin_principal` option for the user name, and the `ipaadmin_password` option for the password. The `install-client.yml` playbook file can then look as follows:
+
.Example inventory hosts file
[subs="+quotes,attributes"]
----
[...]
[ipaclients:vars]
*ipaadmin_principal=my_admin*
*ipaadmin_password=Secret123*
----
+
.Example Playbook using principal and password from inventory file
[subs="+quotes,attributes"]
----
- name: Playbook to unconfigure IPA clients
  hosts: ipaclients
  become: true

  roles:
  - role: ipaclient
    state: true
----

** The *client keytab* from the previous enrollment if it is still available:
+
This option is available if the system was previously enrolled as an Identity Management client. To use this authentication method, uncomment the `ipaclient_keytab` option, specifying the path to the file storing the keytab, for example in the `[ipaclient:vars]` section of `inventory/hosts`.

** A *random, one-time password* (OTP) to be generated during the enrollment. To use this authentication method, use the `ipaclient_use_otp=true` option in your inventory file. For example, you can uncomment the `#ipaclient_use_otp=true` option in the `[ipaclients:vars]` section of the `inventory/hosts` file. Note that with OTP you must also specify one of the following options:

*** The *password of a user authorized to enroll clients*, for example by providing a value for `ipaadmin_password` in the `[ipaclients:vars]` section of the `inventory/hosts` file.
*** The *admin keytab*, for example by providing a value for `ipaadmin_keytab` in the `[ipaclients:vars]` section of `inventory/hosts`.

+
ifeval::[{ProductNumber} == 8]
. Starting with RHEL 8.9, you can also specify the `ipaclient_subid: true` option to have subid ranges configured for IdM users on the IdM level.
endif::[]
ifeval::[{ProductNumber} == 9]
. Starting with RHEL 9.3, you can also specify the `ipaclient_subid: true` option to have subid ranges configured for IdM users on the IdM level.
endif::[]

[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/ipaclient/README.md`
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_and_managing_identity_management/index#assembly_managing-subid-ranges-manually_configuring-and-managing-idm[Managing subID ranges manually]
