:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// introduction-to-the-ipa-command-line-utilities.adoc

[id="how-to-use-special-characters-with-the-ipa-utilities_{context}"]
= How to use special characters with the IdM utilities

[role="_abstract"]
When passing command-line arguments that include special characters to the `ipa` commands, escape these characters with a backslash (\).
For example, common special characters include angle brackets (&lt;{nbsp}and{nbsp}&gt;), ampersand (&), asterisk ({asterisk}), or vertical bar (|).

For example, to escape an asterisk ({asterisk}):

[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa certprofile-show certificate_profile --out=_exported\*profile.cfg_*
....

Commands containing unescaped special characters do not work as expected because the shell cannot properly parse such characters.
