[id="who-can-change-idm-user-passwords-and-how_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Who can change IdM user passwords and how


Regular users without the permission to change other users' passwords can change only their own personal password. The new password must meet the IdM password policies applicable to the groups of which the user is a member. For details on configuring password policies, see xref:defining-idm-password-policies_{ProjectNameID}[Defining IdM password policies].

Administrators and users with password change rights can set initial passwords for new users and reset passwords for existing users. These passwords:

* Do not have to meet the IdM password policies.

* Expire after the first successful login. When this happens, IdM prompts the user to change the expired password immediately. To disable this behavior, see xref:enabling-password-reset-in-idm-without-prompting-the-user-for-a-password-change-at-the-next-login_managing-user-passwords-in-idm[Enabling password reset in IdM without prompting the user for a password change at the next login].

[NOTE]
====

The LDAP Directory Manager (DM) user can change user passwords using LDAP tools. The new password can override any IdM password policies. Passwords set by DM do not expire after the first login.

====
