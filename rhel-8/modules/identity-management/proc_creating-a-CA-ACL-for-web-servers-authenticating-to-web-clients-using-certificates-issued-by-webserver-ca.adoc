:_mod-docs-content-type: PROCEDURE
[id="creating-a-CA-ACL-for-web-servers-authenticating-to-web-clients-using-certificates-issued-by-webserver-ca_{context}"]
= Creating a CA ACL for web servers authenticating to web clients using certificates issued by webserver-ca

[role="_abstract"]
Follow this procedure to create a CA ACL that requires the system administrator to use the *webserver-ca* sub-CA and the *caIPAserviceCert* profile when requesting a certificate for the *HTTP/my_company.idm.example.com@IDM.EXAMPLE.COM* service. If the user requests a certificate from a different sub-CA or of a different profile, the request fails. The only exception is when there is another matching CA ACL that is enabled. To view the available CA ACLs, see xref:viewing-CA-ACLs_in_IdM_CLI_{context}[Viewing CA ACLs in IdM CLI].

.Prerequisites

* Make sure that the *HTTP/my_company.idm.example.com@IDM.EXAMPLE.COM* service is part of IdM.
* Make sure you have obtained IdM administrator's credentials.

.Procedure

. Create a CA ACL using the `ipa caacl` command, and specify its name:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa caacl-add TLS_web_server_authentication*
--------------------------------------------
Added CA ACL "TLS_web_server_authentication"
--------------------------------------------
  ACL name: TLS_web_server_authentication
  Enabled: TRUE
....

. Modify the CA ACL using the `ipa caacl-mod` command to specify the description of the CA ACL:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa caacl-mod TLS_web_server_authentication --desc="CAACL for web servers authenticating to web clients using certificates issued by webserver-ca"*
-----------------------------------------------
Modified CA ACL "TLS_web_server_authentication"
-----------------------------------------------
  ACL name: TLS_web_server_authentication
  Description: CAACL for web servers authenticating to web clients using certificates issued by webserver-ca
  Enabled: TRUE
....


. Add the *webserver-ca* sub-CA to the CA ACL:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa caacl-add-ca TLS_web_server_authentication --ca=webserver-ca*
  ACL name: TLS_web_server_authentication
  Description: CAACL for web servers authenticating to web clients using certificates issued by webserver-ca
  Enabled: TRUE
  CAs: webserver-ca
-------------------------
Number of members added 1
-------------------------
....

. Use the `ipa caacl-add-service` to specify the service whose principal will be able to request a certificate:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa caacl-add-service TLS_web_server_authentication --service=HTTP/my_company.idm.example.com@IDM.EXAMPLE.COM*
  ACL name: TLS_web_server_authentication
  Description: CAACL for web servers authenticating to web clients using certificates issued by webserver-ca
  Enabled: TRUE
  CAs: webserver-ca
  Services: HTTP/my_company.idm.example.com@IDM.EXAMPLE.COM
-------------------------
Number of members added 1
-------------------------
....

. Use the `ipa caacl-add-profile` command to specify the certificate profile for the requested certificate:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa caacl-add-profile TLS_web_server_authentication --certprofiles=caIPAserviceCert*
  ACL name: TLS_web_server_authentication
  Description: CAACL for web servers authenticating to web clients using certificates issued by webserver-ca
  Enabled: TRUE
  CAs: webserver-ca
  Profiles: caIPAserviceCert
  Services: HTTP/my_company.idm.example.com@IDM.EXAMPLE.COM
-------------------------
Number of members added 1
-------------------------
....

+
You can use the newly-created CA ACL straight away. It is enabled after its creation by default.

[NOTE]
====
The point of CA ACLs is to specify which CA and profile combinations are allowed for requests coming from particular principals or groups. CA ACLs do not affect certificate validation or trust. They do not affect how the issued certificates will be used.
====
