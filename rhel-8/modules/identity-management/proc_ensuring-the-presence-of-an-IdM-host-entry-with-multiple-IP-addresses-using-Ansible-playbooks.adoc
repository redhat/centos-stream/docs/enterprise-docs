:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-an-IdM-host-entry-with-multiple-IP-addresses-using-Ansible-playbooks_{context}"]
= Ensuring the presence of an IdM host entry with multiple IP addresses using Ansible playbooks

[role="_abstract"]
Follow this procedure to ensure the presence of a host entry in {IPA} (IdM) using Ansible playbooks. The host entry is defined by its `fully-qualified domain name` (FQDN) and its multiple IP addresses.

[NOTE]
====
In contrast to the `ipa host` utility, the Ansible `ipahost` module can ensure the presence or absence of several IPv4 and IPv6 addresses for a host. The `ipa host-mod` command cannot handle IP addresses.
====

.Prerequisites

* You know the IdM administrator password.

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

.Procedure

. Create an inventory file, for example `inventory.file`, and define `ipaserver` in it:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....

. Create an Ansible playbook file. Specify, as the `name` of the `ipahost` variable, the `fully-qualified domain name` (FQDN) of the host whose presence in IdM you want to ensure. Specify each of the multiple IPv4 and IPv6 `ip_address` values on a separate line by using the *_ip_address_* syntax. To simplify this step, you can copy and modify the example in the `/usr/share/doc/ansible-freeipa/playbooks/host/host-member-ipaddresses-present.yml` file. You can also include additional information:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Host member IP addresses present
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure host101.example.com IP addresses present
    ipahost:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: host01.idm.example.com
      ip_address:
      - 192.168.0.123
      - fe80::20c:29ff:fe02:a1b3
      - 192.168.0.124
      - fe80::20c:29ff:fe02:a1b4
      force: true
....


+
. Run the playbook:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i _path_to_inventory_directory/inventory.file_ _path_to_playbooks_directory/ensure-host-with-multiple-IP-addreses-is-present.yml_*
....

[NOTE]
====
The procedure creates a host entry in the IdM LDAP server but does not enroll the host into the IdM Kerberos realm. For that, you must deploy the host as an IdM client. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/installing_identity_management/installing-an-identity-management-client-using-an-ansible-playbook_installing-identity-management[Installing an Identity Management client using an Ansible playbook].
====


.Verification

. Log in to your IdM server as admin:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ssh admin@server.idm.example.com*
Password:
....



. Enter the `ipa host-show` command and specify the name of the host:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa host-show host01.idm.example.com*
  Principal name: host/host01.idm.example.com@IDM.EXAMPLE.COM
  Principal alias: host/host01.idm.example.com@IDM.EXAMPLE.COM
  Password: False
  Keytab: False
  Managed by: host01.idm.example.com
....

+
The output confirms that *host01.idm.example.com* exists in IdM.



. To verify that the multiple IP addresses of the host exist in the IdM DNS records, enter the `ipa dnsrecord-show` command and specify the following information:

* The name of the IdM domain
* The name of the host
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa dnsrecord-show idm.example.com host01*
[...]
  Record name: host01
  A record: 192.168.0.123, 192.168.0.124
  AAAA record: fe80::20c:29ff:fe02:a1b3, fe80::20c:29ff:fe02:a1b4
....

+
The output confirms that all the IPv4 and IPv6 addresses specified in the playbook are correctly associated with the *host01.idm.example.com* host entry.
