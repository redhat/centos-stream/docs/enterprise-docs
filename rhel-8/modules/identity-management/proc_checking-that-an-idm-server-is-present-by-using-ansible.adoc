:_mod-docs-content-type: PROCEDURE
[id="checking-that-an-idm-server-is-present-by-using-ansible_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Checking that an IdM server is present by using Ansible

[role="_abstract"]
You can use the `ipaserver` `ansible-freeipa` module in an Ansible playbook to verify that an {IPA} (IdM) server exists.

NOTE: The `ipaserver` Ansible module does not install the IdM server.

.Prerequisites

* On the control node:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
** The `SSH` connection from the control node to the IdM server defined in the inventory file is working correctly.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Copy the `server-present.yml` Ansible playbook file located in the `/usr/share/doc/ansible-freeipa/playbooks/server/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/server/server-present.yml server-present-copy.yml*
....

. Open the `server-present-copy.yml` file for editing.

. Adapt the file by setting the following variables in the `ipaserver` task section and save the file:

* Set the `ipaadmin_password` variable to the password of the IdM `admin`.
* Set the `name` variable to the `FQDN` of the server. The `FQDN` of the example server is *server123.idm.example.com*.

+
--

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Server present example
  hosts: ipaserver
  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure server server123.idm.example.com is present
    ipaserver:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: server123.idm.example.com*
....
--

. Run the Ansible playbook and specify the playbook file and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory server-present-copy.yml*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management[Installing an Identity Management server using an Ansible playbook]
* The `README-server.md` file in the `/usr/share/doc/ansible-freeipa/` directory
* The sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/server` directory
