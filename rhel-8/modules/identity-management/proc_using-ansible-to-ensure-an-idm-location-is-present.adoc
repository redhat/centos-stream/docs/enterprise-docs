:_mod-docs-content-type: PROCEDURE
[id="using-ansible-to-ensure-an-idm-location-is-present_{context}"]
= Using Ansible to ensure an IdM location is present

[role="_abstract"]
As a system administrator of {IPA} (IdM), you can configure IdM DNS locations to allow clients to locate authentication servers within the closest network infrastructure.

The following procedure describes how to use an Ansible playbook to ensure a DNS location is present in IdM. The example describes how to ensure that the *germany* DNS location is present in IdM. As a result, you can assign particular IdM servers to this location so that local IdM clients can use them to reduce server response time.

.Prerequisites

* On the control node:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* You understand the xref:deployment-considerations-for-dns-locations_{context}[deployment considerations for DNS locations].

.Procedure

. Navigate to the *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Make a copy of the `location-present.yml` file located in the `/usr/share/doc/ansible-freeipa/playbooks/location/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/location/location-present.yml location-present-copy.yml*
....

. Open the `location-present-copy.yml` Ansible playbook file for editing.

. Adapt the file by setting the following variables in the `ipalocation` task section:

* Adapt the `name` of the task to correspond to your use case.
* Set the `ipaadmin_password` variable to the password of the IdM administrator.
* Set the `name` variable to the name of the location.


+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: location present example
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - *name: Ensure that the "germany" location is present*
    ipalocation:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: germany*
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory location-present-copy.yml*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/working_with_dns_in_identity_management/managing-dns-locations-in-idm_working-with-dns-in-identity-management#assigning-an-idm-server-to-a-dns-location-using-the-idm-web-ui_managing-dns-locations-in-idm[Assigning an IdM server to a DNS location using the IdM Web UI]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/working_with_dns_in_identity_management/managing-dns-locations-in-idm_working-with-dns-in-identity-management#assigning-an-idm-server-to-a-dns-location-using-the-idm-cli_managing-dns-locations-in-idm[Assigning an IdM server to a DNS location using the IdM CLI]
