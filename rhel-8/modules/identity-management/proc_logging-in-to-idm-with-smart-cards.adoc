:_mod-docs-content-type: PROCEDURE
[id="logging-in-to-idm-with-smart-cards_{context}"]
= Logging in to IdM with smart cards

[role="_abstract"]
Follow this procedure to use smart cards for logging in to the IdM Web UI.

.Prerequisites

* The web browser is configured for using smart card authentication.
* The IdM server is configured for smart card authentication.
* The certificate installed on your smart card is either issued by the IdM server or has been added to the user entry in IdM.
* You know the PIN required to unlock the smart card.
* The smart card has been inserted into the reader.


.Procedure

. Open the IdM Web UI in the browser.

. Click *Log In Using Certificate*.
+
image:ipa-login-smart-card.png[A screenshot of the IdM Web UI displaying an empty "Username" field and an empty "Password" field. Below those two fields the "Log in using a Certificate" option has been highlighted.]

. If the *Password Required* dialog box opens, add the PIN to unlock the smart card and click the *OK* button.
+
The *User Identification Request* dialog box opens.
+
If the smart card contains more than one certificate,
select the certificate you want to use for authentication in the drop down list below *Choose a certificate to present as identification*.

. Click the *OK* button.

Now you are successfully logged in to the IdM Web UI.

image:web_ui_users.png[A screenshot of the first screen visible after logging in to the IdM Web UI. There are 5 tabs listed along the top of the screen: Identity - Policy - Authentication - Network Services - IPA Server. The Identity tab has been selected and it is displaying the Users page which is the first menu item among 6 choices just below the tabs: Users - Hosts - Services - Groups - ID Views - Automember. The Active users page displays a table of user logins and their information: First name - Last name - Status - UID - Email address - Telephone number - Job Title.]
