:_mod-docs-content-type: PROCEDURE

:experimental:

[id="proc_importing-personal-certificate-in-thunderbird_{context}"]
= Importing personal certificate in Thunderbird

[role="_abstract"]
The following example shows how to import certificates for personal authentication in the Mozilla Thunderbird email client.


.Prerequisites
. You have a personal certificate stored on your device.

To use a personal certificate for authentication:

.Procedure
. Open `Certificate Manager`.
. Under the `Your Certificates` tab, click btn:[Import].
+
.Importing a personal certificate for authentication in Thunderbird
image::thunderbird-import-custom-cert.png[]

. Select the required certificate from your computer.

. Close the `Certificate Manager`.

. Open the main menu and select `Account Settings`.
+
.Selecting account settings from menu
image::thunderbird-account-settings.png[]

. Select `End-To-End Encryption` in the left panel under your account email address.
+
Selecting end-to-end encryption  section.
+
image::thunderbird-end-to-end.png[]

. Under `S/MIME` section click the first btn:[Select] button to choose your personal certificate to use for signing messages.

. Under `S/MIME` section click the second btn:[Select] button to choose your personal certificate to encrypt and decrypt messages.
+
Choosing certificate for signing and encryption/decryption.
+
image::thunderbird-select-personal-cert.png[]

[NOTE]
====
In case you forgot to import valid certificate, you can open `Certificate Manager` directly using the `Manage S/MIME certificates`.
====
