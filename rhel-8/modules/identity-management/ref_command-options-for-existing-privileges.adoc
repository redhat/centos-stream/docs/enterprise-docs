:_mod-docs-content-type: REFERENCE
[id="command-options-for-existing-privileges_{context}"]
= Command options for existing privileges

[role="_abstract"]
Use the following variants to modify existing privileges as needed:

* To modify existing privileges, use the [command]`ipa privilege-mod` command.
* To find existing privileges, use the [command]`ipa privilege-find` command.
* To view a specific privilege, use the [command]`ipa privilege-show` command.
* The [command]`ipa privilege-remove-permission` command removes one or more permissions from a privilege.
* The [command]`ipa privilege-del` command deletes a privilege completely.

[role="_additional-resources"]
.Additional resources
* See the `ipa` man page on your system.
* See the [command]`ipa help` command.
