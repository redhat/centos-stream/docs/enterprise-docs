:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-an-http-service-in-idm-using-an-ansible-playbook_{context}"]
= Ensuring the presence of an HTTP service in IdM using an Ansible playbook

[role="_abstract"]
Follow this procedure to ensure the presence of an HTTP server in IdM using an Ansible playbook.

.Prerequisites

* The system to host the HTTP service is an IdM client.
* You have the IdM administrator password.

.Procedure


. Create an inventory file, for example `inventory.file`:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *touch inventory.file*
....

. Open the `inventory.file` and define the IdM server that you want to configure in the `[ipaserver]` section. For example, to instruct Ansible to configure *server.idm.example.com*, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....

. Make a copy of the `/usr/share/doc/ansible-freeipa/playbooks/service/service-is-present.yml` Ansible playbook file. For example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/service/service-is-present.yml /usr/share/doc/ansible-freeipa/playbooks/service/service-is-present-copy.yml*
....


. Open the `/usr/share/doc/ansible-freeipa/playbooks/service/service-is-present-copy.yml` Ansible playbook file for editing:
+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to manage IPA service.
  hosts: ipaserver
  gather_facts: false

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  # Ensure service is present
  - ipaservice:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: HTTP/client.idm.example.com
....

. Adapt the file:

* Change the IdM administrator password defined by the `ipaadmin_password` variable.
* Change the name of your IdM client on which the HTTP service is running, as defined by the `name` variable of the `ipaservice` task.

. Save and exit the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i __path_to_inventory_directory__/inventory.file /usr/share/doc/ansible-freeipa/playbooks/service/service-is-present-copy.yml*
....


.Verification

. Log into the IdM Web UI as IdM administrator.

. Navigate to `Identity` -> `Services`.

If *HTTP/client.idm.example.com@IDM.EXAMPLE.COM* is listed in the *Services* list, the Ansible playbook has been successfully added to IdM.

[role="_additional-resources"]
.Additional resources
* To secure the communication between the HTTP server and browser clients, see
ifdef::c-m-idm[]
xref:proc_adding-tls-encryption-to-an-apache-http-server-configuration_restricting-an-application-to-trust-a-subset-of-certs[adding TLS encryption to an Apache HTTP Server].
endif::[]
ifndef::c-m-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/restricting-an-application-to-trust-only-a-subset-of-certificates_configuring-and-managing-idm#proc_adding-tls-encryption-to-an-apache-http-server-configuration_restricting-an-application-to-trust-a-subset-of-certs[adding TLS encryption to an Apache HTTP Server].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/restricting-an-application-to-trust-only-a-subset-of-certificates_managing-certificates-in-idm#proc_adding-tls-encryption-to-an-apache-http-server-configuration_restricting-an-application-to-trust-a-subset-of-certs[adding TLS encryption to an Apache HTTP Server].
endif::[]
endif::[]

* To request a certificate for the HTTP service, see the procedure described in
ifdef::c-m-idm[]
xref:obtain-service-cert-with-certmonger_certmonger-for-issuing-renewing-service-certs[Obtaining an IdM certificate for a service using certmonger].
endif::[]
ifndef::c-m-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/using-certmonger_configuring-and-managing-idm#obtain-service-cert-with-certmonger_certmonger-for-issuing-renewing-service-certs[Obtaining an IdM certificate for a service using certmonger].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/using-certmonger_managing-certificates-in-idm[Obtaining an IdM certificate for a service using certmonger].
endif::[]
endif::[]
