:_mod-docs-content-type: CONCEPT

[id="what-is-a-ca-access-control-list_{context}"]
= What is a CA access control list?

[role="_abstract"]
Certificate Authority access control list (CA ACL) rules define which profiles can be used to issue certificates to which principals. You can use CA ACLs to do this, for example:

* Determine which user, host, or service can be issued a certificate with a particular profile
* Determine which IdM certificate authority or sub-CA is permitted to issue the certificate

For example, using CA ACLs, you can restrict use of a profile intended for employees working from an office located in London only to users that are members of the London office-related IdM user group.

The `ipa caacl` utility for management of CA ACL rules allows privileged users to add, display, modify, or delete a specified CA ACL.

[role="_additional-resources"]
.Additional resources
* See `ipa help caacl`.
