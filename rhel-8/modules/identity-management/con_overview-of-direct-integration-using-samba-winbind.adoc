:_mod-docs-content-type: CONCEPT
[id="overview-of-direct-integration-using-samba-winbind_{context}"]

= Overview of direct integration using Samba Winbind

Samba Winbind emulates a Windows client on a Linux system and communicates with AD servers.

You can use the `realmd` service to configure Samba Winbind by:

* Configuring network authentication and domain membership in a standard way.
* Automatically discovering information about accessible domains and realms.
* Not requiring advanced configuration to join a domain or realm.

Note that:

* Direct integration with Winbind in a multi-forest AD setup requires bidirectional trusts.
* Remote forests must trust the local forest to ensure that the `idmap_ad` plug-in handles remote forest users correctly.

Samba's `winbindd` service provides an interface for the Name Service Switch (NSS) and enables domain users to authenticate to AD when logging into the local system.

Using `winbindd` provides the benefit that you can enhance the configuration to share directories and printers without installing additional software.

[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 9]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/configuring_and_using_network_file_services/assembly_using-samba-as-a-server_configuring-and-using-network-file-services[Using Samba as a server]
endif::[]
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/assembly_using-samba-as-a-server_deploying-different-types-of-servers[Using Samba as a server]
endif::[]
* `realmd` man page on your system
* `winbindd` man page on your system
