:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_adjusting-the-performance-of-the-kdc.adoc

:experimental:

[id="adjusting-the-number-of-krb5kdc-processes_{context}"]
= Adjusting the number of `krb5kdc` processes

[role="_abstract"]
Follow this procedure to manually adjust the number of processes that the Key Distribution Center (KDC) starts to handle incoming connections.

By default, the IdM installer detects the number of CPU cores and enters the value in the [filename]`/etc/sysconfig/krb5kdc` file. For example, the file might contain the following entry:

[literal,subs="+quotes,attributes,verbatim"]
....
KRB5KDC_ARGS='-w 2'
[...]
....

In this example, with the `KRB5KDC_ARGS` parameter set to `-w 2`, the KDC starts two separate processes to handle incoming connections from the main process. You might want to adjust this value, especially in virtual environments where you can easily add or remove the number of virtual CPUs based on your requirements. To prevent performance issues or even IdM servers becoming unresponsive due to an ever-increasing TCP/IP queue on port 88, simulate a higher number of processes by manually setting the `KRB5KDC_ARGS` parameter to a higher value.

.Procedure

. Open the [filename]`/etc/sysconfig/krb5kdc` file in a text editor.

. Specify the value of the `KRB5KDC_ARGS` parameter. In this example, you are setting the number of processes to 10:

+
[literal,subs="+quotes,attributes,verbatim"]
....
KRB5KDC_ARGS='-w 10'
[...]
....

. Save and close the [filename]`/etc/sysconfig/krb5kdc` file.

. Reload the systemd configuration:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# systemctl daemon-reload
....


. Restart the `krb5kdc` service:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# systemctl restart krb5kdc.service
....

[NOTE]
====
You can use the IdM Healthcheck utility to verify that the KDC is configured to use the optimal number of worker processes. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_idm_healthcheck_to_monitor_your_idm_environment/proc_verifying-the-optimal-number-of-kdc-worker-processes-using-idm-healthcheck_using-idm-healthcheck-to-monitor-your-idm-environment[Verifying the optimal number of KDC worker processes using IdM Healthcheck].
====
