
:_mod-docs-content-type: CONCEPT

[id="con_how-to-use-this-guide_{context}"]
= How to use this guide

An {IPA} (IdM) domain includes IdM servers, also called replicas, and IdM clients. While xref:installing-idm[installing an IdM deployment] always starts with installing the primary IdM server, the order of the next installation steps depends on the targeted topology. For example, you can install an IdM replica before or after installing an IdM client. Additionally, certain IdM deployments require xref:integrating-idm-and-ad[a trust with {AD}], while others do not.

.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/index[Planning Identity Management] 
