// Module included in the following assemblies:
//
// include::assemblies/assembly_configuring-smart-cards-locally.adoc[leveloffset=+1]

:_mod-docs-content-type: PROCEDURE
[id="configuring-su-access-using-smart-card-authentication_{context}"]
= Using smart card authentication with the su command

[role="_abstract"]
Changing to a different user requires authentication. You can use a password or a certificate. Follow this procedure to use your smart card with the `su` command. It means that after entering the `su` command, you are prompted for the smart card PIN.

.Prerequisites

* Your IdM server and client have been configured for smart card authentication.
** See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#conf-idm-server-for-smart-card-auth_configuring-idm-for-smart-card-auth[Configuring the IdM server for smart card authentication]
** See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#conf-idm-client-for-smart-card-auth_configuring-idm-for-smart-card-auth[Configuring the IdM client for smart card authentication]
* The smart card contains your certificate and private key. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#storing-a-certificate-on-the-smart-card_configuring-idm-for-smart-card-auth[Storing a certificate on a smart card]
* The card is inserted in the reader and connected to the computer.

.Procedure

* In a terminal window, change to a different user with the `su` command:
+
[literal]
....
$ su - example.user
PIN for smart_card
....
+
If the configuration is correct, you are prompted to enter the smart card PIN.
