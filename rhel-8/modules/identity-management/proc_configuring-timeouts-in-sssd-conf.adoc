// Module included in the following assemblies:
//
// assembly_configuring-certficates-issued-adcs-for-smart-card-authentication.adoc
:_mod-docs-content-type: PROCEDURE
[id="configuring-timeouts-in-sssd-conf_{context}"]
= Configuring timeouts in sssd.conf

[role="_abstract"]
Authentication with a smart card certificate might take longer than the default timeouts used by SSSD. Time out expiration can be caused by:

* Slow reader
* A forwarding form a physical device into a virtual environment
* Too many certificates stored on the smart card
* Slow response from the OCSP (Online Certificate Status Protocol) responder if OCSP is used to verify the certificates

In this case you can prolong the following timeouts in the `sssd.conf` file, for example, to 60 seconds:

* `p11_child_timeout`
* `krb5_auth_timeout`

.Prerequisites

* You must be logged in as root.

.Procedure

. Open the `sssd.conf` file:
+
[literal]
--
[root@idmclient1 ~]# vim /etc/sssd/sssd.conf
--

. Change the value of `p11_child_timeout`:
+
[literal]
--
[pam]
p11_child_timeout = 60
--
. Change the value of `krb5_auth_timeout`:
+
[literal]
--
[domain/IDM.EXAMPLE.COM]
krb5_auth_timeout = 60
--

. Save the settings.

Now, the interaction with the smart card is allowed to run for 1 minute (60 seconds) before authentication will fail with a timeout.
