:_mod-docs-content-type: PROCEDURE
[id="discovering-and-joining-an-ad-domain-using-sssd_{context}"]
= Discovering and joining an AD Domain using SSSD

Follow this procedure to discover an AD domain and connect a RHEL system to that domain using SSSD.

.Prerequisites

* Ensure that the required ports are open:
+
** xref:ports-required-for-direct-integration-of-rhel-systems-into-ad-using-sssd_connecting-rhel-systems-directly-to-ad-using-sssd[Ports required for direct integration of RHEL systems into AD using SSSD]

* Ensure that you are using the AD domain controller server for DNS.
* Verify that the system time on both systems is synchronized. This ensures that Kerberos is able to work correctly.

.Procedure

. Install the following packages:
+
[literal,subs="+quotes,attributes"]
....
# {PackageManagerCommand} install samba-common-tools realmd oddjob oddjob-mkhomedir sssd adcli krb5-workstation
....

. To display information for a specific domain, run `realm discover` and add the name of the domain you want to discover:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# realm discover ad.example.com
ad.example.com
  type: kerberos
  realm-name: AD.EXAMPLE.COM
  domain-name: ad.example.com
  configured: no
  server-software: active-directory
  client-software: sssd
  required-package: oddjob
  required-package: oddjob-mkhomedir
  required-package: sssd
  required-package: adcli
  required-package: samba-common
....
+
The `realmd` system uses DNS SRV lookups to find the domain controllers in this domain automatically.
+
[NOTE]
====
The `realmd` system can discover both Active Directory and Identity Management domains. If both domains exist in your environment, you can limit the discovery results to a specific type of server using the `--server-software=active-directory` option.
====

. Configure the local RHEL system with the [command]`realm join` command. The `realmd` suite edits all required configuration files automatically. For example, for a domain named `ad.example.com`:
+
....
# realm join ad.example.com
....

.Verification

* Display an AD user details, such as the administrator user:
+
....
# getent passwd administrator@ad.example.com
administrator@ad.example.com:*:1450400500:1450400513:Administrator:/home/administrator@ad.example.com:/bin/bash
....

[role="_additional-resources"]
.Additional resources
* `realm(8)` and `nmcli(1)` man pages on your system
