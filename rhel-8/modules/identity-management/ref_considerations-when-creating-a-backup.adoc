:_mod-docs-content-type: REFERENCE
// This module is included in the following assemblies:
//
// assembly_backing-up-and-restoring-idm.adoc
// assembly_preparing-for-data-loss-with-idm-backups.adoc


[id="considerations-when-creating-a-backup_{context}"]
= Considerations when creating a backup

[role="_abstract"]
The important behaviors and limitations of the `ipa-backup` command include the following:

* By default, the `ipa-backup` utility runs in offline mode, which stops all IdM services. The utility automatically restarts IdM services after the backup is finished.

* A full-server backup must *always* run with IdM services offline, but a data-only backup can be performed with services online.

* By default, the `ipa-backup` utility creates backups on the file system containing the `/var/lib/ipa/backup/` directory. {RH} recommends creating backups regularly on a file system separate from the production filesystem used by IdM, and archiving the backups to a fixed medium, such as tape or optical storage.

// Removing the following bullet point because, as of now, we cannot specify the destination for an ipa-backup, so there is no way that DS can somehow write to the operating system's /tmp or /var/tmp, OR to its own PrivateTmp /tmp or /var/tmp... so this isn't an issue when creating an ipa-backup. Maybe when that functionality comes out, we can update this documentation.
// * Do not store backup files in the `/tmp` or `/var/tmp` directories. The IdM Directory Server uses a *PrivateTmp* directory and cannot access the common `/tmp` or `/var/tmp` directories from the operating system.

* Consider performing backups on link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/planning-the-replica-topology_planning-identity-management#the-hidden-replica-mode_planning-the-replica-topology[hidden replicas]. IdM services can be shut down on hidden replicas without affecting IdM clients.

ifeval::[{ProductNumber} == 8]
* Starting with RHEL 8.3.0, the `ipa-backup` utility checks if all of the services used in your IdM cluster, such as a Certificate Authority (CA), Domain Name System (DNS), and Key Recovery Agent (KRA), are installed on the server where you are running the backup. If the server does not have all these services installed, the `ipa-backup` utility exits with a warning, because backups taken on that host would not be sufficient for a full cluster restoration.
+
For example, if your IdM deployment uses an integrated Certificate Authority (CA), a backup run on a non-CA replica will not capture CA data. {RH} recommends verifying that the replica where you perform an `ipa-backup` has all of the IdM services used in the cluster installed.
+
You can bypass the IdM server role check with the `ipa-backup --disable-role-check` command, but the resulting backup will not contain all the data necessary to restore IdM fully.
endif::[]

ifeval::[{ProductNumber} == 9]
* The `ipa-backup` utility checks if all of the services used in your IdM cluster, such as a Certificate Authority (CA), Domain Name System (DNS), and Key Recovery Agent (KRA), are installed on the server where you are running the backup. If the server does not have all these services installed, the `ipa-backup` utility exits with a warning, because backups taken on that host would not be sufficient for a full cluster restoration.
+
For example, if your IdM deployment uses an integrated Certificate Authority (CA), a backup run on a non-CA replica will not capture CA data. {RH} recommends verifying that the replica where you perform an `ipa-backup` has all of the IdM services used in the cluster installed.
+
You can bypass the IdM server role check with the `ipa-backup --disable-role-check` command, but the resulting backup will not contain all the data necessary to restore IdM fully.
endif::[]
