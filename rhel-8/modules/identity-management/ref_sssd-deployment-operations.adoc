:_mod-docs-content-type: REFERENCE
[id='sssd-deployment-operations_{context}']
= SSSD communication patterns

[role="_abstract"]
The System Security Services Daemon (SSSD) is a system service to access remote directories and authentication mechanisms. If configured on an {IPA} IdM client, it connects to the IdM server, which provides authentication, authorization and other identity and policy information. If the IdM server is in a trust relationships with Active Directory (AD), SSSD also connects to AD to perform authentication for AD users using the Kerberos protocol. By default, SSSD uses Kerberos to authenticate any non-local user. In special situations, SSSD might be configured to use the LDAP protocol instead.

The SSSD can be configured to communicate with multiple servers. The tables below show common communication patterns for SSSD in IdM.

////
* SSSD on {IPA} clients talking to {IPA} servers:

** DNS resolution, using the DNS protocol against the DNS resolvers configured on the client system
** Kerberos protocol operations to the IdM Kerberos Key Distribution Center, and Active Directory domain controllers
** the LDAP protocol operations over TCP/TCP6 to port 389 on IdM servers, using SASL GSSAPI authentication, plain LDAP, or both
** (optionally) In case of smart-card authentication, to the OCSP responder, if configured. This often is done via the HTTP protocol to port 80, but it depends on
  the actual value of the OCSP responder URL in a client certificate

+
* SSSD on IdM trust agents talking to Active Directory Domain Controllers:

** DNS resolution, using the DNS protocol against the DNS resolvers configured on the client system
** Kerberos protocol operations to the IdM Kerberos Key Distribution Center, and Active Directory domain controllers
** Requests to TCP/TCP6 ports 389 and 3268 using the LDAP protocol to query Active Directory user and group information
** (optionally) In case of smart-card authentication, to the OCSP responder, if configured. This often is done via the HTTP protocol to port 80, but it depends on the actual value of the OCSP responder URL in a client certificate.

//-
////
[id='tab-sssd-deployment-operations-clients_{context}']
.Communication patterns of SSSD on IdM clients when talking to IdM servers
[cols="3,2,3"]
|===
|Operation|Protocol used|Purpose

|DNS resolution against the DNS resolvers configured on the client system
|DNS
|To discover the IP addresses of IdM servers

|Requests to ports 88 (TCP/TCP6 and UDP/UDP6), 464 (TCP/TCP6 and UDP/UDP6), and 749 (TCP/TCP6) on an {IPA} replica and {AD} domain controllers
|Kerberos
|To obtain a Kerberos ticket; to change a Kerberos password

|Requests over TCP/TCP6 to ports 389 on IdM servers, using SASL GSSAPI authentication, plain LDAP, or both
|LDAP
|To obtain information about IdM users and hosts, download HBAC and sudo rules, automount maps, the SELinux user context, public SSH keys, and other information stored in IdM LDAP

|(optionally) In case of smart-card authentication, requests to the Online Certificate Status Protocol (OCSP) responder, if it is configured. This often is done via port 80, but it depends on the actual value of the OCSP responder URL in a client certificate.
|HTTP
|To obtain information about the status of the certificate installed in the smart card
|===

[id='tab-sssd-deployment-operations-trust-controllers_{context}']
.Communication patterns of SSSD on IdM servers acting as trust agents when talking to Active Directory Domain Controllers
[cols="3,2,3"]
|===
|Operation|Protocol used|Purpose

|DNS resolution against the DNS resolvers configured on the client system
|DNS
|To discover the IP addresses of IdM servers

|Requests to ports 88 (TCP/TCP6 and UDP/UDP6), 464 (TCP/TCP6 and UDP/UDP6), and 749 (TCP/TCP6) on an {IPA} replica and {AD} domain controllers
|Kerberos
|To obtain a Kerberos ticket; change a Kerberos password; administer Kerberos remotely

|Requests to ports 389 (TCP/TCP6 and UDP/UDP6) and 3268 (TCP/TCP6)
|LDAP
|To query Active Directory user and group information; to discover Active Directory domain controllers

|(optionally) In case of smart-card authentication, requests to the Online Certificate Status Protocol (OCSP) responder, if it is configured. This often is done via port 80, but it depends on the actual value of the OCSP responder URL in a client certificate.
|HTTP
|To obtain information about the status of the certificate installed in the smart card
|===

.Additional resources
* xref:ipa-client-communication-with-server_{context}[IdM client's communications with the server during post-installation deployment]
