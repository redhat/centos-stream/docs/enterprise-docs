[id="methods-for-migrating-passwords-when-migrating-ldap-to-idm_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Methods for migrating passwords when migrating LDAP to IdM

To migrate user accounts from LDAP to {IPA} (IdM) without forcing the users to change their passwords, you can use the following methods:

[[using-the-migration-web-page_{context}]]
*Method 1: Using the migration web page*

Tell users to enter their LDAP credentials once into a special page in the IdM Web UI, `_\https://ipaserver.example.com/ipa/migration_`. A script running in the background then captures the clear text password and properly updates the user account with the password and an appropriate Kerberos hash.


[[using-sssd_{context}]]
*Method 2 (recommended): Using SSSD*

Mitigate the user impact of the migration by using the System Security Services Daemon (SSSD) to generate the required user keys. For deployments with a lot of users or where users should not be burdened with password changes, this is the best scenario.


.Workflow

. A user tries to log into a machine with SSSD.

. SSSD attempts to perform Kerberos authentication against the IdM server.

. Even though the user exists in the system, the authentication fails with the error _key type is not supported_ because the Kerberos hashes do not exist yet.

. SSSD performs a plain text LDAP bind over a secure connection.

. IdM intercepts this bind request. If the user has a Kerberos principal but no Kerberos hashes, then the IdM identity provider generates the hashes and stores them in the user entry.

. If authentication is successful, SSSD disconnects from IdM and tries Kerberos authentication again. This time, the request succeeds because the hash exists in the entry.

With method 2, the entire process is invisible to the users. They log in to a client service without noticing that their password has been moved from LDAP to IdM.
