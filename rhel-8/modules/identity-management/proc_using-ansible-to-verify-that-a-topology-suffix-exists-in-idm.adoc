:_mod-docs-content-type: PROCEDURE
[id="using-ansible-to-verify-that-a-topology-suffix-exists-in-idm_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using Ansible to verify that a topology suffix exists in IdM
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
In the context of replication agreements in {IPA} (IdM), topology suffixes store the data that is replicated. IdM supports two types of topology suffixes: `domain` and `ca`. Each suffix represents a separate back end, a separate replication topology.
When a replication agreement is configured, it joins two topology suffixes of the same type on two different servers.

The `domain` suffix contains all domain-related data, such as data about users, groups, and policies.
The `ca` suffix contains data for the Certificate System component. It is only present on servers with a certificate authority (CA) installed.

Follow this procedure to use an Ansible playbook to ensure that a topology suffix exists in IdM. The example describes how to ensure that the `domain` suffix exists in IdM.


.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password` and that you have access to a file that stores the password protecting the *secret.yml* file.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. Navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Copy the `verify-topologysuffix.yml` Ansible playbook file provided by the `ansible-freeipa` package:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/topology/ verify-topologysuffix.yml verify-topologysuffix-copy.yml*
....

. Open the `verify-topologysuffix-copy.yml` Ansible playbook file for editing.

. Adapt the file by setting the following variables in the `ipatopologysuffix` section:

* Indicate that the value of the `ipaadmin_password` variable is defined in the *secret.yml* Ansible vault file.
* Set the `suffix` variable to `domain`. If you are verifying the presence of the `ca` suffix, set the variable to `ca`.
* Ensure that the `state` variable is set to `verified`. No other option is possible.


+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to handle topologysuffix
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Verify topology suffix
    ipatopologysuffix:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *suffix: domain*
      *state: verified*
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory verify-topologysuffix-copy.yml*
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/assembly_managing-replication-topology_installing-identity-management#assembly_explaining-replication-agreements-topology-suffixes-and-topology-segments_assembly_managing-replication-topology[Explaining Replication Agreements, Topology Suffixes, and Topology Segments]
* `/usr/share/doc/ansible-freeipa/README-topology.md` 
* Sample playbooks in `/usr/share/doc/ansible-freeipa/playbooks/topology`
