:_mod-docs-content-type: PROCEDURE

[id="installing-the-expiring-password-notification-tool_{context}"]

= Installing the Expiring Password Notification tool

[role="_abstract"]
Follow this procedure to install the Expiring Password Notification (EPN) tool.

.Prerequisites

* Install the EPN tool on either an Identity Management (IdM) replica or an IdM client with a local Postfix SMTP server configured as a smart host.

.Procedure

* Install the EPN tool:

+
[literal,subs="+quotes,attributes"]
....
# {PackageManagerCommand} install ipa-client-epn
....
