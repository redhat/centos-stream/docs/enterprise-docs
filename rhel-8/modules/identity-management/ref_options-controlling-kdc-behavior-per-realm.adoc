:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// assembly_adjusting-the-performance-of-the-kdc.adoc

:experimental:

[id="ref_options-controlling-kdc-behavior-per-realm_{context}"]
= Options controlling KDC behavior per realm

[role="_abstract"]
To track locking and unlocking user accounts for each Kerberos realm, the KDC writes to its database after each successful and failed authentication. By adjusting the following options in the `[dbmodules]` section of the [filename]`/etc/krb5.conf` file, you may be able to improve performance by minimizing how often the KDC writes information.

disable_last_success:: If set to `true`, this option suppresses KDC updates to the `Last successful authentication` field of principal entries requiring preauthentication.
+
|===
| Default value | `false`
| Valid range | `true` or `false`
|===

disable_lockout:: If set to `true`, this option suppresses KDC updates to the `Last failed authentication` and `Failed password attempts` fields of principal entries requiring preauthentication. Setting this flag may improve performance, but disabling account lockout may be considered a security risk.
+
|===
| Default value | `false`
| Valid range | `true` or `false`
|===


[role="_additional-resources"]
.Additional resources
* xref:proc_adjusting-kdc-settings-per-realm_assembly_adjusting-the-performance-of-the-kdc[Adjusting KDC settings per realm]
