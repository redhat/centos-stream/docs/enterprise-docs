:_mod-docs-content-type: PROCEDURE
:experimental:
[id="proc_removing-server-from-topology-using-web-ui_{context}"]
[[managing-topology-remove-ui]]
= Removing server from topology using the Web UI

[role="_abstract"]
You can use {IPA} (IdM) web interface to remove a server from the topology. This action does not uninstall the server components from the host. 

.Prerequisites

* You are logged in as an IdM administrator.
* The server you want to remove is *not* the only server connecting other servers with the rest of the topology; this would cause the other servers to become isolated, which is not allowed.
* The server you want to remove is *not* your last CA or DNS server.


[WARNING]
====

Removing a server is an irreversible action. If you remove a server, the only way to introduce it back into the topology is to install a new replica on the machine.

====

.Procedure

. Select menu:IPA Server[Topology > IPA Servers].

. Click on the name of the server you want to delete.
+
.Selecting a server
+
image::mng_top_delete.png[]

. Click btn:[Delete Server].

.Additional resources
ifeval::[{ProductNumber} == 8]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html/installing_identity_management/uninstalling-an-ipa-server_installing-identity-management#uninstalling-an-ipa-server_installing-identity-management[Uninstalling an IdM server]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/installing_identity_management/uninstalling-an-ipa-server_installing-identity-management#uninstalling-an-ipa-server_installing-identity-management[Uninstalling an IdM server]
endif::[]