:_mod-docs-content-type: PROCEDURE
[id="ensuring-the-presence-of-an-asymmetric-service-vault-in-idm-using-ansible_{context}"]
= Ensuring the presence of an asymmetric service vault in IdM using Ansible

[role="_abstract"]
Follow this procedure to use an Ansible playbook to create a service vault container with one or more private vaults to securely store sensitive information. In the example used in the procedure below, the administrator creates an asymmetric vault named *secret_vault*. This ensures that the vault members have to authenticate using a private key to retrieve the secret in the vault. The vault members will be able to retrieve the file from any IdM client.


.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* You know the *IdM administrator* password.



.Procedure

. Navigate to the `/usr/share/doc/ansible-freeipa/playbooks/vault` directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd /usr/share/doc/ansible-freeipa/playbooks/vault*
....


. Obtain the public key of the service instance. For example, using the `openssl` utility:
+
.. Generate the `service-private.pem` private key.
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *openssl genrsa -out service-private.pem 2048*
Generating RSA private key, 2048 bit long modulus
.+++
...........................................+++
e is 65537 (0x10001)
....

.. Generate the `service-public.pem` public key based on the private key.
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *openssl rsa -in service-private.pem -out service-public.pem -pubout*
writing RSA key
....


. Optional: Create an inventory file if it does not exist, for example *inventory.file*:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *touch inventory.file*
....

. Open your inventory file and define the IdM server that you want to configure in the `[ipaserver]` section. For example, to instruct Ansible to configure *server.idm.example.com*, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....


. Make a copy of the *ensure-asymmetric-vault-is-present.yml* Ansible playbook file. For example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp ensure-asymmetric-vault-is-present.yml ensure-asymmetric-service-vault-is-present-copy.yml*
....

. Open the *ensure-asymmetric-vault-is-present-copy.yml* file for editing.

. Add a task that copies the *service-public.pem* public key from the Ansible controller to the *server.idm.example.com* server.

. Modify the rest of the file by setting the following variables in the `ipavault` task section:

* Set the `ipaadmin_password` variable to the IdM administrator password.
* Define the name of the vault using the `name` variable, for example *secret_vault*.
* Set the `vault_type` variable to *asymmetric*.
* Set the `service` variable to the principal of the service that owns the vault, for example *HTTP/webserver1.idm.example.com*.
* Set the `public_key_file` to the location of your public key.

+
This is the modified Ansible playbook file for the current example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Tests
  hosts: ipaserver
  gather_facts: false
  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Copy public key to ipaserver.
    copy:
      src: /path/to/service-public.pem
      dest: /usr/share/doc/ansible-freeipa/playbooks/vault/service-public.pem
      mode: 0600
  - name: Add data to vault, from a LOCAL file.
    ipavault:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: secret_vault
      vault_type: asymmetric
      service: HTTP/webserver1.idm.example.com
      public_key_file: /usr/share/doc/ansible-freeipa/playbooks/vault/service-public.pem
....

. Save the file.

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory.file ensure-asymmetric-service-vault-is-present-copy.yml*
....
