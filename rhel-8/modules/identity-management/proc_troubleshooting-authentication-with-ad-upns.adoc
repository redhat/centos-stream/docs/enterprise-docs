:_mod-docs-content-type: PROCEDURE
[id="proc_troubleshooting-authentication-with-ad-upns_{context}"]
= Gathering troubleshooting data for AD UPN authentication issues

[role="_abstract"]
Follow this procedure to gather troubleshooting data about the User Principal Name (UPN) configuration from your Active Directory (AD) environment and your IdM environment. If your AD users are unable to log in using alternate UPNs, you can use this information to narrow your troubleshooting efforts.

.Prerequisites

* You must be logged in to an IdM Trust Controller or Trust Agent to retrieve information from an AD domain controller.
* You need `root` permissions to modify the following configuration files, and to restart IdM services.

.Procedure

. Open the [filename]`/usr/share/ipa/smb.conf.empty` configuration file in a text editor.

. Add the following contents to the file.
+
[literal,subs="+quotes,attributes"]
....
[global]
log level = 10
....

. Save and close the [filename]`/usr/share/ipa/smb.conf.empty` file.

. Open the [filename]`/etc/ipa/server.conf` configuration file in a text editor. If you do not have that file, create one.

. Add the following contents to the file.
+
[literal,subs="+quotes,attributes"]
....
[global]
debug = True
....

. Save and close the [filename]`/etc/ipa/server.conf` file.

. Restart the Apache webserver service to apply the configuration changes:
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *systemctl restart httpd*
....

. Retrieve trust information from your AD domain:
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *ipa trust-fetch-domains _<ad.example.com>_*
....

. Review the debugging output and troubleshooting information in the following log files:
+
* [filename]`/var/log/httpd/error_log`
* [filename]`/var/log/samba/log.*`


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/5825651[Using rpcclient to gather troubleshooting data for AD UPN authentication issues] (Red Hat Knowledgebase)
