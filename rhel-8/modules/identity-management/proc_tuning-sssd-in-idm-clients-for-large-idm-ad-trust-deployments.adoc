:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_tuning-sssd-performance-for-large-idm-ad-trust-deployments.adoc

:experimental:

[id="proc_tuning-sssd-in-idm-clients-for-large-idm-ad-trust-deployments_{context}"]
= Tuning SSSD in IdM clients for large IdM-AD trust deployments

[role="_abstract"]
This procedure applies tuning options to SSSD service configuration in an IdM client to improve its response time when retrieving information from a large AD environment.


.Prerequisites

* You need `root` permissions to edit the [filename]`/etc/sssd/sssd.conf` configuration file.


.Procedure

. Determine the number of seconds a single un-cached login takes.
.. Clear the SSSD cache on the IdM client `client.example.com`.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client ~]# *sss_cache -E*
....
.. Measure how long it takes to log in as an AD user with the `time` command. In this example, from the IdM client `client.example.com`, log into the same host as the user `ad-user` from the `ad.example.com` AD domain.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client ~]# *time ssh _ad-user_@_ad.example.com_@client.example.com*
....
.. Type in the password as soon as possible.
+
[literal,subs="+quotes,attributes,verbatim"]
....
Password:
Last login: Sat Jan 23 06:29:54 2021 from 10.0.2.15
[ad-user@ad.example.com@client ~]$
....
.. Log out as soon as possible to display elapsed time. In this example, a single un-cached login takes about `9` seconds.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[ad-user@ad.example.com@client /]$ *exit*
logout
Connection to client.example.com closed.

*real    0m8.755s*
user    0m0.017s
sys     0m0.013s
....

. Open the [filename]`/etc/sssd/sssd.conf` configuration file in a text editor.

. Add the following options to the `[domain]` section for your Active Directory domain. Set the `pam_id_timeout` and `krb5_auth_timeout` options to the number of seconds an un-cached login takes. If you do not already have a domain section for your AD domain, create one.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[domain/_example.com_/_ad.example.com_]
*krb5_auth_timeout = _9_*
*ldap_deref_threshold = 0*
...
....

. Add the following option to the `[pam]` section:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[pam]
*pam_id_timeout = _9_*
....



. Save and close the [filename]`/etc/sssd/sssd.conf` file on the server.

. Restart the SSSD service to load the configuration changes.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@client ~]# *systemctl restart sssd*
....

[role="_additional-resources"]
.Additional resources
* xref:options-in-sssdconf-for-tuning-servers-and-clients_assembly_tuning-sssd-performance-for-large-idm-ad-trust-deployments[Options for tuning SSSD in IdM servers and clients for large IdM-AD trust deployments]
