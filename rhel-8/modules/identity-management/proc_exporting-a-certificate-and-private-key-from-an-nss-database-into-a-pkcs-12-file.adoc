:_mod-docs-content-type: PROCEDURE

[id="proc_exporting-a-certificate-and-private-key-from-an-nss-database-into-a-pkcs-12-file_{context}"]
= Exporting a certificate and private key from an NSS database into a PKCS #12 file


.Procedure

. Use the [command]`pk12util` command to export the certificate from the NSS database to the `PKCS12` format. For example, to export the certificate with the [replaceable]`some_user` nickname from the NSS database stored in the [filename]`~/certdb` directory into the [replaceable]`~/some_user.p12` file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *pk12util -d [filename]`~/certdb` -o [replaceable]`~/some_user.p12` -n some_user*
Enter Password or Pin for "NSS Certificate DB":
Enter password for PKCS12 file:
Re-enter password:
pk12util: PKCS12 EXPORT SUCCESSFUL
....


+
. Set appropriate permissions for the `.p12` file:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *chmod 600 ~/some_user.p12*
....

+
Because the `PKCS #12` file also contains the private key, it must be protected to prevent other users from using the file. Otherwise, they would be able to impersonate the user.
