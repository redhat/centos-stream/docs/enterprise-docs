:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_generating-crl-on-the-idm-ca-server.adoc
// assembly_generating-crl-on-the-idm-ca-server.adoc

ifdef::stopping-crlgen-rhel8[]

[id="stopping-crl-generation-on-idm-server_{context}"]
= Stopping CRL generation on an IdM server

[role="_abstract"]
To stop generating the Certificate Revocation List (CRL) on the IdM CRL publisher server, use the `ipa-crlgen-manage` command.
Before you disable the generation, verify that the server really generates CRL. You can then disable it.

.Prerequisites

// Only display the following if this is the RHEL 8 Conf&Man guide
ifeval::[{ProductNumber} == 8]
* Identity Management (IdM) server is installed on the RHEL 8.1 system or newer.
endif::[]
* You must be logged in as root.

.Procedure

. Check if your server is generating the CRL:
+
[literal,subs="+quotes,attributes,verbatim"]
--
[root@server ~]# *ipa-crlgen-manage status*
CRL generation: *enabled*
Last CRL update: 2019-10-31 12:00:00
Last CRL Number: 6
The ipa-crlgen-manage command was successful
--

. Stop generating the CRL on the server:
+
[literal,subs="+quotes,attributes,verbatim"]
--
[root@server ~]# *ipa-crlgen-manage disable*
Stopping pki-tomcatd
Editing /var/lib/pki/pki-tomcat/conf/ca/CS.cfg
Starting pki-tomcatd
Editing /etc/httpd/conf.d/ipa-pki-proxy.conf
Restarting httpd
CRL generation disabled on the local host. Please make sure to configure CRL generation on another master with ipa-crlgen-manage enable.
The ipa-crlgen-manage command was successful
--


. Check if the server stopped generating CRL:
+
[literal,subs="+quotes,attributes,verbatim"]
--
[root@server ~]# *ipa-crlgen-manage status*
--

The server stopped generating the CRL. The next step is to enable CRL generation on the IdM replica.
endif::stopping-crlgen-rhel8[]

ifdef::stopping-crlgen-rhel7[]

[id="stopping-crl-generation-on-rhel7-IdM-CA-server_{context}"]
= Stopping CRL generation on a RHEL 7 IdM CA server

[NOTE]
====
Follow these steps only if your IdM deployment uses an embedded certificate authority (CA).
====

Follow this procedure to stop generating the Certificate Revocation List (CRL) on the *rhel7.example.com* CA server using the `ipa-crlgen-manage` command.

.Prerequisites

* You must be logged in as root.

.Procedure

. Optional: Check if *rhel7.example.com* is generating the CRL:
+
[literal,subs="+quotes,attributes,verbatim"]
--
[root@rhel7 ~]# *ipa-crlgen-manage status*
CRL generation: *enabled*
Last CRL update: 2019-10-31 12:00:00
Last CRL Number: 6
The ipa-crlgen-manage command was successful
--

. Stop generating the CRL on the *rhel7.example.com* server:
+
[literal,subs="+quotes,attributes,verbatim"]
--
[root@rhel7 ~]# *ipa-crlgen-manage disable*
Stopping pki-tomcatd
Editing /var/lib/pki/pki-tomcat/conf/ca/CS.cfg
Starting pki-tomcatd
Editing /etc/httpd/conf.d/ipa-pki-proxy.conf
Restarting httpd
CRL generation disabled on the local host. Please make sure to configure CRL generation on another master with ipa-crlgen-manage enable.
The ipa-crlgen-manage command was successful
--

.Verification

. Check if the *rhel7.example.com* server stopped generating the CRL:
+
[literal,subs="+quotes,attributes,verbatim"]
--
[root@rhel7 ~]# *ipa-crlgen-manage status*
--

The *rhel7.example.com* server stopped generating the CRL. The next step is to enable generating the CRL on *rhel8.example.com*.
endif::stopping-crlgen-rhel7[]
