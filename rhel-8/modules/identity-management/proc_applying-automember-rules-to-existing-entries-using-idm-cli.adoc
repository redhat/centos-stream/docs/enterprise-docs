:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="applying-automember-rules-to-existing-entries-using-idm-cli_{context}"]
= Applying automember rules to existing entries using IdM CLI

[role="_abstract"]
Automember rules apply automatically to user and host entries created after the rules were added. They are not applied retroactively to entries that existed before the rules were added.

To apply automember rules to previously added entries, you have to manually rebuild automatic membership. Rebuilding automatic membership re-evaluates all existing automember rules and applies them either to all user or hosts entries, or to specific entries.

NOTE: Rebuilding automatic membership *does not* remove user or host entries from groups, even if the entries no longer match the group's inclusive conditions. To remove them manually, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_idm_users_groups_hosts_and_access_control_rules/managing-user-groups-in-idm-cli_managing-users-groups-hosts#removing-a-member-from-a-user-group-using-idm-cli_managing-user-groups-in-idm-cli[Removing a member from a user group using IdM CLI] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_idm_users_groups_hosts_and_access_control_rules/managing-host-groups-using-the-idm-cli_managing-users-groups-hosts#removing-idm-host-group-members-using-the-cli_managing-host-groups-using-the-idm-cli[Removing IdM host group members using the CLI].

.Prerequisites

* You must be logged in as the administrator. For details, see link:
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]

.Procedure

* To rebuild automatic membership, enter the [command]`ipa automember-rebuild` command. Use the following options to specify the entries to target:

** To rebuild automatic membership for all users, use the [option]`--type=group` option:
+
[literal, subs="+quotes,verbatim"]
--
$ *ipa automember-rebuild --type=group*
--------------------------------------------------------
Automember rebuild task finished. Processed (9) entries.
--------------------------------------------------------
--

** To rebuild automatic membership for all hosts, use the [option]`--type=hostgroup` option.

** To rebuild automatic membership for a specified user or users, use the [option]`--users=_target_user_` option:
+
[literal, subs="+quotes,verbatim"]
--
$ *ipa automember-rebuild --users=target_user1 --users=target_user2*
--------------------------------------------------------
Automember rebuild task finished. Processed (2) entries.
--------------------------------------------------------
--

** To rebuild automatic membership for a specified host or hosts, use the [option]`--hosts=_client.idm.example.com_` option.
