:_mod-docs-content-type: PROCEDURE
[id="uninstalling-an-idm-client-additional-steps-after-multiple-past-installations_{context}"]
= Uninstalling an IdM client: additional steps after multiple past installations


[role="_abstract"]
If you install and uninstall a host as an {IPA} (IdM) client multiple times, the uninstallation procedure might not restore the pre-IdM Kerberos configuration.

In this situation, you must manually remove the IdM Kerberos configuration. In extreme cases, you must reinstall the operating system.

.Prerequisites

* You have used the `ipa-client-install --uninstall` command to uninstall the IdM client configuration from the host. However, you can still obtain a Kerberos ticket-granting ticket (TGT) for an IdM user from the IdM server.
* You have checked that the `/var/lib/ipa-client/sysrestore` directory is empty and hence you cannot restore the prior-to-IdM-client configuration of the system using the files in the directory.


.Procedure

. Check the `/etc/krb5.conf.ipa` file:

** If the contents of the `/etc/krb5.conf.ipa` file are the same as the contents of the  `krb5.conf` file prior to the installation of the IdM client, you can:

+
--
... Remove the `/etc/krb5.conf` file:

+

[literal,subs="+quotes,attributes"]
....
# *rm /etc/krb5.conf*
....

... Rename the `/etc/krb5.conf.ipa` file into `/etc/krb5.conf`:

+
[literal,subs="+quotes,attributes"]
....
# *mv /etc/krb5.conf.ipa /etc/krb5.conf*
....
--

** If the contents of the `/etc/krb5.conf.ipa` file are not the same as the contents of the `krb5.conf` file prior to the installation of the IdM client, you can at least restore the Kerberos configuration to the state directly after the installation of the operating system:

+
--
... Re-install the `krb5-libs` package:

+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} reinstall krb5-libs*
....

+
As a dependency, this command will also re-install the `krb5-workstation` package and the original version of the `/etc/krb5.conf` file.
--

. Remove the `var/log/ipaclient-install.log` file if present.

.Verification

* Try to obtain IdM user credentials. This should fail:

+
[literal,subs="+quotes,attributes"]
....
[root@r8server ~]# *kinit admin*
kinit: Client 'admin@EXAMPLE.COM' not found in Kerberos database while getting initial credentials
[root@r8server ~]#
....

The `/etc/krb5.conf` file is now restored to its factory state. As a result, you cannot obtain a Kerberos TGT for an IdM user on the host.
