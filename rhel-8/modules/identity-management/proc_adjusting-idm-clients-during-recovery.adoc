:_mod-docs-content-type: PROCEDURE
// Module included in the following titles/assemblies:
//
// /configuring-and-maintaining/performing-disaster-recovery-with-identity-management.adoc


[id="adjusting-idm-clients-during-recovery_{context}"]
= Adjusting IdM clients during recovery

[role="_abstract"]
While IdM servers are being restored, you may need to adjust IdM clients to reflect changes in the replica topology.

.Procedure

. *Adjusting DNS configuration*:
.. If `/etc/hosts` contains any references to IdM servers, ensure that hard-coded IP-to-hostname mappings are valid.
.. If IdM clients are using IdM DNS for name resolution, ensure that the `nameserver` entries in `/etc/resolv.conf` point to working IdM replicas providing DNS services.

. *Adjusting Kerberos configuration*:
.. By default, IdM clients look to DNS Service records for Kerberos servers, and will adjust to changes in the replica topology:
+
[literal,subs="+quotes,attributes"]
....
[root@client ~]# *grep dns_lookup_kdc /etc/krb5.conf*
  dns_lookup_kdc = *true*
....
.. If IdM clients have been hard-coded to use specific IdM servers in `/etc/krb5.conf`:
+
[literal,subs="+quotes,attributes"]
....
[root@client ~]# *grep dns_lookup_kdc /etc/krb5.conf*
  dns_lookup_kdc = *false*
....
make sure `kdc`, `master_kdc` and `admin_server` entries in `/etc/krb5.conf` are pointing to IdM servers that work properly:
+
[literal,subs="+quotes,attributes"]
....
[realms]
 EXAMPLE.COM = {
  kdc = *functional-server.example.com*:88
  master_kdc = *functional-server.example.com*:88
  admin_server = *functional-server.example.com*:749
  default_domain = example.com
  pkinit_anchors = FILE:/var/lib/ipa-client/pki/kdc-ca-bundle.pem
  pkinit_pool = FILE:/var/lib/ipa-client/pki/ca-bundle.pem
}
....

. *Adjusting SSSD configuration*:
.. By default, IdM clients look to DNS Service records for LDAP servers and adjust to changes in the replica topology:
+
[literal,subs="+quotes,attributes"]
....
[root@client ~]# *grep ipa_server /etc/sssd/sssd.conf*
ipa_server = *\_srv_*, functional-server.example.com
....
.. If IdM clients have been hard-coded to use specific IdM servers in `/etc/sssd/sssd.conf`, make sure the `ipa_server` entry points to IdM servers that are working properly:
+
[literal,subs="+quotes,attributes"]
....
[root@client ~]# *grep ipa_server /etc/sssd/sssd.conf*
ipa_server = *functional-server.example.com*
....

. *Clearing SSSD's cached information*:
* The SSSD cache may contain outdated information pertaining to lost servers. If users experience inconsistent authentication problems, purge the SSSD cache :
+
[literal,subs="+quotes,attributes"]
....
[root@client ~]# *sss_cache -E*
....


.Verification

. Verify the Kerberos configuration by retrieving a Kerberos Ticket-Granting-Ticket as an IdM user.
+
[literal,subs="+quotes,attributes"]
....
[root@client ~]# *kinit admin*
Password for admin@EXAMPLE.COM:

[root@client ~]# *klist*
Ticket cache: KCM:0
Default principal: admin@EXAMPLE.COM

Valid starting       Expires              Service principal
10/31/2019 18:44:58  11/25/2019 18:44:55  *krbtgt/EXAMPLE.COM@EXAMPLE.COM*
....
. Verify the SSSD configuration by retrieving IdM user information.
+
[literal,subs="+quotes,attributes"]
....
[root@client ~]# *id admin*
uid=1965200000(admin) gid=1965200000(admins) groups=1965200000(admins)
....
