[id="removing-a-certificate-issued-by-an-external-ca-from-an-idm-user-host-or-service-account-by-using-the-idm-cli_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Removing a certificate issued by an external CA from an IdM user, host, or service account by using the IdM CLI
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.


As an {IPA} (IdM) administrator, you can remove an externally signed certificate from the account of an IdM user, host, or service by using the {IPA} (IdM) CLI .

.Prerequisites

* You have obtained the ticket-granting ticket of an administrative user.

.Procedure

* To remove a certificate from an IdM user, enter:

+
[literal,subs="+quotes,verbatim,macros"]
....
$ *ipa user-remove-cert _user_ --certificate=pass:attributes[{blank}]_MIQTPrajQAwg..._*
....

+
The command requires you to specify the following information:

+
--
* The name of the user

* The Base64-encoded DER certificate
--

[NOTE]
====

Instead of copying and pasting the certificate contents into the command line, you can convert the certificate to the DER format and then re-encode it to Base64. For example, to remove the `user_cert.pem` certificate from `user`, enter:

[literal,subs="+quotes,verbatim,macros"]
....
$ *ipa user-remove-cert __user__ --certificate="$(openssl x509 -outform der -in __user_cert.pem__ | base64 -w 0)"*
....

====

You can run the `ipa user-remove-cert` command interactively by executing it without adding any options.



To remove a certificate from an IdM host, enter:

* [command]`ipa host-remove-cert`

To remove a certificate from an IdM service, enter:

* [command]`ipa service-remove-cert`

.Additional resources

* xref:managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca_{ProjectNameID}[Managing certificates for users, hosts, and services using the integrated IdM CA]
