:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="deleting-host-groups-in-the-idm-web-ui_{context}"]
= Deleting host groups in the IdM Web UI

[role="_abstract"]
Follow this procedure to delete IdM host groups using the Web interface (Web UI).

.Prerequisites

* Administrator privileges for managing IdM or User Administrator role.
* You are logged-in to the IdM Web UI. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/accessing-the-ipa-web-ui-in-a-web-browser_configuring-and-managing-idm[Accessing the IdM Web UI in a web browser].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/accessing-the-ipa-web-ui-in-a-web-browser_accessing-idm-services[Accessing the IdM Web UI in a web browser].
endif::[]


.Procedure

. Click *Identity → Groups* and select the *Host Groups* tab.
. Select the IdM host group to remove, and click *Delete*. A confirmation dialog appears.
. Click *Delete* to confirm

+
image:idm-deleting-host-groups.png[Screenshot of the "Remove host groups" pop-up window asking if you are sure you want to delete the selected entries. There are two buttons at the bottom right: "Delete" and "Cancel."]

NOTE: Removing a host group does not delete the group members from IdM.
