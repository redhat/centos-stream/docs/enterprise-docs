:_newdoc-version: 2.18.3
:_template-generated: 2025-02-06
:_mod-docs-content-type: PROCEDURE

[id="viewing-and-modifying-the-visual-representation-of-the-replication-topology-using-the-webui_{context}"]
= Viewing and modifying the visual representation of the replication topology using the WebUI

Using the Web UI, you can view, manipulate, and transform the representation of the replication topology. The topology graph in the web UI shows the relationships between the servers in the domain. You can move individual topology nodes by holding and dragging the mouse.

.Interpreting the topology graph
Servers joined in a domain replication agreement are connected by an orange arrow. Servers joined in a CA replication agreement are connected by a blue arrow.

Topology graph example: recommended topology::
+
The recommended topology example below shows one of the possible recommended topologies for four servers: each server is connected to at least two other servers, and more than one server is a CA server.
+
.Recommended topology example
+
image::mng_top_rec.png[]

Topology graph example: discouraged topology::
+
In the discouraged topology example below, `server1` is a single point of failure. All the other servers have replication agreements with this server, but not with any of the other servers. Therefore, if `server1` fails, all the other servers will become isolated.
+
Avoid creating topologies like this.
+
.Discouraged topology example: Single Point of Failure
+
image::mng_top_single.png[]

.Prerequisites

* You are logged in as an IdM administrator.

.Procedure

. Select menu:IPA Server[Topology > Topology Graph].

. Make changes to the topology:

** You can move the topology graph nodes using the left mouse button:
+
image::customize_graph_1.png[]

** You can zoom in and zoom out the topology graph using the mouse wheel:
+
image::customize_graph_2.png[]

** You can move the canvas of the topology graph by holding the left mouse button:
+
image::customize_graph_3.png[]

. If you make any changes to the topology that are not immediately reflected in the graph, click btn:[Refresh].
