:_mod-docs-content-type: PROCEDURE

:experimental:

[id="proc_importing-personal-certificate-for-authentication-in-firefox_{context}"]
= Importing personal certificate for authentication in Firefox

[role="_abstract"]
The following example shows how to import personal certificates for authentication in the Mozilla Firefox.

.Prerequisites
. You have a personal certificate stored on your device.

To use a personal certificate for authentication:

.Procedure
. Open `Certificate Manager`.
. Select the `Your Certificates` tab and click btn:[Import].
+
.Importing a Personal Certificate for Authentication in Firefox
image::firefox-import-custom-certificate.png[]

. Select the appropriate certificate from your computer.
