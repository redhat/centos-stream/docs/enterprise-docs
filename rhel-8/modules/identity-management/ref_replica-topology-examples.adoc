:_mod-docs-content-type: REFERENCE

[id="replica-topology-examples_{context}"]
= Replica topology examples

[role="_abstract"]
You can create a reliable replica topology by using one of the following examples.

[#{context}-replica-topology-example-1-fin]
.Replica topology with four data centers, each with four servers that are connected with replication agreements
//image::replica_topology_example_1.png[] -- this image was replaced with 64_RHEL_IdM_0120_2.2.png on Jan 29th
image::replica-topology-example-1.png[A diagram showing 4 data centers - Geo 1 through 4. Each data center has four servers connected to each other with replication agreements. There are also replication agreements connecting two servers from Geo 1 to two servers in Geo 2. This pattern continues with two servers in Geo 2 connected to two servers in Geo 3 and two servers in Geo 3 connected to Geo 4. This connects each data center so each server is at most 3 hops away from another Geo.]
//{imagesdir}
{sp} +
{sp} +

[#{context}-replica-topology-example-2-fin]
.Replica topology with three data centers, each with a different number of servers that are all interconnected through replication agreements
//image::replica_topology_example_2.png[] -- this image was replaced with 64_RHEL_IdM_0120_2.3.png on Jan 29th
image::replica-topology-example-2.png[A diagram showing 3 data centers: Geo 1 has 5 servers each connected to the other - Geo 2 has two servers connected to each other - Geo 3 has 3 servers connected in a triangle. There are 2 connections from each Geo connecting two of its servers to 2 servers in the next Geo.]
