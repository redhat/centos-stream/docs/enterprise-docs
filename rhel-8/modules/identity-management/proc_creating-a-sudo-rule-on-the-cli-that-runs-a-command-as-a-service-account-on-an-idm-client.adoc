:_mod-docs-content-type: PROCEDURE
[id="proc_creating-a-sudo-rule-on-the-cli-that-runs-a-command-as-a-service-account-on-an-idm-client_{context}"]
= Creating a sudo rule on the CLI that runs a command as a service account on an IdM client

[role="_abstract"]
In IdM, you can configure a `sudo` rule with a _RunAs alias_ to run a `sudo` command as another user or group. For example, you might have an IdM client that hosts a database application, and you need to run commands as the local service account that corresponds to that application.

Use this example to create a `sudo` rule on the command line called `run_third-party-app_report` to allow the `idm_user` account to run the `/opt/third-party-app/bin/report` command as the `thirdpartyapp` service account on the `idmclient` host.

.Prerequisites

* You are logged in as IdM administrator.

* You have created a user account for `idm_user` in IdM and unlocked the account by creating a password for the user. For details on adding a new IdM user using the CLI, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/managing-user-accounts-using-the-command-line_configuring-and-managing-idm#adding-users-using-the-command-line_managing-idm-users-using-the-command-line[Adding users using the command line].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_idm_users_groups_hosts_and_access_control_rules/managing-user-accounts-using-the-command-line_managing-users-groups-hosts#adding-users-using-the-command-line_managing-idm-users-using-the-command-line[Adding users using the command line].
endif::[]

* No local `idm_user` account is present on the `idmclient` host. The `idm_user` user is not listed in the local `/etc/passwd` file.

* You have a custom application named `third-party-app` installed on the `idmclient` host.

* The `report` command for the `third-party-app` application is installed in the `/opt/third-party-app/bin/report` directory.

* You have created a local service account named `thirdpartyapp` to execute commands for the `third-party-app` application.


.Procedure

. Retrieve a Kerberos ticket as the IdM `admin`.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *kinit admin*
....

. Add the `/opt/third-party-app/bin/report` command to the IdM database of `sudo` commands:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudocmd-add /opt/third-party-app/bin/report*
----------------------------------------------------
Added Sudo Command "/opt/third-party-app/bin/report"
----------------------------------------------------
  Sudo Command: /opt/third-party-app/bin/report
....

. Create a `sudo` rule named `run_third-party-app_report`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add _run_third-party-app_report_*
--------------------------------------------
Added Sudo Rule "run_third-party-app_report"
--------------------------------------------
  Rule name: run_third-party-app_report
  Enabled: TRUE
....

. Use the `--users=_<user>_` option to specify the RunAs user for the `sudorule-add-runasuser` command:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add-runasuser _run_third-party-app_report_ --users=_thirdpartyapp_*
  Rule name: run_third-party-app_report
  Enabled: TRUE
  RunAs External User: thirdpartyapp
-------------------------
Number of members added 1
-------------------------
....
+
The user (or group specified with the `--groups=*` option) can be external to IdM, such as a local service account or an Active Directory user. Do not add a `%` prefix for group names.

. Add the `/opt/third-party-app/bin/report` command to the `run_third-party-app_report` rule:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add-allow-command run_third-party-app_report --sudocmds '/opt/third-party-app/bin/report'*
Rule name: run_third-party-app_report
Enabled: TRUE
Sudo Allow Commands: /opt/third-party-app/bin/report
RunAs External User: thirdpartyapp
-------------------------
Number of members added 1
-------------------------
....

. Apply the `run_third-party-app_report` rule to the IdM `idmclient` host:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add-host run_third-party-app_report --hosts idmclient.idm.example.com*
Rule name: run_third-party-app_report
Enabled: TRUE
Hosts: idmclient.idm.example.com
Sudo Allow Commands: /opt/third-party-app/bin/report
RunAs External User: thirdpartyapp
-------------------------
Number of members added 1
-------------------------
....

. Add the `idm_user` account to the `run_third-party-app_report` rule:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@idmclient ~]# *ipa sudorule-add-user run_third-party-app_report --users idm_user*
Rule name: run_third-party-app_report
Enabled: TRUE
Users: idm_user
Hosts: idmclient.idm.example.com
Sudo Allow Commands: /opt/third-party-app/bin/report
RunAs External User: thirdpartyapp
-------------------------
Number of members added 1
....

[NOTE]
====
Propagating the changes from the server to the client can take a few minutes.
====


.Verification

. Log in to the `idmclient` host as the `idm_user` account.

. Test the new sudo rule:
.. Display which `sudo` rules the `idm_user` account is allowed to perform.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm_user@idmclient ~]$ *sudo -l*
Matching Defaults entries for idm_user@idm.example.com on idmclient:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin,
    env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS",
    env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE",
    env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES",
    env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE",
    env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY KRB5CCNAME",
    secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User idm_user@idm.example.com may run the following commands on idmclient:
    *(thirdpartyapp) /opt/third-party-app/bin/report*
....

.. Run the `report` command as the `thirdpartyapp` service account.
+
[literal,subs="+quotes,attributes,verbatim"]
....
[idm_user@idmclient ~]$ *sudo -u _thirdpartyapp_ /opt/third-party-app/bin/report*
[sudo] password for idm_user@idm.example.com:
Executing report...
Report successful.
....
