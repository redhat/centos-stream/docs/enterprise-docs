[id="recommended-configuration-for-rhel-clients_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Recommended configuration for RHEL clients

[NOTE]
====

The client configuration described is only supported for RHEL 6.1 and later and RHEL 5.7 later, which support the latest versions of SSSD and the [command]`ipa-client` package.
Older versions of RHEL can be configured as described in xref:alternative-supported-configuration_{context}[Alternative supported configuration].

====

The System Security Services Daemon (SSSD) in {RHEL} (RHEL) uses special PAM and NSS libraries, `pam_sss` and `nss_sss`. Using these libraries, SSSD can integrate very closely with {IPA} (IdM) and benefit from its full authentication and identity features. SSSD has a number of useful features, such as caching identity information so that users can log in even if the connection to the central server is lost.

Unlike generic LDAP directory services that use the `pam_ldap` and `nss_ldap` libraries, SSSD establishes relationships between identity and authentication information by defining _domains_. A domain in SSSD defines the following back end functions:

* Authentication
* Identity lookups
* Access
* Password changes

The SSSD domain is then configured to use a _provider_ to supply the information for any one, or all, of these functions. The domain configuration always requires an _identity_ provider. The other three providers are optional; if an authentication, access, or password provider is not defined, then the identity provider is used for that function.

SSSD can use IdM for all of its back end functions. This is the ideal configuration because it provides the full range of IdM functionality, unlike generic LDAP identity providers or Kerberos authentication. For example, during daily operation, SSSD enforces host-based access control rules and security features in IdM.


[id="clients-and-sssd-with-an-idm-back-end_{context}"]
.Clients and SSSD with an IdM back end

image::migr-sssd2.png[]

The [command]`ipa-client-install` script automatically configures SSSD to use IdM for all its back end services, so that RHEL clients are set up with the recommended configuration by default.


.Additional information

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_authentication_and_authorization_in_rhel/understanding-sssd-and-its-benefits_configuring-authentication-and-authorization-in-rhel[Understanding SSSD and its benefits]
