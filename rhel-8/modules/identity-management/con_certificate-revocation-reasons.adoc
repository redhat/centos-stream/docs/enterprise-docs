:_mod-docs-content-type: CONCEPT
[id="certificate-revocation-reasons_{context}"]
= Certificate revocation reasons

////
If you need to invalidate the certificate before its expiration date, you can revoke it. To revoke a certificate using:

* The IdM web UI, see xref:certificate-idm-revoke-ui[if using insert text]

* The command line, see xref:certificate-idm-revoke-cli[if using insert text]
////

[role="_abstract"]
A revoked certificate is invalid and cannot be used for authentication. All revocations are permanent, except for reason 6: `Certificate Hold`.

The default revocation reason is 0: `unspecified`.

.Revocation Reasons

[cols="1,4,8"]
[options="header"]
|===
|ID|Reason|Explanation
|0|Unspecified|
|1|Key Compromised|The key that issued the certificate is no longer trusted.


Possible causes: lost token, improperly accessed file.
|2|CA Compromised|The CA that issued the certificate is no longer trusted.
|3|Affiliation Changed|Possible causes:


* A person has left the company or moved to another department.

* A host or service is being retired.
|4|Superseded|A newer certificate has replaced the current certificate.
|5|Cessation of Operation|The host or service is being decommissioned.
|6|Certificate Hold|The certificate is temporarily revoked. You can restore the certificate later.
|8|Remove from CRL|The certificate is not included in the certificate revocation list (CRL).
|9|Privilege Withdrawn|The user, host, or service is no longer permitted to use the certificate.
|10|Attribute Authority (AA) Compromise|The AA certificate is no longer trusted.
|===
