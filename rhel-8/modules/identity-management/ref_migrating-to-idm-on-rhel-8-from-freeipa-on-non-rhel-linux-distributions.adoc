:_mod-docs-content-type: REFERENCE

[id="ref_migrating-to-idm-on-rhel-8-from-freeipa-on-non-rhel-linux-distributions_{context}"]
= Migrating to IdM on RHEL 8 from FreeIPA on non-RHEL Linux distributions

[role="_abstract"]
To migrate a FreeIPA deployment on a non-RHEL Linux distribution to an Identity Management (IdM) deployment on RHEL 8 servers, you must first add a new RHEL 8 IdM Certificate Authority (CA) replica to your existing FreeIPA environment, transfer certificate-related roles to it, and then retire the non-RHEL FreeIPA servers.

[WARNING]
====
Performing an in-place conversion of a non-RHEL FreeIPA server to a RHEL 8 IdM server using the Convert2RHEL tool is not supported.
====

To perform the migration, follow the same procedure as link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/migrating_to_identity_management_on_rhel_8/migrate-7-to-8_migrating[Migrating your IdM environment from RHEL 7 servers to RHEL 8 servers], with your non-RHEL FreeIPA CA replica acting as the RHEL 7 server:

. Configure a RHEL 8 server and add it as an IdM replica to your current FreeIPA environment on the non-RHEL Linux distribution. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/migrating_to_identity_management_on_rhel_8/migrate-7-to-8_migrating#install-replica_migrate-7-to-8[Installing the RHEL 8 Replica].

. Make the RHEL 8 replica the certificate authority (CA) renewal server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/migrating_to_identity_management_on_rhel_8/migrate-7-to-8_migrating#assigning-the-ca-renewal-server-role-to-the-rhel-8-idm-server_migrate-7-to-8[Assigning the CA renewal server role to the RHEL 8 IdM server].

. Stop generating the certificate revocation list (CRL) on the non-RHEL server and redirect CRL requests to the RHEL 8 replica. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/migrating_to_identity_management_on_rhel_8/migrate-7-to-8_migrating#stopping-crl-generation-on-rhel7-IdM-CA-server_migrate-7-to-8[Stopping CRL generation on a RHEL 7 IdM CA server].

. Start generating the CRL on the RHEL 8 server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/migrating_to_identity_management_on_rhel_8/migrate-7-to-8_migrating#starting-crl-generation-on-the-new-rhel-8-idm-ca-server_migrate-7-to-8[Starting CRL generation on the new RHEL 8 IdM CA server].

. Stop and decommission the original non-RHEL FreeIPA CA renewal server. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/migrating_to_identity_management_on_rhel_8/migrate-7-to-8_migrating#stop-decommission-server_migrate-7-to-8[Stopping and decommissioning the RHEL 7 server].



[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/migrating_to_identity_management_on_rhel_8/migrate-7-to-8_migrating[Migrating your IdM environment from RHEL 7 servers to RHEL 8 servers]
