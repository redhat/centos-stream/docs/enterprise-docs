:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="deleting-an-automember-rule-using-idm-cli_{context}"]
= Deleting an automember rule using IdM CLI

[role="_abstract"]
Follow this procedure to delete an automember rule using the IdM CLI.

Deleting an automember rule also deletes all conditions associated with the rule. To remove only specific conditions from a rule, see xref:removing-a-condition-from-an-automember-rule-using-idm-cli_{context}[Removing a condition from an automember rule using IdM CLI].

.Prerequisites

* You must be logged in as the administrator. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/logging-in-to-ipa-from-the-command-line_configuring-and-managing-idm#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/logging-in-to-ipa-from-the-command-line_accessing-idm-services#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line[Using kinit to log in to IdM manually].
endif::[]


.Procedure

. Enter the [command]`ipa automember-del` command.
. When prompted, specify:

** *Automember rule*. This is the rule you want to delete.
** *Grouping rule*. This specifies whether the rule you want to delete is for a user group or a host group. Enter *group* or *hostgroup*.
