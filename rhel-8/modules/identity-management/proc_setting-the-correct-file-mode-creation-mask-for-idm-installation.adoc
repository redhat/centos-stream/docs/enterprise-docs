:_module-type: PROCEDURE

[id="proc_setting-the-correct-file-mode-creation-mask-for-idm-installation_{context}"]
= Setting the correct file mode creation mask for IdM installation

[role="_abstract"]
The Identity Management (IdM) installation process requires that the file mode creation mask (`umask`) is set to `0022` for the `root` account. This allows users other than `root` to read files created during the installation. If a different `umask` is set, the installation of an IdM server will display a warning. If you continue with the installation, some functions of the server will not perform properly. For example, you will be unable to install an IdM replica from this server. After the installation, you can set the `umask` back to its original value.

.Prerequisites

* You have `root` privileges.

.Procedure

. Optional: Display the current `umask`:
+
----
# umask
0027
----
+
. Set the `umask` to `0022`:
+
----
# umask 0022
----
+
. Optional: After the IdM installation is complete, set the `umask` back to its original value:
+
----
# umask 0027
----
