:_mod-docs-content-type: PROCEDURE
[id="deleting-self-service-rules-using-the-idm-web-ui_{context}"]
= Deleting self-service rules using the IdM Web UI

[role="_abstract"]
Follow this procedure to delete self-service access rules in IdM using the web interface (IdM Web UI).

.Prerequisites

* Administrator privileges for managing IdM or the *User Administrator* role.
* You are logged-in to the IdM Web UI. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/accessing-the-ipa-web-ui-in-a-web-browser_configuring-and-managing-idm[Accessing the IdM Web UI in a web browser].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/accessing_identity_management_services/accessing-the-ipa-web-ui-in-a-web-browser_accessing-idm-services[Accessing the IdM Web UI in a web browser].
endif::[]


.Procedure

. Open the *Role-Based Access Control* submenu in the *IPA Server* tab and select *Self Service Permissions*.
. Select the check box next to the rule you want to delete, then click on the *Delete* button on the right of the list.
+
image:idm-selfservice_delete.png[Deleting a self-service rule]

. A dialog opens, click on *Delete* to confirm.
