:_mod-docs-content-type: CONCEPT

[id="what-is-a-certificate-profile_{context}"]
= What is a certificate profile?

[role="_abstract"]
You can use certificate profiles to determine the content of certificates, as well as constraints for issuing the certificates, such as the following:

* The signing algorithm to use to encipher the certificate signing request.
* The default validity of the certificate.
* The revocation reasons that can be used to revoke a certificate.
* If the common name of the principal is copied to the subject alternative name field.
* The features and extensions that should be present on the certificate.

A single certificate profile is associated with issuing a particular type of certificate. You can define different certificate profiles for users, services, and hosts in IdM. IdM includes the following certificate profiles by default:

* `caIPAserviceCert`
* `IECUserRoles`
* `KDCs_PKINIT_Certs` (used internally)

In addition, you can create and import custom profiles, which allow you to issue certificates for specific purposes. For example, you can restrict the use of a particular profile to only one user or one group, preventing other users and groups from using that profile to issue a certificate for authentication. To create custom certificate profiles, use the `ipa certprofile` command.

[role="_additional-resources"]
.Additional resources
* See the `ipa help certprofile` command.
