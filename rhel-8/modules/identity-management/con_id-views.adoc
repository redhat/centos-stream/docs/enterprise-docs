:_mod-docs-content-type: CONCEPT

[id="id-views_{context}"]
= ID views

[role="_abstract"]
An ID view in {IPA} (IdM) is an IdM client-side view specifying the following information:

* New values for centrally defined POSIX user or group attributes
* The client host or hosts on which the new values apply.

An ID view contains one or more overrides. An override is a specific replacement of a centrally defined POSIX attribute value.

You can only define an ID view for an IdM client centrally on IdM servers. You cannot configure client-side overrides for an IdM client locally.

For example, you can use ID views to achieve the following goals:

* Define different attribute values for different environments. For example, you can allow the IdM administrator or another IdM user to have different home directories on different IdM clients: you can configure `/home/encrypted/username` to be this user's home directory on one IdM client and `/dropbox/username` on another client. Using ID views in this situation is convenient as alternatively, for example, changing `fallback_homedir`, `override_homedir` or other home directory variables in the client's `/etc/sssd/sssd.conf` file would affect all users. See xref:adding-an-ID-view-to-override-an-IdM-user-home-directory-on-an-IdM-client_{context}[Adding an ID view to override an IdM user home directory on an IdM client] for an example procedure.

* Replace a previously generated attribute value with a different value, such as overriding a user's UID. This ability can be useful when you want to achieve a system-wide change that would otherwise be difficult to do on the LDAP side, for example make 1009 the UID of an IdM user. IdM ID ranges, which are used to generate an IdM user UID, never start as low as 1000 or even 10000. If a reason exists for an IdM user to impersonate a local user with UID 1009 on all IdM clients, you can use ID views to override the UID of this IdM user that was generated when the user was created in IdM.

[IMPORTANT]
====
You can only apply ID views to IdM clients, not to IdM servers.
====

[role="_additional-resources"]
.Additional resources
ifdef::c-m-idm[]
* xref:assembly_using-id-views-for-active-directory-users_configuring-and-managing-idm[Using ID views for Active Directory users]
endif::[]
ifdef::managing-users-groups-hosts[]
* xref:assembly_using-id-views-for-active-directory-users_managing-users-groups-hosts[Using ID views for Active Directory users]
endif::[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_authentication_and_authorization_in_rhel/assembly_sssd-client-side-view_configuring-authentication-and-authorization-in-rhel[SSSD Client-side Views]
