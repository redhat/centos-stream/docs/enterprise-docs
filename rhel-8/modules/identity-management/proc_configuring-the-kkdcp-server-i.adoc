:_mod-docs-content-type: PROCEDURE

// Module included in the following assemblies:
//
// assembly_using-the-kdc-proxy-in-idm.adoc

[id="proc_configuring-the-kkdcp-server-i_{context}"]
= Configuring the KKDCP server I

[role="_abstract"]
With the following configuration, you can enable TCP to be used as the transport protocol between the IdM KKDCP and the Active Directory (AD) realm, where multiple Kerberos servers are used.

.Prerequisites

* You have `root` access.

.Procedure

. Set the `use_dns` parameter in the `[global]` section of the `/etc/ipa/kdcproxy/kdcproxy.conf` file to *false*.
+
[literal,subs="+quotes,verbatim"]
....
[global]
use_dns = false
....

. Put the proxied realm information into the `/etc/ipa/kdcproxy/kdcproxy.conf` file. For example, for the [AD._EXAMPLE.COM_] realm with proxy list the realm configuration parameters as follows:
+
[literal,subs="+quotes"]
....
[AD._EXAMPLE.COM_]
kerberos = kerberos+tcp://1.2.3.4:88 kerberos+tcp://5.6.7.8:88
kpasswd = kpasswd+tcp://1.2.3.4:464 kpasswd+tcp://5.6.7.8:464
....
+
[IMPORTANT]
====
The realm configuration parameters must list multiple servers separated by a space, as opposed to `/etc/krb5.conf` and `kdc.conf`, in which certain options may be specified multiple times.
====

. Restart {IPA} (IdM) services:
+
[literal,subs="+quotes"]
....
# ipactl restart
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/3347361[Configure IPA server as a KDC Proxy for AD Kerberos communication] (Red Hat Knowledgebase)
