:_mod-docs-content-type: PROCEDURE
[id="setting-the-parameters-for-a-deployment-with-external-dns-and-an-integrated-ca-as-the-root-ca_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Setting the parameters for a deployment with external DNS and an integrated CA as the root CA

Complete this procedure to configure the inventory file for installing an IdM server with an integrated CA as the root CA in an environment that uses an external DNS solution.

NOTE: The inventory file in this procedure uses the `INI` format. You can, alternatively, use the `YAML` or `JSON` formats.

.Procedure
. Create a `~/MyPlaybooks/` directory:
+
[literal,subs="+quotes,attributes"]
....
$ mkdir MyPlaybooks
....

. Create a `~/MyPlaybooks/inventory` file.

. Open the inventory file for editing. Specify the fully-qualified domain names (`FQDN`) of the host you want to use as an IdM server. Ensure that the `FQDN` meets the following criteria:

+
* Only alphanumeric characters and hyphens (-) are allowed. For example, underscores are not allowed and can cause DNS failures.
* The host name must be all lower-case.

+
. Specify the IdM domain and realm information.

+
. Make sure that the `ipaserver_setup_dns` option is set to `no` or that it is absent.

+
. Specify the passwords for `admin` and for the `Directory Manager`. Use the Ansible Vault to store the password, and reference the Vault file from the playbook file. Alternatively and less securely, specify the passwords directly in the inventory file.

+
. Optional: Specify a custom `firewalld` zone to be used by the IdM server. If you do not set a custom zone, IdM will add its services to the default `firewalld` zone. The predefined default zone is `public`.
+
IMPORTANT: The specified `firewalld` zone must exist and be permanent.

+
.Example of an inventory file with the required server information (excluding the passwords)
[literal,subs="+quotes,attributes"]
....
[ipaserver]
server.idm.example.com

[ipaserver:vars]
ipaserver_domain=idm.example.com
ipaserver_realm=IDM.EXAMPLE.COM
ipaserver_setup_dns=no
[...]
....


+
.Example of an inventory file with the required server information (including the passwords)
[literal,subs="+quotes,attributes"]
....
[ipaserver]
server.idm.example.com

[ipaserver:vars]
ipaserver_domain=idm.example.com
ipaserver_realm=IDM.EXAMPLE.COM
ipaserver_setup_dns=no
ipaadmin_password=MySecretPassword123
ipadm_password=MySecretPassword234

[...]
....

+
.Example of an inventory file with a custom `firewalld` zone
[literal,subs="+quotes,attributes"]
....
[ipaserver]
server.idm.example.com

[ipaserver:vars]
ipaserver_domain=idm.example.com
ipaserver_realm=IDM.EXAMPLE.COM
ipaserver_setup_dns=no
ipaadmin_password=MySecretPassword123
ipadm_password=MySecretPassword234
*ipaserver_firewalld_zone=_custom zone_*
....

+
.Example playbook to set up an IdM server using admin and Directory Manager passwords stored in an Ansible Vault file
[literal,subs="+quotes,attributes"]
....
---
- name: Playbook to configure IPA server
  hosts: ipaserver
  become: true
  vars_files:
  - playbook_sensitive_data.yml

  roles:
  - role: ipaserver
    state: present
....


+
.Example playbook to set up an IdM server using admin and Directory Manager passwords from an inventory file
[literal,subs="+quotes,attributes"]
....
---
- name: Playbook to configure IPA server
  hosts: ipaserver
  become: true

  roles:
  - role: ipaserver
    state: present
....

[role="_additional-resources"]
.Additional resources
* man `ipa-server-install(1)`
* `/usr/share/doc/ansible-freeipa/README-server.md`