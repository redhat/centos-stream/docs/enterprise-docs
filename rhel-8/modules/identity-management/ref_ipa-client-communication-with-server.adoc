:_mod-docs-content-type: REFERENCE
[id='ipa-client-communication-with-server_{context}']
= IdM client's communications with the server during post-installation deployment

[role="_abstract"]
The client side of {IPA} (IdM) framework is implemented with two different applications:

* The `ipa` command-line interface (CLI)
* (_optional_) the browser-based Web UI

xref:tab-ipa-operations_{context}[CLI post-installation operations] shows the operations performed by the CLI during an IdM client post-installation deployment. xref:tab-browser-operations_{context}[Web UI post-installation operations] shows the operations performed by the Web UI during an IdM client post-installation deployment.

[id='tab-ipa-operations_{context}']
.CLI post-installation operations
[cols="3,2,3"]
|===
|Operation|Protocol used|Purpose

|DNS resolution against the DNS resolvers configured on the client system
|DNS
|To discover the IP addresses of IdM servers

|Requests to ports 88 (TCP/TCP6 and UDP/UDP6) and 464 (TCP/TCP6 and UDP/UDP6) on an IdM replica
|Kerberos
|To obtain a Kerberos ticket; change a Kerberos password; authenticate to the IdM Web UI

|JSON-RPC calls to the IdM Apache-based web-service on discovered or configured IdM servers
|HTTPS
|any [command]`ipa` utility usage
|===

//(for example) to look up the CA configuration; add the host’s SSH public key
////
|JSON-RPC calls to the IdM Apache-based web-service on discovered or configured IdM servers
|HTTPS
|To register the client and get a certificate if needed
|===

The command line utility, the `ipa` tool, performs the following operations:

* DNS resolution, using standard DNS protocol, against the DNS resolvers configured on the client system
* Kerberos protocol operations to the Kerberos Key Distribution Center
* JSON-RPC calls to discovered or configured {IPA} servers using the HTTPS protocol to the {IPA} Apache-based web-service
////
[id='tab-browser-operations_{context}']
.Web UI post-installation operations
[cols="3,2,3"]
|===
|Operation|Protocol used|Purpose

|JSON-RPC calls to the IdM Apache-based web-service on discovered or configured IdM servers
|HTTPS
|To retrieve the IdM Web UI pages
|===

//The browser-based Web UI performs its operations against a chosen IdM server using the HTTPS protocol.

.Additional resources
* xref:sssd-deployment-operations_{context}[SSSD communication patterns] for more information about how the [systemitem]`SSSD` daemon communicates with the services available on the IdM and Active Directory servers.
* xref:certmonger-deployment-operations_{context}[Certmonger communication patterns] for more information about how the [systemitem]`certmonger` daemon communicates with the services available on the IdM and Active Directory servers.
