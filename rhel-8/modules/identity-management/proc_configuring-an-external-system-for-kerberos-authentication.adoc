:_mod-docs-content-type: PROCEDURE
//:parent-context: {context}
// Module included in the following assemblies:
//
// logging-in-to-ipa-from-the-command-line.adoc
// logging-in-to-ipa-in-the-web-ui-using-a-kerberos-ticket.adoc

[id='configuring-an-external-system-for-kerberos-authentication-{context}']
= Configuring an external system for Kerberos authentication

[role="_abstract"]
Follow this procedure to configure an external system so that {IPA} (IdM) users can log in to IdM from the external system using their Kerberos credentials.

Enabling Kerberos authentication on external systems is especially useful when your infrastructure includes multiple realms or overlapping domains. It is also useful if the system has not been enrolled into any IdM domain through `ipa-client-install`.

To enable Kerberos authentication to IdM from a system that is not a member of the IdM domain, define an IdM-specific Kerberos configuration file on the external system.

.Prerequisites

* The `krb5-workstation` package is installed on the external system.

+
To find out whether the package is installed, use the following CLI command:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *{PackageManagerCommand} list installed krb5-workstation*
Installed Packages
krb5-workstation.x86_64    1.16.1-19.el8     @BaseOS
....

.Procedure

. Copy the `/etc/krb5.conf` file from the IdM server to the external system. For example:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *scp /etc/krb5.conf root@_externalsystem.example.com_:/etc/krb5_ipa.conf*
....
+
[WARNING]
====

Do not overwrite the existing `krb5.conf` file on the external system.

====

. On the external system, set the terminal session to use the copied IdM Kerberos configuration file:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *export KRB5_CONFIG=/etc/krb5_ipa.conf*
....

+
The `KRB5_CONFIG` variable exists only temporarily until you log out. To prevent this loss, export the variable with a different file name.

. Copy the Kerberos configuration snippets from the `/etc/krb5.conf.d/` directory to the external system.


ifeval::["{context}" == "login-web-ui-krb"]
. Configure the browser on the external system, as described in xref:configuring-the-browser-for-kerberos-authentication-{context}[Configuring the browser for Kerberos authentication].
endif::[]

Users on the external system can now use the `kinit` utility to authenticate against the IdM server.

//:context: {parent-context}
