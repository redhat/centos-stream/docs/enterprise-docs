[id="access-control-instructions-in-idm_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Access control instructions in IdM

The {IPA} (IdM) access control structure is based on the 389 Directory Server access control. By using access control instructions (ACIs), you can grant or deny specific IdM users access over other entries. All entries, including IdM users, are stored in LDAP.

An ACI has three parts:

Actor::
+
The entity that is being granted permission to do something. In LDAP access control models, you can, for example, specify that the ACI rule is applied only when a user binds to the directory using their distinguished name (DN). Such a specification is called the __bind rule__: it defines who the user is and can optionally require other limits on the bind attempt, such as restricting attempts to a certain time of day or a certain machine.

Target::
+
The entry that the actor is allowed to perform operations on.

Operation type::
+
Determines what kinds of actions the actor is allowed to perform. The most common operations are add, delete, write, read, and search. In IdM, the read and search rights of a non-administrative user are limited, and even more so in the IdM Web UI than the IdM CLI.  

When an LDAP operation is attempted, the following occurs:

. The IdM client sends user credentials to an IdM server as part of the bind operation.
. The IdM server DS checks the user credentials.
. The IdM server DS checks the user account to see if the user has a permission to perform the requested operation.
