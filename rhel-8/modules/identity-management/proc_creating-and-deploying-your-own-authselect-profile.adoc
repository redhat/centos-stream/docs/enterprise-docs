:_mod-docs-content-type: PROCEDURE
[id='creating-and-deploying-your-own-authselect-profile_{context}']
= Creating and deploying your own authselect profile

[role="_abstract"]
As a system administrator, you can create and deploy a custom profile by making a customized copy of one of the default profiles.

This is particularly useful if
ifdef::configuring-authentication-and-authorization-in-rhel[]
xref:modifying-a-ready-made-authselect-profile_configuring-user-authentication-using-authselect[Modifying a ready-made authselect profile]
endif::[]
ifndef::configuring-authentication-and-authorization-in-rhel[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_authentication_and_authorization_in_rhel/configuring-user-authentication-using-authselect_configuring-authentication-and-authorization-in-rhel#modifying-a-ready-made-authselect-profile_configuring-user-authentication-using-authselect[Modifying a ready-made authselect profile]
endif::[]
is not enough for your needs. When you deploy a custom profile, the profile is applied to every user logging into the given host.
////
.Prerequisites

Make sure that the [filename]`/etc/krb5.conf` and [filename]`/etc/sssd/sssd.conf` files are configured correctly before creating and deploying your custom [systemitem]`authselect` profile.
////

.Procedure

. Create your custom profile by using the [command]`authselect create-profile` command. For example, to create a custom profile called [replaceable]`user-profile` based on the ready-made [systemitem]`sssd` profile but one in which you can configure the items in the [filename]`/etc/nsswitch.conf` file yourself:
+
[literal,subs="+quotes,attributes"]
....
# [command]`authselect create-profile` [replaceable]`user-profile` -b [systemitem]`sssd` [option]`--symlink-meta` [option]`--symlink-pam`
New profile was created at /etc/authselect/custom/user-profile
....
+
WARNING: If you are planning to modify `/etc/authselect/custom/user-profile/{password-auth,system-auth,fingerprint-auth,smartcard-auth,postlogin}`, then enter the command above without the `--symlink-pam` option. This is to ensure that the modification persists during the upgrade of `authselect-libs`.

+
Including the [option]`--symlink-pam` option in the command means that PAM templates will be symbolic links to the origin profile files instead of their copy; including the [option]`--symlink-meta` option means that meta files, such as README and REQUIREMENTS will be symbolic links to the origin profile files instead of their copy. This ensures that all future updates to the PAM templates and meta files in the original profile will be reflected in your custom profile, too.

+
The command creates a copy of the [filename]`/etc/nsswitch.conf` file in the [filename]`/etc/authselect/custom/user-profile/` directory.

. Configure the [filename]`/etc/authselect/custom/user-profile/nsswitch.conf` file.
+
. Select the custom profile by running the [command]`authselect select` command, and adding `custom/_name_of_the_profile_` as a parameter. For example, to select the [replaceable]`user-profile` profile:
+
[literal,subs="+quotes,attributes"]
....
# [command]`authselect select` custom/[replaceable]`user-profile`
....
+
Selecting the [replaceable]`user-profile` profile for your machine means that if the [systemitem]`sssd` profile is subsequently updated by {RH}, you will benefit from all the updates with the exception of updates made to the [filename]`/etc/nsswitch.conf` file.
+
.Creating a profile
====
The following procedure shows how to create a profile based on the [systemitem]`sssd` profile which only consults the local static table lookup for hostnames in the [filename]`/etc/hosts` file, not in the [systemitem]`dns` or [systemitem]`myhostname` databases.

. Edit the [filename]`/etc/nsswitch.conf` file by editing the following line:
+
----
hosts:      files
----

. Create a custom profile based on [systemitem]`sssd` that excludes changes to [filename]`/etc/nsswitch.conf`:
+
[literal,subs="+quotes,attributes"]
....
# [command]`authselect create-profile` [replaceable]`user-profile` -b [systemitem]`sssd` --symlink-meta --symlink-pam
....

. Select the profile:
+
[literal,subs="+quotes,attributes"]
....
# [command]`authselect select` custom/[replaceable]`user-profile`
....

. Optional: Check that selecting the custom profile has
** created the [filename]`/etc/pam.d/system-auth` file according to the chosen [systemitem]`sssd` profile
** left the configuration in the [filename]`/etc/nsswitch.conf` unchanged:
+
....
hosts:      files
....
+
NOTE: Running [command]`authselect select` [systemitem]`sssd` would, in contrast, result in `hosts:      files dns myhostname`
====

[role="_additional-resources"]
.Additional Resources
ifdef::configuring-authentication-and-authorization-in-rhel[]
* xref:what-is-authselect-used-for_configuring-user-authentication-using-authselect[What is authselect used for]
endif::[]
ifndef::configuring-authentication-and-authorization-in-rhel[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_authentication_and_authorization_in_rhel/configuring-user-authentication-using-authselect_configuring-authentication-and-authorization-in-rhel#what-is-authselect-used-for_configuring-user-authentication-using-authselect[What is authselect used for]

endif::[]
