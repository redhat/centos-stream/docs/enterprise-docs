:_mod-docs-content-type: PROCEDURE

[id="using-ansible-to-restore-ssl-certificates-for-idm-users-hosts-and-services_{context}"]
= Using Ansible to restore SSL certificates for IdM users, hosts, and services

You can use the `ansible-freeipa` `ipacert` module to restore a revoked SSL certificate previously used by an {IPA} (IdM) user, host or a service to authenticate to IdM.

NOTE: You can only restore a certificate that was put on hold. You may have put it on hold because, for example, you were not sure if the private key had been lost. However, now you have recovered the key and as you are certain that no-one has accessed it in the meantime, you want to reinstate the certificate.

Complete this procedure to use an Ansible playbook to release a certificate for a service enrolled into IdM from hold. This example describes how to release a certificate for an HTTP service from hold.

.Prerequisites
* On the control node:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** You have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
endif::[]
** You have stored your `ipaadmin_password` in the *secret.yml* Ansible vault.
* Your IdM deployment has an integrated CA.
* You have obtained the serial number of the certificate, for example by entering the `openssl x509 -noout -text -in path/to/certificate` command. In this example, the certificate serial number is *123456789*.

.Procedure

. Create your Ansible playbook file *restore-certificate.yml* with the following content:

+
[literal,subs="+quotes,attributes,verbatim"]
----
---
- name: Playbook to restore a certificate
  hosts: ipaserver
  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml

  tasks:
  - name: Restore a certificate for a web service
    ipacert:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *serial_number: 123456789*
      *state: released*
----

+
. Run the playbook:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i <path_to_inventory_directory>/hosts <path_to_playbooks_directory>/restore-certificate.yml*
....



[role="_additional-resources"]
.Additional resources
* link:https://github.com/freeipa/ansible-freeipa/blob/master/README-cert.md[The cert module in `ansible-freeipa` upstream docs]
