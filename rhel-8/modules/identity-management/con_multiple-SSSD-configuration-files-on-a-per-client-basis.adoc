:_mod-docs-content-type: CONCEPT
[id="multiple-SSSD-configuration-files-on-a-per-client-basis_{context}"]
= Multiple SSSD configuration files on a per-client basis

[role="_abstract"]
The default configuration file for SSSD is `/etc/sssd/sssd.conf`. Apart from this file, SSSD can read its configuration from all `*.conf` files in the `/etc/sssd/conf.d/` directory.

This combination allows you to use the default `/etc/sssd/sssd.conf` file on all clients and add additional settings in further configuration files to extend the functionality individually on a per-client basis.

[discrete]
== How SSSD processes the configuration files
SSSD reads the configuration files in this order:

. The primary `/etc/sssd/sssd.conf` file

. Other `*.conf` files in `/etc/sssd/conf.d/`, in alphabetical order

If the same parameter appears in multiple configuration files, SSSD uses the last read parameter.

[NOTE]
====

SSSD does not read hidden files (files starting with `.`) in the `conf.d` directory.

====
