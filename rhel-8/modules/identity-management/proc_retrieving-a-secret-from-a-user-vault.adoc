:_mod-docs-content-type: PROCEDURE
[id="retrieving-a-secret-from-a-user-vault_{context}"]
= Retrieving a secret from a user vault

[role="_abstract"]
As an Identity Management (IdM), you can retrieve a secret from your user private vault onto any IdM client to which you are logged in.

Follow this procedure to retrieve, as an IdM user named *idm_user*, a secret from the user private vault named *my_vault* onto *idm_client.idm.example.com*.

.Prerequisites

* *idm_user* is the owner of *my_vault*.

* *idm_user* has xref:storing-a-secret-in-a-user-vault_{context}[archived a secret in the vault].

* *my_vault* is a standard vault, which means that *idm_user* does not have to enter any password to access the contents of the vault.



.Procedure

. SSH to *idm_client* as *idm_user*:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ssh idm_user@idm_client.idm.example.com*
....


. Log in as `idm_user`:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *kinit user*
....

. Use the [command]`ipa vault-retrieve --out` command with the `--out` option to retrieve the contents of the vault and save them into the `secret_exported.txt` file.
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ipa vault-retrieve my_vault --out secret_exported.txt*
--------------------------------------
Retrieved data from vault "my_vault"
--------------------------------------
....
