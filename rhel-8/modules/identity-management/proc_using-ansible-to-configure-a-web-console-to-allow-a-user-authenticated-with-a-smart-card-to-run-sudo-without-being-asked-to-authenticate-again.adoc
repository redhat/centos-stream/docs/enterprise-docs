:_mod-docs-content-type: PROCEDURE

[id="proc_using-ansible-to-configure-a-web-console-to-allow-a-user-authenticated-with-a-smart-card-to-run-sudo-without-being-asked-to-authenticate-again_{context}"]
= Using Ansible to configure a web console to allow a user authenticated with a smart card to run sudo without being asked to authenticate again

After you have logged in to a user account on the RHEL web console, as an {IPA} (IdM) system administrator you might need to run commands with superuser privileges. You can use the link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/assembly_using-constrained-delegation-in-idm_configuring-and-managing-idm#con_constrained-delegation-in-identity-management_assembly_using-constrained-delegation-in-idm[constrained delegation] feature to run `sudo` on the system without being asked to authenticate again.

Follow this procedure to use the `ipaservicedelegationrule` and `ipaservicedelegationtarget` `ansible-freeipa` modules to configure a web console to use constrained delegation. In the example below, the web console session runs on the *myhost.idm.example.com* host.

.Prerequisites

* You have obtained an IdM `admin` ticket-granting ticket (TGT) by authenticating to the web console session with a smart card..
* The web console service has been enrolled into IdM.
* The *myhost.idm.example.com* host is present in IdM.
* You have link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_systems_using_the_rhel_8_web_console/configuring_single_sign_on_for_the_rhel_8_web_console_in_the_idm_domain_system-management-using-the-rhel-8-web-console#enabling-admin-sudo-access-to-domain-administrators-on-the-idm-server_configuring-single-sign-on-for-the-web-console-in-the-idm-domain[enabled `admin` `sudo` access to domain administrators on the IdM server].
* The web console has created an `S4U2Proxy` Kerberos ticket in the user session. To verify that this is the case, log in to the web console as an IdM user, open the `Terminal` page, and enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *klist*
Ticket cache: FILE:/run/user/1894000001/cockpit-session-3692.ccache
Default principal: user@IDM.EXAMPLE.COM

Valid starting     Expires            Service principal
*07/30/21 09:19:06  07/31/21 09:19:06  HTTP/myhost.idm.example.com@IDM.EXAMPLE.COM*
07/30/21 09:19:06  07/31/21 09:19:06  krbtgt/IDM.EXAMPLE.COM@IDM.EXAMPLE.COM
        for client HTTP/myhost.idm.example.com@IDM.EXAMPLE.COM
....


* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server where you are configuring the constrained delegation.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server where you are configuring the constrained delegation.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.


.Procedure

. On your Ansible control node, navigate to your *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....


. Create a `web-console-smart-card-sudo.yml` playbook with the following content:

.. Create a task that ensures the presence of a delegation target:
+
[literal,subs="+quotes,attributes"]
....
---
- name: Playbook to create a constrained delegation target
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure servicedelegationtarget named sudo-web-console-delegation-target is present
    ipaservicedelegationtarget:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: sudo-web-console-delegation-target
....

.. Add a task that adds the target host to the delegation target:
+
[literal,subs="+quotes,attributes"]
....
  - name: Ensure that a member principal named host/myhost.idm.example.com@IDM.EXAMPLE.COM is present in a service delegation target named sudo-web-console-delegation-target
    ipaservicedelegationtarget:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: sudo-web-console-delegation-target
      principal: host/myhost.idm.example.com@IDM.EXAMPLE.COM
      action: member
....


.. Add a task that ensures the presence of a delegation rule:
+
[literal,subs="+quotes,attributes"]
....
  - name: Ensure servicedelegationrule named sudo-web-console-delegation-rule is present
    ipaservicedelegationrule:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: sudo-web-console-delegation-rule
....



.. Add a task that ensures that the Kerberos principal of the web console service is a member of the constrained delegation rule:
+
[literal,subs="+quotes,attributes"]
....
  - name: Ensure the Kerberos principal of the web console service is added to the service delegation rule named sudo-web-console-delegation-rule
    ipaservicedelegationrule:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: sudo-web-console-delegation-rule
      principal: HTTP/myhost.idm.example.com
      action: member
....




.. Add a task that ensures that the constrained delegation rule is associated with the sudo-web-console-delegation-target delegation target:
+
[literal,subs="+quotes,attributes"]
....
  - name: Ensure a constrained delegation rule is associated with a specific delegation target
    ipaservicedelegationrule:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: sudo-web-console-delegation-rule
      target: sudo-web-console-delegation-target
      action: member
....

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory web-console-smart-card-sudo.yml*
....


. Enable `pam_sss_gss`, the PAM module for authenticating users over the Generic Security Service Application Program Interface (GSSAPI) in cooperation with the System Security Services Daemon (SSSD):

.. Open the `/etc/sssd/sssd.conf` file for editing.

.. Specify that `pam_sss_gss` can provide authentication for the `sudo` and `sudo -i` commands in IdM your domain:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[domain/idm.example.com]
*pam_gssapi_services = sudo, sudo-i*
....

.. Save and exit the file.

.. Open the `/etc/pam.d/sudo` file for editing.

.. Insert the following line to the top of the `#%PAM-1.0` list to allow, but not require, GSSAPI authentication for `sudo` commands:
+
[literal,subs="+quotes,attributes,verbatim"]
....
*auth sufficient pam_sss_gss.so*
....

.. Save and exit the file.

. Restart the `SSSD` service so that the above changes take effect immediately:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *systemctl restart sssd*
....

[role="_additional-resources"]
.Additional resources

* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/assembly_using-constrained-delegation-in-idm_configuring-and-managing-idm#con_constrained-delegation-in-identity-management_assembly_using-constrained-delegation-in-idm[Constrained delegation in Identity Management]
* `README-servicedelegationrule.md` and `README-servicedelegationtarget.md` in the `/usr/share/doc/ansible-freeipa/` directory
* Sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/servicedelegationtarget` and `/usr/share/doc/ansible-freeipa/playbooks/servicedelegationrule` directories
