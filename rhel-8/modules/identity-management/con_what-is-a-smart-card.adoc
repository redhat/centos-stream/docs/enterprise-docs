:_mod-docs-content-type: CONCEPT

[id="con_what-is-a-smart-card_{context}"]
= What is a smart card

A smart card is a physical device, usually a plastic card with a microprocessor, that can provide personal authentication using certificates stored on the card. Personal authentication means that you can use smart cards in the same way as user passwords.

You can store user credentials on the smart card in the form of a private key and a certificate, and special software and hardware is used to access them. You place the smart card into a reader or a USB socket and supply the PIN code for the smart card instead of providing your password.
