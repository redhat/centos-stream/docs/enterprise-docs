:_mod-docs-content-type: PROCEDURE
[id="using-an-ansible-playbook-to-ensure-that-the-forward-first-policy-is-set-in-idm-dns-global-configuration_{context}"]
= Using an Ansible playbook to ensure that the forward first policy is set in IdM DNS global configuration

[role="_abstract"]
Follow this procedure to use an Ansible playbook to ensure that global forwarding policy in IdM DNS is set to *forward first*.

If you use the *forward first* DNS forwarding policy, DNS queries are forwarded to the configured forwarder. If a query fails because of a server error or timeout, BIND falls back to the recursive resolution using servers on the Internet. The forward first policy is the default policy. It is suitable for traffic optimization.

.Prerequisites

* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
* You know the IdM administrator password.
* Your IdM environment contains an integrated DNS server.


.Procedure

. Navigate to the `/usr/share/doc/ansible-freeipa/playbooks/dnsconfig` directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd /usr/share/doc/ansible-freeipa/playbooks/dnsconfig*
....


. Open your inventory file and ensure that the IdM server that you want to configure is listed in the `[ipaserver]` section. For example, to instruct Ansible to configure *server.idm.example.com*, enter:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[ipaserver]
server.idm.example.com
....

. Make a copy of the *set-configuration.yml* Ansible playbook file. For example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp set-configuration.yml set-forward-policy-to-first.yml*
....

. Open the *set-forward-policy-to-first.yml* file for editing.

. Adapt the file by setting the following variables in the `ipadnsconfig` task section:

* Set the `ipaadmin_password` variable to your IdM administrator password.
* Set the `forward_policy` variable to *first*.


+
Delete all the other lines of the original playbook that are irrelevant. This is the modified Ansible playbook file for the current example:

+
[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Playbook to set global forwarding policy to first
  hosts: ipaserver
  become: true

  tasks:
  - name: Set global forwarding policy to first.
    ipadnsconfig:
      ipaadmin_password: "{{ ipaadmin_password }}"
      forward_policy: first
....

. Save the file.

. Run the playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory.file set-forward-policy-to-first.yml*
....

[role="_additional-resources"]
.Additional resources
* xref:dns-forward-policies-in-idm_{context}[DNS forward policies in IdM]
* The `README-dnsconfig.md` file in the `/usr/share/doc/ansible-freeipa/` directory
* For more sample playbooks, see the `/usr/share/doc/ansible-freeipa/playbooks/dnsconfig` directory.
