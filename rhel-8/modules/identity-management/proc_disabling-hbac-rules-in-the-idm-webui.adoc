:_mod-docs-content-type: PROCEDURE

[id="disabling-hbac-rules-in-the-idm-webui_{context}"]
= Disabling HBAC rules in the IdM WebUI

[role="_abstract"]
You can disable an HBAC rule but it only deactivates the rule and does not delete it. If you disable an HBAC rule, you can re-enable it later.

[NOTE]
====
Disabling HBAC rules is useful when you are configuring custom HBAC rules for the first time. To ensure that your new configuration is not overridden by the default `allow_all` HBAC rule, you must disable `allow_all`.
====


.Procedure

. Select *Policy>Host-Based Access Control>HBAC Rules*.

. Select the HBAC rule you want to disable.

. Click btn:[Disable].

. Click btn:[OK] to confirm you want to disable the selected HBAC rule.

