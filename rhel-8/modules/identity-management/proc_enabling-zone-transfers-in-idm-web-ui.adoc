:_mod-docs-content-type: PROCEDURE
:experimental:
[id="enabling-zone-transfers-in-idm-web-ui_{context}"]
= Enabling zone transfers in IdM Web UI

[role="_abstract"]
Follow this procedure to enable zone transfers in {IPA} (IdM) using the IdM Web UI.

.Prerequisites

* You are logged in as IdM administrator.

.Procedure

. In the IdM Web UI, click `Network Services` → `DNS` → `DNS Zones`.

. Click `Settings`.

. Under `Allow transfer`, specify the name servers to which you want to transfer the zone records.

+
[id="enabling-zone-transfers_{context}"]
.Enabling zone transfers

image::dns-allow-transfer.png[A screenshot of the "Allow transfer" pop-up window with three fields for servers that each has a different IP address. The "Add" button is at the bottom of the window.]

. Click btn:[Save] at the top of the DNS zone page to confirm the new configuration.
