:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_mitigating-server-loss-with-replication.adoc


[id="recovering-from-losing-the-cars_{context}"]
= Recovering from losing the CA renewal server

[role="_abstract"]
If the Certificate Authority (CA) renewal server is lost, you must first promote another CA replica to fulfill the CA renewal server role, and then deploy a replacement CA replica.

.Prerequisites
* Your deployment uses IdM's internal Certificate Authority (CA).
* Another Replica in the environment has CA services installed.

[WARNING]
====
An IdM deployment is unrecoverable if:

. The CA renewal server has been lost.
. No other server has a CA installed.
. No backup of a replica with the CA role exists.
+
It is critical to make backups from a replica with the CA role so certificate data is protected. For more information about creating and restoring from backups, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/preparing_for_disaster_recovery_with_identity_management/preparing-for-data-loss-with-idm-backups_preparing-for-disaster-recovery[Preparing for data loss with IdM backups].
====

.Procedure

. From another replica in your environment, promote another CA replica in the environment to act as the new CA renewal server. See
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/ipa-ca-renewal_configuring-and-managing-idm#changing-ca-renewal_ipa-ca-renewal[Changing and resetting IdM CA renewal server].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/ipa-ca-renewal_managing-certificates-in-idm#changing-ca-renewal_ipa-ca-renewal[Changing and resetting IdM CA renewal server].
endif::[]

. From another replica in your environment, remove replication agreements to the lost CA renewal server. See
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/assembly_managing-replication-topology_configuring-and-managing-idm#managing-topology-remove-cli[Removing server from topology using the CLI].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_replication_in_identity_management/assembly_managing-replication-topology_managing-replication-in-idm#managing-topology-remove-cli[Removing server from topology using the CLI].
endif::[]

. Install a new CA Replica to replace the lost CA replica. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-ipa-replica_installing-identity-management[Installing an IdM replica with a CA].

. Update DNS to reflect changes in the replica topology. If IdM DNS is used, DNS service records are updated automatically.

. Verify IdM clients can reach IdM servers. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/performing_disaster_recovery_with_identity_management/adjusting-idm-clients-during-recovery_performing-disaster-recovery[Adjusting IdM clients during recovery].

.Verification

. Test the Kerberos server on the new replica by successfully retrieving a Kerberos Ticket-Granting-Ticket as an IdM user.
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *kinit admin*
Password for admin@EXAMPLE.COM:

[root@server ~]# *klist*
Ticket cache: KCM:0
Default principal: admin@EXAMPLE.COM

Valid starting       Expires              Service principal
10/31/2019 15:51:37  11/01/2019 15:51:02  HTTP/server.example.com@EXAMPLE.COM
10/31/2019 15:51:08  11/01/2019 15:51:02  *krbtgt/EXAMPLE.COM@EXAMPLE.COM*
....

. Test the Directory Server and SSSD configuration by retrieving user information.
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *ipa user-show admin*
  User login: admin
  Last name: Administrator
  Home directory: /home/admin
  Login shell: /bin/bash
  Principal alias: admin@EXAMPLE.COM
  UID: 1965200000
  GID: 1965200000
  Account disabled: False
  Password: True
  Member of groups: admins, trust admins
  Kerberos keys available: True
....

. Test the CA configuration with the `ipa cert-show` command.
+
[literal,subs="+quotes,attributes"]
....
[root@server ~]# *ipa cert-show 1*
  Issuing CA: ipa
  Certificate: MIIEgjCCAuqgAwIBAgIjoSIP...
  Subject: CN=Certificate Authority,O=EXAMPLE.COM
  Issuer: CN=Certificate Authority,O=EXAMPLE.COM
  Not Before: Thu Oct 31 19:43:29 2019 UTC
  Not After: Mon Oct 31 19:43:29 2039 UTC
  Serial number: 1
  Serial number (hex): 0x1
  Revoked: False
....

[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/ipa-ca-renewal_configuring-and-managing-idm[Using IdM CA renewal server]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/ipa-ca-renewal_managing-certificates-in-idm[Using IdM CA renewal server]
endif::[]
