:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies:
// assembly_configuring-the-domain-resolution-order-to-resolve-short-ad-user-names.adoc

[id="setting-the-domain-resolution-order-for-an-id-view-on-an-idm-server_{context}"]
= Setting the domain resolution order for an ID view on an IdM server

[role="_abstract"]
This procedure sets the domain resolution order for an ID view that you can apply to a specific set of IdM servers and clients. This example creates an ID view named `ADsubdomain1_first` for IdM host `client1.idm.example.com`, and sets the domain resolution order to search for users and groups in the following order:

. Active Directory (AD) child domain `subdomain1.ad.example.com`
. AD root domain `ad.example.com`
. IdM domain `idm.example.com`

[NOTE]
====
The domain resolution order set in an ID view overrides the global domain resolution order, but it does not override any domain resolution order set locally in the SSSD configuration.
====


.Prerequisites

* You have configured a trust with an AD environment.


.Procedure

. Create an ID view with the [option]`--domain-resolution-order` option set.
+
[literal,subs="+quotes,attributes"]
....
[user@server ~]$ *ipa idview-add ADsubdomain1_first --desc "ID view for resolving AD subdomain1 first on client1.idm.example.com" --domain-resolution-order subdomain1.ad.example.com:ad.example.com:idm.example.com*
---------------------------------
Added ID View "ADsubdomain1_first"
---------------------------------
ID View Name: ADsubdomain1_first
Description: ID view for resolving AD subdomain1 first on client1.idm.example.com
*Domain Resolution Order: subdomain1.ad.example.com:ad.example.com:idm.example.com*
....

. Apply the ID view to IdM hosts.
+
[literal,subs="+quotes,attributes"]
....
[user@server ~]$ *ipa idview-apply ADsubdomain1_first --hosts client1.idm.example.com*
-----------------------------------
Applied ID View "ADsubdomain1_first"
-----------------------------------
  hosts: client1.idm.example.com
---------------------------------------------
Number of hosts the ID View was applied to: 1
---------------------------------------------
....


.Verification

* Display the details of the ID view.
+
[literal,subs="+quotes,attributes"]
....
[user@server ~]$ *ipa idview-show ADsubdomain1_first  --show-hosts*
  ID View Name: ADsubdomain1_first
  Description: ID view for resolving AD subdomain1 first on client1.idm.example.com
  *Hosts the view applies to: client1.idm.example.com*
  *Domain resolution order: subdomain1.ad.example.com:ad.example.com:idm.example.com*
....

* Verify you can retrieve user information for a user from the `subdomain1.ad.example.com` domain using only a short name.
+
[literal,subs="+quotes,attributes"]
....
[root@client1 ~]# *id _<user_from_subdomain1>_*
uid=1916901106(user_from_subdomain1) gid=1916900513(domain users) groups=1916900513(domain users)
....
