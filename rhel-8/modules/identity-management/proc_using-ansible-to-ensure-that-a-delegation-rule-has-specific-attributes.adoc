:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_delegating-permissions-over-users-using-ansible-playbooks.adoc


[id="proc_using-ansible-to-ensure-that-a-delegation-rule-has-specific-attributes_{context}"]
= Using Ansible to ensure that a delegation rule has specific attributes

[role="_abstract"]
The following procedure describes how to use an Ansible playbook to ensure that a delegation rule has specific settings. You can use this playbook to modify a delegation role you have previously created. In the example, you ensure the *basic manager attributes* delegation rule only has the `departmentnumber` member attribute.


.Prerequisites

* On the control node:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

* The *basic manager attributes* delegation rule exists in IdM.


.Procedure

. Navigate to the *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Make a copy of the `delegation-member-present.yml` file located in the `/usr/share/doc/ansible-freeipa/playbooks/delegation/` directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cp /usr/share/doc/ansible-freeipa/playbooks/delegation/delegation-member-present.yml delegation-member-present-copy.yml*
....

. Open the `delegation-member-present-copy.yml` Ansible playbook file for editing.

. Adapt the file by setting the following variables in the `ipadelegation` task section:

* Set the `ipaadmin_password` variable to the password of the IdM administrator.
* Set the `name` variable to the name of the delegation rule to modify.
* Set the `attribute` variable to `departmentnumber`.
* Set the `action` variable to `member`.


+
--
This is the modified Ansible playbook file for the current example:

[literal,subs="+quotes,attributes,verbatim"]
....
---
- name: Delegation member present
  hosts: ipaserver

  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
  - name: Ensure delegation "basic manager attributes" member attribute departmentnumber is present
    ipadelegation:
      *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: "basic manager attributes"*
      *attribute:*
      *- departmentnumber*
      *action: member*
....
--

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i ~/__MyPlaybooks__/inventory delegation-member-present-copy.yml*
....


[role="_additional-resources"]
.Additional resources
* xref:delegation-rules_{context}[Delegation rules]
* The `README-delegation.md` file in the `/usr/share/doc/ansible-freeipa/` directory
* The sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/ipadelegation` directory
