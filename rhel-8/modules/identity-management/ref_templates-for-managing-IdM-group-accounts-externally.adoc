:_mod-docs-content-type: REFERENCE
[id="templates-for-managing-IdM-group-accounts-externally_{context}"]
= Templates for managing IdM group accounts externally

[role="_abstract"]
The following templates can be used for various user group management operations in IdM. The templates show which attributes you must modify using `ldapmodify` to achieve the following aims:

* Creating a new group
* Deleting an existing group
* Adding a member to a group
* Removing a member from a group

The templates are formatted in the LDAP Data Interchange Format (LDIF). LDIF is a standard plain text data interchange format for representing LDAP directory content and update requests.

Using the templates, you can configure the LDAP provider of your provisioning system to manage IdM group accounts.

.Creating a new group

....
dn: cn=group_name,cn=groups,cn=accounts,dc=idm,dc=example,dc=com
changetype: add
objectClass: top
objectClass: ipaobject
objectClass: ipausergroup
objectClass: groupofnames
objectClass: nestedgroup
objectClass: posixgroup
uid: group_name
cn: group_name
gidNumber: GID_number
....


.Modifying groups

* *Deleting an existing group*:

+
....
dn: group_distinguished_name
changetype: delete
....

* *Adding a member to a group*:

+
....
dn: group_distinguished_name
changetype: modify
add: member
member: uid=user_login,cn=users,cn=accounts,dc=idm,dc=example,dc=com
....

+
Do not add stage or preserved users to groups. Even though the update operation completes successfully, the users will not be updated as members of the group. Only active users can belong to groups.


* *Removing a member from a group*:

+
....
dn: distinguished_name
changetype: modify
delete: member
member: uid=user_login,cn=users,cn=accounts,dc=idm,dc=example,dc=com
....


[NOTE]
====
Before modifying a group, obtain the group's distinguished name (DN) by searching using the group's name.

[literal,subs="+quotes,attributes,verbatim"]
....
# *ldapsearch -YGSSAPI  -H ldap://server.idm.example.com -b "cn=groups,cn=accounts,dc=idm,dc=example,dc=com" "cn=group_name"*
dn: cn=group_name,cn=groups,cn=accounts,dc=idm,dc=example,dc=com
ipaNTSecurityIdentifier: S-1-5-21-1650388524-2605035987-2578146103-11017
cn: testgroup
objectClass: top
objectClass: groupofnames
objectClass: nestedgroup
objectClass: ipausergroup
objectClass: ipaobject
objectClass: posixgroup
objectClass: ipantgroupattrs
ipaUniqueID: 569bf864-9d45-11ea-bea3-525400f6f085
gidNumber: 1997010017
....
====
