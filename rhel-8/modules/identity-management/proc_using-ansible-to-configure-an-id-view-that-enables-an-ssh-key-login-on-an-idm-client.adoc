:_newdoc-version: 2.15.1
:_template-generated: 2024-02-13
:_mod-docs-content-type: PROCEDURE

[id="using-ansible-to-configure-an-id-view-that-enables-an-ssh-key-login-on-an-idm-client_{context}"]
= Using Ansible to configure an ID view that enables an SSH key login on an IdM client

[role="_abstract"]
Complete this procedure to use the `idoverrideuser` `ansible-freeipa` module to ensure that an IdM user can use a specific SSH key to log in to a specific IdM client. The procedure uses the example of an ID view that enables an IdM user named *idm_user* to log in to an IdM client named *host1.idm.example.com* with an SSH key.

NOTE: This ID view can be used to enhance a specific HBAC rule.

.Prerequisites
* On the control node:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** You have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
** You are using RHEL 8.10 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server in the *~/__MyPlaybooks__/* directory.
You are using RHEL 9.4 or later.
endif::[]
** You have stored your `ipaadmin_password` in the *secret.yml* Ansible vault.
* You have access to the *idm_user*’s SSH public key.
* The *idview_for_host1* ID view exists.

* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.

.Procedure

. Create your Ansible playbook file *ensure-idoverrideuser-can-login-with-sshkey.yml* with the following content:

+
[literal,subs="+quotes,attributes,verbatim"]
----
---
- name: Playbook to manage idoverrideuser
  hosts: ipaserver
  become: false
  gather_facts: false
  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml

  tasks:
  - name: Ensure test user idm_user is present in idview idview_for_host1 with sshpubkey
    ipaidoverrideuser:
      ipaadmin_password: *”{{ ipaadmin_password }}"*
      idview: *idview_for_host1*
      anchor: *idm_user*
      sshpubkey:
      - ssh-rsa AAAAB3NzaC1yc2EAAADAQABAAABgQCqmVDpEX5gnSjKuv97Ay ...
  - name: Ensure idview_for_host1 is applied to host1.idm.example.com
    ipaidview:
      ipaadmin_password:  *”{{ ipaadmin_password }}"*
      name: idview_for_host1
      host: *host1.idm.example.com*
      action: member

----

+
. Run the playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i <path_to_inventory_directory>/inventory <path_to_playbooks_directory>/ensure-idoverrideuser-can-login-with-sshkey.yml*
....


. Optional: If you have `root` credentials, you can apply the new configuration to the *host1.idm.example.com* system immediately:

+
--
.. SSH to the system as `root`:

+
[literal,subs="+quotes,attributes,verbatim"]
....

$ *ssh root@host1*
Password:
....


.. Clear the SSSD cache:

+
[literal,subs="+quotes,attributes,verbatim"]
....
root@host1 ~]# *sss_cache -E*
....

.. Restart the SSSD daemon:

+
[literal,subs="+quotes,attributes,verbatim"]
....
root@host1 ~]# *systemctl restart sssd*
....
--


.Verification

* Use the public key to `SSH` to *host1*:
+
[literal,subs="+quotes,attributes,verbatim"]
....
[root@r8server ~]# *ssh -i ~/.ssh/id_rsa.pub idm_user@host1.idm.example.com*

Last login: Sun Jun 21 22:34:25 2023 from 192.168.122.229
[idm_user@host1 ~]$
....

The output confirms that you have logged in successfully.


[role="_additional-resources"]
.Additional resources
* The link:https://github.com/freeipa/ansible-freeipa/blob/master/README-idoverrideuser.md[idoverrideuser] module in `ansible-freeipa` upstream docs



