:_newdoc-version: 2.15.1
:_template-generated: 2024-03-01
:_mod-docs-content-type: PROCEDURE

[id="using-ansible-to-enable-an-idm-user-to-authenticate-via-an-external-idp_{context}"]
= Using Ansible to enable an IdM user to authenticate via an external IdP

[role="_abstract"]
You can use the `user` `ansible-freeipa` module to enable an {IPA} (IdM) user to authenticate via an external identity provider (IdP). To do that, associate the external IdP reference you have previously created with the IdM user account. Complete this procedure to use Ansible to associate an external IdP reference named *github_idp* with the IdM user named *idm-user-with-external-idp*. As a result of the procedure, the user is able to use the *my_github_account_name* github identity to authenticate as *idm-user-with-external-idp* to IdM. 

.Prerequisites

ifeval::[{ProductNumber} == 8]
* Your IdM client and IdM servers are using RHEL 8.7 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
* Your IdM client and IdM servers are using RHEL 9.1 or later.
endif::[]
* Your IdM client and IdM servers are using SSSD 2.7.0 or later.
* You have created a reference to an external IdP in IdM. See xref:using-ansible-to-create-a-reference-to-an-external-identity-provider_{context}[Using Ansible to create a reference to an external identity provider].
* You have configured your Ansible control node to meet the following requirements:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package on the Ansible controller.
** You are using RHEL 
ifeval::[{ProductNumber} == 8]
8.10 
endif::[]
ifeval::[{ProductNumber} == 9]
9.4 
endif::[]
or later.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.


.Procedure

. On your Ansible control node, create an *enable-user-to-authenticate-via-external-idp.yml* playbook:
+
[literal,subs="+quotes,attributes,verbatim"]
----
---
- name: Ensure an IdM user uses an external IdP to authenticate to IdM
  hosts: ipaserver
  become: false
  gather_facts: false

  tasks:
  - name: Retrieve Github user ID
    ansible.builtin.uri:
      url: “https://api.github.com/users/my_github_account_name”
      method: GET
      headers: 
        Accept: “application/vnd.github.v3+json”
    register: user_data

  - name: Ensure IdM user exists with an external IdP authentication
    ipauser:
      ipaadmin_password: "{{ ipaadmin_password }}"
      name: *idm-user-with-external-idp*
      first: Example
      last: User
      userauthtype: *idp*
      idp: *github_idp*
      idp_user_id: *my_github_account_name*
----

. Save the file.

. Run the Ansible playbook. Specify the playbook file, the file storing the password protecting the *secret.yml* file, and the inventory file:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory enable-user-to-authenticate-via-external-idp.yml*
....


.Verification

* Log in to an IdM client and verify that the output of the `ipa user-show` command for the *idm-user-with-external-idp* user displays references to the IdP:
+
[literal,subs="+quotes,verbatim"]
....
$ *ipa user-show idm-user-with-external-idp*
User login: idm-user-with-external-idp
First name: Example
Last name: User
Home directory: /home/idm-user-with-external-idp
Login shell: /bin/sh
Principal name: idm-user-with-external-idp@idm.example.com
Principal alias: idm-user-with-external-idp@idm.example.com
Email address: idm-user-with-external-idp@idm.example.com
ID: 35000003
GID: 35000003
*User authentication types: idp*
*External IdP configuration: github*
*External IdP user identifier: idm-user-with-external-idp@idm.example.com*
Account disabled: False
Password: False
Member of groups: ipausers
Kerberos keys available: False
....


[role="_additional-resources"]
.Additional resources

* The link:https://github.com/freeipa/ansible-freeipa/blob/master/README-idp.md[idp] `ansible-freeipa` upstream documentation

