[id="using-ansible-to-add-idm-users-to-a-group-that-owns-nfs-shares_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using Ansible to add IdM users to a group that owns NFS shares
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.


[role="_abstract"]
As an {IPA} (IdM) system administrator, you can use Ansible to create a group of users that is able to access NFS shares, and add IdM users to this group.

This example describes how to use an Ansible playbook to ensure that the *idm_user* account belongs to the *developers* group, so that *idm_user* can access the */exports/project* NFS share.

.Prerequisites

* You have `root` access to the *nfs-server.idm.example.com* NFS server, which is an IdM client located in the *raleigh* automount location.
* On the control node:
ifeval::[{ProductNumber} == 8]
** You are using Ansible version 2.13 or later.
endif::[]
ifeval::[{ProductNumber} == 9]
** You are using Ansible version 2.15 or later.
endif::[]
** You have installed the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_identity_management/installing-an-identity-management-server-using-an-ansible-playbook_installing-identity-management#installing-the-ansible-freeipa-package_server-ansible[`ansible-freeipa`] package.
** The example assumes that in the *~/__MyPlaybooks__/* directory, you have created an
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-idm[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/preparing-your-environment-for-managing-idm-using-ansible-playbooks_using-ansible-to-install-and-manage-identity-management[Ansible inventory file] with the fully-qualified domain name (FQDN) of the IdM server.
endif::[]
** The example assumes that the *secret.yml* Ansible vault stores your `ipaadmin_password`.
* The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.
** In *~/__MyPlaybooks__/*, you have created the `automount-location-map-and-key-present.yml` file that already contains tasks from xref:configuring-automount-locations-maps-and-keys-in-idm-by-using-ansible_{context}[Configuring automount locations, maps, and keys in IdM by using Ansible].

.Procedure


. On your Ansible control node, navigate to the *~/__MyPlaybooks__/* directory:
+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *cd ~/__MyPlaybooks__/*
....

. Open the `automount-location-map-and-key-present.yml` file for editing.

. In the `tasks` section, add a task to ensure that the IdM *developers* group exists and *idm_user* is added to this group:

+
[literal,subs="+quotes,attributes,verbatim"]
....
[...]
  vars_files:
  - /home/user_name/MyPlaybooks/secret.yml
  tasks:
[...]
  - *ipagroup:*
     *ipaadmin_password: "{{ ipaadmin_password }}"*
      *name: developers*
      *user:*
      *- idm_user*
      *state: present*
....

. Save the file.

. Run the Ansible playbook and specify the playbook and inventory files:

+
[literal,subs="+quotes,attributes,verbatim"]
....
$ *ansible-playbook --vault-password-file=password_file -v -i inventory automount-location-map-and-key-present.yml*
....

. On the NFS server, change the group ownership of the */exports/project* directory to *developers* so that every IdM user in the group can access the directory:

+
[literal,subs="+quotes,attributes,verbatim"]
....
# *chgrp developers /exports/project*
....
