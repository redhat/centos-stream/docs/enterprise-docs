:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
// assembly_configuring-certficates-issued-adcs-for-smart-card-authentication.adoc

[id="windows-server-settings-required-for-trust-configuration-and-certificate-usage_{context}"]
= Windows Server settings required for trust configuration and certificate usage

[role="_abstract"]
You must configure the following on the Windows Server:

* Active Directory Certificate Services (ADCS) is installed
* Certificate Authority is created
* Optional: If you are using Certificate Authority Web Enrollment, the Internet Information Services (IIS) must be configured
////
* Make sure you have certificate templates in the *Administrative Tools -> Certificate Authority*.
+
If not, duplicate a web server template:
** change its name
** publish the certificate in Active Directory
** Allow private key to be exported
** Set authenticate users -- Enroll and Autoenroll
** Verify in the *Subject name* tab that he name is  supplied in the request
+
Duplicate a template for a Smart card user:
** change its name
** publish the certificate in Active Directory
** Allow private key to be exported
** Verify that minimum key size is 2048
** Set access rights, authenticated users need permissions for reading/writing/enrolling/autoenrolling the certificate
** Subject name format must be changed to *Common name* and including e-mail name in subject name must be unselected.

* Certficate issued for a web server

gpupdate - group policy update
gpupdate /force
////
Export the certificate:

* Key must have `2048` bits or more
* Include a private key
* You will need a certificate in the following format: Personal Information Exchange -- `PKCS #12(.PFX)`
** Enable certificate privacy
