:_mod-docs-content-type: PROCEDURE
[id="creating-dns-locations-using-the-idm-cli_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Creating DNS locations using the IdM CLI

[role="_abstract"]
You can use DNS locations to increase the speed of communication between {IPA} (IdM) clients and servers. Follow this procedure to create DNS locations using the `ipa location-add` command in the IdM command-line interface (CLI).

.Prerequisites

* Your IdM deployment has integrated DNS.
* You have a permission to create DNS locations in IdM. For example, you are logged in as IdM admin.


.Procedure

. For example, to create a new location [option]`germany`, enter:

+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ *ipa location-add pass:quotes[_germany_]*
----------------------------
Added IPA location "germany"
----------------------------
  Location name: germany
....

. Optional: Repeat the step to add further locations.

[role="_additional-resources"]
.Additional resources
* xref:assigning-an-idm-server-to-a-dns-location-using-the-idm-cli_{context}[Assigning an IdM Server to a DNS Location using the IdM CLI]
* xref:using-ansible-to-ensure-an-idm-location-is-present_using-ansible-to-manage-dns-locations-in-idm[Using Ansible to ensure an IdM location is present]
