:_mod-docs-content-type: REFERENCE

[id="ref_flow-of-information-when-an-idm-client-requests-services-from-an-ad-server_{context}"]
= Flow of information when an IdM client requests services from an AD server

[role="_abstract"]
The following diagram explains the flow of information when an Identity Management (IdM) client requests a service in the Active Directory (AD) domain when you have configured a two-way trust between IdM and AD.

If you have trouble accessing AD services from IdM clients, you can use this information to narrow your troubleshooting efforts and identify the source of the issue.

[NOTE]
====
By default, IdM establishes a one-way trust to AD, which means it is not possible to issue cross-realm ticket-granting ticket (TGT) for resources in an AD forest. To be able to request tickets to services from trusted AD domains, configure a two-way trust.
====

image:231_RHEL_troubleshooting_cross-forest_0422_IdM-request.png[diagram showing how an IdM client communicates with an IdM server and an AD Domain Controller]

. The IdM client requests a ticket-granting ticket (TGT) from the IdM Kerberos Distribution Center (KDC) for the AD service it wants to contact.
. The IdM KDC recognizes that the service belongs to the AD realm, verifies that the realm is known and trusted, and that the client is allowed to request services from that realm.
. Using information from the IdM Directory Server about the user principal, the IdM KDC creates a cross-realm TGT with a Privileged Attribute Certificate (MS-PAC) record about the user principal.
. The IdM KDC sends back a cross-realm TGT to the IdM client.
. The IdM client contacts the AD KDC to request a ticket for the AD service, presenting the cross-realm TGT that contains the MS-PAC provided by the IdM KDC.
. The AD server validates and filters the PAC, and returns a ticket for the AD service.
. The IPA client can now contact the AD service.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/planning_identity_management/planning-a-cross-forest-trust-between-idm-and-ad_planning-identity-management#one-way-trusts-and-two-way-trusts_planning-a-cross-forest-trust-between-idm-and-ad[One-way trusts and two-way trusts]
