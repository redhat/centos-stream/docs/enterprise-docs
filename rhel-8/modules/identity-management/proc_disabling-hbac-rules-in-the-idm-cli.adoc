:_mod-docs-content-type: PROCEDURE

[id="disabling-hbac-rules-in-the-idm-cli_{context}"]
= Disabling HBAC rules in the IdM CLI

[role="_abstract"]
You can disable an HBAC rule but it only deactivates the rule and does not delete it. If you disable an HBAC rule, you can re-enable it later.

[NOTE]
====
Disabling HBAC rules is useful when you are configuring custom HBAC rules for the first time. To ensure that your new configuration is not overridden by the default `allow_all` HBAC rule, you must disable `allow_all`.
====

.Procedure

* Use the `ipa hbacrule-disable` command. For example, to disable the `allow_all` rule:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
$ ipa hbacrule-disable allow_all
------------------------------
Disabled HBAC rule "allow_all"
------------------------------
....

[role="_additional-resources"]
.Additional resources

* See `ipa hbacrule-disable --help` for more details.