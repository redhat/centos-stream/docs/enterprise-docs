:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Enabling Hyper-V enlightenments

[id="enabling-hyper-v-enlightenments-on-a-windows-virtual-machine-in-rhel-8_{context}"]
= Enabling Hyper-V enlightenments on a Windows virtual machine

Hyper-V enlightenments provide better performance in a Windows virtual machine (VM) running in a {ProductShortName}{nbsp}{ProductNumber} host. For instructions on how to enable them, see the following.


.Procedure

. Use the [command]`virsh edit` command to open the XML configuration of the VM. For example:
+
[subs="+quotes,attributes"]
----
# *virsh edit _windows-vm_*
----

ifeval::[{ProductNumber} == 8]
. Add the following `<hyperv>` sub-section to the `<features>` section of the XML:
+
[source,xml]
----
<features>
  [...]
  <hyperv>
    <relaxed state='on'/>
    <vapic state='on'/>
    <spinlocks state='on' retries='8191'/>
    <vpindex state='on'/>
    <runtime state='on' />
    <synic state='on'/>
    <stimer state='on'>
      <direct state='on'/>
    </stimer>
    <frequencies state='on'/>
  </hyperv>
  [...]
</features>
----
+
If the XML already contains a `<hyperv>` sub-section, modify it as shown above.
endif::[]

ifeval::[{ProductNumber} == 9]
. Add the following `<hyperv>` sub-section to the `<features>` section of the XML:
+
[source,xml]
----
<features>
  [...]
  <hyperv>
    <relaxed state='on'/>
    <vapic state='on'/>
    <spinlocks state='on' retries='8191'/>
    <vendor_id state='on' value='KVM Hv'/>
    <vpindex state='on'/>
    <runtime state='on' />
    <synic state='on'/>
    <stimer state='on'>
      <direct state='on'/>
    </stimer>
    <frequencies state='on'/>
    <reset state='on'/>
    <tlbflush state='on'/>
    <reenlightenment state='on'/>
    <ipi state='on'/>
    <evmcs state='on'/>
  </hyperv>
  [...]
</features>
----
+
If the XML already contains a `<hyperv>` sub-section, modify it as shown above.
endif::[]

. Change the `clock` section of the configuration as follows:
+
[source,xml]
----
<clock offset='localtime'>
  ...
  <timer name='hypervclock' present='yes'/>
</clock>
----

. Save and exit the XML configuration.

. If the VM is running, restart it.

.Verification

ifeval::[{ProductNumber} == 8]
* Use the `virsh dumpxml` command to display the XML configuration of the running VM. If it includes the following segments, the Hyper-V enlightenments are enabled on the VM.
+
[source,xml]
----
<hyperv>
  <relaxed state='on'/>
  <vapic state='on'/>
  <spinlocks state='on' retries='8191'/>
  <vpindex state='on'/>
  <runtime state='on' />
  <synic state='on'/>
  <stimer state='on'>
    <direct state='on'/>
  </stimer>
  <frequencies state='on'/>
</hyperv>

<clock offset='localtime'>
  ...
  <timer name='hypervclock' present='yes'/>
</clock>
----
endif::[]

ifeval::[{ProductNumber} == 9]
* Use the `virsh dumpxml` command to display the XML configuration of the running VM. If it includes the following segments, the Hyper-V enlightenments are enabled on the VM.
+
[source,xml]
----
<hyperv>
  <relaxed state='on'/>
  <vapic state='on'/>
  <spinlocks state='on' retries='8191'/>
  <vendor_id state='on' value='KVM Hv'/>
  <vpindex state='on'/>
  <runtime state='on' />
  <synic state='on'/>
  <frequencies state='on'/>
  <reset state='on'/>
  <tlbflush state='on'/>
  <reenlightenment state='on'/>
  <stimer state='on'>
    <direct state='on'/>
  </stimer>
  <ipi state='on'/>
  <evmcs state='on'/>
</hyperv>

<clock offset='localtime'>
  ...
  <timer name='hypervclock' present='yes'/>
</clock>
----
endif::[]
