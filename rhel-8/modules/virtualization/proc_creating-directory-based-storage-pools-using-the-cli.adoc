:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Creating and assigning directory-based storage for virtual machines using the CLI

// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/proc_creating-directory-based-storage-pools-using-the-cli.adoc[leveloffset=+1]

[id="creating-directory-based-storage-pools-using-the-cli_{context}"]
= Creating directory-based storage pools by using the CLI

[role="_abstract"]
A directory-based storage pool is based on a directory in an existing mounted file system. This is useful, for example, when you want to use the remaining space on the file system for other purposes. You can use the `virsh` utility to create directory-based storage pools.

.Prerequisites

* Ensure your hypervisor supports directory storage pools:
+
[subs="+quotes"]
----
# *virsh pool-capabilities | grep "'dir' supported='yes'"*
----
+
If the command displays any output, directory pools are supported.

.Procedure

. *Create a storage pool*
+
Use the [command]`virsh pool-define-as` command to define and create a directory-type storage pool. For example, to create a storage pool named `guest_images_dir` that uses the */guest_images* directory:
+
[subs="quotes"]
----
# *virsh pool-define-as guest_images_dir dir --target "/guest_images"*
Pool guest_images_dir defined
----
+
If you already have an XML configuration of the storage pool you want to create, you can also define the pool based on the XML. For details, see xref:directory-based-storage-pool-parameters_assembly_parameters-for-creating-storage-pools[Directory-based storage pool parameters].

. *Create the storage pool target path*
+
Use the [command]`virsh pool-build` command to create a storage pool target path for a pre-formatted file system storage pool, initialize the storage source device, and define the format of the data.
+
[subs="+quotes"]
----
# *virsh pool-build guest_images_dir*
  Pool guest_images_dir built

# *ls -la /guest_images*
  total 8
  drwx------.  2 root root 4096 May 31 19:38 .
  dr-xr-xr-x. 25 root root 4096 May 31 19:38 ..
----

. *Verify that the pool was created*
+
Use the [command]`virsh pool-list` command to verify that the pool was created.
+
[subs="+quotes"]
----
# *virsh pool-list --all*

  Name                 State      Autostart
  -----------------------------------------
  default              active     yes
  guest_images_dir     inactive   no
----

. *Start the storage pool*
+
Use the [command]`virsh pool-start` command to mount the storage pool.
+
[subs="+quotes"]
----
# *virsh pool-start guest_images_dir*
  Pool guest_images_dir started
----
+
[NOTE]
====
The [command]`virsh pool-start` command is only necessary for persistent storage pools. Transient storage pools are automatically started when they are created.
====

. Optional: Turn on autostart.
+
By default, a storage pool defined with the [command]`virsh` command is not set to automatically start each time virtualization services start. Use the [command]`virsh pool-autostart` command to configure the storage pool to autostart.
+
[subs="+quotes"]
----
# *virsh pool-autostart guest_images_dir*
  Pool guest_images_dir marked as autostarted
----

.Verification

* Use the [command]`virsh pool-info` command to verify that the storage pool is in the `_running_` state. Check if the sizes reported are as expected and if autostart is configured correctly.
+
[subs="+quotes"]
----
# *virsh pool-info guest_images_dir*
  Name:           guest_images_dir
  UUID:           c7466869-e82a-a66c-2187-dc9d6f0877d0
  State:          running
  Persistent:     yes
  Autostart:      yes
  Capacity:       458.39 GB
  Allocation:     197.91 MB
  Available:      458.20 GB
----
