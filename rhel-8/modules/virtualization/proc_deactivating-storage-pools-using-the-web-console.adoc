:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_managing-storage-for-virtual-machines-using-the-web-console.adoc

:experimental:

[id="deactivating-storage-pools-using-the-web-console_{context}"]
= Deactivating storage pools by using the web console

If you do not want to permanently delete a storage pool, you can temporarily deactivate it instead.

When you deactivate a storage pool, no new volumes can be created in that pool. However, any virtual machines (VMs) that have volumes in that pool will continue to run. This is useful for example if you want to limit the number of volumes that can be created in a pool to increase system performance.

To deactivate a storage pool by using the {ProductShortName} web console, see the following procedure.


.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* The web console VM plug-in
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[is installed on your system].
endif::virt-title[]

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. Click btn:[Storage Pools] at the top of the Virtual Machines tab. The Storage Pools window appears, showing a list of configured storage pools.
+
image::web-console-storage-pools-window.png[Image displaying all the storage pools currently configured on the host.]

. Click btn:[Deactivate] on the storage pool row.
+
The storage pool is deactivated.


[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::rhel8-virt[]
xref:understanding-storage-pools_understanding-virtual-machine-storage[Understanding storage pools]
endif::[]
ifndef::rhel8-virt[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#understanding-storage-pools_understanding-virtual-machine-storage[Understanding storage pools]
endif::[]
* {blank}
ifdef::virt-title[]
xref:viewing-storage-pool-information-using-the-web-console_assembly_managing-virtual-machine-storage-pools-using-the-web-console[Viewing storage pool information by using the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#viewing-storage-pool-information-using-the-web-console_assembly_managing-virtual-machine-storage-pools-using-the-web-console[Viewing storage pool information by using the web console]
endif::[]
