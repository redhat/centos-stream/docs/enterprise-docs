:_newdoc-version: 2.15.1
:_template-generated: 2024-01-30
:_mod-docs-content-type: PROCEDURE

[id="reverting-to-a-virtual-machine-snapshot-by-using-the-command-line-interface_{context}"]
= Reverting to a virtual machine snapshot by using the command line

[role="_abstract"]
To return a virtual machine (VM) to the state saved in a snapshot, you can use the command-line-interface (CLI).

.Prerequisites

* A snapshot of the VM is available, which you have previously created xref:creating-virtual-machine-snapshots-by-using-the-web-console_creating-virtual-machine-snapshots[in the web console] or by xref:creating-virtual-machine-snapshots-entering-metadata-on-the-command-line_creating-virtual-machine-snapshots[using the command line].

* Optional: You have created a snapshot of the current state of the VM. If you revert to a previous snapshot without saving the current state, changes performed on the VM since the last snapshot will be lost.

.Procedure

* Use the `virsh snapshot-revert` utility and specify the name of the VM and the name of the snapshot to which you want to revert. For example:
+
[subs="+quotes"]
----
# *virsh snapshot-revert Testguest2 clean-install*
Domain snapshot clean-install reverted
----

.Verification

* Display the currently active snapshot for the reverted VM:
+
[subs="+quotes"]
----
# *virsh snapshot-current Testguest2 --name*
clean-install
----

////
[role="_additional-resources"]
.Next steps

* TBA?
////

////
[role="_additional-resources"]
.Additional resources

* This section is optional.
* Provide a bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
* Use an unnumbered bullet (*) if the list includes only one step.
////
