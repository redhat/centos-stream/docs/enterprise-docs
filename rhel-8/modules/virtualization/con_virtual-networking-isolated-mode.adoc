:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// types-of-virtual-machine-network-connections

[id="virtual-networking-isolated-mode_{context}"]
= Virtual networking in isolated mode

By using _isolated_ mode, virtual machines connected to the virtual switch can communicate with each other and with the host machine, but their traffic will not pass outside of the host machine, and they cannot receive traffic from outside the host machine. Using `dnsmasq` in this mode is required for basic functionality such as DHCP.

image::vn-07-isolated-switch.png[]
