:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_introducing-virtualization-in-rhel.adoc

// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/rhel-virtual-machine-components-and-their-interaction.adoc[leveloffset=+1]

[id=rhel-virtual-machine-components-and-their-interaction_{context}]
= Virtual machine components and their interaction

Virtualization in {ProductShortName} {ProductNumber} consists of the following principal software components:

.Hypervisor

The basis of creating virtual machines (VMs) in {ProductShortName} {ProductNumber} is the _hypervisor_, a software layer that controls hardware and enables running multiple operating systems on a host machine.

The hypervisor includes the *Kernel-based Virtual Machine (KVM)* module and virtualization kernel drivers. These components ensure that the Linux kernel on the host machine provides resources for virtualization to user-space software.

At the user-space level, the *QEMU* emulator simulates a complete virtualized hardware platform that the guest operating system can run in, and manages how resources are allocated on the host and presented to the guest.

In addition, the [package]`libvirt` software suite serves as a management and communication layer, making QEMU easier to interact with, enforcing security rules, and providing a number of additional tools for configuring and running VMs.

.XML configuration

A host-based XML configuration file (also known as a _domain XML_ file) determines all settings and devices in a specific VM. The configuration includes:

* Metadata such as the name of the VM, time zone, and other information about the VM.

* A description of the devices in the VM, including virtual CPUs (vCPUS), storage devices, input/output devices, network interface cards, and other hardware, real and virtual.

* VM settings such as the maximum amount of memory it can use, restart settings, and other settings about the behavior of the VM.

For more information about the contents of an XML configuration, see xref:sample-virtual-machine-xml-configuration_viewing-information-about-virtual-machines[Sample virtual machine XML configuration].

.Component interaction

When a VM is started, the hypervisor uses the XML configuration to create an instance of the VM as a user-space process on the host. The hypervisor also makes the VM process accessible to the host-based interfaces, such as the [command]`virsh`, [command]`virt-install`, and [command]`guestfish` utilities, or the web console GUI.

When these virtualization tools are used, libvirt translates their input into instructions for QEMU. QEMU communicates the instructions to KVM, which ensures that the kernel appropriately assigns the resources necessary to carry out the instructions. As a result, QEMU can execute the corresponding user-space changes, such as creating or modifying a VM, or performing an action in the VM's guest operating system.

[NOTE]
====
While QEMU is an essential component of the architecture, it is not intended to be used directly on {ProductShortName} {ProductNumber} systems, due to security concerns. Therefore, `qemu-*` commands are not supported by Red Hat, and it is highly recommended to interact with QEMU by using libvirt.
====

For more information about the host-based interfaces, see xref:con_tools-and-interfaces-for-virtualization-management_introducing-virtualization-in-rhel[Tools and interfaces for virtualization management].

.{ProductShortName} {ProductNumber} virtualization architecture
image::virt-architecture.png[]
