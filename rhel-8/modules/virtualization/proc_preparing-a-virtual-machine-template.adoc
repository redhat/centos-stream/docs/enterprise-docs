:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// cloning-virtual-machines

[id="preparing-a-virtual-machine-template_{context}"]
= Creating a virtual machine template by using virt-sysprep

To create a cloning template from an existing virtual machine (VM), you can use the `virt-sysprep` utility. This removes certain configurations that might cause the clone to work incorrectly, such as specific network settings or system registration metadata. As a result, `virt-sysprep` makes creating clones of the VM more efficient, and ensures that the clones work more reliably.

.Prerequisites

ifeval::[{ProductNumber} == 8]
* The `libguestfs-tools-c` package, which contains the `virt-sysprep` utility, is installed on your host:
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install libguestfs-tools-c*
----
endif::[]
ifeval::[{ProductNumber} == 9]
* The `guestfs-tools` package, which contains the `virt-sysprep` utility, is installed on your host:
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install guestfs-tools*
----
endif::[]

* The source VM intended as a template is shut down.
* You know where the disk image for the source VM is located, and you are the owner of the VM's disk image file.
+
Note that disk images for VMs created in the
ifdef::virt-title[]
xref:automatic-features-for-virtual-machine-security_securing-virtual-machines-in-rhel[system connection]
endif::[]
ifndef::virt-title[system connection]
of libvirt are located in the `/var/lib/libvirt/images` directory and owned by the root user by default:
+
[subs="+quotes,attributes"]
----
# *ls -la /var/lib/libvirt/images*
-rw-------.  1 root root  9665380352 Jul 23 14:50 a-really-important-vm.qcow2
-rw-------.  1 root root  8591507456 Jul 26  2017 an-actual-vm-that-i-use.qcow2
-rw-------.  1 root root  8591507456 Jul 26  2017 totally-not-a-fake-vm.qcow2
-rw-------.  1 root root 10739318784 Sep 20 17:57 another-vm-example.qcow2
----

* Optional: Any important data on the source VM's disk has been backed up. If you want to preserve the source VM intact, xref:cloning-a-virtual-machine-using-the-command-line-interface_cloning-virtual-machines[clone] it first and turn the clone into a template.

.Procedure

. Ensure you are logged in as the owner of the VM's disk image:
+
[subs="+quotes,attributes"]
----
# *whoami*
root
----

. Optional: Copy the disk image of the VM.
+
[subs="+quotes,attributes"]
----
# *cp /var/lib/libvirt/images/a-really-important-vm.qcow2 /var/lib/libvirt/images/a-really-important-vm-original.qcow2*
----
+
This is used later to verify that the VM was successfully turned into a template.

. Use the following command, and replace _/var/lib/libvirt/images/a-really-important-vm.qcow2_ with the path to the disk image of the source VM.
+
[subs="+quotes,attributes"]
----
# *virt-sysprep -a _/var/lib/libvirt/images/a-really-important-vm.qcow2_*
[   0.0] Examining the guest ...
[   7.3] Performing "abrt-data" ...
[   7.3] Performing "backup-files" ...
[   9.6] Performing "bash-history" ...
[   9.6] Performing "blkid-tab" ...
[...]
----

.Verification

* To confirm that the process was successful, compare the modified disk image to the original one. The following example shows a successful creation of a template:
+
[subs="+quotes,attributes"]
----
# *virt-diff -a _/var/lib/libvirt/images/a-really-important-vm-orig.qcow2_ -A _/var/lib/libvirt/images/a-really-important-vm.qcow2_*
- - 0644       1001 /etc/group-
- - 0000        797 /etc/gshadow-
= - 0444         33 /etc/machine-id
[...]
- - 0600        409 /home/username/.bash_history
- d 0700          6 /home/username/.ssh
- - 0600        868 /root/.bash_history
[...]
----

[role="_additional-resources"]
.Additional resources
* The *OPERATIONS* section in the `virt-sysprep` man page on your system
* {blank}
ifdef::virt-title[]
xref:cloning-a-virtual-machine-using-the-command-line-interface_cloning-virtual-machines[Cloning a virtual machine by using the command line]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/cloning-virtual-machines_configuring-and-managing-virtualization#cloning-a-virtual-machine-using-the-command-line-interface_cloning-virtual-machines[Cloning a virtual machine by using the command line]
endif::[]
