:_newdoc-version: 2.15.0
:_template-generated: 2023-11-1
:_mod-docs-content-type: PROCEDURE

[id="creating-a-virtual-disk-image_{context}"]
= Creating a virtual disk image by using qemu-img

[role="_abstract"]
If you require creating a new virtual disk image separately from a new virtual machine (VM) and xref:creating-and-assigning-storage-volumes-using-the-cli_assembly_managing-virtual-machine-storage-volumes-using-the-cli[creating a storage volume] is not viable for you, you can use the `qemu-img` command-line utility.

// .Prerequisites
// Any required packages, maybe?

.Procedure

* Create a virtual disk image by using the `qemu-img` utility:
+
[subs="+quotes"]
----
# *qemu-img create -f _<format>_ _<image-name>_ _<size>_*
----
+
For example, the following command creates a qcow2 disk image named _test-image_ with the size of 30 gigabytes:
+
[subs="+quotes"]
----
# *qemu-img create -f qcow2 test-image 30G*

Formatting 'test-img', fmt=qcow2 cluster_size=65536 extended_l2=off compression_type=zlib size=32212254720 lazy_refcounts=off refcount_bits=16
----

.Verification

* Display the information about the image you created and check that it has the required size and does not report any corruption: 
+
[subs="+quotes"]
----
# *qemu-img info _<test-img>_*
image: test-img
file format: qcow2
virtual size: 30 GiB (32212254720 bytes)
disk size: 196 KiB
cluster_size: 65536
Format specific information:
    compat: 1.1
    compression type: zlib
    lazy refcounts: false
    refcount bits: 16
    corrupt: false
    extended l2: false

----

[role="_additional-resources"]
.Additional resources

* xref:creating-and-assigning-storage-volumes-using-the-cli_assembly_managing-virtual-machine-storage-volumes-using-the-cli[Creating and assigning storage volumes by using the CLI]
* xref:creating-and-attaching-disks-to-virtual-machines-using-the-web-console_assembly_managing-virtual-machine-storage-disks-using-the-web-console[Adding new disks to virtual machines by using the web console]
* `qemu-img` man page on your system