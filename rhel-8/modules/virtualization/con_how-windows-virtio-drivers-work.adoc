:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// installing-kvm-paravirtualized-drivers-for-rhel-virtual-machines


[id="how-windows-virtio-drivers-work_{context}"]
= How Windows virtio drivers work

Paravirtualized drivers enhance the performance of virtual machines (VMs) by decreasing I/O latency and increasing throughput to almost bare-metal levels. {RH} recommends that you use paravirtualized drivers for VMs that run I/O-heavy tasks and applications.

[package]`virtio` drivers are KVM's paravirtualized device drivers, available for Windows VMs running on KVM hosts. These drivers are provided by the [package]`virtio-win` package, which includes drivers for:

* Block (storage) devices
* Network interface controllers
* Video controllers
* Memory ballooning device
* Paravirtual serial port device
* Entropy source device
* Paravirtual panic device
* Input devices, such as mice, keyboards, or tablets
* A small set of emulated devices

[NOTE]
====
For additional information about emulated, `virtio`, and assigned devices, refer to xref:managing-virtual-devices_configuring-and-managing-virtualization[Managing virtual devices].
====

By using KVM virtio drivers, the following Microsoft{nbsp}Windows versions are expected to run similarly to physical systems:

* Windows{nbsp}Server versions: See link:https://access.redhat.com/articles/973163#rhelkvm[Certified guest operating systems for {ProductName}{nbsp}with{nbsp}KVM] in the {RH}{nbsp}Knowledgebase.

* Windows{nbsp}Desktop (non-server) versions:

** Windows{nbsp}10 (32-bit and 64-bit versions)
ifeval::[{ProductNumber} == 9]
** Windows{nbsp}11 (64-bit)
endif::[]

////
THIS SHOULD GO WITHOUT SAYING
[NOTE]
====
Network connectivity issues sometimes occur when attempting to use older virtio drivers with more recent versions of QEMU. Therefore, it is recommended to keep the drivers up-to-date.
====
////
