:_newdoc-version: 2.15.0
:_template-generated: 2023-10-18
:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_creating-virtual-machines

[id="creating-virtual-machines-using-the-command-line-interface_{context}"]
= Creating virtual machines by using the command line

[role="_abstract"]
To create a virtual machine (VM) on your {ProductShortName}{nbsp}{ProductNumber} by using the command line, use the `virt-install` utility.

.Prerequisites

ifeval::[{ProductNumber} == 8]
* Virtualization is xref:enabling-virtualization-in-rhel8_virt-getting-started[enabled] on your host system.
endif::[]
ifeval::[{ProductNumber} == 9]
* Virtualization is xref:assembly_enabling-virtualization-in-rhel-9_configuring-and-managing-virtualization[enabled] on your host system.
endif::[]

* You have a sufficient amount of system resources to allocate to your VMs, such as disk space, RAM, or CPUs. The recommended values may vary significantly depending on the intended tasks and workload of the VMs.

* An operating system (OS) installation source is available locally or on a network. This can be one of the following:
** An ISO image of an installation medium
** A disk image of an existing VM installation
+
[WARNING]
====
Installing from a host CD-ROM or DVD-ROM device is not possible in {ProductShortName}{nbsp}{ProductNumber}. If you select a CD-ROM or DVD-ROM as the installation source when using any VM installation method available in {ProductShortName}{nbsp}{ProductNumber}, the installation will fail. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/1185913[RHEL 7 or higher can't install guest OS from CD/DVD-ROM].
====
+
Also note that {RH} provides support only for xref:recommended-features-in-rhel-{ProductNumber}-virtualization_feature-support-and-limitations-in-rhel-{ProductNumber}-virtualization[a limited set of guest operating systems].

* Optional: A Kickstart file can be provided for faster and easier configuration of the installation.

.Procedure

To create a VM and start its OS installation, use the `virt-install` command, along with the following mandatory arguments:

* `--name`: the name of the new machine
* `--memory`: the amount of allocated memory
* `--vcpus`: the number of allocated virtual CPUs
* `--disk`: the type and size of the allocated storage
* `--cdrom` or `--location`: the type and location of the OS installation source

Based on the chosen installation method, the necessary options and values can vary. See the commands below for examples:

ifeval::[{ProductNumber} == 8]
[NOTE]
====
The listed commands use the VNC remote display protocol instead of the default SPICE protocol. VNC currently does not have some of the features that SPICE does, but is fully supported on RHEL 9. As a result, VMs that use VNC will not stop working if you migrate your host to RHEL 9. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/considerations_in_adopting_rhel_9/assembly_virtualization_considerations-in-adopting-rhel-9#ref_changes-to-spice_assembly_virtualization[Considerations in adopting RHEL 9].
====
endif::[]

* The following command creates a VM named *demo-guest1* that installs the Windows 10 OS from an ISO image locally stored in the */home/username/Downloads/Win10install.iso* file. This VM is also allocated with 2048 MiB of RAM and 2 vCPUs, and an 80 GiB qcow2 virtual disk is automatically configured for the VM.
+
[subs="+quotes,attributes"]
----
# *virt-install \*
ifeval::[{ProductNumber} == 8]
    *--graphics vnc \*
endif::[]
    *--name demo-guest1 --memory 2048 \*
    *--vcpus 2 --disk size=80 --os-variant win10 \*
    *--cdrom /home/username/Downloads/Win10install.iso*
----

* The following command creates a VM named *demo-guest2* that uses the */home/username/Downloads/rhel{ProductNumber}.iso* image to run a {ProductShortName}{nbsp}{ProductNumber} OS from a live CD. No disk space is assigned to this VM, so changes made during the session will not be preserved. In addition, the VM is allocated with 4096 MiB of RAM and 4 vCPUs.
+
[subs="+quotes,attributes"]
----
# *virt-install \*
ifeval::[{ProductNumber} == 8]
    *--graphics vnc \*
endif::[]
    *--name demo-guest2 --memory 4096 --vcpus 4 \*
    *--disk none --livecd --os-variant rhel{ProductNumber}.0 \*
    *--cdrom /home/username/Downloads/rhel{ProductNumber}.iso*
----

* The following command creates a {ProductShortName} {ProductNumber} VM named *demo-guest3* that connects to an existing disk image, */home/username/backup/disk.qcow2*. This is similar to physically moving a hard drive between machines, so the OS and data available to demo-guest3 are determined by how the image was handled previously. In addition, this VM is allocated with 2048 MiB of RAM and 2 vCPUs.
+
[subs="+quotes,attributes"]
----
# *virt-install \*
ifeval::[{ProductNumber} == 8]
    *--graphics vnc \*
endif::[]
    *--name demo-guest3 --memory 2048 --vcpus 2 \*
    *--os-variant rhel{ProductNumber}.0 --import \*
    *--disk /home/username/backup/disk.qcow2*
----
+
Note that the `--os-variant` option is highly recommended when importing a disk image. If it is not provided, the performance of the created VM will be negatively affected.

* The following command creates a VM named *demo-guest4* that installs from the `\http://example.com/OS-install` URL. For the installation to start successfully, the URL must contain a working OS installation tree. In addition, the OS is automatically configured by using the */home/username/ks.cfg* kickstart file. This VM is also allocated with 2048 MiB of RAM, 2 vCPUs, and a 160 GiB qcow2 virtual disk.
+
[subs="+quotes,attributes"]
----
# *virt-install \*
ifeval::[{ProductNumber} == 8]
    *--graphics vnc \*
endif::[]
    *--name demo-guest4 --memory 2048 --vcpus 2 --disk size=160 \*
    *--os-variant rhel{ProductNumber}.0 --location http://example.com/OS-install \*
    *--initrd-inject /home/username/ks.cfg --extra-args="inst.ks=file:/ks.cfg console=tty0 console=ttyS0,115200n8"*
----
+
ifeval::[{ProductNumber} == 9]
In addition, if you want to host demo-guest4 on an {ProductShortName}{nbsp}{ProductNumber} on an ARM{nbsp}64 host, include the following lines to ensure that the kickstart file installs the `kernel-64k` package:
+
[subs="+quotes,attributes"]
----
%packages
-kernel
kernel-64k
%end
----
endif::[]

* The following command creates a VM named *demo-guest5* that installs from a `{ProductShortName}{ProductNumber}.iso` image file in text-only mode, without graphics. It connects the guest console to the serial console. The VM has 16384 MiB of memory, 16 vCPUs, and 280 GiB disk. This kind of installation is useful when connecting to a host over a slow network link.
+
[subs="+quotes,attributes"]
----
# *virt-install \*
    *--name demo-guest5 --memory 16384 --vcpus 16 --disk size=280 \*
    *--os-variant rhel{ProductNumber}.0 --location {ProductShortName}{ProductNumber}.iso \*
    *--graphics none --extra-args='console=ttyS0'*
----

* The following command creates a VM named *demo-guest6*, which has the same configuration as demo-guest5, but resides on the 192.0.2.1 remote host.
+
[subs="+quotes,attributes"]
----
# *virt-install \*
    *--connect qemu+ssh://root@192.0.2.1/system --name demo-guest6 --memory 16384 \*
    *--vcpus 16 --disk size=280 --os-variant rhel{ProductNumber}.0 --location {ProductShortName}{ProductNumber}.iso \*
    *--graphics none --extra-args='console=ttyS0'*
----

ifeval::[{ProductNumber} == 9]

* The following command creates a VM named *demo-guest-7*, which has the same configuration as demo-guest5, but for its storage, it uses xref:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/configuring_and_managing_virtualization/index#attaching-dasd-devices-to-virtual-machines-on-ibm-z_managing-virtual-devices[an IBM Z DASD mediated device] `mdev_30820a6f_b1a5_4503_91ca_0c10ba12345a_0_0_29a8`, and assigns it device number `1111`.
+
[subs="+quotes,attributes"]
----
# *virt-install \*
    *--name demo-guest7 --memory 16384 --vcpus 16 --disk size=280 \*
    *--os-variant rhel{ProductNumber}.0 --location RHEL{ProductNumber}.iso --graphics none \*
    *--disk none --hostdev mdev_30820a6f_b1a5_4503_91ca_0c10ba12345a_0_0_29a8,address.type=ccw,address.cssid=0xfe,address.ssid=0x0,address.devno=0x1111,boot-order=1 \*
    *--extra-args 'rd.dasd=0.0.1111'*
----
+
Note that the name of the mediated device available for installation can be retrieved by using the `virsh nodedev-list --cap mdev` command.
endif::[]

.Verification

* If the VM is created successfully, a xref:proc_opening-a-virtual-machine-graphical-console-using-virt-viewer_assembly_connecting-to-virtual-machines[virt-viewer] window opens with a graphical console of the VM and starts the guest OS installation.

////
TBD provide info somewhere how to do the installation on GUI-incompatible systems. Advice from SME:
<danpb> i use   --vnc --noautoconsole to virt-install
<danpb> and then launch  virt-viewer from my laptop to connect to it
<danpb> ssh tunnelling is easiest way     virt-viewer -c qemu+ssh://catbus/system  microsoft-bob-vm
////

.Troubleshooting

* If `virt-install` fails with a `cannot find default network` error:
** Ensure that the `libvirt-daemon-config-network` package is installed:
+
[subs="+quotes"]
----
# *{PackageManagerCommand} info libvirt-daemon-config-network*
Installed Packages
Name         : libvirt-daemon-config-network
[...]
----

** Verify that the `libvirt` default network is active and configured to start automatically:
+
[subs="+quotes"]
----
# *virsh net-list --all*
 Name      State    Autostart   Persistent
--------------------------------------------
 default   active   yes         yes
----

** If it is not, activate the default network and set it to auto-start:
+
[subs="+quotes"]
----
# *virsh net-autostart default*
Network default marked as autostarted

# *virsh net-start default*
Network default started
----

*** If activating the default network fails with the following error, the `libvirt-daemon-config-network` package has not been installed correctly.
+
[subs="options"]
----
error: failed to get network 'default'
error: Network not found: no network with matching name 'default'
----
+
To fix this, re-install `libvirt-daemon-config-network`:
+
[subs="+quotes"]
----
# *{PackageManagerCommand} reinstall libvirt-daemon-config-network*
----

*** If activating the default network fails with an error similar to the following, a conflict has occurred between the default network's subnet and an existing interface on the host.
+
[literal]
----
error: Failed to start network default
error: internal error: Network is already in use by interface ens2
----
+
To fix this, use the `virsh net-edit default` command and change the `192.0.2.*` values in the configuration to a subnet not already in use on the host.

[role="_additional-resources"]
.Additional resources
* `virt-install (1)` man page on your system
* xref:creating-vms-and-installing-an-os-using-the-rhel-web-console_assembly_creating-virtual-machines[Creating virtual machines and installing guest operating systems by using the web console]
ifdef::virt-title[]
* xref:cloning-virtual-machines_configuring-and-managing-virtualization[Cloning virtual machines]
endif::[]
ifndef::virt-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/cloning-virtual-machines_configuring-and-managing-virtualization[Cloning virtual machines]
endif::[]
