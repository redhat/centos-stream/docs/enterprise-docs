:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_managing-nvidia-vgpu-devices

[id="proc_obtaining-nvidia-vgpu-information-about-your-system_{context}"]
= Obtaining NVIDIA vGPU information about your system

[role="_abstract"]
To evaluate the capabilities of the vGPU features available to you, you can obtain additional information about the mediated devices on your system, such as:

* How many mediated devices of a given type can be created
* What mediated devices are already configured on your system.

////
.Prerequisites

* The _mdevctl_ package is installed.
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install mdevctl*
----
////

.Procedure

* To see the available GPUs devices on your host that can support vGPU mediated devices, use the `virsh nodedev-list --cap mdev_types` command. For example, the following shows a system with two NVIDIA Quadro RTX6000 devices.
+
[subs=+quotes]
----
# *virsh nodedev-list --cap mdev_types*
pci_0000_5b_00_0
pci_0000_9b_00_0
----


* To display vGPU types supported by a specific GPU device, as well as additional metadata, use the `virsh nodedev-dumpxml` command.
+
[subs=+quotes]
----
# *virsh nodedev-dumpxml pci_0000_9b_00_0*
<device>
  <name>pci_0000_9b_00_0</name>
  <path>/sys/devices/pci0000:9a/0000:9a:00.0/0000:9b:00.0</path>
  <parent>pci_0000_9a_00_0</parent>
  <driver>
    <name>nvidia</name>
  </driver>
  <capability type='pci'>
    <class>0x030000</class>
    <domain>0</domain>
    <bus>155</bus>
    <slot>0</slot>
    <function>0</function>
    <product id='0x1e30'>TU102GL [Quadro RTX 6000/8000]</product>
    <vendor id='0x10de'>NVIDIA Corporation</vendor>
    <capability type='mdev_types'>
      <type id='nvidia-346'>
        <name>GRID RTX6000-12C</name>
        <deviceAPI>vfio-pci</deviceAPI>
        <availableInstances>2</availableInstances>
      </type>
      <type id='nvidia-439'>
        <name>GRID RTX6000-3A</name>
        <deviceAPI>vfio-pci</deviceAPI>
        <availableInstances>8</availableInstances>
      </type>
      [...]
      <type id='nvidia-440'>
        <name>GRID RTX6000-4A</name>
        <deviceAPI>vfio-pci</deviceAPI>
        <availableInstances>6</availableInstances>
      </type>
      <type id='nvidia-261'>
        <name>GRID RTX6000-8Q</name>
        <deviceAPI>vfio-pci</deviceAPI>
        <availableInstances>3</availableInstances>
      </type>
    </capability>
    <iommuGroup number='216'>
      <address domain='0x0000' bus='0x9b' slot='0x00' function='0x3'/>
      <address domain='0x0000' bus='0x9b' slot='0x00' function='0x1'/>
      <address domain='0x0000' bus='0x9b' slot='0x00' function='0x2'/>
      <address domain='0x0000' bus='0x9b' slot='0x00' function='0x0'/>
    </iommuGroup>
    <numa node='2'/>
    <pci-express>
      <link validity='cap' port='0' speed='8' width='16'/>
      <link validity='sta' speed='2.5' width='8'/>
    </pci-express>
  </capability>
</device>
----
+
////
MDEVCTL-BASED INSTRUCTIONS, REPLACED BY VIRSH FOR RHEL 8.6/9.0:
* To see the available vGPU types on your host, use the `mdevctl types` command.
+
For example, the following shows the information for a system that uses a physical Tesla T4 card under the 0000:41:00.0 PCI bus:
+
[subs=+quotes]
----
# mdevctl types
0000:41:00.0
  nvidia-222
    Available instances: 0
    Device API: vfio-pci
    Name: GRID T4-1B
    Description: num_heads=4, frl_config=45, framebuffer=1024M, max_resolution=5120x2880, max_instance=16
  nvidia-223
    Available instances: 0
    Device API: vfio-pci
    Name: GRID T4-2B
    Description: num_heads=4, frl_config=45, framebuffer=2048M, max_resolution=5120x2880, max_instance=8
  nvidia-224
    Available instances: 0
    Device API: vfio-pci
    Name: GRID T4-2B4
    Description: num_heads=4, frl_config=45, framebuffer=2048M, max_resolution=5120x2880, max_instance=8
  nvidia-225
    Available instances: 0
    Device API: vfio-pci
    Name: GRID T4-1A
    Description: num_heads=1, frl_config=60, framebuffer=1024M, max_resolution=1280x1024, max_instance=16
    [...]
----

* To see the active vGPU devices on your host, including their types, UUIDs, and PCI buses of parent devices, use the `mdevctl list` command:
+
[subs="+quotes,attributes"]
----
# *mdevctl list*
85006552-1b4b-45ef-ad62-de05be9171df 0000:41:00.0 nvidia-223
83c32df7-d52e-4ec1-9668-1f3c7e4df107 0000:41:00.0 nvidia-223 (defined)
----
+
This example shows that the `85006552-1b4b-45ef-ad62-de05be9171df` device is running but not defined, and the `83c32df7-d52e-4ec1-9668-1f3c7e4df107` is both defined and running.
////

[role="_additional-resources"]
.Additional resources
* The `man virsh` command
