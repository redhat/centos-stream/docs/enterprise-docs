:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// managing-virtual-devices

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_removing-devices-from-virtual-machines.adoc[leveloffset=+1]

[id="removing-devices-from-virtual-machines_{context}"]
= Removing devices from virtual machines

[role="_abstract"]
You can change the functionality of your virtual machines (VMs) by removing a virtual device. For example, you can remove a virtual disk device from one of your VMs if it is no longer needed.

The following procedure demonstrates how to remove virtual devices from your virtual machines (VMs) by  using the command line (CLI). Some devices, such as disks or NICs, can also be removed from VMs
ifdef::virt-title[]
xref:managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[using the {ProductShortName} {ProductNumber} web console].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[using the {ProductShortName}{nbsp}{ProductNumber} web console].
endif::[]

.Prerequisites

* Optional: Back up the XML configuration of your VM by using `virsh dumpxml _vm-name_` and sending the output to a file. For example, the following backs up the configuration of your _testguest1_ VM as the `testguest1.xml` file:

[subs="+quotes,attributes"]
----
# *virsh dumpxml testguest1  > testguest1.xml*
# *cat testguest1.xml*
<domain type='kvm' xmlns:qemu='http://libvirt.org/schemas/domain/qemu/1.0'>
  <name>testguest1</name>
  <uuid>ede29304-fe0c-4ca4-abcd-d246481acd18</uuid>
  [...]
</domain>
----

.Procedure

. Use the `virt-xml --remove-device` command, including a definition of the device. For example:
+
** The following removes the storage device marked as _vdb_ from the running _testguest_ VM after it shuts down:
+
[subs="+quotes,attributes"]
----
# *virt-xml testguest --remove-device --disk target=vdb*
Domain 'testguest' defined successfully.
Changes will take effect after the domain is fully powered off.
----
+
** The following immediately removes a USB flash drive device from the running _testguest2_ VM:
+
[subs="+quotes,attributes"]
----
# *virt-xml testguest2 --remove-device --update --hostdev type=usb*
Device hotunplug successful.
Domain 'testguest2' defined successfully.
----

.Troubleshooting

* If removing a device causes your VM to become unbootable, use the `virsh define` utility to restore the XML configuration by reloading the XML configuration file you backed up previously.
+
[subs="+quotes,attributes"]
----
# *virsh define testguest.xml*
----

[role="_additional-resources"]
.Additional resources
* The `man virt-xml` command
