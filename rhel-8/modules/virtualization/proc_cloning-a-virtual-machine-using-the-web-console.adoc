:_mod-docs-content-type: PROCEDURE
:experimental:

[id="cloning-a-virtual-machine-using-the-web-console_{context}"]
= Cloning a virtual machine by using the web console
[[proc_cloning-a-virtual-machine-using-the-web-console_cloning-virtual-machines]]

To create new virtual machines (VMs) with a specific set of properties, you can clone a VM that you had previously configured by using the web console.

[NOTE]
====
Cloning a VM also clones the disks associated with that VM.
====

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* The web console VM plug-in
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[is installed on your system].
endif::virt-title[]

* Ensure that the VM you want to clone is shut down.

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. In the Virtual Machines interface of the web console, click the Menu button btn:[⋮] of the VM that you want to clone.
+
A drop down menu appears with controls for various VM operations.
// Removing image to reduce overhead and since it doesn't provide much value
//+
//image::virt-cockpit-VM-shutdown-menu.png[The virtual machines main page displaying the available options when the VM is shut down.]
. Click btn:[Clone].
+
The Create a clone VM dialog appears.
+
image::virt-cockpit-vm-clone.png[Create a clone VM dialog box with an option to enter a new name for the VM., width=100%]
. Optional: Enter a new name for the VM clone.

. Click btn:[Clone].
+
A new VM is created based on the source VM.

.Verification

* Confirm whether the cloned VM appears in the list of VMs available on your host.
