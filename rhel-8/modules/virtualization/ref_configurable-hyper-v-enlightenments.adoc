:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// assembly_enabling-hyper-v-enlightenments

[id="configurable-hyper-v-enlightenments_{context}"]
= Configurable Hyper-V enlightenments

You can configure certain Hyper-V features to optimize Windows VMs. The following table provides information about these configurable Hyper-V features and their values.

.Configurable Hyper-V features
[options="header"]
[cols=",a,a"]
|====
|Enlightenment|Description|Values
|evmcs|Implements paravirtualized protocol between L0 (KVM) and L1 (Hyper-V) hypervisors, which enables faster L2 exits to the hypervisor.
[NOTE]
====
This feature is exclusive to Intel processors.
====
|on, off
|frequencies|Enables Hyper-V frequency Machine Specific Registers (MSRs).
|on, off
|ipi|Enables paravirtualized inter processor interrupts (IPI) support.|on, off
|reenlightenment|Notifies when there is a time stamp counter (TSC) frequency change which only occurs during migration. It also allows the guest to keep using the old frequency until it is ready to switch to the new one.|on, off
|relaxed|Disables a Windows sanity check that commonly results in a BSOD when the VM is running on a heavily loaded host. This is similar to the Linux kernel option no_timer_check, which is automatically enabled when Linux is running on KVM.|on, off
|runtime|Sets processor time spent on running the guest code, and on behalf of the guest code.|on, off
|spinlocks|* Used by a VM's operating system to notify Hyper-V that the calling virtual processor is attempting to acquire a resource that is potentially held by another virtual processor within the same partition.
* Used by Hyper-V to indicate to the virtual machine’s operating system the number of times a spinlock acquisition should be attempted before indicating an excessive spin situation to Hyper-V.|on, off
|stimer|Enables synthetic timers for virtual processors. Note that certain Windows versions revert to using HPET (or even RTC when HPET is unavailable) when this enlightenment is not provided, which can lead to significant CPU consumption, even when the virtual CPU is idle.|on, off
|stimer-direct|Enables synthetic timers when an expiration event is delivered via a normal interrupt.|on, off.
|synic|Together with stimer, activates the synthetic timer. Windows 8 uses this feature in periodic mode.|on, off
|time|Enables the following Hyper-V-specific clock sources available to the VM,

* MSR-based 82 Hyper-V clock source (HV_X64_MSR_TIME_REF_COUNT, 0x40000020)
* Reference TSC 83 page which is enabled via MSR (HV_X64_MSR_REFERENCE_TSC, 0x40000021)|on, off
|tlbflush|Flushes the TLB of the virtual processors.|on, off
|vapic|Enables virtual APIC, which provides accelerated MSR access to the high-usage, memory-mapped Advanced Programmable Interrupt Controller (APIC) registers.|on, off
ifeval::[{ProductNumber} == 9]
|vendor_id|Sets the Hyper-V vendor id.|* on, off
* Id value - string of up to 12 characters
endif::[]
|vpindex|Enables virtual processor index.|on, off
|====
