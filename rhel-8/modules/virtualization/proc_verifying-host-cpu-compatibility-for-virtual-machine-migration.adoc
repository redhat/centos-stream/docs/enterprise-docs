:_mod-docs-content-type: PROCEDURE

[id="verifying-host-cpu-compatibility-for-virtual-machine-migration_{context}"]
= Verifying host CPU compatibility for virtual machine migration

For migrated virtual machines (VMs) to work correctly on the destination host, the CPUs on the source and the destination hosts must be compatible. To ensure that this is the case, calculate a common CPU baseline before you begin the migration.

[NOTE]
====
The instructions in this section use an example migration scenario with the following host CPUs:

* Source host: Intel Core i7-8650U
* Destination hosts: Intel Xeon CPU E5-2620 v2
====

.Prerequisites

* Virtualization is 
ifeval::[{ProductNumber} == 8]
xref:enabling-virtualization-in-rhel8_virt-getting-started[installed and enabled] 
endif::[]
ifeval::[{ProductNumber} == 9]
xref:assembly_enabling-virtualization-in-rhel-9_configuring-and-managing-virtualization[installed and enabled] 
endif::[]
on your system.

* You have administrator access to the source host and the destination host for the migration.

.Procedure

. On the source host, obtain its CPU features and paste them into a new XML file, such as `domCaps-CPUs.xml`.
+
[subs="+quotes"]
----
# *virsh domcapabilities | xmllint --xpath "//cpu/mode[@name='host-model']" - > _domCaps-CPUs.xml_*
----

. In the XML file, replace the `<mode> </mode>` tags with `<cpu> </cpu>`.

. Optional: Verify that the content of the `domCaps-CPUs.xml` file looks similar to the following:
+
[subs="+quotes"]
----
# *cat _domCaps-CPUs.xml_*

    <cpu>
          <model fallback="forbid">Skylake-Client-IBRS</model>
          <vendor>Intel</vendor>
          <feature policy="require" name="ss"/>
          <feature policy="require" name="vmx"/>
          <feature policy="require" name="pdcm"/>
          <feature policy="require" name="hypervisor"/>
          <feature policy="require" name="tsc_adjust"/>
          <feature policy="require" name="clflushopt"/>
          <feature policy="require" name="umip"/>
          <feature policy="require" name="md-clear"/>
          <feature policy="require" name="stibp"/>
          <feature policy="require" name="arch-capabilities"/>
          <feature policy="require" name="ssbd"/>
          <feature policy="require" name="xsaves"/>
          <feature policy="require" name="pdpe1gb"/>
          <feature policy="require" name="invtsc"/>
          <feature policy="require" name="ibpb"/>
          <feature policy="require" name="ibrs"/>
          <feature policy="require" name="amd-stibp"/>
          <feature policy="require" name="amd-ssbd"/>
          <feature policy="require" name="rsba"/>
          <feature policy="require" name="skip-l1dfl-vmentry"/>
          <feature policy="require" name="pschange-mc-no"/>
          <feature policy="disable" name="hle"/>
          <feature policy="disable" name="rtm"/>
    </cpu>
----

. On the destination host, use the following command to obtain its CPU features:
+
[subs="+quotes"]
----
# *virsh domcapabilities | xmllint --xpath "//cpu/mode[@name='host-model']" -*
   
    <mode name="host-model" supported="yes">
            <model fallback="forbid">IvyBridge-IBRS</model>
            <vendor>Intel</vendor>
            <feature policy="require" name="ss"/>
            <feature policy="require" name="vmx"/>
            <feature policy="require" name="pdcm"/>
            <feature policy="require" name="pcid"/>
            <feature policy="require" name="hypervisor"/>
            <feature policy="require" name="arat"/>
            <feature policy="require" name="tsc_adjust"/>
            <feature policy="require" name="umip"/>
            <feature policy="require" name="md-clear"/>
            <feature policy="require" name="stibp"/>
            <feature policy="require" name="arch-capabilities"/>
            <feature policy="require" name="ssbd"/>
            <feature policy="require" name="xsaveopt"/>
            <feature policy="require" name="pdpe1gb"/>
            <feature policy="require" name="invtsc"/>
            <feature policy="require" name="ibpb"/>
            <feature policy="require" name="amd-ssbd"/>
            <feature policy="require" name="skip-l1dfl-vmentry"/>
            <feature policy="require" name="pschange-mc-no"/>
    </mode>
----

. Add the obtained CPU features from the destination host to the `domCaps-CPUs.xml` file on the source host. Again, replace the `<mode> </mode>` tags with `<cpu> </cpu>` and save the file.

. Optional: Verify that the XML file now contains the CPU features from both hosts.
+
[subs="+quotes"]
----
# *cat _domCaps-CPUs.xml_*

    <cpu>
          <model fallback="forbid">Skylake-Client-IBRS</model>
          <vendor>Intel</vendor>
          <feature policy="require" name="ss"/>
          <feature policy="require" name="vmx"/>
          <feature policy="require" name="pdcm"/>
          <feature policy="require" name="hypervisor"/>
          <feature policy="require" name="tsc_adjust"/>
          <feature policy="require" name="clflushopt"/>
          <feature policy="require" name="umip"/>
          <feature policy="require" name="md-clear"/>
          <feature policy="require" name="stibp"/>
          <feature policy="require" name="arch-capabilities"/>
          <feature policy="require" name="ssbd"/>
          <feature policy="require" name="xsaves"/>
          <feature policy="require" name="pdpe1gb"/>
          <feature policy="require" name="invtsc"/>
          <feature policy="require" name="ibpb"/>
          <feature policy="require" name="ibrs"/>
          <feature policy="require" name="amd-stibp"/>
          <feature policy="require" name="amd-ssbd"/>
          <feature policy="require" name="rsba"/>
          <feature policy="require" name="skip-l1dfl-vmentry"/>
          <feature policy="require" name="pschange-mc-no"/>
          <feature policy="disable" name="hle"/>
          <feature policy="disable" name="rtm"/>
    </cpu>
    <cpu>
          <model fallback="forbid">IvyBridge-IBRS</model>
          <vendor>Intel</vendor>
          <feature policy="require" name="ss"/>
          <feature policy="require" name="vmx"/>
          <feature policy="require" name="pdcm"/>
          <feature policy="require" name="pcid"/>
          <feature policy="require" name="hypervisor"/>
          <feature policy="require" name="arat"/>
          <feature policy="require" name="tsc_adjust"/>
          <feature policy="require" name="umip"/>
          <feature policy="require" name="md-clear"/>
          <feature policy="require" name="stibp"/>
          <feature policy="require" name="arch-capabilities"/>
          <feature policy="require" name="ssbd"/>
          <feature policy="require" name="xsaveopt"/>
          <feature policy="require" name="pdpe1gb"/>
          <feature policy="require" name="invtsc"/>
          <feature policy="require" name="ibpb"/>
          <feature policy="require" name="amd-ssbd"/>
          <feature policy="require" name="skip-l1dfl-vmentry"/>
          <feature policy="require" name="pschange-mc-no"/>
    </cpu>
----

. Use the XML file to calculate the CPU feature baseline for the VM you intend to migrate.
+
[subs="+quotes"]
----
# *virsh hypervisor-cpu-baseline _domCaps-CPUs.xml_*

    <cpu mode='custom' match='exact'>
      <model fallback='forbid'>IvyBridge-IBRS</model>
      <vendor>Intel</vendor>
      <feature policy='require' name='ss'/>
      <feature policy='require' name='vmx'/>
      <feature policy='require' name='pdcm'/>
      <feature policy='require' name='pcid'/>
      <feature policy='require' name='hypervisor'/>
      <feature policy='require' name='arat'/>
      <feature policy='require' name='tsc_adjust'/>
      <feature policy='require' name='umip'/>
      <feature policy='require' name='md-clear'/>
      <feature policy='require' name='stibp'/>
      <feature policy='require' name='arch-capabilities'/>
      <feature policy='require' name='ssbd'/>
      <feature policy='require' name='xsaveopt'/>
      <feature policy='require' name='pdpe1gb'/>
      <feature policy='require' name='invtsc'/>
      <feature policy='require' name='ibpb'/>
      <feature policy='require' name='amd-ssbd'/>
      <feature policy='require' name='skip-l1dfl-vmentry'/>
      <feature policy='require' name='pschange-mc-no'/>
    </cpu>
----

. Open the XML configuration of the VM you intend to migrate, and replace the contents of the `<cpu>` section with the settings obtained in the previous step.
+
[subs="+quotes"]
----
# *virsh edit __<vm_name>__*
----

. If the VM is running, shut down the VM and start it again.
+
[subs="+quotes"]
----
# *virsh shutdown __<vm_name>__*

# *virsh start __<vm_name>__*
----

////
.Verification

. TODO?
////

.Next steps

* xref:sharing-virtual-machine-disk-images-with-other-hosts_migrating-virtual-machines[Sharing virtual machine disk images with other hosts]

* xref:migrating-a-virtual-machine-using-the-cli_migrating-virtual-machines[Migrating a virtual machine by using the command line]

* xref:proc_live-migrating-a-virtual-machine-using-the-web-console_migrating-virtual-machines[Live-migrating a virtual machine by using the web console]