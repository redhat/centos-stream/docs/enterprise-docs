:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <powering-down-and-restarting-vms-using-the-rhel-web-console>

// :context: proc_sending_NMIs-using-the-rhel-8-web-console

:experimental:

[id="sending-NMIs-to-vms-using-the-rhel-web-console_{context}"]

= Sending non-maskable interrupts to VMs by using the web console

Sending a non-maskable interrupt (NMI) may cause an unresponsive running virtual machine (VM) to respond or shut down. For example, you can send the kbd:[Ctrl+Alt+Del] NMI to a VM that is not responding to standard input.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* The web console VM plug-in
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[is installed on your system].
endif::virt-title[]

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. In the menu:Virtual Machines[] interface, find the row of the VM to which you want to send an NMI.

. On the right side of the row, click the Menu button btn:[⋮].
+
A drop-down menu of actions appears.

. In the drop-down menu, click menu:Send non-maskable interrupt[].
+
An NMI is sent to the VM.


[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::virt-title[]
xref:powering-up-vms-using-the-rhel-web-console_assembly_starting-virtual-machines[Starting virtual machines by using the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#powering-up-vms-using-the-rhel-web-console_assembly_starting-virtual-machines[Starting virtual machines by using the web console]
endif::[]
* {blank}
ifdef::virt-title[]
xref:restarting-vms-using-the-rhel-web-console_powering-down-and-restarting-vms-using-the-rhel-web-console[Restarting virtual machines by using the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#restarting-vms-using-the-rhel-web-console_powering-down-and-restarting-vms-using-the-rhel-web-console[Restarting virtual machines by using the web console]
endif::[]
* {blank}
ifdef::virt-title[]
xref:powering-down-vms-using-the-rhel-web-console_powering-down-and-restarting-vms-using-the-rhel-web-console[Shutting down virtual machines in the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#powering-down-vms-using-the-rhel-web-console_powering-down-and-restarting-vms-using-the-rhel-web-console[Shutting down virtual machines in the web console]
endif::[]
