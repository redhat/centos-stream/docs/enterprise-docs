:experimental:
:_module-type: PROCEDURE

[id="proc_creating-a-virtual-machine-template-manually_{context}"]
= Creating a virtual machine template manually

To create a template from an existing virtual machine (VM), you can manually reset or unconfigure a guest VM to prepare it for cloning.

.Prerequisites

* Ensure that you know the location of the disk image for the source VM and are the owner of the VM’s disk image file.
+
Note that disk images for VMs created in the
ifdef::virt-title[]
xref:automatic-features-for-virtual-machine-security_securing-virtual-machines-in-rhel[system connection]
endif::[]
ifndef::virt-title[system connection]
of libvirt are by default located in the [directory]`/var/lib/libvirt/images` directory and owned by the root user:
+
[subs="+quotes,attributes"]
----
# *ls -la /var/lib/libvirt/images*
-rw-------.  1 root root  9665380352 Jul 23 14:50 a-really-important-vm.qcow2
-rw-------.  1 root root  8591507456 Jul 26  2017 an-actual-vm-that-i-use.qcow2
-rw-------.  1 root root  8591507456 Jul 26  2017 totally-not-a-fake-vm.qcow2
-rw-------.  1 root root 10739318784 Sep 20 17:57 another-vm-example.qcow2
----

* Ensure that the VM is shut down.

ifdef::virt-title[]
* Optional: Any important data on the VM’s disk has been backed up. If you want to preserve the source VM intact, xref:cloning-a-virtual-machine-using-the-command-line-interface_cloning-virtual-machines[clone] it first and edit the clone to create a template.
endif::[]
ifndef::virt-title[]
* Optional: Any important data on the VM’s disk has been backed up. If you want to preserve the source VM intact, link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/cloning-virtual-machines_configuring-and-managing-virtualization#cloning-a-virtual-machine-using-the-command-line-interface_cloning-virtual-machines[clone] it first and edit the clone to create a template.
endif::[]

.Procedure

. Configure the VM for cloning:
.. Install any software needed on the clone.
.. Configure any non-unique settings for the operating system.
.. Configure any non-unique application settings.

. Remove the network configuration:
.. Remove any persistent udev rules by using the following command:
+
[subs="+quotes,attributes"]
----
# *rm -f /etc/udev/rules.d/70-persistent-net.rules*
----
+
[NOTE]
====
If udev rules are not removed, the name of the first NIC might be `eth1` instead of `eth0`.
====

ifeval::[{ProductNumber} == 8]
.. Remove unique network details from ifcfg scripts by editing [filename]`/etc/sysconfig/network-scripts/ifcfg-eth[x]` as follows:
... Remove the HWADDR and Static lines:
+
[NOTE]
====
If the HWADDR does not match the new guest's MAC address, the `ifcfg` will be ignored.
====
+
[subs="+quotes,attributes"]
----
*DEVICE=eth[x]
BOOTPROTO=none
ONBOOT=yes
#NETWORK=192.0.2.0      <- REMOVE
#NETMASK=255.255.255.0  <- REMOVE
#IPADDR=192.0.2.1       <- REMOVE
#HWADDR=xx:xx:xx:xx:xx  <- REMOVE
#USERCTL=no             <- REMOVE
# Remove any other *unique* or non-desired settings, such as UUID.*
----
... Configure DHCP but do not include HWADDR or any other unique information:
+
[subs="+quotes,attributes"]
----
*DEVICE=eth[x]
BOOTPROTO=dhcp
ONBOOT=yes*
----

.. Ensure the following files also contain the same content, if they exist on your system:
*** [directory]`/etc/sysconfig/networking/devices/ifcfg-eth[x]`
*** [directory]`/etc/sysconfig/networking/profiles/default/ifcfg-eth[x]`
+
[NOTE]
====
If you had used `NetworkManager` or any special settings with the VM, ensure that any additional unique information is removed from the `ifcfg` scripts.
====
endif::[]
ifeval::[{ProductNumber} == 9]
.. Remove unique information from the `NMConnection` files in the `/etc/NetworkManager/system-connections/` directory.
... Remove MAC address, IP address, DNS, gateway, and any other *unique* information or non-desired settings.
+
[subs="+quotes,attributes"]
----
*ID=ExampleNetwork
BOOTPROTO="dhcp"
HWADDR="AA:BB:CC:DD:EE:FF"                  <- REMOVE
NM_CONTROLLED="yes"
ONBOOT="yes"
TYPE="Ethernet"
UUID="954bd22c-f96c-4b59-9445-b39dd86ac8ab" <- REMOVE
----
... Remove similar *unique* information and non-desired settings from the `/etc/hosts` and `/etc/resolv.conf` files.
endif::[]
. Remove registration details:
** For VMs registered on the {RH} Network (RHN):
+
[subs="+quotes,attributes"]
----
# *rm /etc/sysconfig/rhn/systemid*
----

** For VMs registered with {RH} Subscription Manager (RHSM):
*** If you do not plan to use the original VM:
+
[subs="+quotes,attributes"]
----
# *subscription-manager unsubscribe --all
# subscription-manager unregister
# subscription-manager clean*
----
*** If you plan to use the original VM:
+
[subs="+quotes,attributes"]
----
# *subscription-manager clean*
----
+
[NOTE]
====
The original RHSM profile remains in the Portal along with your ID code. Use the following command to reactivate your RHSM registration on the VM after it is cloned:
[subs="+quotes,attributes"]
----
# *subscription-manager register --consumerid=71rd64fx-6216-4409-bf3a-e4b7c7bd8ac9*
----
====

. Remove other unique details:
.. Remove SSH public and private key pairs:
+
[subs="+quotes,attributes"]
----
# *rm -rf /etc/ssh/ssh_host_example*
----
.. Remove the configuration of LVM devices:
+
[subs="+quotes,attributes"]
----
# *rm /etc/lvm/devices/system.devices*
----
+
// This prevents the issues described in https://access.redhat.com/solutions/6988988
.. Remove any other application-specific identifiers or configurations that might cause conflicts if running on multiple machines.

. Remove the `gnome-initial-setup-done` file to configure the VM to run the configuration wizard on the next boot:
+
[subs="+quotes,attributes"]
----
# *rm ~/.config/gnome-initial-setup-done*
----
+
[NOTE]
====
The wizard that runs on the next boot depends on the configurations that have been removed from the VM. In addition, on the first boot of the clone, it is recommended that you change the hostname.
====
