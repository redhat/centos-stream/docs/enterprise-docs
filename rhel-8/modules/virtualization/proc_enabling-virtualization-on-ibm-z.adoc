:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_getting-started-with-virtualization-in-rhel-8-on-ibm-z
// assembly_enabling-virtualization-in-rhel-9


[id="enabling-virtualization-on-ibm-z_{context}"]
= Enabling virtualization on IBM Z

To set up a KVM hypervisor and create virtual machines (VMs) on an IBM Z system running {ProductShortName}{nbsp}{ProductNumber}, follow the instructions below.


.Prerequisites

ifeval::[{ProductNumber} == 8]
* {ProductShortName} 8.6 or later is installed and registered on your host machine.
+
[IMPORTANT]
====
If you already enabled virtualization on an IBM Z machine by using RHEL 8.5 or earlier, you should instead reconfigure your virtualization module and update your system. For instructions, see xref:how-virtualization-on-ibm-z-differs-from-amd64-and-intel64_getting-started-with-virtualization-in-rhel-8-on-ibm-z[How virtualization on IBM Z differs from AMD64 and Intel 64].
====
endif::[]

* The following minimum system resources are available:
** 6 GB free disk space for the host, plus another 6 GB for each intended VM.
** 2 GB of RAM for the host, plus another 2 GB for each intended VM.
** 4 CPUs on the host. VMs can generally run with a single assigned vCPU, but {RH} recommends assigning 2 or more vCPUs per VM to avoid VMs becoming unresponsive during high load.

* Your IBM Z host system is using a z13 CPU or later.

* {ProductShortName}{nbsp}{ProductNumber} is installed on a logical partition (LPAR). In addition, the LPAR supports the _start-interpretive execution_ (SIE) virtualization functions.
+
To verify this, search for `sie` in your `/proc/cpuinfo` file.
+
[subs="+quotes,attributes"]
----
# *grep sie /proc/cpuinfo*
features        : esan3 zarch stfle msa ldisp eimm dfp edat etf3eh highgprs te sie
----

.Procedure

ifeval::[{ProductNumber} == 8]
. Load the KVM kernel module:
+
[subs="+quotes,attributes"]
----
# *modprobe kvm*
----

. Verify that the KVM kernel module is loaded:
+
[subs="+quotes,attributes"]
----
# *lsmod | grep kvm*
----
+
If KVM loaded successfully, the output of this command includes `kvm`.
+
//Add an output example?^


. Install the packages in the `virt:rhel/common` module:
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} module install virt:rhel/common*
----

endif::[]

ifeval::[{ProductNumber} == 9]
. Install the virtualization packages:
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install qemu-kvm libvirt virt-install*
----
endif::[]

. Start the virtualization services:
+
[subs="+quotes,attributes"]
----
# *for drv in qemu network nodedev nwfilter secret storage interface; do systemctl start virt${drv}d{,-ro,-admin}.socket; done*
----

.Verification

. Verify that your system is prepared to be a virtualization host.
+
[subs="+quotes,attributes"]
----
# *virt-host-validate*
[...]
QEMU: Checking if device /dev/kvm is accessible             : PASS
QEMU: Checking if device /dev/vhost-net exists              : PASS
QEMU: Checking if device /dev/net/tun exists                : PASS
QEMU: Checking for cgroup 'memory' controller support       : PASS
QEMU: Checking for cgroup 'memory' controller mount-point   : PASS
[...]
----
+
// Add a more IBM Z-specific output, if needed?

. If all *virt-host-validate* checks return a `PASS` value, your system is prepared for
ifeval::[{ProductNumber} == 8]
xref:assembly_creating-virtual-machines_virt-getting-started[creating VMs].
endif::[]
ifeval::[{ProductNumber} == 9]
xref:assembly_creating-virtual-machines_configuring-and-managing-virtualization[creating VMs].
endif::[]
+
If any of the checks return a `FAIL` value, follow the displayed instructions to fix the problem.
+
If any of the checks return a `WARN` value, consider following the displayed instructions to improve virtualization capabilities.

.Troubleshooting

* If KVM virtualization is not supported by your host CPU, *virt-host-validate* generates the following output:
+
[subs="+quotes,attributes"]
----
QEMU: Checking for hardware virtualization: FAIL (Only emulated CPUs are available, performance will be significantly limited)
----
+
However, VMs on such a host system will fail to boot, rather than have performance problems.
+
To work around this, you can change the `<domain type>` value in the XML configuration of the VM to `qemu`. Note, however, that {RH} does not support VMs that use the `qemu` domain type, and setting this is highly discouraged in production environments.

ifeval::[{ProductNumber} == 9]
[role="_additional-resources"]
.Additional resources
* xref:how-virtualization-on-ibm-z-differs-from-amd64-and-intel64_feature-support-and-limitations-in-rhel-9-virtualization[How virtualization on IBM Z differs from AMD64 and Intel 64]
endif::[]
