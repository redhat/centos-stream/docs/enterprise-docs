:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_shutting-down-virtual-machines

:experimental:

[id="shutting-down-a-virtual-machine-using-the-command-line-interface_{context}"]

= Shutting down a virtual machine by using the command line

Shutting down a virtual machine (VM) requires different steps based on whether the VM is reponsive.

.Shutting down a responsive VM

* If you are
ifeval::[{ProductNumber} == 8]
xref:assembly_connecting-to-virtual-machines_virt-getting-started[connected to the guest], 
endif::[]
ifeval::[{ProductNumber} == 9]
xref:assembly_connecting-to-virtual-machines_configuring-and-managing-virtualization[connected to the guest], 
endif::[]
use a shutdown command or a GUI element appropriate to the guest operating system.
+
[NOTE]
====
In some environments, such as in Linux guests that use the GNOME Desktop, using the GUI power button for suspending or hibernating the guest might instead shut down the VM.
====
+
// Look into how general this is, or if endemic to Gnome Desktop.


* Alternatively, use the `virsh shutdown` command on the host:

** If the VM is on a local host:
+
[subs="+quotes"]
----
# *virsh shutdown _demo-guest1_*
Domain 'demo-guest1' is being shutdown
----

** If the VM is on a remote host, in this example _192.0.2.1_:
+
[subs="+quotes"]
----
# *virsh -c qemu+ssh://root@192.0.2.1/system shutdown _demo-guest1_*

root@192.0.2.1's password:
Domain 'demo-guest1' is being shutdown
----

.Shutting down an unresponsive VM

To force a VM to shut down, for example if it has become unresponsive, use the `virsh destroy` command on the host:

[subs="+quotes"]
----
# *virsh destroy _demo-guest1_*
Domain 'demo-guest1' destroyed
----

[NOTE]
====
The `virsh destroy` command does not actually delete or remove the VM configuration or disk images. It only terminates the running instance of the VM, similarly to pulling the power cord from a physical machine. 

In rare cases, `virsh destroy` may cause corruption of the VM’s file system, so using this command is only recommended if all other shutdown methods have failed.
====


.Verification

* On the host, display the list of your VMs to see their status.
+
[subs="+quotes"]
----
# *virsh list --all*

 Id    Name                 State
------------------------------------------
 1     demo-guest1          *shut off*
----