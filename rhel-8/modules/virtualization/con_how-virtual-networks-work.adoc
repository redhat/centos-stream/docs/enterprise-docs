:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// understanding-virtual-networking-overview

[id="how-virtual-networks-work_{context}"]
= How virtual networks work

Virtual networking uses the concept of a virtual network switch. A virtual network switch is a software construct that operates on a host machine. VMs connect to the network through the virtual network switch. Based on the configuration of the virtual switch, a VM can use an existing virtual network managed by the hypervisor, or a different network connection method.

The following figure shows a virtual network switch connecting two VMs to the network:

image::vn-02-switchandtwoguests.png[]

From the perspective of a guest operating system, a virtual network connection is the same as a physical network connection. Host machines view virtual network switches as network interfaces. When the
ifeval::[{ProductNumber} == 8]
`libvirtd`
endif::[]
ifeval::[{ProductNumber} == 9]
`virtnetworkd`
endif::[]
service is first installed and started, it creates *virbr0*, the default network interface for VMs.

To view information about this interface, use the `ip` utility on the host.

[subs="+quotes,attributes"]
----
$ *ip addr show _virbr0_*
3: virbr0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state
UNKNOWN link/ether 1b:c4:94:cf:fd:17 brd ff:ff:ff:ff:ff:ff
inet 192.0.2.1/24 brd 192.0.2.255 scope global virbr0
----

By default, all VMs on a single host are connected to the same xref:virtual-networking-network-address-translation_types-of-virtual-machine-network-connections[NAT-type] virtual network, named *default*, which uses the *virbr0* interface. For details, see xref:virtual-networking-default-configuration_understanding-virtual-networking-overview[Virtual networking default configuration].

For basic outbound-only network access from VMs, no additional network setup is usually needed, because the default network is installed along with the [package]`libvirt-daemon-config-network` package, and is automatically started when the
ifeval::[{ProductNumber} == 8]
`libvirtd`
endif::[]
ifeval::[{ProductNumber} == 9]
`virtnetworkd`
endif::[]
service is started.

If a different VM network functionality is needed, you can create additional virtual networks and network interfaces and configure your VMs to use them. In addition to the default NAT, these networks and interfaces can be configured to use one of the following modes:

* xref:virtual-networking-routed-mode_types-of-virtual-machine-network-connections[Routed mode]

* xref:virtual-networking-bridged-mode_types-of-virtual-machine-network-connections[Bridged mode]

* xref:virtual-networking-isolated-mode_types-of-virtual-machine-network-connections[Isolated mode]

* xref:virtual-networking-open-mode_types-of-virtual-machine-network-connections[Open mode]

// NOT REALLY NECESSARY INFO? The virtual network uses network address translation (NAT) to assign IP address ranges to virtual networks and `dnsmasq` to automatically assign IP addresses to virtual machine network interface cards (NICs) and to connect to a domain name service (DNS).

////
.TBD?
////
