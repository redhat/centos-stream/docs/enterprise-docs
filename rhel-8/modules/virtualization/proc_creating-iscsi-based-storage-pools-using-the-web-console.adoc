:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_managing-storage-for-virtual-machines-using-the-web-console.adoc

[id="proc_creating-iscsi-based-storage-pools-using-the-web-console_{context}"]
= Creating iSCSI-based storage pools by using the web console

An iSCSI-based storage pool is based on the Internet Small Computer Systems Interface (iSCSI), an IP-based storage networking standard for linking data storage facilities.


.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* The web console VM plug-in
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[is installed on your system].
endif::virt-title[]

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. In the {ProductShortName} web console, click btn:[Storage pools] in the *Virtual Machines* tab.
+
The *Storage pools* window appears, showing a list of configured storage pools, if any.
+
image::web-console-storage-pools-window.png[Image displaying all the storage pools currently configured on the host, if any.]

. Click btn:[Create storage pool].
+
The *Create storage pool* dialog appears.

. Enter a name for the storage pool.

. In the *Type* drop down menu, select *iSCSI target*.
+
image::virt-cockpit-create-iscsi-storage-pool.png[Image displaying the Create storage pool dialog box., width=100%]

. Enter the rest of the information:
+
* *Target Path* - The path specifying the target. This will be the path used for the storage pool.
* *Host* - The hostname or IP address of the ISCSI server.
* *Source path* - The unique iSCSI Qualified Name (IQN) of the iSCSI target.
* *Startup* - Whether or not the storage pool starts when the host boots.

. Click btn:[Create].
+
The storage pool is created. The *Create storage pool* dialog closes, and the new storage pool appears in the list of storage pools.

[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::rhel8-virt[]
xref:understanding-storage-pools_understanding-virtual-machine-storage[Understanding storage pools]
endif::[]
ifndef::rhel8-virt[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#understanding-storage-pools_understanding-virtual-machine-storage[Understanding storage pools]
endif::[]
* {blank}
ifdef::virt-title[]
xref:viewing-storage-pool-information-using-the-web-console_assembly_managing-virtual-machine-storage-pools-using-the-web-console[Viewing storage pool information by using the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#viewing-storage-pool-information-using-the-web-console_assembly_managing-virtual-machine-storage-pools-using-the-web-console[Viewing storage pool information by using the web console]
endif::[]
