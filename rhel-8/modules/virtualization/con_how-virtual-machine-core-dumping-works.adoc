:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_dumping-a-virtual-machine-core-for-later-analysis.adoc

[id="how-virtual-machine-core-dumping-works_{context}"]
= How virtual machine core dumping works

A virtual machine (VM) requires numerous running processes to work accurately and efficiently. In some cases, a running VM may terminate unexpectedly or malfunction while you are using it. Restarting the VM may cause the data to be reset or lost, which makes it difficult to diagnose the exact problem that caused the VM to crash.

In such cases, you can use the `virsh dump` utility to save (or _dump_) the core of a VM to a file before you reboot the VM. The core dump file contains a raw physical memory image of the VM which contains detailed information about the VM. This information can be used to diagnose VM problems, either manually, or by using a tool such as the `crash` utility.


[role="_additional-resources"]
.Additional resources
* `crash` man page on your system
* The link:https://github.com/crash-utility/crash[`crash` Github repository]
