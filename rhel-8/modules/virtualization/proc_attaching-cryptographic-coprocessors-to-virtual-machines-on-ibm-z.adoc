:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// securing-virtual-machines-in-rhel

[id="attaching-cryptographic-coprocessors-to-virtual-machines-on-ibm-z_{context}"]
= Attaching cryptographic coprocessors to virtual machines on IBM Z

[role="_abstract"]
To use hardware encryption in your virtual machine (VM) on an IBM Z host, create mediated devices from a cryptographic coprocessor device and assign them to the intended VMs. For detailed instructions, see below.


.Prerequisites

* Your host is running on IBM Z hardware.

* The cryptographic coprocessor is compatible with device assignment. To confirm this, ensure that the `type` of your coprocessor is listed as `CEX4` or later.
+
[subs="+quotes,attributes"]
----
# *lszcrypt -V*

CARD.DOMAIN TYPE  MODE        STATUS  REQUESTS  PENDING HWTYPE QDEPTH FUNCTIONS  DRIVER
--------------------------------------------------------------------------------------------
05         *CEX5C* CCA-Coproc  online         1        0     11     08 S--D--N--  cex4card
05.0004    *CEX5C* CCA-Coproc  online         1        0     11     08 S--D--N--  cex4queue
05.00ab    *CEX5C* CCA-Coproc  online         1        0     11     08 S--D--N--  cex4queue
----

* The `vfio_ap` kernel module is loaded. To verify, use:
+
[subs="+quotes,attributes"]
----
# *lsmod | grep vfio_ap*
vfio_ap         24576  0
[...]
----
+
To load the module, use:
+
[subs="+quotes,attributes"]
----
# *modprobe vfio_ap*
----

* The `s390utils` version supports `ap` handling:
+
[subs="+quotes"]
----
# *lszdev --list-types*
...
ap           Cryptographic Adjunct Processor (AP) device
...
----


.Procedure

. Obtain the decimal values for the devices that you want to assign to the VM. For example, for the devices `05.0004` and `05.00ab`:
+
[subs="+quotes"]
----
# *echo "obase=10; ibase=16; 04" | bc*
4
# *echo "obase=10; ibase=16; AB" | bc*
171
----

.  On the host, reassign the devices to the `vfio-ap` drivers:
+
[subs="+quotes"]
----
# *chzdev -t ap apmask=-5 aqmask=-4,-171*
----
+
[NOTE]
====
To assign the devices persistently, use the `-p` flag.
====

. Verify that the cryptographic devices have been reassigned correctly.
+
[subs="+quotes,attributes"]
----
# *lszcrypt -V*

CARD.DOMAIN TYPE  MODE        STATUS  REQUESTS  PENDING HWTYPE QDEPTH FUNCTIONS  DRIVER
--------------------------------------------------------------------------------------------
05          CEX5C CCA-Coproc  -              1        0     11     08 S--D--N--  cex4card
05.0004     CEX5C CCA-Coproc  -              1        0     11     08 S--D--N--  *vfio_ap*
05.00ab     CEX5C CCA-Coproc  -              1        0     11     08 S--D--N--  *vfio_ap*
----
+
If the DRIVER values of the domain queues changed to `vfio_ap`, the reassignment succeeded.

. Create an XML snippet that defines a new mediated device.
+
The following example shows defining a persistent mediated device and assigning queues to it. Specifically, the `vfio_ap.xml` XML snippet in this example assigns a domain adapter `0x05`, domain queues `0x0004` and `0x00ab`, and a control domain `0x00ab` to the mediated device.
+
[subs="+quotes,attributes"]
----
# *vim vfio_ap.xml*

<device>
  <parent>ap_matrix</parent>
  <capability type="mdev">
    <type id="vfio_ap-passthrough"/>
    <attr name='assign_adapter' value='0x05'/>
    <attr name='assign_domain' value='0x0004'/>
    <attr name='assign_domain' value='0x00ab'/>
    <attr name='assign_control_domain' value='0x00ab'/>
  </capability>
</device>
----

. Create a new mediated device from the `vfio_ap.xml` XML snippet.
+
[subs="+quotes,attributes"]
----
# *virsh nodedev-define vfio_ap.xml*
Node device 'mdev_8f9c4a73_1411_48d2_895d_34db9ac18f85_matrix' defined from 'vfio_ap.xml'
----

. Start the mediated device that you created in the previous step, in this case `mdev_8f9c4a73_1411_48d2_895d_34db9ac18f85_matrix`.
+
[subs="+quotes,attributes"]
----
# *virsh nodedev-start mdev_8f9c4a73_1411_48d2_895d_34db9ac18f85_matrix*
Device mdev_8f9c4a73_1411_48d2_895d_34db9ac18f85_matrix started
----

. Check that the configuration has been applied correctly
+
[subs="+quotes,attributes"]
----
# *cat /sys/devices/vfio_ap/matrix/mdev_supported_types/vfio_ap-passthrough/devices/_669d9b23-fe1b-4ecb-be08-a2fabca99b71_/matrix*
05.0004
05.00ab
----
+
If the output contains the numerical values of queues that you have previously assigned to `vfio-ap`, the process was successful.

. Attach the mediated device to the VM.

.. Display the UUID of the mediated device that you created and save it for the next step.
+
[subs="+quotes,attributes"]
----
# *virsh nodedev-dumpxml mdev_8f9c4a73_1411_48d2_895d_34db9ac18f85_matrix*

<device>
  <name>mdev_8f9c4a73_1411_48d2_895d_34db9ac18f85_matrix</name>
  <parent>ap_matrix</parent>
  <capability type='mdev'>
    <type id='vfio_ap-passthrough'/>
    *<uuid>8f9c4a73-1411-48d2-895d-34db9ac18f85</uuid>*
    <iommuGroup number='0'/>
    <attr name='assign_adapter' value='0x05'/>
    <attr name='assign_domain' value='0x0004'/>
    <attr name='assign_domain' value='0x00ab'/>
    <attr name='assign_control_domain' value='0x00ab'/>
  </capability>
</device>
----

.. Create and open an XML file for the cryptographic card mediated device. For example:
+
[subs="+quotes"]
----
# *vim crypto-dev.xml*
----

.. Add the following lines to the file and save it. Replace the `uuid` value with the UUID you obtained in step _a_.
+
[source, XML]
----
<hostdev mode='subsystem' type='mdev' managed='no' model='vfio-ap'>
  <source>
    <address uuid='8f9c4a73-1411-48d2-895d-34db9ac18f85'/>
  </source>
</hostdev>
----

.. Use the XML file to attach the mediated device to the VM. For example, to permanently attach a device defined in the `crypto-dev.xml` file to the running `testguest1` VM:
+
[subs="+quotes"]
----
# *virsh attach-device testguest1 crypto-dev.xml --live --config*
----
+
The `--live` option attaches the device to a running VM only, without persistence between boots. The `--config` option makes the configuration changes persistent. You can use the `--config` option alone to attach the device to a shut-down VM.
+
Note that each UUID can only be assigned to one VM at a time.

.Verification

. Ensure that the guest operating system detects the assigned cryptographic devices.
+
[subs="+quotes,attributes"]
----
# *lszcrypt -V*

CARD.DOMAIN TYPE  MODE        STATUS  REQUESTS  PENDING HWTYPE QDEPTH FUNCTIONS  DRIVER
--------------------------------------------------------------------------------------------
05          CEX5C CCA-Coproc  online         1        0     11     08 S--D--N--  cex4card
05.0004     CEX5C CCA-Coproc  online         1        0     11     08 S--D--N--  cex4queue
05.00ab     CEX5C CCA-Coproc  online         1        0     11     08 S--D--N--  cex4queue
----
+
The output of this command in the guest operating system will be identical to that on a host logical partition with the same cryptographic coprocessor devices available.

. In the guest operating system, confirm that a control domain has been successfully assigned to the cryptographic devices.
+
[subs="+quotes,attributes"]
----
# *lszcrypt -d C*

DOMAIN 00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f
------------------------------------------------------
    00  .  .  .  .  U  .  .  .  .  .  .  .  .  .  .  .
    10  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    20  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    30  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    40  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    50  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    60  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    70  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    80  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    90  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    a0  .  .  .  .  .  .  .  .  .  .  .  B  .  .  .  .
    b0  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    c0  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    d0  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    e0  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    f0  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
------------------------------------------------------
C: Control domain
U: Usage domain
B: Both (Control + Usage domain)
----
+
If `lszcrypt -d C` displays `U` and `B` intersections in the cryptographic device matrix, the control domain assignment was successful.

// .Additional resources
