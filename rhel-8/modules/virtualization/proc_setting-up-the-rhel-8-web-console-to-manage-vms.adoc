
// <managing-virtual-machines-it-the-web-console>

// :context: proc_using-the-rhel-8-web-console-for-managing-virtual-machines

// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:experimental:

:_mod-docs-content-type: PROCEDURE
[id="setting-up-the-rhel-web-console-to-manage-vms_{context}"]

= Setting up the web console to manage virtual machines

[role="_abstract"]
Before using the {ProductShortName}{nbsp}{ProductNumber} web console to manage virtual machines (VMs), you must install the web console virtual machine plug-in on the host.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

.Procedure

* Install the [package]`cockpit-machines` plug-in.
+
[subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install cockpit-machines*
----

.Verification

include::common-content/snip_web-console-log-in-step.adoc[]

. If the installation was successful, menu:Virtual Machines[] appears in the web console side menu.
+
image::virt-cockpit-main-page.png[Image displaying the virtual machine tab of the web console.]

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/index[Managing systems by using the {ProductShortName}{nbsp}{ProductNumber} web console]

