:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Creating and assigning LVM-based storage for virtual machines using the CLI

// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/proc_creating-lvm-based-storage-pools-using-the-cli.adoc[leveloffset=+1]

[id="creating-lvm-based-storage-pools-using-the-cli_{context}"]
= Creating LVM-based storage pools by using the CLI

[role="_abstract"]
If you want to have a storage pool that is part of an LVM volume group, you can use the `virsh` utility to create LVM-based storage pools.

.Recommendations

Be aware of the following before creating an LVM-based storage pool:

* LVM-based storage pools do not provide the full flexibility of LVM.
* `libvirt` supports thin logical volumes, but does not provide the features of thin storage pools.
* LVM-based storage pools are volume groups. You can create volume groups by using the `virsh` utility, but this way you can only have one device in the created volume group. To create a volume group with multiple devices, use the LVM utility instead, see link:https://www.redhat.com/sysadmin/create-volume-group[How to create a volume group in Linux with LVM].
+
For more detailed information about volume groups, refer to the _{ProductName} Logical Volume Manager Administration Guide_.
* LVM-based storage pools require a full disk partition. If you activate a new partition or device by using `virsh` commands, the partition will be formatted and all data will be erased. If you are using a host's existing volume group, as in these procedures, nothing will be erased.

.Prerequisites

* Ensure your hypervisor supports LVM-based storage pools:
+
[subs="+quotes"]
----
# *virsh pool-capabilities | grep "'logical' supported='yes'"*
----
+
If the command displays any output, LVM-based pools are supported.

.Procedure

. *Create a storage pool*
+
Use the [command]`virsh pool-define-as` command to define and create an LVM-type storage pool.
For example, the following command creates a storage pool named `guest_images_lvm` that uses the `lvm_vg` volume group and is mounted on the `/dev/lvm_vg` directory:
+
[subs="+quotes,attributes"]
----
# *virsh pool-define-as guest_images_lvm logical --source-name lvm_vg --target /dev/lvm_vg*
Pool guest_images_lvm defined
----
+
If you already have an XML configuration of the storage pool you want to create, you can also define the pool based on the XML. For details, see xref:lvm-based-storage-pool-parameters_assembly_parameters-for-creating-storage-pools[LVM-based storage pool parameters].

. *Verify that the pool was created*
+
Use the [command]`virsh pool-list` command to verify that the pool was created.
+
[subs="+quotes"]
----
# *virsh pool-list --all*

  Name                   State      Autostart
  -------------------------------------------
  default                active     yes
  guest_images_lvm       inactive   no
----

. *Start the storage pool*
+
Use the [command]`virsh pool-start` command to mount the storage pool.
+
[subs="+quotes"]
----
# *virsh pool-start guest_images_lvm*
  Pool guest_images_lvm started
----
+
[NOTE]
====
The [command]`virsh pool-start` command is only necessary for persistent storage pools. Transient storage pools are automatically started when they are created.
====

. Optional: Turn on autostart.
+
By default, a storage pool defined with the [command]`virsh` command is not set to automatically start each time virtualization services start. Use the [command]`virsh pool-autostart` command to configure the storage pool to autostart.
+
[subs="+quotes"]
----
# *virsh pool-autostart guest_images_lvm*
  Pool guest_images_lvm marked as autostarted
----

.Verification

* Use the [command]`virsh pool-info` command to verify that the storage pool is in the `_running_` state. Check if the sizes reported are as expected and if autostart is configured correctly.
+
[subs="+quotes"]
----
# *virsh pool-info guest_images_lvm*
  Name:           guest_images_lvm
  UUID:           c7466869-e82a-a66c-2187-dc9d6f0877d0
  State:          running
  Persistent:     yes
  Autostart:      yes
  Capacity:       458.39 GB
  Allocation:     197.91 MB
  Available:      458.20 GB
----
