:_newdoc-version: 2.15.0
:_template-generated: 2023-10-23

:_mod-docs-content-type: REFERENCE

[id="virtualization-features-that-require-qemu-guest-agent_{context}"]
= Virtualization features that require QEMU Guest Agent

[role="_abstract"]
If you enable QEMU Guest Agent (GA) on a virtual machine (VM), you can use the following commands on your host to manage the VM:

`virsh shutdown --mode=agent`:: This shutdown method is more reliable than `virsh shutdown --mode=acpi`, because `virsh shutdown` used with QEMU GA is guaranteed to shut down a cooperative guest in a clean state.

// SNAPSHOTS CURRENTLY UNSUPPORTED -> TODO: UNCOMMENT WITH https://issues.redhat.com/browse/RHELDOCS-16948
// `virsh snapshot-create --quiesce`:: Allows the guest to flush its I/O into a stable state before the snapshot is created, which allows use of the snapshot without having to perform a fsck or losing partial database transactions. The guest agent allows a high level of disk contents stability by providing guest co-operation.

`virsh domfsfreeze` and `virsh domfsthaw`:: Freezes the guest file system in isolation.

`virsh domfstrim`:: Instructs the guest to trim its file system, which helps to reduce the data that needs to be transferred during migrations.
+
[IMPORTANT]
====
If you want to use this command to manage a Linux VM, you must also set the following SELinux boolean in the guest operating system:

[subs="+quotes"]
----
# *setsebool  virt_qemu_ga_read_nonsecurity_files on*
----
====

`virsh domtime`:: Queries or sets the guest's clock.

`virsh setvcpus --guest`:: Instructs the guest to take CPUs offline, which is useful when CPUs cannot be hot-unplugged.

`virsh domifaddr --source agent`:: Queries the guest operating system's IP address by using QEMU GA. For example, this is useful when the guest interface is directly attached to a host interface.

`virsh domfsinfo`:: Shows a list of mounted file systems in the running guest.

`virsh set-user-password`:: Sets the password for a given user account in the guest.

`virsh set-user-sshkeys`:: Edits the authorized SSH keys file for a given user in the guest.
+
[IMPORTANT]
====
If you want to use this command to manage a Linux VM, you must also set the following SELinux boolean in the guest operating system:

[subs="+quotes"]
----
# *setsebool virt_qemu_ga_manage_ssh on*
----
====

[role="_additional-resources"]
.Additional resources
* xref:installing-qemu-guest-agent-on-linux-guests_enabling-qemu-guest-agent-features-on-your-virtual-machines[Enabling QEMU Guest Agent on Linux guests]
* xref:proc_installing-qemu-guest-agent-on-windows-guests_enabling-qemu-guest-agent-features-on-your-virtual-machines[Enabling QEMU Guest Agent on Windows guests]
