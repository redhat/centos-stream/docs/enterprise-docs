:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// installing-kvm-paravirtualized-drivers-for-rhel-virtual-machines

:experimental:

[id="proc_updating-kvm-drivers-on-a-windows-guest_{context}"]
= Updating virtio drivers on a Windows guest

To update KVM `virtio` drivers on a Windows guest operating system (OS), you can use the `Windows{nbsp}Update` service, if the Windows OS version supports it. If it does not, reinstall the drivers from `virtio` driver installation media attached to the Windows virtual machine (VM).

.Prerequisites

* A Windows guest OS with xref:installing-kvm-drivers-on-a-windows-guest_installing-kvm-paravirtualized-drivers-for-rhel-virtual-machines[virtio drivers installed].
* If not using `Windows{nbsp}Update`, an installation medium with up-to-date KVM `virtio` drivers must be attached to the Windows VM. For instructions on preparing the medium, see xref:installing-kvm-drivers-on-a-host-machine_installing-kvm-paravirtualized-drivers-for-rhel-virtual-machines[Preparing virtio driver installation media on a host machine].

.Procedure 1: Updating the drivers by using Windows{nbsp}Update

On Windows{nbsp}10, Windows{nbsp}Server{nbsp}2016 and later operating systems, check if the driver updates are available by using the `Windows{nbsp}Update` graphical interface:

. Start the Windows VM and log in to its guest OS.
. Navigate to the *Optional updates* page:
+
*Settings → Windows Update → Advanced options → Optional updates*
. Install all updates from *{RH},{nbsp}Inc.*

.Procedure 2: Updating the drivers by reinstalling them

On operating systems prior to Windows{nbsp}10 and Windows{nbsp}Server{nbsp}2016, or if the OS does not have access to `Windows{nbsp}Update`, reinstall the drivers. This restores the Windows guest OS network configuration to default (DHCP). If you want to preserve a customized network configuration, you also need to create a backup and restore it by using the `netsh` utility:

. Start the Windows VM and log in to its guest OS.
. Open the Windows{nbsp}Command{nbsp}Prompt:
.. Use the kbd:[Super+R] keyboard shortcut.
.. In the window that appears, type `cmd` and press kbd:[Ctrl+Shift+Enter] to run as administrator.

. Back up the OS network configuration by using the Windows Command Prompt:
+
[subs="+quotes"]
----
*C:\WINDOWS\system32\netsh dump > backup.txt*
----

. Reinstall KVM `virtio` drivers from the attached installation media. Do one of the following:

** Reinstall the drivers by using the Windows{nbsp}Command{nbsp}Prompt, where _X_ is the installation media drive letter. The following commands install all `virtio` drivers.

*** If using a 64-bit vCPU:
+
[subs="+quotes"]
--
*C:\WINDOWS\system32\msiexec.exe /i _X_:\virtio-win-gt-x64.msi /passive /norestart*
--

*** If using a 32-bit vCPU:
+
[subs="+quotes"]
----
*C:\WINDOWS\system32\msiexec.exe /i _X_:\virtio-win-gt-x86.msi /passive /norestart*
----

** Reinstall the drivers xref:installing-kvm-drivers-on-a-windows-guest_installing-kvm-paravirtualized-drivers-for-rhel-virtual-machines[using the graphical interface] without rebooting the VM.

. Restore the OS network configuration using the Windows{nbsp}Command{nbsp}Prompt:
+
[subs="+quotes"]
----
*C:\WINDOWS\system32\netsh -f backup.txt*
----

. Reboot the VM to complete the driver installation.

[role="_additional-resources"]
.Additional resources
* link:https://docs.microsoft.com/en-us/windows/deployment/update/windows-update-overview[Microsoft documentation on Windows{nbsp}Update]
