// Module included in the following assemblies:
//
// booting-virtual-machines-from-a-pxe-server

:_module-type: PROCEDURE

[id="proc_booting-virtual-machines-using-pxe-and-a-virtual-network_{context}"]
= Booting virtual machines by using PXE and a virtual network

[role="_abstract"]
To boot virtual machines (VMs) from a Preboot Execution Environment (PXE) server available on a virtual network, you must enable PXE booting.

.Prerequisites

* A PXE boot server is set up on the virtual network as described in
ifdef::virt-title[]
xref:proc_setting-up-a-pxe-boot-server-on-a-virtual-network_assembly_booting-virtual-machines-from-a-pxe-server[Setting up a PXE boot server on a virtual network].
endif::[]
ifndef::virt-title[]
// TODO Link
Setting up a PXE boot server on a virtual network.
endif::[]

.Procedure

* Create a new VM with PXE booting enabled. For example, to install from a PXE, available on the `default` virtual network, into a new 10 GB qcow2 image file:
+
[subs=+quotes]
----
# *virt-install --pxe --network network=default --memory 2048 --vcpus 2 --disk size=10*
----

** Alternatively, you can manually edit the XML configuration file of an existing VM. To do so, ensure the guest network is configured to use your virtual network and that the network is configured to be the primary boot device:
+
[subs=+quotes]
----
<interface *type='network'*>
   <mac address='52:54:00:66:79:14'/>
   <source network='default'/>
   <target dev='vnet0'/>
   <alias name='net0'/>
   <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
   *<boot order='1'/>*
</interface>
----

.Verification

* Start the VM by using the `virsh start` command. If PXE is configured correctly, the VM boots from a boot image available on the PXE server.
