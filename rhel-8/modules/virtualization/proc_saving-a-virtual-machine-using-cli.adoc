:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Saving and restoring virtual machines

[id="saving-a-virtual-machines-using-cli_{context}"]
= Saving a virtual machine by using the command line

You can save a virtual machine (VM) and its current state to the host’s disk. This is useful, for example, when you need to use the host’s resources for some other purpose. The saved VM can then be quickly restored to its previous running state.

To save a VM by using the command line, follow the procedure below.

.Prerequisites

* Ensure you have sufficient disk space to save the VM and its configuration. Note that the space occupied by the VM depends on the amount of RAM allocated to that VM.

* Ensure the VM is persistent.
+
// TODO how? Using virsh dumpxml, perhaps?

* Optional: Back up important data from the VM if required.


.Procedure

* Use the `virsh managedsave` utility.
+
For example, the following command stops the _demo-guest1_ VM and saves its configuration.
+
[subs="+quotes,attributes"]
----
# *virsh managedsave demo-guest1*
Domain 'demo-guest1' saved by libvirt
----
+
The saved VM file is located by default in the */var/lib/libvirt/qemu/save* directory as *demo-guest1.save*.
+
The next time the VM is xref:starting-a-virtual-machine-using-the-command-line-interface_saving-and-restoring-virtual-machines[started], it will automatically restore the saved state from the above file.

.Verification

* List the VMs that have managed save enabled. In the following example, the VMs listed as _saved_ have their managed save enabled.
+
[subs="+quotes,attributes"]
----
# *virsh list --managed-save --all*
Id    Name                           State
----------------------------------------------------
-     demo-guest1                    saved
-     demo-guest2                    shut off
----
+
To list the VMs that have a managed save image:
+
[subs="+quotes,attributes"]
----
# *virsh list --with-managed-save --all*
Id    Name                           State
----------------------------------------------------
-     demo-guest1                    shut off
----
+
Note that to list the saved VMs that are in a shut off state, you must use the `--all` or `--inactive` options with the command.

.Troubleshooting

* If the saved VM file becomes corrupted or unreadable, restoring the VM will initiate a standard VM boot instead.


[role="_additional-resources"]
.Additional resources
* The `virsh managedsave --help` command
* {blank}
ifdef::virt-title[]
xref:starting-a-virtual-machine-using-the-command-line-interface_saving-and-restoring-virtual-machines[Restoring a saved VM by using the command line]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/saving-and-restoring-virtual-machines_configuring-and-managing-virtualization#starting-a-virtual-machine-using-the-command-line-interface_saving-and-restoring-virtual-machines[Restoring a saved VM by using the command line]
endif::[]
* {blank}
ifdef::virt-title[]
xref:powering-up-vms-using-the-rhel-web-console_saving-and-restoring-virtual-machines[Restoring a saved VM by using the web console]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/saving-and-restoring-virtual-machines_configuring-and-managing-virtualization#powering-up-vms-using-the-rhel-web-console_saving-and-restoring-virtual-machines[Restoring a saved VM by using the web console]
endif::[]
