:_newdoc-version: 2.15.1
:_template-generated: 2024-02-06

:_mod-docs-content-type: REFERENCE

[id="support-limitations-for-virtual-machine-snapshots_{context}"]
= Support limitations for virtual machine snapshots

[role="_abstract"]
Red Hat supports the snapshot functionality for virtual machines (VMs) on RHEL only when you use *external* snapshots. Currently, external snapshots are created on RHEL only when all of the following requirements are met:

* Your host is using RHEL 9.4 or later.
* The VM is using file-based storage.
* You create the VM snapshot only in one of the following scenarios:
** The VM is shut-down.
** If the VM is running, you use the `--disk-only --quiesce` options or the `--live --memspec` options.

Most other configurations create *internal* snapshots, which are deprecated in RHEL 9. Internal snapshots might work for your use case, but Red Hat does not provide full testing and support for them. 

[WARNING]
====
Do not use internal snapshots in production environments.
====

To ensure that a snapshot is supported, display the XML configuration of the snapshot and check the snapshot type and storage: 

[subs="+quotes"]
----
# *virsh snapshot-dumpxml _<vm-name>_ _<snapshot-name>_*
----

* Example output of a supported snapshot:
+
[source,xml,subs="+quotes"]
----
<domainsnapshot>
  <name>sample-snapshot-name-1<name>
  <state>shutoff</state>
  <creationTime>1706658764</creationTime>
  <memory snapshot='*no*'/>
  <disks>
    <disk name='vda' snapshot='*external*' type='*file*'>
      <driver type='qcow2'/>
      <source file='/var/lib/libvirt/images/vm-name.sample-snapshot-name-1'/>
    </disk>
  </disks>
  <domain type='kvm'>
  [...]
----

* Example output of an unsupported snapshot:
+
[source,xml,subs="+quotes"]
----
<domainsnapshot>
  <name>sample-snapshot-name-2</name>
  <state>running</state>
  <creationTime>1653396424</creationTime>
  <memory snapshot=**'internal'**/>
  <disks>
    <disk name='vda' snapshot='*internal*'/>
    <disk name='sda' snapshot='no'/>
  </disks>
  <domain type='kvm'>
  [...]
----

////
[role="_additional-resources"]
.Additional resources
* TBA?
////
