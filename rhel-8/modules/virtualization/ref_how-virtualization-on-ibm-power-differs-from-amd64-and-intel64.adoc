:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// assembly_getting-started-with-virtualization-in-rhel-8-on-ibm-power

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_how-virtualization-on-ibm-power-differs-from-amd64-and-intel64.adoc[leveloffset=+1]

[id="how-virtualization-on-ibm-power-differs-from-amd64-and-intel64_{context}"]
= How virtualization on IBM POWER differs from AMD64 and Intel 64

KVM virtualization in {ProductShortName}{nbsp}{ProductNumber} on IBM POWER systems is different from KVM on AMD64 and Intel 64 systems in a number of aspects, notably:

Memory requirements:: VMs on IBM POWER consume more memory. Therefore, the recommended minimum memory allocation for a virtual machine (VM) on an IBM POWER host is 2GB RAM.

Display protocols:: The SPICE protocol is not supported on IBM POWER systems. To display the graphical output of a VM, use the `VNC` protocol. In addition, only the following virtual graphics card devices are supported:
+
* `vga` - only supported in `-vga std` mode and not in `-vga cirrus` mode.
+
* `virtio-vga`
+
* `virtio-gpu`

SMBIOS:: SMBIOS configuration is not available.

Memory allocation errors:: POWER8 VMs, including compatibility mode VMs, may fail with an error similar to:
+
[literal]
--
qemu-kvm: Failed to allocate KVM HPT of order 33 (try smaller maxmem?): Cannot allocate memory
--
+
This is significantly more likely to occur on VMs that use {ProductShortName} 7.3 and prior as the guest OS.
+
To fix the problem, increase the CMA memory pool available for the guest's hashed page table (HPT) by adding `kvm_cma_resv_ratio=_memory_` to the host's kernel command line, where _memory_ is the percentage of the host memory that should be reserved for the CMA pool (defaults to 5).

Huge pages:: Transparent huge pages (THPs) do not provide any notable performance benefits on IBM POWER8 VMs. However, IBM POWER9 VMs can benefit from THPs as expected.
+
In addition, the size of static huge pages on IBM POWER8 systems are 16 MiB and 16 GiB, as opposed to 2 MiB and 1 GiB on AMD64, Intel 64, and IBM POWER9. As a consequence, to migrate a VM configured with static huge pages from an IBM POWER8 host to an IBM POWER9 host, you must first set up 1GiB huge pages on the VM.
+
// TBD link to instructions, which should be a part of https://projects.engineering.redhat.com/browse/RHELPLAN-3588

kvm-clock:: The `kvm-clock` service does not have to be configured for time management in VMs on IBM POWER9.

pvpanic:: IBM POWER9 systems do not support the `pvpanic` device. However, an equivalent functionality is available and activated by default on this architecture. To enable it in a VM, use the `<on_crash>` XML configuration element with the `preserve` value.
+
In addition, make sure to remove the `<panic>` element from the `<devices>` section, as its presence can lead to the VM failing to boot on IBM POWER systems.


Single-threaded host:: On IBM POWER8 systems, the host machine must run in *single-threaded mode* to support VMs. This is automatically configured if the _qemu-kvm_ packages are installed. However, VMs running on single-threaded hosts can still use multiple threads.

Peripheral devices:: A number of peripheral devices supported on AMD64 and Intel 64 systems are not supported on IBM POWER systems, or a different device is supported as a replacement.
+
* Devices used for PCI-E hierarchy, including `ioh3420` and `xio3130-downstream`, are not supported. This functionality is replaced by multiple independent PCI root bridges provided by the `spapr-pci-host-bridge` device.
+
* UHCI and EHCI PCI controllers are not supported. Use OHCI and XHCI controllers instead.
+
* IDE devices, including the virtual IDE CD-ROM (`ide-cd`) and the virtual IDE disk (`ide-hd`), are not supported. Use the `virtio-scsi` and `virtio-blk` devices instead.
+
* Emulated PCI NICs (`rtl8139`) are not supported. Use the `virtio-net` device instead.
+
* Sound devices, including `intel-hda`, `hda-output`, and `AC97`, are not supported.
+
* USB redirection devices, including `usb-redir` and `usb-tablet`, are not supported.

v2v and p2v:: The `virt-v2v` and `virt-p2v` utilities are only supported on the AMD64 and Intel 64 architecture, and are not provided on IBM POWER.

.Additional sources

* For a comparison of selected supported and unsupported virtualization features across system architectures supported by {RH}, see xref:an-overview-of-virtualization-features-support-in-rhel-8_feature-support-and-limitations-in-rhel-8-virtualization[An overview of virtualization features support in {ProductShortName}{nbsp}{ProductNumber}].
