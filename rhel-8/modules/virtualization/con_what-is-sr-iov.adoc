:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// Managing SR-IOV devices - context "managing-sr-iov-devices"

// This module can be included from assemblies using the following include statement:
// include::<path>/con_what-is-sr-iov.adoc[leveloffset=+1]

[id="what-is-sr-iov_{context}"]
= What is SR-IOV?

[role="_abstract"]
Single-root I/O virtualization (SR-IOV) is a specification that enables a single PCI Express (PCIe) device to present multiple separate PCI devices, called _virtual functions_ (VFs), to the host system. Each of these devices:

* Is able to provide the same or similar service as the original PCIe device.
* Appears at a different address on the host PCI bus.
* Can be assigned to a different VM by using VFIO assignment.

For example, a single SR-IOV capable network device can present VFs to multiple VMs. While all of the VFs use the same physical card, the same network connection, and the same network cable, each of the VMs directly controls its own hardware network device, and uses no extra resources from the host.

.How SR-IOV works

The SR-IOV functionality is possible thanks to the introduction of the following PCIe functions:

* *Physical functions (PFs)* - A PCIe function that provides the functionality of its device (for example networking) to the host, but can also create and manage a set of VFs. Each SR-IOV capable device has one or more PFs.

* *Virtual functions (VFs)* - Lightweight PCIe functions that behave as independent devices. Each VF is derived from a PF. The maximum number of VFs a device can have depends on the device hardware. Each VF can be assigned only to a single VM at a time, but a VM can have multiple VFs assigned to it.

VMs recognize VFs as virtual devices. For example, a VF created by an SR-IOV network device appears as a network card to a VM to which it is assigned, in the same way as a physical network card appears to the host system.

.SR-IOV architecture
image::virt_SR-IOV.png[]

.Advantages

The primary advantages of using SR-IOV VFs rather than emulated devices are:

* Improved performance
* Reduced use of host CPU and memory resources

For example, a VF attached to a VM as a vNIC performs at almost the same level as a physical NIC, and much better than paravirtualized or emulated NICs. In particular, when multiple VFs are used simultaneously on a single host, the performance benefits can be significant.

// In addition, using VFs can provide increased data protection, as all data transferred between VMs connected using SR-IOV is handled by the host hardware.

.Disadvantages

* To modify the configuration of a PF, you must first change the number of VFs exposed by the PF to zero. Therefore, you also need to remove the devices provided by these VFs from the VM to which they are assigned.

* A VM with an VFIO-assigned devices attached, including SR-IOV VFs, cannot be migrated to another host. In some cases, you can work around this limitation by pairing the assigned device with an emulated device. For example, you can link:https://access.redhat.com/solutions/67546[bond] (Red Hat Knowledgebase) an assigned networking VF to an emulated vNIC, and remove the VF before the migration.

* In addition, VFIO-assigned devices require pinning of VM memory, which increases the memory consumption of the VM and prevents the use of memory ballooning on the VM.

[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::virt-title[]
xref:supported-devices-for-sr-iov-assignment-in-rhel_managing-sr-iov-devices[Supported devices for SR-IOV assignment]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-devices_configuring-and-managing-virtualization#supported-devices-for-sr-iov-assignment-in-rhel_managing-sr-iov-devices[Supported devices for SR-IOV assignment]
endif::[]
* link:https://www.ibm.com/docs/en/linux-on-systems?topic=vfio-pass-through-pci[Configuring passthrough PCI devices on IBM Z]
