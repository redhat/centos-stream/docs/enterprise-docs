:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// Creating and assigning disk-based storage for virtual machines using the CLI

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_disk-based-storage-pool-parameters.adoc[leveloffset=+1]

[id="disk-based-storage-pool-parameters_{context}"]
= Disk-based storage pool parameters

[role="_abstract"]
When you want to create or modify a disk-based storage pool by using an XML configuration file, you must include certain required parameters. See the following table for more information about these parameters.

You can use the [command]`virsh pool-define` command to create a storage pool based on the XML configuration in a specified file. For example:

[subs="+quotes"]
----
# *virsh pool-define ~/guest_images.xml*
  Pool defined from guest_images_disk
----

.Parameters

The following table provides a list of required parameters for the XML file for a disk-based storage pool.

.Disk-based storage pool parameters
[options="header"]
|===
|Description|XML
|The type of storage pool|`<pool type='disk'>`
|The name of the storage pool|`<name>``_name_``</name>`
|The path specifying the storage device. For example, `/dev/sdb`.
|`<source> +
  &nbsp;&nbsp;&nbsp;<path>``_source_path_``</path> +
</source>`
|The path specifying the target device. This will be the path used for the storage pool.
|`<target> +
  &nbsp;&nbsp;&nbsp;<path>``_target_path_``</path> +
</target>`
|===

.Example

The following is an example of an XML file for a disk-based storage pool:

[source,xml]
----
<pool type='disk'>
  <name>phy_disk</name>
  <source>
    <device path='/dev/sdb'/>
    <format type='gpt'/>
  </source>
  <target>
    <path>/dev</path>
  </target>
</pool>
----


[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::virt-title[]
xref:creating-disk-based-storage-pools-using-the-cli_assembly_managing-virtual-machine-storage-pools-using-the-cli[Creating disk-based storage pools by using the CLI]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#creating-disk-based-storage-pools-using-the-cli_assembly_managing-virtual-machine-storage-pools-using-the-cli[Creating disk-based storage pools by using the CLI]
endif::[]
