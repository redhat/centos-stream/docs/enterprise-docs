// Module included in the following assemblies:
//
// managing-virtual-machines-in-the-web-console

:_mod-docs-content-type: CONCEPT
[id='overview-of-virtual-machine-management-using-the-web-console_{context}']
= Overview of virtual machine management by using the web console

[role="_abstract"]
The {ProductShortName} {ProductNumber} web console is a web-based interface for system administration. As one of its features, the web console provides a graphical view of virtual machines (VMs) on the host system, and makes it possible to create, access, and configure these VMs.

Note that to use the web console to manage your VMs on {ProductShortName} {ProductNumber}, you must first install
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[a web console plug-in]
endif::virt-title[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization#setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[a web console plug-in]
endif::virt-title[]
for virtualization.

.Next steps
* For instructions on enabling VMs management in your web console, see
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[Setting up the web console to manage virtual machines].
endif::virt-title[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/managing-virtual-machines-in-the-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[Setting up the web console to manage virtual machines].
endif::virt-title[]

* For a comprehensive list of VM management actions that the web console provides, see
ifdef::virt-title[]
xref:virtual-machine-management-features-available-in-the-web-console_managing-virtual-machines-in-the-web-console[Virtual machine management features available in the web console].
endif::virt-title[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/managing-virtual-machines-in-the-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#virtual-machine-management-features-available-in-the-web-console_managing-virtual-machines-in-the-web-console[Virtual machine management features available in the web console].
endif::virt-title[]

ifeval::[{ProductNumber} == 8]
* For a list of features that are currently not available in the web console but can be used in the *virt-manager* application, see
ifdef::virt-title[]
xref:differences-between-virtualization-features-in-virtual-machine-manager-and-the-rhel-8-web-console_managing-virtual-machines-in-the-web-console[Differences between virtualization features in Virtual Machine Manager and the web console].
endif::virt-title[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_systems_using_the_rhel_8_web_console/managing-virtual-machines-in-the-web-console_system-management-using-the-rhel-8-web-console#differences-between-virtualization-features-in-virtual-machine-manager-and-the-rhel-8-web-console_managing-virtual-machines-in-the-web-console[Differences between virtualization features in Virtual Machine Manager and the web console].
endif::virt-title[]
endif::[]
