// Module included in the following assemblies:
//
//managing-virtual-optical-drives

:_mod-docs-content-type: PROCEDURE

[id="removing-a-cd-rom-from-a-running-virtual-machine_{context}"]
= Removing a CD-ROM from a running virtual machine by using the web console

You can use the web console to eject a CD-ROM device from a running virtual machine (VM).

.Prerequisites

* {blank}
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[You have installed the web console VM plug-in on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[You have installed the web console VM plug-in on your system].
endif::virt-title[]

.Procedure

. In the menu:Virtual Machines[] interface, click the VM from which you want to remove the CD-ROM.

. Scroll to menu:Disks[].
+
The Disks section displays information about the disks assigned to the VM, as well as options to *Add* or *Edit* disks.
+
image::virt-cockpit-eject-cdrom.png[Image displaying the disks section of the VM.]

. Click the btn:[Eject] option for the *cdrom* device.
+
The *Eject media from VM?* dialog box opens.

. Click btn:[Eject].

.Verification

* In the menu:Virtual Machines[] interface, the attached file is no longer displayed under the *Disks* section.
