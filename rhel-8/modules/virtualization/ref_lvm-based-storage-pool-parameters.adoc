:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// Creating and assigning LVM-based storage for virtual machines using the CLI

// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/ref_lvm-based-storage-pool-parameters.adoc[leveloffset=+1]

[id="lvm-based-storage-pool-parameters_{context}"]
= LVM-based storage pool parameters

[role="_abstract"]
When you want to create or modify an LVM-based storage pool by using an XML configuration file, you must include certain required parameters. See the following table for more information about these parameters.

You can use the [command]`virsh pool-define` command to create a storage pool based on the XML configuration in a specified file. For example:

[subs="+quotes"]
----
# *virsh pool-define ~/guest_images.xml*
  Pool defined from guest_images_logical
----

.Parameters

The following table provides a list of required parameters for the XML file for a LVM-based storage pool.

.LVM-based storage pool parameters
[options="header"]
|===
|Description|XML
|The type of storage pool|`<pool type='logical'>`
|The name of the storage pool|`<name>``_name_``</name>`
|The path to the device for the storage pool|`<source>  +
&nbsp;&nbsp;&nbsp;<device path='_device_path_' />``
|The name of the volume group|&nbsp;&nbsp;&nbsp; `<name>``_VG-name_``</name>`
|The virtual group format|&nbsp;&nbsp;&nbsp; `<format type='lvm2' />   +
</source>`
|The target path|`<target>  +
&nbsp;&nbsp;&nbsp;<path=_target_path_ /> +
</target>`
|===

[NOTE]
====
If the logical volume group is made of multiple disk partitions, there may be multiple source devices listed. For example:
----
<source>
  <device path='/dev/sda1'/>
  <device path='/dev/sdb3'/>
  <device path='/dev/sdc2'/>
  ...
</source>
----
====

.Example

The following is an example of an XML file for a storage pool based on the specified LVM:

[source,xml]
----
<pool type='logical'>
  <name>guest_images_lvm</name>
  <source>
    <device path='/dev/sdc'/>
    <name>libvirt_lvm</name>
    <format type='lvm2'/>
  </source>
  <target>
    <path>/dev/libvirt_lvm</path>
  </target>
</pool>
----


[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::virt-title[]
xref:creating-lvm-based-storage-pools-using-the-cli_assembly_managing-virtual-machine-storage-pools-using-the-cli[Creating LVM-based storage pools by using the CLI]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#creating-lvm-based-storage-pools-using-the-cli_assembly_managing-virtual-machine-storage-pools-using-the-cli[Creating LVM-based storage pools by using the CLI]
endif::[]
