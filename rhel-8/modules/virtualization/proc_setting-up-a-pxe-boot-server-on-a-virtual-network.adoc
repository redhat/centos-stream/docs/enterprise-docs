// Module included in the following assemblies:
//
// booting-virtual-machines-from-a-pxe-server

:_module-type: PROCEDURE

[id="proc_setting-up-a-pxe-boot-server-on-a-virtual-network_{context}"]
= Setting up a PXE boot server on a virtual network

[role="_abstract"]
This procedure describes how to configure a `libvirt` virtual network to provide Preboot Execution Environment (PXE). This enables virtual machines on your host to be configured to boot from a boot image available on the virtual network.

.Prerequisites
* A local PXE server (DHCP and TFTP), such as:
** libvirt internal server
** manually configured dhcpd and tftpd
** dnsmasq
** Cobbler server
* PXE boot images, such as `PXELINUX` configured by Cobbler or manually.

.Procedure

. Place the PXE boot images and configuration in `/var/lib/tftpboot` folder.

. Set folder permissions:
+
[subs="+quotes"]
----
# *chmod -R a+r /var/lib/tftpboot*
----

. Set folder ownership:
+
[subs="+quotes"]
----
# *chown -R nobody: /var/lib/tftpboot*
----

. Update SELinux context:
+
[subs="+quotes"]
----
# *chcon -R --reference /usr/sbin/dnsmasq /var/lib/tftpboot*
# *chcon -R --reference /usr/libexec/libvirt_leaseshelper /var/lib/tftpboot*
----

. Shut down the virtual network:
+
[subs="+quotes"]
----
# *virsh net-destroy default*
----

. Open the virtual network configuration file in your default editor:
+
[subs="+quotes"]
----
# *virsh net-edit default*
----

. Edit the `<ip>` element to include the appropriate address, network mask, DHCP address range, and boot file, where _example-pxelinux_ is the name of the boot image file.
+
[source,xml,subs="+quotes"]
----
<ip address='192.0.2.1' netmask='255.255.255.0'>
   <tftp root='/var/lib/tftpboot'/>
   <dhcp>
      <range start='192.0.2.2' end='192.0.2.254' />
      <bootp file='*_example-pxelinux_*'/>
   </dhcp>
</ip>
----

. Start the virtual network:
+
[subs="+quotes"]
----
# *virsh net-start default*
----

.Verification

* Verify that the `default` virtual network is active:
+
[subs="+quotes"]
----
# *virsh net-list*
Name             State    Autostart   Persistent
---------------------------------------------------
default          active   no          no
----

[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html-single/performing_an_advanced_rhel_8_installation/index#network-install-overview_preparing-for-a-network-install[Preparing to install from the network by using PXE]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/interactively_installing_rhel_over_the_network/preparing-for-a-network-install_rhel-installer[Preparing a PXE installation source]
endif::[]
