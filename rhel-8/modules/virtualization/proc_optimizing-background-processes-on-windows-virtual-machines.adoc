:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_optimizing-windows-virtual-machines-on-rhel

[id="optimizing-background-processes-on-windows-virtual-machines_{context}"]
= Optimizing background processes on Windows virtual machines

To optimize the performance of a virtual machine (VM) running a Windows OS, you can configure or disable a variety of Windows processes.

[WARNING]
====
Certain processes might not work as expected if you change their configuration.
====

.Procedure

You can optimize your Windows VMs by performing any combination of the following:

* Remove unused devices, such as USBs or CD-ROMs, and disable the ports.

//** Removing bullet since RHEL specific driver updates are now being delivered via Win updates. See Germano's comment on RHELPLAN-103641
////
* Disable automatic Windows Update. For more information about how to do so, see link:https://docs.microsoft.com/en-us/windows-server/administration/windows-server-update-services/deploy/4-configure-group-policy-settings-for-automatic-updates[Configure Group Policy Settings for Automatic Updates] or link:https://docs.microsoft.com/en-us/windows/deployment/update/waas-wufb-group-policy[ Configure Windows Update for Business].
+
Note that Windows Update is essential for installing latest updates and hotfixes from Microsoft. As such, Red Hat does not recommend disabling Windows Updates
////

* Disable background services, such as SuperFetch and Windows Search. For more information about stopping services, see link:https://docs.microsoft.com/en-us/windows-server/security/windows-services/security-guidelines-for-disabling-system-services-in-windows-server[Disabling system services] or link:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/stop-service?view=powershell-7[Stop-Service].
* Disable `useplatformclock`. To do so, run the following command,
+
[subs="+quotes,attributes"]
----
# *bcdedit /set useplatformclock No*
----
* Review and disable unnecessary scheduled tasks, such as scheduled disk defragmentation. For more information about how to do so, see link:https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/disable-scheduledtask?view=win10-ps[Disable Scheduled Tasks].
* Make sure the disks are not encrypted.
* Reduce periodic activity of server applications. You can do so by editing the respective timers. For more information, see link:https://docs.microsoft.com/en-us/windows/win32/multimedia/multimedia-timers[Multimedia Timers].
* Close the Server Manager application on the VM.
* Disable the antivirus software. Note that disabling the antivirus might compromise the security of the VM.
* Disable the screen saver.
* Keep the Windows OS on the sign-in screen when not in use.
