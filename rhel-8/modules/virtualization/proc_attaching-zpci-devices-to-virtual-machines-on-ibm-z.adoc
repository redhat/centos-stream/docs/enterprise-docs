:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// managing-virtual-devices

[id="attaching-zpci-devices-to-virtual-machines-on-ibm-z_{context}"]
= Attaching PCI devices to virtual machines on IBM Z

[role="_abstract"]
By using the `vfio-pci` device driver, you can assign PCI devices in pass-through mode to your virtual machines (VMs) on IBM Z hosts. This for example makes it possible for the VM to use NVMe flash disks for handling databases.

.Prerequisites

* You have a host system with the IBM Z hardware architecture.

* You have a target VM of Linux operating system.

* The necessary `vfio` kernel modules have been loaded on the host.
+
[subs="+quotes,attributes"]
----
# *lsmod | grep vfio*
----
+
The output of this command must contain the following modules:
+
** `vfio_pci`
** `vfio_pci_core`
** `vfio_iommu_type1`


.Procedure

. Obtain the PCI address identifier of the device that you want to use.
+
[subs="+quotes"]
----
# *lspci -nkD*

0000:00:00.0 0000: 1014:04ed
	Kernel driver in use: ism
	Kernel modules: ism
0001:00:00.0 0000: 1014:04ed
	Kernel driver in use: ism
	Kernel modules: ism
0002:00:00.0 0200: 15b3:1016
	Subsystem: 15b3:0062
	Kernel driver in use: mlx5_core
	Kernel modules: mlx5_core
0003:00:00.0 0200: 15b3:1016
	Subsystem: 15b3:0062
	Kernel driver in use: mlx5_core
	Kernel modules: mlx5_core
----

. Open the XML configuration of the VM to which you want to attach the PCI device.
+
[subs="+quotes"]
----
# *virsh edit _vm-name_*
----

. Add the following `<hostdev>` configuration to the `<devices>` section of the XML file.
+
Replace the values on the `address` line with the PCI address of your device. For example, if the device address is `0003:00:00.0`, use the following configuration:
+
[subs="+quotes"]
----
<hostdev mode="subsystem" type="pci" managed="yes">
  <driver name="vfio"/>
   <source>
    <address domain="_0x0003_" bus="_0x00_" slot="_0x00_" function="_0x0_"/>
   </source>
   <address type="pci"/> 
</hostdev>
----

. Optional: To modify how the guest operating system will detect the PCI device, you can also add a `<zpci>` sub-element to the `<address>` element. In the `<zpci>` line, you can adjust the `uid` and `fid` values, which modifies the PCI address and function ID of the device in the guest operating system.
+
[subs="+quotes"]
----
<hostdev mode="subsystem" type="pci" managed="yes">
  <driver name="vfio"/>
   <source>
    <address domain="_0x0003_" bus="_0x00_" slot="_0x00_" function="_0x0_"/>
   </source>
   <address type="pci"> 
     <zpci uid="0x0008" fid="0x001807"/>
   </address>
</hostdev>
----
+
In this example:
+
* `uid="0x0008"` sets the domain PCI address of the device in the VM to `0008:00:00.0`.
* `fid="0x001807"` sets the slot value of the device to `0x001807`. As a result, the device configuration in the file system of the VM is saved to `/sys/bus/pci/slots/00001087/address`.
+
If these values are not specified, `libvirt` configures them automatically.

. Save the XML configuration.

. If the VM is running, shut it down.
+
[subs="+quotes"]
----
# *virsh shutdown _vm-name_*
----

.Verification

. Start the VM and log in to its guest operating system.

. In the guest operating system, confirm that the PCI device is listed.
+
For example, if the device address is `0003:00:00.0`, use the following command:
+
[subs="+quotes"]
----
# *lspci -nkD | grep _0003:00:00.0_*

0003:00:00.0 8086:9a09 (rev 01)
----