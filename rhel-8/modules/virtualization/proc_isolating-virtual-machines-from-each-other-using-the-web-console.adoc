:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// recommended-virtual-machine-networking-configurations-using-the-web-console

:experimental:

[id="isolating-virtual-machines-from-each-other-using-the-web-console_{context}"]
= Isolating virtual machines from each other by using the web console

To prevent a virtual machine (VM) from communicating with other VMs on your host, for example to avoid data sharing or to increase system security, you can completely isolate the VM from host-side network traffic.

By default, a newly created VM connects to a NAT-type network that uses `virbr0`, the default virtual bridge on the host. This ensures that the VM can use the host's NIC for connecting to outside networks, as well as to other VMs on the host. This is a generally secure connection, but in some cases, connectivity to the other VMs may be a security or data privacy hazard. In such situations, you can isolate the VM by using direct `macvtap` connection in private mode instead of the default network.

In private mode, the VM is visible to external systems and can receive a public IP on the host's subnet, but the VM and the host cannot access each other, and the VM is also not visible to other VMs on the host.

Following instructions set up `macvtap` private mode on your VM by using the web console.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* The web console VM plug-in
ifdef::virt-title[]
xref:setting-up-the-rhel-web-console-to-manage-vms_managing-virtual-machines-in-the-web-console[is installed on your system].
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization[is installed on your system].
endif::virt-title[]

* An
ifeval::[{ProductNumber} == 8]
xref:assembly_creating-virtual-machines_virt-getting-started[existing VM]
endif::[]
ifeval::[{ProductNumber} == 9]
xref:assembly_creating-virtual-machines_configuring-and-managing-virtualization[existing VM]
endif::[]
with the default NAT setup.

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. In the *Virtual Machines* pane, click the row with the virtual machine you want to isolate.
+
A pane with the basic information about the VM opens.
. Click the *Network Interfaces* tab.
. Click btn:[Edit].
+
The `Virtual Machine Interface Settings` dialog opens.
. Set *Interface Type* to *Direct Attachment*
. Set *Source* to the host interface of your choice.
+
Note that the interface you select will vary depending on your use case and the network configuration on your host.
////

ALTERNATIVE SOLUTION - DETACH THE INTERFACE:
. In the Virtual Machines pane, click the row with the virtual machine you want to isolate.
. Click the Network Interfaces tab.
. Click the `Plug` button.

.Verification

* Verify the state of the VM's network interace in the guest OS. For example, on a Linux VM:
+
[subs="+quotes,attributes"]
----
# *ip addr*
[...]
virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
----
////

.Verification

. Start the VM by clicking btn:[Run].

. In the *Terminal* pane of the web console, list the interface statistics for the VM. For example, to view the network interface traffic for the _panic-room_ VM:
+
[subs="+quotes,attributes"]
----
# *virsh domstats _panic-room_ --interface*
Domain: 'panic-room'
  net.count=1
  net.0.name=macvtap0
  net.0.rx.bytes=0
  net.0.rx.pkts=0
  net.0.rx.errs=0
  net.0.rx.drop=0
  net.0.tx.bytes=0
  net.0.tx.pkts=0
  net.0.tx.errs=0
  net.0.tx.drop=0
----
+
If the command displays similar output, the VM has been isolated successfully.


[role="_additional-resources"]
.Additional resources
* {blank}
ifdef::virt-title[]
xref:isolating-virtual-machines-from-each-other-using-the-command-line-interface_recommended-virtual-machine-networking-configurations-using-the-command-line-interface[Isolating virtual machines from each other by using the command line]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/configuring-virtual-machine-network-connections_configuring-and-managing-virtualization#isolating-virtual-machines-from-each-other-using-the-command-line-interface_recommended-virtual-machine-networking-configurations-using-the-command-line-interface[Isolating virtual machines from each other by using the command line]
endif::[]
* {blank}
ifdef::virt-title[]
xref:direct-attachment-of-the-virtual-network-device_types-of-virtual-machine-network-connections[Using `macvtap` in private mode]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/configuring-virtual-machine-network-connections_configuring-and-managing-virtualization#direct-attachment-of-the-virtual-network-device_types-of-virtual-machine-network-connections[Using `macvtap` in private mode]
endif::[]
* {blank}
ifeval::[{ProductNumber} == 8]
ifdef::virt-title[]
xref:securing-virtual-machines-in-rhel_configuring-and-managing-virtualization[Securing virtual machines]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/securing-virtual-machines-in-rhel_configuring-and-managing-virtualization[Securing virtual machines]
endif::[]
endif::[]
ifeval::[{ProductNumber} == 9]
xref:securing-virtual-machines-in-rhel_configuring-and-managing-virtualization[Securing virtual machines]
endif::[]
