:_mod-docs-content-type: PROCEDURE
// This module can be included from assemblies using the following include statement:
// include::modules/virtualization/proc_creating-virtual-machine-snapshots-entering-metadata-on-the-command-line.adoc[leveloffset=+1]

[id="creating-virtual-machine-snapshots-entering-metadata-on-the-command-line_{context}"]
// CHANGE ID AND FILE NAME WHEN REVIEWS ARE DONE

= Creating virtual machine snapshots by using the command line

To save the state of a virtual machine (VM) in a snapshot, you can use the [command]`virsh snapshot-create-as` command.

// The command can use the virtual machine _name_, _id_, or _uid_ to specify the virtual machine.

.Prerequisites

* Your host is using RHEL 9.4 or later.
* The VM uses file-based storage. To check whether this is the case, use the following command and ensure that for the `disk` device, it displays `disk type` as `file`:
+
[subs="+quotes"]
----
# *virsh dumpxml _<vm-name>_ | grep "disk type"*
    <disk type='*file*' device='disk'>
    <disk type='file' device='cdrom'>
----

* If you want to create a VM snapshot that includes the memory of a running VM, you must have sufficient disk space to store the memory of the VM.

** The minimum recommended space for saving the memory of a VM is equal to the VM's assigned RAM. For example, saving the memory of a VM with 32 GB RAM requires up to 32 GB of disk space.
** If the VM is under heavy I/O load, significant additional disk space might be required.
** If the VM has assigned VFIO passthrough devices, additional disk space might be required.
** If a snapshot is created without pausing the VM, additional disk space might be required.
+
[WARNING]
====
Red Hat recommends not saving the memory of a running VMs that is under very high workload or that uses VFIO passthrough devices. Saving the memory of such VMs might fill up the host disk and degrade the system. Instead, consider creating snapshots without memory for such VMs.

In addition, note that not all VFIO devices are capable of creating snapshot with memory. Currently, creating a snapshot with memory works correctly only in the following situations:

* The attached VFIO device is a Mellanox VF with the migration capability enabled.
* The attached VFIO device is an NVIDIA vGPU with the migration capability enabled.
====


.Procedure

* To create a VM snapshot with the required parameters, use the `virsh snapshot-create-as` command. 
+
[subs="+quotes"]
----
# *virsh snapshot-create-as _<vm-name>_ _<snapshot-name>_ _<optional-description>_ _<additional-parameters>_*
----

** To create a snapshot of a shut-down VM, use the `--disk-only` parameter. For example, the following command creates `Snapshot1` from the current disk-state of the shut-down `Testguest1` VM:
+
[subs="+quotes,attributes"]
----
# *virsh snapshot-create-as Testguest1 Snapshot1 --disk-only*
Domain snapshot Snapshot1 created.
----

** To create a snapshot that saves the disk-state of a running VM but not its memory, use the `--disk-only --quiesce` parameters. For example, the following command creates `Snapshot2` from the current disk state of the running `Testguest2` VM, with the description `clean system install`:
+
[subs="+quotes,attributes"]
----
# *virsh snapshot-create-as Testguest2 Snapshot2 "clean system install" --disk-only --quiesce*
Domain snapshot Snapshot2 created.
----

** To create a snapshot that pauses a running VM and saves its disk-state and memory, use the `--memspec` parameter. For example, the following command pauses the `Testguest3` VM and creates `Snapshot3` from the current disk and memory state of the VM. The VM memory is saved in the `/var/lib/libvirt/images/saved_memory.img` file. When the snapshot is complete, the VM automatically resumes operation.
+
[subs="+quotes,attributes"]
----
# *virsh snapshot-create-as Testguest3 Snapshot3 --memspec /var/lib/libvirt/images/saved_memory.img*
Domain snapshot Snapshot3 created.
----
+
Pausing the VM during the snapshot process creates downtime, but might work more reliably than creating a live snapshot of a running VM (by using the `--live` option), especially for VMs under a heavy load.


** To create a snapshot that saves the disk-state of a running VM as well as its live memory, use the `--live --memspec` parameters. For example, the following command creates `Snapshot4` from the current disk and memory state of the running `Testguest4` VM, and saves the memory state in the `/var/lib/libvirt/images/saved_memory2.img` file.
+
[subs="+quotes,attributes"]
----
# *virsh snapshot-create-as Testguest4 Snapshot4 --live --memspec /var/lib/libvirt/images/saved_memory2.img*
Domain snapshot Snapshot4 created.
----

[WARNING]
====
Saving the memory of a VM in a snapshot saves the state of the running processes in the guest operating system of the VM. However, when you revert to such a snapshot, the processes might fail due to a variety of factors, such as loss of network connectivity or unsynchronized system time.
====


.Verification

. List the snapshots associated with the specified VM:
+
[subs="+quotes,attributes"]
----
# *virsh snapshot-list _<Testguest1>_*

 Name                    Creation Time               State
--------------------------------------------------------------
Snapshot1               2024-01-30 18:34:58 +0100   shutoff

----

. Verify that the snapshot has been created as _external_:
+
[subs="+quotes"]
----
# *virsh snapshot-dumpxml _<Testguest1>_ _<Snapshot1>_ | grep external*

  <disk name='vda' snapshot='*external*' type='*file*'>
----
+
If the output of this command includes `snapshot='external'`, the snapshot is external and therefore fully supported by Red Hat.


[role="_additional-resources"]
.Next steps
* xref:reverting-to-a-virtual-machine-snapshot-by-using-the-command-line-interface_creating-virtual-machine-snapshots[Reverting to a VM snapshot by using the CLI]
* xref:reverting-to-a-virtual-machine-snapshot-by-using-the-web-console_creating-virtual-machine-snapshots[Reverting to a VM snapshot by using the web console]



[role="_additional-resources"]
.Additional resources
* link:https://libvirt.org/formatsnapshot.html[Upstream libvirt information on snapshot metadata]
* `virsh` man page on your system
