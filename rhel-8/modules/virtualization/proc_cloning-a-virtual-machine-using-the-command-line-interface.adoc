:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// cloning-virtual-machines

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_cloning-a-virtual-machine-using-the-command-line-interface.adoc[leveloffset=+1]

[id="cloning-a-virtual-machine-using-the-command-line-interface_{context}"]
= Cloning a virtual machine by using the command line

For testing, to create a new virtual machine (VM) with a specific set of properties, you can clone an existing VM by using CLI.

.Prerequisites

* The source VM is shut down.
* Ensure that there is sufficient disk space to store the cloned disk images.
* Optional: When creating multiple VM clones, remove unique data and settings from the source VM to ensure the cloned VMs work properly. For instructions, see xref:assembly_creating-virtual-machine-templates_cloning-virtual-machines[Creating virtual machine templates].

.Procedure

* Use the `virt-clone` utility with options that are appropriate for your environment and use case.
+
*Sample use cases*
+
** The following command clones a local VM named `example-VM-1` and creates the `example-VM-1-clone` VM. It also creates and allocates the `example-VM-1-clone.qcow2` disk image in the same location as the disk image of the original VM, and with the same data:
+
[subs="+quotes"]
----
# *virt-clone --original _example-VM-1_ --auto-clone*
Allocating 'example-VM-1-clone.qcow2'                            | 50.0 GB  00:05:37

Clone 'example-VM-1-clone' created successfully.
----

** The following command clones a VM named `example-VM-2`, and creates a local VM named `example-VM-3`, which uses only two out of multiple disks of `example-VM-2`:
+
[subs="+quotes"]
----
# *virt-clone --original _example-VM-2_ --name _example-VM-3_ --file /var/lib/libvirt/images/_disk-1-example-VM-2_.qcow2 --file /var/lib/libvirt/images/_disk-2-example-VM-2_.qcow2*
Allocating 'disk-1-example-VM-2-clone.qcow2'                                      | 78.0 GB  00:05:37
Allocating 'disk-2-example-VM-2-clone.qcow2'                                      | 80.0 GB  00:05:37

Clone 'example-VM-3' created successfully.
----

** To clone your VM to a different host, migrate the VM without undefining it on the local host. For example, the following commands clone the previously created `example-VM-3` VM to the `192.0.2.1` remote system, including its local disks. Note that you require root privileges to run these commands for `192.0.2.1`:
+
[subs="+quotes"]
----
# *virsh migrate --offline --persistent _example-VM-3_ qemu+ssh://root@192.0.2.1/system*
root@192.0.2.1's password:

# *scp /var/lib/libvirt/images/__<disk-1-example-VM-2-clone>__.qcow2 root@192.0.2.1/__<user@remote_host.com>__://var/lib/libvirt/images/*

# *scp /var/lib/libvirt/images/__<disk-2-example-VM-2-clone>__.qcow2 root@192.0.2.1/__<user@remote_host.com>__://var/lib/libvirt/images/*
----

.Verification

. To verify the VM has been successfully cloned and is working correctly:
.. Confirm the clone has been added to the list of VMs on your host:
+
[subs="+quotes"]
----
# *virsh list --all*
Id   Name                  State
---------------------------------------
-    example-VM-1          shut off
-    example-VM-1-clone    shut off
----

.. Start the clone and observe if it boots up:
+
[subs="+quotes"]
----
# *virsh start _example-VM-1-clone_*
Domain 'example-VM-1-clone' started
----

[role="_additional-resources"]
.Additional resources
* `virt-clone (1)` man page on your system
* {blank}
ifdef::virt-title[]
xref:migrating-virtual-machines_configuring-and-managing-virtualization[Migrating virtual machines]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/migrating-virtual-machines_configuring-and-managing-virtualization[Migrating virtual machines]
endif::[]
