// Module included in the following assemblies:
//
// managing-virtual-machines-in-the-web-console

:_mod-docs-content-type: REFERENCE
[id="virtual-machine-management-features-available-in-the-web-console_{context}"]
= Virtual machine management features available in the web console

[role="_abstract"]
By using the {ProductShortName}{nbsp}{ProductNumber} web console, you can perform the following actions to manage the virtual machines (VMs) on your system.

ifdef::virt-title[]
[options="header"]
.VM management tasks that you can perform in the {ProductShortName}{nbsp}{ProductNumber} web console
|===
|Task |For details, see

|Create a VM and install it with a guest operating system | xref:creating-vms-and-installing-an-os-using-the-rhel-web-console_assembly_creating-virtual-machines[Creating virtual machines and installing guest operating systems by using the web console]

|Delete a VM | xref:deleting-vms-using-the-rhel-web-console_assembly_deleting-virtual-machines[Deleting virtual machines by using the web console]

|Start, shut down, and restart the VM | xref:powering-up-vms-using-the-rhel-web-console_assembly_starting-virtual-machines[Starting virtual machines by using the web console] and xref:powering-down-and-restarting-vms-using-the-rhel-web-console_assembly_shutting-down-virtual-machines[Shutting down and restarting virtual machines by using the web console]

|Connect to and interact with a VM using a variety of consoles | xref:viewing-vm-consoles-using-the-rhel-web-console_assembly_connecting-to-virtual-machines[Interacting with virtual machines by using the web console]

|View a variety of information about the VM | xref:viewing-vm-information-using-the-rhel-web-console_viewing-information-about-virtual-machines[Viewing virtual machine information by using the web console]

|Adjust the host memory allocated to a VM | xref:adding-and-removing-virtual-machine-ram-using-the-web-console_configuring-virtual-machine-ram[Adding and removing virtual machine memory by using the web console]

|Manage network connections for the VM | xref:managing-virtual-machine-network-interfaces-using-the-web-console_configuring-virtual-machine-network-connections[Using the web console for managing virtual machine network interfaces]

|Manage the VM storage available on the host and attach virtual disks to the VM | xref:managing-storage-for-virtual-machines_configuring-and-managing-virtualization[Managing storage for virtual machines by using the web console]

|Configure the virtual CPU settings of the VM | xref:managing-virtual-cpus-using-the-web-console_optimizing-virtual-machine-cpu-performance[Managing virtal CPUs by using the web console]

|Live migrate a VM | xref:proc_live-migrating-a-virtual-machine-using-the-web-console_migrating-virtual-machines[Live migrating a virtual machine by using the web console]

|Manage host devices | xref:assembly_managing-virtual-devices-using-the-web-console_managing-virtual-devices[Managing host devices by using the web console]

|Manage virtual optical drives | xref:managing-virtual-optical-drives_managing-virtual-devices[Managing virtual optical drives]

|Attach watchdog device | xref:Attaching-a-watchdog-device-to-a-virtual-machine-using-the-web-console_managing-virtual-devices[Attaching a watchdog device to a virtual machine by using the web console]
|===
endif::[]

ifndef::virt-title[]
.VM tasks that can be performed in the {ProductShortName}{nbsp}{ProductNumber} web console
|===
|Task|For details, see:

|Create a VM and install it with a guest operating system | 
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#creating-vms-and-installing-an-os-using-the-rhel-web-console_assembly_creating-virtual-machines[Creating virtual machines and installing guest operating systems by using the web console]
endif::[]

ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_virtualization/assembly_creating-virtual-machines_configuring-and-managing-virtualization#creating-vms-and-installing-an-os-using-the-rhel-web-console_assembly_creating-virtual-machines[Creating virtual machines and installing guest operating systems by using the web console]
endif::[]

|Delete a VM. | 
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#deleting-vms-using-the-rhel-web-console_assembly_deleting-virtual-machines[Deleting virtual machines by using the web console].
endif::[]

ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_virtualization/assembly_deleting-virtual-machines_configuring-and-managing-virtualization#deleting-vms-using-the-rhel-web-console_assembly_deleting-virtual-machines[Deleting virtual machines by using the web console].
endif::[]

ifeval::[{ProductNumber} == 8]
|Start, shut down, and restart the VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#powering-up-vms-using-the-rhel-web-console_assembly_starting-virtual-machines[Starting virtual machines by using the web console] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#powering-down-and-restarting-vms-using-the-rhel-web-console_assembly_shutting-down-virtual-machines[Shutting down and restarting virtual machines by using the web console]
endif::[]

ifeval::[{ProductNumber} == 9]
|Start, shut down, and restart the VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_virtualization/assembly_starting-virtual-machines_configuring-and-managing-virtualization#powering-up-vms-using-the-rhel-web-console_assembly_starting-virtual-machines[Starting virtual machines by using the web console] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_virtualization/assembly_shutting-down-virtual-machines_configuring-and-managing-virtualization#powering-down-and-restarting-vms-using-the-rhel-web-console_assembly_shutting-down-virtual-machines[Shutting down and restarting virtual machines by using the web console]
endif::[]

|Connect to and interact with a VM using a variety of consoles | 
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#viewing-vm-consoles-using-the-rhel-web-console_assembly_connecting-to-virtual-machines[Interacting with virtual machines by using the web console]
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_virtualization/assembly_connecting-to-virtual-machines_configuring-and-managing-virtualization#viewing-vm-consoles-using-the-rhel-web-console_assembly_connecting-to-virtual-machines[Interacting with virtual machines by using the web console]
endif::[]

|View a variety of information about the VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/viewing-information-about-virtual-machines_configuring-and-managing-virtualization#viewing-vm-information-using-the-rhel-web-console_viewing-information-about-virtual-machines[Viewing virtual machine information by using the web console]

|Adjust the host memory allocated to a VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/optimizing-virtual-machine-performance-in-rhel_configuring-and-managing-virtualization#adding-and-removing-virtual-machine-ram-using-the-web-console_configuring-virtual-machine-ram[Adding and removing virtual machine memory by using the web console]

|Manage network connections for the VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/configuring-virtual-machine-network-connections_configuring-and-managing-virtualization#managing-virtual-machine-network-interfaces-using-the-web-console_configuring-virtual-machine-network-connections[Using the web console for managing virtual machine network interfaces]

|Manage the VM storage available on the host and attach virtual disks to the VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-storage-for-virtual-machines_configuring-and-managing-virtualization#managing-storage-for-virtual-machines_configuring-and-managing-virtualization[Managing storage for virtual machines]

|Configure the virtual CPU settings of the VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/optimizing-virtual-machine-performance-in-rhel_configuring-and-managing-virtualization#managing-virtual-cpus-using-the-web-console_optimizing-virtual-machine-cpu-performance[Managing virtual CPUs by using the web console]

|Live migrate a VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/migrating-virtual-machines_configuring-and-managing-virtualization#proc_live-migrating-a-virtual-machine-using-the-web-console_migrating-virtual-machines[Live migrating a virtual machine by using the web console]

|Rename a VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/managing-virtual-machines-in-the-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#proc_renaming-virtual-machines-using-the-web-console_managing-virtual-machines-in-the-web-console[Renaming virtual machines by using the web console]

|Share files between the host and the VM | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/sharing-files-between-the-host-and-its-virtual-machines_configuring-and-managing-virtualization[Sharing files between the host and its virtual machines]

|Manage host devices | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-devices_configuring-and-managing-virtualization#assembly_managing-virtual-devices-using-the-web-console_managing-virtual-devices[Managing virtual devices by using the web console]

|Manage virtual optical drives | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-devices_configuring-and-managing-virtualization#managing-virtual-optical-drives_managing-virtual-devices[Managing virtual optical drives]

|Attach watchdog device | link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-devices_configuring-and-managing-virtualization#Attaching-a-watchdog-device-to-a-virtual-machine-using-the-web-console_managing-virtual-devices[Attaching a watchdog device to a virtual machine by using the web console]
|===
endif::[]
