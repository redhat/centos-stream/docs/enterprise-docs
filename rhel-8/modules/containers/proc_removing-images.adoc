:_mod-docs-content-type: PROCEDURE

[id="proc_removing-images_{context}"]
= Removing images

[role="_abstract"]
Use the `podman rmi` command to remove locally stored container images. You can remove an image by its ID or name.


.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. List all images on your local system:
+
[literal,subs="+quotes,attributes"]
----
$ *podman images*
REPOSITORY                           TAG     IMAGE ID      CREATED      SIZE
registry.redhat.io/rhel8/rsyslog     latest  4b32d14201de  7 weeks ago  228 MB
registry.redhat.io/ubi8/ubi          latest  3269c37eae33  7 weeks ago  208 MB
localhost/myubi                      X.Y     3269c37eae33  7 weeks ago  208 MB
----

. List all containers:
+
[literal,subs="+quotes,attributes"]
----
$ *podman ps -a*
CONTAINER ID  IMAGE                                    COMMAND          CREATED        STATUS            PORTS   NAMES
7ccd6001166e  registry.redhat.io/rhel8/rsyslog:latest  /bin/rsyslog.sh  6 seconds ago  Up 5 seconds ago          mysyslog
----
+
To remove the `registry.redhat.io/rhel8/rsyslog` image, you have to stop all containers running from this image using the `podman stop` command. You can stop a container by its ID or name.

. Stop the `mysyslog` container:
+
[literal,subs="+quotes,attributes"]
----
$ *podman stop mysyslog*
7ccd6001166e9720c47fbeb077e0afd0bb635e74a1b0ede3fd34d09eaf5a52e9
----

. Remove the `registry.redhat.io/rhel8/rsyslog` image:
+
[literal,subs="+quotes,attributes"]
----
$ *podman rmi registry.redhat.io/rhel8/rsyslog*
----
+
* To remove multiple images:
+
[literal,subs="+quotes,attributes"]
----
$ *podman rmi registry.redhat.io/rhel8/rsyslog registry.redhat.io/ubi8/ubi*
----

* To remove all images from your system:
+
[literal,subs="+quotes,attributes"]
----
$ *podman rmi -a*
----

* To remove images that have multiple names (tags) associated with them, add the `-f` option to remove them:
+
[literal,subs="+quotes,attributes"]
----
$ *podman rmi -f 1de7d7b3f531*
1de7d7b3f531...
----


//.Verification



[role="_additional-resources"]
.Additional resources
* `podman-rmi` man page on your system
