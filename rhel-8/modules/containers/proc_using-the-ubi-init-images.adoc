:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

[id="using-the-ubi-init-images_{context}"]
= Using the UBI init images

[role="_abstract"]
You can build a container using a `Containerfile` that installs and configures a Web server (`httpd`) to start automatically by the `systemd` service (`/sbin/init`) when the container is run on a host system. The `podman build` command builds an image using instructions in one or more `Containerfiles` and a specified build context directory.
The context directory can be specified as the URL of an archive, Git repository or `Containerfile`. If no context directory is specified, then the current working directory is considered as the build context, and must contain the `Containerfile`.
You can also specify a `Containerfile` with the `--file` option.

//The available commands that are usable inside a `Containerfile` and a `Dockerfile` are equivalent.

.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Create a `Containerfile` with the following contents to a new directory:
+
[literal,subs="+quotes,attributes"]
----
FROM registry.access.redhat.com/ubi{ProductNumberLink}/ubi-init
RUN {PackageManagerCommand} -y install httpd; {PackageManagerCommand} clean all; systemctl enable httpd;
RUN echo "Successful Web Server Test" > /var/www/html/index.html
RUN mkdir /etc/systemd/system/httpd.service.d/; echo -e '[Service]\nRestart=always' > /etc/systemd/system/httpd.service.d/httpd.conf
EXPOSE 80
CMD [ "/sbin/init" ]
----
+
The `Containerfile` installs the `httpd` package, enables the `httpd` service to start at boot time, creates a test file (`index.html`), exposes the Web server to the host (port 80), and starts the `systemd` init service (`/sbin/init`) when the container starts.

. Build the container:
+
[literal,subs="+quotes,attributes"]
----
# *podman build --format=docker -t mysysd .*
----

. Optional: If you want to run containers with `systemd` and SELinux is enabled on your system, you must set the `container_manage_cgroup` boolean variable:
+
[literal,subs="+quotes,attributes"]
----
# *setsebool -P container_manage_cgroup 1*
----

. Run the container named `mysysd_run`:
+
[literal,subs="+quotes,attributes"]
----
# *podman run -d --name=mysysd_run -p 80:80 mysysd*
----
+
The `mysysd` image runs as the `mysysd_run` container as a daemon process, with port 80 from the container exposed to port 80 on the host system.
+
[NOTE]
====
In rootless mode, you have to choose host port number >= 1024. For example:

[literal,subs="+quotes,attributes"]
----
$ *podman run -d --name=mysysd -p 8081:80 mysysd*
----

To use port numbers < 1024, you have to modify the `net.ipv4.ip_unprivileged_port_start` variable:
[literal,subs="+quotes,attributes"]
----
# *sysctl net.ipv4.ip_unprivileged_port_start=80*
----
====

. Check that the container is running:
+
[literal,subs="+quotes,attributes"]
----
# *podman ps*
a282b0c2ad3d  localhost/mysysd:latest  /sbin/init  15 seconds ago  Up 14 seconds ago  0.0.0.0:80->80/tcp  mysysd_run
----

. Test the web server:
+
[literal,subs="+quotes,attributes"]
----
# *curl localhost/index.html*
Successful Web Server Test
----

[role="_additional-resources"]
.Additional resources
* link:https://github.com/containers/podman/blob/main/rootless.md#shortcomings-of-rootless-podman[Shortcomings of Rootless Podman]
