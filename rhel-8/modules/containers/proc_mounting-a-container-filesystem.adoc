:_mod-docs-content-type: PROCEDURE
[id="proc_mounting-a-container-filesystem_{context}"]
= Mounting a container filesystem

[role="_abstract"]
Use the `podman mount` command to mount a working container root filesystem in a location accessible from the host.


.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Run the container named `mysyslog`:
+
[literal,subs="+quotes,attributes"]
----
# *podman run -d --name=mysyslog registry.redhat.io/rhel{ProductNumberLink}/rsyslog*
----

. Optional: List all containers:
+
[literal,subs="+quotes,attributes"]
----
# *podman ps -a*
CONTAINER ID  IMAGE                                    COMMAND          CREATED         STATUS                     PORTS   NAMES
c56ef6a256f8  registry.redhat.io/rhel{ProductNumberLink}/rsyslog:latest  /bin/rsyslog.sh  20 minutes ago  Up 20 minutes ago                  mysyslog
----

. Mount the `mysyslog` container:
+
[literal,subs="+quotes,attributes"]
----
# *podman mount mysyslog*
/var/lib/containers/storage/overlay/990b5c6ddcdeed4bde7b245885ce4544c553d108310e2b797d7be46750894719/merged
----

. Display the content of the mount point using `ls` command:
+
[literal,subs="+quotes,attributes"]
----
# *ls /var/lib/containers/storage/overlay/990b5c6ddcdeed4bde7b245885ce4544c553d108310e2b797d7be46750894719/merged*
bin  boot  dev  etc  home  lib  lib64  lost+found  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
----

. Display the OS version:
+
[literal,subs="+quotes,attributes"]
----
# *cat /var/lib/containers/storage/overlay/990b5c6ddcdeed4bde7b245885ce4544c553d108310e2b797d7be46750894719/merged/etc/os-release*
NAME="Red Hat Enterprise Linux"
VERSION="{ProductNumberLink} (Ootpa)"
ID="rhel"
ID_LIKE="fedora"
...
----

//.Verification



[role="_additional-resources"]
.Additional resources
* `podman-mount` man page on your system
