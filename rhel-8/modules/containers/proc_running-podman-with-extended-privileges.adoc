:_mod-docs-content-type: PROCEDURE

[id="proc_running-podman-with-extended-privileges_{context}"]
= Running Podman with extended privileges

[role="_abstract"]
If you cannot run your workloads in a rootless environment, you need to run these workloads as a root user.
Running a container with extended privileges should be done judiciously, because it disables all security features.

.Prerequisites

include::snip_container_tools.adoc[]

.Procedure
* Run the Podman container in the Podman container:
+
[literal,subs="+quotes,attributes"]
----
$ *podman run --privileged --name=privileged_podman \*
  *registry.access.redhat.com//podman podman run ubi{ProductNumberLink} echo hello*
Resolved "ubi{ProductNumberLink}" as an alias (/etc/containers/registries.conf.d/001-rhel-shortnames.conf)
Trying to pull registry.access.redhat.com/ubi{ProductNumberLink}:latest...
...
Storing signatures
hello
----
+
* Run the outer container named `privileged_podman` based on the  `registry.access.redhat.com/ubi{ProductNumberLink}/podman` image.
* The `--privileged` option disables the security features that isolate the container from the host.
* Run `podman run ubi{ProductNumberLink} echo hello` command to create the inner container based on the `ubi{ProductNumberLink}` image.
* Notice that the `ubi{ProductNumberLink}` short image name was resolved as an alias. As a result, the `registry.access.redhat.com/ubi{ProductNumberLink}:latest` image is pulled.

.Verification
* List all containers:
+
[literal,subs="+quotes,attributes"]
----
$ *podman ps -a*
CONTAINER ID  IMAGE                            COMMAND               CREATED            STATUS                          PORTS   NAMES
52537876caf4  registry.access.redhat.com/ubi{ProductNumberLink}/podman               podman run ubi{ProductNumberLink} e...  30 seconds ago     Exited (0) 13 seconds ago               privileged_podman
----

[role="_additional-resources"]
.Additional resources
* link:https://www.redhat.com/sysadmin/podman-inside-container[How to use Podman inside of a container]
* `podman-run` man page on your system
