:_mod-docs-content-type: PROCEDURE

[id="proc_listing-images_{context}"]
= Listing images

[role="_abstract"]
Use the `podman images` command to list images in your local storage.

.Prerequisites

include::snip_container_tools.adoc[]
* A pulled image is available on the local system.

.Procedure

* List all images in the local storage:
+
[literal,subs="+quotes,attributes"]
----
$ *podman images*
REPOSITORY                           TAG     IMAGE ID      CREATED      SIZE
registry.access.redhat.com/ubi{ProductNumberLink}/ubi  latest  3269c37eae33  6 weeks ago  208 MB
----


//.Verification


[role="_additional-resources"]
.Additional resources
* `podman-images` man page on your system
