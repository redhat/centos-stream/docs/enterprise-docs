:_mod-docs-content-type: PROCEDURE

[id="proc_pulling-images-from-registries_{context}"]
= Pulling images from registries

[role="_abstract"]
Use the `podman pull` command to get the image to your local system.

.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Log in to the registry.redhat.io registry:
+
[literal,subs="+quotes,attributes"]
----
$ *podman login registry.redhat.io*
Username: <username>
Password: <password>
Login Succeeded!
----

. Pull the registry.redhat.io/ubi{ProductNumberLink}/ubi container image:
+
[literal,subs="+quotes,attributes"]
----
$ *podman pull registry.redhat.io/ubi{ProductNumberLink}/ubi*
----

.Verification

* List all images pulled to your local system:
+
[literal,subs="+quotes,attributes"]
----
$ *podman images*
REPOSITORY                           TAG     IMAGE ID      CREATED      SIZE
registry.redhat.io/ubi{ProductNumberLink}/ubi          latest  3269c37eae33  7 weeks ago  208 MB
----

[role="_additional-resources"]
.Additional resources
* `podman-pull` man page on your system
