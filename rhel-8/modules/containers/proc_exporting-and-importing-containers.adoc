:_mod-docs-content-type: PROCEDURE
[id="proc_exporting-and-importing-containers_{context}"]
= Exporting and importing containers

[role="_abstract"]
You can use the `podman export` command to export the file system of a running container to a tarball on your local machine. For example, if you have a large container that you use infrequently or one that you want to save a snapshot of in order to revert back to it later, you can use the `podman export` command to export a current snapshot of your running container into a tarball.

You can use the `podman import` command to import a tarball and save it as a filesystem image. Then you can run this filesystem image or you can use it as a layer for other images.


.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Run the `myubi` container based on the `registry.access.redhat.com/ubi{ProductNumberLink}/ubi` image:
+
[literal,subs="+quotes,attributes"]
----
$ *podman run -dt --name=myubi registry.access.redhat.com/{ProductNumberLink}/ubi*
----

. Optional: List all containers:
+
[literal,subs="+quotes,attributes"]
----
$ *podman ps -a*
CONTAINER ID  IMAGE                                    COMMAND          CREATED     STATUS         PORTS   NAMES
a6a6d4896142  registry.access.redhat.com/{ProductNumberLink}:latest   /bin/bash        7 seconds ago  Up 7 seconds ago          myubi
----

. Attach to the `myubi` container:
+
[literal,subs="+quotes,attributes"]
----
$ *podman attach myubi*
----

. Create a file named `testfile`:
+
[literal,subs="+quotes,attributes"]
----
[root@a6a6d4896142 /]# *echo "hello" > testfile*
----

. Detach from the container with `CTRL+p` and `CTRL+q`.

. Export the file system of the `myubi` as a `myubi-container.tar` on the local machine:
+
[literal,subs="+quotes,attributes"]
----
$ *podman export -o myubi.tar a6a6d4896142*
----

. Optional: List the current directory content:
+
[literal,subs="+quotes,attributes"]
----
$ *ls -l*
-rw-r--r--. 1 user user 210885120 Apr  6 10:50 myubi-container.tar
...
----

. Optional: Create a `myubi-container` directory, extract all files from the `myubi-container.tar` archive. List a content of the `myubi-directory` in a tree-like format:
+
[literal,subs="+quotes,attributes"]
----
$ *mkdir myubi-container*
$ tar -xf myubi-container.tar -C myubi-container
$ tree -L 1 myubi-container
├── bin -> usr/bin
├── boot
├── dev
├── etc
├── home
├── lib -> usr/lib
├── lib64 -> usr/lib64
├── lost+found
├── media
├── mnt
├── opt
├── proc
├── root
├── run
├── sbin -> usr/sbin
├── srv
├── sys
├── testfile
├── tmp
├── usr
└── var

20 directories, 1 file
----
+
You can see that the `myubi-container.tar` contains the container file system.

. Import the `myubi.tar` and saves it as a filesystem image:
+
[literal,subs="+quotes,attributes"]
----
$ *podman import myubi.tar myubi-imported*
Getting image source signatures
Copying blob 277cab30fe96 done
Copying config c296689a17 done
Writing manifest to image destination
Storing signatures
c296689a17da2f33bf9d16071911636d7ce4d63f329741db679c3f41537e7cbf
----

. List all images:
+
[literal,subs="+quotes,attributes"]
----
$ *podman images*
REPOSITORY                              TAG     IMAGE ID      CREATED         SIZE
docker.io/library/myubi-imported       latest  c296689a17da  51 seconds ago  211 MB
----

. Display the content of the `testfile` file:
+
[literal,subs="+quotes,attributes"]
----
$ *podman run -it  --name=myubi-imported docker.io/library/myubi-imported cat testfile*
hello
----

//.Verification

[role="_additional-resources"]
.Additional resources
* `podman-export` and `podman-import` man pages on your system
