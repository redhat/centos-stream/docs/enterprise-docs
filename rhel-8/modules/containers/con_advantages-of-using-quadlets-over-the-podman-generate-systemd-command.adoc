:_mod-docs-content-type: CONCEPT

[id="advantages-of-using-quadlets-over-the-podman-generate-systemd-command_{context}"]
= Advantages of using Quadlets over the podman generate systemd command

You can use the Quadlets tool, which describes how to run a container in a format similar to regular `systemd` unit files. 

NOTE: Quadlet is available beginning with Podman v4.6.

Quadlets have many advantages over generating unit files using the `podman generate systemd` command, such as: 

* *Easy to maintain*: The container descriptions focus on the relevant container details and hide technical details of running containers under `systemd`.
* *Automatically updated*: Quadlets do not require manually regenerating unit files after an update. If a newer version of Podman is released, your service is automatically updated when the `systemclt daemon-reload` command is executed, for example, at boot time. 
* *Simplified workflow*: Thanks to the simplified syntax, you can create Quadlet files from scratch and deploy them anywhere. 
* *Support standard systemd options*: Quadlet extends the existing systemd-unit syntax with new tables, for example, a table to configure a container.

[NOTE]
==== 
Quadlet supports a subset of Kubernetes YAML capabilities. For more information, see the link:https://docs.podman.io/en/latest/markdown/podman-kube-play.1.html#podman-kube-play-support[support matrix of supported YAML fields]. You can generate the YAML files by using one of the following tools:

* Podman:  `podman generate kube` command 
* OpenShift: `oc generate` command with the `--dry-run` option 
* Kubernetes: `kubectl create` command with the `--dry-run` option
====

Quadlet supports these unit file types:

* *Container units*: Used to manage containers by running the `podman run` command.
** File extension: `.container` 
** Section name: `[Container]`
** Required fields: `Image` describing the container image the service runs

* *Kube units*: Used to manage containers defined in Kubernetes YAML files by running the `podman kube play` command.
** File extension: `.kube` 
** Section name: `[Kube]`
** Required fields: `Yaml` defining the path to the Kubernetes YAML file 

* *Network units*: Used to create Podman networks that may be referenced in `.container` or `.kube` files.
** File extension: `.network` 
** Section name: `[Network]`
** Required fields: None

* *Volume units*: Used to create Podman volumes that may be referenced in `.container` files.
** File extension: `.volume` 
** Section name: `[Volume]`
** Required fields: None

[role="_additional-resources"]
.Additional resources
* link:https://docs.podman.io/en/latest/markdown/podman-systemd.unit.5.html[Quadlet upstream documentation]

