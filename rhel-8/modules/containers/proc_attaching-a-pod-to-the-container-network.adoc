:_mod-docs-content-type: PROCEDURE

[id="proc_attaching-a-pod-to-the-container-network_{context}"]
= Attaching a pod to the container network

Attach containers in pod to the network during the pod creation.


.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Create a network named `pod-net`:
+
[literal,subs="+quotes,attributes"]
----
# *podman network create pod-net*

/etc/cni/net.d/pod-net.conflist
----

. Create a pod `web-pod`:
+
[literal,subs="+quotes,attributes"]
----
# *podman pod create --net pod-net --name web-pod*
----

. Run a container named `web-container` inside the `web-pod`:
+
[literal,subs="+quotes,attributes"]
----
# *podman run -d --pod webt-pod --name=web-container docker.io/library/httpd*
----

. Optional: Display the pods the containers are associated with:
+
[literal,subs="+quotes,attributes"]
----
# *podman ps -p*

CONTAINER ID  IMAGE                           COMMAND           CREATED        STATUS            PORTS       NAMES               POD ID        PODNAME
b7d6871d018c   registry.access.redhat.com/ubi{ProductNumberLink}/pause:latest                             9 minutes ago  Up 6 minutes ago              a8e7360326ba-infra  a8e7360326ba  web-pod
645835585e24  docker.io/library/httpd:latest  httpd-foreground  6 minutes ago  Up 6 minutes ago              web-container    a8e7360326ba  web-pod
----

.Verification
* Show all networks connected to the container:
+
[literal,subs="+quotes,attributes"]
----
# *podman ps  --format="{{.Networks}}"*

pod-net
----

//[role="_additional-resources"]
//.Additional resources
