:_mod-docs-content-type: PROCEDURE
[id="proc_running-buildah-in-a-container_{context}"]
= Running Buildah in a container

[role="_abstract"]
The procedure demonstrates how to run Buildah in a container and create a working container based on an image.

.Prerequisites

include::snip_container_tools.adoc[]


.Procedure

. Log in to the registry.redhat.io registry:
+
[literal,subs="+quotes,attributes"]
----
$ *podman login registry.redhat.io*
Username: myuser@mycompany.com
Password: <password>
Login Succeeded!
----

. Pull and run the `registry.redhat.io/rhel{ProductNumberLink}/buildah` image:
+
[literal,subs="+quotes,attributes"]
----
# *podman run --rm --device /dev/fuse -it \*
  *registry.redhat.io/rhel{ProductNumberLink}/buildah  /bin/bash*
----
+
* The `--rm` option removes the `registry.redhat.io/rhel{ProductNumberLink}/buildah` image after the container exits.
* The `--device` option adds a host device to the container.
* The `sys_chroot` - capability to change to a different root directory. It is not included in the default capabilities of a container.

. Create a new container using a `registry.access.redhat.com/ubi{ProductNumberLink}` image:
+
[literal,subs="+quotes,attributes"]
----
# *buildah from registry.access.redhat.com/ubi{ProductNumberLink}*
...
ubi{ProductNumberLink}-working-container
----

. Run the `ls /` command inside the `ubi{ProductNumberLink}-working-container` container:
+
[literal,subs="+quotes,attributes"]
----
# *buildah run  --isolation=chroot ubi{ProductNumberLink}-working-container ls /*
bin  boot  dev  etc  home  lib  lib64  lost+found  media  mnt  opt  proc  root  run  sbin  srv
----

. Optional: List all images in a local storage:
+
[literal,subs="+quotes,attributes"]
----
# *buildah images*
REPOSITORY                        TAG      IMAGE ID       CREATED       SIZE
registry.access.redhat.com/ubi{ProductNumberLink}   latest   ecbc6f53bba0   5 weeks ago   211 MB
----

. Optional: List the working containers and their base images:
+
[literal,subs="+quotes,attributes"]
----
# *buildah containers*
CONTAINER ID  BUILDER  IMAGE ID     IMAGE NAME                       CONTAINER NAME
0aaba7192762     *     ecbc6f53bba0 registry.access.redhat.com/ub... ubi{ProductNumberLink}-working-container
----

. Optional: Push the `registry.access.redhat.com/ubi{ProductNumberLink}` image to the a local registry located on `registry.example.com`:
+
[literal,subs="+quotes,attributes"]
----
# *buildah push ecbc6f53bba0 registry.example.com:5000/ubi{ProductNumberLink}/ubi*
----


//.Verification



[role="_additional-resources"]
.Additional resources
* link:https://developers.redhat.com/blog/2019/08/14/best-practices-for-running-buildah-in-a-container[Best practices for running Buildah in a container]
