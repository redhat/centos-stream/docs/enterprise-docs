:_mod-docs-content-type: PROCEDURE
[id="proc_adding-software-to-a-ubi-container-on-a-unsubscribed-host_{context}"]
= Adding software to a UBI container on a unsubscribed host

[role="_abstract"]
You do not have to disable any repositories when adding software packages on unsubscribed {ProductShortName} systems.

.Prerequisites

include::snip_container_tools.adoc[]


.Procedure
* Add a package to a running container based on the UBI standard or UBI init images. Do not disable any repositories. Use the `podman run` command to run the container. then use the `{PackageManagerCommand} install` command inside a container.
** For example, to add the `bzip2` package to the UBI standard based container:
+
[literal,subs="+quotes,attributes"]
----
$ *podman run -it --name myubi registry.access.redhat.com/ubi{ProductNumberLink}/ubi*
# *{PackageManagerCommand} install bzip2*
----
** For example, to add the `bzip2` package to the UBI init based container:
+
[literal,subs="+quotes,attributes"]
----
$ *podman run -it --name myubimin registry.access.redhat.com/ubi{ProductNumberLink}/ubi-minimal*
# *microdnf install bzip2*
----


.Verification
. List all enabled repositories:
** To list all enabled repositories inside the containers based on UBI standard or UBI init images:
+
[literal,subs="+quotes,attributes"]
----
#  *{PackageManagerCommand} repolist*
----
** To list all enabled repositories inside the containers based on UBI minimal containers:
+
[literal,subs="+quotes,attributes"]
----
# *microdnf repolist*
----


. Ensure that the required repositories are listed.
. List all installed packages:
+
[literal,subs="+quotes,attributes"]
----
# *rpm -qa*
----
. Ensure that the required packages are listed.


//[role="_additional-resources"]
//.Additional resources
