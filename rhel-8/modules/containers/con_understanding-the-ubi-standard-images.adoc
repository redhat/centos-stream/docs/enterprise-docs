:_mod-docs-content-type: CONCEPT
[id="con_understanding-the-ubi-standard-images_{context}"]
= Understanding the UBI standard images

[role="_abstract"]
The standard images (named `ubi`) are designed for any application that runs on {ProductShortName}. The key features of UBI standard images include:

* **init system**: All the features of the `systemd` initialization system you need to manage `systemd` services are available in the standard base images. These init systems let you install RPM packages that are pre-configured to
start up services automatically, such as a Web server (`httpd`) or FTP server (`vsftpd`).

* **{PackageManagerCommand}**: You have access to free {PackageManagerCommand} repositories for adding and updating software. You can use the standard set of `{PackageManagerCommand}` commands (`{PackageManagerCommand}`, `{PackageManagerCommand}-config-manager`, `{PackageManagerCommand}downloader`, and so on).

* **utilities**:  Utilities include `tar`, `dmidecode`, `gzip`, `getfacl` and further acl commands, `dmsetup` and further device mapper commands, between other utilities not mentioned here.


//[role="_additional-resources"]
//.Additional resources
