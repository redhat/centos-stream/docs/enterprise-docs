:_mod-docs-content-type: PROCEDURE
[id="proc_copying-container-images-to-or-from-the-host_{context}"]
= Copying container images to or from the host

[role="_abstract"]
Skopeo, Buildah, and Podman share the same local container-image storage. If you want to copy containers to or from the host container storage, you need to mount it into the Skopeo container.

NOTE: The path to the host container storage differs between root (`/var/lib/containers/storage`) and non-root users (`$HOME/.local/share/containers/storage`).

.Prerequisites

include::snip_container_tools.adoc[]


.Procedure

. Copy the `registry.access.redhat.com/ubi{ProductNumberLink}/ubi` image into your local container storage:
+
[literal,subs="+quotes,attributes"]
----
$ *podman run --privileged --rm -v $HOME/.local/share/containers/storage:/var/lib/containers/storage \*
*registry.redhat.io/rhel{ProductNumberLink}/skopeo skopeo copy \*
*docker://registry.access.redhat.com/ubi{ProductNumberLink}/ubi containers-storage:registry.access.redhat.com/ubi{ProductNumberLink}/ubi*
----
+
* The `--privileged` option disables all security mechanisms. Red Hat recommends only using this option in trusted environments.
* To avoid disabling security mechanisms, export the images to a tarball or any other path-based image transport and mount them in the Skopeo container:
** `$ podman save --format oci-archive -o oci.tar $IMAGE`
** `$ podman run --rm -v oci.tar:/oci.tar registry.redhat.io/rhel{ProductNumberLink}/skopeo copy oci-archive:/oci.tar $DESTINATION`

. Optional: List images in local storage:
+
[literal,subs="+quotes,attributes"]
----
$ *podman images*
REPOSITORY                               TAG     IMAGE ID      CREATED       SIZE
registry.access.redhat.com/ubi{ProductNumberLink}/ubi      latest  ecbc6f53bba0  8 weeks ago   211 MB
----


//.Verification



[role="_additional-resources"]
.Additional resources
* link:https://www.redhat.com/sysadmin/how-run-skopeo-container[How to run skopeo in a container]
