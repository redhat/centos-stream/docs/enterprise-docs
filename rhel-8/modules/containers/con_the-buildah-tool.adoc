:_mod-docs-content-type: CONCEPT

[id="con_the-buildah-tool_{context}"]
= The Buildah tool

[role="_abstract"]
Buildah is a command-line tool for creating Open Container Initiative (OCI) container images and a working container from the image. 
With Buildah, you can create containers and container images in different ways:

Container image from scratch:: 
You can create minimal container images from scratch with the `buildah from scratch` command. Minimal container images have the following benefits: Avoid including any unnecessary files or dependencies, enhanced security, and optimized performance. 
For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/building_running_and_managing_containers/index#proc_creating-images-from-scratch-with-buildah_assembly_building-container-images-with-buildah[Creating images from scratch with Buildah].

Containers from a container image::
You can create a working container from the container image by using the `buildah from <image>` command. Then you can modify the container by using the `buildah mount` and `buildah copy` commands.   
For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/building_running_and_managing_containers/index#working-with-containers-using-buildah[Working with containers using Buildah].

Container image from an existing container::
You can create a new container image by using the `bulidah commit` command. Optionally, you can push the newly created container image to a container registry by using the `buildah push` command. 
For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/building_running_and_managing_containers/index#working-with-containers-using-buildah[Working with containers using Buildah].

Container image from instructions in a Containerfile::
You can build a container image from instructions in a `Containerfile` by using the `buildah build` or `buildah bud` commands.  
For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/building_running_and_managing_containers/index#proc_building-an-image-from-a-containerfile-with-buildah_assembly_building-container-images-with-buildah[Building and image from a Containerfile using Buildah].


Using Buildah differs from building images with the `docker` command in the following ways:

No Daemon::
Buildah requires no container runtime daemon.

Base image or scratch::
You can build an image based on another container or start from scratch with an empty image .

Reduced image size::
Buildah images do not include build tools, such as `gcc`, `make`, and `dnf`. As a result, the images are more secure and you can more easily transport the images.

Compatibility::
You can easily migrate from Docker to Buildah because Buildah supports building container images with a Containerfile.  You can use the same commands inside a `Containerfile` as in a `Dockerfile`.

Interactive image building::
You can build images step-by-step interactively by creating and committing changes to containers.

Simplified image creation::
You can create `rootfs`, generate JSON files, and build OCI-compliant images with Buildah.

Flexibility::
You can script container builds directly in Bash.



[role="_additional-resources"]
.Additional resources
* link:https://www.redhat.com/sysadmin/building-buildah[Building with Buildah: Dockerfiles, command line, or scripts] article
* link:https://opensource.com/article/19/3/tips-tricks-rootless-buildah[How rootless Buildah works: Building containers in unprivileged environments] article
