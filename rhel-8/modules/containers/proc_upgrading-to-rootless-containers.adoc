:_mod-docs-content-type: PROCEDURE
[id="proc_upgrading-to-rootless-containers_{context}"]
= Upgrading to rootless containers

[role="_abstract"]
To upgrade to rootless containers from {RHEL7}, you must configure user and group IDs manually.

Here are some things to consider when upgrading to rootless containers from {RHEL7}:

* If you set up multiple rootless container users, use unique ranges for each user.

* Use 65536 UIDs and GIDs for maximum compatibility with existing container images, but the number can be reduced.

* Never use UIDs or GIDs under 1000 or reuse UIDs or GIDs from existing user accounts (which, by default, start at 1000).


.Prerequisites

* The user account has been created.


.Procedure

* Run the `usermod` command to assign UIDs and GIDs to a user:
+
[literal,subs="+quotes,attributes"]
----
# *usermod --add-subuids 200000-201000 --add-subgids 200000-201000 _<username>_*
----
** The `usermod --add-subuid` command manually adds a range of accessible user IDs to the user's account.
** The `usermod --add-subgids` command manually adds a range of accessible user GIDs and group IDs to the user's account.



.Verification
* Check that the UIDs and GIDs are set properly:
+
[literal,subs="+quotes,attributes"]
----
# *grep _<username>_ /etc/subuid /etc/subgid*
 /etc/subuid:__<username>__:200000:1001
 /etc/subgid:__<username>__:200000:1001
----


//[role="_additional-resources"]
//.Additional resources
