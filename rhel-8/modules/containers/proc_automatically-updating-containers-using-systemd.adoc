:_mod-docs-content-type: PROCEDURE
[id="proc_automatically-updating-containers-using-systemd_{context}"]
= Automatically updating containers using systemd

[role="_abstract"]
As mentioned in section xref:proc_automatically-updating-containers-using-podman_assembly_porting-containers-to-systemd-using-podman[Automatically updating containers using Podman],


you can update the container using the `podman auto-update` command.  It integrates into custom scripts and can be invoked when needed. Another way to auto update the containers is to use the pre-installed `podman-auto-update.timer` and `podman-auto-update.service` `systemd` service.
The `podman-auto-update.timer` can be configured to trigger auto updates at a specific date or time.
The `podman-auto-update.service` can further be started by the `systemctl` command or be used as a dependency by other `systemd` services.
As a result, auto updates based on time and events can be triggered in various ways to meet individual needs and use cases.

NOTE: Starting with Podman v4.6, you can use the Quadlets that describe how to run a container in a format similar to regular `systemd` unit files and hides the complexity of running containers under `systemd`.

.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Display the `podman-auto-update.service` unit file:
+
[literal,subs="+quotes,attributes"]
----
# *cat /usr/lib/systemd/system/podman-auto-update.service*

[Unit]
Description=Podman auto-update service
Documentation=man:podman-auto-update(1)
Wants=network.target
After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/podman auto-update

[Install]
WantedBy=multi-user.target default.target
----

. Display the `podman-auto-update.timer` unit file:
+
[literal,subs="+quotes,attributes"]
----
# *cat /usr/lib/systemd/system/podman-auto-update.timer*

[Unit]
Description=Podman auto-update timer

[Timer]
OnCalendar=daily
Persistent=true

[Install]
WantedBy=timers.target
----
+
In this example, the `podman auto-update` command is launched daily at midnight.

. Enable the `podman-auto-update.timer` service at system start:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl enable podman-auto-update.timer*
----

. Start the `systemd` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl start podman-auto-update.timer*
----

. Optional: List all timers:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl list-timers --all*
NEXT                         LEFT      LAST                         PASSED       UNIT                         ACTIVATES
Wed 2020-12-09 00:00:00 CET  9h left   n/a                          n/a          podman-auto-update.timer     podman-auto-update.service
----
+
You can see that `podman-auto-update.timer` activates the `podman-auto-update.service`.

//.Verification

[role="_additional-resources"]
.Additional resources
* link:https://www.redhat.com/sysadmin/improved-systemd-podman[Improved Systemd Integration with Podman 2.0]
* link:https://www.redhat.com/sysadmin/podman-shareable-systemd-services[Running containers with Podman and shareable systemd services]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/managing-systemd_configuring-basic-system-settings#enabling-a-system-service_managing-system-services-with-systemctl[Enabling a system service to start at boot]
