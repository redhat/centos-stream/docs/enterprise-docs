////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

[id="proc_signing-container-images-with-gpg-signatures_{context}"]
= Signing container images with GPG signatures
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////
You can sign images using a GNU Privacy Guard (GPG) key. 


.Prerequisites

include::snip_container_tools.adoc[]
* The GPG tool is installed. 
* The lookaside web server is set up and you can publish files on it.
** You can check the system-wide registries configuration in the `/etc/containers/registries.d/default.yaml` file. 
The `lookaside-staging` option references a file path for signature writing and is typically set on hosts publishing signatures.
+
[literal,subs="+quotes,attributes"]
----
# *cat /etc/containers/registries.d/default.yaml* 
docker:
    <registry>:
        lookaside: https://registry-lookaside.example.com
        lookaside-staging: file:///var/lib/containers/sigstore
...        
----

.Procedure

. Generate a GPG key:
+
[literal,subs="+quotes,attributes"]
----
# *gpg --full-gen-key*
----

. Export the public key: 
+
[literal,subs="+quotes,attributes"]
----
# *gpg --output <path>/key.gpg --armor --export _<username@domain.com>_*
----

. Build the container image using `Containerfile` in the current directory:  
+
[literal,subs="+quotes,attributes"]
----               
$ *podman build -t _<registry>/<namespace>/<image>_*  
----
+
Replace `_<registry>_`, `_<namespace>_`, and `_<image>_` with the container image identifiers. For more details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/building_running_and_managing_containers/index#con_container-registries_working-with-container-registries[Container registries]. 

. Sign the image and push it to the registry:
+
[literal,subs="+quotes,attributes"]
----      
 $  *podman push \*
    *--sign-by _<username@domain.com>_ \*
    *_<registry>/<namespace>/<image>_* 
----
+
NOTE: If you need to sign existing images while moving them across container registries, you can use the `skopeo copy` command.

. Optional: Display the new image signature:
+
[literal,subs="+quotes,attributes"]
----  
# *(cd /var/lib/containers/sigstore/; find . -type f)*
./_<image>_@sha256=_<digest>_/signature-1
----

. Copy your local signatures to the lookaside web server: 
+
[literal,subs="+quotes,attributes"]
----  
# *rsync -a /var/lib/containers/sigstore _<user@registry-lookaside.example.com>_:/registry-lookaside/webroot/sigstore* 
----

The signatures are stored in the location determined by the `lookaside-staging` option, in this case, `/var/lib/containers/sigstore` directory. 

.Verification

* For more details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/building_running_and_managing_containers/index#proc_verifying-gpg-image-signatures_assembly_signing-container-images[Verifying GPG image signatures].


[role="_additional-resources"]
.Additional resources

* `podman-image-trust` man page 
* `podman-push` man page 
* `podman-build` man page 
* link:https://www.redhat.com/sysadmin/creating-gpg-keypairs[How to generate GPG key pairs] 
