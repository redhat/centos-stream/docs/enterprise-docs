:_mod-docs-content-type: PROCEDURE

[id="proc_pushing-containers-to-the-docker-hub_{context}"]
= Pushing containers to the Docker Hub

[role="_abstract"]
Use your Docker Hub credentials to push and pull images from the Docker Hub with the `buildah` command.


.Prerequisites

include::snip_container_tools.adoc[]
* An image built using instructions from Containerfile. For details, see section
// When building in Pv1, use a traditional xref with and ID and a context. This is expected to work only in the `building-running-and-managing-containers` title.
xref:proc_building-an-image-from-a-containerfile-with-buildah_assembly_building-container-images-with-buildah[Building an image from a Containerfile with Buildah].

// When building in Pv2, use a path-based xref, which is more reliable with Pv2.



.Procedure
. Push the `docker.io/library/myecho:latest` to your Docker Hub. Replace `_username_` and `_password_` with your Docker Hub credentials:
+
[literal,subs="+quotes,attributes"]
----
# *buildah push --creds username:password \*
  *docker.io/library/myecho:latest docker://testaccountXX/myecho:latest*
----

.Verification
* Get and run the `docker.io/testaccountXX/myecho:latest` image:
** Using Podman tool:
+
[literal,subs="+quotes,attributes"]
----
# *podman run docker.io/testaccountXX/myecho:latest*
This container works!
----

** Using Buildah and Podman tools:
+
[literal,subs="+quotes,attributes"]
----
# *buildah from docker.io/testaccountXX/myecho:latest*
myecho2-working-container-2
# *podman run myecho-working-container-2*
----


[role="_additional-resources"]
.Additional resources
* `buildah-push` man page on your system
