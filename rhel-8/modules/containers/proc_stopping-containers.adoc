:_mod-docs-content-type: PROCEDURE
[id="proc_stopping-containers_{context}"]
= Stopping containers

[role="_abstract"]
Use the `podman stop` command to stop a running container. You can specify the containers by their container ID or name.


.Prerequisites

include::snip_container_tools.adoc[]
* At least one container is running.

.Procedure

* Stop the `myubi` container:
** Using the container name:
+
[literal,subs="+quotes,attributes"]
----
$ *podman stop myubi*
----

** Using the container ID:
+
[literal,subs="+quotes,attributes"]
----
$ *podman stop 1984555a2c27*
----

To stop a running container that is attached to a terminal session, you can enter the `exit` command inside the container.

The `podman stop` command sends a SIGTERM signal to terminate a running container. If the container does not stop after a defined period (10 seconds by default), Podman sends a SIGKILL signal.


You can also use the `podman kill` command to kill a container (SIGKILL) or send a different signal to a container.
Here is an example of sending a SIGHUP signal to a container (if supported by the application, a SIGHUP causes the application to re-read its configuration files):

----
# *podman kill --signal="SIGHUP" 74b1da000a11*
74b1da000a114015886c557deec8bed9dfb80c888097aa83f30ca4074ff55fb2
----

//.Verification



[role="_additional-resources"]
.Additional resources
* `podman-stop` and `podman-kill` man pages on your system
