////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken. The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in an assembly file.

Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

[id="performing-a-health-check-using-a-containerfile_{context}"]
= Performing a health check using a Containerfile

You can set a health check by using the `HEALTHCHECK` instruction in the `Containerfile`.


.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Create a `Containerfile`:
+
[literal,subs="+quotes,attributes"]
----
$ *cat Containerfile*
FROM registry.access.redhat.com/ubi8/httpd-24  
EXPOSE 8080 
HEALTHCHECK CMD curl http://localhost:8080 || exit 1      
----                                    
+
NOTE: The `HEALTHCHECK` instruction is supported only for the `docker` image format. For the `oci` image format, the instruction is ignored.

. Build the container and add an image name:
+
[literal,subs="+quotes,attributes"]
----
$ *podman build --format=docker -t hc-container .*
STEP 1/3: FROM registry.access.redhat.com/ubi8/httpd-24
STEP 2/3: EXPOSE 8080
--> 5aea97430fd
STEP 3/3: HEALTHCHECK CMD curl http://localhost:8080 || exit 1
COMMIT health-check
Successfully tagged localhost/health-check:latest
a680c6919fe6bf1a79219a1b3d6216550d5a8f83570c36d0dadfee1bb74b924e
----


. Run the container:
+
[literal,subs="+quotes,attributes"]
----
$ *podman run -dt --name=hc-container localhost/hc-container*
----

. Check the health status of the `hc-container` container:
* Using the `podman inspect` command:
+
[literal,subs="+quotes,attributes"]
----
$ *podman  inspect --format='{{json .State.Health.Status}}' hc-container*
healthy
----

* Using the `podman ps` command:
+
[literal,subs="+quotes,attributes"]
----
$ *podman ps*
CONTAINER ID  IMAGE                 COMMAND               CREATED      STATUS          PORTS       NAMES
a680c6919fe  localhost/hc-container:latest  /usr/bin/run-http...  2 minutes ago  Up 2 minutes (healthy) hc-container
----

* Using the `podman healthcheck run` command:
+
[literal,subs="+quotes,attributes"]
----
$ *podman healthcheck run hc-container*
healthy
----

[role="_additional-resources"]
.Additional resources
* `podman-healthcheck` and `podman-run` man pages on your system
* link:https://www.redhat.com/sysadmin/podman-edge-healthcheck[Podman at the edge: Keeping services alive with custom healthcheck actions]
* link:https://developers.redhat.com/blog/2019/04/18/monitoring-container-vitality-and-availability-with-podman[Monitoring container vitality and availability with Podman]
