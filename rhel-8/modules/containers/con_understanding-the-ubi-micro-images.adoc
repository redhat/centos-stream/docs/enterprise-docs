:_mod-docs-content-type: CONCEPT
[id="con_understanding-the-ubi-micro-images_{context}"]
= Understanding the UBI micro images

[role="_abstract"]
The `ubi-micro` is the smallest possible UBI image, obtained by excluding a package manager and all of its dependencies which are normally included in a container image. This minimizes the attack surface of container images based on the `ubi-micro` image and is suitable for minimal applications, even if you use UBI Standard, Minimal, or Init for other applications. 
The container image without the Linux distribution packaging is called a Distroless container image.




//[role="_additional-resources"]
//.Additional resources
