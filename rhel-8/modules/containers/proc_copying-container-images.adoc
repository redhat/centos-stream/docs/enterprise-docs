:_mod-docs-content-type: PROCEDURE

[id="proc_copying-container-images_{context}"]
= Copying container images

[role="_abstract"]
You can use the `skopeo copy` command to copy a container image from one registry to another. For example, you can populate an internal repository with images from external registries, or sync image registries in two different locations.


.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

* Copy the `skopeo` container image from `docker://quay.io` to `docker://registry.example.com`:
+
[literal,subs="+quotes,attributes"]
----
$ *skopeo copy docker://quay.io/skopeo/stable:latest docker://registry.example.com/skopeo:latest*
----



//.Verification

[role="_additional-resources"]
.Additional resources
* `skopeo-copy` man page on your system
