:_mod-docs-content-type: PROCEDURE
[id="proc_mounting-directory-on-localhost-to-the-container_{context}"]
= Mounting directory on localhost to the container

[role="_abstract"]
You can make log messages from inside a container available to the host system by mounting the host `/dev/log` device inside the container.


.Prerequisites

include::snip_container_tools.adoc[]

.Procedure

. Run the container named `log_test` and mount the host `/dev/log` device inside the container:
+
[literal,subs="+quotes,attributes"]
----
# *podman run --name="log_test" -v /dev/log:/dev/log --rm \*
  *registry.redhat.io/ubi{ProductNumberLink}/ubi logger "Testing logging to the host"*
----

. Use the `journalctl` utility to display logs:
+
[literal,subs="+quotes,attributes"]
----
# *journalctl -b | grep Testing*
Dec 09 16:55:00 localhost.localdomain root[14634]: Testing logging to the host
----
+
The `--rm` option removes the container when it exits.



//.Verification



[role="_additional-resources"]
.Additional resources
* `podman-run` man page on your system
