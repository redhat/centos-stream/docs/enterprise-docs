ifeval::[{ProductNumber} == 8]
* The `container-tools` module is installed.
endif::[]

ifeval::[{ProductNumber} >= 9]
* The `container-tools` meta-package is installed.
endif::[]

