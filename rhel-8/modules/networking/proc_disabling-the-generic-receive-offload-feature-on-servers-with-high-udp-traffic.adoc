:_mod-docs-content-type: PROCEDURE

[id="disabling-the-generic-receive-offload-feature-on-servers-with-high-udp-traffic_{context}"]
= Disabling the Generic Receive Offload feature on servers with high UDP traffic
Applications that use high-speed UDP bulk transfer should enable and use UDP Generic Receive Offload (GRO) on the UDP socket. However, you can disable GRO to increase the throughput if the following conditions apply:

* The application does not support GRO and the feature cannot be added.
* TCP throughput is not relevant.
+
[WARNING]
====
Disabling GRO significantly reduces the receive throughput of TCP traffic. Therefore, do not disable GRO on hosts where TCP performance is relevant.
====


.Prerequisites

* The host mainly processes UDP traffic.
* The application does not use GRO.
* The host does not use UDP tunnel protocols, such as VXLAN.
* The host does not run virtual machines (VMs) or containers.


.Procedure

. Optional: Display the NetworkManager connection profiles:
+
[literal,subs="+quotes"]
....
# *nmcli connection show*
NAME     UUID                                  TYPE      DEVICE
_example_  _f2f33f29-bb5c-3a07-9069-be72eaec3ecf_  _ethernet_  _enp1s0_
....

. Disable GRO support in the connection profile:
+
[literal,subs="+quotes"]
....
# *nmcli connection modify _example_ ethtool.feature-gro off*
....

. Reactivate the connection profile:
+
[literal,subs="+quotes"]
....
# *nmcli connection up _example_*
....


.Verification

. Verify that GRO is disabled:
+
[literal,subs="+quotes"]
....
# *ethtool -k _enp1s0_ | grep generic-receive-offload*
generic-receive-offload: off
....

. Monitor the throughput on the server. Re-enable GRO in the NetworkManager profile if the setting has negative side effects to other applications on the host.


ifeval::[{ProductNumber} == 8]
[role="_additional-resources"]
.Additional resources

* link:https://developers.redhat.com/articles/2021/11/05/improve-udp-performance-rhel-85[Improve UDP performance in RHEL 8.5]
endif::[]

