:_mod-docs-content-type: PROCEDURE

[id="tuning-the-network-device-backlog-queue-to-avoid-packet-drops_{context}"]
= Tuning the network device backlog queue to avoid packet drops
When a network card receives packets and before the kernel protocol stack processes them, the kernel stores these packets in backlog queues. The kernel maintains a separate queue for each CPU core.

If the backlog queue for a core is full, the kernel drops all further incoming packets that the `netif_receive_skb()` kernel function assigns to this queue. If the server contains a 10 Gbps or faster network adapter or multiple 1 Gbps adapters, tune the backlog queue size to avoid this problem.


.Prerequisites

* A 10 Gbps or faster or multiple 1 Gbps network adapters


.Procedure

. Determine whether tuning the backlog queue is needed, display the counters in the `/proc/net/softnet_stat` file:
+
[literal,subs="+quotes"]
....
# *awk '{for (i=1; i<=NF; i++) printf strtonum("0x" $i) (i==NF?"\n":" ")}' /proc/net/softnet_stat | column -t*
221951548  0      0  0  0  0  0  0  0  0  0  0  0
192058677  18862  0  0  0  0  0  0  0  0  0  0  1
455324886  0      0  0  0  0  0  0  0  0  0  0  2
...
....
+
This `awk` command converts the values in `/proc/net/softnet_stat` from hexadecimal to decimal format and displays them in table format. Each line represents a CPU core starting with core 0.
+
The relevant columns are:
+
* First column: The total number of received frames
* Second column: The number of dropped frames because of a full backlog queue
//* Third column: The number of times the `ksoftirqd` service could not issue a software interrupt (softirq) or ran out of CPU time
* Last column: The CPU core number

. If the values in the second column of the `/proc/net/softnet_stat` file increment over time, increase the size of the backlog queue:

.. Display the current backlog queue size:
+
[literal,subs="+quotes"]
....
# *sysctl net.core.netdev_max_backlog*
net.core.netdev_max_backlog = _1000_
....

.. Create the `/etc/sysctl.d/10-netdev_max_backlog.conf` file with the following content:
+
[literal,subs="+quotes"]
....
*net.core.netdev_max_backlog = _2000_*
....
+
Set the `net.core.netdev_max_backlog` parameter to a double of the current value.

.. Load the settings from the `/etc/sysctl.d/10-netdev_max_backlog.conf` file:
+
[literal,subs="+quotes"]
....
# *sysctl -p /etc/sysctl.d/10-netdev_max_backlog.conf*
....


.Verification

* Monitor the second column in the `/proc/net/softnet_stat` file:
+
[literal,subs="+quotes"]
....
# *awk '{for (i=1; i<=NF; i++) printf strtonum("0x" $i) (i==NF?"\n":" ")}' /proc/net/softnet_stat | column -t*
....
+
If the values still increase, double the `net.core.netdev_max_backlog` value again. Repeat this process until the packet drop counters no longer increase.

