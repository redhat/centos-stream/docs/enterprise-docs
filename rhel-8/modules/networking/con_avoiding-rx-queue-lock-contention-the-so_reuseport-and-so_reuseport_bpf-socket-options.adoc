:_mod-docs-content-type: CONCEPT

[id="avoiding-rx-queue-lock-contention-the-so_reuseport-and-so_reuseport_bpf-socket-options_{context}"]
= Avoiding RX queue lock contention: The SO_REUSEPORT and SO_REUSEPORT_BPF socket options
On a multi-core system, you can improve the performance of multi-threaded network server applications if the application opens the port by using the `SO_REUSEPORT` or `SO_REUSEPORT_BPF` socket option. If the application does not use one of these socket options, all threads are forced to share a single socket to receive the incoming traffic. Using a single socket causes:

* Significant contention on the receive buffer, which can cause packet drops and higher CPU usage.
* A significant increase of CPU usage
* Possibly packet drops

image::lock-contention.png[]

With the `SO_REUSEPORT` or `SO_REUSEPORT_BPF` socket option, multiple sockets on one host can bind to the same port:

image::so_reuseport.png[]


{ProductName} provides a code example of how to use the `SO_REUSEPORT` socket options in the kernel sources. To access the code example:

. Enable the `rhel-{ProductNumberLink}-for-x86_64-baseos-debug-rpms` repository:
+
[literal,subs="+quotes,attributes"]
....
# *subscription-manager repos --enable rhel-{ProductNumberLink}-for-x86_64-baseos-debug-rpms*
....

. Install the `kernel-debuginfo-common-x86_64` package:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} install kernel-debuginfo-common-x86_64*
....

. The code example is now available in the `/usr/src/debug/kernel-_<version>_/linux-<version>/tools/testing/selftests/net/reuseport_bpf_cpu.c` file.


[role="_additional-resources"]
.Additional resources
* `socket(7)` man page on your system
* `/usr/src/debug/kernel-_<version>_/linux-<version>/tools/testing/selftests/net/reuseport_bpf_cpu.c`

