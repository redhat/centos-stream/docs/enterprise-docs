:_mod-docs-content-type: REFERENCE

[id="ref_flow-control-in-ethernet-networks_{context}"]
= Flow control for Ethernet networks

On an Ethernet link, continuous data transmission between a network interface and a switch port can lead to full buffer capacity. Full buffer capacity results in network congestion. In this case, when the sender transmits data at a higher rate than the processing capacity of the receiver, packet loss can occur due to the lower data processing capacity of a network interface on the other end of the link which is a switch port.

The flow control mechanism manages data transmission across the Ethernet link where each sender and receiver has different sending and receiving capacities. To avoid packet loss, the Ethernet flow control mechanism temporarily suspends the packet transmission to manage a higher transmission rate from a switch port. Note that routers do not forward pause frames beyond a switch port.

When receive (RX) buffers become full, a receiver sends pause frames to the transmitter. The transmitter then stops data transmission for a short sub-second time frame, while continuing to buffer incoming data during this pause period. This duration provides enough time for the receiver to empty its interface buffers and prevent buffer overflow.

[NOTE]
====
Either end of the Ethernet link can send pause frames to another end. If the receive buffers of a network interface are full, the network interface will send pause frames to the switch port. Similarly, when the receive buffers of a switch port are full, the switch port sends pause frames to the network interface.
====

By default, most of the network drivers in {RHEL} have pause frame support enabled. To display the current settings of a network interface, enter:

[subs="+quotes"]
----
# *ethtool --show-pause _enp1s0_*
Pause parameters for enp1s0: 
...
RX:     on 
TX:     on
...
----

Verify with your switch vendor to confirm if your switch supports pause frames.

[role="_additional-resources"]
.Additional resources

* `ethtool(8)` man page on your system
* https://access.redhat.com/solutions/68817[What is network link flow control and how does it work in Red Hat Enterprise Linux?] (Red Hat Knowledgebase)
