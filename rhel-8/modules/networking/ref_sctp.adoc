:_mod-docs-content-type: REFERENCE
[id="sctp_{context}"]
= A new data chunk type, `I-DATA`, added to SCTP

This update adds a new data chunk type, `I-DATA`, and stream schedulers to the Stream Control Transmission Protocol (SCTP). Previously, SCTP sent user messages in the same order as they were sent by a user. Consequently, a large SCTP user message blocked all other messages in any stream until completely sent. When using `I-DATA` chunks, the Transmission Sequence Number (TSN) field is not overloaded. As a result, SCTP now can schedule the streams in different ways, and  `I-DATA` allows user messages interleaving (RFC 8260). Note that both peers must support the `I-DATA` chunk type.
