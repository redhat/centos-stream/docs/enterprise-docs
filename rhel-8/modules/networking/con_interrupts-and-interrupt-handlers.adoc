:_mod-docs-content-type: CONCEPT

[id="interrupts-and-interrupt-handlers_{context}"]
= Interrupts and interrupt handlers
When a network interface controller (NIC) receives incoming data, it copies the data into kernel buffers by using Direct Memory Access (DMA). The NIC then notifies the kernel about this data by triggering a hard interrupt. These interrupts are processed by interrupt handlers which do minimal work, as they have already interrupted another task and the handlers cannot interrupt themselves. Hard interrupts can be costly in terms of CPU usage, especially if they use kernel locks.

The hard interrupt handler then leaves the majority of packet reception to a software interrupt request (SoftIRQ) process. The kernel can schedule these processes more fairly.

.Displaying hardware interrupts
[example]
====
The kernel stores the interrupt counters in the `/proc/interrupts` file. To display the counters for a specific NIC, such as `enp1s0`, enter:

[literal,subs="+quotes"]
....
# *egrep "CPU|_enp1s0_" /proc/interrupts*
         CPU0     CPU1     CPU2    CPU3    CPU4   CPU5
 _105_:  _141606_        _0_        _0_       _0_       _0_      _0_  _IR-PCI-MSI-edge_      _enp1s0-rx-0_
 _106_:       _0_   _141091_        _0_       _0_       _0_      _0_  _IR-PCI-MSI-edge_      _enp1s0-rx-1_
 _107_:       _2_        _0_   _163785_       _0_       _0_      _0_  _IR-PCI-MSI-edge_      _enp1s0-rx-2_
 _108_:       _3_        _0_        _0_  _194370_       _0_      _0_  _IR-PCI-MSI-edge_      _enp1s0-rx-3_
 _109_:       _0_        _0_        _0_       _0_       _0_      _0_  _IR-PCI-MSI-edge_      _enp1s0-tx_
....

Each queue has an interrupt vector in the first column assigned to it. The kernel initializes these vectors when the system boots or when a user loads the NIC driver module. Each receive (`RX`) and transmit (`TX`) queue is assigned a unique vector that informs the interrupt handler which NIC or queue the interrupt is coming from. The columns represent the number of incoming interrupts for every CPU core.
====

