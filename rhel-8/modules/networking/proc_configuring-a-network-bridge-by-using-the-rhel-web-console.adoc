:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-a-network-bridge-by-using-the-rhel-web-console_{context}"]
= Configuring a network bridge by using the RHEL web console
Use the RHEL web console to configure a network bridge if you prefer to manage network settings using a web browser-based interface.


.Prerequisites

* Two or more physical or virtual network devices are installed on the server.

* To use Ethernet devices as ports of the bridge, the physical or virtual Ethernet devices must be installed on the server.

* To use team, bond, or VLAN devices as ports of the bridge, you can either create these devices while you create the bridge or you can create them in advance as described in:

ifdef::networking-title[]
** xref:proc_configuring-a-network-team-by-using-the-rhel-web-console_configuring-network-teaming[Configuring a network team using the RHEL web console]
** xref:proc_configuring-a-network-bond-by-using-the-rhel-web-console_configuring-network-bonding[Configuring a network bond by using the RHEL web console]
** xref:proc_configuring-vlan-tagging-by-using-the-rhel-web-console_configuring-vlan-tagging[Configuring VLAN tagging by using the RHEL web console]
endif::[]

ifndef::networking-title[]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/configuring-network-teaming_configuring-and-managing-networking#proc_configuring-a-network-team-by-using-the-rhel-web-console_configuring-network-teaming[Configuring a network team using the RHEL web console]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/configuring-network-teaming_configuring-and-managing-networking#proc_configuring-a-network-team-by-using-the-rhel-web-console_configuring-network-teaming[Configuring a network bond using the RHEL web console]
** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/configuring-vlan-tagging_configuring-and-managing-networking#proc_configuring-vlan-tagging-by-using-the-rhel-web-console_configuring-vlan-tagging[Configuring VLAN tagging by using the RHEL web console]
endif::[]

include::common-content/snip_web-console-prerequisites.adoc[]

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. Select the *Networking* tab in the navigation on the left side of the screen.

. Click btn:[Add bridge] in the *Interfaces* section.

. Enter the name of the bridge device you want to create.

. Select the interfaces that should be ports of the bridge.

. Optional: Enable the *Spanning tree protocol (STP)* feature to avoid bridge loops and broadcast radiation.
+
image::bridge-settings.png[]

. Click btn:[Apply].

. By default, the bridge uses a dynamic IP address. If you want to set a static IP address:

.. Click the name of the bridge in the *Interfaces* section.

.. Click *Edit* next to the protocol you want to configure.

.. Select *Manual* next to *Addresses*, and enter the IP address, prefix, and default gateway.

.. In the *DNS* section, click the btn:[+] button, and enter the IP address of the DNS server. Repeat this step to set multiple DNS servers.

.. In the *DNS search domains* section, click the btn:[+] button, and enter the search domain.

.. If the interface requires static routes, configure them in the *Routes* section.
+
image::bond-team-bridge-vlan.ipv4.png[]

.. Click btn:[Apply]


.Verification

* Select the *Networking* tab in the navigation on the left side of the screen, and check if there is incoming and outgoing traffic on the interface:
+
image::bridge-verify.png[]

