:_mod-docs-content-type: REFERENCE

// Module included in the following assemblies:
//
// assembly_introduction-to-networkmanager-debugging.adoc

// Base the file name and the ID on the module title. For example:
// * file name: my-reference-a.adoc
// * ID: [id="my-reference-a"]
// * Title: = My reference A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="debugging-levels-and-domains_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Debugging levels and domains
// In the title of a reference module, include nouns that are used in the body text. For example, "Keyboard shortcuts for ___" or "Command options for ___." This helps readers and search engines find the information quickly.

[role="_abstract"]
You can use the `levels` and `domains` parameters to manage the debugging for NetworkManager. The level defines the verbosity level, whereas the domains define the category of the messages to record the logs with given severity (`level`).

[options="header"]
[cols="25%,75%"]
|====
|Log levels|Description
|`OFF`|Does not log any messages about NetworkManager
|`ERR`|Logs only critical errors
|`WARN`|Logs warnings that can reflect the operation
|`INFO`|Logs various informational messages that are useful for tracking state and operations
|`DEBUG`|Enables verbose logging for debugging purposes
|`TRACE`|Enables more verbose logging than the `DEBUG` level
|====

Note that subsequent levels log all messages from earlier levels. For example, setting the log level to `INFO` also logs messages contained in the `ERR` and `WARN` log level.

[role="_additional-resources"]
.Additional resources
* `NetworkManager.conf(5)` man page on your system
