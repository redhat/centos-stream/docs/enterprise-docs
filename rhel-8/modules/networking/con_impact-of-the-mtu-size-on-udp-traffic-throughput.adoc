:_mod-docs-content-type: CONCEPT

[id="impact-of-the-mtu-size-on-udp-traffic-throughput_{context}"]
= Impact of the MTU size on UDP traffic throughput
If your application uses a large UDP message size, using jumbo frames can improve the throughput. According to the IEEE 802.3 standard, a default Ethernet frame without Virtual Local Area Network (VLAN) tag has a maximum size of 1518 bytes. Each of these frames includes an 18 bytes header, leaving 1500 bytes for payload. Consequently, for every 1500 bytes of data the server transmits over the network, 18 bytes (1.2%) are overhead.

Jumbo frames are non-standardized frames that have a larger Maximum Transmission Unit (MTU) than the standard Ethernet payload size of 1500 bytes. For example, if you configure jumbo frames with the maximum allowed MTU of 9000 bytes payload, the overhead of each frame reduces to 0.2%.

[IMPORTANT]
====
All network devices on the transmission path and the involved broadcast domains must support jumbo frames and use the same MTU. Packet fragmentation and reassembly due to inconsistent MTU settings on the transmission path reduces the network throughput.
====

Different connection types have certain MTU limitations:

* Ethernet: the MTU is limited to 9000 bytes.
* IP over InfiniBand (IPoIB) in datagram mode: The MTU is limited to 4 bytes less than the InfiniBand MTU.
* In-memory networking commonly supports larger MTUs. For details, see the respective documentation.


//[role="_additional-resources"]
//.Additional resources
//* <TBA - link to jumbo frames documentation (RHELPLAN-138247)>

