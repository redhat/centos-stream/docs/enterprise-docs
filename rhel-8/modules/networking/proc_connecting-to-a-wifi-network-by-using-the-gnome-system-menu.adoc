:_mod-docs-content-type: PROCEDURE

[id="proc_connecting-to-a-wifi-network-by-using-the-gnome-system-menu_{context}"]
= Connecting to a wifi network by using the GNOME system menu

You can use the GNOME system menu to connect to a wifi network. When you connect to a network for the first time, GNOME creates a NetworkManager connection profile for it. If you configure the connection profile to not automatically connect, you can also use the GNOME system menu to manually connect to a wifi network with an existing NetworkManager connection profile.

[NOTE]
====
Using the GNOME system menu to establish a connection to a wifi network for the first time has certain limitations. For example, you can not configure IP address settings. In this case first configure the connections:

* xref:proc_connecting-to-a-wifi-network-by-using-the-gnome-settings-application_assembly_managing-wifi-connections[In the `GNOME settings` application]
* xref:proc_configuring-a-wifi-connection-by-using-nm-connection-editor_assembly_managing-wifi-connections[In the `nm-connection-editor` application]
* xref:proc_connecting-to-a-wifi-network-by-using-the-gnome-settings-application_assembly_managing-wifi-connections[Using `nmcli` commands]
====


.Prerequisites

* A wifi device is installed on the host.
* The wifi device is enabled. To verify, use the `nmcli radio` command.


.Procedure

. Open the system menu on the right side of the top bar.

. Expand the `Wi-Fi Not Connected` entry.

. Click `Select Network`:
+
image::gnome-select-wifi.png[]

. Select the wifi network you want to connect to.

. Click `Connect`.

. If this is the first time you connect to this network, enter the password for the network, and click `Connect`.


.Verification

. Open the system menu on the right side of the top bar, and verify that the wifi network is connected:
+
image::gnome-wifi-connected.png[]
+
If the network appears in the list, it is connected.

. Ping a hostname or IP address:
+
[literal,subs="+quotes"]
....
# **ping -c 3 __example.com__**
....

