:_mod-docs-content-type: REFERENCE
[id="network-interface-names_{context}"]
= Network interface name changes

In Red Hat Enterprise Linux 8, the same consistent network device naming scheme is used by default as in RHEL 7. However, certain kernel drivers, such as `e1000e`, `nfp`, `qede`, `sfc`, `tg3` and `bnxt_en` changed their consistent name on a fresh installation of RHEL 8. However, the names are preserved on upgrade from RHEL 7.

//BZ#1701968
