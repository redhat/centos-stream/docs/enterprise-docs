:_mod-docs-content-type: PROCEDURE

[id="increasing-the-system-wide-udp-socket-buffers_{context}"]
= Increasing the system-wide UDP socket buffers
Socket buffers temporarily store data that the kernel has received or should send:

* The read socket buffer holds packets that the kernel has received but which the application has not read yet.
* The write socket buffer holds packets that an application has written to the buffer but which the kernel has not passed to the IP stack and network driver yet.

If a UDP packet is too large and exceeds the buffer size or packets are sent or received at a too fast rate, the kernel drops any new incoming UDP packet until the data is removed from the buffer. In this case, increasing the socket buffers can prevent packet loss.

[IMPORTANT]
====
Setting too large buffer sizes wastes memory. Each socket can be set to the size that the application requests, and the kernel doubles this value. For example, if an application requests a 256 KiB socket buffer size and opens 1 million sockets, the system requires 512 GB RAM (512 KiB x 1 million) only for the potential socket buffer space.
====



.Prerequisites

* You encountered a significant rate of dropped UDP packets.


.Procedure

. Create the `/etc/sysctl.d/10-udp-socket-buffers.conf` file and either set the maximum read or write buffer size, or both, based on your requirements:
+
[literal,subs="+quotes"]
....
*net.core.rmem_max = __16777216__*
*net.core.wmem_max = __16777216__*
....
+
Specify the values in bytes. The values in this example set the maximum size of buffers to 16 MiB. The default values of both parameters are `212992` bytes (208 KiB).

. Load the settings from the `/etc/sysctl.d/10-udp-socket-buffers.conf` file:
+
[literal,subs="+quotes"]
....
# *sysctl -p /etc/sysctl.d/__10-udp-socket-buffers.conf__*
....

. Configure your applications to use the larger socket buffer sizes.
+
The `net.core.rmem_max` and `net.core.wmem_max` parameters define the maximum buffer size that the `setsockopt()` function in an application can request. Note that, if you configure your application to not use the `setsockopt()` function, the kernel uses the values from the `rmem_default` and `wmem_default` parameters.
+
For further details, see the documentation of the programming language of your application. If you are not the developer of the application, contact the developer.

. Restart the applications to use the new UDP buffer sizes.


.Verification

* Monitor the packet drop statistics using the same method as you used when you encountered the packet drops.
+
If packet drops still occur but at a lower rate, increase the buffer sizes further.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/85913[What are the implications of changing socket buffer sizes?] (Red Hat Knowledgebase)
* `udp(7)` and `socket(7)` man pages on your system

