:_mod-docs-content-type: REFERENCE
[id="network-kernel_parameters_{context}"]
= The `ipv6`, `netmask`, `gateway`, and `hostname` kernel parameters have been removed

The `ipv6`, `netmask`, `gateway`, and `hostname` kernel parameters to configure the network in the kernel command line are no longer available since RHEL 8.3. Instead, use the consolidated `ip` parameter that accepts different formats, such as the following:

[literal,subs="+quotes"]
....
ip=__IP_address__:__peer__:__gateway_IP_address__:__net_mask__:__host_name__:__interface_name__:__configuration_method__
....

For further details about the individual fields and other formats this parameter accepts, see the description of the `ip` parameter in the `dracut.cmdline(7)` man page on your system.

//BZ#1905138
