////
Base the file name and the ID on the module title. For example:
* file name: con-my-concept-module-a.adoc
* ID: [id="con-my-concept-module-a_{context}"]
* Title: = My concept module A
////

////
Indicate the module type in one of the following
ways:
Add the prefix con- or con_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: CONCEPT



[id="con_scheduler-policies_{context}"]
= Scheduler policies


[role="_abstract"]
The real-time threads have higher priority than the standard threads. The policies have scheduling priority values that range from the minimum value of 1 to the maximum value of 99.

The following policies are critical to real-time:

* `SCHED_OTHER` or `SCHED_NORMAL` policy
+
This is the default scheduling policy for Linux threads. It has a dynamic priority that is changed by the system based on the characteristics of the thread.  `SCHED_OTHER` threads have nice values between -20, which is the highest priority and 19, which is the lowest priority. The default nice value for `SCHED_OTHER` threads is 0. 

* `SCHED_FIFO` policy
+
Threads with `SCHED_FIFO` run with higher priority over `SCHED_OTHER` tasks. Instead of using nice values, `SCHED_FIFO` uses a fixed priority between 1, which is the lowest and 99, which is the highest. A `SCHED_FIFO` thread with a priority of 1 always schedules first over a `SCHED_OTHER` thread.

* `SCHED_RR` policy
+
The `SCHED_RR` policy is similar to the `SCHED_FIFO` policy. The threads of equal priority are scheduled in a round-robin fashion. `SCHED_FIFO` and `SCHED_RR` threads run until one of the following events occurs:

** The thread goes to sleep or waits for an event.
** A higher-priority real-time thread gets ready to run.
+
Unless one of the above events occurs, the threads run indefinitely on the specified processor, while the lower-priority threads remain in the queue waiting to run. This might cause the system service threads to be resident and prevent being swapped out and fail the filesystem data flushing.

* `SCHED_DEADLINE` policy
+
The `SCHED_DEADLINE` policy specifies the timing requirements. It schedules each task according to the task’s deadline. The task with the earliest deadline first (EDF) schedule runs first.
+
The kernel requires `runtime<=deadline<=period` to be true. The relation between the required options is `runtime⇐deadline⇐period`.
