:_mod-docs-content-type: PROCEDURE

[id="proc_setting-the-process-priority-using-a-library-call_{context}"]
= Setting the process priority using a library call

The scheduler policy and other parameters can be set using the [function]`sched_setscheduler()` function. Currently, real-time policies have one parameter, [parameter]`sched_priority`. This parameter is used to adjust the priority of the process.

The `sched_setscheduler()` function requires three parameters, in the form: `sched_setscheduler(pid_t pid, int policy, const struct sched_param *sp);`.

[NOTE]
The `sched_setscheduler(2)` man page lists all possible return values of `sched_setscheduler()`, including the error codes.

If the process ID is zero, the `sched_setscheduler()` function acts on the calling process.

The following code excerpt sets the scheduler policy of the current process to the `SCHED_FIFO` scheduler policy and the priority to `50`:

----
struct sched_param sp = { .sched_priority = 50 };
int ret;

ret = sched_setscheduler(0, SCHED_FIFO, &sp);
if (ret == -1) {
  perror("sched_setscheduler");
  return 1;
}
----
