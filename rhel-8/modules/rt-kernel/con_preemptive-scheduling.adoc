////
Base the file name and the ID on the module title. For example:
* file name: con-my-concept-module-a.adoc
* ID: [id="con-my-concept-module-a_{context}"]
* Title: = My concept module A
////

////
Indicate the module type in one of the following
ways:
Add the prefix con- or con_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: CONCEPT



[id="con_preemptive-scheduling_{context}"]
= Preemptive scheduling


[role="_abstract"]
The real-time preemption is the mechanism to temporarily interrupt an executing task, with the intention of resuming it at a later time. It occurs when a higher priority process interrupts the CPU usage. Preemption can have a particularly negative impact on performance, and constant preemption can lead to a state known as thrashing. This problem occurs when processes are constantly preempted and no process ever gets to run completely. Changing the priority of a task can help reduce involuntary preemption.

You can check for voluntary and involuntary preemption occurring on a single process by viewing the contents of the `/proc/PID/status` file, where PID is the process identifier.

The following example shows the preemption status of a process with PID 1000.

[literal,subs="+quotes,verbatim,normal"]
----
# *grep voluntary /proc/1000/status*
voluntary_ctxt_switches: 194529
nonvoluntary_ctxt_switches: 195338
----
