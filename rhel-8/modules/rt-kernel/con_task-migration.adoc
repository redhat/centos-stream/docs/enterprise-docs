:_mod-docs-content-type: CONCEPT

[id="con_task-migration_{context}"]
= Task migration

[role="_abstract"]
If a `SCHED_OTHER` task spawns a large number of other tasks, they will all run on the same CPU. The `migration` task or `softirq` will try to balance these tasks so they can run on idle CPUs.

The `sched_nr_migrate` option can be adjusted to specify the number of tasks that will move at a time. Because real-time tasks have a different way to migrate, they are not directly affected by this. However, when `softirq` moves the tasks, it locks the run queue spinlock, thus disabling interrupts.

If there are a large number of tasks that need to be moved, it occurs while interrupts are disabled, so no timer events or wakeups will be allowed to happen simultaneously. This can cause severe latencies for real-time tasks when `sched_nr_migrate` is set to a large value.
