////
Base the file name and the ID on the module title. For example:
* file name: con-my-concept-module-a.adoc
* ID: [id="con-my-concept-module-a_{context}"]
* Title: = My concept module A
////

////
Indicate the module type in one of the following
ways:
Add the prefix con- or con_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: CONCEPT



[id="con_processes_{context}"]
= Processes


[role="_abstract"]
A real-time process, in simplest terms, is a program in execution. The term process refers to an independent address space, potentially containing multiple threads. When the concept of more than one process running inside one address space was developed, Linux turned to a process structure that shares an address space with another process. This works well, as long as the process data structure is small.

A UNIX®-style process construct contains:

* Address mappings for virtual memory.
* An execution context (PC, stack, registers).
* State and accounting information.

In real-time, each process starts with a single thread, often called the parent thread. You can create additional threads from parent threads using the `fork()` system calls. `fork()` creates a new child process which is identical to the parent process except for the new process identifier. The child process runs independent of the creating process. The parent and child processes can be executed simultaneously. The difference between the `fork()` and `exec()` system calls is that, `fork()` starts a new process which is the copy of the parent process and `exec()` replaces the current process image with the new one.

In real-time, the `fork()` system call, when successful, returns the process identifier of the child process and the parent process returns a non-zero value. On error, it returns an error number.
