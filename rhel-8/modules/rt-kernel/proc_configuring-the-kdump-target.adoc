:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_configuring-kdump-on-the-command-line-consolidated.adoc

[id="configuring-the-kdump-target_{context}"]
= Configuring the kdump target

[role="_abstract"]
The crash dump is usually stored as a file in a local file system, written directly to a device. Alternatively, you can set up for the crash dump to be sent over a network by using the `NFS` or `SSH` protocols. To preserve a crash dump file, only one of these options can be set at a time. The default behavior is to store it in the `/var/crash/` directory of the local file system. 

.Prerequisites

* You have root permissions on the system.

* Fulfilled requirements for `kdump` configurations and targets.
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#supported-kdump-configurations-and-targets_managing-monitoring-and-updating-the-kernel[Supported kdump configurations and targets].

.Procedure
* To store the crash dump file in `/var/crash/` directory of the local file system, edit the `/etc/kdump.conf` file and specify the path:
+
----
path /var/crash
----
+
The option `path /var/crash` represents the path to the file system in which `kdump` saves the crash dump file. When you specify a dump target in the `/etc/kdump.conf` file, then the `path` is relative to the specified dump target.
+
If you do not specify a dump target in the `/etc/kdump.conf` file, then the `path` represents the absolute path from the root directory. Depending on what is mounted in the current system, the dump target and the adjusted dump path are taken automatically.

[WARNING]
====
`kdump` saves the crash dump file in `/var/crash/var/crash` directory, when the dump target is mounted at `/var/crash` and the option `path` is also set as `/var/crash` in the `/etc/kdump.conf` file. For example, in the following instance, the `ext4` file system is already mounted at `/var/crash` and the `path` are set as `/var/crash`:

[subs="+quotes,verbatim"]
....
# *grep -v ^# /etc/kdump.conf | grep -v ^$*
ext4 /dev/mapper/vg00-varcrashvol
path /var/crash
core_collector makedumpfile -c --message-level 1 -d 31
....

This results in the `/var/crash/var/crash` path. To solve this problem, use the option `path /` instead of `path /var/crash`
====

* To change the local directory in which the crash dump is to be saved, as `root`, edit the `/etc/kdump.conf` configuration file as described below.

. Remove the hash sign ("#") from the beginning of the `#path /var/crash` line.

. Replace the value with the intended directory path. For example:
+
[subs="quotes"]
----
path /usr/local/cores
----
+
[IMPORTANT]
====
In {ProductShortName} 8, the directory defined as the kdump target using the `path` directive must exist before the `kdump` `systemd` service starts. This prevents `systemd` service failures. This behavior is different from earlier releases of {ProductShortName}, where the directory creates automatically if it does exists when the service starts.
====

* To write the file to a different partition, as `root`, edit the `/etc/kdump.conf` configuration file as described below.

. Remove the hash sign ("#") from the beginning of the `#ext4` line, depending on your choice.
+
 ** device name (the `#ext4 /dev/vg/lv_kdump` line)
 ** file system label (the `#ext4 LABEL=/boot` line)
 ** UUID (the `#ext4 UUID=03138356-5e61-4ab3-b58e-27507ac41937` line)

. Change the file system type as well as the device name, label or UUID to the desired values. For example:
+
[subs="quotes"]
----
ext4 UUID=03138356-5e61-4ab3-b58e-27507ac41937
----
+
[IMPORTANT]
====

It is recommended to specify storage devices using a `LABEL=` or `UUID=`. Disk device names such as `/dev/sda3` does not guarantee consistent behavior across reboot.

====
+
ifeval::["{ProjectNameID}" == "managing-monitoring-and-updating-the-kernel"]
[IMPORTANT]
====

When dumping to Direct Access Storage Device (DASD) on IBM Z hardware, it is essential that the you correctly specify dump devices in the `/etc/dasd.conf` file before proceeding.

====
endif::[]

* To write the crash dump directly to a device, edit the `/etc/kdump.conf` configuration file:

. Remove the hash sign ("#") from the beginning of the `#raw /dev/vg/lv_kdump` line.

. Replace the value with the intended device name. For example:
+
[subs="quotes"]
----
raw /dev/sdb1
----

* To store the crash dump to a remote machine using the `NFS` protocol, edit the `/etc/kdump.conf` configuration file:

. Remove the hash sign ("#") from the beginning of the `#nfs my.server.com:/export/tmp` line.

. Replace the value with a valid hostname and directory path. For example:
+
[subs="quotes"]
----
nfs penguin.example.com:/export/cores
----

* To store the crash dump to a remote machine using the `SSH` protocol, edit the `/etc/kdump.conf` configuration file:

. Remove the hash sign ("#") from the beginning of the `#ssh ++user@my.server.com++` line.

. Replace the value with a valid username and hostname.

. Include your `SSH` key in the configuration.
+
** Remove the hash sign from the beginning of the `#sshkey /root/.ssh/kdump_id_rsa` line.
+
** Change the value to the location of a key valid on the server you are trying to dump to. For example:
+
[subs="quotes"]
----
ssh john@penguin.example.com
sshkey /root/.ssh/mykey
----

////
[role="_additional-resources"]
.Additional resources
ifdef::kernel-title[]

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_basic_system_settings/index[Configuring basic system settings] in {ProductShortName}
endif::kernel-title[]
ifndef::kernel-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_monitoring_and_updating_the_kernel/installing-and-configuring-kdump_managing-monitoring-and-updating-the-kernel#configuring-the-kdump-target_configuring-kdump-on-the-command-line[Supported kdump configurations and targets]
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_basic_system_settings/index#sec-SSH[Configuring basic system settings] in {ProductShortName}
endif::kernel-title[]
////
