:_mod-docs-content-type: PROCEDURE
[id="proc_moving-interrupts-to-specified-cpus-using-the-tuna-interface_{context}"]
= Moving interrupts to specified CPUs using the `tuna` CLI
:experimental:
[role="_abstract"]
You can use the [application]`tuna` CLI to move interrupts (IRQs) to dedicated CPUs to minimize or eliminate latency in real-time environments. For more information about moving IRQs, see xref:con_interrupt-and-process-binding_assembly_binding-interrupts-and-processes[Interrupt and process binding].

.Prerequisites

* The `tuna` and `python-linux-procfs` packages are installed.
* You have root permissions on the system.

.Procedure

. List the CPUs to which a list of IRQs is attached.
+
ifeval::[{ProductNumber} == 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna --irqs=_<irq_list>_ --show_irqs*
----
endif::[]

ifeval::[{ProductNumber} != 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna show_irqs --irqs=_<irq_list>_*
----
endif::[]
+
`_irq_list_` is a comma-separated list of the IRQs for which you want to list attached CPUs.
+
For example:
+
ifeval::[{ProductNumber} == 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna --irqs=128 --show_irqs*
   # users         affinity
 128 iwlwifi          0,1,2,3
----
endif::[]

ifeval::[{ProductNumber} != 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna show_irqs --irqs=128*
----
endif::[]

. Attach a list of IRQs to a list of CPUs.
+
ifeval::[{ProductNumber} == 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna --irqs=_irq_list_ --cpus=_<cpu_list>_ --move*
----
endif::[]

ifeval::[{ProductNumber} != 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna move --irqs=irq_list --cpus=_<cpu_list>_*
----
endif::[]
+
`_irq_list_` is a comma-separated list of the IRQs you want to attach and `_cpu_list_` is a comma-separated list of the CPUs to which they will be attached or a range of CPUs.
+
For example:
+
ifeval::[{ProductNumber} == 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna --irqs=128 --cpus=3 --move*
----
endif::[]

ifeval::[{ProductNumber} != 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna move --irqs=128 --cpus=3*
----
endif::[]

.Verification 

* Compare the state of the selected IRQs before and after moving any IRQ to a specified CPU.

+
ifeval::[{ProductNumber} == 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna --irqs=128 --show_irqs*
   # users         affinity
 128 iwlwifi          3
----
endif::[]

ifeval::[{ProductNumber} != 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna show_irqs --irqs=128*
----
endif::[]