:_mod-docs-content-type: PROCEDURE
:experimental:
[id="proc_changing-the-clock-source-to-use_{context}"]
= Temporarily changing the clock source to use

[role="_abstract"]
Sometimes the best-performing clock for a system’s main application is not used due to known problems on the clock. After ruling out all problematic clocks, the system can be left with a hardware clock that is unable to satisfy the minimum requirements of a real-time system.

Requirements for crucial applications vary on each system. Therefore, the best clock for each application, and consequently each system, also varies. Some applications depend on clock resolution, and a clock that delivers reliable nanoseconds readings can be more suitable. Applications that read the clock too often can benefit from a clock with a smaller reading cost (the time between a read request and the result).

In these cases it is possible to override the clock selected by the kernel, provided that you understand the side effects of the override and can create an environment which will not trigger the known shortcomings of the given hardware clock.

[IMPORTANT]
The kernel automatically selects the best available clock source. Overriding the selected clock source is not recommended unless the implications are well understood.



.Prerequisites

* You have root permissions on the system.

.Procedure

. View the available clock sources.
+
[literal,subs=quotes]
----
# *cat /sys/devices/system/clocksource/clocksource0/available_clocksource*
tsc hpet acpi_pm
----
+
As an example, consider the available clock sources in the system are TSC, HPET, and ACPI_PM.


. Write the name of the clock source you want to use to the [filename]`/sys/devices/system/clocksource/clocksource0/current_clocksource` file.
+
[literal,subs=quotes]
----
# *echo hpet > /sys/devices/system/clocksource/clocksource0/current_clocksource*
----
+
[NOTE]
The changes apply to the clock source currently in use. When the system reboots, the default clock is used. To make the change persistent, see xref:proc_making-proc-sys-parameter-changes-persistent_setting-persistent-tuning-parameters[Making persistent kernel tuning parameter changes].

.Verification

* Display the `current_clocksource` file to ensure that the current clock source is the specified clock source.
+
[literal,subs=quotes]
----
# *cat /sys/devices/system/clocksource/clocksource0/current_clocksource*
hpet
----
+
The example uses HPET as the current clock source in the system. 
