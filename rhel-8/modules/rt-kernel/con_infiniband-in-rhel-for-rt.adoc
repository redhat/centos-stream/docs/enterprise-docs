:_mod-docs-content-type: CONCEPT

[id="con_infiniband-in-rhel-for-rt"]
= InfiniBand in RHEL for Real Time

[role="_abstract"]
InfiniBand is a type of communications architecture often used to increase bandwidth, improve quality of service (QOS), and provide for failover. It can also be used to improve latency by using the Remote Direct Memory Access (RDMA) mechanism.

The support for InfiniBand on RHEL for Real Time is the same as the support available on {ProductName} {ProductNumber}. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/configuring_infiniband_and_rdma_networks/index[Configuring InfiniBand and RDMA networks].
