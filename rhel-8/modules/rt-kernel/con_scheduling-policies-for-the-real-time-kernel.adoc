:_mod-docs-content-type: CONCEPT

[id="scheduling-policies-for-the-real-time-kernel_{context}"]
= Scheduling policies for the real-time kernel

The real-time scheduling policies share one main characteristic: they run until a higher priority thread interrupts the thread or the threads wait, either by sleeping or performing I/O. 

In the case of `SCHED_RR`, the operating system interrupts a running thread so that another thread of equal `SCHED_RR` priority can run. In either of these cases, no provision is made by the `POSIX` specifications that define the policies for allowing lower priority threads to get any CPU time. This characteristic of real-time threads means that it is easy to write an application, which monopolizes 100% of a given CPU. However, this causes problems for the operating system. For example, the operating system is responsible for managing both system-wide and per-CPU resources and must periodically examine data structures describing these resources and perform housekeeping activities with them. But if a core is monopolized by a `SCHED_FIFO` thread, it cannot perform its housekeeping tasks. Eventually the entire system becomes unstable and can potentially crash.

On the RHEL for Real Time kernel, interrupt handlers run as threads with a `SCHED_FIFO` priority. The default priority is 50. A cpu-hog thread with a `SCHED_FIFO` or `SCHED_RR` policy higher than the interrupt handler threads can prevent interrupt handlers from running. This causes the  programs waiting for data signaled by those interrupts to starve and fail.





