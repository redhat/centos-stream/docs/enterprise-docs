:_mod-docs-content-type: PROCEDURE

[id="proc_analyzing-performance-analysis-results_{context}"]
= Analyzing performance analysis results

[role="_abstract"]
The data from the `perf record` feature can now be investigated directly using the `perf report` command.

.Procedure

* Analyze the results directly from the [filename]`perf.data` file or from an archived tarball.
+
[literal,subs="+quotes,attributes"]
----
# *perf report*
----
+
The output of the report is sorted according to the maximum CPU usage in percentage by the application. It shows if the sample has occurred in the kernel or user space of the process.
+
The report shows information about the module from which the sample was taken:
+
** A kernel sample that did not take place in a kernel module is marked with the notation `[kernel.kallsyms]`.

** A kernel sample that took place in the kernel module is marked as `[module]`, `[ext4]`.

** For a process in user space, the results might show the shared library linked with the process.

+
The report denotes whether the process also occurs in kernel or user space.
** The result `[.]` indicates user space.

** The result `[k]` indicates kernel space.

+
Finer grained details are available for review, including data appropriate for experienced `perf` developers.
