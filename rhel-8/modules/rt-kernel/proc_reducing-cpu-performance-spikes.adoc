:_mod-docs-content-type: PROCEDURE

[id="proc_reducing-cpu-performance-spikes_{context}"]
= Reducing CPU performance spikes

[role="_abstract"]

A common source of latency spikes is when multiple CPUs contend on common locks in the kernel timer tick handler. The usual lock responsible for the contention is `xtime_lock`, which is used by the timekeeping system and the Read-Copy-Update (RCU) structure locks. By using `skew_tick=1`, you can offset the timer tick per CPU to start at a different time and avoid potential lock conflicts. 

The [parameter]`skew_tick` kernel command line parameter might prevent latency fluctuations on moderate to large systems with large core-counts and have latency-sensitive workloads.   

.Prerequisites

* You have administrator permissions.

.Procedure

. Enable the `skew_tick=1` parameter with `grubby`. 
+
[subs="+quotes"]
----
# *grubby --update-kernel=ALL --args="skew_tick=1"*
----

. Reboot for changes to take effect.
+
[subs="+quotes"]
----
# *reboot*
----

[NOTE]
====
Enabling `skew_tick=1` causes a significant increase in power consumption and, therefore, you must enable the `skew` boot parameter only if you are running latency sensitive real-time workloads and consistent latency is an important consideration over power consumption.
====

.Verification
Display the `/proc/cmdline` file and ensure `skew_tick=1` is specified. The `/proc/cmdline` file shows the parameters passed to the kernel. 

* Check the new settings in the `/proc/cmdline` file.
+
[subs="+quotes"]
----
# cat /proc/cmdline
----

