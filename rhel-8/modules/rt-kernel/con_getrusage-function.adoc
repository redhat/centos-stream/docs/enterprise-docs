:_mod-docs-content-type: CONCEPT

// [id="con_getrusage-function_{context}"]
= getrusage() function

The `getrusage()` function retrieves important information from a specified process or its threads. It reports on information such as:

* The number of voluntary and involuntary context switches.

* Major and minor page faults.

* Amount of memory in use.

`getrusage()` enables you to query an application to provide information relevant to both performance tuning and debugging activities. `getrusage()` retrieves information that would otherwise need to be cataloged from several different files in the `/proc/` directory and would be hard to synchronize with specific actions or events on the application.

[NOTE]
====
Not all the fields contained in the structure filled with `getrusage()` results are set by the kernel. Some of them are kept for compatibility reasons only.
====

[role="_additional-resources"]
.Additional resources
* `getrusage(2)` man page on your system
