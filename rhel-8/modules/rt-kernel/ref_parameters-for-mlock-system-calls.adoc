
:_mod-docs-content-type: REFERENCE

[id="ref_parameters-for-mlock-system-calls_{context}"]
= Parameters for mlock() system calls


[role="_abstract"]
The parameters for memory lock system call and the functions they perform are listed and described in the `mlock` parameters table.


.`mlock` parameters
[options="header"]
|====
|Parameter|Description
|`addr`|Specifies the process address space to lock or unlock. When NULL, the kernel chooses the page-aligned arrangement of data in the memory. If `addr` is not NULL, the kernel chooses a nearby page boundary, which is always above or equal to the value specified in `/proc/sys/vm/mmap_min_addr` file.

|`len`
|Specifies the length of the mapping, which must be greater than 0.
|`fd`|Specifies the file descriptor.
|`prot`|`mmap` and `munmap` calls  define the desired memory protection with this parameter. `prot` takes one or a combination of `PROT_EXEC`, `PROT_READ`, `PROT_WRITE` or `PROT_NONE` values.
|`flags`|Controls the mapping  visibility to other processes that map the same file. It takes one of
the values: `MAP_ANONYMOUS`, `MAP_LOCKED`, `MAP_PRIVATE` or `MAP_SHARED` values.
|`MCL_CURRENT`|Locks all pages that are currently mapped into a process.
|`MCL_FUTURE`|Sets the mode to lock subsequent memory allocations. These could be new pages required by a growing heap  and stack, new memory-mapped files, or shared memory regions.
|====
