:_mod-docs-content-type: PROCEDURE

[id="proc_disabling-messages-from-printing-on-graphics-console_{context}"]

= Disabling messages from printing on graphics console

[role="_abstract"]
You can control the amount of output messages that are sent to the graphics console by configuring the required log levels in the `/proc/sys/kernel/printk` file.

.Procedure
. View the current console log level:
+
[subs="quotes"]
----
$ *cat /proc/sys/kernel/printk*
  7    4    1    7
----
+
The command prints the current settings for system log levels. The numbers correspond to current, default, minimum, and boot-default values for the system logger.
. Configure the desired log level in the `/proc/sys/kernel/printk` file.
+
[subs="quotes"]
----
$ *echo “1” > /proc/sys/kernel/printk*
----    
+
The command changes the current console log level. For example, setting log level 1, will print only alert messages and prevent display of other messages on the graphics console.
