:_mod-docs-content-type: CONCEPT
[id="con_power-saving-states_{context}"]
= Power saving states

[role="_abstract"]
Modern processors actively transition to higher power saving states (C-states) from lower states. Unfortunately, transitioning from a high power saving state back to a running state can consume more time than is optimal for a real-time application. To prevent these transitions, an application can use the Power Management Quality of Service (PM QoS) interface.

With the PM QoS interface, the system can emulate the behavior of the `idle=poll` and `processor.max_cstate=1` parameters, but with a more fine-grained control of power saving states. `idle=poll` prevents the processor from entering the `idle` state. `processor.max_cstate=1` prevents the processor from entering deeper C-states (energy-saving modes).

When an application holds the [filename]`/dev/cpu_dma_latency` file open, the PM QoS interface prevents the processor from entering deep sleep states, which cause unexpected latencies when they are being exited. When the file is closed, the system returns to a power-saving state.


// [role="_additional-resources"]
// .Additional resources
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * Currently, modules cannot include xrefs, so you cannot include links to other content in your collection. If you need to link to another assembly, add the xref to the assembly that includes this module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
