:_mod-docs-content-type: PROCEDURE

[id="proc_cleaning-up-a-mutex-attribute-object_{context}"]
= Cleaning up a mutex attribute object

[role="_abstract"]
After the mutex has been created using the mutex attribute object, you can keep the attribute object to initialize more mutexes of the same type, or you can clean it up. The mutex is not affected in either case.

.Procedure
* Clean up the attribute object using the `pthread_mutexattr_destroy()` function:
+
[literal,subs="+quotes"]
----
pthread_mutexattr_destroy(_&my_mutex_attr_);
----
+
The mutex now operates as a regular *pthread_mutex* and can be locked, unlocked, and destroyed as normal.
