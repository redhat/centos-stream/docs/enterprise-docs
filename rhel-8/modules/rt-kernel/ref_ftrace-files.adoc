:_module-type: REFERENCE

[id="ref_ftrace-files_{context}"]
= ftrace files

[role="_abstract"]
The following are the main files in the [filename]`/sys/kernel/debug/tracing/` directory.

.ftrace files

trace:: The file that shows the output of an `ftrace` trace. This is really a snapshot of the trace in time, because the trace stops when this file is read, and it does not consume the events read. That is, if the user disabled tracing and reads this file, it will report the same thing every time it is read.

trace_pipe:: The file that shows the output of an `ftrace` trace as it reads the trace live. It is a producer/consumer trace. That is, each read will consume the event that is read. This can be used to read an active trace without stopping the trace as it is read.

available_tracers:: A list of ftrace tracers that have been compiled into the kernel.

current_tracer:: Enables or disables an `ftrace` tracer.

events:: A directory that contains events to trace and can be used to enable or disable events, as well as set filters for the events.

tracing_on:: Disable and enable recording to the `ftrace` buffer. Disabling tracing via the `tracing_on` file does not disable the actual tracing that is happening inside the kernel. It only disables writing to the buffer. The work to do the trace still happens, but the data does not go anywhere.
