:_module-type: REFERENCE

[id="ref_ftrace-tracers_{context}"]
= ftrace tracers

[role="_abstract"]
Depending on how the kernel is configured, not all tracers may be available for a given kernel. For the RHEL for Real Time kernels, the trace and debug kernels have different tracers than the production kernel does. This is because some of the tracers have a noticeable overhead when the tracer is configured into the kernel, but not active. Those tracers are only enabled for the `trace` and `debug` kernels.

.Tracers

function:: One of the most widely applicable tracers. Traces the function calls within the kernel. This can cause noticeable overhead depending on the number of functions traced. When not active, it creates little overhead.

function_graph:: The `function_graph` tracer is designed to present results in a more visually appealing format. This tracer also traces the exit of the function, displaying a flow of function calls in the kernel.
+
[NOTE]
This tracer has more overhead than the `function` tracer when enabled, but the same low overhead when disabled.

wakeup:: A full CPU tracer that reports the activity happening across all CPUs. It records the time that it takes to wake up the highest priority task in the system, whether that task is a real time task or not. Recording the max time it takes to wake up a non-real time task hides the times it takes to wake up a real time task.

wakeup_rt:: A full CPU tracer that reports the activity happening across all CPUs. It records the time that it takes from the current highest priority task to wake up to until the time it is scheduled. This tracer only records the time for real time tasks.

preemptirqsoff:: Traces the areas that disable preemption or interrupts, and records the maximum amount of time for which preemption or interrupts were disabled.

preemptoff:: Similar to the preemptirqsoff tracer, but traces only the maximum interval for which pre-emption was disabled.

irqsoff:: Similar to the preemptirqsoff tracer, but traces only the maximum interval for which interrupts were disabled.

nop:: The default tracer. It does not provide any tracing facility itself, but as events may interleave into any tracer, the `nop` tracer is used for specific interest in tracing events.
