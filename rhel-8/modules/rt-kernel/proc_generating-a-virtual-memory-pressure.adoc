////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

[id="proc_generating-a-virtual-memory-pressure_{context}"]
= Generating a virtual memory pressure


[role="_abstract"]
When under memory pressure, the kernel starts writing pages out to swap. You can stress the virtual memory by using the `--page-in` option to force non-resident pages to swap back into the virtual memory. This causes the virtual machine to be heavily exercised. Using the `--page-in` option, you can enable this mode for the `bigheap`, `mmap` and virtual machine (`vm`) stressors. The `--page-in` option, touch allocated pages that are not in core, forcing them to page in.

.Prerequisites

* You have root privileges on the system.

.Procedure

* To stress test a virtual memory, use the `--page-in` option:
+
[literal,subs="+quotes",options="nowrap"]
----
# *stress-ng --vm 2 --vm-bytes 2G --mmap 2 --mmap-bytes 2G --page-in*
----
+
In this example, `stress-ng` tests memory pressure on a system with 4GB of memory, which is less than the allocated buffer sizes, 2 x 2GB of `vm` stressor and 2 x 2GB of `mmap` stressor with `--page-in` enabled.
