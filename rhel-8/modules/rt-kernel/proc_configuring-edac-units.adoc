:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Setting BIOS parameters for system tuning
:experimental:
[id="proc_configuring-edac-units_{context}"]
= Improving response times by disabling error detection and correction units

[role="_abstract"]

Error Detection and Correction (EDAC) units are devices for detecting and correcting errors signaled from Error Correcting Code (ECC) memory. Usually EDAC options range from no ECC checking to a periodic scan of all memory nodes for errors. The higher the EDAC level, the more time the BIOS uses. This may result in missing crucial event deadlines.

To improve response times, turn off EDAC. If this is not possible, configure EDAC to the lowest functional level.
