:_mod-docs-content-type: PROCEDURE

[id="proc_isolating-cpus-using-tuneds-isolated_cores-option_{context}"]
= Isolating CPUs using TuneD’s isolated_cores option

[role="_abstract"]
The initial mechanism for isolating CPUs is specifying the boot parameter `isolcpus=cpulist` on the kernel boot command line. The recommended way to do this for RHEL for Real Time is to use the [daemon]`TuneD` daemon and its [package]`tuned-profiles-realtime` package.

[NOTE]
====
In [package]`tuned-profiles-realtime` version 2.19 and later, the built-in function [variable]`calc_isolated_cores` applies the initial CPU setup automatically. The [filename]`/etc/tuned/realtime-variables.conf` configuration file includes the default variable content as `isolated_cores=${f:calc_isolated_cores:2}`.

By default, [variable]`calc_isolated_cores` reserves one core per socket for housekeeping and isolates the rest. If you must change the default configuration, comment out the `isolated_cores=${f:calc_isolated_cores:2}` line in `/etc/tuned/realtime-variables.conf` configuration file and follow the procedure steps for Isolating CPUs using TuneD’s `isolated_cores` option.
====

.Prerequisites

* The `TuneD` and `tuned-profiles-realtime` packages are installed.
* You have root permissions on the system.

.Procedure

. As a root user, open [filename]`/etc/tuned/realtime-variables.conf` in a text editor.

. Set [variable]`isolated_cores=_cpulist_` to specify the CPUs that you want to isolate. You can use CPU numbers and ranges.
+
.Examples:
+
----
isolated_cores=0-3,5,7
----
+
This isolates cores 0, 1, 2, 3, 5, and 7.
+
In a two socket system with 8 cores, where NUMA node 0 has cores 0-3 and NUMA node 1 has cores 4-8, to allocate two cores for a multi-threaded application, specify:
+
----
isolated_cores=4,5
----
+
This prevents any user-space threads from being assigned to CPUs 4 and 5.
+
To pick CPUs from different NUMA nodes for unrelated applications, specify:
+
----
isolated_cores=0,4
----
This prevents any user-space threads from being assigned to CPUs 0 and 4.

. Activate the real-time `TuneD` profile using the `tuned-adm` utility.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuned-adm profile realtime*
----
. Reboot the machine for changes to take effect.

.Verification

* Search for the `isolcpus` parameter in the kernel command line:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
----
$ *cat /proc/cmdline | grep isolcpus*
BOOT_IMAGE=/vmlinuz-4.18.0-305.rt7.72.el8.x86_64 root=/dev/mapper/rhel_foo-root ro crashkernel=auto rd.lvm.lv=rhel_foo/root rd.lvm.lv=rhel_foo/swap console=ttyS0,115200n81 *isolcpus=0,4*
----
