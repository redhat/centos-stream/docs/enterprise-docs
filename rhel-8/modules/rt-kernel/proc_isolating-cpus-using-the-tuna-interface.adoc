:_mod-docs-content-type: PROCEDURE
[id="proc_isolating-cpus-using-the-tuna-interface_{context}"]
= Isolating CPUs using the `tuna` CLI
:experimental:
[role="_abstract"]
You can use the [application]`tuna` CLI to isolate interrupts (IRQs) from user processes on different dedicated CPUs to minimize latency in real-time environments. For more information about isolating CPUs, see xref:con_interrupt-and-process-binding_assembly_binding-interrupts-and-processes[Interrupt and process binding].

.Prerequisites

* The `tuna` and the `python-linux-procfs` packages are installed.
* You have root permissions on the system.

.Procedure

* Isolate one or more CPUs.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
ifeval::[{ProductNumber} == 8]
----
# *tuna --cpus=_<cpu_list>_ --isolate*
----
endif::[]

ifeval::[{ProductNumber} != 8]
----
# *tuna isolate --cpus=_<cpu_list>_*
----
endif::[]
+
`_cpu_list_` is a comma-separated list or a range of CPUs to isolate.
+
For example:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
ifeval::[{ProductNumber} == 8]
----
# *tuna --cpus=0,1 --isolate*
----
endif::[]

ifeval::[{ProductNumber} != 8]
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna isolate --cpus=0,1*
----
+
or
+
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *tuna isolate --cpus=0-5*
----
endif::[]