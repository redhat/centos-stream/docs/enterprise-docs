////
Base the file name and the ID on the module title. For example:
* file name: con-my-concept-module-a.adoc
* ID: [id="con-my-concept-module-a_{context}"]
* Title: = My concept module A
////

////
Indicate the module type in one of the following
ways:
Add the prefix con- or con_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: CONCEPT

////
The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID so you can include it multiple times in the same guide.
////

[id="con_barriers_{context}"]
= Barriers

[role="_abstract"]
Barriers operate in a very different way when compared to other thread synchronization methods. The barriers define a point in the code where all active threads stop until all threads and processes reach this barrier. Barriers are used in situations when a running application needs to ensure that all threads have completed specific tasks before execution can continue.

The barrier mutex in real-time, take following two variables:

* The first variable records the `stop` and `pass` state of the barrier.
* The second variable records the total number of threads that enter the barrier.

The barrier sets the state to `pass` only when the specified number of threads reach the defined barrier. When the barrier state is set to `pass`, the threads and processes proceed further. The `pthread_barrier_init()` function allocates the required resources to use the defined barrier and initializes it with the attributes referenced by the `attr` attribute object.

The `pthread_barrier_init()` and `pthread_barrier_destroy()` functions return the zero value, when successful. On error, they return an error number.
