:_mod-docs-content-type: PROCEDURE
:experimental:
[id="proc_prioritizing-processes-to-kill-in-an-out-of-memory-state_{context}"]
= Prioritizing processes to kill when in an Out of Memory state

[role="_abstract"]
You can prioritize the processes that get terminated by the [function]`oom_killer()` function. This can ensure that high-priority processes keep running during an OOM state. Each process has a directory, `/proc/_PID_`. Each directory includes the following files:

* [filename]`oom_adj` - Valid scores for `oom_adj` are in the range -16 to +15. This value is used to calculate the performance footprint of the process, using an algorithm that also takes into account how long the process has been running, among other factors.

* [filename]`oom_score` - Contains the result of the algorithm calculated using the value in `oom_adj`.

In an Out of Memory state, the [function]`oom_killer()` function terminates processes with the highest `oom_score`.

You can prioritize the processes to terminate by editing the `oom_adj` file for the process.


.Prerequisites
* Know the process ID (PID) of the process you want to prioritize.

.Procedure

. Display the current `oom_score` for a process.
+
[subs="quotes"]
----
# *cat /proc/12465/oom_score*
79872
----

. Display the contents of `oom_adj` for the process.
+
[subs="quotes"]
----
# *cat /proc/12465/oom_adj*
13
----

. Edit the value in `oom_adj`.
+
[subs="quotes"]
----
# *echo -5 > /proc/12465/oom_adj*
----

.Verification

. Display the current `oom_score` for the process.
+
[subs="quotes"]
----
# *cat /proc/12465/oom_score*
78
----

. Verify that the displayed value is lower than the previous value.
