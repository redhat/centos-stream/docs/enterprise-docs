:_mod-docs-content-type: PROCEDURE
:experimental:
[id="proc_displaying-the-default-kernel_{context}"]
= Displaying the default kernel
[role="_abstract"]

You can display the kernel configured to boot by default.

// Kernel versions in RHEL 8 and 9:
ifeval::[{ProductNumber} == 8]
:kernel: /boot/vmlinuz-4.18.0-80.rt9.138.el8.x86_64
endif::[]
ifeval::[{ProductNumber} == 9]
:kernel: /boot/vmlinuz-kernel-rt-5.14.0-70.13.1.rt21.83.el9_0
endif::[]

.Procedure

* To view the default kernel:
+
[literal,subs="quotes,attributes"]
----
# *grubby --default-kernel*

{kernel}
----
+
The `rt` in the output of the command shows that the default kernel is a real time kernel.
