:_mod-docs-content-type: CONCEPT

[id="con_the-effects-of-using-tcp_nodelay_{context}"]
= The effects of using TCP_NODELAY

[role="_abstract"]
Applications that require low latency on every packet sent must be run on sockets with the [option]`TCP_NODELAY` option enabled.
This sends buffer writes to the kernel as soon as an event occurs.

Note:: For [option]`TCP_NODELAY` to be effective, applications must avoid doing small, logically related buffer writes.
Otherwise, these small writes cause `TCP` to send these multiple buffers as individual packets, resulting in poor overall performance.

If applications have several buffers that are logically related and must be sent as one packet, apply one of the following workarounds to avoid poor performance:

* Build a contiguous packet in memory and then send the logical packet to `TCP` on a socket configured with [option]`TCP_NODELAY`.

* Create an I/O vector and pass it to the kernel using the [command]`writev` command on a socket configured with [option]`TCP_NODELAY`.

* Use the [option]`TCP_CORK` option. [option]`TCP_CORK` tells `TCP` to wait for the application to remove the cork before sending any packets.
This command causes the buffers it receives to be appended to the existing buffers. This allows applications to build a packet in kernel space,
which can be required when using different libraries that provide abstractions for layers.

When a logical packet has been built in the kernel by the various components in the application, the socket should be uncorked, allowing `TCP`
to send the accumulated logical packet immediately.
