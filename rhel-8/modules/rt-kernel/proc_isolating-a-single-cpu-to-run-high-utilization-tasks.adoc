
:_mod-docs-content-type: PROCEDURE

[id="proc_isolating-a-single-cpu-to-run-high-utilization-tasks_{context}"]
= Isolating a single CPU to run high utilization tasks


[role="_abstract"]
With the `cpusets` mechanism, you can assign a set of CPUs and memory nodes for `SCHED_DEADLINE` tasks. In a task set that has high and low CPU utilizing tasks, isolating a CPU to run the high utilization task and scheduling small utilization tasks on different sets of CPU, enables all tasks to meet the assigned `runtime`.


.Prerequisites

* You have root permissions on the system.

.Procedure

. Create two directories named as `cpuset`:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *cd /sys/fs/cgroup/cpuset/*
# *mkdir cluster*
# *mkdir partition*
----
+
. Disable the load balance of the root `cpuset` to create two new root domains in the `cpuset` directory:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *echo 0 > cpuset.sched_load_balance*
----
+
. In the cluster `cpuset`, schedule the low utilization tasks to run on CPU 1 to 7, verify memory size, and name the CPU as exclusive:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *cd cluster/*
# *echo 1-7 > cpuset.cpus*
# *echo 0 > cpuset.mems*
# *echo 1 > cpuset.cpu_exclusive*
----
+
. Move all low utilization tasks to the cpuset directory:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *ps -eLo lwp | while read thread; do echo $thread > tasks ; done*
----
+
. Create a partition named as `cpuset` and assign the high utilization task:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *cd ../partition/*
# *echo 1 > cpuset.cpu_exclusive*
# *echo 0 > cpuset.mems*
# *echo 0 > cpuset.cpus*
----
+
. Set the shell to the cpuset and start the deadline workload:
+
[literal,subs="+quotes,verbatim,normal"]
----
# echo $$ > tasks
----
+
With this setup, the task isolated in the partitioned `cpuset` directory does not interfere with the task in the cluster `cpuset` directory. This enables all real-time tasks to meet the scheduler deadline.
