:_mod-docs-content-type: PROCEDURE

[id="creating-a-rhel-for-edge-image-with-image-builder-using-the-command-line-interface_{context}"]
= Creating a RHEL for Edge Commit image by using image builder CLI
[role="_abstract"]
To create a RHEL for Edge Commit image by using RHEL image builder command-line interface, ensure that you have met the following prerequisites and follow the procedure.

.Prerequisites

* You have created a blueprint for RHEL for Edge Commit image.

.Procedure

. Create the RHEL for Edge Commit image.
+
[subs="quotes,attributes"]
----
# composer-cli compose start _blueprint-name_ _image-type_
----
+
Where,

* _blueprint-name_ is the RHEL for Edge blueprint name.
* _image-type_ is `edge-commit` for *network-based deployment*.
+
A confirmation that the composer process has been added to the queue appears. It also shows a Universally Unique Identifier (UUID) number for the image created.
Use the UUID number to track your build. Also keep the UUID number handy for further tasks.

. Check the image compose status.
+
[subs="quotes,attributes"]
----
# composer-cli compose status
----
+
The output displays the status in the following format:
+
[subs="quotes,attributes"]
----
_&lt;UUID&gt;_ RUNNING date _blueprint-name_ _blueprint-version_ _image-type_
----
+
NOTE: The image creation process takes up to 20 minutes to complete.
+
To interrupt the image creation process, run:
+
[subs="quotes,attributes"]
----
# composer-cli compose cancel _&lt;UUID&gt;_
----
+
To delete an existing image, run:
+
[subs="quotes,attributes"]
----
# composer-cli compose delete _&lt;UUID&gt;_
----
+
After the image is ready, you can download it and use the image on your *network deployments*.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/composing_installing_and_managing_rhel_for_edge_images/composing-a-rhel-for-edge-image-using-image-builder-command-line_composing-installing-managing-rhel-for-edge-images[Composing a RHEL for Edge image using RHEL image builder command-line]
