:_mod-docs-content-type: PROCEDURE

:experimental:

[id="adding-a-component-to-rhel-for-edge-image-blueprint-using-image-builder-in-rhel-web-console_{context}"]
= Adding a component to RHEL for Edge blueprint using image builder in RHEL web console

[role="_abstract"]
To add a component to a RHEL for Edge image blueprint, ensure that you have met the following prerequisites and then follow the procedure to edit the corresponding blueprint. 

.Prerequisites

* On a RHEL system, you have accessed the RHEL image builder dashboard.
* You have created a blueprint for RHEL for Edge image. 

.Procedure

. On the RHEL image builder dashboard, click the blueprint that you want to edit.
+
To search for a specific blueprint, enter the blueprint name in the filter text box, and click btn:[Enter].

. On the upper right side of the blueprint, click btn:[Edit Packages].
+
The  *Edit blueprints* wizard opens.

. On the *Details* page, update the blueprint name and click btn:[Next].

. On the *Packages* page, follow the steps:
.. In the *Available Packages*, enter the package name that you want to add in the filter text box, and click btn:[Enter].
+
A list with the component name appears.

.. Click btn:[&gt;] to add the component to the blueprint.

. On the *Review* page, click btn:[Save].
+ 
The blueprint is now updated with the new package.
