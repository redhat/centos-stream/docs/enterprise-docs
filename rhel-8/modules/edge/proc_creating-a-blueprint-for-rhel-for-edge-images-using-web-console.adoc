:_mod-docs-content-type: PROCEDURE

:experimental:

[id="creating-a-blueprint-for-rhel-for-edge-images-using-web-console_{context}"]
= Creating a blueprint for a RHEL for Edge image using image builder in the web console

[role="_abstract"]
To create a blueprint for a RHEL for Edge image by using RHEL image builder in RHEL web console, ensure that you have met the following prerequisites and then follow the procedure. 

.Prerequisites

* On a RHEL system, you have opened the RHEL image builder dashboard. 

.Procedure

. On the RHEL image builder dashboard, click btn:[Create Blueprint]. 
+
The *Create Blueprint* dialogue box opens. 

. On the `Details` page:
.. Enter the name of the blueprint and, optionally, its description. Click btn:[Next].

. Optional: In the `Packages` page: 
.. On the `Available packages` search, enter the package name and click the btn:[>] button to move it to the Chosen packages field. Search and include as many packages as you want. Click btn:[Next].
+
NOTE: These customizations are all optional unless otherwise specified.

. On the `Kernel` page, enter a kernel name and the command-line arguments.

. On the `File system` page, select `Use automatic partitioning`. OSTree systems do not support filesystem customization, because OSTree images have their own mount rule, such as read-only. Click  btn:[Next].

. On the `Services` page, you can enable or disable services:
.. Enter the service names you want to enable or disable, separating them by a comma, by space, or by pressing the btn:[Enter] key. Click btn:[Next].

. On the `Firewall` page, set up your firewall setting:
.. Enter the `Ports`, and the firewall services you want to enable or disable.
.. Click the btn:[Add zone] button to manage your firewall rules for each zone independently. Click btn:[Next].

. On the `Users` page, add a users by following the steps:
.. Click btn:[Add user].
.. Enter a `Username`, a `password`, and a `SSH key`. You can also mark the user as a privileged user, by clicking the `Server administrator` checkbox. Click btn:[Next].

. On the `Groups` page, add groups by completing the following steps:
.. Click the btn:[Add groups] button:
... Enter a `Group name` and a `Group ID`. You can add more groups. Click btn:[Next].
. On the `SSH keys` page, add a key:
.. Click the btn:[Add key] button.
... Enter the SSH key. 
... Enter a `User`. Click  btn:[Next].

. On the `Timezone` page, set your timezone settings:
.. On the `Timezone` field, enter the timezone you want to add to your system image. For example, add the following timezone format: "US/Eastern".
+
If you do not set a timezone, the system uses Universal Time, Coordinated (UTC) as default. 
+
.. Enter the `NTP` servers. Click btn:[Next].

. On the `Locale` page, complete the following steps:
.. On the `Keyboard` search field, enter the package name you want to add to your system image. For example: ["en_US.UTF-8"].
.. On the `Languages` search field, enter the package name you want to add to your system image. For example: "us". Click btn:[Next].

. On the `Others` page, complete the following steps:
.. On the `Hostname` field, enter the hostname you want to add to your system image. If you do not add a hostname, the operating system determines the hostname. 
.. Mandatory only for the Simplifier Installer image: On the `Installation Devices` field, enter a valid node for your system image. For example: `dev/sda`. Click btn:[Next].

. Mandatory only when building FIDO images: On the `FIDO device onboarding` page, complete the following steps:
.. On the `Manufacturing server URL` field, enter the following information:
... On the `DIUN public key insecure` field, enter the insecure public key.
... On the `DIUN public key hash` field, enter the public key hash.
... On the `DIUN public key root certs` field, enter the public key root certs. Click btn:[Next].

. On the `OpenSCAP` page, complete the following steps:
.. On the `Datastream` field, enter the `datastream` remediation instructions you want to add to your system image.
.. On the `Profile ID` field, enter the `profile_id` security profile you want to add to your system image. Click btn:[Next].

. Mandatory only when building Ignition images: On the `Ignition` page, complete the following steps:
.. On the `Firstboot URL` field, enter the package name you want to add to your system image.
.. On the `Embedded Data` field, drag or upload your file. Click btn:[Next].

. . On the `Review` page, review the details about the blueprint. Click btn:[Create].

The RHEL image builder view opens, listing existing blueprints.

