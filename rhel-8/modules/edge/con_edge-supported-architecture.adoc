:_mod-docs-content-type: CONCEPT
[id="edge-supported-architecture_{context}"]

= RHEL for Edge—supported architecture  

[role="_abstract"]
Currently, you can deploy RHEL for Edge images on AMD and Intel 64-bit systems.

ifeval::[{ProductNumber} == 8]
NOTE: RHEL for Edge does not support ARM systems in RHEL 8.
endif::[]

ifeval::[{ProductNumber} == 9]
NOTE: RHEL for Edge supports ARM systems on some devices. For more information about the supported devices, see link:https://catalog.redhat.com/hardware[Red Hat certified hardware].
endif::[]


