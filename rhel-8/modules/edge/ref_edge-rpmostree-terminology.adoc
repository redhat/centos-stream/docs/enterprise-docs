:_mod-docs-content-type: REFERENCE
[id="edge-rpmostree-terminology_{context}"]

= OSTree and `rpm-ostree` terminology

[role="_abstract"]
Following are some helpful terms that are used in context to OSTree and `rpm-ostree` images.

.OSTree and `rpm-ostree` terminology
[options="header"]
|===
| Term  | Definition
| `OSTree` | A tool used for managing Linux-based operating system versions. The OSTree tree view is similar to Git and is based on similar concepts.
| `rpm-ostree` | A hybrid image or system package that hosts operating system updates.
| `Commit` |  A release or image version of the operating system. RHEL image builder generates an OSTree commit for RHEL for Edge images. You can use these images to install or update RHEL on Edge servers.
| `Refs` | Represents a branch in OSTree. Refs always resolve to the latest commit. For example, `rhel/{ProductNumber}/x86_64/edge`.
| `Revision (Rev)` | SHA-256 for a specific commit.
| `Remote` | The http or https endpoint that hosts the OSTree content. This is analogous to the baseurl for a {PackageManagerCommand} repository.
| `static-delta` | Updates to OSTree images are always delta updates. In case of RHEL for Edge images, the TCP overhead can be higher than expected due to the updates to number of files. To avoid TCP overhead, you can generate static-delta between specific commits, and send the update in a single connection. This optimization helps large deployments with constrained connectivity.
|===
