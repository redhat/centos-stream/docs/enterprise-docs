:_mod-docs-content-type: PROCEDURE

:experimental:

[id="installing-the-rhel-for-edge-image-using-a-kickstart-file_{context}"]

= Performing an attended installation to an edge device by using Kickstart

[role="_abstract"]
For an attended installation in a network-based environment, you can install the RHEL for Edge image to a device by using the RHEL Installer ISO, a Kickstart file, and a web server. The web server serves the RHEL for Edge Commit and the Kickstart file to boot the link:https://developers.redhat.com/products/rhel/download[RHEL Installer ISO] image.

.Prerequisites

* You have made the RHEL for Edge Commit available by running a web server. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/composing_installing_and_managing_rhel_for_edge_images/index#setting-up-a-web-server-to-install-rhel-for-edge-image_installing-rpm-ostree-images[Setting up a web server to install RHEL for Edge images].
* You have created a `.qcow2` disk image to be used as the target of the attended installation. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html-single/configuring_and_managing_virtualization/index#creating-a-virtual-disk-image_managing-virtual-disk-images-by-using-the-cli[Creating a virtual disk image by using qemu-img].

.Procedure

. Create a Kickstart file. The following is an example in which the `ostreesetup` directive instructs the Anaconda Installer to fetch and deploy the commit. Additionally, it creates a user and a password.
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
----
lang en_US.UTF-8
keyboard us
timezone UTC
zerombr
clearpart --all --initlabel
autopart --type=plain --fstype=xfs --nohome
reboot
text
network --bootproto=dhcp
user --name=core --groups=wheel --password=edge
services --enabled=ostree-remount
ostreesetup --nogpg --url=http://edge_device_ip:port/repo/ --osname=rhel --remote=edge --ref=rhel/9/x86_64/edge
----

. Run the RHEL Anaconda Installer by using the `libvirt virt-install` utility to create a virtual machine (VM) with a RHEL operating system. Use the `.qcow2` disk image as the target disk in the attended installation:
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
----
virt-install \
--name rhel-edge-test-1 \
--memory 2048 \
--vcpus 2 \
--disk path=prepared_disk_image.qcow2,format=qcow2,size=8 \
--os-variant rhel{ProductNumber} \
--cdrom /home/username/Downloads/rhel-{ProductNumber}-x86_64-boot.iso
----

. On the installation screen: 
+
[id="non-network-based_{context}"]
.Red Hat Enterprise Linux boot menu
image::rhel-8.6-boot.png[The Red Hat Enterprise Linux boot menu]
+
.. Press the btn:[e] key to add an additional kernel parameter:
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
....
inst.ks=http://web-server_device_ip:port/kickstart.ks
....
+
The kernel parameter specifies that you want to install RHEL by using the Kickstart file and not the RHEL image contained in the RHEL Installer.

.. After adding the kernel parameters, press kbd:[Ctrl]+kbd:[X] to boot the RHEL installation by using the Kickstart file.
+
The RHEL Installer starts, fetches the Kickstart file from the server (HTTP) endpoint and executes the commands, including the command to install the RHEL for Edge image commit from the HTTP endpoint.
After the installation completes, the RHEL Installer prompts you for login details.

.Verification

. On the Login screen, enter your user account credentials and click btn:[Enter].

. Verify whether the RHEL for Edge image is successfully installed.
+
[subs="+quotes,attributes",options="nowrap",role="white-space-pre"]
----
$ rpm-ostree status
----
+
The command output provides the image commit ID and shows that the installation is successful.
+
Following is a sample output:
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
....
State: idle
Deployments:
* ostree://edge:rhel/{ProductNumber}/x86_64/edge
		  Timestamp: 2020-09-18T20:06:54Z
			Commit: 836e637095554e0b634a0a48ea05c75280519dd6576a392635e6fa7d4d5e96
....

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/60959[How to embed a Kickstart file into an ISO image] (Red Hat Knowledgebase)
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/interactively_installing_rhel_from_installation_media/index#booting-the-installer-from-local-media_rhel-installer[Booting the installation]
