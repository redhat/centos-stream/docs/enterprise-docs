:_mod-docs-content-type: PROCEDURE

[id="proc_creating-a-blueprint-for-a-simplified-image-using-image-builder-gui_{context}"]
= Creating a blueprint for a Simplified image RHEL using image builder GUI

To create a RHEL for Edge Simplified Installer image, you must create a blueprint and ensure that you customize it with:

* A device node location to enable an unattended installation to your device.
* A URL to perform the initial device credential exchange.
* A user or user group.

NOTE: You can also add any other customizations that your image requires.

To create a blueprint for a simplified RHEL for Edge image in the RHEL image builder GUI,  complete the following steps:

.Prerequisites

* You have opened the image builder app from the web console in a browser. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/composing_a_customized_rhel_system_image/index#accessing-composer-gui-in-the-rhel-8-web-console_creating-system-images-with-composer-web-console-interface[Accessing the RHEL image builder GUI in the RHEL web console].

.Procedure

. Click Create Blueprint in the upper-right corner of the RHEL image builder app.
+
A dialog wizard with fields for the blueprint name and description opens.

. On the `Details` page:
.. Enter the name of the blueprint and, optionally, its description. Click btn:[Next].

. Optional: On the Packages page, complete the following steps:
.. In the `Available packages` search, enter the package name and click the btn:[>] button to move it to the Chosen packages field. Search and include as many packages as you want. Click btn:[Next].
+
NOTE: The customizations are all optional unless otherwise specified.

. Optional: On the `Kernel` page, enter a kernel name and the command-line arguments.

. Optional: On the `File system` page, select `Use automatic partitioning`.The filesystem customization is not supported for OSTree systems, because OSTree images have their own mount rule, such as read-only. Click btn:[Next].

. Optional: On the `Services` page, you can enable or disable services:
.. Enter the service names you want to enable or disable, separating them by a comma, by space, or by pressing the btn:[Enter] key. Click btn:[Next].

. Optional: On the `Firewall` page, set up your firewall setting:
.. Enter the `Ports`, and the firewall services you want to enable or disable.
.. Click the btn:[Add zone] button to manage your firewall rules for each zone independently. Click btn:[Next].

. On the `Users` page, add a users by following the steps:
.. Click btn:[Add user].
.. Enter a `Username`, a `password`, and a `SSH key`. You can also mark the user as a privileged user, by clicking the `Server administrator` checkbox. 
+
NOTE: When you specify the user in the blueprint customization and then create an image from that blueprint,  the blueprint creates the user under the `/usr/lib/passwd` directory and the password under the `/usr/etc/shadow` during installation time. You can log in to the device with the username and password you created for the blueprint. After you access the system, you must create users, for example, using the `useradd` command.
+
Click btn:[Next].

. Optional: On the `Groups` page, add groups by completing the following steps:
.. Click the btn:[Add groups] button:
... Enter a `Group name` and a `Group ID`. You can add more groups. Click btn:[Next].

. Optional: On the `SSH keys` page, add a key:
.. Click the btn:[Add key] button.
... Enter the SSH key. 
... Enter a `User`. Click  btn:[Next].

. Optional: On the `Timezone` page, set your timezone settings:
.. On the `Timezone` field, enter the timezone you want to add to your system image. For example, add the following timezone format: "US/Eastern".
+
If you do not set a timezone, the system uses Universal Time, Coordinated (UTC) as default. 
+
.. Enter the `NTP` servers. Click btn:[Next].

. Optional: On the `Locale` page, complete the following steps:
.. On the `Keyboard` search field, enter the package name you want to add to your system image. For example: ["en_US.UTF-8"].
.. On the `Languages` search field, enter the package name you want to add to your system image. For example: "us". Click btn:[Next].

. Mandatory: On the `Others` page, complete the following steps:
.. In the `Hostname` field, enter the hostname you want to add to your system image. If you do not add a hostname, the operating system determines the hostname. 
.. Mandatory: In the `Installation Devices` field, enter a valid node for your system image to enable an unattended installation to your device. For example: `dev/sda1`. Click btn:[Next].

. Optional:  On the `FIDO device onboarding` page, complete the following steps:
.. On the `Manufacturing server URL` field, enter the `manufacturing server URL` to perform the initial device credential exchange, for example: "http://10.0.0.2:8080". The FDO customization in the blueprints is optional, and you can build your RHEL for Edge Simplified Installer image with no errors.

.. On the `DIUN public key insecure` field, enter the certification public key hash to perform the initial device credential exchange. This field accepts "true" as value, which means this is an insecure connection to the manufacturing server. For example: `manufacturing_server_url="http://${FDO_SERVER}:8080"
diun_pub_key_insecure="true"`. You must use only one of these three options: "key insecure", "key hash" and "key root certs".

.. On the `DIUN public key hash` field, enter the hashed version of your public key. For example: `17BD05952222C421D6F1BB1256E0C925310CED4CE1C4FFD6E5CB968F4B73BF73`. You can get the key hash by generating it based on the certificate of the manufacturing server. To generate the key hash, run the command:
+
[subs="quotes,attributes"]
----
# openssl x509 -fingerprint -sha256 -noout -in /etc/fdo/aio/keys/diun_cert.pem | cut -d"=" -f2 | sed 's/://g'
----  
+
The `/etc/fdo/aio/keys/diun_cert.pem` is the certificate that is stored in the manufacturing server.

.. On the `DIUN public key root certs` field, enter the public key root certs. This field accepts the content of the certification file that is stored in the manufacturing server. To get the content of certificate file, run the command:
+
[subs="quotes,attributes"]
----
$ cat /etc/fdo/aio/keys/diun_cert.pem.
----  

. Click btn:[Next].

. On the `Review` page, review the details about the blueprint. Click btn:[Create].

The RHEL image builder view opens, listing existing blueprints.
