:_mod-docs-content-type: PROCEDURE

[id="deploying-the-simplified-iso-image-from-a-usb-flash-drive_{context}"]
= Deploying the Simplified ISO image from a USB flash drive

[role="_abstract"]
Deploy the RHEL for Edge ISO image you generated  by creating a RHEL for Edge Simplified image by using an *USB installation*.

This example shows how to create a *USB installation* source from your ISO image.

.Prerequisites

* You have created a simplified installer image, which is an ISO image.
* You have a 8 GB USB flash drive. 

.Procedure

. Copy the ISO image file to a USB flash drive.

. Connect the USB flash drive to the port of the computer you want to boot.

. Boot the ISO image from the USB flash drive.The boot menu shows you the following options: 
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
....
Install Red Hat Enterprise Linux {ProductNumber}
Test this media & install Red Hat Enterprise Linux {ProductNumber}
....

. Choose Install Red Hat Enterprise Linux {ProductNumber}. This starts the system installation.

[role="_additional-resources"]
.Additional resources
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/interactively_installing_rhel_from_installation_media/index#booting-the-installer-from-local-media_rhel-installer[Booting the installation]