:_mod-docs-content-type: REFERENCE
[id="edge-ostree-commands_{context}"]

= OSTree commands

[role="_abstract"]
The following table provides a few OSTree commands that you can use when installing or managing OSTree images.

.ostree commands
|===
| ostree pull | [command]`ostree pull-local --repo [path] src`

[command]`ostree pull-local <path> <rev> --repo=<repo-path>`

[command]`ostree pull <URL> <rev> --repo=<repo-path>`
| ostree summary | [command]`ostree summary -u --repo=<repo-path>`
| View refs | [command]`ostree refs --repo ~/Code/src/osbuild-iot/build/repo/ --list`
| View commits in repo | [command]`ostree log --repo=/home/gicmo/Code/src/osbuild-iot/build/repo/ <REV>`
| Inspect a commit | [command]`ostree show --repo build/repo <REV>`
| List remotes of a repo | [command]`ostree remote list --repo <repo-path>`
| Resolve a REV | [command]`ostree rev-parse --repo ~/Code/src/osbuild-iot/build/repo fedora/x86_64/osbuild-demo`

[command]`ostree rev-parse --repo ~/Code/src/osbuild-iot/build/repo b3a008eceeddd0cfd`
| Create static-delta | [command]`ostree static-delta generate --repo=[path] --from=REV --to=REV`
| Sign an `existing` ostree commit with a GPG key | [command]`ostree gpg-sign --repo=<repo-path> --gpg-homedir <gpg_home> COMMIT KEY-ID…`
|===
