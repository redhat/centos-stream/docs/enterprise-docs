:_newdoc-version: 2.15.0
:_template-generated: 2024-3-1
:_mod-docs-content-type: PROCEDURE

[id="performing-an-unattended-installation-to-an-edge-device-by-using-kickstart_{context}"]
= Performing an unattended installation to an edge device by using Kickstart

[role="_abstract"]
For an unattended installation in a network-based environment, you can install the RHEL for Edge image to an Edge device by using a Kickstart file and a web server. The web server serves the RHEL for Edge Commit and the Kickstart file, and both artifacts are used to start the link:https://developers.redhat.com/products/rhel/download[RHEL Installer ISO] image.

.Prerequisites

* You have the `qemu-img` utility installed on your host system.
* You have created a `.qcow2` disk image to install the commit you created. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html-single/composing_a_customized_rhel_system_image/index#creating-a-system-image-with-composer-in-the-command-line-interface_creating-system-images-with-composer-command-line-interface[Creating a system image with RHEL image builder in the CLI].
* You have a running web server. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/composing_installing_and_managing_rhel_for_edge_images/assembly_deploying-a-non-network-rhel-for-edge-image_composing-installing-managing-rhel-for-edge-images#building-a-rhel-for-edge-container-commit-for-non-network-based-deployments_assembly_deploying-a-non-network-rhel-for-edge-image[Creating a RHEL for Edge Container image for non-network-based deployments].

.Procedure

. Run a RHEL for Edge Container image to start a web server. The server fetches the commit in the RHEL for Edge Container image and becomes available and running.

. Run the RHEL Anaconda Installer, passing the customized `.qcow2` disk image, by using `libvirt virt-install`:
+
[subs="+quotes,attributes"]
----
virt-install \
--name rhel-edge-test-1 \
--memory 2048 \
--vcpus 2 \
--disk path=prepared_disk_image.qcow2,format=qcow2,size=8 \
--os-variant rhel{ProductNumber} \
--cdrom /home/username/Downloads/rhel-{ProductNumber}-x86_64-boot.iso
----

. On the installation screen: 
+
.Red Hat Enterprise Linux boot menu
image::rhel-8.6-boot.png[The Red Hat Enterprise Linux boot menu]
+
.. Press the btn:[TAB] key and add the Kickstart kernel argument:
+
[subs="+quotes,attributes"]
----
inst.ks=http://web-server_device_ip:port/kickstart.ks
----
+
The kernel parameter specifies that you want to install RHEL by using the Kickstart file and not the RHEL image contained in the RHEL Installer.

.. After adding the kernel parameters, press kbd:[Ctrl]+kbd:[X] to boot the RHEL installation by using the Kickstart file.
+
The RHEL Installer starts, fetches the Kickstart file from the server (HTTP) endpoint, and executes the commands, including the command to install the RHEL for Edge image commit from the HTTP endpoint.
After the installation completes, the RHEL Installer prompts you for login details.

.Verification

. On the Login screen, enter your user account credentials and click btn:[Enter].

. Verify whether the RHEL for Edge image is successfully installed.
+
[subs="+quotes,attributes"]
----
$ rpm-ostree status
----
+
The command output provides the image commit ID and shows that the installation is successful.
+
The following is a sample output:
+
[subs="+quotes,attributes"]
----
State: idle
Deployments:
* ostree://edge:rhel/{ProductNumber}/x86_64/edge
		  Timestamp: 2020-09-18T20:06:54Z
			Commit: 836e637095554e0b634a0a48ea05c75280519dd6576a392635e6fa7d4d5e96
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/60959[How to embed a Kickstart file into an ISO image] (Red Hat Knowledgebase)
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html-single/interactively_installing_rhel_from_installation_media/index#booting-the-installer-from-local-media_rhel-installer[Booting the installation]
