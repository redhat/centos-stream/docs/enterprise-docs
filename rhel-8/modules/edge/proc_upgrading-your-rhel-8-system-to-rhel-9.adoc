:_mod-docs-content-type: PROCEDURE

[id="proc_upgrading-your-rhel-8-system-to-rhel-9_{context}"]
= Upgrading your RHEL 8 system to RHEL 9

You can upgrade your RHEL 8 system to RHEL 9 by using the `rpm-ostree rebase` command. The command fully supports the default package set of RHEL for Edge upgrades from the most recent updates of RHEL 8 to the most recent updates of RHEL 9. The upgrade downloads and installs the RHEL 9 image in the background. After the upgrade finishes, you must reboot your system to use the new RHEL 9 image.

WARNING: The upgrade does not support every possible `rpm` package version and inclusions. You must test your package additions to ensure that these packages works as expected.

.Prerequisites

* You have a running RHEL for Edge 8 system
* You have an  OSTree repository server by HTTP
* You created a blueprint for RHEL for Edge 9 image that you will upgrade

.Procedure

. On the system where RHEL image builder runs, create a RHEL for Edge 9 image:

.. Start the image compose:
+
[subs="quotes,attributes"]
----
$ *sudo composer-cli compose start _blueprint-name_ _edge-commit_*
----
+
Optionally, you can also create the new RHEL for Edge 9 image by using a pre-existing OSTree repository, by using the following command:
+
[subs="quotes,attributes"]
----
$ *sudo composer-cli compose start-ostree --ref _rhel/8/x86_64/edge_ --parent _parent-OSTree-REF_ --url _URL_ _blueprint-name_ _edge-commit_*
----

.. After the compose finishes, download the image.

.. Extract the downloaded image to `/var/www/html/` folder:
+
[subs="quotes,attributes"]
----
$ *sudo tar -xf _image_file_ -C /var/www/html*
----

.. Start the `httpd` service:
+
[subs="quotes,attributes"]
----
$ *systemctl start httpd.service*
----

. On the RHEL for Edge device, check the current remote repository configuration:
+
[subs="quotes,attributes"]
----
$ *sudo cat /etc/ostree/remotes.d/edge.conf*
----
+
NOTE: Depending on how your Kickstart file is configured, the `/etc/ostree/remotes.d` repository can be empty. If you configured your remote repository, you can see its configuration. For the
`edge-installer`, `raw-image`, and `simplified-installer` images, the remote is configured by default.

. Check the current URL repository:
+
[subs="quotes,attributes"]
----
$ *sudo ostree remote show-url [replaceable]__edge__*
----
+
_edge_ is the of the Ostree repository.

. List the remote reference branches:
+
[subs="quotes,attributes"]
----
$ *ostree remote refs [replaceable]__edge__*
----
+
You can see the following output:
+
[subs="quotes,attributes"]
----
Error: Remote refs not available; server has no summary file
----

. To add the new repository:
.. Configure the URL key to add a remote repository. For example:
+
[subs="quotes,attributes"]
----
$ *sudo ostree remote add \ 
--no-gpg-verify rhel9 http://192.168.122.1/repo/*
----

.. Configure the URL key to point to the RHEL 9 commit for the upgrade. For example:
+
[subs="quotes,attributes"]
----
$ *sudo cat /etc/ostree/remotes.d/_edge_.conf*

[remote "edge"]
url=http://192.168.122.1/ostree/repo/
gpg-verify=false
----

.. Confirm if the URL has been set to the new remote repository:
+
[subs="quotes,attributes"]
----
$ *sudo cat /etc/ostree/remotes.d/_rhel9_.conf* 

[remote "edge"]
url=http://192.168.122.1/repo/
gpg-verify=false
----

.. See the new URL repository:
+
[subs="quotes,attributes"]
----
$ *sudo ostree remote show-url rhel9 http://192.168.122.1/ostree-rhel9/repo/*
----

.. List the current remote list options:
+
[subs="quotes,attributes"]
----
$ *sudo ostree remote list*

output:
edge
rhel9
----

. Rebase your system to the RHEL version, providing the reference path for the RHEL 9 version:
+
[subs="quotes,attributes"]
----
$ *rpm-ostree rebase rhel9:rhel/9/x86_64/edge*
----

. Reboot your system. 
+
[subs="quotes,attributes"]
----
$ *systemctl reboot*
----

. Enter your username and password.

. Check the current system status:
+
[subs="quotes,attributes"]
----
$ *rpm-ostree status*
----

.Verification

. Check the current status of the currently running deployment:
+
[subs="quotes,attributes"]
----
$ *rpm-ostree status*
----

. Optional: List the processor and tasks managed by the kernel in real-time.
+
[subs="quotes,attributes"]
----
$ *top*
----

. If the upgrade does not support your requirements, you have the option to manually rollback to the previous stable deployment RHEL 8 version:
+
[subs="quotes,attributes"]
----
$ *sudo rpm-ostree rollback*
----

. Reboot your system. Enter your username and password:
+
[subs="quotes,attributes"]
----
$ *systemctl reboot*
----
+
After rebooting, your system runs RHEL 9 successfully. 
+
NOTE: If your upgrade succeeds and you do not want to use the previous deployment RHEL 8 version, you can delete the old repository:
+
[subs="quotes,attributes"]
----
$ *sudo ostree remote delete [replaceable]__edge__*
----

//https://docs.fedoraproject.org/en-US/iot/rebasing/
[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/7033165[rpm-ostree update and rebase fails with failed to find kernel error] (Red Hat Knowledgebase)

