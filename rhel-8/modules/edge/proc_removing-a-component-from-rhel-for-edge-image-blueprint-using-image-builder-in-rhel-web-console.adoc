:_mod-docs-content-type: PROCEDURE

:experimental:

[id="removing-a-component-from-rhel-for-edge-image-blueprint-using-image-builder-in-rhel-web-console_{context}"]
= Removing a component from a blueprint using RHEL image builder in the web console

[role="_abstract"]
To remove one or more unwanted components from a blueprint that you created by using RHEL image builder, ensure that you have met the following prerequisites and then follow the procedure.

.Prerequisites

* On a RHEL system, you have accessed the RHEL image builder dashboard.
* You have created a blueprint for RHEL for Edge image. 
* You have added at least one component to the RHEL for Edge blueprint.

.Procedure

. On the RHEL image builder dashboard, click the blueprint that you want to edit.
+
To search for a specific blueprint, enter the blueprint name in the filter text box, and click btn:[Enter].

. On the upper right side of the blueprint, click btn:[Edit Packages].
+
The  *Edit blueprints* wizard opens.

. On the *Details* page, update the blueprint name and click btn:[Next].

. On the *Packages* page, follow the steps:
.. From the *Chosen packages*, click btn:[&lt;] to remove the chosen component. You can also click btn:[&lt;&lt;] to remove all the packages at once.

. On the *Review* page, click btn:[Save].
+ 
The blueprint is now updated.
