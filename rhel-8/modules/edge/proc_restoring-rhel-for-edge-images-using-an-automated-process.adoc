:_mod-docs-content-type: PROCEDURE

[id="restoring-rhel-for-edge-images-using-an-automated-process_{context}"]

= Rolling back RHEL for Edge images using an automated process

[role="_abstract"]
`Greenboot` checks provides a framework that is integrated into the boot process and can trigger `rpm-ostree` rollbacks when a health check fails. For the health checks, you can create a custom script that indicates whether a health check passed or failed. Based on the result, you can decide when a rollback should be triggered. The following procedure shows how to create an health check script example:

.Procedure

. Create a script that returns a standard exit code `0`.
+
For example, the following script ensures that the configured DNS server is available:
+
[subs="quotes,attributes"]
====
----
#!/bin/bash

DNS_SERVER=$(grep ^nameserver /etc/resolv.conf | head -n 1 | cut -f2 -d" ")
COUNT=0
# check DNS server is available
ping -c1 $DNS_SERVER
while [ $? != '0' ] && [ $COUNT -lt 10 ]; do
((COUNT++))
echo "Checking for DNS: Attempt $COUNT ."
sleep 10
ping -c 1 $DNS_SERVER
done
----
====

. Include an executable file for the health checks at `/etc/greenboot/check/required.d/`.
+
[subs="quotes,attributes"]
----
chmod +x check-dns.sh
----
+
During the next reboot, the script is executed as part of the boot process, before the system enters the `boot-complete.target` unit. If the health checks are successful, no action is taken. If the health checks fail,  the system will reboot several times, before marking the update as failed and rolling back to the previous update.

.Verification

To check if the default gateway is reachable, run the following health check script: 

. Create a script that returns a standard exit code `0`.
+
[subs="quotes,attributes"]
====
----
#!/bin/bash

DEF_GW=$(ip r | awk '/^default/ {print $3}')
SCRIPT=$(basename $0)

count=10
connected=0
ping_timeout=5
interval=5

while [ $count -gt 0 -a $connected -eq 0 ]; do
  echo "$SCRIPT: Pinging default gateway $DEF_GW"
  ping -c 1 -q -W $ping_timeout $DEF_GW > /dev/null 2>&1 && connected=1 || sleep $interval
  ((--count))
done

if [ $connected -eq 1 ]; then
  echo "$SCRIPT: Default gateway $DEF_GW is reachable."
  exit 0
else
  echo "$SCRIPT: Failed to ping default gateway $DEF_GW!" 1>&2
  exit 1
fi
----
====

. Include an executable file for the health checks at `/etc/greenboot/check/required.d/` directory.
+
[subs="quotes,attributes"]
====
----
chmod +x check-gw.sh
----
====
