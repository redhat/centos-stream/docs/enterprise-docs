:_mod-docs-content-type: CONCEPT

[id="con_network-based-deployments_{context}"]
= Network-based deployments

[role="_abstract"]
Use RHEL image builder to create flexible RHEL `rpm-ostree` images to suit your requirements, and then use Anaconda to deploy them in your environment.
RHEL image builder automatically identifies the details of your deployment setup and generates the image output as an `edge-commit` as a `.tar` file.

You can access RHEL image builder through a command-line interface in the `composer-cli` tool, or use a graphical user interface in the RHEL web console.

You can compose and deploy the RHEL for Edge image by performing the following high-level steps:

*For an attended installation*

. Install and register a RHEL system
. Install RHEL image builder
. Using RHEL image builder, create a blueprint for RHEL for Edge image
. Import the RHEL for Edge blueprint in RHEL image builder
. Create a RHEL for Edge Commit (`.tar`) image
. Download the RHEL for Edge image file
. On the same system where you have installed RHEL image builder, install a web server that you want to serve the RHEL for Edge Commit content. For instructions, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/deploying_web_servers_and_reverse_proxies/setting-up-and-configuring-nginx_deploying-web-servers-and-reverse-proxies[Setting up and configuring NGINX]
. Extract the RHEL for Edge Commit (`.tar`) content to the running web server
. Create a Kickstart file that pulls the OSTree content from the running web server. For details on how to modify the Kickstart to pull the OSTree content, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/composing_installing_and_managing_rhel_for_edge_images/installing-rpm-ostree-images_composing-installing-managing-rhel-for-edge-images#extracting-the-tar-file-commit_installing-rpm-ostree-images[Extracting the RHEL for Edge image commit]
. Boot the RHEL installer ISO on the edge device and provide the Kickstart to it.

For an unattended installation, you can customize the RHEL installation ISO and embed the Kickstart file to it.

//The following diagram represents the RHEL for Edge network image deployment workflow:
//[id="network-based_{context}"]
//.Deploying RHEL for Edge in network-base environment
//image::edge-deployment-workflow.png[RHEL for Edge network deployment workflow]
