:_mod-docs-content-type: PROCEDURE

[id="setting-up-a-web-server-to-install-rhel-for-edge-image_{context}"]

= Setting up a web server to install RHEL for Edge images

[role="_abstract"]
After you have extracted the RHEL for Edge image contents, set up a web server to provide the image commit details to the RHEL installer by using HTTP.

The following example provides the steps to set up a web server by using a container.

.Prerequisites

* You have installed Podman on your system. See the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/3650231[How do I install Podman in RHEL].

.Procedure

. Create the `nginx` configuration file with the following instructions:
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
....
events {

}

http {
    server{              
        listen 8080;
        root /usr/share/nginx/html;
                }
         }

pid /run/nginx.pid;
daemon off;
....

. Create a Dockerfile with the following instructions:
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
....
FROM registry.access.redhat.com/ubi8/ubi
RUN {PackageManagerCommand} -y install nginx && {PackageManagerCommand} clean all
COPY kickstart.ks /usr/share/nginx/html/
COPY repo /usr/share/nginx/html/
COPY nginx /etc/nginx.conf
EXPOSE 8080
CMD ["/usr/sbin/nginx", "-c", "/etc/nginx.conf"]
ARG commit
ADD ${commit} /usr/share/nginx/html/
....
+
Where,
+
* `_kickstart.ks_` is the name of the Kickstart file from the RHEL for Edge image. The Kickstart file includes directive information. To help you manage the images later, it is advisable to include the checks and settings for greenboot checks. For that, you can update the Kickstart file to include the following settings:
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
....
lang en_US.UTF-8
keyboard us
timezone Etc/UTC --isUtc
text
zerombr
clearpart --all --initlabel
autopart
reboot
user --name=core --group=wheel
sshkey --username=core "ssh-rsa AAAA3Nza...."

ostreesetup --nogpg --osname=rhel --remote=edge
--url=https://mirror.example.com/repo/
--ref=rhel/{ProductNumber}/x86_64/edge

%post
cat << EOF > /etc/greenboot/check/required.d/check-dns.sh
#!/bin/bash

DNS_SERVER=$(grep nameserver /etc/resolv.conf | cut -f2 -d" ")
COUNT=0

# check DNS server is available
ping -c1 $DNS_SERVER
while [ $? != '0' ] && [ $COUNT -lt 10 ]; do
((COUNT++))
echo "Checking for DNS: Attempt $COUNT ."
sleep 10
ping -c 1 $DNS_SERVER
done
EOF
%end
....
+
Any HTTP service can host the OSTree repository, and the example, which uses a container, is just an option for how to do this. The Dockerfile performs the following tasks:
+
.. Uses the latest Universal Base Image (UBI)
.. Installs the web server (nginx)
.. Adds the Kickstart file to the server
.. Adds the RHEL for Edge image commit to the server


. Build a Docker container
+
[subs="quotes,attributes"]
----
# podman build -t _name-of-container-image_ --build-arg commit=_uuid_-commit.tar .
----

. Run the container
+
[subs="quotes,attributes"]
----
# podman run --rm -d -p _port_:8080 localhost/_name-of-container-image_
----
+
As a result, the server is set up and ready to start the RHEL Installer by using the `commit.tar` repository and the Kickstart file.
