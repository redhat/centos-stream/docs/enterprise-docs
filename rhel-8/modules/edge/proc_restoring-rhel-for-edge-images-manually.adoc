:_mod-docs-content-type: PROCEDURE

[id="restoring-rhel-for-edge-images-manually_{context}"]
= Manually rolling back RHEL for Edge images

[role="_abstract"]
When you upgrade your operating system, a new deployment is created, and the `rpm-ostree` package also keeps the previous deployment. If there are issues on the updated version of the operating system, you can manually roll back to the previous deployment with a single `rpm-ostree` command, or by selecting the previous deployment in the GRUB boot loader. 

To manually roll back to a previous version, perform the following steps.

.Prerequisite

. You updated your system and it failed.

.Procedure

. Optional: Check for the fail error message:
+
[subs="quotes,attributes"]
----
$ journalctl -u greenboot-healthcheck.service.
----
. Run the `rollback` command:
+
[subs="quotes,attributes"]
----
# rpm-ostree rollback
----
The command output provides details about the commit ID that is being moved and indicates a completed transaction with the details of the package being removed.

. Reboot the system.
+
[subs="quotes,attributes"]
----
# systemctl reboot 
----
+
The command activates the previous commit with the stable content. The changes are applied and the previous version is restored.
