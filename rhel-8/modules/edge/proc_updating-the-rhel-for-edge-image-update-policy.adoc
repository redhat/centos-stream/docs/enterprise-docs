:_mod-docs-content-type: PROCEDURE

[id="updating-the-rhel-for-edge-image-update-policy_{context}"]

= Updating the RHEL for Edge image update policy

[role="_abstract"]
To update the image update policy, use the `AutomaticUpdatePolicy` and an `IdleExitTimeout` setting from the `rpm-ostreed.conf` file at `/etc/rpm-ostreed.conf` location on an Edge device.

The `AutomaticUpdatePolicy` settings controls the automatic update policy and has the following update checks options:

* `none`: Disables automatic updates. By default, the `AutomaticUpdatePolicy` setting is set to `none`.
* `check`: Downloads enough metadata to display available updates with `rpm-ostree` status.
* `stage`: Downloads and unpacks the updates that are applied on a reboot.

The `IdleExitTimeout` setting controls the time in seconds of inactivity before the daemon exit and has the following options:

* 0: Disables auto-exit.
* 60: By default, the `IdleExitTimeout` setting is set to `60`.

To enable automatic updates, perform the following steps:

.Procedure

. In the `/etc/rpm-ostreed.conf` file, update the following:
+

* Change the value of `AutomaticUpdatePolicy` to `check`.
* To run the update checks, specify a value in seconds for `IdleExitTimeout`.


. Reload the `rpm-ostreed` service and enable the `systemd` timer.
+
[subs="quotes,attributes"]
----
# systemctl reload rpm-ostreed
# systemctl enable rpm-ostreed-automatic.timer --now
----

. Verify the `rpm-ostree` status to ensure the automatic update policy is configured and time is active.
+
[subs="quotes,attributes"]
----
# rpm-ostree status
----
+
The command output shows the following:
+
[subs="quotes,attributes"]
----
State: idle; auto updates enabled (check; last run &lt;minutes&gt; ago)
----
+
Additionally, the output also displays information about the available updates.


