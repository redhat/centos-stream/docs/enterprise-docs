:_mod-docs-content-type: REFERENCE
[id="pre-install-script-section-options_{context}"]
= %pre-install script section options

[role="_abstract"]
The following options can be used to change the behavior of `pre-install` scripts.
To use an option, append it to the `%pre-install` line at the beginning of the script. For example:

----
%pre-install --interpreter=/usr/libexec/platform-python
-- Python script omitted --
%end
----

You can have multiple `%pre-install` sections, with same or different interpreters. They are evaluated in their order of appearance in the Kickstart file.


[option]`--interpreter=`::
Allows you to specify a different scripting language, such as Python. Any scripting language available on the system can be used; in most cases, these are `/usr/bin/sh`, `/usr/bin/bash`, and `/usr/libexec/platform-python`.
+
The `platform-python` interpreter uses Python version 3.6. You must change your Python scripts from previous RHEL versions for the new path and version. Additionally, `platform-python` is meant for system tools: Use the `python36` package outside the installation environment. For more details about Python in {productname}, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/installing-and-using-dynamic-programming-languages_configuring-basic-system-settings#assembly_introduction-to-python_installing-and-using-dynamic-programming-languages[Introduction to Python] in _Configuring basic system settings_.
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/installing_and_using_dynamic_programming_languages/assembly_introduction-to-python_installing-and-using-dynamic-programming-languages[Introduction to Python] in _Installing and using dynamic programming languages_.
endif::[]


[option]`--erroronfail`::
Displays an error and halts the installation if the script fails. The error message will direct you to where the cause of the failure is logged. The installed system might get into an unstable and unbootable state. You can use the `inst.nokill` option to debug the script.

[option]`--log=`::
Logs the script's output into the specified log file. For example:
+
----
%pre-install --log=/mnt/sysroot/root/ks-pre.log
----
