:_mod-docs-content-type: PROCEDURE
[id="making-a-kickstart-file-available-on-a-local-volume_{context}"]
= Sharing the installation files on a local volume

[role="_abstract"]
This procedure describes how to store the Kickstart script file on a volume on the system to be installed. This method enables you to bypass the need for another system.


.Prerequisites

* You have a drive that can be moved to the machine to be installed, such as a USB stick.
* The drive contains a partition that can be read by the installation program. The supported types are `ext2`, `ext3`, `ext4`, `xfs`, and `fat`.
* The drive is connected to the system and its volumes are mounted.


.Procedure

. List volume information and note the UUID of the volume to which you want to copy the Kickstart file.
+
----
# lsblk -l -p -o name,rm,ro,hotplug,size,type,mountpoint,uuid
----

. Navigate to the file system on the volume.

. Copy the Kickstart file to this file system.

. Make a note of the string to use later with the [option]`inst.ks=` option. This string is in the form `hd:UUID=__volume-UUID__:__path/to/kickstart-file.cfg__`. Note that the path is relative to the file system root, not to the `/` root of file system hierarchy. Replace __volume-UUID__ with the UUID you noted earlier.

. Unmount all drive volumes:
+
[subs="quotes,attributes"]
----
# umount _/dev/xyz_ ...
----
+
Add all the volumes to the command, separated by spaces.
