:_newdoc-version: 2.18.3
:_template-generated: 2024-11-25

:_mod-docs-content-type: CONCEPT

[id="boot-media-compatibility-for-ibm-z-servers_{context}"]
= Boot media compatibility for IBM Z servers

The following table provides detailed information about the supported boot media options for installing Red Hat Enterprise Linux (RHEL) on 64-bit IBM Z servers. It outlines the compatibility of each boot medium with different system types and indicates whether the `zipl` boot loader is used. This information helps you determine the most suitable boot medium for your specific environment.

[cols="30,15,15,15,15", options="header"]
|===
| System type / Boot media | Uses zipl boot loader | z/VM | KVM | LPAR

| z/VM Reader            | No                   | Yes  | N/A | N/A  
| SE or HMC (remote SFTP, FTPS, FTP server, DVD) | No  | N/A  | N/A  | Yes  
| DASD                   | Yes                  | Yes  | Yes  | Yes  
| FCP SCSI LUNs          | Yes                  | Yes  | Yes | Yes  
| FCP SCSI DVD           | Yes                  | Yes  | Yes | Yes  
|===
N/A indicates that the boot medium is not applicable for the specified system type.