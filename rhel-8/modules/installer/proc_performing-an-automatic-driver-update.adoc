:_mod-docs-content-type: PROCEDURE
[id="performing-an-automatic-driver-update_{context}"]
= Performing an automatic driver update

[role="_abstract"]
This procedure describes how to perform an automatic driver update during installation.

.Prerequisites

* You have placed the driver update image on a standard disk partition with an `OEMDRV` label or burnt the `OEMDRV` driver update image to a CD or DVD. Advanced storage, such as RAID or LVM volumes, may not be accessible during the driver update process.
* You have connected a block device with an `OEMDRV` volume label to your system, or inserted the prepared CD or DVD into your system's CD/DVD drive before starting the installation process.

.Procedure

* When you complete the prerequisite steps, the drivers load automatically when the installation program starts and installs during the system's installation process.
