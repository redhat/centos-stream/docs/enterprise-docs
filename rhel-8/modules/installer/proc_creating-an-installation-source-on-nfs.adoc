:_mod-docs-content-type: PROCEDURE
[id="creating-an-installation-source_{context}"]
= Creating an installation source on an NFS server

[role="_abstract"]
You can use this installation method to install multiple systems from a single source, without having to connect to physical media.

.Prerequisites

* You have an administrator-level access to a server with {RHEL} {ProductNumber}, and this server is on the same network as the system to be installed.
* You have downloaded the full installation DVD ISO from the link:https://access.redhat.com/downloads/content/rhel[Product Downloads] page.
* You have created a bootable CD, DVD, or USB device from the image file. 
ifdef::interactive-install-media[]
For more information, see xref:assembly_creating-a-bootable-installation-medium_rhel-installer[Creating a bootable installation medium for RHEL].
endif::[]
* You have verified that your firewall allows the system you are installing to access the remote installation source. For more information, see xref:ports-for-network-based-installation_preparing-network-based-repositories[Ports for network-based installation]. 

[IMPORTANT]
====
Ensure that you use different paths in `inst.ks` and `inst.repo`. When using NFS to host the installation source, you cannot use the same nfs share to host the Kickstart.
====

.Procedure

. Install the [package]`nfs-utils` package:
+
[subs="quotes, macros, attributes"]
----
# {PackageManagerCommand} install nfs-utils
----

. Copy the DVD ISO image to a directory on the NFS server.

. Open the [filename]`/etc/exports` file using a text editor and add a line with the following syntax:
+
[subs="quotes, macros, attributes"]
----
/__exported_directory__/ __clients__
----

* Replace _/exported_directory/_ with the full path to the directory with the ISO image.
* Replace __clients__ with one of the following:
+
--
** The host name or IP address of the target system
** The subnetwork that all target systems can use to access the ISO image
** To allow any system with network access to the NFS server to use the ISO image, the asterisk sign (`*`)
--
+
See the `exports(5)` man page for detailed information about the format of this field.
+
For example, a basic configuration that makes the `/rhel{ProductNumber}-install/` directory available as read-only to all clients is:
+
[subs="quotes, macros, attributes"]
----
/rhel{ProductNumber}-install *
----
. Save the [filename]`/etc/exports` file and exit the text editor.
. Start the nfs service:
+
[subs="quotes, macros, attributes"]
----
# systemctl start nfs-server.service
----
+
If the service was running before you changed the [filename]`/etc/exports` file, reload the NFS server configuration:
+
[subs="quotes, macros, attributes"]
----
# systemctl reload nfs-server.service
----
+
The ISO image is now accessible over NFS and ready to be used as an installation source.
+ 
When configuring the installation source, use `nfs:` as the protocol, the server host name or IP address, the colon sign `(:)`, and the directory holding the ISO image. For example, if the server host name is `myserver.example.com` and you have saved the ISO image in `/rhel{ProductNumber}-install/`, specify `nfs:myserver.example.com:/rhel{ProductNumber}-install/` as the installation source.

