:_mod-docs-content-type: PROCEDURE

:experimental:

[id="configuring-lvm-logical-volume_{context}"]
= Configuring an LVM logical volume

[role="_abstract"]
You can configure a newly-created LVM logical volume based on your requirements.

[WARNING]
====
Placing the `/boot` partition on an LVM volume is not supported.
====

.Procedure

. From the *Manual Partitioning* window, create a mount point by using any of the following options:
** Use the *Click here to create them automatically* option or click the *+* button. 
** Select Mount Point from the drop-down list or enter manually.
** Enter the size of the file system in to the *Desired Capacity* field; for example, 70 GiB for `/`, 1 GiB for `/boot`.
+
Note: Skip this step to use the existing mount point.
. Select the mount point.

. Click the *Device Type* drop-down menu and select `LVM`. The *Volume Group* drop-down menu is displayed with the newly-created volume group name.

. Click btn:[Modify] to configure the newly-created volume group. The *Configure Volume Group* dialog box opens.
+
[NOTE]
====
You cannot specify the size of the volume group's physical extents in the configuration dialog. The size is always set to the default value of 4 MiB. If you want to create a volume group with different physical extents, you must create it manually by switching to an interactive shell and using the [command]`vgcreate` command, or use a Kickstart file with the [command]`volgroup --pesize=pass:attributes[{blank}]_size_pass:attributes[{blank}]` command. For more information, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html-single/automatically_installing_rhel/index[Automatically installing RHEL] document. 
====


. Optional: From the *RAID Level* drop-down menu, select the RAID level that you require.
+
The available RAID levels are the same as with actual RAID devices.

. Select the *Encrypt* check box to mark the volume group for encryption.

. From the *Size policy* drop-down menu, select any of the following size policies for the volume group:
+
The available policy options are:
+
====
Automatic:: The size of the volume group is set automatically so that it is large enough to contain the configured logical volumes. This is optimal if you do not need free space within the volume group.

As large as possible:: The volume group is created with maximum size, regardless of the size of the configured logical volumes it contains. This is optimal if you plan to keep most of your data on LVM and later need to increase the size of some existing logical volumes, or if you need to create additional logical volumes within this group.

Fixed:: You can set an exact size of the volume group. Any configured logical volumes must then fit within this fixed size. This is useful if you know exactly how large you need the volume group to be.
====

. Click btn:[Save] to apply the settings and return to the *Manual Partitioning* window.

. Click btn:[Update Settings] to save your changes.

. Click btn:[Done] to return to the *Installation Summary* window.