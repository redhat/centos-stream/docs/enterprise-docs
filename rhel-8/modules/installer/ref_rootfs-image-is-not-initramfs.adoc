////
Base the file name and the ID on the module title. For example:
* file name: ref-my-reference-a.adoc
* ID: [id="ref-my-reference-a_{context}"]
* Title: = My reference A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix ref- or ref_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: REFERENCE

[id="ref_rootfs-image-is-not-initramfs_{context}"]
= Rootfs image is not initramfs

[role="_abstract"]
If you get the following message on the console during booting the installer, the transfer of the installer `initrd.img` might have had errors:

[literal,subs="+quotes,attributes,verbatim"]
....
[ ...] rootfs image is not initramfs
....

To resolve this issue, download `initrd` again or run the `sha256sum` with `initrd.img` and compare it with the checksum stored in the `.treeinfo` file on the installation medium, for example,

[literal,subs="+quotes,attributes,verbatim"]
....
$ sha256sum dvd/images/pxeboot/initrd.img
fdb1a70321c06e25a1ed6bf3d8779371b768d5972078eb72b2c78c925067b5d8 dvd/images/pxeboot/initrd.img
....

To view the checksum in `.treeinfo`:

[literal,subs="+quotes,attributes,verbatim"]
....
$ grep sha256 dvd/.treeinfo
images/efiboot.img = sha256:__d357d5063b96226d643c41c9025529554a422acb43a4394e4ebcaa779cc7a917__
images/install.img = sha256:__8c0323572f7fc04e34dd81c97d008a2ddfc2cfc525aef8c31459e21bf3397514__
images/pxeboot/initrd.img = sha256:__fdb1a70321c06e25a1ed6bf3d8779371b768d5972078eb72b2c78c925067b5d8__
images/pxeboot/vmlinuz = sha256:__b9510ea4212220e85351cbb7f2ebc2b1b0804a6d40ccb93307c165e16d1095db__
....


Despite having correct `initrd.img`, if you get the following kernel messages during booting the installer, often a boot parameter is missing or mis-spelled, and the installer could not load `stage2`, typically referred to by the `inst.repo=` parameter, providing the full installer initial ramdisk for its in-memory root file system:

[literal,subs="+quotes,attributes,verbatim"]
....
[ ...] No filesystem could mount root, tried:
[ ...] Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(1,0)
[ ...] CPU: 0 PID: 1 Comm: swapper/0 Not tainted 5.14.0-55.el9.s390x #1
[ ...] ...
[ ...] Call Trace:
[ ...] ([<...>] show_trace+0x.../0x...)
[ ...]  [<...>] show_stack+0x.../0x...
[ ...]  [<...>] panic+0x.../0x...
[ ...]  [<...>] mount_block_root+0x.../0x...
[ ...]  [<...>] prepare_namespace+0x.../0x...
[ ...]  [<...>] kernel_init_freeable+0x.../0x...
[ ...]  [<...>] kernel_init+0x.../0x...
[ ...]  [<...>] kernel_thread_starter+0x.../0x...
[ ...]  [<...>] kernel_thread_starter+0x.../0x…
....

To resolve this issue, check

* if the installation source specified is correct on the kernel command line (`inst.repo=`) or in the kickstart file
* the network configuration is specified on the kernel command line (if the installation source is specified as network)
* the network installation source is accessible from another system
