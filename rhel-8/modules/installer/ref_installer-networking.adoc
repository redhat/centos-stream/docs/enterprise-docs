:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_installer-networking.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
[id="installer-networking_{context}"]
= Installer networking

//== Device naming scheme

[role="_abstract"]
A new network device naming scheme that generates network interface names based on a user-defined prefix is available in {productname} 8.
The `net.ifnames.prefix` boot option allows the device naming scheme to be used by the installation program and the installed system.

[role="_additional-resources"]
.Additional resources
* For more information, see link:https://access.redhat.com/articles/4153841[RHEL-8 new custom NIC names helper] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_networking/consistent-network-interface-device-naming_configuring-and-managing-networking#proc_customizing-the-prefix-for-ethernet-interfaces-during-installation_consistent-network-interface-device-naming[Customizing the prefix for Ethernet interfaces during installation].
