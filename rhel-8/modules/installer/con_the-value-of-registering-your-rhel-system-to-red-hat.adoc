
:_mod-docs-content-type: CONCEPT

[id="the-value-of-registering-your-rhel-system-to-red-hat_{context}"]
= The value of registering your RHEL system to Red Hat

Registration establishes an authorized connection between your system and Red Hat. Red Hat issues the registered system, whether a physical or virtual machine, a certificate that identifies and authenticates the system so that it can receive protected content, software updates, security patches, support, and managed services from Red Hat.

With a valid subscription, you can register a {ProductName} ({ProductShortName}) system in the following ways:

* During the installation process, using an installer graphical user interface (GUI) or text user interface (TUI)
* After installation, using the command line (CLI)
* Automatically, during or after installation, using a kickstart script or an activation key.

The specific steps to register your system depend on the version of {ProductShortName} that you are using and the registration method that you choose.

Registering your system to Red Hat enables features and capabilities that you can use to manage your system and report data. For example, a registered system is authorized to access protected content repositories for subscribed products through the Red Hat Content Delivery Network (CDN) or a Red Hat Satellite Server. These content repositories contain Red Hat software packages and updates that are available only to customers with an active subscription. These packages and updates include security patches, bug fixes, and new features for {ProductShortName} and other Red Hat products.

[IMPORTANT]
The entitlement-based subscription model is deprecated and will be retired in the future. Simple content access is now the default subscription model. It provides an improved subscription experience that eliminates the need to attach a subscription to a system before you can access Red Hat subscription content on that system. If your Red Hat account uses the entitlement-based subscription model, contact your Red hat account team, for example, a technical account manager (TAM) or solution architect (SA) to prepare for migration to simple content access. For more information, see link:https://access.redhat.com/articles/transition_of_subscription_services_to_the_hybrid_cloud_console[Transition of subscription services to the hybrid cloud].