////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

:experimental:

[id="proc_editing-the-prompt_{context}"]
= Editing predefined boot options using the > prompt

On BIOS-based AMD64 and Intel 64 systems, you can use the `>` prompt to edit predefined boot options. 

.Prerequisites

* You have created bootable installation media (USB, CD or DVD).
* You have booted the installation from the media, and the installation boot menu is open.

.Procedure

. From the boot menu, select an option and press the kbd:[Tab] key on your keyboard. The `>` prompt is accessible and displays the available options.
. Optional: To view a full set of options, select `Test this media and install {ProductShortName} {ProductNumber}`.
. Append the options that you require to the `>` prompt.
+
For example, to enable the cryptographic module self-checks mandated by the Federal Information Processing Standard (FIPS) 140, add `fips=1`:
+
----
>vmlinuz initrd=initrd.img inst.stage2=hd:LABEL=RHEL-9-5-0-BaseOS-x86_64 rd.live.check quiet fips=1
----

. Press kbd:[Enter] to start the installation.

. Press kbd:[Esc] to cancel editing and return to the boot menu.
