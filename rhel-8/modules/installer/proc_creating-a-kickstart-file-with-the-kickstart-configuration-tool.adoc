:_mod-docs-content-type: PROCEDURE
[id="creating-a-kickstart-file-with-the-kickstart-configuration-tool_{context}"]
= Creating a Kickstart file with the Kickstart configuration tool

[role="_abstract"]
Users with a {rh} Customer Portal account can use the Kickstart Generator tool in the Customer Portal Labs to generate Kickstart files online. This tool will walk you through the basic configuration and enables you to download the resulting Kickstart file.

.Prerequisites

* You have a {rh} Customer Portal account and an active {rh} subscription.


.Procedure

. Open the Kickstart generator lab information page at https://access.redhat.com/labsinfo/kickstartconfig.

. Click the [GUI]*Go to Application* button to the left of heading and wait for the next page to load.

. Select [GUI]*{ProductName} {ProductNumber}* in the drop-down menu and wait for the page to update.

. Describe the system to be installed using the fields in the form.
+
You can use the links on the left side of the form to quickly navigate between sections of the form.

. To download the generated Kickstart file, click the red [GUI]*Download* button at the top of the page.
+
Your web browser saves the file.

. Install the [package]*pykickstart* package.
+
[subs="+attributes"]
----
# {PackageManagerCommand} install pykickstart
----

. Run [command]`ksvalidator` on your Kickstart file.
+
[subs="quotes,attributes"]
----
$ ksvalidator -v {ProductShortName}{ProductNumber} __/path/to/kickstart.ks__
----
+
Replace __/path/to/kickstart.ks__ with the path to the Kickstart file you want to verify.
+
The validation tool cannot guarantee the installation will be successful. It ensures only that the syntax is correct and that the file does not include deprecated options. It does not attempt to validate the `%pre`, `%post` and `%packages` sections of the Kickstart file.