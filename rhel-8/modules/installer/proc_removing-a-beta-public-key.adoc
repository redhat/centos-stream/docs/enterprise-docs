:_mod-docs-content-type: PROCEDURE
[id="removing-a-beta-public-key_{context}"]
= Removing a Beta public key

[role="_abstract"]
If you plan to remove the {productname} Beta release, and install a {productname} General Availability (GA) release, or a different operating system, then remove the Beta public key.

The procedure describes how to remove a Beta public key.

.Procedure

. Begin to remove the Red Hat Beta public key from the system's Machine Owner Key (MOK) list:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] mokutil --reset
....
+
. Enter a password when prompted.

. Reboot the system and press any key to continue the startup.
  The Shim UEFI key management utility starts during the system startup.

. Select *Reset MOK*.

. Select *Continue*.

. Select *Yes* and enter the password that you had specified in step 2.
  The key is removed from the system's firmware.

. Select *Reboot*.
