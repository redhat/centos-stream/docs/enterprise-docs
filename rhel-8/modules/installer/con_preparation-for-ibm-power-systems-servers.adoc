:_mod-docs-content-type: CONCEPT
[id="preparation-for-ibm-power-systems-servers_{context}"]
= Overview of installation process on IBM Power System LC servers

[role="_abstract"]
Use this information to install {RHEL}{NBSP}{ProductNumber} on a non-virtualized or bare metal IBM Power System LC server.

The installation workflow follows these general steps:

* Check the system requirements
* Download the required installation ISO image
* Create an installation boot medium
* Complete the prerequisites and boot the firmware
* Connect to the BMC firmware to set up network connection
* Connect to the BMC firmware with IPMI
* Choose the installation boot method:
** Booting the installation from a USB device
** Booting the installation using the Baseboard Management Controller
* Install Red Hat Enterprise Linux

[role="_additional-resources"]
.Additional resources
* link:https://www.ibm.com/support/knowledgecenter/linuxonibm/liaam/liaamdistros.htm[Supported Linux distributions and virtualization options for POWER8 and POWER9 Linux on Power systems]
