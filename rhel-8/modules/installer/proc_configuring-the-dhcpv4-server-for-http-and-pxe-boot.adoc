:_mod-docs-content-type: PROCEDURE

[id="configuring-the-dhcpv4-server-for-http-and-pxe-boot_{context}"]
= Configuring the DHCPv4 server for network boot

[role="_abstract"]
Enable the DHCP version 4 (DHCPv4) service on your server, so that it can provide network boot functionality.

.Prerequisites

* You are preparing network installation over the IPv4 protocol.
+
For IPv6, see xref:configuring-the-dhcpv6-server-for-http-and-pxe-boot_{context}[Configuring the DHCPv6 server for network boot] instead.

* Find the network addresses of the server.
+
In the following examples, the server has a network card with this configuration:
+
IPv4 address:: 192.168.124.2/24
IPv4 gateway:: 192.168.124.1

.Procedure

. Install the DHCP server:
+
[subs="+attributes"]
----
{PackageManagerCommand} install dhcp-server
----

. Set up a DHCPv4 server. Enter the following configuration in the `/etc/dhcp/dhcpd.conf` file. Replace the addresses to match your network card.
+
[subs="+quotes"]
----
option architecture-type code 93 = unsigned integer 16;
 
subnet __192.168.124.0__ netmask __255.255.255.0__ {
  option routers __192.168.124.1__;
  option domain-name-servers __192.168.124.1__;
  range __192.168.124.100__ __192.168.124.200__;
  class "pxeclients" {
    match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
    next-server __192.168.124.2__;
          if option architecture-type = 00:07 {
            filename "redhat/EFI/BOOT/BOOTX64.EFI";
          }
          else {
            filename "pxelinux/pxelinux.0";
          }
  }
  class "httpclients" {
    match if substring (option vendor-class-identifier, 0, 10) = "HTTPClient";
    option vendor-class-identifier "HTTPClient";
    filename "http://__192.168.124.2__/redhat/EFI/BOOT/BOOTX64.EFI";
  }
}
----

. Start the DHCPv4 service:
+
----
# systemctl enable --now dhcpd 
----
