:_mod-docs-content-type: REFERENCE
[id="recommended-partitioning-scheme_{context}"]
= Recommended partitioning scheme

[role="_abstract"]
Create separate file systems at the following mount points. However, if required, you can also create the file systems at `/usr`, `/var`, and `/tmp` mount points.

* `/boot`

* `/` (root)

* `/home`

* `swap`

* `/boot/efi`

* `PReP`

This partition scheme is recommended for bare metal deployments and it does not apply to virtual and cloud deployments.

`/boot` partition - recommended size at least 1 GiB::
+
The partition mounted on `/boot` contains the operating system kernel, which allows your system to boot {ProductName} {productnumber}, along with files used during the bootstrap process. Due to the limitations of most firmwares, create a small partition to hold these. In most scenarios, a 1 GiB boot partition is adequate. Unlike other mount points, using an LVM volume for `/boot` is not possible - `/boot` must be located on a separate disk partition. 

If you have a RAID card, be aware that some BIOS types do not support booting from the RAID card. In such a case, the `/boot` partition must be created on a partition outside of the RAID array, such as on a separate disk.

[WARNING]
====
* Normally, the `/boot` partition is created automatically by the installation program. However, if the `/` (root) partition is larger than 2{nbsp}TiB and (U)EFI is used for booting, you need to create a separate `/boot` partition that is smaller than 2{nbsp}TiB to boot the machine successfully.

* Ensure the `/boot` partition is located within the first 2{nbsp}TB of the disk while manual partitioning. Placing the `/boot` partition beyond the 2{nbsp}TB boundary might result in a successful installation, but the system fails to boot because BIOS cannot read the `/boot` partition beyond this limit.
====

`root` - recommended size of 10 GiB::
+
This is where "pass:attributes[{blank}]`/`pass:attributes[{blank}]", or the root directory, is located. The root directory is the top-level of the directory structure. By default, all files are written to this file system unless a different file system is mounted in the path being written to, for example, `/boot` or `/home`.
+
While a 5{nbsp}GiB root file system allows you to install a minimal installation, it is recommended to allocate at least 10{nbsp}GiB so that you can install as many package groups as you want.

Do not confuse the `/` directory with the `/root` directory. The `/root` directory is the home directory of the root user. The `/root` directory is sometimes referred to as _slash root_ to distinguish it from the root directory.

`/home` - recommended size at least 1 GiB::
+
To store user data separately from system data, create a dedicated file system for the `/home` directory. Base the file system size on the amount of data that is stored locally, number of users, and so on. You can upgrade or reinstall {ProductName} {productnumber} without erasing user data files. If you select automatic partitioning, it is recommended to have at least 55 GiB of disk space available for the installation, to ensure that the `/home` file system is created.

`swap` partition - recommended size at least 1 GiB::
+
Swap file systems support virtual memory; data is written to a swap file system when there is not enough RAM to store the data your system is processing. Swap size is a function of system memory workload, not total system memory and therefore is not equal to the total system memory size. It is important to analyze what applications a system will be running and the load those applications will serve in order to determine the system memory workload. Application providers and developers can provide guidance.
+
When the system runs out of swap space, the kernel terminates processes as the system RAM memory is exhausted. Configuring too much swap space results in storage devices being allocated but idle and is a poor use of resources. Too much swap space can also hide memory leaks. The maximum size for a swap partition and other additional information can be found in the `mkswap(8)` manual page.
+
The following table provides the recommended size of a swap partition depending on the amount of RAM in your system and if you want sufficient memory for your system to hibernate. If you let the installation program partition your system automatically, the swap partition size is established using these guidelines. Automatic partitioning setup assumes hibernation is not in use. The maximum size of the swap partition is limited to 10 percent of the total size of the disk, and the installation program cannot create swap partitions more than 1TiB. To set up enough swap space to allow for hibernation, or if you want to set the swap partition size to more than 10 percent of the system's storage space, or more than 1TiB, you must edit the partitioning layout manually.

.Recommended system swap space

[options="header"]
|===
|Amount of RAM in the system|Recommended swap space|Recommended swap space if allowing for hibernation
|Less than{nbsp}2 GiB|2 times the amount of RAM|3 times the amount of RAM
|2 GiB - 8 GiB|Equal to the amount of RAM|2 times the amount of RAM
|8 GiB - 64 GiB|4 GiB to 0.5 times the amount of RAM|1.5 times the amount of RAM
|More than 64 GiB|Workload dependent (at least 4GiB)|Hibernation not recommended
|===


`/boot/efi` partition - recommended size of 200 MiB::

UEFI-based AMD64, Intel 64, and 64-bit ARM require a 200 MiB EFI system partition. The recommended minimum size is 200 MiB, the default size is 600 MiB, and the maximum size is 600 MiB.
BIOS systems do not require an EFI system partition.

At the border between each range, for example, a system with 2{nbsp}GiB, 8{nbsp}GiB, or 64{nbsp}GiB of system RAM, discretion can be exercised with regard to chosen swap space and hibernation support. If your system resources allow for it, increasing the swap space can lead to better performance.

Distributing swap space over multiple storage devices - particularly on systems with fast drives, controllers and interfaces - also improves swap space performance.

Many systems have more partitions and volumes than the minimum required. Choose partitions based on your particular system needs. If you are unsure about configuring partitions, accept the automatic default partition layout provided by the installation program.

[NOTE]
====
Only assign storage capacity to those partitions you require immediately. You can allocate free space at any time, to meet needs as they occur.
====

`PReP` boot partition - recommended size of 4 to 8 MiB::
When installing {productname} on IBM Power System servers, the first partition of the disk should include a `PReP` boot partition. This contains the GRUB boot loader, which allows other IBM Power Systems servers to boot {productname}.