:_mod-docs-content-type: CONCEPT
[id="installation-options_{context}"]
= Supported architectures

[role="_abstract"]
{productname} supports the following architectures:


* AMD and Intel 64-bit architectures 
* The 64-bit ARM architecture 
* IBM Power Systems, Little Endian  
* 64-bit IBM Z architectures

[NOTE]
====
For installation instructions on IBM Power Servers, see link:https://www.ibm.com/docs/en/linux-on-systems?topic=servers-quick-start-guides-installing-linux[_IBM installation documentation_]. To ensure that your system is supported for installing RHEL, see https://catalog.redhat.com and https://access.redhat.com/articles/rhel-limits.
====
