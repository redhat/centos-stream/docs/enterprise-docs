:_mod-docs-content-type: PROCEDURE

:experimental:

[id="completing-your-lc-server-installation_{context}"]
= Completing your RHEL installation on IBM LC servers

[role="_abstract"]
After you boot the {RHEL}{NBSP}{ProductNumber} installer, the installation program walks you through the steps.

. Complete the installation program for {productname} to set up disk options, your user name and password, time zones, and so on. The last step is to restart your system.
+
[NOTE]
====
While your system is restarting, remove the USB device.
====

. After the system restarts, Petitboot displays the option to boot {RHEL}{NBSP}{ProductNumber}. Select this option and press kbd:[Enter].
