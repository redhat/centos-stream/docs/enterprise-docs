:_mod-docs-content-type: CONCEPT
[id="adding-a-qeth-device_{context}"]
= Adding a qeth device

[role="_abstract"]
The `qeth` network device driver supports 64-bit IBM{nbsp}Z OSA-Express features in QDIO mode, HiperSockets, z/VM guest LAN, and z/VM VSWITCH.

//The qeth device driver assigns the same interface name for Ethernet and Hipersockets devices: `enc__<device number>__`. The bus ID is composed of the channel subsystem ID, subchannel set ID, and device number, separated by dots; the device number is the last part of the bus ID, without leading zeroes. For example, the interface name will be `enca00` for a device with the bus ID `0.0.0a00`.

For more information about the qeth device driver naming scheme, see 
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/interactively_installing_rhel_over_the_network/index#customizing-boot-parameters_preparing-a-rhel-installation-on-64-bit-ibm-z[Customizing boot parameters].

//this is not specifically a concept, but rather a description of functionality.
