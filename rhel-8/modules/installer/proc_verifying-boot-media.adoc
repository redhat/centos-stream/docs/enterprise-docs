:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_verifying-boot-media.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="verifying-boot-media_{context}"]
= Verifying boot media

[role="_abstract"]
Verifying ISO images helps to avoid problems that are sometimes encountered during installation. These sources include DVD and ISO images stored on a disk or NFS server.
Use this procedure to test the integrity of an ISO-based installation source before using it to install {RHEL}.

.Prerequisites

* You have accessed the {RHEL} boot menu.

.Procedure

ifeval::[{ProductNumber} == 8]
. From the boot menu, select *Test this media & install {RHEL} 8.1* to test the boot media.
endif::[]

ifeval::[{ProductNumber} == 9]
. From the boot menu, select *Test this media & install {RHEL} 9* to test the boot media.
endif::[]
. The boot process tests the media and highlights any issues.

. Optional: You can start the verification process by appending `rd.live.check` to the boot command line.
