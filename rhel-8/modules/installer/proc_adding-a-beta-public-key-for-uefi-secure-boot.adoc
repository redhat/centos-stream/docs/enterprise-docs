:_mod-docs-content-type: PROCEDURE
[id="adding-a-beta-public-key-for-uefi-secure-boot_{context}"]
= Adding a Beta public key for UEFI Secure Boot

[role="_abstract"]
This section contains information about how to add a {productname} Beta public key for UEFI Secure Boot.

.Prerequisites

* The UEFI Secure Boot is disabled on the system.
* The {productname} Beta release is installed, and Secure Boot is disabled even after system reboot.
* You are logged in to the system, and the tasks in the *Initial Setup* window are complete.

.Procedure

. Begin to enroll the Red Hat Beta public key in the system's Machine Owner Key (MOK) list:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] mokutil --import /usr/share/doc/kernel-keys/$(uname -r)/kernel-signing-ca.cer
....
+
`$(uname -r)` is replaced by the kernel version - for example, *4.18.0-80.el8.x86_64*.

. Enter a password when prompted.

. Reboot the system and press any key to continue the startup.
  The Shim UEFI key management utility starts during the system startup.

. Select *Enroll MOK*.

. Select *Continue*.

. Select *Yes* and enter the password.
  The key is imported into the system's firmware.

. Select *Reboot*.

. Enable Secure Boot on the system.
