:_mod-docs-content-type: PROCEDURE
[id="connecting-to-the-installation-system_{context}"]
= Performing a remote RHEL installation by using VNC on IBM Z

[role="_abstract"]
After the Initial Program Load (IPL) of the Anaconda installation program is complete, connect to the 64-bit IBM Z system from a local machine, as an `install` user, using an ssh connection.

You need to connect to the installation system to continue the installation process. Use a VNC mode to run a GUI-based installation or use the established connection to run a text mode installation.

.Prerequisite

* You booted the installation media as described in xref:installing-in-an-lpar_booting-the-installation-media[Booting the installation on IBM Z to install RHEL in an LPAR].
+
The initial program boot is complete on the 64-bit IBM Z system, and the command prompt displays:
+
----
Starting installer, one moment...
        Please ssh install@my-z-system (system ip address) to begin the install.
----

* If you want to restrict VNC access to the installation system, then ensure `inst.vncpassword=_PASSWORD_` boot parameter is configured.

.Procedure

From a local machine, run the steps below to set up a remote connection with the 64-bit IBM Z system.

. On the command prompt, run the following command:
+
----
$ssh install@_my-z-system-domain-name_
----
+
or
+
----
$ssh install@_my-z-system-IP-address_
----

. Depending on whether or not have you configured the `inst.vnc` parameter, the ssh session displays the following output:
+
When `inst.vnc` parameter is configured:
+
----
Starting installer, one moment...
Please manually connect your vnc client to my-z-system:1 (_system-ip-address:1_) to begin the install.
----
+
When `inst.vnc` parameter is not configured:
+
----
Starting installer, one moment...
Graphical installation is not available. Starting text mode.
=============
Text mode provides a limited set of installation options.
It does not offer custom partitioning for full control
over the disk layout. Would you like to use VNC mode instead?
1) Start VNC
2) Use text mode
Please make your choice from above ['q' to quit | 'c' to continue | 'r' to refresh]:
----
+
If you have configured the `inst.vnc` parameter, proceed to step 5.

. Enter 1 to start VNC.

. Enter a password, if you have not set the `inst.vncpassword=` boot option, but want to secure the server connection.

. From a new command prompt, connect to the VNC server.
+
----
$vncviewer _my-z-system-ip-address:display_number_
----
+
If you have secured the connection, use the password that you have entered in the previous step or the one that you had set for `inst.vncpassword=` boot option.
+
The RHEL installer is launched in the VNC client.
