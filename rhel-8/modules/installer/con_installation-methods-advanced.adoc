:_mod-docs-content-type: CONCEPT
[id="installation-methods_advanced_{context}"]
= Available installation methods

[role="_abstract"]
You can install Red Hat Enterprise Linux using any of the following methods:

* GUI-based installations
* System or cloud image-based installations
* Advanced installations

.GUI-based installations

You can choose from the following GUI-based installation methods:

* *Install {productshortname} using an ISO image from the Customer Portal:*
Install {productname} by downloading the *DVD ISO* image file from the Customer Portal. Registration is performed after the installation completes. This installation method is supported by the GUI and Kickstart.

* *Register and install {productshortname} from the Content Delivery Network:*
Register your system, attach subscriptions, and install {productname} from the Content Delivery Network (CDN). This installation method supports *Boot ISO* and *DVD ISO* image files; however, the *Boot ISO* image file is recommended as the installation source defaults to CDN for the Boot ISO image file.
After registering the system, the installer downloads and installs packages from the CDN.
This installation method is also supported by Kickstart.

* *Perform a remote {productshortname} installation using VNC:*
The {productshortname} installation program offers two Virtual Network Computing (VNC) installation modes: Direct and Connect.
After a connection is established, the two modes do not differ. The mode you select depends on your environment.

* *Install {productshortname} from the network using PXE :*
With a network installation using preboot execution environment (PXE), you can install {productname} to a system that has access to an installation server.
At a minimum, two systems are required for a network installation.

.System or cloud image-based installations

You can use system or cloud image-based installation methods only in virtual and cloud environments.
To perform a system or cloud image-based installation, use Red Hat Image Builder.
Image builder creates customized system images of Red Hat Enterprise Linux, including the system images for cloud deployment.

For more information about installing {productshortname} using Image builder, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/composing_a_customized_rhel_system_image/index/[Composing a customized {productshortname} system image].

.Advanced installations

You can choose from the following advanced installation methods:

* *Perform an automated {productshortname} installation using Kickstart:*
Kickstart is an automated process that helps you install the operating system by specifying all your requirements and configurations in a file.
The Kickstart file contains {productshortname} installation options, for example, the time zone, drive partitions, or packages to be installed.
Providing a prepared Kickstart file completes installation without the need for any user intervention.
This is useful when deploying Red Hat Enterprise Linux on a large number of systems at once.

* *Register and install {productshortname} from the Content Delivery Network:*
Register and install {productname} on all architectures from the Content Delivery Network (CDN).
Registration is performed before the installation packages are downloaded and installed from CDN.
This installation method is supported by the graphical user interface and Kickstart.
