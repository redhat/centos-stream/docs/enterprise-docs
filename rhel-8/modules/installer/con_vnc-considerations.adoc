:_mod-docs-content-type: CONCEPT

[id="vnc-considerations_{context}"]
= Considerations

[role="_abstract"]
By default, the installation program has a VNC server included. Consider the following items when performing a remote RHEL installation using VNC:

* *VNC client application:* A VNC client application is required to perform both a VNC Direct and Connect installation. VNC client applications are available in the repositories of most Linux distributions, and free VNC client applications are also available for other operating systems such as Windows. The following VNC client applications are available in RHEL:

** `tigervnc` is independent of your desktop environment and is installed as part of the `tigervnc` package.
** `vinagre` is part of the GNOME desktop environment and is installed as part of the `vinagre` package.


* *Network and firewall:*
** If the target system is not allowed inbound connections by a firewall, then you must use Connect mode or disable the firewall. Disabling a firewall can have security implications.
** If the system that is running the VNC viewer is not allowed incoming connections by a firewall, then you must use Direct mode, or disable the firewall. Disabling a firewall can have security implications. See the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/security_hardening/index/[Security hardening] document for more information about configuring the firewall.

* *Custom Boot Options:* You must specify custom boot options to start a VNC installation and the installation instructions might differ depending on your system architecture.
////
See *<add link to updated boot options when it is ready>*
////

* *VNC in Kickstart installations:* You can use VNC-specific commands in Kickstart installations. Using only the `vnc` command runs a RHEL installation in Direct mode. Additional options are available to set up an installation using Connect mode.


////
For more information about the vnc command and options used in Kickstart files, see *<add link to Kickstart boot options>*
////
