:_mod-docs-content-type: PROCEDURE

:experimental:

[id="creating-a-user-account_{context}"]
= Creating a user account

[role="_abstract"]
Create a user account to finish the installation. If you do not create a user account, you must log in to the system as `root` directly, which is *not* recommended.

.Procedure

. On the *Installation Summary* window, select *User Settings > User Creation*. The *Create User* window opens.

. Type the user account name in to the *Full name* field, for example: John Smith.

. Type the username in to the *User name* field, for example: jsmith.
+
The *User name* is used to log in from a command line; if you install a graphical environment, then your graphical login manager uses the *Full name*.

. Select the *Make this user administrator* check box if the user requires administrative rights (the installation program adds the user to the `wheel` group ).
+
An administrator user can use the `sudo` command to perform tasks that are only available to `root` using the user password, instead of the `root` password. This may be more convenient, but it can also cause a security risk.

. Select the *Require a password to use this account* check box.
+
If you give administrator privileges to a user, ensure the account is password protected. Never give a user administrator privileges without assigning a password to the account.

. Type a password into the *Password* field.

. Type the same password into the *Confirm password* field.

. Click btn:[Done] to apply the changes and return to the *Installation Summary* window.