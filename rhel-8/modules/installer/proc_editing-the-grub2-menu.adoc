////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

:experimental:

[id="proc_editing-the-grub2-menu_{context}"]
= Editing boot options for the UEFI-based systems

You can edit the GRUB boot menu on UEFI-based systems during a RHEL installation to customize parameters. This allows configuring specific settings ensuring the installation meets their requirements.

.Prerequisites

* You have created bootable installation media (USB, CD or DVD).
* You have booted the installation from the media, and the installation boot menu is open.

.Procedure

. From the boot menu window, select the required option and press kbd:[e].

. On UEFI systems, the kernel command line starts with `linuxefi`. Move the cursor to the end of the `linuxefi` kernel command line.

. Edit the parameters as required. For example, to enable the cryptographic module self-checks mandated by the Federal Information Processing Standard (FIPS) 140, add `fips=1`:
+
[subs="quotes, macros, attributes"]
----
linuxefi /images/pxeboot/vmlinuz inst.stage2=hd:LABEL=RHEL-9-4-0-BaseOS-x86_64 rd.live.\
check quiet fips=1
----

. When you finish editing, press kbd:[Ctrl]+kbd:[X] to start the installation using the specified options.
