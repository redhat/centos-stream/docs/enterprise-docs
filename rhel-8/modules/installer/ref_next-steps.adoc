:_mod-docs-content-type: REFERENCE
[id="next-steps_{context}"]
= Next steps

[role="_abstract"]
When you have completed the required post-installation steps, you can configure basic system settings. For information about completing tasks such as installing software with {PackageManagerCommand}, using systemd for service management, managing users, groups, and file permissions, using chrony to configure NTP, and working with Python 3, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/index[_Configuring basic system settings_] document.
