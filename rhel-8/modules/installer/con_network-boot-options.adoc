:_mod-docs-content-type: CONCEPT
[id="network-boot-options_{context}"]
= Network boot options

[role="_abstract"]
If your scenario requires booting from an image over the network instead of booting from a local image, you can use the following options to customize network booting.

[NOTE]
====
Initialize the network with the `dracut` tool. For complete list of `dracut` options, see the `dracut.cmdline(7)` man page on your system.
====

ip=::
Use the `ip=` boot option to configure one or more network interfaces. To configure multiple interfaces, use one of the following methods;

* use the `ip` option multiple times, once for each interface; to do so, use the `rd.neednet=1` option, and specify a primary boot interface using the `bootdev` option.
* use the `ip` option once, and then use Kickstart to set up further interfaces. This option accepts several different formats. The following tables contain information about the most common options.

In the following tables:

* The `ip` parameter specifies the client IP address and `IPv6` requires square brackets, for example 192.0.2.1 or [2001:db8::99].
* The `gateway` parameter is the default gateway. `IPv6` requires square brackets.
* The `netmask` parameter is the netmask to be used. This can be either a full netmask (for example, 255.255.255.0) or a prefix (for example, 64).
* The `hostname` parameter is the host name of the client system. This parameter is optional.

+
.Boot option formats to configure the network interface
[options="header"]
|===
| Boot option format | Configuration method
| `ip=_method_` | Automatic configuration of any interface
| `ip=_interface:method_` | Automatic configuration of a specific interface
| `ip=_ip::gateway:netmask:hostname:interface_:none` | Static configuration, for example, IPv4: `ip=192.0.2.1::192.0.2.254:255.255.255.0:server.example.com:enp1s0:none`

IPv6: `ip=[2001:db8::1]::[2001:db8::fffe]:64:server.example.com:enp1s0:none`
| `ip=_ip::gateway:netmask:hostname:interface:method:mtu_` | Automatic configuration of a specific interface with an override
|===
+

[discrete]
== Configuration methods for the automatic interface
The method `automatic configuration of a specific interface with an override` opens the interface using the specified method of automatic configuration, such as `dhcp`, but overrides the automatically obtained IP address, gateway, netmask, host name or other specified parameters. All parameters are optional, so specify only the parameters that you want to override.

+
The `method` parameter can be any of the following:

DHCP::
`dhcp`

IPv6 DHCP::
`dhcp6`

IPv6 automatic configuration::
`auto6`

iSCSI Boot Firmware Table (iBFT):: `ibft`

+
[NOTE]
====
* If you use a boot option that requires network access, such as `inst.ks=http://host/path`, without specifying the `ip` option, the default value of the `ip` option is `ip=dhcp`..
* To connect to an iSCSI target automatically, activate a network device for accessing the target by using the `ip=ibft` boot option.
====

nameserver=::
The `nameserver=` option specifies the address of the name server. You can use this option multiple times.
+
[NOTE]
====
The `ip=` parameter requires square brackets. However, an IPv6 address does not work with square brackets. An example of the correct syntax to use for an IPv6 address is `nameserver=2001:db8::1`.
====
+
bootdev=::
The `bootdev=` option specifies the boot interface. This option is mandatory if you use more than one `ip` option.

ifname=::
The `ifname=` options assigns an interface name to a network device with a given MAC address. You can use this option multiple times. The syntax is `ifname=interface:MAC`. For example:
+
[subs="macros,attributes"]
----
ifname=eth0:01:23:45:67:89:ab
----
+
[NOTE]
====
The `ifname=` option is the only supported way to set custom network interface names during installation.
====

inst.dhcpclass=::

The `inst.dhcpclass=` option specifies the DHCP vendor class identifier. The `dhcpd` service recognizes this value as `vendor-class-identifier`. The default value is `anaconda-$(uname -srm)`. To ensure the `inst.dhcpclass=` option is applied correctly, request network activation during the early stage of installation by also adding the `ip` option.

inst.waitfornet=::
Using the `inst.waitfornet=SECONDS` boot option causes the installation system to wait for network connectivity before installation. The value given in the `SECONDS` argument specifies the maximum amount of time to wait for network connectivity before timing out and continuing the installation process even if network connectivity is not present.

vlan=::
Use the `vlan=` option to configure a Virtual LAN (VLAN) device on a specified interface with a given name. The syntax is `vlan=name:interface`. For example:
+
[subs="macros,attributes"]
----
vlan=vlan5:enp0s1
----
+
This configures a VLAN device named `vlan5` on the `enp0s1` interface. The name can take the following forms:
+
* VLAN_PLUS_VID: `vlan0005`
* VLAN_PLUS_VID_NO_PAD: `vlan5`
* DEV_PLUS_VID: `enp0s1.0005`
* DEV_PLUS_VID_NO_PAD: `enp0s1.5`


bond=::
Use the `bond=` option to configure a bonding device with the following syntax: `bond=name[:interfaces][:options]`. Replace _name_ with the bonding device name, _interfaces_ with a comma-separated list of physical (Ethernet) interfaces, and _options_ with a comma-separated list of bonding options. For example:
+
[subs="macros,attributes"]
----
bond=bond0:enp0s1,enp0s2:mode=active-backup,tx_queues=32,downdelay=5000
----
+
For a list of available options, execute the `modinfo` bonding command.

team=::
Use the `team=` option to configure a team device with the following syntax: `team=name:interfaces`. Replace _name_ with the desired name of the team device and _interfaces_ with a comma-separated list of physical (Ethernet) devices to be used as underlying interfaces in the team device. For example:
+
[subs="macros,attributes"]
----
team=team0:enp0s1,enp0s2
----
+
ifeval::[{ProductNumber} == 9]
[IMPORTANT]
====
NIC teaming is deprecated in {RHEL9}. Consider using the network bonding driver as an alternative. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/configuring-network-bonding_configuring-and-managing-networking[Configuring a network bond].
====
endif::[]

bridge=::
Use the `bridge=` option to configure a bridge device with the following syntax: `bridge=name:interfaces`. Replace _name_ with the desired name of the bridge device and _interfaces_ with a comma-separated list of physical (Ethernet) devices to be used as underlying interfaces in the bridge device. For example:
+
[subs="macros,attributes"]
----
bridge=bridge0:enp0s1,enp0s2
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/configuring_and_managing_networking/index/[_Configuring and managing networking_]
