:_mod-docs-content-type: PROCEDURE
[id="making-a-kickstart-file-available-on-an-nfs-server_{context}"]
= Sharing the installation files on an NFS server

[role="_abstract"]
You can store the Kickstart script file on an NFS server. Storing it on an NFS server enables you to install multiple systems from a single source without having to use physical media for the Kickstart file.


// A network-based installation is convenient when combined with a TFTP server for PXE boot. This approach eliminates the need to create physical media and enables simultaneous deployment of {RHEL} on multiple systems.


.Prerequisites

* You have an administrator-level access to a server with {ProductName} {ProductNumber} on the local network.
* The system to be installed can connect to the server.
* The firewall on the server allows connections from the system you are installing to. 
ifdef::interactive-install-media[]
See xref:ports-for-network-based-installation_preparing-network-based-repositories[Ports for Network based Installation] for more information.
endif::[]

[IMPORTANT]
====
Ensure that you use different paths in `inst.ks` and `inst.repo`. When using NFS to host the Kickstart, you cannot use the same nfs share to host the installation source.
====

.Procedure

. Install the [package]`nfs-utils` package by running the following command as root:
+
[subs="+attributes"]
----
# {PackageManagerCommand} install nfs-utils
----

. Copy the Kickstart file to a directory on the NFS server.

. Open the [filename]`/etc/exports` file using a text editor and add a line with the following syntax:
+
[subs="quotes,attributes"]
----
/__exported_directory__/ __clients__
----
+
Replace __/exported_directory/__ with the full path to the directory holding the Kickstart file. Instead of __clients__, use the host name or IP address of the computer that is to be installed from this NFS server, the subnetwork from which all computers are to have access the ISO image, or the asterisk sign (`*`) if you want to allow any computer with network access to the NFS server to use the ISO image. See the __exports(5)__ man page for detailed information about the format of this field.
A basic configuration that makes the [filename]`/rhel{ProductNumber}-install/` directory available as read-only to all clients is:
+
[subs="quotes,attributes"]
----
/rhel{ProductNumber}-install *
----

. Save the [filename]`/etc/exports` file and exit the text editor.

. Start the nfs service:
+
----
# systemctl start nfs-server.service
----
+
If the service was running before you changed the [filename]`/etc/exports` file, enter the following command, in order for the running NFS server to reload its configuration:
+
----
# systemctl reload nfs-server.service
----
+
The Kickstart file is now accessible over NFS and ready to be used for installation.

NOTE: When specifying the Kickstart source, use `nfs:` as the protocol, the server’s host name or IP address, the colon sign (`:`), and the path inside directory holding the file. For example, if the server’s host name is `myserver.example.com` and you have saved the file in [filename]`/rhel{ProductNumber}-install/my-ks.cfg`, specify `inst.ks=nfs:myserver.example.com:/rhel{ProductNumber}-install/my-ks.cfg` as the installation source boot option.


[role="_additional-resources"]
.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/interactively_installing_rhel_over_the_network/index#preparing-a-remote-installation-by-using-vnc_rhel-installer[Preparing a remote installation by using VNC]