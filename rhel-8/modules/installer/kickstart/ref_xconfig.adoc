:_mod-docs-content-type: REFERENCE
[id="xconfig_{context}"]
= xconfig

The [command]`xconfig` Kickstart command is optional. It configures the X Window System. Use this command only once.


.Syntax

[subs="quotes,attributes"]
----
[command]`xconfig [--startxonboot]`
----


.Options

* [option]`--startxonboot` - Use a graphical login on the installed system.


.Notes

* Because {productname} {ProductNumber} does not include the KDE Desktop Environment, do not use the [option]`--defaultdesktop=` documented in upstream.
