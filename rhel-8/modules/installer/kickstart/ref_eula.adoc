:_mod-docs-content-type: REFERENCE
[id="eula_{context}"]
= eula

The [command]`eula` Kickstart command is optional. Use this option to accept the End User License Agreement (EULA) without user interaction. Specifying this option prevents Initial Setup from prompting you to accept the license agreement after you finish the installation and reboot the system for the first time. Use this command only once.


.Syntax

[subs="quotes,macros"]
----
[command]`eula [--agreed]`
----

.Options

* [option]`--agreed` (required) - Accept the EULA. This option must always be used, otherwise the [command]`eula` command is meaningless.
