:_mod-docs-content-type: REFERENCE
[id="firstboot_{context}"]
= firstboot

The [command]`firstboot` Kickstart command is optional. It determines whether the [GUI]`Initial Setup` application starts the first time the system is booted. If enabled, the [package]*initial-setup* package must be installed. If not specified, this option is disabled by default. Use this command only once.


.Syntax

[subs="quotes,macros"]
----
[command]`firstboot __OPTIONS__`
----


.Options

* [option]`--enable` or [option]`--enabled` - Initial Setup is started the first time the system boots.

* [option]`--disable` or [option]`--disabled` - Initial Setup is not started the first time the system boots.

* [option]`--reconfig` - Enable the Initial Setup to start at boot time in reconfiguration mode. This mode enables the root password, time & date, and networking & host name configuration options in addition to the default ones.
