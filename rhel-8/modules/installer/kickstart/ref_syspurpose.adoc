:_mod-docs-content-type: REFERENCE
[id="syspurpose_{context}"]
= syspurpose

The [command]`syspurpose` Kickstart command is optional. Use it to set the system purpose which describes how the system will be used after installation. This information helps apply the correct subscription entitlement to the system. Use this command only once.

ifeval::[{ProductNumber} == 8]
[NOTE]
====
{RHEL} 8.6 and later enables you to manage and display system purpose attributes with a single module by making the `role`, `service-level`, `usage`, and `addons` subcommands available under one `subscription-manager syspurpose` module.
Previously, system administrators used one of four standalone `syspurpose` commands to manage each attribute.
This standalone `syspurpose` command is deprecated starting with RHEL 8.6 and is planned to be removed in RHEL 9.
Red Hat will provide bug fixes and support for this feature during the current release lifecycle, but this feature will no longer receive enhancements.
Starting with RHEL 9, the single `subscription-manager syspurpose` command and its associated subcommands is the only way to use system purpose.
====
endif::[]

ifeval::[{ProductNumber} == 9]
[NOTE]
====
{RHEL} 9.0 and later enables you to manage and display system purpose attributes with a single module by making the `role`, `service-level`, `usage`, and `addons` subcommands available under one `subscription-manager syspurpose` module.
Previously, system administrators used one of four standalone `syspurpose` commands to manage each attribute.
This standalone `syspurpose` command is deprecated starting with RHEL 9.0 and is planned to be removed in post RHEL 9.
Red Hat will provide bug fixes and support for this feature during the current release lifecycle, but this feature will no longer receive enhancements.
Starting with RHEL 9, the single `subscription-manager syspurpose` command and its associated subcommands is the only way to use system purpose.
====
endif::[]

.Syntax

[subs="quotes,macros"]
----
[command]``syspurpose [__OPTIONS__]``
----


.Options

* [option]`--role=` - Set the intended system role. Available values are:
** Red Hat Enterprise Linux Server
** Red Hat Enterprise Linux Workstation
** Red Hat Enterprise Linux Compute Node

* [option]`--sla=` - Set the Service Level Agreement. Available values are:
** Premium
** Standard
** Self-Support

* [option]`--usage=` - The intended usage of the system. Available values are:
** Production
** Disaster Recovery
** Development/Test

* [option]`--addon=` - Specifies additional layered products or features. You can use this option multiple times.


.Notes

* Enter the values with spaces and enclose them in double quotes:
+
----
syspurpose --role="Red Hat Enterprise Linux Server"
----

ifeval::[{ProductNumber} == 8]

* While it is strongly recommended that you configure System Purpose, it is an optional feature of the {RHEL} installation program. If you want to enable System Purpose after the installation completes, you can do so using the `syspurpose` command-line tool.

endif::[]

ifeval::[{ProductNumber} == 9]

* While it is strongly recommended that you configure System Purpose, it is an optional feature of the {RHEL} installation program.

endif::[]

ifeval::[{ProductNumber} == 8]
[NOTE]
====
{RHEL} 8.6 and later enables you to manage and display system purpose attributes with a single module by making the `role`, `service-level`, `usage`, and `addons` subcommands available under one `subscription-manager syspurpose` module.
Previously, system administrators used one of four standalone `syspurpose` commands to manage each attribute.
This standalone `syspurpose` command is deprecated starting with RHEL 8.6 and is planned to be removed in RHEL 9.
Red Hat will provide bug fixes and support for this feature during the current release lifecycle, but this feature will no longer receive enhancements.
Starting with RHEL 9, the single `subscription-manager syspurpose` command and its associated subcommands is the only way to use system purpose.
====
endif::[]
