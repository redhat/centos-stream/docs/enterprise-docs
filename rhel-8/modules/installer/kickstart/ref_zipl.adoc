:_mod-docs-content-type: REFERENCE
[id="zipl_{context}"]
= zipl

The [command]`zipl` Kickstart command is optional. It specifies the ZIPL configuration for 64-bit IBM Z. Use this command only once.

.Options

* [option]`--secure-boot` - Enables secure boot if it is supported by the installing system.

[NOTE]
====
When installed on a system that is later than IBM z14, the installed system cannot be booted from an IBM z14 or earlier model.
====

* [option]`--force-secure-boot` - Enables secure boot unconditionally.

[NOTE]
====
Installation is not supported on IBM z14 and earlier models.
====

* [option]`--no-secure-boot` - Disables secure boot.

[NOTE]
====
Secure Boot is not supported on IBM z14 and earlier models. Use `--no-secure-boot` if you intend to boot the installed system on IBM z14 and earlier models.
====
