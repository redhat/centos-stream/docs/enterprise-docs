:_mod-docs-content-type: REFERENCE
[id="zerombr_{context}"]
= zerombr

The [command]`zerombr` Kickstart command is optional. The [command]`zerombr` initializes any invalid partition tables that are found on disks and destroys all of the contents of disks with invalid partition tables. This command is required when performing an installation on an 64-bit IBM Z system with unformatted Direct Access Storage Device (DASD) disks, otherwise the unformatted disks are not formatted and used during the installation. Use this command only once.
// This command is required when performing an unattended installation on a system with previously initialized disks.


.Syntax

[subs="quotes,macros"]
----
[command]`zerombr`
----


.Notes

* On 64-bit IBM{nbsp}Z, if [command]`zerombr` is specified, any Direct Access Storage Device (DASD) visible to the installation program which is not already low-level formatted is automatically low-level formatted with dasdfmt. The command also prevents user choice during interactive installations.

* If [command]`zerombr` is not specified and there is at least one unformatted DASD visible to the installation program, a non-interactive Kickstart installation exits unsuccessfully.

* If [command]`zerombr` is not specified and there is at least one unformatted DASD visible to the installation program, an interactive installation exits if the user does not agree to format all visible and unformatted DASDs. To circumvent this, only activate those DASDs that you will use during installation. You can always add more DASDs after installation is complete.

* This command has no options.
