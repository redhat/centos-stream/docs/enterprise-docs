:_mod-docs-content-type: REFERENCE
[id="cdrom_{context}"]
= cdrom

The [command]`cdrom` Kickstart command is optional. It performs the installation from the first optical drive on the system. Use this command only once.


.Syntax

[subs="quotes,macros"]
----
[command]`cdrom`
----


.Notes

ifeval::[{ProductNumber} == 8]
* Previously, the [command]`cdrom` command had to be used together with the [command]`install` command. The [command]`install` command has been deprecated and [command]`cdrom` can be used on its own, because it implies [command]`install`.
endif::[]

* This command has no options.
* To actually run the installation, you must specify one of `cdrom`, `harddrive`, `hmc`, `nfs`, `liveimg`, `ostreesetup`, `rhsm`, or `url` unless the `inst.repo` option is specified on the kernel command line.