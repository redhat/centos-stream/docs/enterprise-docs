:_mod-docs-content-type: CONCEPT
[id="system-purpose_{context}"]
= Overview

[role="_abstract"]
You can enter System Purpose data in one of the following ways:

* During image creation
* During a GUI installation when using the *Connect to Red Hat* screen to register your system and attach your Red Hat subscription
* During a Kickstart installation when using the `syspurpose Kickstart` command
ifeval::[{ProductNumber} == 8]
* After installation using the `syspurpose` command-line (CLI) tool
endif::[]

ifeval::[{ProductNumber} == 9]
* After installation using the `subscription-manager` command-line (CLI) tool
endif::[]

To record the intended purpose of your system, you can configure the following components of System Purpose. The selected values are used by the entitlement server upon registration to attach the most suitable subscription for your system.

Role::
** Red Hat Enterprise Linux Server
** Red Hat Enterprise Linux Workstation
** Red Hat Enterprise Linux Compute Node

Service Level Agreement::
** Premium
** Standard
** Self-Support

Usage::
** Production
** Development/Test
** Disaster Recovery

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/composing_a_customized_rhel_system_image/index/[Composing a customized RHEL system image]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/automatically_installing_rhel/index[Automatically installing RHEL]
* link:https://docs.redhat.com/en/documentation/subscription_central/1-latest/html/getting_started_with_rhel_system_registration/adv-reg-rhel-using-rhsm_#understanding_rhsm_con[Using and Configuring Red Hat Subscription Manager]
