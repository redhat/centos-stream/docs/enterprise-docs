:_mod-docs-content-type: CONCEPT
[id="console-environment-and-display-boot-options_{context}"]
= Console boot options

[role="_abstract"]
This section describes how to configure boot options for your console, monitor display, and keyboard.

console=::
Use the `console=` option to specify a device that you want to use as the primary console. For example, to use a console on the first serial port, use `console=ttyS0`. When using the `console=` argument, the installation starts with a text UI. If you must use the `console=` option multiple times, the boot message is displayed on all specified console. However, the installation program uses only the last specified console. For example, if you specify `console=ttyS0 console=ttyS1`, the installation program uses `ttyS1`.

inst.lang=::
Use the `inst.lang=` option to set the language that you want to use during the installation. To view the list of locales, enter the command `locale -a | grep _` or the `localectl list-locales | grep _` command.

ifeval::[{ProductNumber} == 8]
inst.singlelang::
Use the `inst.singlelang` option to install in single language mode, which results in no available interactive options for the installation language and language support configuration. If a language is specified using the `inst.lang` boot option or the `lang` Kickstart command, then it is used. If no language is specified, the installation program defaults to `en_US.UTF-8`.
endif::[]

inst.geoloc=::
Use the `inst.geoloc=` option to configure geolocation usage in the installation program. Geolocation is used to preset the language and time zone, and uses the following syntax: `inst.geoloc=value`. The `value` can be any of the following parameters:

* Disable geolocation: `inst.geoloc=0`
* Use the Fedora GeoIP API: `inst.geoloc=provider_fedora_geoip`.
ifeval::[{ProductNumber} == 9]
This option is deprecated.
endif::[]
* Use the Hostip.info GeoIP API: `inst.geoloc=provider_hostip`.
ifeval::[{ProductNumber} == 9]
This option is deprecated.
endif::[]
ifeval::[{ProductNumber} == 8]
+
If you do not specify the `inst.geoloc=` option, the default option is `provider_fedora_geoip`.
endif::[]
inst.keymap=::
Use the `inst.keymap=` option to specify the keyboard layout to use for the installation.

inst.cmdline::
Use the `inst.cmdline` option to force the installation program to run in command-line mode. This mode does not allow any interaction, and you must specify all options in a Kickstart file or on the command line.

inst.graphical::
Use the `inst.graphical` option to force the installation program to run in graphical mode. The graphical mode is the default.

inst.text::
Use the `inst.text` option to force the installation program to run in text mode instead of graphical mode.

inst.noninteractive::
Use the `inst.noninteractive` boot option to run the installation program in a non-interactive mode. User interaction is not permitted in the non-interactive mode, and `inst.noninteractive` you can use the `inst.nointeractive` option with a graphical or text installation. When you use the `inst.noninteractive` option in text mode, it behaves the same as the `inst.cmdline` option.

inst.resolution=::
Use the `inst.resolution=` option to specify the screen resolution in graphical mode. The format is `NxM`, where _N_ is the screen width and _M_ is the screen height (in pixels). The recommended resolution is 1024x768.

inst.vnc::
Use the `inst.vnc` option to run the graphical installation using Virtual Network Computing (VNC). You must use a VNC client application to interact with the installation program. When VNC sharing is enabled, multiple clients can connect. A system installed using VNC starts in text mode.

inst.vncpassword=::
Use the `inst.vncpassword=` option to set a password on the VNC server that is used by the installation program.

inst.vncconnect=::
Use the `inst.vncconnect=` option to connect to a listening VNC client at the given host location, for example, `inst.vncconnect=<host>[:<port>]`
The default port is 5900. You can use this option by entering the command `vncviewer -listen`.

inst.xdriver=::
Use the `inst.xdriver=` option to specify the name of the X driver to use both during installation and on the installed system.

inst.usefbx::
Use the `inst.usefbx` option to prompt the installation program to use the frame buffer X driver instead of a hardware-specific driver. This option is equivalent to the `inst.xdriver=fbdev` option.

modprobe.blacklist=::
Use the `modprobe.blacklist=` option to blocklist or completely disable one or more drivers. Drivers (mods) that you disable using this option cannot load when the installation starts. After the installation finishes, the installed system retains these settings. You can find a list of the blocklisted drivers in the `/etc/modprobe.d/` directory. Use a comma-separated list to disable multiple drivers. For example:
+
[subs="macros,attributes"]
----
modprobe.blacklist=ahci,firewire_ohci
----
+
[NOTE]
====
You can use `modprobe.blacklist` in combination with the different command line options. For example, use it with the `inst.dd` option to ensure that an updated version of an existing driver is loaded from a driver update disc:

[subs="macros,attributes"]
----
modprobe.blacklist=virtio_blk
----
====

inst.xtimeout=::

Use the `inst.xtimeout=` option to specify the timeout in seconds for starting X server.

inst.sshd::
Use the `inst.sshd` option to start the `sshd` service during installation, so that you can connect to the system during the installation using SSH, and monitor the installation progress. For more information about SSH, see the `ssh(1)` man page on your system. By default, the `sshd` option is automatically started only on the 64-bit IBM Z architecture. On other architectures, `sshd` is not started unless you use the `inst.sshd` option.
+
[NOTE]
====
During installation, the root account has no password by default. You can set a root password during installation with the `sshpw` Kickstart command.
====

inst.kdump_addon=::
Use the `inst.kdump_addon=` option to enable or disable the Kdump configuration screen (add-on) in the installation program. This screen is enabled by default; use `inst.kdump_addon=off` to disable it. Disabling the add-on disables the Kdump screens in both the graphical and text-based interface as well as the `%addon com_redhat_kdump` Kickstart command.
