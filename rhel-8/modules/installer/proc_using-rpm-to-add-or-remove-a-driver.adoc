:_mod-docs-content-type: PROCEDURE

[id="using-rpm-to-add-or-remove-a-driver_{context}"]
= Using {PackageManagerCommand} to add or remove a driver

[role="_abstract"]
Missing or malfunctioning drivers cause problems when booting the system. Rescue mode provides an environment in which you can add or remove a driver even when the system fails to boot. Wherever possible, use the {PackageManagerCommand} package manager to remove malfunctioning drivers or to add updated or missing drivers. 

[IMPORTANT]
====
When you install a driver from a driver disc, the driver disc updates all `initramfs` images on the system to use this driver. If a problem with a driver prevents a system from booting, you cannot rely on booting the system from another `initramfs` image.
====

== Adding a driver using {PackageManagerCommand}
Use this procedure to add a driver.

.Prerequisites

* You have booted into rescue mode.
* You have mounted the installed system in read-write mode.

.Procedure

. Make the RPM package that contains the driver available. For example, mount a CD or USB flash drive and copy the RPM package to a location of your choice under `/mnt/sysroot/`, for example: `/mnt/sysroot/root/drivers/`.

. Change the root directory to `/mnt/sysroot/`:
+
[subs="quotes, macros, attributes"]
----
sh-4.2# chroot /mnt/sysroot/
----
+
. Use the `{PackageManagerCommand} install` command to install the driver package. For example, run the following command to install the `xorg-x11-drv-wacom` driver package from `/root/drivers/`:
+
[subs="quotes, macros, attributes"]
----
sh-4.2# {PackageManagerCommand} install /root/drivers/xorg-x11-drv-wacom-0.23.0-6.el7.x86_64.rpm
----
+
[NOTE]
====
The `/root/drivers/` directory in this chroot environment is the `/mnt/sysroot/root/drivers/` directory in the original rescue environment.
====
+
. Exit the chroot environment:
+
[subs="quotes, macros, attributes"]
----
sh-4.2# exit
----

== Removing a driver using {PackageManagerCommand}

Use this procedure to remove a driver.

.Prerequisites

* You have booted into rescue mode.
* You have mounted the installed system in read-write mode.

.Procedure

. Change the root directory to the `/mnt/sysroot/` directory:
+
[subs="quotes, macros, attributes"]
----
sh-4.2# chroot /mnt/sysroot/
----
+
. Use the `{PackageManagerCommand} remove` command to remove the driver package. For example, to remove the `xorg-x11-drv-wacom` driver package, run:
+
[subs="quotes, macros, attributes"]
----
sh-4.2# {PackageManagerCommand} remove xorg-x11-drv-wacom
----
+
. Exit the chroot environment:
+
[subs="quotes, macros, attributes"]
----
sh-4.2# exit
----
+
If you cannot remove a malfunctioning driver for some reason, you can instead blocklist the driver so that it does not load at boot time.

. When you have finished adding and removing drivers, reboot the system.
