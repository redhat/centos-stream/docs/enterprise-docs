:_mod-docs-content-type: CONCEPT
[id="choose-your-installation-method-on-ibm-lc-servers_{context}"]
= Choose your installation boot method on IBM LC servers

[role="_abstract"]
You can either boot the Red Hat Enterprise Linux installation from a USB device or through virtual media.

To boot the installation from a USB device, see xref:configuring-petitboot-for-installation-with-usb-device_preparing-a-rhel-installation-on-ibm-power-system-lc-servers[Configuring Petitboot for installation with USB device]

To boot the installation through virtual media, see xref:access-bmc-advanced-system-management-interface-to-configure-virtual-media_preparing-a-rhel-installation-on-ibm-power-system-lc-servers[Access BMC Advanced System Management interface to configure virtual media].
