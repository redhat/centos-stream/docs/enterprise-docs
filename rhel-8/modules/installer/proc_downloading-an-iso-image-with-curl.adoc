:_mod-docs-content-type: PROCEDURE
[id="downloading-an-iso-image-with-curl_{context}"]
= Downloading an ISO image using curl

With the `curl` tool, you can fetch the required file from the web using the command line to save locally or pipe it into another program as required.
This section explains how to download installation images using the `curl` command.

.Prerequisites

* The `curl` and `jq` packages are installed.
+
If your Linux distribution does not use `{PackageManagerCommand}` or `apt`, or if you do not use Linux, download the most appropriate software package from the link:https://curl.haxx.se/download.html[curl website].
* You have an offline token generated from link:https://access.redhat.com/management/api[Red Hat API Tokens].
* You have a checksum of the file you want to download from link:https://access.redhat.com/downloads/[Product Downloads].

.Procedure

. Create a bash file with the following content:
+
[source,subs="+quotes,attributes"]
....
#!/bin/bash
# set the offline token and checksum parameters
offline_token="_<offline_token>_"
checksum=_<checksum>_

# get an access token
access_token=$(curl https://sso.redhat.com/auth/realms/redhat-external/protocol/openid-connect/token -d grant_type=refresh_token -d client_id=rhsm-api -d refresh_token=$offline_token | jq -r '.access_token')

# get the filename and download url
image=$(curl -H "Authorization: Bearer $access_token" "https://api.access.redhat.com/management/v1/images/$checksum/download")
filename=$(echo $image | jq -r .body.filename)
url=$(echo $image | jq -r .body.href)

# download the file
curl $url -o $filename
....
+
In the text above, replace _<offline_token>_ with the token collected from the Red Hat API portal and _<checksum>_ with the checksum value taken from the _Product Downloads_ page.

. Make this file executable.
+
[literal,subs="+quotes,attributes"]
----
$ chmod u+x FILEPATH/FILENAME.sh
----

. Open a terminal window and execute the bash file.
+
[literal,subs="+quotes,attributes"]
----
$ ./FILEPATH/FILENAME.sh
----

[WARNING]
====
Use password management that is consistent with networking best practices.

* Do not store passwords or credentials in a plain text.
* Keep the token safe against unauthorized use.
====

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/articles/3626371[Getting started with Red Hat APIs]

