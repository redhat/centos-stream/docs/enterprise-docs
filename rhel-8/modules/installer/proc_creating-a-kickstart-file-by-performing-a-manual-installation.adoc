:_mod-docs-content-type: PROCEDURE
[id="creating-a-kickstart-file-by-performing-a-manual-installation_{context}"]
= Creating a Kickstart file by performing a manual installation

[role="_abstract"]
The recommended approach to creating Kickstart files is to use the file created by a manual installation of {RHEL}. After an installation completes, all choices made during the installation are saved into a Kickstart file named [filename]`anaconda-ks.cfg`, located in the [filename]`/root/` directory on the installed system. You can use this file to reproduce the installation in the same way as before. Alternatively, copy this file, make any changes you need, and use the resulting configuration file for further installations.


.Procedure

. Install RHEL. For more details, see link:documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html-single/interactively_installing_rhel_from_installation_media/index[Interactively installing RHEL from installation media].
+
During the installation, create a user with administrator privileges.

. Finish the installation and reboot into the installed system.

. Log into the system with the administrator account.

. Copy the file [filename]`/root/anaconda-ks.cfg` to a location of your choice. The file contains information about users and passwords.
* To display the file contents in terminal:
+
----
# cat /root/anaconda-ks.cfg
----
+
You can copy the output and save to another file of your choice.

* To copy the file to another location, use the file manager. Remember to change permissions on the copy, so that the file can be read by non-root users.

. Install the [package]*pykickstart* package.
+
[subs="+attributes"]
----
# {PackageManagerCommand} install pykickstart
----

. Run [command]`ksvalidator` on your Kickstart file.
+
[subs="quotes,attributes"]
----
$ ksvalidator -v {ProductShortName}{ProductNumber} __/path/to/kickstart.ks__
----
+
Replace __/path/to/kickstart.ks__ with the path to the Kickstart file you want to verify.

[IMPORTANT]
The validation tool cannot guarantee the installation will be successful. It ensures only that the syntax is correct and that the file does not include deprecated options. It does not attempt to validate the `%pre`, `%post` and `%packages` sections of the Kickstart file.