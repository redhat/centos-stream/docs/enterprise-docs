:_mod-docs-content-type: CONCEPT

[id="system_purposes-adoc_{context}"]
= Introduction to System Purpose


[role="_abstract"]
You use system purpose to record the intended use of a Red Hat Enterprise Linux (RHEL) system. Setting system purpose  allows you to specify system attributes, such as the role, service level agreement, and usage. The following values are available for each system purpose attribute by default.

* *Role*
** Red Hat Enterprise Linux Server
** Red Hat Enterprise Linux Workstation
** Red Hat Enterprise Linux Compute Node

* *Service Level Agreement*
** Premium
** Standard
** Self-Support

* *Usage*
** Production
** Development/Test
** Disaster Recovery

Configuring system purpose offers the following benefits:

* In-depth system-level information for system administrators and business operations.
* Reduced overhead when determining why a system was procured and its intended purpose.
* Automated discovery and reconciliation of system usage.

You can set system purpose data in any of the following ways:

* During activation key creation
* During image creation
* During a GUI installation when using the *Connect to Red Hat* screen to register your system
* During a Kickstart installation when using the `syspurpose` Kickstart command
* After installation using the `subscription-manager` command-line (CLI) tool


[role="_additional-resources"]
.Additional resources
* To configure system purpose with an activation key, see link:https://access.redhat.com/documentation/en-us/subscription_central/2023/html/getting_started_with_activation_keys_on_the_hybrid_cloud_console/assembly-creating-managing-activation-keys#proc-creating-act-keys-console_[Creating an activation key].

* To configure system purpose for RHEL 9 with Subscription Manager, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/interactively_installing_rhel_over_the_network/index#proc_configuring-system-purpose-using-the-subscription-manager-command-line-tool_rhel-installer[Configuring System Purpose using the subscription-manager command-line tool].