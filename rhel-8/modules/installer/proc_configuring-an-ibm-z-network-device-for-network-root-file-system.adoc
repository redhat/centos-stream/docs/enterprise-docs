:_mod-docs-content-type: PROCEDURE
[id="configuring-an-ibm-z-network-device-for-network-root-file-system_{context}"]
= Configuring an 64-bit IBM Z network device for network root file system

[role="_abstract"]
To add a network device that is required to access the root file system, you only have to change the boot options. The boot options can be in a parameter file, however, the `/etc/zipl.conf` file no longer contains specifications of the boot records. The file that needs to be modified can be located using the following commands:

[literal,subs="+quotes,attributes,verbatim"]
....
# machine_id=$(cat /etc/machine-id)
# kernel_version=$(uname -r)
# ls /boot/loader/entries/$machine_id-$kernel_version.conf
....

//(see <<chap-parameter-configuration-files-s390>> for more information.)

[application]*Dracut*, the [application]*mkinitrd* successor that provides the functionality in the initramfs that in turn replaces [application]*initrd*, provides a boot parameter to activate network devices on 64-bit IBM{nbsp}Z early in the boot process: `rd.znet=`.

As input, this parameter takes a comma-separated list of the `NETTYPE` (qeth, lcs, ctc), two (lcs, ctc) or three (qeth) device bus IDs, and optional additional parameters consisting of key-value pairs corresponding to network device sysfs attributes. This parameter configures and activates the 64-bit IBM{nbsp}Z network hardware. The configuration of IP addresses and other network specifics works the same as for other platforms. See the [application]*dracut* documentation for more details.

The [application]*cio_ignore* commands for the network channels are handled transparently on boot.

Example boot options for a root file system accessed over the network through NFS:

[literal,subs="+quotes,attributes,verbatim"]
....
root=10.16.105.196:/nfs/nfs_root cio_ignore=all,!condev rd.znet=qeth,0.0.0a00,0.0.0a01,0.0.0a02,layer2=1,portno=0,portname=OSAPORT ip=10.16.105.197:10.16.105.196:10.16.111.254:255.255.248.0:nfs‑server.subdomain.domain:enc9a0:none rd_NO_LUKS rd_NO_LVM rd_NO_MD rd_NO_DM LANG=en_US.UTF-8 SYSFONT=latarcyrheb-sun16 KEYTABLE=us
....
