:_mod-docs-content-type: CONCEPT
[id="booting-the-installation_{context}"]
= Boot media for installing RHEL on 64-bit IBM Z servers

[role="_abstract"]
After establishing a connection with the mainframe, you need to perform an initial program load (IPL), or boot, from the medium containing the installation program. This document describes the most common methods of installing {ProductName} on 64-bit  IBM{nbsp}Z. In general, any method may be used to boot the Linux installation system, which consists of a kernel (`kernel.img`) and initial RAM disk (`initrd.img`) with parameters in the `generic.prm` file supplemented by user defined parameters. Additionally, a `generic.ins` file is loaded which determines file names and memory addresses for the initrd, kernel and `generic.prm`.

The Linux installation system is also called the _installation program_ in this book.

You can use the following boot media only if Linux is to run as a guest operating system under z/VM:

* z/VM reader

You can use the following boot media only if Linux is to run in LPAR mode:

* SE or HMC through a remote SFTP, FTPS or FTP server

* SE or HMC DVD

You can use the following boot media for both z/VM and LPAR:

* DASD

* SCSI disk device that is attached through an FCP channel

ifeval::[{ProductNumber} == 8]
* FCP-attached SCSI DVD
endif::[]

If you use DASD or an FCP-attached SCSI disk device as boot media, you must have a configured `zipl` boot loader.
