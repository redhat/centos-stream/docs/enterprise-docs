////
This module is included in the following assembly: assembly_using-the-rhc-system-role-to-register-the-system.adoc
////
:_mod-docs-content-type: PROCEDURE

[id="configuring-auto-updates-insights-using-rhc-system-role_{context}"]
= Managing auto updates of Insights rules by using the `rhc` RHEL system role

You can enable or disable the automatic collection rule updates for Red Hat Insights by using the `rhc` {RHELSystemRoles}. By default, when you connect your system to Red Hat Insights, this option is enabled. You can disable it by using `rhc`. 

[WARNING]
====
If you disable this feature, you risk using outdated rule definition files and not getting the most recent validation updates. 
====

.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]
// The following are prerequisites that are specific for this task:
* You have registered the system.

.Procedure

. Store your sensitive variables in an encrypted file:

.. Create the vault:
+
[subs="+quotes"]
....
$ *ansible-vault create vault.yml*
New Vault password: _<password>_
Confirm New Vault password: _<vault_password>_
....

.. After the `ansible-vault create` command opens an editor, enter the sensitive data in the `_<key>_: _<value>_` format:
+
[source,ini,subs="+quotes"]
....
username: _<username>_
password: _<password>_
....

.. Save the changes, and close the editor. Ansible encrypts the data in the vault.

. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Managing systems with the rhc RHEL system role
  hosts: managed-node-01.example.com
  vars_files:
    - vault.yml
  tasks:
    - name: Enable Red Hat Insights autoupdates 
        ansible.builtin.include_role:
          name: rhel-system-roles.rhc
        vars:
          rhc_auth:
            login:
              username: "{{ username }}"
              password: "{{ password }}"
          rhc_insights:
            autoupdate: true
            state: present
....
+
The settings specified in the example playbook include the following:

`autoupdate: _true|false_`:: Enables or disables the automatic collection rule updates for Red Hat Insights.

. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check --ask-vault-pass ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.

. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass ~/playbook.yml*
....

[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.rhc/README.md` file
* `/usr/share/doc/rhel-system-roles/rhc/` directory
ifdef::system-roles-ansible[]
* xref:ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible Vault]
endif::[]
ifndef::system-roles-ansible[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/index#ansible-vault[Ansible Vault]
endif::[]