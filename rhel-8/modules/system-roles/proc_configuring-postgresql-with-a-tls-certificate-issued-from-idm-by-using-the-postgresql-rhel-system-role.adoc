:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="configuring-postgresql-with-a-tls-certificate-issued-from-idm-by-using-the-postgresql-rhel-system-role_{context}"]
= Configuring PostgreSQL with a TLS certificate issued from IdM by using the `postgresql` RHEL system role
If your application requires a PostgreSQL database server, you can configure the PostgreSQL service with TLS encryption to enable secure communication between the application and the database. If the PostgreSQL host is a member of a {RHEL} Identity Management (IdM) domain, the `certmonger` service can manage the certificate request and future renewals.

By using the `postgresql` RHEL system role, you can automate this process. You can remotely install and configure PostgreSQL with TLS encryption, and the `postgresql` role uses the `certificate` RHEL system role to configure `certmonger` and request a certificate from IdM.

[NOTE]
====
The `postgresql` role cannot open ports in the `firewalld` service. To allow remote access to the PostgreSQL server, add a task to your playbook that uses the `firewall` RHEL system role.
====


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]
+
* You enrolled the managed node in an IdM domain.


.Procedure
. Store your sensitive variables in an encrypted file:

.. Create the vault:
+
[subs="+quotes"]
....
$ *ansible-vault create vault.yml*
New Vault password: _<vault_password>_
Confirm New Vault password: _<vault_password>_
....

.. After the `ansible-vault create` command opens an editor, enter the sensitive data in the `_<key>_: _<value>_` format:
+
[source,ini,subs="+quotes"]
....
pwd: _<password>_
....

.. Save the changes, and close the editor. Ansible encrypts the data in the vault.


. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Installing and configuring PostgreSQL
  hosts: managed-node-01.example.com
  vars_files:
    - vault.yml
  tasks:
    - name: PostgreSQL with certificates issued by IdM
      ansible.builtin.include_role:
        name: rhel-system-roles.postgresql
      vars:
        postgresql_version: "16"
        postgresql_password: "{{ pwd }}"
        postgresql_ssl_enable: true
        postgresql_certificates:
          - name: postgresql_cert
            dns: "{{ inventory_hostname }}"
            ca: ipa
            principal: "postgresql/{{ inventory_hostname }}@EXAMPLE.COM"
        postgresql_server_conf:
          listen_addresses: "'*'"
          password_encryption: scram-sha-256
        postgresql_pg_hba_conf:
          - type: local
            database: all
            user: all
            auth_method: scram-sha-256
          - type: hostssl
            database: all
            user: all
            address: '127.0.0.1/32'
            auth_method: scram-sha-256
          - type: hostssl
            database: all
            user: all
            address: '::1/128'
            auth_method: scram-sha-256
          - type: hostssl
            database: all
            user: all
            address: '192.0.2.0/24'
            auth_method: scram-sha-256


    - name: Open the PostgresQL port in firewalld
      ansible.builtin.include_role:
        name: rhel-system-roles.firewall
      vars:
        firewall:
          - service: postgresql
            state: enabled
....
+
The settings specified in the example playbook include the following:
+
--
`postgresql_version: _<version>_`:: Sets the version of PostgreSQL to install. The version you can set depends on the PostgreSQL versions that are available in {ProductName} running on the managed node.
+
You cannot upgrade or downgrade PostgreSQL by changing the `postgresql_version` variable and running the playbook again.

`postgresql_password: _<password>_`:: Sets the password of the `postgres` database superuser.
+
You cannot change the password by changing the `postgresql_password` variable and running the playbook again.

`postgresql_certificates: _<certificate_role_settings>_`:: A list of YAML dictionaries with settings for the `certificate` role.

`postgresql_server_conf: _<list_of_settings>_`:: Defines `postgresql.conf` settings you want the role to set. The role adds these settings to the `/etc/postgresql/system-roles.conf` file and includes this file at the end of `/var/lib/pgsql/data/postgresql.conf`. Consequently, settings from the `postgresql_server_conf` variable override settings in `/var/lib/pgsql/data/postgresql.conf`.
+
Re-running the playbook with different settings in `postgresql_server_conf` overwrites the `/etc/postgresql/system-roles.conf` file with the new settings.

`postgresql_pg_hba_conf: _<list_of_authentication_entries>_`:: Configures client authentication entries in the `/var/lib/pgsql/data/pg_hba.conf` file. For details, see see the PostgreSQL documentation.
+
The example allows the following connections to PostgreSQL:
+
* Unencrypted connections by using local UNIX domain sockets.
* TLS-encrypted connections to the IPv4 and IPv6 localhost addresses.
* TLS-encrypted connections from the 192.0.2.0/24 subnet. Note that access from remote addresses is only possible if you also configure the `listen_addresses` setting in the `postgresql_server_conf` variable appropriately.

+
Re-running the playbook with different settings in `postgresql_pg_hba_conf` overwrites the `/var/lib/pgsql/data/pg_hba.conf` file with the new settings.
--
+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.postgresql/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass ~/playbook.yml*
....


.Verification

* Use the `postgres` super user to connect to a PostgreSQL server and execute the `\conninfo` meta command:
+
[subs="+quotes"]
....
# *psql "postgresql://postgres@managed-node-01.example.com:5432" -c '\conninfo'*
Password for user postgres:
You are connected to database "postgres" as user "postgres" on host "192.0.2.1" at port "5432".
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, compression: off)
....
+
If the output displays a TLS protocol version and cipher details, the connection works and TLS encryption is enabled.



[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.postgresql/README.md` file
* `/usr/share/doc/rhel-system-roles/postgresql/` directory
ifdef::system-roles-ansible[]
* xref:ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]
endif::[]
ifndef::system-roles-ansible[]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]
endif::[]

