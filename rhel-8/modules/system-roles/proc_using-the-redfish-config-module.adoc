:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="proc_using-the-redfish-config-module_{context}"]
= Using the `redfish_config` module
The following example shows how to use the `redfish_config` module in a playbook to configure a system to boot with UEFI. For simplicity, the example uses the same host as the Ansible control host and managed host, thus executing the modules on the same host where the playbook is executed.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]
+
* The `ansible-collection-redhat-rhel_mgmt` package is installed.
* The `python3-pyghmi` package is installed either on the control node or the managed nodes.
* OOB controller access details.


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Manages out-of-band controllers using Redfish APIs
  hosts: managed-node-01.example.com
  tasks:
    - name: Set BootMode to UEFI
      redhat.rhel_mgmt.redfish_config:
        baseuri: "_<URI>_"
        username: "_<username>_"
        password: "_<password>_"
	category: Systems
        command: SetBiosAttributes
        bios_attributes:
          BootMode: Uefi
....


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification
* The system boot mode is set to UEFI.


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/collections/ansible_collections/redhat/rhel_mgmt/README.md` file

