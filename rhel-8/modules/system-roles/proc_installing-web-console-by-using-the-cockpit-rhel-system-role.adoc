////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

[id="proc_installing-web-console-by-using-the-cockpit-rhel-system-role_{context}"]
= Installing web console by using the `cockpit` RHEL system role
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
Follow the below steps to install web console in your system and make the services accessible in it.

.Prerequisites

* Access and permissions to one or more _managed nodes_, which are systems you want to configure with the `vpn` system role.

* Access and permissions to a _control node_, which is a system from which Red Hat Ansible Core configures other systems.
+
On the control node:

** The `ansible-core` and `rhel-system-roles` packages are installed.

** An inventory file which lists the managed nodes.


.Procedure

. Create a new `_playbook.yml_` file with the following content:
+
[subs="quotes"]
----
---
- hosts: managed-node-01.example.com
  tasks:
    - name: Install RHEL web console
      ansible.builtin.include_role:
        name: rhel-system-roles.cockpit
      vars:
        cockpit_packages: default
        #cockpit_packages: minimal
        #cockpit_packages: full

    - name: Configure Firewall for web console
      ansible.builtin.include_role:
        name: rhel-system-roles.firewall
      vars:
        firewall:
          service: cockpit
          state: enabled
----
+
NOTE: The cockpit port is open by default in firewalld, so the "Configure Firewall for web console" task only applies if the system administrator customized this.

. Optional: Verify playbook syntax.
+
[subs="quotes"]
----
# *ansible-playbook --syntax-check -i _inventory_file_ _playbook.yml_*
----

. Run the playbook on your inventory file:
+
[subs="quotes"]
----
# *ansible-playbook -i _inventory_file_ _/path/to/file/playbook.yml_*
----

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_systems_using_the_rhel_8_web_console/getting-started-with-the-rhel-8-web-console_system-management-using-the-rhel-8-web-console#installing-the-web-console_getting-started-with-the-rhel-8-web-console[Installing and enabling the web console]
