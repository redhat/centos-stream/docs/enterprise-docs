:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="proc_example-using-the-ipmi_power-module_{context}"]
= Using the `ipmi_power` module
This example shows how to use the `ipmi_boot` module in a playbook to check if the system is turned on. For simplicity, the examples use the same host as the Ansible control host and managed host, thus executing the modules on the same host where the playbook is executed.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]
+
* The `ansible-collection-redhat-rhel_mgmt` package is installed.
* The `python3-pyghmi` package is installed either on the control node or the managed nodes.
* The IPMI BMC that you want to control is accessible over network from the control node or the managed host (if not using `localhost` as the managed host). Note that the host whose BMC is being configured by the module is generally different from the managed host, as the module contacts the BMC over the network using the IPMI protocol.
* You have credentials to access BMC with an appropriate level of access.


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Power management
  hosts: managed-node-01.example.com
  tasks:
    - name: Ensure machine is powered on
      redhat.rhel_mgmt.ipmi_power:
        user: _<admin_user>_
        password: _<password>_
        state: on
....


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification
* When you run the playbook, Ansible returns `true`.


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/collections/ansible_collections/redhat/rhel_mgmt/README.md` file


