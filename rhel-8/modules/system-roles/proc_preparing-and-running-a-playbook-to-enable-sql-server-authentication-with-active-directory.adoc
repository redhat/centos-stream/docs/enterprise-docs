:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE

[id="preparing-and-running-a-playbook-to-enable-sql-server-authentication-with-active-directory_{context}"]
= Installing and configuring SQL Server with AD integration by using the `microsoft.sql.server` Ansible system role
You can integrate Microsoft SQL Server into an Active Directory (AD) to enable AD users to authenticate to SQL Server. By using the `microsoft.sql.server` Ansible system role, you can automate this process and remotely install and configure SQL Server accordingly. Note that you must still perform manual steps in AD and SQL Server after you run the playbook.

Depending on the RHEL version on the managed host, the version of SQL Server that you can install differs:

* RHEL 7.9: SQL Server 2017 and 2019
* RHEL 8: SQL Server 2017, 2019, and 2022
* RHEL 9.4 and later: SQL Server 2022


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]
+
* You installed the `ansible-collection-microsoft-sql` package or the link:https://console.redhat.com/ansible/automation-hub/repo/published/microsoft/sql[`microsoft.sql`] collection on the control node.
* The managed node has 2 GB or more RAM installed.
* The managed node uses one of the following versions: RHEL 7.9, RHEL 8, RHEL 9.4 or later.
* An AD domain is available in the network.
* A reverse DNS (RDNS) zone exists in AD, and it contains Pointer (PTR) resource records for each AD domain controller (DC).
* The managed host's network settings use an AD DNS server.
* The managed host can resolve the following DNS entries:
** Both the hostnames and the fully-qualified domain names (FQDNs) of the AD DCs resolve to their IP addresses.
** The IP addresses of the AD DCs resolve to their FQDNs.


.Procedure
. Store your sensitive variables in an encrypted file:

.. Create the vault:
+
[subs="+quotes"]
....
$ *ansible-vault create vault.yml*
New Vault password: _<vault_password>_
Confirm New Vault password: _<vault_password>_
....

.. After the `ansible-vault create` command opens an editor, enter the sensitive data in the `_<key>_: _<value>_` format:
+
[source,ini,subs="+quotes"]
....
sa_pwd: _<sa_password>_
sql_pwd: _<SQL_AD_password>_
ad_admin_pwd: _<AD_admin_password>_
....

.. Save the changes, and close the editor. Ansible encrypts the data in the vault.


. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Installing and configuring Microsoft SQL Server
  hosts: managed-node-01.example.com
  vars_files:
    - vault.yml
  tasks:
    - name: SQL Server with AD authentication
      ansible.builtin.include_role:
        name: microsoft.sql.server
      vars:
        mssql_accept_microsoft_odbc_driver_17_for_sql_server_eula: true
        mssql_accept_microsoft_cli_utilities_for_sql_server_eula: true
        mssql_accept_microsoft_sql_server_standard_eula: true

        mssql_version: 2022
        mssql_password: "{{ sa_pwd }}"
        mssql_edition: Developer
        mssql_tcp_port: 1433
        mssql_manage_firewall: true

        mssql_ad_configure: true
        mssql_ad_join: true
        mssql_ad_sql_user: sqluser
        mssql_ad_sql_password: "{{ sql_pwd }}"
        ad_integration_realm: ad.example.com
        ad_integration_user: Administrator
        ad_integration_password: "{{ ad_admin_pwd }}"
....
The settings specified in the example playbook include the following:
+
`mssql_ad_configure: true`:: Enables authentication against AD.
`mssql_ad_join: true`:: Uses the `ad_integration` RHEL system role to join the managed node to AD. The role uses the settings from the `ad_integration_realm`, `ad_integration_user`, and `ad_integration_password` variables to join the domain.
`mssql_ad_sql_user: _<username>_`:: Sets the name of an AD account that the role should create in AD and SQL Server for administration purposes.
`ad_integration_user: _<AD_user>_`:: Sets the name of an AD user with privileges to join machines to the domain and to create the AD user specified in `mssql_ad_sql_user`.

+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/microsoft.sql-server/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass ~/playbook.yml*
....


. In your AD domain, enable 128 bit and 256 bit Kerberos authentication for the AD SQL user which you specified in the playbook. Use one of the following options:
+
** In the *Active Directory Users and Computers* application:
+
... Navigate to *ad.example.com* > *Users* > *sqluser* > *Accounts*.
... In the *Account options* list, select *This account supports Kerberos AES 128 bit encryption* and *This account supports Kerberos AES 256 bit encryption*.
... Click *Apply*.
** In PowerShell in admin mode, enter:
+
[subs="+quotes"]
....
C:\> *Set-ADUser -Identity `sqluser` -KerberosEncryptionType AES128,AES256*
....


. Authorize AD users that should be able to authenticate to SQL Server. On the SQL Server, perform the following steps:

.. Obtain a Kerberos ticket for the `Administrator` user:
+
[subs="+quotes"]
....
$ *kinit Administrator@ad.example.com*
....

.. Authorize an AD user:
+
[literal,subs="+quotes"]
....
$ */opt/mssql-tools/bin/sqlcmd -S. -Q 'CREATE LOGIN [AD\<AD_user>] FROM WINDOWS;'*
....
+
Repeat this step for every AD user who should be able to access SQL Server.


.Verification

* On the managed node that runs SQL Server:

.. Obtain a Kerberos ticket for an AD user:
+
[subs="+quotes"]
....
$ *kinit _<AD_user>_@ad.example.com*
....

.. Use the `sqlcmd` utility to log in to SQL Server and run a query, for example:
+
[subs="+quotes"]
....
$ */opt/mssql-tools/bin/sqlcmd -S. -Q 'SELECT SYSTEM_USER'*
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/microsoft.sql-server/README.md` file
ifdef::system-roles-ansible[]
* xref:ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]
endif::[]
ifndef::system-roles-ansible[]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]
endif::[]

