:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="installing-and-configuring-sql-server-using-the-microsoft-sql-server-ansible-role-with-existing-certificate-files_{context}"]
= Installing and configuring SQL Server with an existing TLS certificate by using the `microsoft.sql.server` Ansible system role
If your application requires a Microsoft SQL Server database, you can configure SQL Server with TLS encryption to enable secure communication between the application and the database. By using the `microsoft.sql.server` Ansible system role, you can automate this process and remotely install and configure SQL Server with TLS encryption. In the playbook, you can use an existing private key and a TLS certificate that was issued by a certificate authority (CA).

Depending on the RHEL version on the managed host, the version of SQL Server that you can install differs:

* RHEL 7.9: SQL Server 2017 and 2019
* RHEL 8: SQL Server 2017, 2019, and 2022
* RHEL 9.4 and later: SQL Server 2022


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]
+
* You installed the `ansible-collection-microsoft-sql` package or the link:https://console.redhat.com/ansible/automation-hub/repo/published/microsoft/sql[`microsoft.sql`] collection on the control node.
* The managed node has 2 GB or more RAM installed.
* The managed node uses one of the following versions: RHEL 7.9, RHEL 8, RHEL 9.4 or later.
* You stored the certificate in the `sql_crt.pem` file in the same directory as the playbook.
* You stored the private key in the `sql_cert.key` file in the same directory as the playbook.
* SQL clients trust the CA that issued the certificate.



.Procedure
. Store your sensitive variables in an encrypted file:

.. Create the vault:
+
[subs="+quotes"]
....
$ *ansible-vault create vault.yml*
New Vault password: _<vault_password>_
Confirm New Vault password: _<vault_password>_
....

.. After the `ansible-vault create` command opens an editor, enter the sensitive data in the `_<key>_: _<value>_` format:
+
[source,ini,subs="+quotes"]
....
sa_pwd: _<sa_password>_
....

.. Save the changes, and close the editor. Ansible encrypts the data in the vault.


. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Installing and configuring Microsoft SQL Server
  hosts: managed-node-01.example.com
  vars_files:
    - vault.yml
  tasks:
    - name: SQL Server with an existing private key and certificate
      ansible.builtin.include_role:
        name: microsoft.sql.server
      vars:
        mssql_accept_microsoft_odbc_driver_17_for_sql_server_eula: true
        mssql_accept_microsoft_cli_utilities_for_sql_server_eula: true
        mssql_accept_microsoft_sql_server_standard_eula: true

        mssql_version: 2022
        mssql_password: "{{ sa_pwd  }}"
        mssql_edition: Developer
        mssql_tcp_port: 1433
        mssql_manage_firewall: true

        mssql_tls_enable: true
        mssql_tls_cert: sql_crt.pem
        mssql_tls_private_key: sql_cert.key
        mssql_tls_version: 1.2
        mssql_tls_force: true
....
+
The settings specified in the example playbook include the following:
+
`mssql_tls_enable: true`:: Enables TLS encryption. If you enable this setting, you must also define `mssql_tls_cert` and `mssql_tls_private_key`.
`mssql_tls_cert: _<path>_`:: Sets the path to the TLS certificate stored on the control node. The role copies this file to the `/etc/pki/tls/certs/` directory on the managed node.
`mssql_tls_private_key: _<path>_`:: Sets the path to the TLS private key on the control node. The role copies this file to the `/etc/pki/tls/private/` directory on the managed node.
`mssql_tls_force: true`:: Replaces the TLS certificate and private key in their destination directories if they exist.

+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/microsoft.sql-server/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass ~/playbook.yml*
....


.Verification

* On the SQL Server host, use the `sqlcmd` utility with the `-N` parameter to establish an encrypted connection to SQL server and run a query, for example:
+
[subs="+quotes"]
....
$ */opt/mssql-tools/bin/sqlcmd -N -S server.example.com -U "sa" -P _<sa_password>_ -Q 'SELECT SYSTEM_USER'*
....
+
If the command succeeds, the connection to the server was TLS encrypted.


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/microsoft.sql-server/README.md` file
ifdef::system-roles-ansible[]
* xref:ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]
endif::[]
ifndef::system-roles-ansible[]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]
endif::[]

