:_mod-docs-content-type: REFERENCE
[id="compatibility-breaking-changes-in-gcc_{context}"]
= Compatibility-breaking changes in GCC in RHEL 8

[discrete]
== {cpp} ABI change in `std::string` and `std::list`

The Application Binary Interface (ABI) of the `std::string` and `std::list` classes from the `pass:[libstdc++]` library changed between RHEL 7 (GCC 4.8) and RHEL 8 (GCC 8) to conform to the {cpp}11 standard. The `pass:[libstdc++]` library supports both the old and new ABI, but some other {cpp} system libraries do not. As a consequence, applications that dynamically link against these libraries will need to be rebuilt. This affects all {cpp} standard modes, including {cpp}98. It also affects applications built with Red Hat Developer Toolset compilers for RHEL 7, which kept the old ABI to maintain compatibility with the system libraries.
//(BZ#1704867)

[discrete]
== GCC no longer builds Ada, Go, and Objective C/{cpp} code
Capability for building code in the Ada (GNAT), GCC Go, and Objective C/{cpp} languages has been removed from the GCC compiler.

To build Go code, use the Go Toolset instead.
//(BZ#1650618)

