:_mod-docs-content-type: REFERENCE
[id="tools-useful-for-recording-application-interactions_{context}"]
= Tools useful for recording application interactions

ifdef::internal[]
Story: _As a DevOps, I need to understand what tools I have for recording interactions of an application with kernel (?)_ +
SMEs: _fche, pmuldoon_ +
Revised by: _fche_ +
Peer review: _asteflova_ +
Module type: _reference_ +
Components: _strace, ltrace, systemtap, gdb_ +
endif::internal[]


{ProductName} offers multiple tools for analyzing an application's interactions.

strace::
The [command]`strace` tool primarily enables logging of system calls (kernel functions) used by an application.
+
* The [command]`strace` tool can provide a detailed output about calls, because [command]`strace` interprets parameters and results with knowledge of the underlying kernel code. Numbers are turned into the respective constant names, bitwise combined flags expanded to flag list, pointers to character arrays dereferenced to provide the actual string, and more. Support for more recent kernel features may be lacking.
* You can filter the traced calls to reduce the amount of captured data.
* The use of [command]`strace` does not require any particular setup except for setting up the log filter.
* Tracing the application code with [command]`strace` results in significant slowdown of the application's execution. As a result, [command]`strace` is not suitable for many production deployments. As an alternative, consider using [command]`ltrace` or SystemTap.
* The version of [command]`strace` available in {rhdts} can also perform system call tampering. This capability is useful for debugging.

ltrace::
The [command]`ltrace` tool enables logging of an application's user space calls into shared objects (dynamic libraries).
+
--
* The [command]`ltrace` tool enables tracing calls to any library.
* You can filter the traced calls to reduce the amount of captured data.
* The use of [command]`ltrace` does not require any particular setup except for setting up the log filter.
* The [command]`ltrace` tool is lightweight and fast, offering an alternative to [command]`strace`: it is possible to trace the respective interfaces in libraries such as [systemitem]`glibc` with [command]`ltrace` instead of tracing kernel functions with [command]`strace`.
* Because [command]`ltrace` does not handle a known set of calls like [command]`strace`, it does not attempt to explain the values passed to library functions. The [command]`ltrace` output contains only raw numbers and pointers. The interpretation of [command]`ltrace` output requires consulting the actual interface declarations of the libraries present in the output.
--
+
NOTE: In {ProductName} {ProductNumber}, a known issue prevents [command]`ltrace` from tracing system executable files. This limitation does not apply to executable files built by users.

SystemTap::
SystemTap is an instrumentation platform for probing running processes and kernel activity on the Linux system. SystemTap uses its own scripting language for programming custom event handlers.
+
* Compared to using [command]`strace` and [command]`ltrace`, scripting the logging means more work in the initial setup phase. However, the scripting capabilities extend SystemTap's usefulness beyond just producing logs.
* SystemTap works by creating and inserting a kernel module. The use of SystemTap is efficient and does not create a significant slowdown of the system or application execution on its own.
* SystemTap comes with a set of usage examples.

GDB::
The GNU Debugger (GDB) is primarily meant for debugging, not logging. However, some of its features make it useful even in the scenario where an application's interaction is the primary activity of interest.
+
* With GDB, it is possible to conveniently combine the capture of an interaction event with immediate debugging of the subsequent execution path.
* GDB is best suited for analyzing response to infrequent or singular events, after the initial identification of problematic situation by other tools. Using GDB in any scenario with frequent events becomes inefficient or even impossible.

.Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-systemtap_monitoring-and-managing-system-status-and-performance[Getting started with SystemTap]

* link:https://access.redhat.com/documentation/en-us/red_hat_developer_toolset/{dts-ver}/html-single/user_guide/[{dts} User Guide]
