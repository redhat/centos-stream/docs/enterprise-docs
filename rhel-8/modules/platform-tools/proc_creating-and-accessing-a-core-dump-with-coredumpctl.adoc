:_mod-docs-content-type: PROCEDURE
[id="creating-and-accessing-a-core-dump-with-coredumpctl_{context}"]
= Creating and accessing a core dump with coredumpctl

The `coredumpctl` tool of `systemd` can significantly streamline working with core dumps on the machine where the crash happened. This procedure outlines how to capture a core dump of unresponsive process.


.Prerequisites


* The system must be configured to use `systemd-coredump` for core dump handling. To verify this is true:
+
----
$ sysctl kernel.core_pattern
----
+
The configuration is correct if the output starts with the following:
+
----
kernel.core_pattern = |/usr/lib/systemd/systemd-coredump
----


.Procedure


. Find the PID of the hung process, based on a known part of the executable file name:
+
[subs="quotes,attributes"]
----
$ pgrep -a __executable-name-fragment__
----
+
This command will output a line in the form
+
[subs="quotes,attributes"]
----
__PID__ __command-line__
----
+
Use the __command-line__ value to verify that the __PID__ belongs to the intended process.
+
For example:
+
----
$ pgrep -a bc
5459 bc
----

. Send an abort signal to the process:
+
[subs="quotes,attributes"]
----
# kill -ABRT __PID__
----

. Verify that the core has been captured by `coredumpctl`:
+
[subs="quotes,attributes"]
----
$ coredumpctl list __PID__
----
+
For example:
+
----
$ coredumpctl list 5459
TIME                            PID   UID   GID SIG COREFILE  EXE
Thu 2019-11-07 15:14:46 CET    5459  1000  1000   6 present   /usr/bin/bc
----

. Further examine or use the core file as needed.
+
You can specify the core dump by PID and other values. See the _coredumpctl(1)_ manual page for further details.
+
====

* To show details of the core file:
+
[subs="quotes,attributes"]
----
$ coredumpctl info __PID__
----

* To load the core file in the GDB debugger:
+
[subs="quotes,attributes"]
----
$ coredumpctl debug __PID__
----
+
Depending on availability of debugging information, GDB will suggest commands to run, such as:
+
----
Missing separate debuginfos, use: dnf debuginfo-install bc-1.07.1-5.el8.x86_64
----
+
ifdef::devguide[]
For more details on this process, see xref:getting-debuginfo-packages-for-an-application-or-library-using-gdb_enabling-debugging-with-debugging-information[].
endif::[]
ifndef::devguide[]
ifeval::[{ProductNumber} == 8]
For more details on this process, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/developing_c_and_cpp_applications_in_rhel_8/index#getting-debuginfo-packages-for-an-application-or-library-using-gdb_enabling-debugging-with-debugging-information[Getting debuginfo packages for an application or library using GDB].
endif::[]
endif::[]
ifndef::devguide[]
ifeval::[{ProductNumber} == 9]
For more details on this process, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/developing_c_and_cpp_applications_in_rhel_9/debugging-applications_developing-applications#getting-debuginfo-packages-for-an-application-or-library-using-gdb_enabling-debugging-with-debugging-information[Getting debuginfo packages for an application or library using GDB].
endif::[]
endif::[]

* To export the core file for further processing elsewhere:
+
[subs="quotes,attributes"]
----
$ coredumpctl dump __PID__ > __/path/to/file_for_export__
----
+
Replace __/path/to/file_for_export__ with the file where you want to put the core dump.

====
