:_mod-docs-content-type: REFERENCE
[id="debugging-forking-or-threaded-programs-with-gdb_{context}"]
= Debugging forking or threaded programs with GDB

ifdef::internal[]
Story: __As a developer, I need to debug a program that uses fork() or threads__ +
SMEs: _pmuldoon, jkratoch_ +
Revised by: _pmuldoon_ +
Peer review: _asteflova_ +
Module type: _reference_ +
Components: _gdb_ +
endif::internal[]

Some programs use forking or threads to achieve parallel code execution. Debugging multiple simultaneous execution paths requires special considerations.


.Prerequisites

* You must understand the concepts of process forking and threads.


.Debugging forked programs with GDB

Forking is a situation when a program (*parent*) creates an independent copy of itself (*child*). Use the following settings and commands to affect what GDB does when a fork occurs:

* The [option]`follow-fork-mode` setting controls whether GDB follows the parent or the child after the fork.
+
--
[command]`set follow-fork-mode parent`::  After a fork, debug the parent process. This is the default.

[command]`set follow-fork-mode child`::  After a fork, debug the child process.

[command]`show follow-fork-mode`::  Display the current setting of [option]`follow-fork-mode`.
--

* The [command]`set detach-on-fork` setting controls whether the GDB keeps control of the other (not followed) process or leaves it to run.
+
--
[command]`set detach-on-fork on`::  The process which is not followed (depending on the value of [command]`follow-fork-mode`) is detached and runs independently. This is the default.

[command]`set detach-on-fork off`::  GDB keeps control of both processes. The process which is followed (depending on the value of [command]`follow-fork-mode`) is debugged as usual, while the other is suspended.

[command]`show detach-on-fork`::  Display the current setting of [command]`detach-on-fork`.
--
+
//To summarize, the default action is to follow the parent process and let the child process run outside GDB control.

.Debugging Threaded Programs with GDB

GDB has the ability to debug individual threads, and to manipulate and examine them independently. To make GDB stop only the thread that is examined, use the commands [command]`set non-stop on` and [command]`set target-async on`. You can add these commands to the [filename]`.gdbinit` file. After that functionality is turned on, GDB is ready to conduct thread debugging.

GDB uses a concept of _current thread_. By default, commands apply to the current thread only.

[command]`info threads`::  Display a list of threads with their `id` and `gid` numbers, indicating the current thread.

[command]`thread _id_`::  Set the thread with the specified `id` as the current thread.

[command]`thread apply _ids_ _command_`::  Apply the command `_command_` to all threads listed by `_ids_`. The `_ids_` option is a space-separated list of thread ids. A special value `all` applies the command to all threads.

[command]`break _location_ thread _id_ if _condition_`::  Set a breakpoint at a certain `_location_` with a certain `_condition_` only for the thread number `_id_`.

[command]`watch _expression_ thread _id_`::  Set a watchpoint defined by `_expression_` only for the thread number `_id_`.

[command]`command&`::  Execute command `command` and return immediately to the gdb prompt `(gdb)`, continuing any code execution in the background.

[command]`interrupt`::  Halt execution in the background.

[role="_additional-resources"]
.Additional resources
* Debugging with GDB &mdash; link:https://sourceware.org/gdb/onlinedocs/gdb/Threads.html[4.10 Debugging Programs with Multiple Threads]
* Debugging with GDB &mdash; link:https://sourceware.org/gdb/onlinedocs/gdb/Forks.html[4.11 Debugging Forks]
