:_mod-docs-content-type: PROCEDURE
[id="enabling-the-built-by-tool_{context}"]

= Enabling the `built-by` tool

You can use the `annocheck` `built-by` tool to find the name of the compiler that built the binary file.

.Procedure

* To enable the `built-by` tool, use:
+
[subs=+quotes]
----
$ annocheck --enable-built-by
----

.Additional information

* For more information about the `built-by` tool, see the `--help` command-line option.
