:_mod-docs-content-type: REFERENCE
[id="hardening-checker-options_{context}"]

= Hardening checker options

The `annocheck` program checks the following options:

* Lazy binding is disabled using the `-z now` linker option.
* The program does not have a stack in an executable region of memory.
* The relocations for the GOT table are set to read only.
* No program segment has all three of the read, write and execute permission bits set.
* There are no relocations against executable code.
* The runpath information for locating shared libraries at runtime includes only directories rooted at /usr.
* The program was compiled with `annobin` notes enabled.
* The program was compiled with the `-fstack-protector-strong` option enabled.
* The program was compiled with `-D_FORTIFY_SOURCE=2`.
* The program was compiled with `-D_GLIBCXX_ASSERTIONS`.
* The program was compiled with `-fexceptions` enabled.
* The program was compiled with `-fstack-clash-protection` enabled.
* The program was compiled at `-O2` or higher.
* The program does not have any relocations held in a writeable.
* Dynamic executables have a dynamic segment.
* Shared libraries were compiled with `-fPIC` or `-fPIE`.
* Dynamic executables were compiled with `-fPIE` and linked with `-pie`.
* If available, the `-fcf-protection=full` option was used.
* If available, the `-mbranch-protection` option was used.
* If available, the `-mstackrealign` option was used.
