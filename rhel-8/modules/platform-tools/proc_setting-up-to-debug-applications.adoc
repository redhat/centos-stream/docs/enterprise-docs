:_mod-docs-content-type: PROCEDURE
[id="setting-up-to-debug-applications_{context}"]
= Setting up to debug applications

ifdef::internal[]
Story: _As a dev, I want to set up my RHEL for debugging_ +
SMEs: _mpolacek?_ +
Revised by: - +
Peer review: - +
Module type: _procedure_ +
Components: _gdb, valgrind, systemtap, strace, ltrace, memstomp, devtoolset_ +
endif::internal[]

{ProductName} offers multiple debugging and instrumentation tools to analyze and troubleshoot internal application behavior.


.Prerequisites

* The debug and source repositories must be enabled.


.Procedure

. Install the tools useful for debugging:
+
[subs="quotes,attributes"]
----
# {PackageManagerCommand} install gdb valgrind systemtap ltrace strace
----

. Install the [package]*{PackageManagerCommand}-utils* package in order to use the [command]`debuginfo-install` tool:
+
[subs="quotes,attributes"]
----
# {PackageManagerCommand} install {PackageManagerCommand}-utils
----

. Run a SystemTap helper script for setting up the environment.
+
[subs="quotes,attributes"]
----
# stap-prep
----
+
Note that [application]*stap-prep* installs packages relevant to the currently _running_ kernel, which might not be the same as the actually installed kernel(s). To ensure [application]*stap-prep* installs the correct [package]*kernel-debuginfo* and [package]*kernel-headers* packages, double-check the current kernel version by using the [command]`uname -r` command and reboot your system if necessary.

. Make sure `SELinux` policies allow the relevant applications to run not only normally, but in the debugging situations, too. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/[Using SELinux].

//. To capture kernel dumps, install and configure [systemitem]*kdump*. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_system_administration/[Configuring and managing system administration].
// kernel != applications

ifdef::devguide[]


.Additional resources

* xref:enabling-debugging-with-debugging-information_debugging-applications[]

endif::[]
