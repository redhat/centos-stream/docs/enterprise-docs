:_mod-docs-content-type: PROCEDURE
[id="removing-redundant-annobin-notes_{context}"]

= Removing redundant annobin notes

Using `annobin` increases the size of binaries. To reduce the size of the binaries compiled with `annobin` you can remove redundant `annobin` notes. To remove the redundant `annobin` notes use the `objcopy` program, which is a part of the `binutils` package.

.Procedure

* To remove the redundant `annobin` notes, use:
+
[subs=+quotes]
----
  $ objcopy --merge-notes _file-name_
----
Replace _file-name_ with the name of the file.
