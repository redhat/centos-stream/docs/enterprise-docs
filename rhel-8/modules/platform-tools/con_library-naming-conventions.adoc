:_mod-docs-content-type: CONCEPT
[id="library-naming-conventions_{context}"]
= Library naming conventions

// conceptual

ifdef::internal[]
Story: _As a developer, I need to know what is the file that contains the library with given name_ +
SMEs: _ptisnovs, mpolacek_ +
Revised by: _ptisnovs_ +
Peer review: _bfallonf_ +
Module type: _concept_ +
Components: _gcc_ +
endif::internal[]


A special file name convention is used for libraries: a library known as *foo* is expected to exist as file [filename]`lib__foo__.so` or [filename]`lib__foo__.a`. This convention is automatically understood by the linking input options of GCC, but not by the output options:

* When linking against the library, the library can be specified only by its name *foo* with the [option]`-l` option as [option]`-l__foo__`:
+
[subs="quotes,macros"]
----
$ gcc ... -l__foo__ ...
----

* When creating the library, the full file name [filename]`lib__foo__.so` or [filename]`lib__foo__.a` must be specified.

ifdef::devguide[]

.Additional resources

* xref:the-soname-mechanism_creating-libraries-with-gcc[]

endif::[]
