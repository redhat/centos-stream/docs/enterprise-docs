:_mod-docs-content-type: PROCEDURE
[id="example-building-a-c-program-with-gcc_{context}"]
= Example: Building a C program with GCC (compiling and linking in one step)

ifdef::internal[]
Story: _I want an example of using GCC to build a C program_ +
SMEs: _mpolacek, ptisnovs_ +
Revised by: - +
Peer review: - +
Module type: _procedure_ +
Components: _gcc_ +
endif::internal[]

This example shows the exact steps to build a simple sample C program.

In this example, compiling and linking the code is done in one step.

.Prerequisites

* You must understand how to use GCC.


.Procedure

. Create a directory [filename]`hello-c` and change to it:
+
----
$ mkdir hello-c
$ cd hello-c
----

. Create file [filename]`hello.c` with the following contents:
+
[source,c]
----
#include <stdio.h>

int main() {
  printf("Hello, World!\n");
  return 0;
}
----

. Compile and link the code with GCC:
+
----
$ gcc hello.c -o helloworld
----
+
This compiles the code, creates the object file `hello.o`, and links the executable file [filename]`helloworld` from the object file.

. Run the resulting executable file:
+
----
$ ./helloworld
Hello, World!
----

////
. Optional: Remove all the files again:
+
----
$ cd ..
$ rm -r hello-c
----
////

ifdef::devguide[]

.Additional resources

* xref:example-building-a-c-program-using-a-makefile_managing-more-code-with-make[]

endif::[]
