:_mod-docs-content-type: REFERENCE
[id="cpp-compatibility-in-gcc-toolset-13_{context}"]
= {cpp} compatibility in {gcct} 13

IMPORTANT: The compatibility information presented here apply only to the GCC from {gcct} 13.

The GCC compiler in {gcct} can use the following {cpp} standards:

{cpp}14::
This language standard is available in {gcct} 13.
+
Using the {cpp}14 language version is supported when all {cpp} objects compiled with the respective flag have been built using GCC version 6 or later.

{cpp}11::
This language standard is available in {gcct} 13.
+
Using the {cpp}11 language version is supported when all {cpp} objects compiled with the respective flag have been built using GCC version 5 or later.

{cpp}98::
This language standard is available in {gcct} 13. Binaries, shared libraries and objects built using this standard can be freely mixed regardless of being built with GCC from {gcct}, Red Hat Developer Toolset, and RHEL 5, 6, 7 and 8.

{cpp}17::
This language standard is available in {gcct} 13.
+
This is the default language standard setting for {gcct} 13, with GNU extensions, equivalent to explicitly using option `-std=gnu++17`.
+
Using the {cpp}17 language version is supported when all {cpp} objects compiled with the respective flag have been built using GCC version 10 or later.

{cpp}20 and {cpp}23::
These language standards are available in {gcct} 13 only as an experimental, unstable, and unsupported capability. Additionally, compatibility of objects, binary files, and libraries built using this standard cannot be guaranteed.
+
To enable the {cpp}20 standard, add the command-line option `pass:c[-std=c++20]` to your g++ command line.
+
To enable the {cpp}23 standard, add the command-line option `pass:c[-std=c++23]` to your g++ command line.

All of the language standards are available in both the standard compliant variant or with GNU extensions.

When mixing objects built with {gcct} with those built with the RHEL toolchain (particularly `.o` or `.a` files), {gcct} toolchain should be used for any linkage. This ensures any newer library features provided only by {gcct} are resolved at link time.

