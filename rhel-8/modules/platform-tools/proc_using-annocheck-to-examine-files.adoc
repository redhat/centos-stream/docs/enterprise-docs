:_mod-docs-content-type: PROCEDURE
[id="using-annockeck-to-examine-files_{context}"]

= Using annocheck to examine files

The following section describes how to examine ELF files using `annocheck`.

.Procedure

* To examine a file, use:
+
[subs=+quotes]
----
$ annocheck _file-name_
----
Replace _file-name_ with the name of a file.

NOTE: The files must be in ELF format. `annocheck` does not handle any other binary file types. `annocheck` processes static libraries that contain ELF object files.

.Additional information

* For more information about `annocheck` and possible command line options, see the `annocheck` man page on your system.
