:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-response-policy-zones-in-bind-to-override-dns-records_{context}"]
= Configuring response policy zones in BIND to override DNS records
Using DNS blocking and filtering, administrators can rewrite a DNS response to block access to certain domains or hosts. In BIND, response policy zones (RPZs) provide this feature. You can configure different actions for blocked entries, such as returning an `NXDOMAIN` error or not responding to the query.

If you have multiple DNS servers in your environment, use this procedure to configure the RPZ on the primary server, and later configure zone transfers to make the RPZ available on your secondary servers.


.Prerequisites

* BIND is already configured, for example, as a caching name server.
* The `named` or `named-chroot` service is running.


.Procedure

. Edit the `/etc/named.conf` file, and make the following changes:

.. Add a `response-policy` definition to the `options` statement:
+
[literal,subs="+quotes"]
....
options {
    ...

    **response-policy {**
        **zone "__rpz.local__";**
    **};**

    ...
}
....
+
You can set a custom name for the RPZ in the `zone` statement in `response-policy`. However, you must use the same name in the zone definition in the next step.

.. Add a `zone` definition for the RPZ you set in the previous step:
+
[literal,subs="+quotes"]
....
**zone "rpz.local" {**
    **type master;**
    **file "rpz.local";**
    **allow-query { __localhost; 192.0.2.0/24; 2001:db8:1::/64;__ };**
    **allow-transfer { none; };**
**};**
....
+
These settings state:
+
* This server is the primary server (`type master`) for the RPZ named `rpz.local`.
* The `/var/named/rpz.local` file is the zone file. If you set a relative path, as in this example, this path is relative to the directory you set in `directory` in the `options` statement.
* Any hosts defined in `allow-query` can query this RPZ. Alternatively, specify IP ranges or BIND access control list (ACL) nicknames to limit the access.
* No host can transfer the zone. Allow zone transfers only when you set up secondary servers and only for the IP addresses of the secondary servers.

. Verify the syntax of the `/etc/named.conf` file:
+
[literal,subs="+quotes"]
....
# **named-checkconf**
....
+
If the command displays no output, the syntax is correct.

. Create the `/var/named/rpz.local` file, for example, with the following content:
+
[literal,subs="+quotes,macros"]
....
**$TTL __10m__**
**@ IN SOA __ns1.example.com.__ __hostmaster.example.com.__ (**
                          **__2022070601__** **; serial number**
                          **__1h__**         **; refresh period**
                          **__1m__**         **; retry period**
                          **__3d__**         **; expire time**
                          **__1m__** )       **; minimum TTL**

                 **IN NS**    **ns1.example.com.**

**example.org**      **IN CNAME** **.**
**pass:[*].example.org**    **IN CNAME** **.**
**example.net**      **IN CNAME** **rpz-drop.**
**pass:[*].example.net**    **IN CNAME** **rpz-drop.**

....
+
This zone file:
+
* Sets the default time-to-live (TTL) value for resource records to 10 minutes. Without a time suffix, such as `h` for hour, BIND interprets the value as seconds.
* Contains the required start of authority (SOA) resource record with details about the zone.
* Sets `ns1.example.com` as an authoritative DNS server for this zone. To be functional, a zone requires at least one name server (`NS`) record. However, to be compliant with RFC 1912, you require at least two name servers.
* Return an `NXDOMAIN` error for queries to `example.org` and hosts in this domain.
* Drop queries to `example.net` and hosts in this domain.

+
For a full list of actions and examples, see link:https://tools.ietf.org/id/draft-vixie-dnsop-dns-rpz-00.html[IETF draft: DNS Response Policy Zones (RPZ)].

. Verify the syntax of the `/var/named/rpz.local` file:
+
[literal,subs="+quotes"]
....
# **named-checkzone __rpz.local__ __/var/named/rpz.local__**
zone __rpz.local/IN__: loaded serial __2022070601__
OK
....

. Reload BIND:
+
[literal,subs="+quotes"]
....
# **systemctl reload named**
....
+
If you run BIND in a change-root environment, use the `systemctl reload named-chroot` command to reload the service.


.Verification

. Attempt to resolve a host in `example.org`, that is configured in the RPZ to return an `NXDOMAIN` error:
+
[literal,subs="+quotes"]
....
# **dig @localhost www.example.org**
...
;; ->>HEADER<<- opcode: QUERY, status: **NXDOMAIN**, id: 30286
...
....
+
This example assumes that BIND runs on the same host and responds to queries on the `localhost` interface.

. Attempt to resolve a host in the `example.net` domain, that is configured in the RPZ to drop queries:
+
[literal,subs="+quotes"]
....
# **dig @localhost www.example.net**
...
;; connection timed out; no servers could be reached
...
....


[role="_additional-resources"]
.Additional resources

* link:https://tools.ietf.org/id/draft-vixie-dnsop-dns-rpz-00.html[IETF draft: DNS Response Policy Zones (RPZ)]


