:_mod-docs-content-type: CONCEPT

[id="dhcp-transaction-phases_{context}"]
= DHCP transaction phases

[role="_abstract"]
The DHCP works in four phases: Discovery, Offer, Request, Acknowledgement, also called the DORA process. DHCP uses this process to provide IP addresses to clients.

Discovery:: The DHCP client sends a message to discover the DHCP server in the network. This message is broadcasted at the network and data link layer.

Offer:: The DHCP server receives messages from the client and offers an IP address to the DHCP client. This message is unicast at the data link layer but broadcast at the network layer.

Request:: The DHCP client requests the DHCP server for the offered IP address. This message is unicast at the data link layer but broadcast at the network layer.

Acknowledgment:: The DHCP server sends an acknowledgment to the DHCP client. This message is unicast at the data link layer but broadcast at the network layer. It is the final message of the DHCP DORA process.

