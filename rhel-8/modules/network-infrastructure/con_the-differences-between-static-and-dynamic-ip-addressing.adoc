:_mod-docs-content-type: CONCEPT

[id="the-differences-between-static-and-dynamic-ip-addressing_{context}"]
= The difference between static and dynamic IP addressing

[role="_abstract"]
Static IP addressing::
+
When you assign a static IP address to a device, the address does not change over time unless you change it manually. Use static IP addressing if you want:
+
** To ensure network address consistency for servers such as DNS, and authentication servers.
+
** To use out-of-band management devices that work independently of other network infrastructure.

Dynamic IP addressing::
+
When you configure a device to use a dynamic IP address, the address can change over time. For this reason, dynamic addresses are typically used for devices that connect to the network occasionally because the IP address can be different after rebooting the host.
+
Dynamic IP addresses are more flexible, easier to set up, and administer. The Dynamic Host Control Protocol (DHCP) is a traditional method of dynamically assigning network configurations to hosts.

[NOTE]
====
There is no strict rule defining when to use static or dynamic IP addresses. It depends on user's needs, preferences, and the network environment.
====

