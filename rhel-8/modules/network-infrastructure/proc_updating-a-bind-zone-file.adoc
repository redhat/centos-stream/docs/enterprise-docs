:_mod-docs-content-type: PROCEDURE

[id="proc_updating-a-bind-zone-file_{context}"]
= Updating a BIND zone file
In certain situations, for example if an IP address of a server changes, you must update a zone file. If multiple DNS servers are responsible for a zone, perform this procedure only on the primary server. Other DNS servers that store a copy of the zone will receive the update through a zone transfer.


.Prerequisites

* The zone is configured.
* The `named` or `named-chroot` service is running.


.Procedure

. Optional: Identify the path to the zone file in the `/etc/named.conf` file:
+
[literal,subs="+quotes"]
....
options {
    ...
    directory       "__/var/named__";
}

zone "__example.com__" {
    ...
    file "__example.com.zone__";
};
....
+
You find the path to the zone file in the `file` statement in the zone's definition. A relative path is relative to the directory set in `directory` in the `options` statement.

. Edit the zone file:

.. Make the required changes.

.. Increment the serial number in the start of authority (SOA) record.
+
[IMPORTANT]
====
If the serial number is equal to or lower than the previous value, secondary servers will not update their copy of the zone.
====

. Verify the syntax of the zone file:
+
[literal,subs="+quotes"]
....
# **named-checkzone __example.com__ __/var/named/example.com.zone__**
zone __example.com/IN__: loaded serial __2022062802__
OK
....

. Reload BIND:
+
[literal,subs="+quotes"]
....
# **systemctl reload named**
....
+
If you run BIND in a change-root environment, use the `systemctl reload named-chroot` command to reload the service.


.Verification

* Query the record you have added, modified, or removed, for example:
+
[literal,subs="+quotes"]
....
# **dig +short @__localhost__ __A__ __ns2.example.com__**
192.0.2.2
....
+
This example assumes that BIND runs on the same host and responds to queries on the `localhost` interface.


[role="_additional-resources"]
.Additional resources
* xref:ref_the-soa-record-in-zone-files_assembly_configuring-zones-on-a-bind-dns-server[The SOA record in zone files]
* xref:proc_setting-up-a-forward-zone-on-a-bind-primary-server_assembly_configuring-zones-on-a-bind-dns-server[Setting up a forward zone on a BIND primary server]
* xref:proc_setting-up-a-reverse-zone-on-a-bind-primary-server_assembly_configuring-zones-on-a-bind-dns-server[Setting up a reverse zone on a BIND primary server]

