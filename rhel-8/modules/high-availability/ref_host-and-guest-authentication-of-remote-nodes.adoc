:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_remote-node-management.adoc

[id='ref_host-and-guest-authentication-of-remote-nodes-{context}']

= Host and guest authentication of pacemaker_remote nodes

[role="_abstract"]
The connection between cluster nodes and pacemaker_remote is secured using Transport Layer Security (TLS) with pre-shared key (PSK) encryption and authentication over TCP (using port 3121 by default). This means both the cluster node and the node running `pacemaker_remote` must share the same private key. By default this key must be placed at `/etc/pacemaker/authkey` on both cluster nodes and remote nodes.

The [command]`pcs cluster node add-guest` command sets up the `authkey` for guest nodes and the [command]`pcs cluster node add-remote` command sets up the `authkey` for remote nodes.

