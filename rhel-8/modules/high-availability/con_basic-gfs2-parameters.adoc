:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_planning-gfs2-deployment.adoc

[id='con_basic-gfs2-parameters-{context}']

= Key GFS2 parameters to determine

[role="_abstract"]
There are a number of key GFS2 parameters you should plan for before you install and configure a GFS2 file system.

GFS2 nodes::
+
Determine which nodes in the cluster will mount the GFS2 file systems.

Number of file systems::
+
Determine how many GFS2 file systems to create initially. More file systems can be added later.

File system name::
+
Each GFS2 file system should have a unique name. This name is usually the same as the LVM logical volume name and is used as the DLM lock table name when a GFS2 file system is mounted. For example, this guide uses file system names `mydata1` and `mydata2` in some example procedures.

Journals::
+
Determine the number of journals for your GFS2 file systems. GFS2 requires one journal for each node in the cluster that needs to mount the file system. For example, if you have a 16-node cluster but need to mount only the file system from two nodes, you need only two journals. GFS2 allows you to add journals dynamically at a later point with the `gfs2_jadd` utility as additional servers mount a file system.
// For information about adding journals to a GFS2 file
// system, see
// <<s1-manage-addjournalfs>>.

Storage devices and partitions::
+
Determine the storage devices and partitions to be used for creating logical volumes (using `lvmlockd`) in the file systems.

Time protocol::
+
Make sure that the clocks on the GFS2 nodes are synchronized. It is recommended that you use the Precision Time Protocol (PTP) or, if necessary for your configuration, the Network Time Protocol (NTP) software provided with your Red Hat Enterprise Linux distribution.
+
The system clocks in GFS2 nodes must be within a few minutes of each other to prevent unnecessary inode time stamp updating. Unnecessary inode time stamp updating severely impacts cluster performance.

[NOTE]
====

You may see performance problems with GFS2 when many create and delete operations are issued from more than one node in the same directory at the same time. If this  causes performance problems in your system, you should localize file  creation and deletions by a node to directories specific to that node as much as possible.

====
