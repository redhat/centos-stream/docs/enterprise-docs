:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies
//
// assembly_configuring-cluster-resources.adoc

[id='con_determining-resource-behavior-{context}']
= Determining resource behavior

[role="_abstract"]
You can determine the behavior of a resource in a cluster by configuring constraints for that resource. You can configure the following categories of constraints:

* `location` constraints &mdash; A location constraint determines which nodes a resource can run on. For information about configuring location constraints, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_determining-which-node-a-resource-runs-on-configuring-and-managing-high-availability-clusters[Determining which nodes a resource can run on].

* `order` constraints &mdash; An ordering constraint determines the order in which the resources run. For information about configuring ordering constraints, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_determining-resource-order.adoc-configuring-and-managing-high-availability-clusters[Determining the order in which cluster resources are run].

* `colocation` constraints &mdash; A colocation constraint determines where resources will be placed relative to other resources. For information about colocation constraints, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_colocating-cluster-resources.adoc_configuring-and-managing-high-availability-clusters[Colocating cluster resources].


As a shorthand for configuring a set of constraints that will locate a set of resources together and ensure that the resources start sequentially and stop in reverse order, Pacemaker supports the concept of resource groups. After you have created a resource group, you can configure constraints on the group itself just as you configure constraints for individual resources.
