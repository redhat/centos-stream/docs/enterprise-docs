////
Base the file name and the ID on the module title. For example:
* file name: ref-my-reference-a.adoc
* ID: [id="ref-my-reference-a_{context}"]
* Title: = My reference A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

////
Indicate the module type in one of the following
ways:
Add the prefix ref- or ref_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: REFERENCE

[id="ref_recommended-rhosp-server-group-configuration_{context}"]
= RHOSP server group configuration for HA instances
////
In the title of a reference module, include nouns that are used in the body text. For example, "Keyboard shortcuts for ___" or "Command options for ___." This helps readers and search engines find the information quickly.

Be sure to include a line break between the title and the module introduction.
////

Create an instance server group before you create the RHOSP HA cluster node instances. Group the instances by affinity policy. If you configure multiple clusters, ensure that you have only one server group per cluster.

The affinity policy you set for the server group can determine whether the cluster remains operational if the hypervisor fails.

The default affinity policy is `affinity`. With this affinity policy, all of the cluster nodes could be created on the same RHOSP hypervisor. In this case, if the hypervisor fails the entire cluster fails. For this reason, set an affinity policy for the server group of `anti-affinity` or `soft-anti-affinity`.

* With an affinity policy of `anti-affinity`, the server group allows only one cluster node per Compute node. Attempting to create more cluster nodes than Compute nodes generates an error. While this configuration provides the highest protection against RHOSP hypervisor failures, it may require more resources to deploy large clusters than you have available.

* With an affinity policy of `soft-anti-affinity`, the server group distributes cluster nodes as evenly as possible across all Compute nodes. Although this provides less protection against hypervisor failures than a policy of `anti-affinity`, it provides a greater level of high availability than an affinity policy of `affinity`.

Determining the server group affinity policy for your deployment requires balancing your cluster needs against the resources you have available by taking the following cluster components into account:

* The number of nodes in the cluster
* The number of RHOSP Compute nodes available
* The number of nodes required for cluster quorum to retain cluster operations

For information about affinity and creating an instance server group, link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/configuring_the_compute_service_for_instance_creation/assembly_configuring-instance-scheduling-and-placement_scheduling-and-placement#ref_compute-scheduler-filters_scheduling-and-placement[Compute scheduler filters] and the link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2/html/command_line_interface_reference/server#server_group_create[Command Line Interface Reference].

