:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies:
// assembly_creating-mounting-gfs2.adoc


[id='proc_mounting-gfs2-filesystem_{context}']

= Mounting a GFS2 file system

[role="_abstract"]
Before you can mount a GFS2 file system, the file system must exist, the volume where the file system exists must be activated, and the supporting clustering and locking systems must be started. After those requirements have been met, you can mount the GFS2 file system as you would any Linux file system.

[NOTE]
====
You should always use Pacemaker to manage the GFS2 file system in a production environment rather than manually mounting the file system with a [command]`mount` command, as this may cause issues at system shutdown.
// LINK TO as described in
// xref:proc_unmounting-gfs2-filesystem-mounting-gfs2-filesystem[Unmounting a GFS2 file system].
====

To manipulate file ACLs, you must mount the file system with the [command]`-o acl` mount option. If a file system is mounted without the [command]`-o acl` mount option, users are allowed to view ACLs (with [command]`getfacl`), but are not allowed to set them (with [command]`setfacl`).

== Mounting a GFS2 file system with no options specified

In this example, the GFS2 file system on `/dev/vg01/lvol0` is mounted on the `/mygfs2` directory.

[subs="+quotes"]
....

# *mount /dev/vg01/lvol0 /mygfs2*

....

== Mounting a GFS2 file system that specifies mount options

The following is the format for the command to mount a GFS2 file system that specifies mount options.


[subs="+quotes"]
....

mount _BlockDevice_ _MountPoint_ -o _option_

....

`BlockDevice`::
+
Specifies the block device where the GFS2 file system resides.

`MountPoint`::
+
Specifies the directory where the GFS2 file system should be mounted.


The [option]`-o option` argument consists of GFS2-specific options or acceptable standard Linux [command]`mount -o` options, or a combination of both. Multiple `option` parameters are separated by a comma and no spaces.

[NOTE]
====

The [command]`mount` command is a Linux system command. In addition to using these GFS2-specific options, you can use other, standard, [command]`mount` command options (for example, [option]`-r`). For information about other Linux [command]`mount` command options, see the Linux [command]`mount` man page on your system.

====

The following table describes the available GFS2-specific [option]`-o option` values that can be passed to GFS2 at mount time.

[NOTE]
====

This table includes descriptions of options that are used with local file systems only. Note, however, that Red Hat does not support the use of GFS2 as a single-node file system. Red Hat will continue to support single-node GFS2 file systems for mounting snapshots of cluster file systems (for example, for backup purposes).

====

[[tb-table-gfs2-mount]]
.GFS2-Specific Mount Options

[options="header"]
|===
|Option|Description
|`acl`|Allows manipulating file ACLs. If a file system is mounted without the  [command]`acl` mount option, users are allowed to view ACLs (with [command]`getfacl`), but are not allowed to set them (with [command]`setfacl`).
|`data=[ordered\|writeback]`|When `data=ordered` is set, the user data modified by a transaction is flushed to the disk before the transaction is committed to disk. This should prevent the user from seeing uninitialized blocks in a file after a crash. When `data=writeback` mode is set, the user data is written to the disk at any time after it is dirtied; this does not provide the same consistency guarantee as `ordered` mode, but it should be slightly faster for some workloads. The default value is `ordered` mode.
|`ignore_local_fs`

`Caution:` This option should not be used when GFS2 file systems are shared.|Forces GFS2 to treat the file system as a multi-host file system. By default, using `lock_nolock` automatically turns on the pass:attributes[{blank}]`localflocks` flag.
|`localflocks`

`Caution:` This option should not be used when GFS2 file systems are shared.|Tells GFS2 to let the VFS (virtual file system) layer do all flock and fcntl. The `localflocks` flag is automatically turned on by `lock_nolock`.
|`lockproto=`pass:attributes[{blank}]pass:attributes[{blank}]`LockModuleName`|Allows the user to specify which locking protocol to use with the file system. If `LockModuleName` is not specified, the locking protocol name is read from the file system superblock.
|`locktable=`pass:attributes[{blank}]pass:attributes[{blank}]`LockTableName`|Allows the user to specify which locking table to use with the file system.
|`quota=[off/account/on]`|Turns quotas on or off for a file system. Setting the quotas to be in the `account` state causes the per UID/GID usage statistics to be correctly maintained by the file system; limit and warn values are ignored. The default value is `off`.
|[command]`errors=panic\|withdraw`|When `errors=panic` is specified, file system errors will cause a kernel panic. When `errors=withdraw` is specified, which is the default behavior, file system errors will cause the system to withdraw from the file system and make it inaccessible until the next reboot; in some cases the system may remain running.
|`discard/nodiscard`|Causes GFS2 to generate "discard" I/O requests for blocks that have been freed. These can be used by suitable hardware to implement thin provisioning and similar schemes.
|`barrier/nobarrier`|Causes GFS2 to send I/O barriers when flushing the journal. The default value is `on`. This option is automatically turned `off` if the underlying device does not support I/O barriers. Use of I/O barriers with GFS2 is highly recommended at all times unless the block device is designed so that it cannot lose its write cache content (for example, if it is on a UPS or it does not have a write cache).
|`quota_quantum=pass:attributes[{blank}]_secs_pass:attributes[{blank}]`|Sets the number of seconds for which a change in the quota information may sit on one node before being written to the quota file. This is the preferred way to set this parameter. The value is an integer number of seconds greater than zero. The default is 60 seconds. Shorter settings result in faster updates of the lazy quota information and less likelihood of someone exceeding their quota. Longer settings make file system operations involving quotas faster and more efficient.
|`statfs_quantum=pass:attributes[{blank}]_secs_pass:attributes[{blank}]`|Setting `statfs_quantum` to 0 is the preferred way to set the slow version of [command]`statfs`. The default value is 30 secs which sets the maximum time period before `statfs` changes will be synced to the master `statfs` file. This can be adjusted to allow for faster, less accurate `statfs` values or slower more accurate values. When this option is set to 0, `statfs` will always report the true values.
|`statfs_percent=pass:attributes[{blank}]_value_pass:attributes[{blank}]`|Provides a bound on the maximum percentage change in the `statfs` information about a local basis before it is synced back to the master `statfs` file, even if the time period has not expired. If the setting of `statfs_quantum` is 0, then this setting is ignored.
|===


== Unmounting a GFS2 file system

GFS2 file systems that have been mounted manually rather than automatically through Pacemaker will not be known to the system when file systems are unmounted at system shutdown. As a result, the GFS2 resource agent will not unmount the GFS2 file system. After the GFS2 resource agent is shut down, the standard shutdown process kills off all remaining user processes, including the cluster infrastructure, and tries to unmount the file system. This unmount will fail without the cluster infrastructure and the system will hang.

To prevent the system from hanging when the GFS2 file systems are unmounted, you should do one of the following:

* Always use Pacemaker to manage the GFS2 file system.

* If a GFS2 file system has been mounted manually with the [command]`mount` command, be sure to unmount the file system manually with the [command]`umount` command before rebooting or shutting down the system.

If your file system hangs while it is being unmounted during system shutdown under these circumstances, perform a hardware reboot. It is unlikely that any data will be lost since the file system is synced earlier in the shutdown process.

The GFS2 file system can be unmounted the same way as any Linux file system, by using the [command]`umount` command.

[NOTE]
====

The [command]`umount` command is a Linux system command. Information about this command can be found in the Linux [command]`umount` command man pages.

====
Usage
[subs="+quotes"]
....

umount _MountPoint_

....

`MountPoint`::
+
Specifies the directory where the GFS2 file system is currently mounted.

