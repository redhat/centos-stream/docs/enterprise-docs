:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_cluster-maintenance.adoc

[id='proc_disabling-resources-{context}']

= Disabling, enabling, and banning cluster resources

[role="_abstract"]
In addition to the [command]`pcs resource move` and [command]`pcs resource relocate` commands, there are a variety of other commands you can use to control the behavior of cluster resources.
// described in <<s1-manually_moving_resources-HAAR>>,

[discrete]
== Disabling a cluster resource

You can manually stop a running resource and prevent the cluster from starting it again with the following command. Depending on the rest of the configuration (constraints, options, failures, and so on), the resource may remain started. If you specify the [option]`--wait` option, [application]*pcs* will wait up to 'n' seconds for the resource to stop and then return 0 if the resource is stopped or 1 if the resource has not stopped. If 'n' is not specified it defaults to 60 minutes.

[subs="+quotes"]
....

pcs resource disable _resource_id_ [--wait[=_n_]]

....

ifeval::[{ProductNumber} == 8]
As of RHEL 8.2, you can specify that a resource be disabled only if disabling
endif::[]
ifeval::[{ProductNumber} == 9]
You can specify that a resource be disabled only if disabling
endif::[]
the resource would not have an effect on other resources. Ensuring that this would be the case can be impossible to do by hand when complex resource relations are set up.

* The [command]`pcs resource disable --simulate` command shows the effects of disabling a resource while not changing the cluster configuration.
* The [command]`pcs resource disable --safe` command disables a resource only if no other resources would be affected in any way, such as being migrated from one node to another. The [command]`pcs resource safe-disable` command is an alias for the `pcs resource disable --safe` command.
* The [command]`pcs resource disable --safe --no-strict` command disables a resource only if no other resources would be stopped or demoted

ifeval::[{ProductNumber} == 8]
As of RHEL 8.5 you can specify the `--brief` option for the `pcs resource disable --safe` command to print errors only. Also as of RHEL 8.5, the error report that the `pcs resource disable --safe` command generates if the safe disable  operation fails contains the affected resource IDs.
endif::[]
ifeval::[{ProductNumber} == 9]
You can specify the `--brief` option for the `pcs resource disable --safe` command to print errors only. The error report that the `pcs resource disable --safe` command generates if the safe disable  operation fails contains the affected resource IDs.
endif::[]
If you need to know only the resource IDs of resources that would be affected by disabling a resource, use the `--brief` option, which does not provide the full simulation result.


[discrete]
== Enabling a cluster resource

Use the following command to allow the cluster to start a resource. Depending on the rest of the configuration, the resource may remain stopped. If you specify the [option]`--wait` option, [application]*pcs* will wait up to 'n' seconds for the resource to start and then return 0 if the resource is started or 1 if the resource has not started. If 'n' is not specified it defaults to 60 minutes.

[subs="+quotes"]
....

pcs resource enable _resource_id_ [--wait[=_n_]]

....


[discrete]
== Preventing a resource from running on a particular node

Use the following command to prevent a resource from running on a specified node, or on the current node if no node is specified.

ifeval::[{ProductNumber} == 8]
[subs="+quotes"]
....

pcs resource ban _resource_id_ [_node_] [--master] [lifetime=_lifetime_] [--wait[=_n_]]

....
endif::[]
ifeval::[{ProductNumber} == 9]
[literal,subs="+quotes,verbatim,macros,attributes"]
[subs="+quotes"]
....

pcs resource ban _resource_id_ [_node_] [--promoted] [lifetime=_lifetime_] [--wait[=_n_]]

....
endif::[]


Note that when you execute the [command]`pcs resource ban` command, this adds a -INFINITY location constraint to the resource to prevent it from running on the indicated node. You can execute the [command]`pcs resource clear` or the [command]`pcs constraint delete` command to remove the constraint. This does not necessarily move the resources back to the indicated node; where the resources can run at that point depends on how you have configured your resources initially. 
// LINK TO
// For information about resource constraints, see <<ch-resourceconstraints-HAAR>>.

ifeval::[{ProductNumber} == 8]
If you specify the `--master` parameter of the [command]`pcs resource ban` command, the scope of the constraint is limited to the master role and you must specify _master_id_ rather than _resource_id_.
endif::[]
ifeval::[{ProductNumber} == 9]
If you specify the `--promoted` parameter of the [command]`pcs resource ban` command, the scope of the constraint is limited to the promoted role and you must specify _promotable_id_ rather than _resource_id_.
endif::[]


You can optionally configure a `lifetime` parameter for the `pcs resource ban` command to indicate a period of time the constraint should remain.
// LINK TO For information on specifying units for the `lifetime` parameter and on specifying the intervals at which the `lifetime` parameter should be checked, see <<s1-manually_moving_resources-HAAR>>.

You can optionally configure a `--wait[=pass:attributes[{blank}]_n_pass:attributes[{blank}]]` parameter for the `pcs resource ban` command to indicate the number of seconds to wait for the resource to start on the destination node before returning 0 if the resource is started or 1 if the resource has not yet started. If you do not specify n, the default resource timeout will be used.

[discrete]
== Forcing a resource to start on the current node

Use the [command]`debug-start` parameter of the [command]`pcs resource` command to force a specified resource to start on the current node, ignoring the cluster recommendations and printing the output from starting the resource. This is mainly used for debugging resources; starting resources on a cluster is (almost) always done by Pacemaker and not directly with a [command]`pcs` command. If your resource is not starting, it is usually due to either a misconfiguration of the resource (which you debug in the system log), constraints that prevent the resource from starting, or the resource being disabled. You can use this command to test resource configuration, but it should not normally be used to start resources in a cluster.

The format of the [command]`debug-start` command is as follows.

[subs="+quotes"]
....

pcs resource debug-start _resource_id_

....
