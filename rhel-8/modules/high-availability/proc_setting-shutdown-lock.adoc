:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-resources-to-remain-stopped.adoc

[id='proc_setting-shutdown-lock-{context}']

= Setting the shutdown-lock cluster property

[role="_abstract"]
The following example sets the `shutdown-lock` cluster property to `true` in an example cluster and shows the effect this has when the node is shut down and started again. This example cluster consists of three nodes: `z1.example.com`, `z2.example.com`, and `z3.example.com`.

.Procedure

. Set the `shutdown-lock` property to `true` and verify its value. In this example the `shutdown-lock-limit` property maintains its default value of 0.
+
[subs="+quotes"]
....

[root@z3 ~]# *pcs property set shutdown-lock=true*
[root@z3 ~]# *pcs property list --all | grep shutdown-lock*
 shutdown-lock: true
 shutdown-lock-limit: 0

....

. Check the status of the cluster. In this example, resources `third` and `fifth` are running on `z1.example.com`.
+
[subs="+quotes"]
....

[root@z3 ~]# *pcs status*
...
Full List of Resources:
...
 * first	(ocf::pacemaker:Dummy):	Started z3.example.com
 * second	(ocf::pacemaker:Dummy):	Started z2.example.com
 * third	(ocf::pacemaker:Dummy):	Started z1.example.com
 * fourth	(ocf::pacemaker:Dummy):	Started z2.example.com
 * fifth	(ocf::pacemaker:Dummy):	Started z1.example.com
...

....

. Shut down `z1.example.com`, which will stop the resources that are running on that node.
+
[subs="+quotes"]
....

[root@z3 ~] *# pcs cluster stop z1.example.com*
Stopping Cluster (pacemaker)...
Stopping Cluster (corosync)...

....

. Running the `pcs status` command shows
that node `z1.example.com` is offline and that
the resources that had been running on `z1.example.com`
are `LOCKED` while the node is down.
+
[subs="+quotes"]
....

[root@z3 ~]# *pcs status*
...

Node List:
 * Online: [ z2.example.com z3.example.com ]
 * OFFLINE: [ z1.example.com ]

Full List of Resources:
...
 * first	(ocf::pacemaker:Dummy):	Started z3.example.com
 * second	(ocf::pacemaker:Dummy):	Started z2.example.com
 * third	(ocf::pacemaker:Dummy):	Stopped z1.example.com (LOCKED)
 * fourth	(ocf::pacemaker:Dummy):	Started z3.example.com
 * fifth	(ocf::pacemaker:Dummy):	Stopped z1.example.com (LOCKED)

...

....

. Start cluster services again on `z1.example.com` so that it rejoins the cluster. Locked resources should get started on that node, although once they start they will not not necessarily remain on the same node.
+
[subs="+quotes"]
....

[root@z3 ~]# *pcs cluster start z1.example.com*
Starting Cluster...

....

. In this example, resouces `third` and `fifth` are recovered on node `z1.example.com`.
+
[subs="+quotes"]
....

[root@z3 ~]# *pcs status*
...

Node List:
 * Online: [ z1.example.com z2.example.com z3.example.com ]

Full List of Resources:
..
 * first	(ocf::pacemaker:Dummy):	Started z3.example.com
 * second	(ocf::pacemaker:Dummy):	Started z2.example.com
 * third	(ocf::pacemaker:Dummy):	Started z1.example.com
 * fourth	(ocf::pacemaker:Dummy):	Started z3.example.com
 * fifth	(ocf::pacemaker:Dummy):	Started z1.example.com

...

....

