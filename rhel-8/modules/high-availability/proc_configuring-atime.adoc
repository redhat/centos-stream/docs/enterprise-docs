:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_gfs2-usage-considerations.adoc

[id='proc_configuring-atime-{context}']

= Configuring `atime` updates

[role="_abstract"]
Each file inode and directory inode has three time stamps associated with it:

* [command]`ctime` &mdash; The last time the inode status was changed

* [command]`mtime` &mdash; The last time the file (or directory) data was modified

* [command]`atime` &mdash; The last time the file (or directory) data was accessed

If [command]`atime` updates are enabled as they are by default on GFS2 and other Linux file systems, then every time a file is read its inode needs to be updated.

Because few applications use the information provided by [command]`atime`, those updates can require a significant amount of unnecessary write traffic and file locking traffic. That traffic can degrade performance; therefore, it may be preferable to turn off or reduce the frequency of [command]`atime` updates.

The following methods of reducing the effects of [command]`atime` updating are available:

* Mount with `relatime` (relative atime), which updates the [command]`atime` if the previous [command]`atime` update is older than the [command]`mtime` or [command]`ctime` update.  This is the default mount option for GFS2 file systems.

* Mount with [command]`noatime` or [command]`nodiratime`. Mounting with [command]`noatime` disables [command]`atime` updates for both files and directories on that file system, while mounting with [command]`nodiratime` disables [command]`atime` updates only for directories on that file system, It is generally recommended that you mount GFS2 file systems with the `noatime` or `nodiratime` mount option whenever possible, with the preference for `noatime` where the application allows for this. For more information about the effect of these arguments on GFS2 file system performance, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_gfs2_file_systems/assembly_gfs2-performance-configuring-gfs2-file-systems#con_gfs2-node-locking-gfs2-performance[GFS2 Node Locking].

Use the following command to mount a GFS2 file system with the [option]`noatime` Linux mount option.

[subs="+quotes"]
....

mount _BlockDevice_ _MountPoint_ -o noatime

....

`BlockDevice`::
+
Specifies the block device where the GFS2 file system resides.

`MountPoint`::
+
Specifies the directory where the GFS2 file system should be mounted.

In this example, the GFS2 file system resides on `/dev/vg01/lvol0` and is mounted on directory `/mygfs2` with [command]`atime` updates turned off.

[subs="+quotes"]
....

# *mount /dev/vg01/lvol0 /mygfs2 -o noatime*

....

