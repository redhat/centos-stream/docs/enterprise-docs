:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_planning-gfs2-deployment.adoc

[id='con_gfs2-filesystem-format-{context}']

= GFS2 file system format version 1802

[role="_abstract"]
As of Red Hat Enterprise Linux 9, GFS2 file systems are created with format version 1802.

Format version 1802 enables the following features:

* Extended attributes in the `trusted` namespace ("trusted.* xattrs") are recognized by `gfs2` and `gfs2-utils`.

* The `rgrplvb` option is active by default. This allows `gfs2` to attach updated resource group data to DLM lock requests, so the node acquiring the lock does not need to update the resource group information from disk. This improves performance in some cases.

Filesystems created with the new format version will not be able to be mounted under earlier RHEL versions and older versions of the `fsck.gfs2` utility will not be able to check them.

Users can create a file system with the older format version by running the `mkfs.gfs2` command with the option `-o format=1801`.

Users can upgrade the format version of an older file system running `tunegfs2 -r 1802 _device_` on an unmounted file system. Downgrading the format version is not supported.


