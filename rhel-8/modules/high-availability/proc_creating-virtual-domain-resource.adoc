:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-virtual-domain-as-a-resource.adoc

[id='proc_creating-virtual-domain-resource-{context}']

= Creating the virtual domain resource

[role="_abstract"]
The following procedure creates a `VirtualDomain` resource in a cluster for a virtual machine you have previously created.

.Procedure
. To create the `VirtualDomain` resource agent for the management of the virtual machine, Pacemaker requires the virtual machine's `xml` configuration file to be dumped to a file on disk. For example, if you created a virtual machine named `guest1`, dump the `xml` file to a file somewhere on one of the cluster nodes that will be allowed to run the guest. You can use a file name of your choosing; this example uses `/etc/pacemaker/guest1.xml`.
+
[subs="+quotes"]
....

# *virsh dumpxml guest1 > /etc/pacemaker/guest1.xml*

....

. Copy the virtual machine's `xml` configuration file to all of the other cluster nodes that will be allowed to run the guest, in the same location on each node.

. Ensure that all of the nodes allowed to run the virtual domain have access to the necessary storage devices for that virtual domain.

. Separately test that the virtual domain can start and stop on each node that will run the virtual domain.

. If it is running, shut down the guest node. Pacemaker will start the node when it is configured in the cluster. The virtual machine should not be configured to start automatically when the host boots.

. Configure the `VirtualDomain` resource with the [command]`pcs resource create` command. For example, the following command configures a `VirtualDomain` resource named `VM`. Since the `allow-migrate` option is set to `true` a `pcs resource move VM _nodeX_pass:attributes[{blank}]` command would be done as a live migration.
+
In this example `migration_transport` is set to `ssh`. Note that for SSH migration to work properly, keyless logging must work between nodes.
+
[subs="+quotes"]
....

# *pcs resource create VM VirtualDomain config=/etc/pacemaker/guest1.xml migration_transport=ssh meta allow-migrate=true*

....

