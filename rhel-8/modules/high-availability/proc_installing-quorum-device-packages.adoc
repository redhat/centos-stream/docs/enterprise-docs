:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-quorum-devices.adoc

[id='proc_installing-quorum-device-packages-{context}']

= Installing quorum device packages

[role="_abstract"]
Configuring a quorum device for a cluster requires that you install the following
packages:

* Install `corosync-qdevice` on the nodes of an existing cluster.
+
[subs="+quotes,attributes"]
....

[root@node1:~]# *{PackageManagerCommand} install corosync-qdevice*
[root@node2:~]# *{PackageManagerCommand} install corosync-qdevice*

....

* Install `pcs` and `corosync-qnetd` on the quorum device host.
+
[subs="+quotes,attributes"]
....

[root@qdevice:~]# *{PackageManagerCommand} install pcs corosync-qnetd*

....

* Start the `pcsd` service and enable `pcsd`
at system start on the quorum device host.
+
[subs="+quotes"]
....

[root@qdevice:~]# *systemctl start pcsd.service*
[root@qdevice:~]# *systemctl enable pcsd.service*

....
