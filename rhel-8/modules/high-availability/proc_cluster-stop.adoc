:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_clusternode-management.adoc

[id='proc_cluster-stop-{context}']

= Stopping cluster services

[role="_abstract"]
The following command stops cluster services on the specified node or nodes. As with the [command]`pcs cluster start`, the `--all` option stops cluster services on all nodes and if you do not specify any nodes, cluster services are stopped on the local node only.

[subs="+quotes"]
....

pcs cluster stop [--all | _node_] [...]

....

You can force a stop of cluster services on the local node with the following command, which performs a [command]`kill -9` command.

....

pcs cluster kill

....
