:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_getting-started-with-pacemaker.adoc

[id='ref_configuring-constraint-using-rules.adoc-{context}']

= Configuring a Pacemaker location constraint using rules

[role="_abstract"]
Use the following command to configure a Pacemaker constraint that uses rules. If `score` is omitted, it defaults to INFINITY. If `resource-discovery` is omitted, it defaults to `always`.

For information about the `resource-discovery` option, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_determining-which-node-a-resource-runs-on-configuring-and-managing-high-availability-clusters#proc_limiting-resource-discovery-to-a-subset-of-nodes-determining-which-node-a-resource-runs-on[Limiting resource discovery to a subset of nodes].

As with basic location constraints, you can use regular expressions for resources with these constraints as well.

When using rules to configure location constraints, the value of `score` can be positive or negative, with a positive value indicating "prefers" and a negative value indicating "avoids".

ifeval::[{ProductNumber} == 8]
[subs="+quotes"]
....

pcs constraint location _rsc_ rule [resource-discovery=_option_] [role=master|slave] [score=_score_ | score-attribute=_attribute_] _expression_

....
endif::[]
ifeval::[{ProductNumber} == 9]
[subs="+quotes"]
....

pcs constraint location _rsc_ rule [resource-discovery=_option_] [role=promoted|unpromoted] [score=_score_ | score-attribute=_attribute_] _expression_

....
endif::[]

The _expression_ option can be one of the following where _duration_options_ and _date_spec_options_ are: hours, monthdays, weekdays, yeardays, months, weeks, years, weekyears, and moon as described in the "Properties of a Date Specification" table in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_determining-resource-location-with-rules-configuring-and-managing-high-availability-clusters#date_specifications[Date specifications].

* `defined|not_defined _attribute_`

ifeval::[{ProductNumber} == 8]
* `_attribute_ lt|gt|lte|gte|eq|ne [string|integer|number`(RHEL 8.4 and later)`|version] _value_`
endif::[]
ifeval::[{ProductNumber} == 9]
* `_attribute_ lt|gt|lte|gte|eq|ne [string|integer|number|version] _value_`
endif::[]

* `date gt|lt _date_`

* `date in_range _date_ to _date_`

* `date in_range _date_ to duration _duration_options_ ...`

* `date-spec _date_spec_options_`

* `_expression_ and|or _expression_`

* `(_expression_)`

Note that durations are an alternative way to specify an end for `in_range` operations by means of calculations. For example, you can specify a duration of 19 months.


The following location constraint configures an expression that is true if now is any time in the year 2018.

[subs="+quotes"]
....

# *pcs constraint location Webserver rule score=INFINITY date-spec years=2018* 

....

The following command configures an expression that is true from 9 am to 5 pm, Monday through Friday. Note that the hours value of 16 matches up to 16:59:59, as the numeric value (hour) still matches.

[subs="+quotes"]
....

# *pcs constraint location Webserver rule score=INFINITY date-spec hours="9-16" weekdays="1-5"* 

....

The following command configures an expression that is true when there is a full moon on Friday the thirteenth.

[subs="+quotes"]
....

# *pcs constraint location Webserver rule date-spec weekdays=5 monthdays=13 moon=4*

....

To remove a rule, use the following command. If the rule that you are removing is the last rule in its constraint, the constraint will be removed.

[subs="+quotes"]
....

pcs constraint rule remove _rule_id_

....

