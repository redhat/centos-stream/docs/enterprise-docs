:_mod-docs-content-type: REFERENCE
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-resources-to-remain-stopped.adoc

[id='ref_cluster-properties-shutdown-lock-{context}']

= Cluster properties to configure resources to remain stopped on clean node shutdown

[role="_abstract"]
The ability to prevent resources from failing over on a clean node shutdown is implemented by means of the following cluster properties.

`shutdown-lock`::
+
When this cluster property is set to the default value of `false`, the cluster will recover resources that are active on nodes being cleanly shut down. When this property is set to `true`, resources that are active on the nodes being cleanly shut down are unable to start elsewhere until they start on the node again after it rejoins the cluster.
+
The `shutdown-lock` property will work for either cluster nodes or remote nodes, but not guest nodes.
+
If `shutdown-lock` is set to `true`, you can remove the lock on one cluster resource when a node is down so that the resource can start elsewhere by performing a manual refresh on the node with the following command.
+
[command]`pcs resource refresh _resource_ node=_nodename_`
+
Note that once the resources are unlocked, the cluster is free to move the resources elsewhere. You can control the likelihood of this occurring by using stickiness values or location preferences for the resource.
+
[NOTE]
====

A manual refresh will work with remote nodes only if you first run the following commands:

. Run the [command]`systemctl stop pacemaker_remote` command on the remote node to stop the node.

. Run the [command]`pcs resource disable _remote-connection-resource_` command.

You can then perform a manual refresh on the remote node.

====

`shutdown-lock-limit`::
+
When this cluster property is set to a time other than the default value of 0, resources will be available for recovery on other nodes if the node does not rejoin within the specified time since the shutdown was initiated.
+
[NOTE]
====

The `shutdown-lock-limit` property will work with remote nodes only if you first run the following commands:

. Run the [command]`systemctl stop pacemaker_remote` command on the remote node to stop the node.

. Run the [command]`pcs resource disable _remote-connection-resource_` command.

After you run these commands, the resources that had been running on the remote node will be available for recovery on other nodes when the amount of time specified as the `shutdown-lock-limit` has passed.

====
