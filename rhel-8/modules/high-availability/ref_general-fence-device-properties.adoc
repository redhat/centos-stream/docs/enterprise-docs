:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-fencing.adoc

[id='ref_general-fence-device-properties-{context}']


= General properties of fencing devices

[role="_abstract"]
There are many general properties you can set for fencing devices, as well as various cluster properties that determine fencing behavior.

Any cluster node can fence any other cluster node with any fence device, regardless of whether the fence resource is started or stopped. Whether the resource is started controls only the recurring monitor for the device, not whether it can be used, with the following exceptions:

* You can disable a fencing device by running the [command]`pcs stonith disable _stonith_id_` command. This will prevent any node from using that device.

* To prevent a specific node from using a fencing device, you can configure location constraints for the fencing resource with the [command]`pcs constraint location ... avoids` command.

* Configuring `stonith-enabled=false` will disable fencing altogether. Note, however, that Red Hat does not support clusters when fencing is disabled, as it is not suitable for a production environment. 

The following table describes the general properties you can set for fencing devices.


[[tb-fencedevice-props-HAAR]]
.General Properties of Fencing Devices

[cols="2,1,2,3" options="header"]
|===
|Field|Type|Default|Description
|`pcmk_host_map`|string||A mapping of host names to port numbers for devices that do not support host names. For example: `node1:1;node2:2,3` tells the cluster to use port 1 for node1 and ports 2 and 3 for node2.
ifeval::[{ProductNumber} == 8]
As of RHEL 8.7, the
endif::[]
ifeval::[{ProductNumber} == 9]
the
endif::[]
`pcmk_host_map` property supports special characters inside `pcmk_host_map` values using a backslash in front of the value. For example, you can specify `pcmk_host_map="node3:plug\ 1"` to include a space in the host alias.
|`pcmk_host_list`|string||A list of machines controlled by this device (Optional unless `pcmk_host_check=static-list`).
|`pcmk_host_check`|string|
* `static-list` if either `pcmk_host_list` or `pcmk_host_map` is set

* Otherwise, `dynamic-list` if the fence device supports the `list` action

* Otherwise, `status` if the fence device supports the `status` action

*Otherwise, `none`.
|How to determine which machines are controlled by the device. Allowed values: `dynamic-list` (query the device), `static-list` (check the `pcmk_host_list` attribute), none (assume every device can fence every machine)
|===

The following table summarizes additional properties you can set for fencing devices. Note that these properties are for advanced use only.

[[tb-fencepropsadvanced-HAAR]]
.Advanced Properties of Fencing Devices

[cols="2,1,1,3" options="header"]
|===
|Field|Type|Default|Description
|`pcmk_host_argument`|string|port|An alternate parameter to supply instead of port. Some devices do not support the standard port parameter or may provide additional ones. Use this to specify an alternate, device-specific parameter that should indicate the machine to be fenced. A value of `none` can be used to tell the cluster not to supply any additional parameters.
|`pcmk_reboot_action`|string|reboot|An alternate command to run instead of `reboot`. Some devices do not support the standard commands or may provide additional ones. Use this to specify an alternate, device-specific, command that implements the reboot action.
|`pcmk_reboot_timeout`|time|60s|Specify an alternate timeout to use for reboot actions instead of `stonith-timeout`. Some devices need much more/less time to complete than normal. Use this to specify an alternate, device-specific, timeout for reboot actions.
|`pcmk_reboot_retries`|integer|2|The maximum number of times to retry the `reboot` command within the timeout period. Some devices do not support multiple connections. Operations may fail if the device is busy with another task so Pacemaker will automatically retry the operation, if there is time remaining. Use this option to alter the number of times Pacemaker retries reboot actions before giving up.
|`pcmk_off_action`|string|off|An alternate command to run instead of `off`. Some devices do not support the standard commands or may provide additional ones. Use this to specify an alternate, device-specific, command that implements the off action.
|`pcmk_off_timeout`|time|60s|Specify an alternate timeout to use for off actions instead of `stonith-timeout`. Some devices need much more or much less time to complete than normal. Use this to specify an alternate, device-specific, timeout for off actions.
|`pcmk_off_retries`|integer|2|The maximum number of times to retry the off command within the timeout period. Some devices do not support multiple connections. Operations may fail if the device is busy with another task so Pacemaker will automatically retry the operation, if there is time remaining. Use this option to alter the number of times Pacemaker retries off actions before giving up.
|`pcmk_list_action`|string|list|An alternate command to run instead of `list`. Some devices do not support the standard commands or may provide additional ones. Use this to specify an alternate, device-specific, command that implements the list action.
|`pcmk_list_timeout`|time|60s|Specify an alternate timeout to use for list actions. Some devices need much more or much less time to complete than normal. Use this to specify an alternate, device-specific, timeout for list actions.
|`pcmk_list_retries`|integer|2|The maximum number of times to retry the `list` command within the timeout period. Some devices do not support multiple connections. Operations may fail if the device is busy with another task so Pacemaker will automatically retry the operation, if there is time remaining. Use this option to alter the number of times Pacemaker retries list actions before giving up.
|`pcmk_monitor_action`|string|monitor|An alternate command to run instead of `monitor`. Some devices do not support the standard commands or may provide additional ones. Use this to specify an alternate, device-specific, command that implements the monitor action.
|`pcmk_monitor_timeout`|time|60s|Specify an alternate timeout to use for monitor actions instead of `stonith-timeout`. Some devices need much more or much less time to complete than normal. Use this to specify an alternate, device-specific, timeout for monitor actions.
|`pcmk_monitor_retries`|integer|2|The maximum number of times to retry the `monitor` command within the timeout period. Some devices do not support multiple connections. Operations may fail if the device is busy with another task so Pacemaker will automatically retry the operation, if there is time remaining. Use this option to alter the number of times Pacemaker retries monitor actions before giving up.
|`pcmk_status_action`|string|status|An alternate command to run instead of `status`. Some devices do not support the standard commands or may provide additional ones. Use this to specify an alternate, device-specific, command that implements the status action.
|`pcmk_status_timeout`|time|60s|Specify an alternate timeout to use for status actions instead of `stonith-timeout`. Some devices need much more or much less time to complete than normal. Use this to specify an alternate, device-specific, timeout for status actions.
|`pcmk_status_retries`|integer|2|The maximum number of times to retry the status command within the timeout period. Some devices do not support multiple connections. Operations may fail if the device is busy with another task so Pacemaker will automatically retry the operation, if there is time remaining. Use this option to alter the number of times Pacemaker retries status actions before giving up.
|`pcmk_delay_base`|string|0s|Enables a base delay for fencing actions and specifies a base delay value.
ifeval::[{ProductNumber} == 8]
As of Red Hat Enterprise Linux 8.6, you
endif::[] 
ifeval::[{ProductNumber} == 9]
You
endif::[]
can specify different values for different nodes with the `pcmk_delay_base` parameter.
For general information about fencing delay parameters and their interactions, see xref:ref_fence-delays-configuring-fencing[Fencing delays].
|`pcmk_delay_max`|time|0s|Enables a random delay for fencing actions and specifies the maximum delay, which is the maximum value of the combined base delay and random delay. For example, if the base delay is 3 and `pcmk_delay_max` is 10, the random delay will be between 3 and 10.
For general information about fencing delay parameters and their interactions, see xref:ref_fence-delays-configuring-fencing[Fencing delays].
|`pcmk_action_limit`|integer|1|The maximum number of actions that can be performed in parallel on this device.
ifeval::[{ProductNumber} == 8]
The cluster property `concurrent-fencing=true` needs to be configured first (this is the default value for RHEL 8.1 and later).
endif::[]
ifeval::[{ProductNumber} == 9]
The cluster property `concurrent-fencing=true` needs to be configured first (this is the default value).
endif::[]
A value of -1 is unlimited.
|`pcmk_on_action`|string|on|For advanced use only: An alternate command to run instead of `on`. Some devices do not support the standard commands or may provide additional ones. Use this to specify an alternate, device-specific, command that implements the `on` action.
|`pcmk_on_timeout`|time|60s|For advanced use only: Specify an alternate timeout to use for `on` actions instead of `stonith-timeout`. Some devices need much more or much less time to complete than normal. Use this to specify an alternate, device-specific, timeout for `on` actions.
|`pcmk_on_retries`|integer|2|For advanced use only: The maximum number of times to retry the `on` command within the timeout period. Some devices do not support multiple connections. Operations may `fail` if the device is busy with another task so Pacemaker will automatically retry the operation, if there is time remaining. Use this option to alter the number of times Pacemaker retries `on` actions before giving up.
|===

In addition to the properties you can set for individual fence devices, there are also cluster properties you can set that determine fencing behavior, as described in the following table.

[[tb-clusterfenceprops-HAAR]]
.Cluster Properties that Determine Fencing Behavior

[cols="2,2,3" options="header"]
|===
|Option|Default|Description
|`stonith-enabled`|true|Indicates that failed nodes and nodes with resources that cannot be stopped should be fenced. Protecting your data requires that you set this `true`.

If `true`, or unset, the cluster will refuse to start resources unless one or more STONITH resources have been configured also.

Red Hat only supports clusters with this value set to `true`.
|`stonith-action`|reboot|Action to send to fencing device. Allowed values: `reboot`, `off`. The value `poweroff` is also allowed, but is only used for legacy devices.
|`stonith-timeout`|60s|How long to wait for a STONITH action to complete.
|`stonith-max-attempts`|10|How many times fencing can fail for a target before the cluster will no longer immediately re-attempt it.
|`stonith-watchdog-timeout`||The maximum time to wait until a node can be assumed to have been killed by the hardware watchdog. It is recommended that this value be set to twice the value of the hardware watchdog timeout. This option is needed only if watchdog-only SBD configuration is used for fencing.
ifeval::[{ProductNumber} == 8]
|`concurrent-fencing`|true (RHEL 8.1 and later)|Allow fencing operations to be performed in parallel.
endif::[]
ifeval::[{ProductNumber} == 9]
|`concurrent-fencing`|true|Allow fencing operations to be performed in parallel.
endif::[]
ifeval::[{ProductNumber} == 8]
|`fence-reaction`|stop|(Red Hat Enterprise Linux 8.2 and later) Determines how a
endif::[]
ifeval::[{ProductNumber} == 9]
|`fence-reaction`|stop|Determines how a
endif::[]
cluster node should react if notified of its own fencing. A cluster node may receive notification of its own fencing if fencing is misconfigured, or if fabric fencing is in use that does not cut cluster communication. Allowed values are `stop` to attempt to immediately stop Pacemaker and stay stopped, or `panic` to attempt to immediately reboot the local node, falling back to stop on failure.

Although the default value for this property is `stop`, the safest choice for this value is `panic`, which attempts to immediately reboot the local node. If you prefer the stop behavior, as is most likely to be the case in conjunction with fabric fencing, it is recommended that you set this explicitly.
ifeval::[{ProductNumber} == 8]
|`priority-fencing-delay`|0 (disabled)|(RHEL 8.3 and later) Sets a fencing delay that allows you to configure a two-node cluster so that in a split-brain situation the node with the fewest or least important resources running is the node that gets fenced.
endif::[]
ifeval::[{ProductNumber} == 9]
|`priority-fencing-delay`|0 (disabled)|Sets a fencing delay that allows you to configure a two-node cluster so that in a split-brain situation the node with the fewest or least important resources running is the node that gets fenced.
endif::[]
For general information about fencing delay parameters and their interactions, see xref:ref_fence-delays-configuring-fencing[Fencing delays].
|===

For information about setting cluster properties, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_controlling-cluster-behavior-configuring-and-managing-high-availability-clusters#setting-cluster-properties-controlling-cluster-behavior[Setting and removing cluster properties].


