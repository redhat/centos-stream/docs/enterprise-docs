:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_creating-mounting-gfs2.adoc

[id='proc_growing-gfs2-filesystem-{context}']

= Growing a GFS2 file system

[role="_abstract"]
The [command]`gfs2_grow` command is used to expand a GFS2 file system after the device where the file system resides has been expanded. Running the [command]`gfs2_grow` command on an existing GFS2 file system fills all spare space between the current end of the file system and the end of the device with a newly initialized GFS2 file system extension. All nodes in the cluster can then use the extra storage space that has been added.


// The [command]`gfs2_grow` command must be run on a mounted
// file system, but only needs to be run on one node in a cluster.
// All the other nodes sense that the expansion has occurred and
// automatically start using the new space.

[NOTE]
====

You cannot decrease the size of a GFS2 file system.

====

The [command]`gfs2_grow` command must be run on a mounted file system. The following procedure increases the size of the GFS2 file system in a cluster that is mounted on the logical volume `shared_vg/shared_lv1` with a mount point of `/mnt/gfs2`.

.Procedure

. Perform a backup of the data on the file system.

. If you do not know the logical volume that is used by the file system to be expanded, you can determine this by running the [command]`df _mountpoint_` command.  This will display the device name in the following format:
+
`/dev/mapper/_vg_-_lv_`
+
For example, the device name `/dev/mapper/shared_vg-shared_lv1` indicates that the logical volume is `shared_vg/shared_lv1`.

. On one node of the cluster, expand the underlying cluster volume with the [command]`lvextend` command.
ifeval::[{ProductNumber} == 8]
If you are running RHEL 8.0, use the `--lockopt skiplv` option to override normal logical volume locking. This is not necessary for systems running RHEL 8.1 or later.
+
For RHEL 8.1 and later use the following command.
endif::[]
+
[subs="+quotes"]
....

# *lvextend -L+1G shared_vg/shared_lv1*
Size of logical volume shared_vg/shared_lv1 changed from 5.00 GiB (1280 extents) to 6.00 GiB (1536 extents).
WARNING: extending LV with a shared lock, other hosts may require LV refresh.
Logical volume shared_vg/shared_lv1 successfully resized.

....
+
ifeval::[{ProductNumber} == 8]
For RHEL 8.0, use the following command.
+
[subs="+quotes"]
....

# *lvextend --lockopt skiplv  -L+1G shared_vg/shared_lv1*
WARNING: skipping LV lock in lvmlockd.
Size of logical volume shared_vg/shared_lv1 changed from 5.00 GiB (1280 extents) to 6.00 GiB (1536 extents).
WARNING: extending LV with a shared lock, other hosts may require LV refresh.
Logical volume shared_vg/shared_lv1 successfully resized.

....
endif::[]

ifeval::[{ProductNumber} == 8]
. If you are running RHEL 8.0, on every additional node of the cluster refresh the logical volume to update the active logical volume on that node. This step is not necessary on systems running RHEL 8.1 and later as the step is automated when the logical volume is extended.
+
[subs="+quotes"]
....

# *lvchange --refresh shared_vg/shared_lv1*

....
endif::[]

. One one node of the cluster, increase the size of the GFS2 file system. Do not extend the file system if the logical volume was not refreshed on all of the nodes, otherwise the file system data may become unavailable throughout the cluster.
+
[subs="+quotes"]
....

# *gfs2_grow /mnt/gfs2*
FS: Mount point:             /mnt/gfs2
FS: Device:                  /dev/mapper/shared_vg-shared_lv1
FS: Size:                    1310719 (0x13ffff)
DEV: Length:                 1572864 (0x180000)
The file system will grow by 1024MB.
gfs2_grow complete.

....

. Run the [command]`df` command on all nodes to check that the new space is now available in the file system. Note that it may take up to 30 seconds for the [command]`df` command on all nodes to show the same file system size 
+
[subs="+quotes"]
....

# *df -h /mnt/gfs2*] 
Filesystem                        Size  Used Avail Use% Mounted on
/dev/mapper/shared_vg-shared_lv1  6.0G  4.5G  1.6G  75% /mnt/gfs2

....
