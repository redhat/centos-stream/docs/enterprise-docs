:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_gfs2-disk-quota-administration.adoc

[id='proc_synchronizing-gfs2-quotas-{context}']


= Synchronizing quotas with the quotasync Command

[role="_abstract"]
GFS2 stores all quota information in its own internal file on disk. A GFS2 node does not update this quota file for every file system write; rather, by default it updates the quota file once every 60 seconds. This is necessary to avoid contention among nodes writing to the quota file, which would cause a slowdown in performance.

As a user or group approaches their quota limit, GFS2 dynamically reduces the time between its quota-file updates to prevent the limit from being exceeded. The normal time period between quota synchronizations is a tunable parameter, `quota_quantum`. You can change this from its default value of 60 seconds using the `quota_quantum=` mount option, as described in the "GFS2-Specific Mount Options" table in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_gfs2_file_systems/assembly_creating-mounting-gfs2-configuring-gfs2-file-systems#mounting_a_gfs2_file_system_that_specifies_mount_options[Mounting a GFS2 file system that specifies mount options].


The `quota_quantum` parameter must be set on each node and each time the file system is mounted. Changes to the [command]`quota_quantum` parameter are not persistent across unmounts. You can update the `quota_quantum` value with the [command]`mount -o remount`.

You can use the [command]`quotasync` command to synchronize the quota information from a node to the on-disk quota file between the automatic updates performed by GFS2. Usage [application]*Synchronizing Quota Information*

[subs="+quotes"]
....

quotasync [-ug] -a|_mountpoint_...

....

`u`::
+
Sync the user quota files.

`g`::
+
Sync the group quota files

`a`::
+
Sync all file systems that are currently quota-enabled and support sync. When -a is absent, a file system mountpoint should be specified.

`mountpoint`::
+
Specifies the GFS2 file system to which the actions apply.

You can tune the time between synchronizations by specifying a `quota-quantum` mount option.

[subs="+quotes"]
....

# *mount -o quota_quantum=_secs_,remount _BlockDevice_ _MountPoint_*

....

`MountPoint`::
+
Specifies the GFS2 file system to which the actions apply.

`secs`::
+
Specifies the new time period between regular quota-file synchronizations by GFS2. Smaller values may increase contention and slow down performance.

The following example synchronizes all the cached dirty quotas from the node it is run on to the on-disk quota file for the file system `/mnt/mygfs2`.

[subs="+quotes"]
....

# *quotasync -ug /mnt/mygfs2*

....

This following example changes the default time period between regular quota-file updates to one hour (3600 seconds) for file system `/mnt/mygfs2` when remounting that file system on logical volume `/dev/volgroup/logical_volume`.

[subs="+quotes"]
....

# *mount -o quota_quantum=3600,remount /dev/volgroup/logical_volume /mnt/mygfs2*

....
