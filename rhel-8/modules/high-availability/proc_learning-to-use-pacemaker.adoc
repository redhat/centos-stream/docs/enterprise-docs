:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_getting-started-with-pacemaker.adoc


[id='proc_learning-to-use-pacemaker-{context}']

= Learning to use Pacemaker

[role="_abstract"]
By working through this procedure, you will learn how to use Pacemaker to set up a cluster, how to display cluster status, and how to configure a cluster service. This example creates an Apache HTTP server as a cluster resource and shows how the cluster responds when the resource fails.

In this example:

* The node is `z1.example.com`.

* The floating IP address is 192.168.122.120.

.Prerequisites

ifeval::[{ProductNumber} == 8]
* A single node running RHEL 8
endif::[]
ifeval::[{ProductNumber} == 9]
* A single node running RHEL 9
endif::[]

* A floating IP address that resides on the same network as one of the node's statically assigned IP addresses

* The name of the node on which you are running is in your `/etc/hosts` file

.Procedure

. Install the Red Hat High Availability Add-On software packages from the High Availability channel, and start and enable the `pcsd` service.
+
[subs="+quotes,attributes"]
....

# *{PackageManagerCommand} install pcs pacemaker fence-agents-all*
...
# *systemctl start pcsd.service*
# *systemctl enable pcsd.service*

....
+
If you are running the `firewalld` daemon, enable the ports that are required  by the Red Hat High Availability Add-On.
+
[subs="+quotes"]
....

# *firewall-cmd --permanent --add-service=high-availability*
# *firewall-cmd --reload*

....

. Set a password for user `hacluster` on each node in the cluster and authenticate user `hacluster` for each node in the cluster on the node from which you will be running the `pcs` commands. This example is using only a single node, the node from which you are running the commands, but this step is included here since it is a necessary step in configuring a supported Red Hat High Availability multi-node cluster.
+
[subs="+quotes"]
....

# *passwd hacluster*
...
# *pcs host auth z1.example.com*

....

. Create a cluster named `my_cluster` with one member and check the status of the cluster. This command creates and starts the cluster in one step.
+
[subs="+quotes"]
....

# *pcs cluster setup my_cluster --start z1.example.com*
...
# *pcs cluster status*
Cluster Status:
 Stack: corosync
 Current DC: z1.example.com (version 2.0.0-10.el8-b67d8d0de9) - partition with quorum
 Last updated: Thu Oct 11 16:11:18 2018
 Last change: Thu Oct 11 16:11:00 2018 by hacluster via crmd on z1.example.com
 1 node configured
 0 resources configured

PCSD Status:
  z1.example.com: Online

....

. A Red Hat High Availability cluster requires that you configure fencing for the cluster. The reasons for this requirement are described in the Red Hat Knowledgebase solution link:++https://access.redhat.com/solutions/15575++[Fencing in a Red Hat High Availability Cluster]. For this introduction, however, which is intended to show only how to use the basic Pacemaker commands, disable fencing by setting the `stonith-enabled` cluster option to `false`.
+
[WARNING]
====

The use of `stonith-enabled=false` is completely inappropriate for a production cluster. It tells the cluster to simply pretend that failed nodes are safely fenced.

====
+
[subs="+quotes"]
....

# *pcs property set stonith-enabled=false*

....

. Configure a web browser on your system and create a web page to display a simple text message.  If you are running the `firewalld` daemon, enable the ports that are required  by `httpd`.
+
[NOTE]
====

Do not use [command]`systemctl enable` to enable any services that will be managed by the cluster to start at system boot.

====
+
[subs="+quotes,attributes"]
....

# *{PackageManagerCommand} install -y httpd wget*
...
# *firewall-cmd --permanent --add-service=http*
# *firewall-cmd --reload*

# *cat <<-END >/var/www/html/index.html*
*<html>*
*<body>My Test Site - $(hostname)</body>*
*</html>*
*END*

....
+
In order for the Apache resource agent to get the status of Apache, create the following addition to the existing configuration to enable the status server URL.
+
[subs="+quotes"]
....

# *cat <<-END > /etc/httpd/conf.d/status.conf*
*<Location /server-status>*
*SetHandler server-status*
*Order deny,allow*
*Deny from all*
*Allow from 127.0.0.1*
*Allow from ::1*
*</Location>*
*END*

....

. Create `IPaddr2` and `apache` resources for the cluster to manage.  The 'IPaddr2' resource is a floating IP address that must not be one already associated with a physical node. If the 'IPaddr2' resource's NIC device is not specified, the floating IP must reside on the same network as the statically assigned IP address used by the node.
+
You can display a list of all available resource types with the [command]`pcs resource list` command. You can use the [command]`pcs resource describe _resourcetype_pass:attributes[{blank}]` command to display the parameters you can set for the specified resource type. For example, the following command displays the parameters you can set for a resource of type `apache`:
+
[subs="+quotes"]
....

# *pcs resource describe apache*
...

....
+
In this example, the IP address resource and the apache resource are both configured as part of a group named `apachegroup`, which ensures that the resources are kept together to run on the same node when you are configuring a working multi-node cluster.
+
[subs="+quotes"]
....

# *pcs resource create ClusterIP ocf:heartbeat:IPaddr2 ip=192.168.122.120 --group apachegroup*

# *pcs resource create WebSite ocf:heartbeat:apache  configfile=/etc/httpd/conf/httpd.conf  statusurl="http://localhost/server-status" --group apachegroup*

# *pcs status*
Cluster name: my_cluster
Stack: corosync
Current DC: z1.example.com (version 2.0.0-10.el8-b67d8d0de9) - partition with quorum
Last updated: Fri Oct 12 09:54:33 2018
Last change: Fri Oct 12 09:54:30 2018 by root via cibadmin on z1.example.com

1 node configured
2 resources configured

Online: [ z1.example.com ]

Full list of resources:

Resource Group: apachegroup
    ClusterIP  (ocf::heartbeat:IPaddr2):       Started z1.example.com
    WebSite    (ocf::heartbeat:apache):        Started z1.example.com

PCSD Status:
  z1.example.com: Online
...

....
+
After you have configured a cluster resource, you can use the [command]`pcs resource config` command to display the options that are configured for that resource.
+
[subs="+quotes"]
....

# *pcs resource config WebSite*
Resource: WebSite (class=ocf provider=heartbeat type=apache)
 Attributes: configfile=/etc/httpd/conf/httpd.conf statusurl=http://localhost/server-status
 Operations: start interval=0s timeout=40s (WebSite-start-interval-0s)
             stop interval=0s timeout=60s (WebSite-stop-interval-0s)
             monitor interval=1min (WebSite-monitor-interval-1min)

....

. Point your browser to the website you created using the floating IP address you configured. This should display the text message you defined.

. Stop the apache web service and check the cluster status. Using [command]`killall -9` simulates an application-level crash.
+
[subs="+quotes"]
....

# *killall -9 httpd*

....
+
Check the cluster status. You should see that stopping the web service caused a failed action, but that the cluster software restarted the service and you should still be able to access the website.
+
[subs="+quotes"]
....

# *pcs status*
Cluster name: my_cluster
...
Current DC: z1.example.com (version 1.1.13-10.el7-44eb2dd) - partition with quorum
1 node and 2 resources configured

Online: [ z1.example.com ]

Full list of resources:

Resource Group: apachegroup
    ClusterIP  (ocf::heartbeat:IPaddr2):       Started z1.example.com
    WebSite    (ocf::heartbeat:apache):        Started z1.example.com

Failed Resource Actions:
* WebSite_monitor_60000 on z1.example.com 'not running' (7): call=13, status=complete, exitreason='none',
    last-rc-change='Thu Oct 11 23:45:50 2016', queued=0ms, exec=0ms

PCSD Status:
    z1.example.com: Online

....
+
You can clear the failure status on the resource that failed once the service is up and running again and the failed action notice will no longer appear when you view the cluster status.
+
[subs="+quotes"]
....

# *pcs resource cleanup WebSite*

....

. When you are finished looking at the cluster and the cluster status, stop the cluster services on the node. Even though you have only started services on one node for this introduction, the `--all` parameter is included since it would stop cluster services on all nodes on an actual multi-node cluster.
+
[subs="+quotes"]
....

# *pcs cluster stop --all*

....
