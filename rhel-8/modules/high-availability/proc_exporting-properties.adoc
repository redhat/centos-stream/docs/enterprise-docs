:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_controlling-cluster-behavior.adoc


[id='proc_exporting-properties-{context}']


= Exporting cluster properties as `pcs` commands

[role="_abstract"]
ifeval::[{ProductNumber} == 8]
As of Red Hat Enterprise Linux 8.9,
endif::[]
ifeval::[{ProductNumber} == 9]
As of Red Hat Enterprise Linux 9.3,
endif::[]
you can display the `pcs` commands that can be used to re-create configured cluster properties on a different system using the `--output-format=cmd` option of the `pcs property config` command.

The following command sets the `migration-limit` cluster property to 10.

[subs="+quotes"]
....

# *pcs property set migration-limit=10*

....

After you set the cluster property, the following command displays the `pcs` command you can use to set the cluster property on a different system.

[subs="+quotes"]
....

# *pcs property config --output-format=cmd*
pcs property set --force -- \
 migration-limit=10 \
 placement-strategy=minimal

....

