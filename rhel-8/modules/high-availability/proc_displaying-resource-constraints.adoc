:_mod-docs-content-type: PROCEDURE
// Module included in the following title
//
// rhel-8-docs/enterprise/titles/configuring-and-maintaining/configuring-and-managing-high-availability/master.adoc

[id='proc_displaying-resource-constraints.adoc-{context}']

= Displaying resource constraints and resource dependencies

[role="_abstract"]
There are a several commands you can use to display constraints that have been configured. You can display all configured resource constraints, or you can limit the display of esource constraints to specific types of resource constraints. Additionally, you can display configured resource dependencies.

.Displaying all configured constraints

The following command lists all current location, order, and colocation constraints. If the `--full` option is specified, show the internal constraint IDs.

....

pcs constraint [list|show] [--full]

....

ifeval::[{ProductNumber} == 8]
As of RHEL 8.2, listing resource constraints no longer by default displays expired constraints.
endif::[]
ifeval::[{ProductNumber} == 9]
By default, listing resource constraints does not display expired constraints.
endif::[]
To include expired constaints in the listing, use the `--all` option of the `pcs constraint` command. This will list expired constraints, noting the constraints and their associated rules as `(expired)` in the display.

.Displaying location constraints

The following command lists all current location constraints.

* If `resources` is specified, location constraints are displayed per resource. This is the default behavior.

* If `nodes` is specified, location constraints are displayed per node.

* If specific resources or nodes are specified, then only information about those resources or nodes is displayed.

[subs="+quotes"]
....

pcs constraint location [show [resources [_resource_...]] | [nodes [_node_...]]] [--full]

....

.Displaying ordering constraints

The following command lists all current ordering constraints.

....

pcs constraint order [show] 

....

.Displaying colocation constraints


The following command lists all current colocation constraints.

....

pcs constraint colocation [show] 

....

.Displaying resource-specific constraints


The following command lists the constraints that reference
specific resources.

[subs="+quotes"]
....

pcs constraint ref _resource_ ...

....

ifeval::[{ProductNumber} == 8]
.Displaying resource dependencies (Red Hat Enterprise Linux 8.2 and later)
endif::[]
ifeval::[{ProductNumber} == 9]
.Displaying resource dependencies 
endif::[]

The following command displays the relations between cluster resources in a tree structure.


[subs="+quotes"]
....

pcs resource relations _resource_ [--full]

....

If the `--full` option is used, the command displays additional information, including
the constraint IDs and the resource types.

In the following example, there are 3 configured resources: C, D, and E.

[subs="+quotes"]
....


# *pcs constraint order start C then start D*
Adding C D (kind: Mandatory) (Options: first-action=start then-action=start)
# *pcs constraint order start D then start E*
Adding D E (kind: Mandatory) (Options: first-action=start then-action=start)

# *pcs resource relations C*
C
`- order
   |  start C then start D
   `- D
      `- order
         |  start D then start E
         `- E
# *pcs resource relations D*
D
|- order
|  |  start C then start D
|  `- C
`- order
   |  start D then start E
   `- E
# pcs *resource relations E*
E
`- order
   |  start D then start E
   `- D
      `- order
         |  start C then start D
         `- C
....

In the following example, there are 2 configured resources: A and B.
Resources A and B are part of resource group G.

[subs="+quotes"]
....

# *pcs resource relations A*
A
`- outer resource
   `- G
      `- inner resource(s)
         |  members: A B
         `- B
# *pcs resource relations B*
B
`- outer resource
   `- G
      `- inner resource(s)
         |  members: A B
         `- A
# *pcs resource relations G*
G
`- inner resource(s)
   |  members: A B
   |- A
   `- B
....
