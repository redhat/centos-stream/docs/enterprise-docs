:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_resource-monitoring-operations.adoc

[id='proc_configuring-resource-monitoring-operations-{context}']

= Configuring resource monitoring operations

[role="_abstract"]
You can configure monitoring operations when you create a resource with the following command.

[subs="+quotes"]
....

pcs resource create _resource_id_ _standard:provider:type|type_ [_resource_options_] [op _operation_action_ _operation_options_ [_operation_type_ _operation_options_]...]

....

For example, the following command creates an `IPaddr2` resource with a monitoring operation. The new resource is called `VirtualIP` with  an IP address of 192.168.0.99 and a netmask of 24 on `eth2`. A monitoring operation will be performed every 30 seconds.

[subs="+quotes"]
....

# *pcs resource create VirtualIP ocf:heartbeat:IPaddr2 ip=192.168.0.99 cidr_netmask=24 nic=eth2 op monitor interval=30s*

....

Alternately, you can add a monitoring operation to an existing resource with the following command.

[subs="+quotes"]
....

pcs resource op add _resource_id_ _operation_action_ [_operation_properties_]

....

Use the following command to delete a configured resource operation.

[subs="+quotes"]
....

pcs resource op remove _resource_id_ _operation_name_ _operation_properties_

....

[NOTE]
====

You must specify the exact operation properties to properly remove an existing operation.

====

To change the values of a monitoring option, you can update the resource. For example, you can create a `VirtualIP` with the following command.

[subs="+quotes"]
....

# *pcs resource create VirtualIP ocf:heartbeat:IPaddr2 ip=192.168.0.99 cidr_netmask=24 nic=eth2*

....

By default, this command creates these operations.

....

Operations: start interval=0s timeout=20s (VirtualIP-start-timeout-20s)
            stop interval=0s timeout=20s (VirtualIP-stop-timeout-20s)
            monitor interval=10s timeout=20s (VirtualIP-monitor-interval-10s)

....

To change the stop timeout operation, execute the following command.

[subs="+quotes"]
....

# *pcs resource update VirtualIP op stop interval=0s timeout=40s*

# *pcs resource config VirtualIP*
 Resource: VirtualIP (class=ocf provider=heartbeat type=IPaddr2)
  Attributes: ip=192.168.0.99 cidr_netmask=24 nic=eth2
  Operations: start interval=0s timeout=20s (VirtualIP-start-timeout-20s)
              monitor interval=10s timeout=20s (VirtualIP-monitor-interval-10s)
              stop interval=0s timeout=40s (VirtualIP-name-stop-interval-0s-timeout-40s) 

....

