:_mod-docs-content-type: PROCEDURE

[id="configuring-a-block-storage-resource-in-an-ha-cluster-on-red-hat-openstack-platform_{context}"]
= Configuring a block storage resource in an HA cluster on Red Hat OpenStack Platform

The following procedure creates a block storage resource for an HA cluster on RHOSP. This procedure uses a `clouds.yaml` file for RHOSP authentication.

.Prerequisites

* A configured HA cluster running on RHOSP
* A block storage volume created by the RHOSP administrator
* Access to the RHOSP APIs, using the RHOSP authentication method you will use for cluster configuration, as described in xref:authentication-methods-for-rhosp_configurng-a-red-hat-high-availability-cluster-on-red-hat-openstack-platform[Setting up an authentication method for RHOSP]


.Procedure

Complete the following steps from any node in the cluster.

. To view the options for the `openstack-cinder-volume` resource agent, run the following command.
+ 
[subs="+quotes"]
....

# *pcs resource describe openstack-cinder-volume*

....

. Determine the volume ID of the block storage volume you are configuring as a cluster resource.
+
Run the following command to display a table of available volumes that includes the UUID and name of each volume.
+
[subs="+quotes"]
....

# *openstack --os-cloud=ha-example volume list*
| ID                                  | Name                        |
| 23f67c9f-b530-4d44-8ce5-ad5d056ba926| testvolume-cinder-data-disk |

....
+
If you already know the volume name, you can run the following command, specifying the volume you are configuring. This displays a table with an ID field.
+
[subs="+quotes"]
....

# *openstack --os-cloud=ha-example volume show testvolume-cinder-data-disk*

....

. Create the block storage resource, specifying the ID for the volume.
+
[subs="+quotes"]
....

# *pcs resource create cinder-vol openstack-cinder-volume volume_id="23f67c9f-b530-4d44-8ce5-ad5d056ba926" cloud="ha-example"*

....

. Configure an ordering constraint to ensure that the `openstack-info` resource starts before the block storage resource.
+
[subs="+quotes"]
....

# *pcs constraint order start openstack-info-clone then cinder-vol*
Adding openstack-info-clone cinder-vol (kind: Mandatory) (Options: first-action=start then-action=start

....

. Configure a location constraint to ensure that the block storage resource runs on the same node as the `openstack-info` resource.
+
[subs="+quotes"]
....

# *pcs constraint colocation add cinder-vol with openstack-info-clone score=INFINITY*

....

.Verification

. Verify the resource constraint configuration.
+
[subs="+quotes"]
....

# *pcs constraint config*
Location Constraints:
Ordering Constraints:
  start openstack-info-clone then start cinder-vol (kind:Mandatory)
Colocation Constraints:
  cinder-vol with openstack-info-clone (score:INFINITY)

....

. Check the cluster status to verify that the resource is running.
+
[subs="+quotes"]
....

# *pcs status*
. . .
Full List of Resources:
  * Clone Set: openstack-info-clone [openstack-info]:
    * Started: [ node01 node02 node03 ]
  * cinder-vol  (ocf::heartbeat:openstack-cinder-volume):        Started node03
  * fenceopenstack      (stonith:fence_openstack):       Started node01

....
