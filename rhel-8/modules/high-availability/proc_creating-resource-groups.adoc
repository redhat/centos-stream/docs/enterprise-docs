:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_resource-groups.adoc

[id='proc_creating-resource-groups-{context}']

= Configuring resource groups

[role="_abstract"]
One of the most common elements of a cluster is a set of resources that need to be located together, start sequentially, and stop in the reverse order. To simplify this configuration, Pacemaker supports the concept of resource groups.

== Creating a resource group

You create a resource group with the following command, specifying the resources to include in the group. If the group does not exist, this command creates the group. If the group exists, this command adds additional resources to the group. The resources will start in the order you specify them with this command, and will stop in the reverse order of their starting order.

[subs="+quotes"]
....

pcs resource group add _group_name_ _resource_id_ [_resource_id_] ... [_resource_id_] [--before _resource_id_ | --after _resource_id_]

....

You can use the [option]`--before` and [option]`--after` options of this command to specify the position of the added resources relative to a resource that already exists in the group.

You can also add a new resource to an existing group when you create the resource, using the following command. The resource you create is added to the group named _group_name_.  If the group _group_name_ does not exist, it will be created.

[subs="+quotes"]
....

pcs resource create _resource_id_ [_standard_:[_provider_:]]_type_ [resource_options] [op _operation_action_ _operation_options_] --group _group_name_

....

There is no limit to the number of resources a group can contain. The fundamental properties of a group are as follows.

* Resources are colocated within a group.

* Resources are started in the order in which you specify them. If a resource in the group cannot run anywhere, then no resource specified after that resource is allowed to run.

* Resources are stopped in the reverse order in which you specify them.

The following example creates a resource group named `shortcut` that contains the existing resources `IPaddr` and `Email`.

[subs="+quotes"]
....

# *pcs resource group add shortcut IPaddr Email*

....

In this example:

* The `IPaddr` is started first,
then `Email`.

* The `Email` resource is stopped first, then `IPAddr`.

* If `IPaddr` cannot run anywhere, neither can `Email`.

* If `Email` cannot run anywhere, however,
this does not affect `IPaddr`
in any way.

== Removing a resource group

You remove a resource from a group with the following command. If there are no remaining resources in the group, this command removes the group itself.

[subs="+quotes"]
....

pcs resource group remove _group_name_ _resource_id_...

....

== Displaying resource groups

The following command lists all currently configured resource
groups.

....

pcs resource group list

....

[[s2-group_options-HAAR]]
== Group options

You can set the following options for a resource group, and they maintain the same meaning as when they are set for a single resource: `priority`, `target-role`, `is-managed`. For information about resource meta options, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_configuring-cluster-resources-configuring-and-managing-high-availability-clusters#proc_configuring-resource-meta-options-configuring-cluster-resources[Configuring resource meta options].

[[s2-group_stickiness-HAAR]]
== Group stickiness

Stickiness, the measure of how much a resource wants to stay where it is, is additive in groups. Every active resource of the group will contribute its stickiness value to the group’s total. So if the default `resource-stickiness` is 100, and a group has seven members, five of which are active, then the group as a whole will prefer its current location with a score of 500.

