:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_determining-which-node-a-resource-runs-on.adoc

[id='proc_configuring-location-constraints-{context}']

= Configuring location constraints

[role="_abstract"]
You can configure a basic location constraint to specify whether a resource prefers or avoids a node, with an optional `score` value to indicate the relative degree of preference for the constraint.

The following command creates a location constraint for a resource to prefer the specified node or nodes. Note that it is possible to create constraints on a particular resource for more than one node with a single command.

[subs="+quotes"]
....

pcs constraint location _rsc_ prefers _node_[=_score_] [_node_[=_score_]] ...

....

The following command creates a location constraint for a resource to avoid the specified node or nodes.

[literal,subs="+quotes"]
....

pcs constraint location _rsc_ avoids _node_[=_score_] [_node_[=_score_]] ...

....

The following table summarizes the meanings of the basic options for configuring location constraints.

[id="tb-locationconstraint-options-HAAR-{context}"]
.Location Constraint Options

[cols="2,4" options="header"]
|===
|Field|Description
|`rsc`|A resource name
|`node`|A node’s name
|`score`|Positive integer value to indicate the degree of preference for whether the given resource should prefer or avoid the given node. `INFINITY` is the default `score` value for a resource location constraint.

A value of `INFINITY` for `score` in a `pcs constraint location _rsc_ prefers` command indicates that the resource will prefer that node if the node is available, but does not prevent the resource from running on another node if the specified node is unavailable.

A value of `INFINITY` for `score` in a `pcs constraint location _rsc_ avoids` command indicates that the resource will never run on that node, even if no other node is available. This is the equivalent of setting a `pcs constraint location add` command with a score of `-INFINITY`.

A numeric score (that is, not `INFINITY`) means the constraint is optional, and will be honored unless some other factor outweighs it. For example, if the resource is already placed on a different node, and its `resource-stickiness` score is higher than a `prefers` location constraint's score, then the resource will be left where it is.
|===

The following command creates a location constraint to specify that the resource `Webserver` prefers node `node1`.

[subs="+quotes"]
....

# *pcs constraint location Webserver prefers node1*

....

[command]`pcs` supports regular expressions in location constraints on the command line. These constraints apply to multiple resources based on the regular expression matching resource name. This allows you to configure multiple location constraints with a single command line.

The following command creates a location constraint to specify that resources `dummy0` to `dummy9` prefer `node1`.

[subs="+quotes"]
....

# *pcs constraint location 'regexp%dummy[0-9]' prefers node1*

....

Since Pacemaker uses POSIX extended regular expressions as documented at link:++http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_04++[http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_04], you can specify the same constraint with the following command.

[subs="+quotes"]
....

# *pcs constraint location 'regexp%dummy[[:digit:]]' prefers node1*

....
