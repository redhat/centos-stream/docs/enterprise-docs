:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_cluster-maintenance.adoc

[id='proc_manually-move-resources-{context}']

= Manually moving cluster resources

[role="_abstract"]
You can override the cluster and force resources to move from their current location. There are two occasions when you would want to do this:

* When a node is under maintenance, and you need to move all resources running on that node to a different node

* When individually specified resources needs to be moved

To move all resources running on a node to a different node, you put the node in standby mode.
//  For information about putting a cluster node in standby node.
// , see <<s2-standbymode-HAAR>>.

You can move individually specified resources in either of the following ways.

* You can use the [command]`pcs resource move` command to move a resource off a node on which it is currently running.
// , as described in <<s2-moving_resources_resourcemove-HAAR>>.

* You can use the [command]`pcs resource relocate run` command to move a resource to its preferred node, as determined by current cluster status, constraints, location of resources and other settings.
//  For information about this command, see <<s2-moving_resources_relocate-HAAR>>.


== Moving a resource from its current node

To move a resource off the node on which it is currently running, use the following command, specifying the _resource_id_ of the resource as defined. Specify the `destination_node` if you want to indicate on which node to run the resource
that you are moving.

ifeval::[{ProductNumber} == 8]
[subs="+quotes"]
....

pcs resource move _resource_id_ [_destination_node_] [--master] [lifetime=_lifetime_]

....

[NOTE]
====

When you run the [command]`pcs resource move` command, this adds a constraint to the resource to prevent it from running on the node on which it is currently running. As of RHEL 8.6, you can specify the `--autodelete` option for this command, which will cause the location constraint that this command creates to be removed automatically once the resource has been moved.  For earlier releases, you can run the [command]`pcs resource clear` or the [command]`pcs constraint delete` command to remove the constraint manually. Removing the constraint does not necessarily move the resources back to the original node; where the resources can run at that point depends on how you have configured your resources initially.

====

If you specify the `--master` parameter of the [command]`pcs resource move` command, the constraint applies only to promoted instances of the resource.

You can optionally configure a `lifetime` parameter for the `pcs resource move` command to indicate a period of time the constraint should remain. You specify the units of a `lifetime` parameter according to the format defined in ISO 8601, which requires that you specify the unit as a capital letter such as Y (for years), M (for months), W (for weeks), D (for days), H (for hours), M (for minutes), and S (for seconds).

To distinguish a unit of minutes(M) from a unit of months(M), you must specify PT before indicating the value in minutes. For example, a `lifetime` parameter of 5M indicates an interval of five months, while a `lifetime` parameter of PT5M indicates an interval of five minutes.

The following command moves the resource `resource1` to node `example-node2` and prevents it from moving back to the node on which it was originally running for one hour and thirty minutes.

....

pcs resource move resource1 example-node2 lifetime=PT1H30M

....

The following command moves the resource `resource1` to node `example-node2` and prevents it from moving back to the node on which it was originally running for thirty minutes.

....

pcs resource move resource1 example-node2 lifetime=PT30M

....

endif::[]

ifeval::[{ProductNumber} == 9]
[subs="+quotes"]
....
 
pcs resource move _resource_id_ [_destination_node_] [--promoted] [--strict] [--wait[=_n_]]

....

When you execute the [command]`pcs resource move` command, this adds a constraint to the resource to prevent it from running on the node on which it is currently running. By default, the location constraint that the command creates is automatically removed once the resource has been moved. If removing the constraint would cause the resource to move back to the original node, as might happen if the `resource-stickiness` value for the resource is 0, the `pcs resource move` command fails. If you would like to move a resource and leave the resulting constraint in place, use the `pcs resource move-with-constraint` command.

If you specify the `--promoted` parameter of the [command]`pcs resource move` command, the constraint applies only to promoted instances of the resource.

If you specify the `--strict` parameter of the [command]`pcs resource move` command, the command will fail if other resources than the one specified in the command would be affected.

You can optionally configure a `--wait[=_n_]` parameter for the `pcs resource move` command to indicate the number of seconds to wait for the resource to start on the destination node before returning 0 if the resource is started or 1 if the resource has not yet started. If you do not specify n, it defaults to a value of 60 minutes.
endif::[]

// LINK TO For information about resource constraints, see
// <<ch-resourceconstraints-HAAR>>.


== Moving a resource to its preferred node

After a resource has moved, either due to a failover or to an administrator manually moving the node, it will not necessarily move back to its original node even after the circumstances that caused the failover have been corrected. To relocate resources to their preferred node, use the following command. A preferred node is determined by the current cluster status, constraints, resource location, and other settings and may change over time.

[subs="+quotes"]
....

pcs resource relocate run [_resource1_] [_resource2_] ...

....

If you do not specify any resources, all resource are relocated to their preferred nodes.

This command calculates the preferred node for each resource while ignoring resource stickiness. After calculating the preferred node, it creates location constraints which will cause the resources to move to their preferred nodes. Once the resources have been moved, the constraints are deleted automatically. To remove all constraints created by the [command]`pcs resource relocate run` command, you can enter the [command]`pcs resource relocate clear` command. To display the current status of resources and their optimal node ignoring resource stickiness, enter the [command]`pcs resource relocate show` command.

