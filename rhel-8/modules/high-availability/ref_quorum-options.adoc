:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-cluster-quorum.adoc

[id='ref_quorum-options-{context}']

= Configuring quorum options

[role="_abstract"]
There are some special features of quorum configuration that you can set when you create a cluster with the [command]`pcs cluster setup` command. The following table summarizes these options.

[[tb-quorumoptions-HAAR]]
.Quorum Options

[cols="4,5" options="header"]
|===
|Option|Description
|`auto_tie_breaker`|When enabled, the cluster can suffer up to 50% of the nodes failing at the same time, in a deterministic fashion. The cluster partition, or the set of nodes that are still in contact with the `nodeid` configured in `auto_tie_breaker_node` (or lowest `nodeid` if not set), will remain quorate. The other nodes will be inquorate.

The [option]`auto_tie_breaker` option is principally used for clusters with an even number of nodes, as it allows the cluster to continue operation with an even split. For more complex failures, such as multiple, uneven splits, it is recommended that you use a quorum device.

The [option]`auto_tie_breaker` option is incompatible with quorum devices.
|`wait_for_all`|When enabled, the cluster will be quorate for the first time only after all nodes have been visible at least once at the same time.

The [option]`wait_for_all` option is primarily used for two-node clusters and for even-node clusters using the quorum device `lms` (last man standing) algorithm.

The [option]`wait_for_all` option is automatically enabled when a cluster has two nodes, does not use a quorum device, and [option]`auto_tie_breaker` is disabled. You can override this by explicitly setting [option]`wait_for_all` to 0.
|`last_man_standing`|When enabled, the cluster can dynamically recalculate `expected_votes` and quorum under specific circumstances. You must enable `wait_for_all` when you enable this option. The `last_man_standing` option is incompatible with quorum devices.
|`last_man_standing_window`|The time, in milliseconds, to wait before recalculating `expected_votes` and quorum after a cluster loses nodes.
|===

For further information about configuring and using these options, see the [command]`votequorum`(5) man page.

