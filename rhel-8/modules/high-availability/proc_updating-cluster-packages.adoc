:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_cluster-maintenance.adoc

[id='proc_updating-cluster-packages-{context}']

= Updating a RHEL high availability cluster

[role="_abstract"]
Updating packages that make up the RHEL High Availability and Resilient Storage Add-Ons, either individually or as a whole, can be done in one of two general ways:

* _Rolling Updates_: Remove one node at a time from service, update its software, then integrate it back into the cluster. This allows the cluster to continue providing service and managing resources while each node is updated.

* _Entire Cluster Update_: Stop the entire cluster, apply updates to all nodes, then start the cluster back up.

[WARNING]
====

It is critical that when performing software update procedures for Red Hat Enterprise Linux High Availability and Resilient Storage clusters, you ensure that any node that will undergo updates is not an active member of the cluster before those updates are initiated.

====

For a full description of each of these methods and the procedures to follow for the updates, see link:++https://access.redhat.com/articles/2059253/++[Recommended Practices for Applying Software Updates to a RHEL High Availability or Resilient Storage Cluster].

