:_mod-docs-content-type: PROCEDURE

[id="configuring-a-floating-ip-address-in-an-ha-cluster-on-red-hat-openstack-platform_{context}"]
= Configuring a floating IP address in an HA cluster on Red Hat OpenStack Platform

The following procedure creates a floating IP address resource for an HA cluster on RHOSP. This procedure uses a `clouds.yaml` file for RHOSP authentication.

.Prerequisites

* A configured HA cluster running on RHOSP
* An IP address on the public network to use as the floating IP address, assigned by the RHOSP administrator
* Access to the RHOSP APIs, using the RHOSP authentication method you will use for cluster configuration, as described in xref:authentication-methods-for-rhosp_configurng-a-red-hat-high-availability-cluster-on-red-hat-openstack-platform[Setting up an authentication method for RHOSP]


.Procedure

Complete the following steps from any node in the cluster.

. To view the options for the `openstack-floating-ip` resource agent, run the following command.
+ 
[subs="+quotes"]
....

# *pcs resource describe openstack-floating-ip*

....

. Find the subnet ID for the address on the public network that you will use to create the floating IP address resource.  

.. The public network is usually the network with the default gateway. Run the following command to display the default gateway address.
+ 
[subs="+quotes"]
....

# *route -n | grep ^0.0.0.0 | awk '{print $2}'*
172.16.0.1

....

.. Run the following command to find the subnet ID for the public network. This command generates a table with ID and Subnet headings. 
+ 
[subs="+quotes"]
....

# *openstack --os-cloud=ha-example subnet list*
+-------------------------------------+---+---------------+
| ID                                   |   | Subnet         
+-------------------------------------+---+---------------+
| 723c5a77-156d-4c3b-b53c-ee73a4f75185 |   | 172.16.0.0/24 |
+--------------------------------------+------------------+

....

. Create the floating IP address resource, specifying the public IP address for the resource and the subnet ID for that address. When you configure the floating IP address resource, the resource agent configures a virtual IP address on the public network and associates it with a cluster node. 
+ 
[subs="+quotes"]
....

# *pcs resource create float-ip openstack-floating-ip cloud="ha-example" ip_id="10.19.227.211" subnet_id="723c5a77-156d-4c3b-b53c-ee73a4f75185"*

....

. Configure an ordering constraint to ensure that the `openstack-info` resource starts before the floating IP address resource.
+ 
[subs="+quotes"]
....

# *pcs constraint order start openstack-info-clone then  float-ip*
Adding openstack-info-clone float-ip (kind: Mandatory) (Options: first-action=start then-action=start

....

. Configure a location constraint to ensure that the floating IP address resource runs on the same node as the `openstack-info` resource.
+ 
[subs="+quotes"]
....

# *pcs constraint colocation add float-ip with openstack-info-clone score=INFINITY*

....

.Verification

. Verify the resource constraint configuration.
+ 
[subs="+quotes"]
....

# *pcs constraint config*
Location Constraints:
Ordering Constraints:
  start openstack-info-clone then start float-ip (kind:Mandatory)
Colocation Constraints:
  float-ip with openstack-info-clone (score:INFINITY)

....

. Check the cluster status to verify that the resources are running.
+ 
[subs="+quotes"]
....

# *pcs status*
. . .
Full List of Resources:
  * fenceopenstack      (stonith:fence_openstack):       Started node01
  * Clone Set: openstack-info-clone [openstack-info]:
    * Started: [ node01 node02 node03 ]
  * float-ip    (ocf::heartbeat:openstack-floating-ip):  Started node02

....
