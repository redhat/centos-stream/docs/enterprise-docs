:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_cluster-maintenance.adoc

[id='proc_upgrading-remote-nodes-{context}']


= Upgrading remote nodes and guest nodes

[role="_abstract"]
If the `pacemaker_remote` service is stopped on an active remote node or guest node, the cluster will gracefully migrate resources off the node before stopping the node. This allows you to perform software upgrades and other routine maintenance procedures without removing the node from the cluster. Once `pacemaker_remote` is shut down, however, the cluster will immediately try to reconnect. If `pacemaker_remote` is not restarted within the resource's monitor timeout, the cluster will consider the monitor operation as failed.

If you wish to avoid monitor failures when the `pacemaker_remote` service is stopped on an active Pacemaker Remote node, you can use the following procedure to take the node out of the cluster before performing any system administration that might stop `pacemaker_remote`.

.Procedure

. Stop the node's connection resource with the [command]`pcs resource disable _resourcename_pass:attributes[{blank}]` command, which will move all services off the node. The connection resource would be the `ocf:pacemaker:remote` resource for a remote node or, commonly, the `ocf:heartbeat:VirtualDomain` resource for a guest node. For guest nodes, this command will also stop the VM, so the VM must be started outside the cluster (for example, using [command]`virsh`) to perform any maintenance.
+
[subs="quotes"]
....

pcs resource disable _resourcename_

....

. Perform the required maintenance.

. When ready to return the node to the cluster, re-enable the resource with the [command]`pcs resource enable` command.
+
[subs="quotes"]
....

pcs resource enable _resourcename_

....
