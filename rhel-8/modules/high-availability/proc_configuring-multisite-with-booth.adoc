:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-multisite-cluster.adoc

[id='proc-configuring-multisite-with-booth-{context}']

= Configuring multi-site clusters with Pacemaker

[role="_abstract"]
You can configure a multi-site configuration that uses the Booth ticket manager with the following procedure.

These example commands use the following arrangement:

* Cluster 1 consists of the nodes `cluster1-node1` and `cluster1-node2`

* Cluster 1 has a floating IP address assigned to it of 192.168.11.100

* Cluster 2 consists of `cluster2-node1` and `cluster2-node2`

* Cluster 2 has a floating IP address assigned to it of 192.168.22.100

* The arbitrator node is `arbitrator-node` with an ip address of 192.168.99.100

* The name of the Booth ticket that this configuration uses is `apacheticket`

These example commands assume that the cluster resources for an Apache service have been configured as part of the resource group `apachegroup` for each cluster. It is not required that the resources and resource groups be the same on each cluster to configure a ticket constraint for those resources, since the Pacemaker instance for each cluster is independent, but that is a common failover scenario.

Note that at any time in the configuration procedure you can enter the [command]`pcs booth config` command to display the booth configuration for the current node or cluster or the [command]`pcs booth status` command to display the current status of booth on the local node.

.Procedure

. Install the `booth-site` Booth ticket manager package on each node of both clusters.
+
[subs="+quotes,attributes"]
....

[root@cluster1-node1 ~]# *{PackageManagerCommand} install -y booth-site*
[root@cluster1-node2 ~]# *{PackageManagerCommand} install -y booth-site*
[root@cluster2-node1 ~]# *{PackageManagerCommand} install -y booth-site*
[root@cluster2-node2 ~]# *{PackageManagerCommand} install -y booth-site*

....

. Install the `pcs`, `booth-core`, and `booth-arbitrator` packages on the arbitrator node.
+
[subs="+quotes,attributes"]
....

[root@arbitrator-node ~]# *{PackageManagerCommand} install -y pcs booth-core booth-arbitrator*

....

. If you are running the `firewalld` daemon, execute the following commands on all nodes in both clusters as well as on the arbitrator node to enable the ports that are required by the Red Hat High Availability Add-On.
+
[subs="+quotes"]
....

# *firewall-cmd --permanent --add-service=high-availability*
# *firewall-cmd --add-service=high-availability*

....
+
You may need to modify which ports are open to suit local conditions. For more information about the ports that are required by the Red Hat High-Availability Add-On, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_creating-high-availability-cluster-configuring-and-managing-high-availability-clusters#proc_enabling-ports-for-high-availability-creating-high-availability-cluster[Enabling ports for the High Availability Add-On].

. Create a Booth configuration on one node of one cluster. The addresses you specify for each cluster and for the arbitrator must be IP addresses. For each cluster, you specify a floating IP address.
+
[subs="+quotes"]
....

[cluster1-node1 ~] # *pcs booth setup sites 192.168.11.100 192.168.22.100 arbitrators 192.168.99.100*

....
+
This command creates the configuration files `/etc/booth/booth.conf` and `/etc/booth/booth.key` on the node from which it is run.

. Create a ticket for the Booth configuration. This is the ticket that you will use to define the resource constraint that will allow resources to run only when this ticket has been granted to the cluster.
+
This basic failover configuration procedure uses only one ticket, but you can create additional tickets for more complicated scenarios where each ticket is associated with a different resource or resources.
+
[subs="+quotes"]
....

[cluster1-node1 ~] # *pcs booth ticket add apacheticket*

....

. Synchronize the Booth configuration to all nodes in the current cluster.
+
[subs="+quotes"]
....

[cluster1-node1 ~] # *pcs booth sync*

....

. From the arbitrator node, pull the Booth configuration to the arbitrator. If you have not previously done so, you must first authenticate `pcs` to the node from which you are pulling the configuration.
+
[subs="+quotes"]
....

[arbitrator-node ~] # *pcs host auth cluster1-node1*
[arbitrator-node ~] # *pcs booth pull cluster1-node1*

....

. Pull the Booth configuration to the other cluster and synchronize to all the nodes of that cluster. As with the arbitrator node, if you have not previously done so, you must first authenticate `pcs` to the node from which you are pulling the configuration.
+
[subs="+quotes"]
....

[cluster2-node1 ~] # *pcs host auth cluster1-node1*
[cluster2-node1 ~] # *pcs booth pull cluster1-node1*
[cluster2-node1 ~] # *pcs booth sync*

....

. Start and enable Booth on the arbitrator.
+
[NOTE]
====

You must not manually start or enable Booth on any of the nodes of the clusters since Booth runs as a Pacemaker resource in those clusters.

====
+
[subs="+quotes"]
....

[arbitrator-node ~] # *pcs booth start*
[arbitrator-node ~] # *pcs booth enable*

....

. Configure Booth to run as a cluster resource on both cluster sites, using the floating IP addresses assigned to each cluster. This creates a resource group with `booth-ip` and `booth-service` as members of that group.
+
[subs="+quotes"]
....

[cluster1-node1 ~] # *pcs booth create ip 192.168.11.100*
[cluster2-node1 ~] # *pcs booth create ip 192.168.22.100*

....

. Add a ticket constraint to the resource group you have defined for each cluster.
+
[subs="+quotes"]
....

[cluster1-node1 ~] # *pcs constraint ticket add apacheticket apachegroup*
[cluster2-node1 ~] # *pcs constraint ticket add apacheticket apachegroup*

....
+
You can enter the following command to display the currently configured ticket constraints.
+
....

pcs constraint ticket [show]

....

. Grant the ticket you created for this setup to the first cluster.
+
Note that it is not necessary to have defined ticket constraints before granting a ticket. Once you have initially granted a ticket to a cluster, then Booth takes over ticket management unless you override this manually with the [command]`pcs booth ticket revoke` command. For information about the [command]`pcs booth` administration commands, see the PCS help screen for the [command]`pcs booth` command.
+
[subs="+quotes"]
....

[cluster1-node1 ~] # *pcs booth ticket grant apacheticket*

....

It is possible to add or remove tickets at any time, even after completing this procedure. After adding or removing a ticket, however, you must synchronize the configuration files to the other nodes and clusters as well as to the arbitrator and grant the ticket as is shown in this procedure.

For information about additional Booth administration commands that you can use for cleaning up and removing Booth configuration files, tickets, and resources, see the PCS help screen for the [command]`pcs booth` command.
