:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies:
//
// assembly_creating-multinode-resources.adoc


[id='proc_creating-promotable-clone-resources-{context}']

= Promotable clone resources

[role="_abstract"]
Promotable clone resources are clone resources with the `promotable` meta attribute set to `true`. They allow the instances to be in one of two operating modes; these are
ifeval::[{ProductNumber} == 8]
called `master` and `slave`.
endif::[]
ifeval::[{ProductNumber} == 9]
called `promoted` and `unpromoted`.
endif::[]
The names of the modes do not have specific meanings, except for the limitation that when an instance is started, it
ifeval::[{ProductNumber} == 8]
must come up in the `Slave` state.
endif::[]
ifeval::[{ProductNumber} == 9]
must come up in the `Unpromoted` state. Note: The Promoted and Unpromoted role names are the functional equivalent of the Master and Slave Pacemaker roles in previous RHEL releases.
endif::[]


== Creating a promotable clone resource

You can create a resource as a promotable clone with the following single command.

ifeval::[{ProductNumber} == 8]
RHEL 8.4 and later:
endif::[]


[subs="+quotes"]
....

pcs resource create _resource_id_ [_standard_:[_provider_:]]_type_ [_resource options_] promotable [_clone_id_] [_clone options_]

....

ifeval::[{ProductNumber} == 8]
RHEL 8.3 and earlier:

[subs="+quotes"]
....

pcs resource create _resource_id_ [_standard_:[_provider_:]]_type_ [_resource options_] promotable [_clone options_]

....
endif::[]


By default, the name of the promotable clone will be `pass:attributes[{blank}]_resource_id_-clone`.

ifeval::[{ProductNumber} == 8]
As of RHEL 8.4, you can set a custom name for the clone by specifying a value for the _clone_id_ option.
endif::[]

ifeval::[{ProductNumber} == 9]
You can set a custom name for the clone by specifying a value for the _clone_id_ option.
endif::[]

Alternately, you can create a promotable resource from a previously-created resource or resource group with the following command.

ifeval::[{ProductNumber} == 8]
RHEL 8.4 and later:
endif::[]

[subs="+quotes"]

....

pcs resource promotable _resource_id_ [_clone_id_] [_clone options_]

....

ifeval::[{ProductNumber} == 8]
RHEL 8.3 and earlier:

[subs="+quotes"]

....

pcs resource promotable _resource_id_ [_clone options_]

....
endif::[]


By default, the name of the promotable clone will be `pass:attributes[{blank}]_resource_id_-clone` or `pass:attributes[{blank}]_group_name_-clone`.

ifeval::[{ProductNumber} == 8]
As of RHEL 8.4, you can
set a custom name for the clone by specifying a value for the _clone_id_ option.
endif::[]
ifeval::[{ProductNumber} == 9]
You can
set a custom name for the clone by specifying a value for the _clone_id_ option.
endif::[]


The following table describes the extra clone options you can specify for a promotable resource.

[[tb-promotablecloneoptions-HAAR]]
.Extra Clone Options Available for Promotable Clones

[options="header"]
|===
|Field|Description
|`promoted-max`|How many copies of the resource can be promoted; default 1.
|`promoted-node-max`|How many copies of the resource can be promoted on a single node; default 1.
|===


== Configuring promotable resource constraints

In most cases, a promotable resource will have a single copy on each active cluster node. If this is not the case, you can indicate which nodes the cluster should preferentially assign copies to with resource location constraints. These constraints are written no differently than those for regular resources.

ifeval::[{ProductNumber} == 8]
You can create a colocation constraint which specifies whether the resources are operating in a master or slave role.
endif::[]
ifeval::[{ProductNumber} == 9]
You can create a colocation constraint which specifies whether the resources are operating in a promoted or unpromoted role.
endif::[]
The following command creates a resource colocation constraint.

[subs="+quotes"]

....

ifeval::[{ProductNumber} == 8]
pcs constraint colocation add [master|slave] _source_resource_ with [master|slave] _target_resource_ [_score_] [_options_]
endif::[]
ifeval::[{ProductNumber} == 9]
pcs constraint colocation add [promoted|unpromoted] _source_resource_ with [promoted|unpromoted] _target_resource_ [_score_] [_options_]
endif::[]
....

For information about colocation constraints, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_colocating-cluster-resources.adoc_configuring-and-managing-high-availability-clusters[Colocating cluster resources].

When configuring an ordering constraint that includes promotable resources, one of the actions that you can specify for the resources is `promote`, indicating that the resource be promoted
ifeval::[{ProductNumber} == 8]
from slave role to master role.
endif::[]
ifeval::[{ProductNumber} == 9]
from unpromoted role to promoted role.
endif::[]
Additionally, you can specify an action of `demote`, indicated that the resource be demoted
ifeval::[{ProductNumber} == 8]
from master role to slave role.
endif::[]
ifeval::[{ProductNumber} == 9]
from promoted role to unpromoted role.
endif::[]

The command for configuring an order constraint is as follows.

[subs="+quotes"]
....

pcs constraint order [_action_] _resource_id_ then [_action_] _resource_id_ [_options_]

....

For information about resource order constraints, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_determining-resource-order.adoc-configuring-and-managing-high-availability-clusters[Determining the order in which cluster resources are run].
