:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_cluster-maintenance.adoc

[id='proc_migrating-cluster-vms-{context}']

= Migrating VMs in a RHEL cluster

[role="_abstract"]
Red Hat does not support live migration of active cluster nodes across hypervisors or hosts, as noted in link:++https://access.redhat.com/articles/3131111/++[Support Policies for RHEL High Availability Clusters - General Conditions with Virtualized Cluster Members]. If you need to perform a live migration, you will first need to stop the cluster services on the VM to remove the node from the cluster, and then start the cluster back up after performing the migration. The following steps outline the procedure for removing a VM from a cluster, migrating the VM, and restoring the VM to the cluster.

The following steps outline the procedure for removing a VM from a cluster, migrating the VM, and restoring the VM to the cluster.

This procedure applies to VMs that are used as full cluster nodes, not to VMs managed as cluster resources (including VMs used as guest nodes) which can be live-migrated without special precautions. For general information about the fuller procedure required for updating packages that make up the RHEL High Availability and Resilient Storage Add-Ons, either individually or as a whole, see link:++https://access.redhat.com/articles/2059253/++[Recommended Practices for Applying Software Updates to a RHEL High Availability or Resilient Storage Cluster].

[NOTE]
====

Before performing this procedure, consider the effect on cluster quorum of removing a cluster node. For example, if you have a three-node cluster and you remove one node, your cluster can not withstand any node failure. This is because if one node of a three-node cluster is already down, removing a second node will lose quorum.

====

.Procedure

. If any preparations need to be made before stopping or moving the resources or software running on the VM to migrate, perform those steps.
    
. Run the following command on the VM to stop the cluster software on the VM.
+
[subs="+quotes"]
....

# *pcs cluster stop*

....

. Perform the live migration of the VM.

. Start cluster services on the VM.
+
[subs="+quotes"]
....

# *pcs cluster start*

....

