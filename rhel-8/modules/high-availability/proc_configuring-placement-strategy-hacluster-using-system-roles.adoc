:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role.adoc

[id="configuring-hacluster-with-placement-strategy-using-system-roles-{context}"]
= Configuring a placement strategy for a high availability cluster by using the RHEL `ha_cluster` RHEL system role

(RHEL 9.5 and later)
A Pacemaker cluster allocates resources according to a resource allocation score. By default, if the resource allocation scores on all the nodes are equal, Pacemaker allocates the resource to the node with the smallest number of allocated resources. If the resources in your cluster use significantly different proportions of a node's capacities, such as memory or I/O, the default behavior may not be the best strategy for balancing your system's workload. In this case, you can customize an allocation strategy by configuring utilization attributes and placement strategies for nodes and resources.

For detailed information about configuring utilization attributes and placement strategies, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/configuring_and_managing_high_availability_clusters/index#proc_setting-shutdown-lock-configuring-resources-to-remain-stopped[Configuring a node placement strategy].

This example procedure uses the `ha_cluster` RHEL system role to create a high availability cluster in an automated fashion that configures utilization attributes to define a placement strategy.

[WARNING]
====

The `ha_cluster` RHEL system role replaces any existing cluster configuration
on the specified nodes. Any settings not specified in the playbook
will be lost.

====
.Prerequisites
include::common-content/snip_common-prerequisites.adoc[]
+
* The systems that you will use as your cluster members have active subscription coverage for RHEL and the RHEL High Availability Add-On.
* The inventory file specifies the cluster nodes as described in xref:ha-system-role-inventory_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role[Specifying an inventory for the ha_cluster RHEL system role].
For general information about creating an inventory file, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/automating_system_administration_by_using_rhel_system_roles/index#proc_preparing-a-control-node_assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[Preparing a control node on RHEL 9].


.Procedure
. Store your sensitive variables in an encrypted file:

.. Create the vault:
+
[subs="+quotes"]
....
$ *ansible-vault create vault.yml*
New Vault password: _<vault_password>_
Confirm New Vault password: _<vault_password>_
....

.. After the `ansible-vault create` command opens an editor, enter the sensitive data in the `_<key>_: _<value>_` format:
+
[source,ini,subs="+quotes"]
....
cluster_password: _<cluster_password>_
....

.. Save the changes, and close the editor. Ansible encrypts the data in the vault.

. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Create a high availability cluster 
  hosts: node1 node2
  vars_files:
    - vault.yml
  tasks:
    - name: Configure a cluster with utilization attributes
      ansible.builtin.include_role:
        name: rhel-system-roles.ha_cluster
      vars:
          ha_cluster_cluster_name: my-new-cluster
          ha_cluster_hacluster_password: "{{ cluster_password }}"
          ha_cluster_manage_firewall: true
          ha_cluster_manage_selinux: true
          ha_cluster_cluster_properties:
            - attrs:
                - name: placement-strategy
                  value: utilization
          ha_cluster_node_options:
            - node_name: node1
              utilization:
                - attrs:
                    - name: utilization1
                      value: 1
                    - name: utilization2
                      value: 2
            - node_name: node2
              utilization:
                - attrs:
                    - name: utilization1
                      value: 3
                    - name: utilization2
                      value: 4
          ha_cluster_resource_primitives:
            - id: resource1
              agent: 'ocf:pacemaker:Dummy'
              utilization:
                - attrs:
                    - name: utilization1
                      value: 2
                    - name: utilization2
                      value: 3
....
+
The settings specified in the example playbook include the following:
+
`ha_cluster_cluster_name: _<cluster_name>_`:: The name of the cluster you are creating.
`ha_cluster_hacluster_password: _<password>_`:: The password of the `hacluster` user. The `hacluster` user has full access to a cluster.
`ha_cluster_manage_firewall: true`:: A variable that determines whether the `ha_cluster` RHEL system role manages the firewall.
`ha_cluster_manage_selinux: true`:: A variable that determines whether the `ha_cluster` RHEL system role manages the ports of the firewall high availability service using the `selinux` RHEL system role.
`ha_cluster_cluster_properties: _<cluster properties>_`:: List of sets of cluster properties for Pacemaker cluster-wide configuration. For utilization to have an effect, the `placement-strategy` property must be set and its value must be different from the value `default`.
`ha_cluster_node_options: _<node options>_:: A variable that defines various settings which vary from cluster node to cluster node.
`ha_cluster_resource_primitives: _<cluster resources>_`:: A list of resource definitions for the Pacemaker resources configured by the `ha_cluster` RHEL system role, including fencing resources.
+

For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.ha_cluster/README.md` file on the control node.

. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check --ask-vault-pass ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass ~/playbook.yml*
....

[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.ha_cluster/README.md` file
* `/usr/share/doc/rhel-system-roles/ha_cluster/` directory
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/automating_system_administration_by_using_rhel_system_roles/ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]




