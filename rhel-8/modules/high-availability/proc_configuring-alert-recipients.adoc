:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-pacemaker-alert-agents.adoc

[id='configuring-alert-recipients-{context}']

= Configuring cluster alert recipients

[role="_abstract"]
Usually alerts are directed towards a recipient. Thus each alert may be additionally configured with one or more recipients. The cluster will call the agent separately for each recipient.

The recipient may be anything the alert agent can recognize: an IP address, an email address, a file name, or whatever the particular agent supports.

The following command adds a new recipient to the specified alert.

[subs="+quotes"]
....

pcs alert recipient add _alert-id_ value=_recipient-value_ [id=_recipient-id_] [description=_description_] [options [_option_=_value_]...] [meta [_meta-option_=_value_]...] 

....

The following command updates an existing alert recipient.

[subs="+quotes"]
....

pcs alert recipient update _recipient-id_ [value=_recipient-value_] [description=_description_] [options [_option_=_value_]...] [meta [_meta-option_=_value_]...] 

....

The following command removes the specified alert recipient.

[subs="+quotes"]
....

pcs alert recipient remove _recipient-id_  

....

Alternately, you can run the `pcs alert recipient delete` command, which is identical to the `pcs alert recipient remove` command.  Both the `pcs alert recipient remove` and the `pcs alert recipient delete` commands allow you to remove more than one alert recipient.


The following example command adds the alert recipient `my-alert-recipient` with a recipient ID of `my-recipient-id` to the alert `my-alert`. This will configure the cluster to call the alert script that has been configured for `my-alert` for each event, passing the recipient `some-address` as an environment variable.

[subs="+quotes"]
....

#  *pcs alert recipient add my-alert value=my-alert-recipient id=my-recipient-id options value=some-address*

....

