:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-active-passive-http-server-in-a-cluster.adoc

[id='proc_configuring-apache-http-web-server-{context}']

= Configuring an Apache HTTP Server

[role="_abstract"]
Configure an Apache HTTP Server with the following procedure.

.Procedure

. Ensure that the Apache HTTP Server is installed on each node in the cluster. You also need the `wget` tool installed on the cluster to be able to check the status of the Apache HTTP Server.
+
On each node, execute the following command.
+
[subs="+quotes,attributes"]
....

# *{PackageManagerCommand} install -y httpd wget*

....
+
If you are running the `firewalld` daemon, on each node in the cluster enable the ports that are required  by the Red Hat High Availability Add-On and enable the ports you will require for running `httpd`.  This example enables the `httpd` ports for public access, but the specific ports to enable for `httpd` may vary for production use.
+
[subs="+quotes"]
....

# *firewall-cmd --permanent --add-service=http*
# *firewall-cmd --permanent --zone=public --add-service=http*
# *firewall-cmd --reload*

....

. In order for the Apache resource agent to get the status of Apache, on each node in the cluster create the following addition to the existing configuration to enable the status server URL.

+
[subs="+quotes"]
....

# *cat <<-END > /etc/httpd/conf.d/status.conf*
*<Location /server-status>*
    *SetHandler server-status*
    *Require local*
*</Location>*
*END*

....

. Create a web page for Apache to serve up.
+
On one node in the cluster, ensure that the logical volume you created in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_configuring-active-passive-http-server-in-a-cluster-configuring-and-managing-high-availability-clusters#proc_configuring-lvm-volume-with-ext4-file-system-configuring-ha-http[Configuring an LVM volume with an XFS file system] is activated, mount the file system that you created on that logical volume, create the file `index.html` on that file system, and then unmount the file system.
+
[subs="+quotes"]
....

# *lvchange -ay my_vg/my_lv*
# *mount /dev/my_vg/my_lv /var/www/*
# *mkdir /var/www/html*
# *mkdir /var/www/cgi-bin*
# *mkdir /var/www/error*
# *restorecon -R /var/www*
# *cat <<-END >/var/www/html/index.html*
*<html>*
*<body>Hello</body>*
*</html>*
*END*
# *umount /var/www*

....
