:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_configuring-pacemaker-alert-agents.adoc

[id='creating-cluster-alert-{context}']

= Creating a cluster alert

[role="_abstract"]
The following command creates a cluster alert. The options that you configure are agent-specific configuration values that are passed to the alert agent script at the path you specify as additional environment variables. If you do not specify a value for `id`, one will be generated.

[subs="+quotes"]
....

pcs alert create path=_path_ [id=_alert-id_] [description=_description_] [options [_option_=_value_]...] [meta [_meta-option_=_value_]...]

....

Multiple alert agents may be configured; the cluster will call all of them for each event. Alert agents will be called only on cluster nodes. They will be called for events involving Pacemaker Remote nodes, but they will never be called on those nodes.

The following example creates a simple alert that will call `myscript.sh` for each event.

[subs="+quotes"]
....

# *pcs alert create id=my_alert path=/path/to/myscript.sh*

....

