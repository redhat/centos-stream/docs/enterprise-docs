:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8-docs/enterprise/assemblies/assembly_clusternode-management.adoc


[id='proc_add-nodes-to-multiple-ip-cluster-{context}']

= Adding a node to a cluster with multiple links 

[role="_abstract"]
When adding a node to a cluster with multiple links, you must specify addresses for all links.

The following example adds the node `rh80-node3` to a cluster,
specifying IP address 192.168.122.203 for the first link and 192.168.123.203 as the second link.

[subs="+quotes"]
....

# *pcs cluster node add rh80-node3 addr=192.168.122.203 addr=192.168.123.203*

....
