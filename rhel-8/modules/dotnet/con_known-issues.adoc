:_mod-docs-content-type: CONCEPT


[id="known-issues_{context}"]
= Known issues

The known issues for running {dotnet} on {rhel} (RHEL) include the following:

ifeval::[{dotnet-ver} == 3.1]
. {dotnet} only runs on RHEL 7.x, RHEL 8.x, and RHEL 9.x;
endif::[]
ifeval::[{dotnet-ver} == 2.1]
. {dotnet} only runs on RHEL 7.x and RHEL 8.x;
endif::[]

ifeval::[{dotnet-ver} >= 3.1]
. `dotnet dev-certs https --trust` does not work on RHEL.
+
{dotnet} supports the creation of HTTPS certificate through `dotnet dev-certs https`, but it does not support trusting them through `dotnet dev-certs https --trust`.
The client that connects to the ASP.NET Core application, such as `curl` or Firefox, will warn about the untrusted self-signed certificate.
To work around this in a browser such as Firefox, ignore the warning and trust the certificate explicitly when the warning about the untrusted certificate comes up.
Command-line tools support flags to ignore untrusted certificates.
For `curl`, use the `--insecure` flag.
For `wget`, use the `--no-check-certificate` flag.

ifeval::[{dotnet-ver} < 6]
. There are different values for math libraries on different platforms.
+
Math libraries that are part of {dotnet} {dotnet-ver} can return different values on different platforms.
This is expected behavior.
{dotnet} {dotnet-ver} takes advantage of the platform-specific libraries to improve performance and reduce overhead.
See the link:https://github.com/dotnet/coreclr/issues/4318[Math.Cos(double.MaxValue) returns different values on Windows and other platforms issue discussion] for more information.
endif::[]
endif::[]

ifeval::[{dotnet-ver} >= 7]
. There are no NuGet packages for `ppc64le` and `s390x` on nuget.org.
+
Using the `rhel.8-s390x`, `linux-s390x`, `rhel.8-ppc64le`, or `linux-ppc64le` runtime identifier can cause some `dotnet` commands to fail when they try to obtain these packages. These commands are either not fully supported on `ppc64le` and `s390x` as described in the other known issues, or the issue can be fixed by not specifying the runtime identifier.

. Single file applications are not supported on `ppc64le` or `s390x`.

. PublishReadyToRun/crossgen is not supported on `ppc64le` or `s390x`.

. OmniSharp, the language server used by IDEs like Visual Studio Code, is not available on `ppc64le` and `s390x`.
endif::[]
ifeval::[{dotnet-ver == 7}]
. The default version of the `Microsoft.NET.Test.Sdk` package in the test project templates (`xunit`, `nunit`, `mstest`) is unusable on `ppc64le`. Trying to build/run tests will fail with a "System.NotSupportedException: Specified method is not supported" exception.
+
If you are trying to run test on `ppc64le`, update the version of the `Microsoft.NET.Test.Sdk` package to at least 17.4.0.
endif::[]
ifeval::[{dotnet-ver} == 8]
. Several workloads can not be installed in the {dotnet} {dotnet-ver} shipped by Red Hat:
.. `android`
.. `macos`
.. `maui-android`
.. `maui-tizen`
.. `maui-windows`
endif::[]

ifeval::[{dotnet-ver} == 6]
. There are no NuGet packages for `s390x` on nuget.org.
+
Using the `rhel.8-s390x`, `linux-s390x`, `rhel.8-ppc64le`, or `linux-ppc64le` runtime identifier can cause some `dotnet` commands to fail when they try to obtain these packages. These commands are either not fully supported on `ppc64le` and `s390x` as described in the other known issues, or the issue can be fixed by not specifying the runtime identifier.

. Single file applications are not supported on `s390x`.

. PublishReadyToRun/crossgen is not supported on `s390x`.

. {dotnet} {dotnet-ver} on `s390x` does not understand memory and cpu limits in containers.
+
In such environments, it is possible that {dotnet} {dotnet-ver} will try to use more memory than allocated to the container, causing the container to get killed or restarted in {ocp}. As a workaround you can manually specify a heap limit through an environment variable: `MONO_GC_PARAMS=max-heap-size=<limit>`. You should set the limit to 75% of the memory allocated to the container. For example, if the container memory limit is 300MB, set `MONO_GC_PARAMS=max-heap-size=225M`.

. The default version of the `Microsoft.NET.Test.Sdk` package in the test project templates (`xunit`, `nunit`, `mstest`) is unusable on `s390x`. Trying to build/run tests will fail with a "System.NotSupportedException: Specified method is not supported" exception.
+
If you are trying to run test on `s390x`, update the version of the `Microsoft.NET.Test.Sdk` package to at least 17.0.0.

. OmniSharp, the language server used by IDEs like Visual Studio Code, is not available on `s390x`.
endif::[]

ifeval::[{dotnet-ver} == 2.1]
. {dotnet} SDK 2.1.3xx contains an incorrect `NETStandard.Library` version. Using this SDK version may result in build failures. To correct this issue, update to SDK 2.1.402 (or later). To update to the latest SDK version, install the latest {dotnet} software. To install the latest software, see link:{url-dotnet-getting-started-rhel7}#installing-dotnet_using-dotnet-on-rhel[Installing {dotnet} {dotnet-ver}].
+
If there is a `global.json` file that specifies the SDK version and you do not want to update to the latest SDK version, you can work around this issue by removing the following entry in the `csproj` file.
+
[options="nowrap"]
----
<TreatWarningsAsErrors>true</TreatWarningsAsErrors>
----
+
. The ASP.NET Core Shared Framework is not available on RHEL. Deployed ASP.NET Core applications must include the ASP.NET Core packages. For more information, see link:{url-dotnet-getting-started-rhel7}#publishing-asp-dotnet-apps_using-dotnet-on-rhel[Publishing ASP.NET applications].
+
. Global tools that are preinstalled with Microsoft SDK are not available by default on RHEL. The `dev-certs`, `ef`, `sql-cache`, `user-secrets`, and `watch` commands are available by default with the Microsoft SDK. On RHEL, these tools can be installed from link:https://www.nuget.org/[nuget.org] using the `dotnet tool install --global <tool> --version '2.1.\*'` command. When a tool is not installed, the CLI prints the command to install it, for example, `dotnet tool install --global dotnet-ef`. Note that the version flag `--version '2.1.*'` should be used to work around the following known issue.
+
The Microsoft SDK always installs the latest version of a tool. When the latest tool version is not compatible with the installed runtimes, the installation will fail. To install the tool, you must specify an older version that is compatible with {dotnet} {dotnet-ver} using the `--version` option. The versions can be found at link:https://nuget.org[nuget.org]. The 2.2.x tool versions preinstalled with the Microsoft SDK are not compatible with {dotnet} {dotnet-ver}. To install them for {dotnet} {dotnet-ver}, add `--version '2.1.*'` as shown in the example in the previous paragraph. This issue is being tracked on link:https://github.com/dotnet/source-build/issues/926[GitHub].
+
. Starting with the release of {dotnet} 2.2, these global tools may fail to install via `dotnet tool install --global dotnet-ef`. The error will say that the tool only supports `netcoreapp2.2`. Use an explicit `--version` flag to work around this. For example, use `dotnet tool install --global dotnet-ef --version 2.1.1` to install a version of `dotnet ef` that works with {dotnet} {dotnet-ver}. The versions can be found via link:https://nuget.org[nuget.org].
+
. `dotnet dev-certs https --trust` does not work on RHEL.
+
{dotnet} supports the creation of HTTPS certificate through `dotnet dev-certs https`, but it does not support trusting them through `dotnet dev-certs https --trust`. The client that connects to the ASP.NET Core application, such as curl or firefox, will warn about the untrusted self-signed certificate. To work around this in a browser such as Firefox, ignore the warning and trust the certificate explicitly when the warning about the untrusted certificate comes up. Command-line tools support flags to ignore untrusted certificate. For curl, use `--insecure`. For wget, use `--no-check-certificate`.
. There are different values for math libraries on different platforms.
+
Math libraries that are part of {dotnet} {dotnet-ver} can return different values on different platforms. This is expected behavior. {dotnet} {dotnet-ver} takes advantage of the platform-specific libraries to improve performance and reduce overhead. See the link:https://github.com/dotnet/coreclr/issues/4318[Math.Cos(double.MaxValue) returns different values on Windows and other platforms issue discussion] for more information.
+
The following issues affected the previous release of {dotnet} on RHEL and were fixed by updates.

. For some commands, the 2.1.300 SDK prints out it is a preview version. This erroneous message can be safely ignored. This was fixed in the {dotnet} SDK 2.1.301 update release.
. The language server used by Visual Studio Code, OmniSharp, does not work with {dotnet} {dotnet-ver}. This was fixed in the {dotnet} SDK 2.1.301 update release.

See link:https://github.com/aspnet/Tooling/blob/master/known-issues-vs2017.md[Known issues for .NET Core, ASP.NET Core, and ASP.NET and Web Tools in Visual Studio 2017] for more information about using Visual Studio with {dotnet} {dotnet-ver}.
endif::[]
