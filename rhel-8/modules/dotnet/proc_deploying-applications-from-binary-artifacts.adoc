:_mod-docs-content-type: PROCEDURE


[id="deploying-applications-from-binary-artifacts_{context}"]
= Deploying {apps} from binary artifacts using `oc`

You can use {dotnet} Source-to-Image (S2I) builder image to build applications using binary artifacts that you provide.

.Prerequisites

. Published application.
+
For more information, see
ifdef::getting-started-with-dotnet-on-openshift[]
link:https://access.redhat.com/documentation/en-us/net/3.1/html/getting_started_with_.net_on_rhel_{rhel-ver}/using-dotnet-on-rhel_getting-started-with-dotnet-on-rhel-{rhel-ver}#publishing-apps-using-dotnet_using-dotnet-on-rhel[Publishing applications using .NET Core 3.1].
endif::[]
ifdef::getting-started-with-dotnet-rhel-{rhel-ver}[]
xref:https://access.redhat.com/documentation/en-us/net/{dotnet-ver}/html-single/getting_started_with_.net_on_rhel_{rhel-ver}/index#assembly_publishing-apps-using-dotnet_getting-started-with-dotnet-on-rhel-{rhel-ver}[Publishing applications with {dotnet} {dotnet-ver}].
endif::[]

.Procedure

. Create a new binary build:
+
ifeval::[{dotnet-ver} >= 5.0]
[subs="+quotes,attributes"]
----
$ oc new-build --name=_my-web-app_ dotnet:{dotnet-ver}-ubi8 --binary=true
----
endif::[]
ifeval::[{dotnet-ver} <= 3.1]
[subs="+quotes,attributes"]
----
$ oc new-build --name=_my-web-app_ dotnet:{dotnet-ver} --binary=true
----
endif::[]

. Start the build and specify the path to the binary artifacts on your local machine:
+
[subs="+quotes,attributes"]
----
$ oc start-build _my-web-app_ --from-dir=_bin/Release/{dotnet-publish-command}/publish_
----

. Create a new application:
+
[subs="+quotes,attributes"]
----
$ oc new-app _my-web-app_
----
