:_mod-docs-content-type: PROCEDURE


[id="installing-image-streams-on-windows_{context}"]
= Installing image streams on Windows

You can use link:https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/main/install-imagestreams.ps1[this script] to install, upgrade, or remove the image streams on Windows.

.Procedure

. Download the script.
+
----
Invoke-WebRequest https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/main/install-imagestreams.ps1 -UseBasicParsing -OutFile install-imagestreams.ps1
----

. Log in to the {os} cluster:
[subs="+quotes,attributes"]
+
----
$ oc login
----

. Install image streams and add a pull secret for authentication against the `registry.redhat.io`:
+
ifeval::[{dotnet-ver} >= 5.0]
[subs="+quotes,attributes"]
----
.\install-imagestreams.ps1 --OS rhel [-User _subscription_username_ -Password _subscription_password_]
----
endif::[]
ifeval::[{dotnet-ver} <= 3.1]
ifeval::[{rhel-ver} < 9]
[subs="+quotes,attributes"]
----
./install-imagestreams.sh --OS rhel{rhel-ver} [-User _subscription_username_ -Password _subscription_password_]
----
endif::[]
endif::[]
ifeval::[{dotnet-ver} <= 3.1]
ifeval::[{rhel-ver} == 9]
[subs="+quotes,attributes"]
----
./install-imagestreams.sh --OS rhel [-User _subscription_username_ -Password _subscription_password_]
----
endif::[]
endif::[]
Replace _subscription_username_ with the name of the user, and replace _subscription_password_ with the user's password. The credentials may be omitted if you do not plan to use the RHEL7-based images.
+
If the pull secret is already present, the `-User` and `-Password` arguments are ignored.

[NOTE]
====
The PowerShell `ExecutionPolicy` may prohibit executing this script.
To relax the policy, run `Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass -Force`.
====

.Additional information

* `Get-Help .\install-imagestreams.ps1`
