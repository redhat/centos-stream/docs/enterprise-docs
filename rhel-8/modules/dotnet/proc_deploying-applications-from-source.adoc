:_mod-docs-content-type: PROCEDURE


[id="deploying-applications-from-source_{context}"]
= Deploying {apps} from source using `oc`

The following example demonstrates how to deploy the _example-app_ application using `oc`, which is in the `app` folder on the `{dotnet-branch}` branch of the `redhat-developer/s2i-dotnetcore-ex` GitHub repository:

.Procedure

. Create a new OpenShift project:
+
[subs="+quotes,attributes"]
----
$ oc new-project _sample-project_
----

. Add the ASP.NET Core application:
+
ifeval::[{dotnet-ver} >= 5.0]
[subs="+quotes,attributes"]
----
$ oc new-app --name=_example-app_ 'dotnet:{dotnet-ver}-ubi8~https://github.com/redhat-developer/s2i-dotnetcore-ex#{dotnet-branch}' --build-env DOTNET_STARTUP_PROJECT=app
----
endif::[]
ifeval::[{dotnet-ver} <= 3.1]
[subs="+quotes,attributes"]
----
$ oc new-app --name=_example-app_ 'dotnet:{dotnet-ver}~https://github.com/redhat-developer/s2i-dotnetcore-ex#{dotnet-branch}' --build-env DOTNET_STARTUP_PROJECT=app
----
endif::[]
. Track the progress of the build:
+
[subs="+quotes,attributes"]
----
$ oc logs -f bc/_example-app_
----

. View the deployed application once the build is finished:
+
[subs="+quotes,attributes"]
----
$ oc logs -f dc/_example-app_
----
The application is now accessible within the project.

. Optional: Make the project accessible externally:
+
[subs="+quotes,attributes"]
----
$ oc expose svc/_example-app_
----

. Obtain the shareable URL:
+
[subs="+quotes,attributes"]
----
$ oc get routes
----
