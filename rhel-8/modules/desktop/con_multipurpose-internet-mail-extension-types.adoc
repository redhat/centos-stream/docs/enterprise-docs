:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-concept-module-a.adoc
// * ID: [id="my-concept-module-a-{context}"]
// * Title: = My concept module A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="multipurpose-internet-mail-extension-types_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Multipurpose Internet Mail Extension types
// In the title of concept modules, include nouns or noun phrases that are used in the body text. This helps readers and search engines find the information quickly.
// Do not start the title of concept modules with a verb. See also _Wording of headings_ in _The IBM Style Guide_.

The GNOME desktop uses MIME types to:

* Determine which application should open a specific file format by default.
* Register other applications that can open files of a specific format.
* Set a string describing the type of a file, for example, in a file properties dialog of the files application.
* Set an icon representing a specific file format, for example, in a file properties dialog of the files application.

MIME type names follow a given format:
----
media-type/subtype-identifier
----

====
In the `image/jpeg` MIME type name, `image` is a media type and `jpeg` is the subtype identifier.
====

GNOME follows Multipurpose Internet Mail Extension (MIME) info specification from the http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/[_Freedesktop.org_] to determine:

* The machine-wide and user-specific location to store all the MIME type specification files.
* How to register a MIME type so that the desktop environment knows which application you can use to open a specific file format.
* How users can change which applications should open with what file formats.

[discrete]
== MIME database

The MIME database is a collection of all the MIME type specification files that GNOME uses to store information about known MIME types.

The most important part of the MIME database from the system administrator's point of view is the `/usr/share/mime/packages/` directory, where the MIME type-related files specifying information about known MIME types are stored. One example of such a file is `/usr/share/mime/packages/freedesktop.org.xml`, specifying information about the standard MIME types available on the system, by default. The shared-mime-info package provides this file.

[role="_additional-resources"]
.Additional resources
* For detailed information describing the MIME type system, see Shared MIME Info specifications on the http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/[Freedesktop website].
