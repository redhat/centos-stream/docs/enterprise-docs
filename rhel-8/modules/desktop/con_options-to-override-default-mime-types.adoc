:_mod-docs-content-type: CONCEPT

[id="options-to-override-default-mime-types_{context}"]

= Options to override default MIME types


By default, the package-installed `/usr/share/applications/mimeapps.list` and `/usr/share/applications/gnome-mimeapps.list` files specify which application is registered to open specific MIME types.

System administrators can create the `/etc/xdg/mimeapps.list` or `/etc/xdg/gnome-mimeapps.list` file with a list of MIME types they want to override with the default registered application.

Local users can create the `~/.local/share/applications/mimeapps.list` or `~/.local/share/applications/gnome-mimeapps.list` file with a list of MIME types for which they want to override the default registered application.

Configurations are applied in the following order:

. `/usr/share/applications/`
. `/etc/xdg/`
. `~/.local/share/application/`

Within a particular location, the configurations are applied in the following order:

. mimeapps.list
. gnome-mimeapps.list
