:_mod-docs-content-type: CONCEPT
[id="con_input-methods_{context}"]
= Input methods

[role="_abstract"]
Certain scripts, such as Chinese, Japanese, or Korean, require keyboard input to go through an Input Method Engine (IME) to enter native text.

An input method is a set of conversion rules between the text input and the selected script. An IME is a software that performs the input conversion specified by the input method.

To input text in these scripts, you must set up an IME. If you installed the system in your native language and selected your language at the *GNOME Initial Setup* screen, the input method for your language is enabled by default.


// [role="_additional-resources"]
// .Additional resources
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * Currently, modules cannot include xrefs, so you cannot include links to other content in your collection. If you need to link to another assembly, add the xref to the assembly that includes this module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

