:_mod-docs-content-type: PROCEDURE

[id="listing-available-wacom-tablet-configuration-paths_{context}"]

= Listing available Wacom tablet configuration paths

Wacom tablet and stylus configuration files are saved in the following locations by default:

Tablet configuration:: `/org/gnome/settings-daemon/peripherals/wacom/_<D-Bus_machine_-_id>_-_<device_id>_`
Wacom tablet configuration schema:: `org.gnome.settings-daemon.peripherals.wacom`

Stylus configuration:: `/org/gnome/settings-daemon/peripherals/wacom/_<device_id>_/_<tool_id>_`. If your product range does not support `<tool_id>`, a generic identifier is used instead. 

Stylus configuration schema for :: `org.gnome.settings-daemon.peripherals.wacom.stylus`
Eraser configuration schema:: `org.gnome.settings-daemon.peripherals.wacom.eraser`

.Prerequisites

* The [package]`gnome-settings-daemon` package is installed on your system.

.Procedure

* List all tablet configuration paths used on your system:
+
[literal,subs=+quotes]
----
$ **/usr/libexec/gsd-list-wacom**
----

[IMPORTANT]
====

Using `machine-id`, `device-id`, and `tool-id` in configuration paths allows for shared home directories with independent tablet configuration per system. However, when sharing home directories between systems, the Wacom settings apply only to one system.

This is because the `machine-id` for your Wacom tablet is included in the configuration path of the `/org/gnome/settings-daemon/peripherals/wacom/machine-id-device-id GSettings` key, which stores your tablet settings. 

====



