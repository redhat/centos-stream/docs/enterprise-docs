:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_locking-down-enabled-extensions.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.

[id="locking-down-enabled-extensions_{context}"]
= Locking down enabled extensions

You can prevent the user from enabling or disabling extensions by locking down the `org.gnome.shell.enabled-extensions` key.
// Locking down the org.gnome.shell.development-tools key ensures that the user cannot use GNOME Shell's integrated debugger and inspector tool (Looking Glass) to disable any mandatory extensions.

.Procedure

. Create a local database file for machine-wide settings in [filename]`/etc/dconf/db/local.d/00-extensions`:
+
[subs="+quotes"]
....
[org/gnome/shell]
# List all extensions that you want to have enabled for all users
enabled-extensions=['myextension1@myname.example.com', 'myextension2@myname.example.com']
....
+
The `enabled-extensions` key specifies the enabled extensions using the extensions' UUID (`myextension1@myname.example.com` and `myextension2@myname.example.com`).

. Override the user's setting and prevent the user from changing it in [filename]`/etc/dconf/db/local.d/locks/extensions`:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# Lock the list of mandatory extensions
/org/gnome/shell/enabled-extensions
....

. Update the system databases:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# dconf update
....

. Users must log out and back in again before the system-wide settings take effect. 

After locking down the `org.gnome.shell.enabled-extensions`, any extensions installed in [filename]`~/.local/share/gnome-shell/extensions` or [filename]`/usr/share/gnome-shell/extensions` that are not listed in the `org.gnome.shell.enabled-extensions` key will not be loaded by GNOME Shell, thus preventing the user from using them. 
