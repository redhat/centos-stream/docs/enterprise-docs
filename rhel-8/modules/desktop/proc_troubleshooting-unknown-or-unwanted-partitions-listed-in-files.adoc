:_mod-docs-content-type: PROCEDURE

:experimental:

// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="troubleshooting-unknown-or-unwanted-partitions-listed-in-files_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Troubleshooting unknown or unwanted partitions listed in Files
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
You might see unknown or unwanted partitions when you plug a disk in. For example, when you plug in a flash disk, it is automatically mounted and its volumes are shown in the *Files* side bar. Some devices have a special partition with backups or help files, which you might not want to see each time you plug in the device.

.Procedure
. Open the *Disks* application.
. Select the disk in the side bar.
. Below *Volumes*, click *Additional partition options > Edit Mount Options*
. Deselect *Show in user interface*.
. Confirm by clicking btn:[OK].
