:_mod-docs-content-type: CONCEPT
[id="the-gnome-software-application_{context}"]
= The GNOME Software application

// After RHEL 8, GNOME Software no longer supports managing Shell extensions.

[role="_abstract"]
GNOME Software is an utility that enables you to install and update
ifeval::[{ProductNumber} == 8]
applications, software components, and GNOME Shell extensions
endif::[]
ifeval::[{ProductNumber} == 9]
applications and software components
endif::[]
in a graphical interface.

GNOME Software provides a catalog of graphical applications, which are the applications that include a `*.desktop` file. The available applications are grouped into multiple categories according to their purpose. 

GNOME Software uses the PackageKit and Flatpak technologies as its back ends.

// .Additional resources
// 
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * Currently, modules cannot include xrefs, so you cannot include links to other content in your collection. If you need to link to another assembly, add the xref to the assembly that includes this module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
