:_mod-docs-content-type: CONCEPT
[id="graphics-back-ends-in-rhel-8_{context}"]
= Graphics back ends in RHEL 8

In {ProductShortName} 8, you can choose between two protocols to build a graphical user interface:

X11::
The *X11* protocol uses *X.Org* as the display server. Displaying graphics based on this protocol works the same way as in {ProductShortName} 7, where this was the only option.

Wayland::
The *Wayland* protocol on {ProductShortName} 8 uses *GNOME Shell* as its compositor and display server, which is further referred as *GNOME Shell on Wayland*. Displaying graphics based on the *Wayland* protocol has some differences and limitation compared to *X11*.

New installations of {ProductShortName} 8 automatically select *GNOME Shell on Wayland*. However, you can switch to *X.Org*, or select the required combination of GNOME environment and display server as described in
ifdef::desktop-title[]
xref:selecting-gnome-environment_overview-of-gnome-environments[].
endif::[]
ifndef::desktop-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/using_the_desktop_environment_in_rhel_8/index#selecting-gnome-environment_overview-of-gnome-environments[Selecting GNOME environment and display protocol].
endif::[]

Note that there are also a few environments where *X.Org* is preferred over *GNOME Shell on Wayland*, such as:

* Cirrus graphics used in a VM environment
* Matrox graphics
* Aspeed graphics
* QXL graphics used in a VM environment
* Nvidia graphics when used with the proprietary driver

[IMPORTANT]
====

The Nvidia graphics by default use `nouveau`, which is an open source driver. `nouveau` is supported on *Wayland*, hence you can use Nvidia graphics with `nouveau` on *GNOME Shell on Wayland* without any limitations. However, using Nvidia graphics with proprietary Nvidia binary drivers is not supported on *GNOME Shell on Wayland*. In this case, you need to switch to *X.Org* as described in
ifdef::desktop-title[]
xref:selecting-gnome-environment_overview-of-gnome-environments[].
endif::[]
ifndef::desktop-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/using_the_desktop_environment_in_rhel_8/index#selecting-gnome-environment_overview-of-gnome-environments[Selecting GNOME environment and display protocol].
endif::[]

====

[role="_additional-resources"]
.Additional resources
* You can find the current list of environments for which Wayland is not available in the [filename]`/usr/lib/udev/rules.d/61-gdm.rules` file.

* For additional information about the *Wayland* project, see link:https://wayland.freedesktop.org/[Wayland documentation].
