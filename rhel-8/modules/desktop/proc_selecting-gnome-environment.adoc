:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_selecting-gnome-environement.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.

[id="selecting-gnome-environment_{context}"]
= Selecting GNOME environment and display protocol

The default desktop environment for {RHEL} 8 is GNOME Standard with *GNOME Shell on Wayland* as the display server. However, due to certain limitations of *Wayland*, you might want to switch the graphics protocol stack. You might also want to switch from GNOME Standard to GNOME Classic.


.Procedure

. From the login screen (GDM), click the gear button next to the *Sign In* button.
+
NOTE: You cannot access this option from the lock screen. The login screen appears when you first start {RHEL} 8 or when you log out of your current session.
+
image:../images/gnome-environments-new.png[]

. From the drop-down menu that appears, select the option that you prefer.
+
[NOTE]
====
Note that in the menu that appears on the login screen, the *X.Org* display server is marked as *X11* display server.
====

[IMPORTANT]
====
The change of GNOME environment and graphics protocol stack resulting from the above procedure is persistent across user logouts, and also when powering off or rebooting the computer.
====
