:_mod-docs-content-type: CONCEPT

[id="the-flatpak-technology_{context}"]
= The Flatpak technology

[role="_abstract"]
Flatpak provides a sandbox environment for application building, deployment, distribution, and installation.

Applications that you launch using Flatpak have minimum access to the host system, which protects the system installation against third-party applications. Flatpak provides application stability regardless of the versions of libraries installed on the host system.

Flatpak applications are distributed from repositories called remotes. {RH} provides a remote with {ProductShortName} applications. Additionally, third-party remotes are available as well. {RH} does not support applications from third-party remotes.

