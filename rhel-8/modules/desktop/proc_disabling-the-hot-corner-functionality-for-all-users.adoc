:_mod-docs-content-type: PROCEDURE
[id="disabling-the-hot-corner-functionality-for-all-users_{context}"]
= Disabling the hot corner functionality for all users

With the GNOME Shell extension called [application]*No topleft hot corner* provided by the [package]`gnome-shell-extension-no-hot-corner` package, you can disable the hot corner feature system-wide.

.Prerequisites

* The [package]`gnome-shell-extension-no-hot-corner` package is installed on the system:
+
[literal,subs="+quotes,verbatim,normal"]
....
# {PackageManagerCommand} install gnome-shell-extension-no-hot-corner
....

.Procedure

. Enable the [application]*No topleft hot corner* extension by switching it on in the *Tweaks* tool.
ifeval::[{ProductNumber} == 8]
+
For more information about how to use *Tweaks*, see xref:customizing-gnome-shell-with-tweaks_getting-started-with-gnome[].
endif::[]
. Log out, and restart the user session so that the extension can take effect.
