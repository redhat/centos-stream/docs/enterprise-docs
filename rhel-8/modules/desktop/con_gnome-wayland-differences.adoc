:_mod-docs-content-type: CONCEPT

// Module included in the following assemblies:
//
// include::<path>/assembly_overview-of-new-features-in-gnome.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/con_new-features-of-gnome-in-rhel8.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.

[id="wayland-differences-xorg_{context}"]
= Key differences between the Wayland and X11 protocol

.X11 applications

Client applications need to be ported to the *Wayland* protocol or use a graphical toolkit that has a *Wayland* backend, such as GTK, to be able to work natively with the compositor and display server based on *Wayland*.

Legacy *X11* applications that cannot be ported to *Wayland* automatically use *Xwayland* as a proxy between the *X11* legacy clients and the *Wayland* compositor. *Xwayland* functions both as an *X11* server and a *Wayland* client. The role of *Xwayland* is to translate the *X11* protocol into the *Wayland* protocol and reversely, so that *X11* legacy applications can work with the display server based on *Wayland*.

ifdef::desktop-title[]
On *GNOME Shell on Wayland*, *Xwayland* is started automatically at startup, which ensures that most *X11* legacy applications work as expected when using *GNOME Shell on Wayland*. However, the *X11* and *Wayland* protocols are different, and hence some clients relying on *X11*-specific features may behave differently under *Xwayland*. For such specific clients, you can switch to the *X.Org* display server as described in xref:selecting-gnome-environment_overview-of-gnome-environments[].
endif::[]

.libinput

{RHEL} 8 uses a new unified input stack, `libinput`, which manages all common device types, such as mice, touchpads, touchscreens, tablets, trackballs and pointing sticks. This unified stack is used both by the *X.Org* and by the *GNOME Shell on Wayland* compositor.

*GNOME Shell on Wayland* uses `libinput` directly for all devices, and no switchable driver support is available. Under *X.Org*, `libinput` is implemented as the *X.Org* `libinput` driver, and driver support is outlined below.

Mice, touchscreens, trackballs, pointing sticks::
{RHEL} 8 uses the *X.Org* `libinput` driver for these devices. The `X.Org evdev` driver, which was used in {RHEL} 7, is available as fallback where required.

Touchpads::
{RHEL} 8 uses the *X.Org* `libinput` driver for touchpads. The `X.Org synaptics` driver, which was used for touchpads in {RHEL} 7, is no longer available.

Graphics tablets::
{RHEL} 8 continues using the *X.Org* `wacom` driver, which was used for tablet devices in {RHEL} 7. However, the *X.Org* `libinput` driver is available where required.


Other input devices::
{RHEL} 7 used the *X.Org* `evdev` driver for other input devices that are not included in the above categories. {RHEL} 8 uses the *X.Org* `libinput` driver by default but can fall back to the *X.Org* `evdev` driver if a device is incompatible with `libinput`.

.Gestures

*GNOME Shell on Wayland* supports new touchpad and touchscreen gestures. These gestures include:

* Switching workspaces by dragging up or down with four fingers.

* Opening the *Activities* overview by bringing three fingers closer together.
