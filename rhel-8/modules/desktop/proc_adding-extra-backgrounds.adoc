:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_adding-extra-backgounds.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="adding-extra-backgrounds_{context}"]
= Adding extra backgrounds

You can make extra backgrounds available to users on your system.

.Procedure

. Create the `/usr/share/gnome-background-properties/extra-backgrounds.xml` file.

. In the new file, specify the extra background files and their appearance in the following format:
+
[source,xml, subs="+quotes"]
----
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd">
<wallpapers>
  <wallpaper deleted="false">
    <name>__Background name__</name>
    <filename>__full-path-to-the-image__</filename>
    <options>__display-option__</options>
    <shade_type>__background-shade__</shade_type>
    <pcolor>__primary-color__</pcolor>
    <scolor>__secondary-color__</scolor>
  </wallpaper>
</wallpapers>
----

. The new backgrounds are now available to all users in the *Background* section of the *Settings* application.
