:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="configuring-enterprise-credentials-at-the-gnome-welcome-screen_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Configuring enterprise credentials at the GNOME welcome screen
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

Perform the following steps to configure workstation for enterprise credentials using the welcome screen that belongs to the *GNOME Initial Setup* program.

The initial setup runs only when you create a new user and log into that account for the first time.

.Procedure
. At the login welcome screen, choose *Use Enterprise Login*.
. Enter your domain name into the *Domain* field.
. Enter your domain account user name and password.
. Click *Next*.
. Depending on the domain configuration, a pop up prompts for the domain administrator’s credentials.
