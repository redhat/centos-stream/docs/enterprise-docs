:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_locking-down-user-logout-and-user-switching.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.

[id="locking-down-user-logout-and-user-switching_{context}"]
= Locking down user logout and user switching

To prevent the user from logging out, use the following procedure.

.Procedure

. Create the [filename]`/etc/dconf/profile/user` profile, which contains the following lines:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
user-db:user
system-db:local
....
+
where `local` is the name of a dconf database

. Create the [filename]`/etc/dconf/db/local.d/` directory if it does not already exist.

. Create the [filename]`/etc/dconf/db/local.d/00-logout` key file to provide information for the local database:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
[org/gnome/desktop/lockdown]
# Prevent the user from user switching
disable-log-out=true
....

. Override the user's setting, and prevent the user from changing it in the [filename]`/etc/dconf/db/local.d/locks/lockdown` file:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# Lock this key to disable user logout
/org/gnome/desktop/lockdown/disable-log-out
....

. Update the system databases:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# dconf update
....

. Users must log out and back in again before the system-wide settings take effect.

[IMPORTANT]
====
Users can evade the logout lockdown by switching to a different user. To prevent such scenario, lock down user switching as well.
====

To lock down user switching, use the following procedure:

.Procedure

. Create the [filename]`/etc/dconf/profile/user` profile, which contains the following lines:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
user-db:user
system-db:local
....
+
where `local` is the name of a dconf database

. Create the [filename]`/etc/dconf/db/local.d/` directory if it does not already exist.

. Create the [filename]`/etc/dconf/db/local.d/00-user-switching` key file to provide information for the local database:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# Prevent the user from user switching
[org/gnome/desktop/lockdown]
disable-user-switching=true

[org/gnome/desktop/screensaver]
user-switch-enabled=false
....

. Override the user's setting, and prevent the user from changing it in the [filename]`/etc/dconf/db/local.d/locks/lockdown` file:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# Lock this key to disable user switching
/org/gnome/desktop/lockdown/disable-user-switching
/org/gnome/desktop/screensaver/user-switch-enabled
....

. Update the system databases:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# dconf update
....

. Users must log out and back in again before the system-wide settings take effect.
