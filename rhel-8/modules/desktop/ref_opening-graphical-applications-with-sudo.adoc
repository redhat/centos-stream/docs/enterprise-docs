:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_opening-graphical-applications-with-sudo.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.

[id="opening-graphical-applications-with-sudo_{context}"]
= Opening graphical applications with sudo

When attempting to open a graphical application in a terminal using the [command]`sudo` command, you must do the following:

[discrete]
== X11 applications
 
If the application uses the `X11` display protocol, add the local user `root` in the X server access control list. As a result, `root` is allowed to connect to `Xwayland`, which translates the `X11` protocol into the `Wayland` protocol and reversely.

.Adding `root` to the X server access control list to open xclock with sudo
====
$ xhost +si:localuser:root

$ sudo xclock
====


[discrete]
== Wayland applications

If the application is `Wayland` native, include the [option]`-E` option.

.Opening GNOME Calculator with sudo
====
$ sudo -E gnome-calculator
====


Otherwise, if you type just [command]`sudo` and the name of the application, the operation of opening the application fails with the following error message:

[literal,subs="+quotes,verbatim,normal,normal"]
....
No protocol specified
Unable to init server: could not connect: connection refused
# Failed to parse arguments: Cannot open display
....






