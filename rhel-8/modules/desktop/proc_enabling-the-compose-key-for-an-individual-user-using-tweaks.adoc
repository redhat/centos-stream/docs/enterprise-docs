:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="enabling-the-compose-key-for-and-individual-user-using-tweaks_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Enabling the Compose key for an individual user with the Tweaks application
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

To enable the Compose key for an individual user by Tweaks application follow these steps.

.Prerequisites

* The *Tweaks* application is installed on your system.
+
[subs="+attributes"]
----
# {PackageManagerCommand} install gnome-tweaks
----

.Procedure
. Open the *Tweaks* application.
. Select *Keyboard & Mouse* in the side bar.
. Enable *Compose Key*.
. Choose which key from the listed keys triggers the Compose functionality.
+
image::enabling-compose-key.png[]
