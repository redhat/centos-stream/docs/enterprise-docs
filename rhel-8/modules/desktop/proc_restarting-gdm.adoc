:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_restarting-gdm.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.

[id="restarting-gdm_{context}"]
= Restarting GDM

When you make changes to the system configuration such as setting up the login screen banner message, login screen logo, or login screen background, restart GDM for your changes to take effect.

include::common-content/snip_restarting-gdm-warning.adoc[leveloffset=+1]

.Procedure

* To restart the GDM service, run the following command:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# systemctl restart gdm.service
....

.Procedure

* To display results of the GDM configuration, run the following command:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
$ DCONF_PROFILE=gdm gsettings list-recursively org.gnome.login-screen
....
