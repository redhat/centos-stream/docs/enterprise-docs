// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="proc_enabling-system-security-classification-banners_{context}"]
= Enabling system security classification banners
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
You can create a permanent classification banner to state the overall security classification level of the system. This is useful for deployments where the user must always be aware of the security classification level of the system that they are logged into.

The permanent classification banner can appear within the running session, the lock screen, and login screen, and customize its background color, its font, and its position within the screen.

This procedure creates a red banner with a white text placed on both the top and bottom of the login screen.

.Procedure

. Install the [package]`gnome-shell-extension-classification-banner` package:
+
[subs=+attributes]
----
# {PackageManagerCommand} install gnome-shell-extension-classification-banner
----
ifeval::[{ProductNumber} == 8]
+
NOTE: The package is only available in {ProductShortName} 8.6 and later.
endif::[]

. Create the [filename]`99-class-banner` file at either of the following locations:

** To configure a notification at the login screen, create [filename]`/etc/dconf/db/gdm.d/99-class-banner`.
** To configure a notification in the user session, create [filename]`/etc/dconf/db/local.d/99-class-banner`.

. Enter the following configuration in the created file:
+
[subs="+quotes",options="nowrap"]
----
[org/gnome/shell]
enabled-extensions=['classification-banner@gnome-shell-extensions.gcampax.github.com']

[org/gnome/shell/extensions/classification-banner]
background-color='*_rgba(200,16,46,0.75)_*'
message='*_TOP SECRET_*'
top-banner=*_true_*
bottom-banner=*_true_*
system-info=*_true_*
color='*_rgb(255,255,255)_*'
----
+
[WARNING]
--
This configuration overrides similar configuration files that also enable an extension, such as
ifdef::using-the-desktop-environment-in-rhel-8,administering-the-system-using-the-gnome-desktop-environment,customizing-the-gnome-desktop-environment[]
xref:proc_notifying-of-the-system-security-classification_{context}[Notifying of the system security classification].
endif::[]
ifndef::using-the-desktop-environment-in-rhel-8,administering-the-system-using-the-gnome-desktop-environment,customizing-the-gnome-desktop-environment[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_the_desktop_environment_in_rhel_8/assembly_displaying-the-system-security-classification_using-the-desktop-environment-in-rhel-8#proc_notifying-of-the-system-security-classification_assembly_displaying-the-system-security-classification[Notifying of the system security classification].
endif::[]

To enable multiple extensions, specify all of them in the `enabled-extensions` list. For example:

----
enabled-extensions=['heads-up-display@gnome-shell-extensions.gcampax.github.com', 'classification-banner@gnome-shell-extensions.gcampax.github.com']
----
--

. Update the `dconf` database:
+
----
# dconf update
----

. Reboot the system.

.Troubleshooting
* If the classification banners are not displayed for an existing user, log in as the user and enable the *Classification banner* extension using the
ifeval::[{ProductNumber} == 8]
*Tweaks*
endif::[]
ifeval::[{ProductNumber} >= 9]
*Extensions*
endif::[]
application.
