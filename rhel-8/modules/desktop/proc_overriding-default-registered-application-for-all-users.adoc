:_mod-docs-content-type: PROCEDURE

[id="overriding-default-registered-application-for-all-users_{context}"]

= Overriding default registered application for all the users


As a system administer, you can update the configuration based on the requirements.
System administrator's configuration takes precedence over default package configuration. Within each, the desktop-specific configuration takes precedence over the configuration that does not specify the desktop environment.

.Procedure

. Consult the `/usr/share/applications/mimeapps.list` file to determine the MIME types for which you want to change the default registered application. For example, the following sample of the `mimeapps.list` file specifies the default registered application for the `text/html` and `application/xhtml+xml` MIME types:

+
----
[Default Applications]
text/html=firefox.desktop
application/xhtml+xml=firefox.desktop
----
This example above specifies default application (Mozilla Firefox) by specifying its corresponding `.desktop` file (`firefox.desktop`). You can find `.desktop` files for other applications in the `/usr/share/applications/` directory.
. Create the `/etc/xdg/mimeapps.list` file and specify the MIME types and their corresponding default registered applications in this file.
+
----
[Default Applications]
text/html=myapplication1.desktop
application/xhtml+xml=myapplication2.desktop
----
This example above sets the default registered application for the `text/html` MIME type to `myapplication1.desktop` and `application/xhtml+xml` MIME type to `myapplication2.desktop`.

.Verification

* For these settings to function correctly, ensure that both the `myapplication1.desktop` and `myapplication2.desktop` files are placed in the `/usr/share/applications/` directory.

* Verify that the default registered application is set correctly:
+
[literal,subs="+quotes"]
----
$ *gio mime text/html*
Default application for 'text/html': myapplication1.desktop
Registered applications:
	myapplication1.desktop
	firefox.desktop
Recommended applications:
	myapplication1.desktop
	firefox.desktop
----
