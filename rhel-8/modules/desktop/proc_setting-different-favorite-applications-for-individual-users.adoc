:_mod-docs-content-type: PROCEDURE

:experimental:

[id="setting-different-favorite-applications-for-individual-users_{context}"]
= Setting different favorite applications for individual users

You can set the default favorite applications for individual users.

.Procedure

. Open the *Activities* overview by clicking btn:[Activities] at the top left of the screen.

. Add applications into your favorite list using any of the following methods:

** Click the grid button to find the application you want, right-click the application icon, and select *Add to Favorites*.
** Click-and-drag the icon into the dash.

. View all the applications that exists in the favorite list:
+
----
$ dconf read /org/gnome/shell/favorite-apps
----

[NOTE]
If you want to lock down the above settings to prevent users from changing them, see xref:locking-down-selected-tasks_using-the-desktop-environment-in-rhel-8[].

