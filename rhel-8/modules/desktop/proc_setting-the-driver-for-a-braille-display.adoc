:_mod-docs-content-type: PROCEDURE
[id="setting-the-driver-for-a-braille-display_{context}"]
= Setting the driver for a Braille display device

The `brltty` service automatically chooses a driver for your Braille display device. If the automatic detection fails or takes too long, you can set the driver manually.

.Prerequisites

* The automatic driver detection has failed or takes too long.

.Procedure

. Open the [filename]`/etc/brltty.conf` configuration file.

. Find the `braille-driver` directive, which specifies the driver for your Braille display device.

. Specify the identification code of the required driver in the `braille-driver` directive.
+
Choose the identification code of required driver from the list provided in [filename]`/etc/brltty.conf`. For example, to use the XWindow driver:
+
----
# XWindow
braille-driver	xw
----
+
To set multiple drivers, list them separated by commas. Automatic detection then chooses from the listed drivers.

