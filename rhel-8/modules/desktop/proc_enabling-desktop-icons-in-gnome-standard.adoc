:_mod-docs-content-type: PROCEDURE

:experimental:

[id="enabling-desktop-icons-in-gnome-standard_{context}"]
= Enabling desktop icons in GNOME Standard

This procedure enables the desktop icons functionality in the GNOME Standard environment.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.
ifeval::[{ProductNumber} == 9]
.Prerequisites

* The *Extensions* application is installed on the system:
+
[subs="+attributes"]
....
# {PackageManagerCommand} install gnome-shell-extension-desktop-icons
....
endif::[]

.Procedure

ifeval::[{ProductNumber} == 8]
. Open the *Tweaks* application.

. Select menu:Extensions[Desktop icons], and enable the extension.
+
image:desktop-icons-on.png[]
endif::[]
ifeval::[{ProductNumber} == 9]
. Open the *Extensions* application.

. Enable the *Desktop Icons* extension.
+
image::desktop-icons-on-9.png[Enabling desktop icons in GNOME Standard]
endif::[]

// .Verification
//
// . Optional: Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.
// . Use an unnumbered bullet (*) if the procedure includes only one step.
//
// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * Currently, modules cannot include xrefs, so you cannot include links to other content in your collection. If you need to link to another assembly, add the xref to the assembly that includes this module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
