:_mod-docs-content-type: PROCEDURE
[id="locking-down-specific-settings_{context}"]
= Locking down specific settings

By default, any settings that users make take precedence over the system settings. Therefore, users can override the system settings with their own. Using the lockdown mode in `dconf`, you can prevent users from changing specific settings.

.Procedure

. Create the [filename]`/etc/dconf/db/local.d/locks/` directory.

. In this directory, add any number of files listing keys that you want to lock.

.Locking the settings for the default wallpaper
====

. Set a default wallpaper.

. Create the [filename]`/etc/dconf/db/local.d/locks/` directory.

. Create the [filename]`/etc/dconf/db/local.d/locks/00-default-wallpaper` file with the following content, listing one key per line:
+
----
# Prevent users from changing values for the following keys:
/org/gnome/desktop/background/picture-uri
/org/gnome/desktop/background/picture-options
/org/gnome/desktop/background/primary-color
/org/gnome/desktop/background/secondary-color
----

. Update the system databases:
+
----
# dconf update
----

====
