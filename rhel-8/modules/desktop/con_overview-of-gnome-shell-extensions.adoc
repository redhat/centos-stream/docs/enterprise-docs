:_mod-docs-content-type: CONCEPT
[id="overview-of-gnome-shell-extensions_{context}"]
= Overview of GNOME Shell extensions

The following is an overview of GNOME Shell extensions available on {ProductShortName} 8, including the name of the package providing a particular extension, and the description of what each extension does.

.Overview of available GNOME Shell extensions
[cols="35%,15%,50%"]
|====
| Package name | Extension name | Description

| gnome-shell-extension-apps-menu | apps-menu | *Applications* menu for GNOME Shell
| gnome-shell-extension-top-icons | Top Icons | Show legacy icons on top
| gnome-shell-extension-user-theme | user-theme | Support for custom themes in GNOME Shell
| gnome-shell-extension-drive-menu | drive-menu | Drive status menu for GNOME Shell
| gnome-shell-extension-window-list | window-list | Display a window list at the bottom of the screen in GNOME Shell
| gnome-shell-extension-dash-to-dock | Dash to Dock | Dock for the Gnome Shell by micxgx.gmail.com
| gnome-shell-extension-desktop-icons | Desktop Icons | Desktop icons support for the GNOME Classic experience
| gnome-shell-extension-no-hot-corner | nohotcorner | Disable the hot corner in GNOME Shell
| gnome-shell-extension-systemMonitor | systemMonitor | *System Monitor* for GNOME Shell
| gnome-shell-extension-updates-dialog | Updates Dialog | Show a modal dialog when there are software updates
| gnome-shell-extension-window-grouper | window-grouper | Keep windows that belong to the same process on the same workspace
| gnome-shell-extension-panel-favorites | panel-favorites | Favorite launchers in GNOME Shell's top bar
| gnome-shell-extension-windowsNavigator | windowNavigator | Support for keyboard selection of windows and workspaces in GNOME shell
| gnome-shell-extension-auto-move-windows | Autom Move Windows | Assign specific workspaces to applications in GNOME Shell
| gnome-shell-extension-launch-new-instance | launch-new-instance | Always launch a new application instance for GNOME Shell
| gnome-shell-extension-workspace-indicator | workspace-indicator | Workspace indicator for GNOME Shell
| gnome-shell-extension-disable-screenshield | Disable Screen Shield | Disable GNOME Shell screen shield if lock is disabled
| gnome-shell-extension-native-window-placement | native-window-placement | Native window placement for GNOME Shell
| gnome-shell-extension-screenshot-window-sizer | screenshot-window-sizer | Screenshot window sizer for GNOME Shell
| gnome-shell-extension-horizontal-workspaces | horizontal-workspaces | Desktop icons support for the GNOME Classic experience
| gnome-shell-extension-places-menu | places-menu | *Places* status menu for GNOME Shell
| gnome-classic-session | – | GNOME Classic mode session
|====
