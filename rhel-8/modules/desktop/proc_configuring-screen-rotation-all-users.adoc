// Module included in the following assemblies:
//
// assemblies/assembly_managing-user-sessions.adoc 

[id="configuring-screen-rotation-all-users_{context}"]
= Configuring screen rotation for all users

[role="_abstract"]
This procedure sets a default screen rotation for *all* users on a system and is suitable for mass deployment of homogenized display configuration.

.Procedure

. Prepare the preferable setup for a single user as in xref:configuring-screen-rotation-single-user_configuring-screen-rotation[Configuring the screen rotation for a single user].

. Copy the *transform* section of the [file]`~/.config/monitors.xml` configuration file, which configures the screen rotation. An example portrait orientation: 
+
[source, xml]
----
<?xml version="1.0" encoding="UTF-8"?>

<transform>
  <rotation>left</rotation>
  <flipped>no</flipped>
</transform>
----

. Paste the content in the [file]`/etc/xdg/monitors.xml` file that stores system-wide configuration.

. Save the changes. 

The new setup takes effect for all the users the next time they log in in the system.

[role="_additional-resources"]
.Additional resources
* xref:configuring-screen-rotation-single-user_configuring-screen-rotation[Configuring screen rotation for a single user]

