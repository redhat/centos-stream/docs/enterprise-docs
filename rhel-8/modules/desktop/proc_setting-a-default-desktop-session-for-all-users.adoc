// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>


[id="proc_setting-a-default-desktop-session-for-all-users_{context}"]
= Setting a default desktop session for all users

[role="_abstract"]
You can configure a default desktop session that is preselected for all users that have not logged in yet.

If a user logs in using a different session than the default, their selection persists to their next login.

.Procedure

. Copy the configuration file template:
+
[literal,subs="+quotes"]
....
# cp /usr/share/accountsservice/user-templates/standard \
     /etc/accountsservice/user-templates/standard
....

. Edit the new `/etc/accountsservice/user-templates/standard` file. On the `Session=__gnome__` line, replace `__gnome__` with the session that you want to set as the default.

. Optional: To configure an exception to the default session for a certain user, follow these steps:
.. Copy the template file to `/var/lib/AccountsService/users/__user-name__`:
+
[literal,subs="+quotes"]
....
# cp /usr/share/accountsservice/user-templates/standard \
     /var/lib/AccountsService/users/__user-name__
....

.. In the new file, replace variables such as `${USER}` and `${ID}` with the user values.
.. Edit the `Session` value.
