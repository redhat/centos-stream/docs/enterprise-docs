:_mod-docs-content-type: PROCEDURE

[id="proc_enabling-the-red-hat-flatpak-remote_{context}"]
= Enabling the Red Hat Flatpak remote

[role="_abstract"]
This procedure configures the {RH} Container Catalog as a Flatpak remote on your system.

.Prerequisites

* You have an account on the {RH} Customer Portal.
+
NOTE: For large-scale deployments where the users do not have Customer Portal accounts, {RH} recommends using registry service accounts. For details, see link:https://access.redhat.com/terms-based-registry/[Registry Service Accounts].

.Procedure

. Enable the `rhel` Flatpak remote:
+
----
$ flatpak remote-add \
          --if-not-exists \
          rhel \
          https://flatpaks.redhat.io/rhel.flatpakrepo
----

. Log into the {RH} Container Catalog:
+
[subs=+quotes]
----
$ podman login registry.redhat.io

Username: __your-user-name__
Password: __your-password__
----
+
Provide the credentials to your {RH} Customer Portal account or your registry service account tokens.
+
By default, Podman saves the credentials only until you log out.

. Optional: Save your credentials permanently. Use one of the following options:

** Save the credentials for the current user:
+
----
$ cp $XDG_RUNTIME_DIR/containers/auth.json \
     $HOME/.config/flatpak/oci-auth.json
----

** Save the credentials system-wide:
+
----
# cp $XDG_RUNTIME_DIR/containers/auth.json \
     /etc/flatpak/oci-auth.json
----
+
For best practices, {RH} recommends that you log into the {RH} Container Catalog using registry account tokens when installing credentials system-wide.

.Verification

* List the enabled Flatpak remotes:
+
----
$ flatpak remotes

Name    Options
rhel    system,oci,no-gpg-verify
----

// [role="_additional-resources"]
// .Additional resources
// * A bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

