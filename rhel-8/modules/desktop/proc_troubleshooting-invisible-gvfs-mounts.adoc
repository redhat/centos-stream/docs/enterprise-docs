:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="troubleshooting-invisible-gvfs-mounts_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Troubleshooting access to GVFS locations from non-GIO clients
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
If you have problems accessing GVFS locations from your application, it might mean that it is not native GIO client.
Native GIO clients are typically all GNOME applications using GNOME libraries (`glib`, `gio`). The `gvfs-fuse` service is provided as a fallback for non-GIO clients.

.Prerequisite
* The `gvfs-fuse` package is installed.
+
[subs="+attributes"]
----
$ {PackageManagerCommand} install gvfs-fuse
----

.Procedure

. Ensure that `gvfs-fuse` is running.
+
----
$ ps ax | grep gvfsd-fuse
----
+
If `gvfs-fuse` is not running, log out and log back in. {RH} does not recommend starting `gvfs-fuse` manually.

. Find the system user ID (UID) for the `/run/user/_UID_/gvfs/` path.
+
The `gvfsd-fuse` daemon requires a path where it can expose its services. When the `/run/user/_UID_/gvfs/` path is unavailable, `gvfsd-fuse` uses the `~/.gvfs` path.
+
----
$ id -u
----

. If `gvfsd-fuse` is still not running, start the `gvfsd-fuse` daemon:
+
----
$ /usr/libexec/gvfsd-fuse -f /run/user/_UID_/gvfs
----
+
Now, the FUSE mount is available, and you can manually browse for the path in your application.

. Find the GVFS mounts under the `/run/user/_UID_/gvfs/` or `~/.gvfs` locations.
