:_mod-docs-content-type: PROCEDURE

[id="configuring-an-ipoib-connection-by-using-the-network-system-role_{context}"]
= Configuring an IPoIB connection by using the `network` {RHELSystemRoles}
You can use IP over InfiniBand (IPoIB) to send IP packets over an InfiniBand interface. To configure IPoIB, create a NetworkManager connection profile. By using Ansible and the `network` system role, you can automate this process and remotely configure connection profiles on the hosts defined in a playbook.

You can use the `network` RHEL system role to configure IPoIB and, if a connection profile for the InfiniBand's parent device does not exist, the role can create it as well.


.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]
// The following are prerequisites that are specific for this task:
* An InfiniBand device named `mlx4_ib0` is installed in the managed nodes.
* The managed nodes use NetworkManager to configure the network.


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure the network
  hosts: managed-node-01.example.com
  tasks:
    - name: IPoIB connection profile with static IP address settings
      ansible.builtin.include_role:
        name: rhel-system-roles.network
      vars:
        network_connections:
          # InfiniBand connection mlx4_ib0
          - name: mlx4_ib0
            interface_name: mlx4_ib0
            type: infiniband

          # IPoIB device mlx4_ib0.8002 on top of mlx4_ib0
          - name: mlx4_ib0.8002
            type: infiniband
            autoconnect: yes
            infiniband:
              p_key: 0x8002
              transport_mode: datagram
            parent: mlx4_ib0
            ip:
              address:
                - 192.0.2.1/24
                - 2001:db8:1::1/64
            state: up
....
+
The settings specified in the example playbook include the following:
+
`type: _<profile_type>_`:: Sets the type of the profile to create. The example playbook creates two connection profiles: One for the InfiniBand connection and one for the IPoIB device.
`parent: _<parent_device>_`:: Sets the parent device of the IPoIB connection profile.
`p_key: _<value>_`:: Sets the InfiniBand partition key. If you set this variable, do not set `interface_name` on the IPoIB device.
`transport_mode: _<mode>_`:: Sets the IPoIB connection operation mode. You can set this variable to `datagram` (default) or `connected`.

+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification

. Display the IP settings of the `mlx4_ib0.8002` device:
+
[literal,subs="+quotes"]
....
# *ansible managed-node-01.example.com -m command -a 'ip address show mlx4_ib0.8002'*
managed-node-01.example.com | CHANGED | rc=0 >>
...
inet __192.0.2.1/24 brd 192.0.2.255__ scope global noprefixroute ib0.8002
   valid_lft forever preferred_lft forever
inet6 __2001:db8:1::1/64__ scope link tentative noprefixroute 
   valid_lft forever preferred_lft forever
....

. Display the partition key (P_Key) of the `mlx4_ib0.8002` device:
+
[literal,subs="+quotes"]
....
# *ansible managed-node-01.example.com -m command -a 'cat /sys/class/net/mlx4_ib0.8002/pkey'*
managed-node-01.example.com | CHANGED | rc=0 >>
0x8002
....

. Display the mode of the `mlx4_ib0.8002` device:
+
[literal,subs="+quotes"]
....
# *ansible managed-node-01.example.com -m command -a 'cat /sys/class/net/mlx4_ib0.8002/mode'*
managed-node-01.example.com | CHANGED | rc=0 >>
datagram
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file
* `/usr/share/doc/rhel-system-roles/network/` directory

