:_mod-docs-content-type: PROCEDURE

:experimental:

[id="configuring-an-ipoib-connection-using-nm-connection-editor_{context}"]
= Configuring an IPoIB connection by using nm-connection-editor

[role="_abstract"]
The `nmcli-connection-editor` application configures and manages network connections stored by NetworkManager by using the management console.

.Prerequisites

* An InfiniBand device is installed on the server.
* The corresponding kernel module is loaded.
* The `nm-connection-editor` package is installed.

.Procedure

. Enter the command:
+
[literal,subs="+quotes,attributes"]
....
$ **nm-connection-editor**
....

. Click the kbd:[+] button to add a new connection.

. Select the [gui]`InfiniBand` connection type and click btn:[Create].

. On the [gui]`InfiniBand` tab:

.. Change the connection name if you want to.

.. Select the transport mode.

.. Select the device.

.. Set an MTU if needed.

. On the [gui]`IPv4 Settings` tab, configure the IPv4 settings. For example, set a static IPv4 address, network mask, default gateway, and DNS server:
image:infiniband-IPv4-settings-nm-connection-editor.png[]

. On the [gui]`IPv6 Settings` tab, configure the IPv6 settings. For example, set a static IPv6 address, network mask, default gateway, and DNS server:
image:infiniband-IPv6-settings-nm-connection-editor.png[]

. Click btn:[Save] to save the team connection.

. Close `nm-connection-editor`.

. You can set a `P_Key` interface. As this setting is not available in `nm-connection-editor`, you must set this parameter on the command line.
+
For example, to set `0x8002` as `P_Key` interface of the `mlx4_ib0` connection:
+
[literal,subs="+quotes,attributes"]
....
# **nmcli connection modify mlx4_ib0 infiniband.p-key 0x8002**
....
