:_mod-docs-content-type: PROCEDURE

:experimental:

[id="configuring-an-ipoib-connection-using-nmcli-commands_{context}"]
= Configuring an IPoIB connection by using `nmcli`

[role="_abstract"]
You can use the `nmcli` utility to create an IP over InfiniBand connection on the command line.


.Prerequisites

* An InfiniBand device is installed on the server
* The corresponding kernel module is loaded


.Procedure

. Create the InfiniBand connection to use the `mlx4_ib0` interface in the `Connected` transport mode and the maximum MTU of `65520` bytes:
+
[literal,subs="+quotes,attributes"]
....
# **nmcli connection add type infiniband con-name mlx4_ib0 ifname mlx4_ib0 transport-mode Connected mtu 65520**
....

. Set a `P_Key`, for example:
+
[literal,subs="+quotes,attributes"]
....
# **nmcli connection modify mlx4_ib0 infiniband.p-key 0x8002**
....

. Configure the IPv4 settings:
+
** To use DHCP, enter:
+
[literal,subs="+quotes"]
....
# *nmcli connection modify mlx4_ib0 ipv4.method auto*
....
+
Skip this step if `ipv4.method` is already set to `auto` (default).

** To set a static IPv4 address, network mask, default gateway, DNS servers, and search domain, enter:
+
[literal,subs="+quotes"]
....
# *nmcli connection modify mlx4_ib0 ipv4.method manual ipv4.addresses 192.0.2.1/24 ipv4.gateway 192.0.2.254 ipv4.dns 192.0.2.200 ipv4.dns-search example.com*
....

. Configure the IPv6 settings:
+
** To use stateless address autoconfiguration (SLAAC), enter:
+
[literal,subs="+quotes"]
....
# *nmcli connection modify mlx4_ib0 ipv6.method auto*
....
+
Skip this step if `ipv6.method` is already set to `auto` (default).

** To set a static IPv6 address, network mask, default gateway, DNS servers, and search domain, enter:
+
[literal,subs="+quotes"]
....
# *nmcli connection modify mlx4_ib0 ipv6.method manual ipv6.addresses 2001:db8:1::fffe/64 ipv6.gateway 2001:db8:1::fffe ipv6.dns 2001:db8:1::ffbb ipv6.dns-search example.com*
....

. To customize other settings in the profile, use the following command:
+
[literal,subs="+quotes"]
....
# *nmcli connection modify mlx4_ib0 _<setting>_ _<value>_*
....
+
Enclose values with spaces or semicolons in quotes.

. Activate the profile:
+
[literal,subs="+quotes"]
....
# *nmcli connection up mlx4_ib0*
....


.Verification

* Use the `ping` utility to send ICMP packets to the remote host’s InfiniBand adapter, for example:
+
[subs="+quotes"]
....
# *ping -c5 192.0.2.2*
....

