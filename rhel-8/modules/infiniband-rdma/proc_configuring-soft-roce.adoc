:_mod-docs-content-type: PROCEDURE

:experimental:

[id="configuring-soft-roce_{context}"]
= Configuring Soft-RoCE

[role="_abstract"]
Soft-RoCE is a software implementation of remote direct memory access (RDMA) over Ethernet, which is also called RXE. Use Soft-RoCE on hosts without RoCE host channel adapters (HCA).

[IMPORTANT]
====
ifeval::[{ProductNumber} >= 9]
The Soft-RoCE feature is deprecated and will be removed in RHEL 10.
endif::[]

Soft-RoCE is provided as a Technology Preview only. Technology Preview features are not supported with Red Hat production Service Level Agreements (SLAs), might not be functionally complete, and Red Hat does not recommend using them for production. These previews provide early access to upcoming product features, enabling customers to test functionality and provide feedback during the development process.

See link:https://access.redhat.com/support/offerings/techpreview[Technology Preview Features Support Scope] on the Red Hat Customer Portal for information about the support scope for Technology Preview features. 
====

.Prerequisites

* An Ethernet adapter is installed

.Procedure

. Install the `iproute`, `libibverbs`, `libibverbs-utils`, and `infiniband-diags` packages:
+
[literal,subs="+quotes,attributes"]
....
# **`{PackageManagerCommand}` install iproute libibverbs libibverbs-utils infiniband-diags**
....

. Display the RDMA links:
+
[literal,subs="+quotes,attributes"]
....
# **rdma link show**
....

. Add a new `rxe` device named `rxe0` that uses the `enp0s1` interface:
+
[literal,subs="+quotes,attributes"]
....
# **rdma link add rxe0 type rxe netdev enp1s0**
....

.Verification

. View the state of all RDMA links:
+
[literal,subs="+quotes,attributes"]
....
# **rdma link show**

link rxe0/1 state ACTIVE physical_state LINK_UP netdev enp1s0
....

. List the available RDMA devices:
+
[literal,subs="+quotes,attributes"]
....
# **ibv_devices**

    device          	   node GUID
    ------          	----------------
    rxe0            	505400fffed5e0fb
....
+

. You can use the `ibstat` utility to display a detailed status:
+
[literal,subs="+quotes,attributes"]
....
# **ibstat rxe0**

CA 'rxe0'
	CA type:
	Number of ports: 1
	Firmware version:
	Hardware version:
	Node GUID: 0x505400fffed5e0fb
	System image GUID: 0x0000000000000000
	Port 1:
		State: Active
		Physical state: LinkUp
		Rate: 100
		Base lid: 0
		LMC: 0
		SM lid: 0
		Capability mask: 0x00890000
		Port GUID: 0x505400fffed5e0fb
		Link layer: Ethernet
....
