:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_installing-java-on-rhel-8.adoc

// This module can be included from assemblies using the following include statement:
// include::modules/development/proc_installing-multiple-minor-versions-of-openjdk-on-rhel.adoc[leveloffset=+1]

[id="installing-multiple-minor-versions-of-openjdk-on-rhel_{context}"]
= Installing multiple minor versions of OpenJDK on RHEL using {PackageManagerCommand}

You can install multiple minor versions of OpenJDK on RHEL. This is done by preventing the installed minor versions from being updated.

[discrete]
== Procedure

* Add the [option]`installonlypkgs` option in [filename]`/etc/yum.conf` to specify the Java packages that [package]`{PackageManagerCommand}` can install but not update.
+
`installonlypkgs=java-_<version>_--openjdk,java-_<version>_--openjdk-headless,java-_<version>_--openjdk-devel`
+
Updates will install new packages while leaving the old versions on the system:
+
[literal, subs="+quotes"]
----
$ rpm -qa | grep java-1.8.0-openjdk
java-1.8.0-openjdk-1.8.0.242.b08-0.el8_1.x86_64
java-1.8.0-openjdk-headless-1.8.0.242.b08-0.el8_1.x86_64
----

* The different minor versions of OpenJDK can be found in the [filename]`/usr/lib/jvm/_<minor version>_` files.
+
For example, the following shows part of [filename]`/usr/lib/jvm/java-1.8.0-openjdk-1.8.0`:
+
----
$ /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.242.b08-0.el8_1.x86_64/bin/java -version
openjdk version "1.8.0_242"
OpenJDK Runtime Environment (build 1.8.0_242-b08)
OpenJDK 64-Bit Server VM (build 25.242-b08, mixed mode)

$ /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.172-7.b11.el7.x86_64/bin/java -version
openjdk version "1.8.0_172"
OpenJDK Runtime Environment (build 1.8.0_172-b11)
OpenJDK 64-Bit Server VM (build 25.172-b11, mixed mode)
----

You can choose system-wide using version by following xref:selecting-a-system-wide-java-version_configuring-java-on-rhel-8[Selecting a system-wide java version].
