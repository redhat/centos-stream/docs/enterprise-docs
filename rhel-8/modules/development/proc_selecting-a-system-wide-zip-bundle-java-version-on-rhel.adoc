:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Configuring Java on RHEL 8

// This module can be included from assemblies using the following include statement:
// include::modules/development/proc_selecting-a-system-wide-zip-bundle-java-version.adoc[leveloffset=+1]

[id="selecting-a-system-wide-zip-bundle-java-version_{context}"]
= Selecting a system-wide ZIP-bundle Java version

If you have multiple version of Java installed on RHEL 8 using [filename]`ZIP` bundles, you can select a specific Java version to use system-wide.

[discrete]
== Prerequisites

* Know the locations of the Java versions installed using [filename]`ZIP` bundles.

[discrete]
== Procedure

To specify the Java version to use for a single session:

. Configure [variable]`JAVA_HOME` with the path to the Java version you want used system-wide.
+

[command]`$ export JAVA_HOME=/opt/jdk/jdk-11.0.3`

. Add [filename]`$JAVA_HOME/bin` to the [variable]`PATH` environment variable.
+
[command]`$ export PATH="$JAVA_HOME/bin:$PATH"`

To specify the Java version to use permanently for a single user, add these commands into [filename]`~/.bashrc`:
----
export JAVA_HOME=/opt/jdk/jdk-11.0.3
export PATH="$JAVA_HOME/bin:$PATH"
----

To specify the Java version to use permanently for all users, add these commands into [filename]`/etc/bashrc`:
----
export JAVA_HOME=/opt/jdk/jdk-11.0.3
export PATH="$JAVA_HOME/bin:$PATH"
----

[NOTE]
Be aware of the exact meaning of `JAVA_HOME`. For more information, see link:https://fedoraproject.org/wiki/Changes/Decouple_system_java_setting_from_java_command_setting[Changes/Decouple system java setting from java command setting]. If you do not want to redefine `JAVA_HOME`, add only the PATH command to [filename]`bashrc`, specifying the path to the Java binary. For example, [command]`export PATH="/opt/jdk/jdk-11.0.3/bin:$PATH"`.
