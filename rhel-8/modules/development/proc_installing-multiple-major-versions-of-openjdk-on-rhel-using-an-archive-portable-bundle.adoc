:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_installing-multiple-major-versions-of-openjdk-on-rhel-using-an-archive-portable-bundle.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="installing-multiple-major-versions-of-openjdk-on-rhel-using-an-archive-portable-bundle_{context}"]
= Installing multiple major versions of OpenJDK on RHEL using an archive portable bundle

You can install multiple major versions of OpenJDK by using the same procedures found in xref:installing-jvm-on-rhel-using-an-archive_{context}[Installing a JRE on RHEL using an archive portable bundle] or xref:installing-the-jdk-on-rhel-8-using-an-archive-bundle_{context}[Installing a JDK on RHEL 8 using an archive portable bundle] using multiple major versions.

[NOTE]
For instructions how to configure the default Java version for the system, see xref:interactively-selecting-a-java-version_configuring-java-on-rhel-8[Selecting a system-wide zip-bundle java version].
