:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_installing-java-on-rhel-8.adoc

// This module can be included from assemblies using the following include statement:
// include::modules/development/proc_installing-the-jdk-on-rhel-8-using-an-archive.adoc[leveloffset=+1]

[id="installing-the-jdk-on-rhel-8-using-an-archive-bundle_{context}"]
= Installing a JDK on RHEL 8 using an archive portable bundle

You can install an OpenJDK (Open Java Development Kit) using the ZIP bundle. This is useful if the Java administrator does not have root privileges.

[NOTE]
It is recommended that you create a parent directory to contain your JDKs and create a symbolic link to the latest JDK using a generic path. This eases upgrades to later versions.

[discrete]
== Procedure

. link:https://access.redhat.com/jbossnetwork/restricted/listSoftware.html?product=core.service.openjdk&downloadType=distributions[Download the latest version of the JDK ZIP bundle for Linux].

. Extract the contents of the ZIP bundle to a directory of your choice:
+
----
$ mkdir ~/jdks
$ cd ~/jdks
$ tar -xf java-1.8.0-openjdk-1.8.0.242.b08-1.static.jdk.openjdkportable.x86_64.tar.xz
----

. Create a generic path by using symbolic links to your JRE for easier upgrades:
+
`$ ln -s ~/jdks/java-1.8.0-openjdk-1.8.0.242.b08-1.static.jdk.openjdkportable.x86_64 ~/jdks/java-8`

. Configure the [variable]`JAVA_HOME` environment variable:
+
----
$ export JAVA_HOME=~/jdks/java-8
----

. Verify that [variable]`JAVA_HOME` environment variable is set correctly:
+
----
$ printenv | grep JAVA_HOME
JAVA_HOME=~/jdks/java-8
----
+
[NOTE]
When installed using this method, Java will only be available for the current user.

. Add the [filename]`bin` directory of the generic JDK path to the [variable]`PATH` environment variable:
+
----
$ export PATH="$JAVA_HOME/bin:$PATH"
----

. Verify that `javac -version` works without supplying the full path:
+
----
$ javac -version
javac "1.8.0_242"
----

[NOTE]
You can ensure that [variable]`JAVA_HOME` environment variable persists for the current user by exporting the environment variable in [filename]`~/.bashrc`.

// [discrete]
// == Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
