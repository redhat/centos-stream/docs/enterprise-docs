:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// Configuring Java on RHEL 8

// This module can be included from assemblies using the following include statement:
// include::modules/development/proc_selecting-a-system-wide-java-version.adoc[leveloffset=+1]

[id="selecting-a-system-wide-java-version_{context}"]
= Non-interactively selecting a system-wide Java version on RHEL

If you have multiple versions of Java installed on RHEL, you can select the default Java version to use system-wide in a non-interactive way. This is useful for administrators who have root privileges on a Red Hat Enterprise Linux system and need to switch the default Java on many systems in an automated way.

[NOTE]
If you do not have root privileges, you can select a Java version by
xref:configuring-the-java_home-environment-variable-on-rhel_configuring-java-on-rhel-8[configuring the `JAVA_HOME` environment variable.]

[discrete]
== Prerequisites

* You must have root privileges on the system.

* Multiple versions of Java were installed using the [utility]`{PackageManagerCommand}` package manager.

[discrete]
== Procedure

. Select the major Java version to switch to. For example, for Java 11, use java-11-openjdk.
+
----
# PKG_NAME=java-11-openjdk`
# JAVA_TO_SELECT=$(alternatives --display java | grep "family $PKG_NAME" | cut -d' ' -f1)`
# alternatives --set java $JAVA_TO_SELECT`
----

. Verify that the active Java version is the one you specified.
+
----
$ java -version
openjdk version "11.0.3" 2019-04-16 LTS
OpenJDK Runtime Environment 18.9 (build 11.0.3+7-LTS)
OpenJDK 64-Bit Server VM 18.9 (build 11.0.3+7-LTS, mixed mode, sharing)
----
+
[NOTE]
For Java 8, set [variable]`PKG_NAME` to [option]`java-1.8.0-openjdk`.

+
[NOTE]
A similar approach can be followed for `javac`.
