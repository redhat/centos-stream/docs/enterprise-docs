:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies and titles:
//
// enterprise/assemblies/assembly_generating-an-sos-report-for-technical-support.adoc

[id="generating-an-sos-report-from-the-command-line_{context}"]
= Generating an `sos` report from the command line

[role="_abstract"]
Use the [command]`sos report` command to gather an [systemitem]`sos` report from a RHEL server.

.Prerequisites

* You have installed the [package]`sos` package.
* You need `root` privileges.

.Procedure

. Run the [command]`sos report` command and follow the on-screen instructions.
You can add the [option]`--upload` option to transfer the [systemitem]`sos` report to Red Hat immediately after generating it.
+
[literal,subs="+quotes,attributes"]
....
[user@server1 ~]$ *sudo sos report*
[sudo] password for user:

sos report (version 4.2)

This command will collect diagnostic and configuration information from
this Red Hat Enterprise Linux system and installed applications.

An archive containing the collected information will be generated in
/var/tmp/sos.qkn_b7by and may be provided to a Red Hat support
representative.

...

*Press ENTER to continue, or CTRL-C to quit.*
....

. Optional: If you have already opened a Technical Support case with Red Hat, enter the case number to embed it in the [systemitem]`sos` report file name,
and it will be uploaded to that case if you specified the `--upload` option. If you do not have a case number, leave this field blank.
Entering a case number is optional and does not affect the operation of the `sos` utility.
+
[literal,subs="+quotes,attributes"]
....
Please enter the case id that you are generating this report for []: *_<8-digit_case_number>_*
....

. Take note of the [systemitem]`sos` report file name displayed at the end of the console output.
+
[literal,subs="+quotes,attributes"]
....
...
Finished running plugins
Creating compressed archive...

Your sos report has been generated and saved in:
*/var/tmp/sosreport-server1-12345678-2022-04-17-qmtnqng.tar.xz*

Size    *16.51MiB*
Owner   *root*
sha256  *bf303917b689b13f0c059116d9ca55e341d5fadcd3f1473bef7299c4ad2a7f4f*

Please send this file to your support representative.
....


[NOTE]
====
* You can use the [option]`--batch` option to generate an [systemitem]`sos` report without prompting for interactive input.

[literal,subs="+quotes,attributes"]
....
[user@server1 ~]$ sudo sos report *--batch* --case-id _<8-digit_case_number>_
....

* You can also use the [option]`--clean` option to obfuscate a just-collected `sos` report.

[literal,subs="+quotes,attributes"]
....
[user@server1 ~]$ sudo sos report *--clean*
....
====


.Verification

* Verify that the [systemitem]`sos` utility created an archive in [filename]`/var/tmp/` matching the description from the command output.
+
[literal,subs="+quotes,attributes"]
....
[user@server1 ~]$ *sudo ls -l /var/tmp/sosreport**
[sudo] password for user:
-rw-------. 1 root root *17310544* Sep 17 19:11 */var/tmp/sosreport-server1-12345678-2022-04-17-qmtnqng.tar.xz*
....


[role="_additional-resources"]
.Additional resources
* xref:methods-for-providing-an-sos-report-to-red-hat-technical-support_generating-an-sosreport-for-technical-support[Methods for providing an `sos` report to Red Hat technical support]
