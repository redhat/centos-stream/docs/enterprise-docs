:_mod-docs-content-type: PROCEDURE
// This module is included in the following assemblies and titles:
//
// enterprise/assemblies/assembly_generating-and-maintaining-the-diagnostic-reports-using-the-rhel-web-console.adoc


[id="generating-diagnostic-reports-using-the-rhel-web-console_{context}"]
= Generating diagnostic reports using the RHEL web console

[role ="_abstract"]


.Prerequisites

* The RHEL web console has been installed. For details, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumber}/html-single/managing_systems_using_the_rhel_{ProductNumber}_web_console/index#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].
* The `cockpit-storaged` package is installed on your system. 
* You have administrator privileges.

.Procedure

. Log in to the RHEL web console. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#logging-in-to-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Logging in to the web console].
 
. In the left side menu, select **Tools >> Diagnostic reports**.

. To generate a new diagnostic report, click the btn:[Run report] button.
+
image::sosreport-web-console.png[Screenshot of the Run report screen]

. Enter the label for the report you want to create.

. Optional: Customize your report.

.. Enter the encryption passphrase to encrypt your report.
If you want to skip the encryption of the report, leave the field empty.

.. Check the checkbox **Obfuscate network addresses, hostnames, and usernames** to obfuscate certain data.

.. Check the checkbox **Use verbose logging** to increase logging verbosity.

. Click the btn:[Run report] button to generate a report and wait for the process to complete. You can stop generating the report using the btn:[Stop report] button.
