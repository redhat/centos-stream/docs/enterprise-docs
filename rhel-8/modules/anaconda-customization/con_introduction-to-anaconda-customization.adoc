:_mod-docs-content-type: CONCEPT
[id="introduction-to-anaconda-customization_{context}"]
= Introduction to Anaconda Customization


The {productname} and Fedora installation program, [application]*Anaconda*, brings many improvements in its most recent versions. One of these improvements is enhanced customizability. You can now write add-ons to extend the base installer functionality, and change the appearance of the graphical user interface.

This document will explain how to customize the following:

* Boot menu - pre-configured options, color scheme and background

* Appearance of the graphical interface - logo, backgrounds, product name

* Installer functionality - add-ons which can enhance the installer by adding new Kickstart commands and new screens in the graphical and textual user interfaces

//Some of the topics discussed in this book require significant pre-existing knowledge. In particular, developing custom [application]*Anaconda* add-ons requires knowledge of Python, making changes to the boot menu requires involves editing plain text configuration files, and visual customizations of the installer require familiarity with computer graphics and cascading style sheets (CSS).

Also note that this document applies only to {productname}{nbsp}8 and Fedora{nbsp}17 and later.

[IMPORTANT]
====

Procedures described in this book are written for {productname}{nbsp} {ProductNumber} or a similar system. On other systems, the tools and applications used (such as [command]`genisoimage` for creating custom ISO images) may be different, and procedures may need to be adjusted.

====

.Support Statement

Red Hat supports only customizing the {ProductName} installation media and images by using {ProductName} Image Builder. Alternatively, you can use Kickstart to deploy consistent systems in your infrastructure. 