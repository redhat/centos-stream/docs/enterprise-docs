:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// * file name: my-concept-module-a.adoc
// * ID: [id="my-concept-module-a-{context}"]
// * Title: = My concept module A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="architecture-of-anaconda_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Anaconda Architecture 

[application]*Anaconda* is a set of Python modules and scripts. It also uses several external packages and libraries. The major components of this toolset include the following packages:

* `pykickstart` - parses and validates the Kickstart files. Also, provides data structure that stores values that drive the installation. 

* `{PackageManagerCommand}` - the package manager that installs packages and resolves dependencies

* `blivet` - handles all activities related to storage management

* `pyanaconda` - contains the user interface and modules for [application]*Anaconda*, such as keyboard and timezone selection, network configuration, and user creation. Also provides various utilities to perform system-oriented functions

* `python-meh` - contains an exception handler that gathers and stores additional system information in case of a crash and passes this information to the `libreport` library, which itself is a part of the link:++https://fedorahosted.org/abrt/++[ABRT Project]

* `dasbus` - enables communication between the `D-Bus` library with modules of anaconda and with external components
 
* `python-simpleline` - text UI framework library to manage user interaction in the *Anaconda* text mode

* `gtk` - the Gnome toolkit library for creating and managing GUI

Apart from the division into packages previously mentioned, *Anaconda* is internally divided into the user interface and a set of modules that run as separate processes and communicate using the `D-Bus` library. These modules are:

* `Boss` - manages the internal module discovery, lifecycle, and coordination

* `Localization` - manages locales

* `Network` - handles network

* `Payloads` - handles data for installation in different formats, such as `rpm`, `ostree`, `tar` and other installation formats. Payloads manage the sources of data for installation; sources can vary in format such as CD-ROM, HDD, NFS, URLs, and other sources

* `Security` - manages security related aspects

* `Services` - handles services

* `Storage` - manages storage using `blivet`

* `Subscription` - handles the `subscription-manager` tool and Insights.

* `Timezone` - deals with time, date, zones, and time synchronization.

* `Users` - creates users and groups.

Each module declares which parts of Kickstart it handles, and has methods to apply the configuration from Kickstart to the installation environment and to the installed system.

The Python code portion of Anaconda (`pyanaconda`) starts as a “main” process that owns the user interface. Any Kickstart data you provide are parsed using the `pykickstart` module and the `Boss` module is started, it discovers all other modules, and starts them. Main process then sends Kickstart data to the modules according to their declared capabilities. Modules process the data, apply the configuration to the installation environment, and the UI validates if all required choices have been made. If not, you must supply the data in an interactive installation mode. Once all required choices have been made, the installation can start - the modules write data to the installed system.
