:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="creating-custom-boot-images_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Creating custom boot images
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

After you customize the boot images and the GUI layout, create a new image that includes the changes you made.

To create custom boot images, follow the procedure below.

.Procedure

. Make sure that all of your changes are included in the working directory. For example, if you are testing an add-on, make sure to place the `product.img` in the `images/` directory.

. Make sure your current working directory is the top-level directory of the extracted ISO image, for example, `/tmp/ISO/iso/`.

. Create a new ISO image using the [command]`genisoimage`:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....

pass:quotes[`#`] genisoimage -U -r -v -T -J -joliet-long -V "RHEL-{ProductNumber} Server.x86_64" -volset "RHEL-{ProductNumber} Server.x86_64" -A "RHEL-{ProductNumber} Server.x86_64" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot -o ../NEWISO.iso .

....
+
In the above example:
+
** Make sure that the values for [option]`-V`, [option]`-volset`, and [option]`-A` options match the image's boot loader configuration, if you are using the `LABEL=` directive for options that require a location to load a file on the same disk. If your boot loader configuration (`isolinux/isolinux.cfg` for BIOS and `EFI/BOOT/grub.cfg` for UEFI) uses the [option]`inst.stage2=LABEL=pass:attributes[{blank}]_disk_label_pass:attributes[{blank}]` stanza to load the second stage of the installer from the same disk, then the disk labels must match.
+
[IMPORTANT]
====

In boot loader configuration files, replace all spaces in disk labels with `\x20`. For example, if you create an ISO image with a `RHEL {ProductNumber}.0` label, boot loader configuration should use `RHEL\x20{ProductNumber}.0`.

====
+
** Replace the value of the [option]`-o` option (`-o ../NEWISO.iso`) with the file name of your new image. The value in the example creates the `NEWISO.iso` file in the directory _above_ the current one.
+
For more information about this command, see the `genisoimage(1)` man page on your system.

. Implant an MD5 checksum into the image. Note that without an MD5 checksu, the image verification check might fail (the [option]`rd.live.check` option in the boot loader configuration) and the installation can hang.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
pass:quotes[`#`] implantisomd5 ../NEWISO.iso
....
+
In the above example, replace _../NEWISO.iso_ with the file name and the location of the ISO image that you have created in the previous step.
+
You can now write the new ISO image to physical media or a network server to boot it on physical hardware, or you can use it to start installing a virtual machine.

[role="_additional-resources"]
.Additional resources

* For instructions on preparing boot media or network server, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/interactively_installing_rhel_over_the_network/index#installation-source-boot-options_custom-boot-options[Advanced installation boot options].

* For instructions on creating virtual machines with ISO images, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/index[Configuring and Managing Virtualization].
