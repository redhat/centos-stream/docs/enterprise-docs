:_mod-docs-content-type: PROCEDURE

[id="pre-conversion-analysis-fails-to-complete_{context}"]
= Pre-conversion analysis task fails to complete

[role="_abstract"]
After running the *Pre-conversion analysis for converting to RHEL* task, one or more of the systems can fail to generate a report with the error message *Task failed to complete for an unknown reason. Retry this task at a later time.* If this issue occurs, complete the steps below to troubleshoot.

.Procedure

. Verify if the affected system is unavailable, for example because of a network accessibility issue or because the system is shut off.
. Review the `RHC systemd` service (`rhcd`) for errors:
.. Stop rhcd in your terminal:
+
[subs="+quotes"]
----
# *systemctl stop rhcd*
----
.. Set `rhcd` logging to the highest level:
+
[subs="+quotes"]
----
# *sed -ie 's%error%trace%' /etc/rhc/config.toml*
----
.. Restart `rhcd`:
+
[subs="+quotes"]
----
# *systemctl start rhcd*
----
.. Review error messages posted by `rhcd`:
+
[subs="+quotes"]
----
# *journalctl -u rhcd*
----
. Review the `rhc-worker-script` log file for errors:
+
[subs="+quotes"]
----
# *less /var/log/rhc-worker-script/rhc-worker-script.log*
----


