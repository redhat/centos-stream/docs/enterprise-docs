////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

:_mod-docs-content-type: PROCEDURE
[id="proc_preparing-for-a-rhel-conversion_{context}"]
= Preparing for a RHEL conversion
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
This procedure describes the steps that are necessary before performing the conversion from Alma Linux, CentOS Linux, Oracle Linux, or Rocky Linux to Red Hat Enterprise Linux (RHEL).

.Prerequisites

ifdef::converting-to-rhel[]
* You have verified that your system is supported for conversion to RHEL. See xref:con_supported-conversion-paths_converting-from-a-linux-distribution-to-rhel[Supported conversion paths] for more information.
endif::[]
ifndef::converting-to-rhel[]
* You have verified that your system is supported for conversion to RHEL. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/converting_from_a_linux_distribution_to_rhel_using_the_convert2rhel_utility/index#con_supported-conversion-paths_converting-from-a-linux-distribution-to-rhel[Supported conversion paths] for more information.
endif::[]


* You have stopped important applications, database services, and any other services that store data to reduce the risk of data integrity issues.
* You have temporarily disabled antivirus software to prevent the conversion from failing.
* You have disabled or adequately reconfigured any configuration management system, such as Salt, Chef, Puppet, Ansible, to not attempt to restore the original system.
* The `sos` package is installed. You must use this package to generate an `sosreport` that is required when opening a support case for the Red Hat Support team.
* You have created an activation key in Satellite or RHSM. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.8/html/content_management_guide/managing_activation_keys[Managing activation keys] in Satellite documentation and link:https://access.redhat.com/documentation/en-us/subscription_central/2023/html/getting_started_with_activation_keys_on_the_hybrid_cloud_console/index[Getting started with activation keys on the Hybrid Cloud Console] in RHSM documentation.
* You have enabled link:https://access.redhat.com/articles/simple-content-access[Simple Content Access] (SCA). Red Hat accounts created after July 15, 2022 have SCA enabled by default.

.Procedure

. Back up your system and verify that it can be restored if needed.

ifdef::converting-to-rhel[]
. Check xref:ref_known-issues-and-limitations_assembly_troubleshooting-rhel-conversions[Known issues and limitations] and verify that your system is supported for conversion. Apply workarounds where applicable.
endif::[]
ifndef::converting-to-rhel[]
. Check link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/converting_from_a_linux_distribution_to_rhel_using_the_convert2rhel_utility/index#ref_known-issues-and-limitations_assembly_troubleshooting-rhel-conversions[Known issues and limitations] and verify that your system is supported for conversion. Apply workarounds where applicable.
endif::[]
+
////
. Ensure that the standard kernel is the booted kernel:
* Alma Linux: the standard Alma Linux Kernel
* CentOS Linux: the standard CentOS Linux kernel
* Oracle Linux: the Red Hat Compatible Kernel (RHCK)
* Rocky Linux: the standard Rocky Linux Kernel
+
If the kernel your system is booted into is not the standard kernel, for example CentOS realtime kernel or Oracle Linux Unbreakable Enterprise Kernel (UEK), change the default kernel to the standard kernel and reboot your system to apply the changes.
////

. If converting from CentOS Linux 8, remove any CentOS Stream packages from your system. CentOS Stream is not currently supported for conversion, and the conversion might fail if any packages are present on the system.

. If you are converting with a firewall, using Red Hat Satellite, or through a proxy server, ensure that you have access to the following connections:
* \https://cdn.redhat.com
* \https://cdn-public.redhat.com
* \https://subscription.rhsm.redhat.com - required only for systems with firewalls
* \https://*.akamaiedge.net - required only for systems with firewalls
* \https://cert.console.redhat.com

. If converting from CentOS Linux, update the CentOS repository URLs:
+
[subs="quotes,attributes"]
----
# sed -i 's/^mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
# sed -i 's|#baseurl=http://mirror.centos.org|baseurl=https://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
----
+
IMPORTANT: CentOS Linux 7 and CentOS Linux 8 have reached end of life. For more information, see link:https://blog.centos.org/2023/04/end-dates-are-coming-for-centos-stream-8-and-centos-linux-7/[CentOS Linux EOL].

. Install `Convert2RHEL`:
.. Download the Red Hat GPG key:
+
[subs="quotes,attributes"]
----
# curl -o /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release https://security.access.redhat.com/data/fd431d51.txt
----
.. Install the `Convert2RHEL` repository file.
... For conversions to RHEL 7, enter the following command:
+
[subs="+quotes,verbatim,macros"]
----
# curl -o /etc/yum.repos.d/convert2rhel.repo https://cdn-public.redhat.com/content/public/repofiles/convert2rhel-for-rhel-7-x86_64.repo
----
... For conversions to RHEL 8, enter the following command:
+
[subs="+quotes,verbatim,macros"]
----
# curl -o /etc/yum.repos.d/convert2rhel.repo https://cdn-public.redhat.com/content/public/repofiles/convert2rhel-for-rhel-8-x86_64.repo
----
... For conversions to RHEL 9, enter the following command:
+
[subs="+quotes,verbatim,macros"]
----
# curl -o /etc/yum.repos.d/convert2rhel.repo https://cdn-public.redhat.com/content/public/repofiles/convert2rhel-for-rhel-9-x86_64.repo 
----
+
[NOTE]
====
You must perform the conversion with the latest version of the `Convert2RHEL` repository file. If you had previously installed an earlier version of the repository file, remove the earlier version and install the current version.
====
.. Install the `Convert2RHEL` utility:
+
[subs="quotes,attributes"]
----
# {PackageManagerCommand} -y install convert2rhel
----

. Ensure you have access to RHEL packages through one of the following methods:
.. Red Hat Content Delivery Network (CDN) through Red Hat Subscription Manager (RHSM). You must have a Red Hat account and an appropriate RHEL subscription to access RHSM. Note that the OS will be converted to the corresponding minor version of RHEL per Table 1.1.
.. Red Hat Satellite in a version that has Full or Maintenance support. For more information, see link:https://access.redhat.com/support/policy/updates/satellite[Red Hat Satellite Product Life Cycle].
+
[NOTE]
====
Ensure that the Satellite server meets the following conditions:

* Satellite has a subscription manifest with RHEL repositories imported. For more information, see the Managing Red Hat Subscriptions chapter in the Managing Content guide for the particular version of link:https://access.redhat.com/documentation/en-us/red_hat_satellite/[Red Hat Satellite], for example, for link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.14/html/managing_content/managing_red_hat_subscriptions_content-management[version 6.14].
* All required repositories are enabled and synchronized with the latest target OS updates and published on Satellite. Enable at minimum the following repositories for the appropriate major version of the OS:
** Red Hat Enterprise Linux 7 Server RPMs x86_64 7Server
** Red Hat Enterprise Linux 8 for x86_64 - AppStream RPMs <__target_os__>
** Red Hat Enterprise Linux 8 for x86_64 - BaseOS RPMs <__target_os__>
+
Replace _target_os_ with `8.5` for CentOS Linux conversions and `8.9`, `8.8`, or `8.6` for Alma Linux, Oracle Linux, or Rocky Linux conversions.
** Red Hat Enterprise Linux 9 for x86_64 - BaseOS (RPMs)
** Red Hat Enterprise Linux 9 for x86_64 - AppStream (RPMs)
====
.. Custom repositories configured in the `/etc/yum.repos.d/` directory and pointing to a mirror of the target OS repositories. Use custom repositories for systems that have access to only local networks or portable media and therefore cannot access Red Hat CDN through RHSM. Make sure that the repositories contain the latest content available for that RHEL minor version to prevent downgrading and potential conversion failures. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/3176811[Creating a Local Repository and Sharing With Disconnected/Offline/Air-gapped Systems].
+
[NOTE]
====
RHEL 8 and RHEL 9 content is distributed through two default repositories, BaseOS and AppStream. If you are accessing RHEL packages through custom repositories, you must configure both default repositories for a successful conversion. When running the `Convert2RHEL` utility, make sure to enable both repositories using the `--enablerepo` option. For more information about these repositories, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/considerations_in_adopting_rhel_8/repositories_considerations-in-adopting-rhel-8[Considerations in adopting RHEL 8] and link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/considerations_in_adopting_rhel_9/index[Considerations in adopting RHEL 9].
====

. If you are accessing RHEL packages through a Red Hat Satellite server, register your system with Red Hat Satellite. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_satellite/6.14/html/managing_hosts/registering_hosts_to_server_managing-hosts[Registering Hosts and Setting Up Host Integration].

. If you are converting by using  RHSM and have not yet registered the system, update the `/etc/convert2rhel.ini` file to include the following data:
+
[subs="+quotes"]
----
[subscription_manager]
org = _<organization_ID>_
activation_key = _<activation_key>_
----
+
Replace _organization_id_ and _activation_key_ with the organization ID and activation key from the Red Hat Customer Portal if you are using Red Hat CDN. 

. Temporarily disable antivirus software to prevent the conversion from failing. 

. If you are accessing RHEL packages by using custom repositories, disable these repositories. The `Convert2RHEL` utility enables the custom repositories during the conversion process.

. Update the original OS to the minor version supported for conversion as specified in Table 1.1 and then reboot the system.
+
You must perform the conversion with the latest packages from the minor version of the OS that is supported for conversion to use the rollback feature in case the conversion fails.
ifdef::converting-to-rhel[]
For more information, see xref:con_rollback_converting-from-a-linux-distribution-to-rhel[Conversion rollback].
endif::[]
ifndef::converting-to-rhel[]
For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/converting_from_a_linux_distribution_to_rhel_using_the_convert2rhel_utility/index#con_rollback_converting-from-a-linux-distribution-to-rhel[Conversion rollback].
endif::[]
