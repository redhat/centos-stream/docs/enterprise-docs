:_newdoc-version: 2.17.0
:_template-generated: 2024-06-06
:_mod-docs-content-type: PROCEDURE

[id="preparing-for-a-rhel-conversion-with-a-proxy_{context}"]
= Preparing for a RHEL conversion with a proxy server

If you are using a proxy server with your operating system, before running the pre-conversion analysis by using Red{nbsp}Hat Insights to perform the conversion, you must complete the following procedure.

.Prerequisites

* The `http_proxy` environment variable is set:
+
[literal,subs="+quotes,attributes"]
----
# *export http_proxy='_http://<proxy_hostname>:<proxy_port>_'*
----
+
Where http://_<proxy_hostname>:<proxy_port>_ is your proxy server.

* The repository for client tools is set:
+
[literal,subs="+quotes,attributes"]
----
# *curl -o /etc/yum.repos.d/client-tools-for-rhel-7-server.repo https://ftp.redhat.com/redhat/client-tools/client-tools-for-rhel-7-server.repo*
----

* You have the Red{nbsp}Hat GPG public key to verify the downloaded client tools packages:
+
[literal,subs="+quotes,attributes"]
----
# *curl -o /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release https://security.access.redhat.com/data/fd431d51.txt*
----

* The [command]`yum` command is configured to use HTTP proxy. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/4200391[How to enable Proxy Settings for Yum Command on RHEL?].

* The following packages are installed:
** [package]`subscription-manager`
** [package]`subscription-manager-rhsm-certificates`
** [package]`insights-client`
** [package]`rhc`
** [package]`rhc-worker-script`.

.Procedure

. Edit the `/etc/systemd/system/rhcd.service.d/proxy.conf` file for the Remote Host Configuration (RHC) daemon to use your proxy server:  
+
[literal,subs="+quotes,attributes"]
----
*[Service]*
*Environment="HTTP_PROXY=http://_<proxy_hostname>:<proxy_port>_"*
*Environment="HTTPS_PROXY=http://_<proxy_hostname>:<proxy_port>_"*
----
+
Where http://_<proxy hostname>:<proxy port>_ is your proxy server.

. Reload the RHC daemon to apply new configuration:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl daemon-reload*
----

. Edit the `/etc/insights-client/insights-client.conf` file for the Insights to use your proxy server:
+
[literal,subs="+quotes,attributes"]
----
*[insights-client]*
*proxy=http://_<proxy_hostname>:<proxy_port>_*
----
+
Where http://_<proxy_hostname>:<proxy_port>_ is your proxy server.

. Edit the `/etc/rhsm/rhsm.conf` file to configure the Red{nbsp}Hat Subscription Manager to use a proxy server:
+
[literal,subs="+quotes,attributes"]
----
*proxy_hostname = _<proxy_hostname>_*
*proxy_port = _<proxy_port>_*
*proxy_scheme = http*
----
+
Where _<proxy_hostname>_ and _<proxy_port>_ are parameters of you proxy server.

. Register your system with the Red{nbsp}Hat Subscription Manager and Insights:
+
[literal,subs="+quotes,attributes"]
----
# *rhc connect --activation-key _<activation_key>_ --organization _<organization_ID>_*
----
+
Where the _<activation_key>_ and the _<organization_ID>_ are the activation key and organization ID from the link:https://access.redhat.com/management/activation_keys[Red Hat Customer Portal]. For more information on Remote Host Configuration, see the link: https://access.redhat.com/articles/rhc[Remote Host Configuration (rhc)] Knowledgebase article.
[role="_additional-resources"]

.Verification
. Log in to the link:http://console.redhat.com[Red Hat Hybrid Cloud Console] and go to *Red Hat Insights* > *RHEL* > *Inventory* > *Systems*.
. Verify that your CentOS Linux systems appear as expected.

.Next steps

* Proceed to xref:reviewing-the-pre-conversion-analysis-report-using-insights_converting-from-a-linux-distribution-to-rhel-in-insights[review the pre-conversion analysis report using Insights]. 
* You can start xref:proc_converting-to-a-rhel-system-using-insights_converting-from-a-linux-distribution-to-rhel-in-insights[conversion to a RHEL system using Insights]. 

NOTE: The only Red Hat Insights service you can use with registered CentOS Linux systems is the RHEL conversion. All other Insights services are available only after the conversion to RHEL.

[role="_additional-resources"]
.Additional resources

* For more details on how to configure your system with a proxy server, see the following Knowledgebase articles:
** link:https://access.redhat.com/solutions/7005002[How to configure HTTP proxy for Remote Host Configuration (rhc)] (Red Hat Knowledgebase)
** link:https://access.redhat.com/solutions/57669[How to configure HTTP Proxy for Red{nbsp}Hat Subscription Management] (Red Hat Knowledgebase)
** link:https://access.redhat.com/solutions/1351253[How to apply a system wide proxy] (Red Hat Knowledgebase)
