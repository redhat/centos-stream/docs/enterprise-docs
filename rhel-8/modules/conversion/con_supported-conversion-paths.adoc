////
Base the file name and the ID on the module title. For example:
* file name: con-my-concept-module-a.adoc
* ID: [id="con-my-concept-module-a_{context}"]
* Title: = My concept module A
////

////
The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID so you can include it multiple times in the same guide.
////

:_mod-docs-content-type: CONCEPT
[id="con_supported-conversion-paths_{context}"]
= Supported conversion paths
////
In the title of concept modules, include nouns or noun phrases that are used in the body text. This helps readers and search engines find the information quickly. Do not start the title of concept modules with a verb. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
[IMPORTANT]
====
Red Hat recommends that you seek the support of link:https://www.redhat.com/en/services/consulting[Red Hat Consulting services] to ensure that the conversion process is smooth.
====

Currently, it is possible to convert your systems from the following Linux distributions and versions to the corresponding minor version of RHEL listed in Table 1.1.

.Supported conversion paths
[options="header"]
|====
| Source OS | Source version| Target OS and version | Product Variant | Available Conversion Methods

.3+|Alma Linux 
| 9.5
| RHEL 9.5
| N/A
| Command line, Satellite

| 8.10
| RHEL 8.10
| N/A
| Command line, Satellite

| 8.8
| RHEL 8.8 EUS
| N/A
| Command line, Satellite

.2+|CentOS Linux 
| 8.5 
| RHEL 8.5
| N/A
| Command line, Satellite

| 7.9
| RHEL 7.9
| Server
| Command line, Satellite, Red Hat Insights

.3+| Oracle Linux 
| 9.5
| RHEL 9.5
| N/A
| Command line, Satellite

| 8.10
| RHEL 8.10
| N/A
| Command line, Satellite

| 7.9
| RHEL 7.9
| Server
| Command line, Satellite

.3+|Rocky Linux 
| 9.5
| RHEL 9.5
| N/A
| Command line, Satellite

| 8.10
| RHEL 8.10
| N/A
| Command line, Satellite

| 8.8
| RHEL 8.8 EUS
| N/A
| Command line, Satellite

|====

Because the last available minor version of CentOS Linux is CentOS Linux 8.5, it is not possible to convert from CentOS Linux 8 directly to the latest available minor version of RHEL 8.  It is recommended to update your system to the latest version of RHEL after the conversion.

RHEL 7 reaches the end of the Maintenance Support Phase on June 30, 2024. If you are converting to RHEL 7 and plan to stay on RHEL 7, it is strongly recommended to purchase the Extended Life Cycle Support (ELS) add-on subscription. If you plan to convert to RHEL 7 and then immediately upgrade to RHEL 8 or later, an ELS subscription is not needed. Note that without ELS, you have limited support for RHEL 7, including for the upgrade from RHEL 7 to RHEL 8. For more information, see the link:https://access.redhat.com/support/policy/updates/errata[Red Hat Enterprise Linux Life Cycle] and the link:https://access.redhat.com/support/policy/convert2rhel-support[Convert2RHEL Support Policy].

////
[NOTE]
====
It is no longer possible to perform a conversion, supported or unsupported, to RHEL 6. To convert a CentOS Linux 6 or Oracle Linux 6 system to RHEL, upgrade your system to CentOS Linux 7 or Oracle Linux 7 and then perform a supported conversion to RHEL 7. 
====
////

In addition to the above supported conversion paths, it is also possible to perform an unsupported conversion from Scientific Linux 7, CentOS Stream 8, and CentOS Stream 9 to RHEL. For information about unsupported conversions, see link:https://access.redhat.com/articles/2360841[How to perform an unsupported conversion from a RHEL-derived Linux distribution to RHEL].

For information about Red Hat’s support policy for Linux distribution conversions, see link:https://access.redhat.com/support/policy/convert2rhel-support[Convert2RHEL Support Policy].

////
[NOTE]
====
If you are converting to RHEL 8.5, it is recommended to update your system to the latest version of RHEL after the conversion.
====
////

