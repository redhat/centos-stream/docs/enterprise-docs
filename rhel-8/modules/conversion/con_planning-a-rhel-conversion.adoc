:_mod-docs-content-type: CONCEPT

[id="con_planning-a-rhel-conversion_{context}"]
= Planning a RHEL conversion

The automated conversion process is performed on a running system. The `Convert2RHEL` utility replaces all RPM packages from the original Linux distribution by their RHEL version. At the end of the process, it is necessary to restart the system to boot the RHEL kernel.

Packages that are available only in the original distribution and do not have corresponding counterparts in RHEL repositories, and third-party packages, which originate neither from the original Linux distribution nor from RHEL, are not affected by the conversion. Red Hat does not provide support for third-party packages that are left unchanged during the conversion process. See the link:https://access.redhat.com/third-party-software-support[Red Hat policy on supporting third-party software].

[NOTE]
====
The `Convert2RHEL` utility does not directly affect local users and their data in the `/home` and `/srv` directories. However, `Convert2RHEL` cannot control actions that RPM package scriptlets perform during the conversion process.
====

You should consider the following before converting your system to RHEL:

* *Architecture* - The source OS must be installed on a system with 64-bit Intel architecture. It is not possible to convert with other system architectures.
* *Security* - Systems in FIPS mode are not supported for conversion.
* *Kernel* - Systems using kernel modules that do not exist in RHEL kernel modules are not currently supported for conversion. Red Hat recommends disabling or uninstalling foreign kernel modules before the conversion and then enabling or reinstalling those kernel modules afterwards. Unsupported kernel modules include:
** Kernel modules for specialized applications, GPUs, network drivers, or storage drivers
** Custom compiled kernel modules built by DKMS
* *Public clouds* - Conversions on public clouds are supported in the following situations:
** Alma Linux, CentOS Linux, and Rocky Linux - Using Red Hat Subscription Manager (RHSM) for the following:
*** Images on Amazon Web Services (AWS), Microsoft Azure, and Google Cloud with no associated software cost.
*** User-provided custom images on all public clouds
** Oracle Linux - Using RHSM for user-provided custom images on all public clouds.
+
`Convert2RHEL` is unable to access RHEL packages through Red Hat Update Infrastructure (RHUI) during the conversion.
* *High Availability* - Systems using high availability cluster software by Red Hat or third parties are not currently tested or supported for conversion to RHEL. Red Hat recommends migrating to newly installed RHEL systems to ensure the integrity of these environments.
* *Identity Management* - Performing an in-place conversion of a FreeIPA server is not supported. For more information about how to migrate a FreeIPA deployment to IdM, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/linux_domain_identity_authentication_and_policy_guide/index#Migrating_to_IdM_on_RHEL_7_from_FreeIPA_on_non-RHEL_Linux_distributions[Migrating to IdM on RHEL 7 from FreeIPA on non-RHEL Linux distributions], link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html/migrating_to_identity_management_on_rhel_8/ref_migrating-to-idm-on-rhel-8-from-freeipa-on-non-rhel-linux-distributions_migrating-to-idm-from-external-sources[Migrating to IdM on RHEL 8 from FreeIPA on non-RHEL Linux distributions], and link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/migrating_to_identity_management_on_rhel_9/migrating-to-idm-on-rhel-9-from-freeipa-on-non-rhel-linux-distributions_migrating-to-idm-from-external-sources[Migrating to IdM on RHEL 9 from FreeIPA on non-RHEL Linux distributions].
* *Foreman* - Conversions of systems that use Foreman with the Katello plugin are not supported. To perform a supported conversion, migrate to Red Hat Satellite first and then proceed with the conversion.
* *RAID* - It is not possible to convert UEFI-based systems with `mdadm`-managed RAID devices.

