////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

:_mod-docs-content-type: PROCEDURE
[id="proc_fixing-dependency-errors_{context}"]
= Fixing dependency errors
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
During a conversion from a different Linux distribution to RHEL, certain packages might be installed without some of their dependencies. 

.Prerequisites

* You have successfully completed the conversion to RHEL. See 
ifdef::converting-to-rhel[]
xref:proc_converting-to-a-rhel-system_converting-using-the-command-line[Converting to a RHEL system]
endif::[]
ifndef::converting-to-rhel[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/converting_from_a_linux_distribution_to_rhel_using_the_convert2rhel_utility/index#proc_converting-to-a-rhel-system_converting-using-the-command-line[Converting to a RHEL system]
endif::[]
for more information.

.Procedure

. Identify dependencies errors:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} check dependencies*
....
+
If the command displays no output, no further actions are required.

. To fix dependency errors, reinstall the affected packages. During this operation, the [systemitem]`{PackageManagerCommand}` utility automatically installs missing dependencies. If the required dependencies are not provided by repositories available on the system, install those packages manually.

