:_mod-docs-content-type: CONCEPT
[id="scriptlets-directives_{context}"]
= Scriptlets directives

[role="_abstract"]
**Scriptlets** are a series of RPM directives that are executed before or after packages are installed or deleted.

Use **Scriptlets** only for tasks that cannot be done at build time or in an start up script.

A set of common **Scriptlet** directives exists. They are similar to the `spec` file section headers, such as `%build` or `%install`. They are defined by multi-line segments of code, which are often written as a standard POSIX shell script. However, they can also be written in other programming languages that RPM for the target machine’s distribution accepts. RPM Documentation includes an exhaustive list of available languages.

The following table includes **Scriptlet** directives listed in their execution order. Note that a package containing the scripts is installed between the `%pre` and `%post` directive, and it is uninstalled between the `%preun` and `%postun` directive.

.Scriptlet directives
[cols="20%,80%"]
|====
| Directive | Definition

| ``%pretrans`` | Scriptlet that is executed just before installing or removing any package.
| ``%pre`` | Scriptlet that is executed just before installing the package on the target system.
| ``%post`` | Scriptlet that is executed just after the package was
installed on the target system.
| ``%preun`` | Scriptlet that is executed just before uninstalling the package from the target system.
| ``%postun`` | Scriptlet that is executed just after the package was
uninstalled from the target system.
| ``%posttrans`` | Scriptlet that is executed at the end of the transaction.
|====
