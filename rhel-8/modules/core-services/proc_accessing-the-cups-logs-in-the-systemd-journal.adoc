:_mod-docs-content-type: PROCEDURE

[id="accessing-the-cups-logs-in-the-systemd-journal_{context}"]
= Accessing the CUPS logs in the systemd journal
By default, CUPS stores log messages in the `systemd` journal. This includes:

* Error messages
* Access log entries
* Page log entries


.Prerequisites

* xref:installing-and-configuring-cups_configuring-printing[CUPS is installed].


.Procedure

* Display the log entries:

** To display all log entries, enter:
+
[literal,subs="+quotes"]
....
# *journalctl -u cups*
....

** To display the log entries for a specific print job, enter:
+
[literal,subs="+quotes"]
....
# *journalctl -u cups JID=_<print_job_id>_*
....

** To display log entries within a specific time frame, enter:
+
[literal,subs="+quotes"]
....
# *journalectl -u cups --since=_<YYYY-MM-DD>_ --until=_<YYYY-MM-DD>_*
....
+
Replace `YYYY` with the year, `MM` with the month, and `DD` with the day.


[role="_additional-resources"]
.Additional resources

* `journalctl(1)` man page on your system

