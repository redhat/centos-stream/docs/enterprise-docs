:_mod-docs-content-type: PROCEDURE
[id="proc_booting-to-rescue-mode_{context}"]
= Booting to rescue mode

[role="_abstract"]
You can boot to the _rescue mode_ that provides a single-user environment for troubleshooting or repair if the system cannot get to a later target, and the regular booting process fails. In rescue mode, the system attempts to mount all local file systems and start certain important system services, but it does not activate network interfaces.

.Prerequisites

* Root access


.Procedure

* To enter the rescue mode, change the current target in the current session:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl rescue*

Broadcast message from root@localhost on pts/0 (Fri 2023-03-24 18:23:15 CEST):

The system is going down to rescue mode NOW!
----
+
[NOTE]
====
This command is similar to `systemctl isolate rescue.target`, but it also sends an informative message to all users that are currently logged into the system.

To prevent `systemd` from sending a message, enter the following command with the `--no-wall` command-line option:
[literal,subs="+quotes,attributes"]
----
# *systemctl --no-wall rescue*
----
====

.Troubleshooting

If your system is not able to enter the rescue mode, you can boot to _emergency mode_, which provides the most minimal environment possible. In emergency mode, the system mounts the root file system only for reading, does not attempt to mount any other local file systems, does not activate network interfaces, and only starts a few essential services. 
//To enter the emergency mode, see: xref:troubleshooting-the-boot-process[Troubleshooting the boot process]

