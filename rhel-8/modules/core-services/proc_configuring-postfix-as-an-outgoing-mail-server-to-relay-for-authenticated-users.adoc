:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-postfix-as-an-outgoing-mail-server-to-relay-for-authenticated-users_{context}"]
= Configuring Postfix as an outgoing mail server to relay for authenticated users

[role="_abstract"]
You can configure Postfix to relay mail for authenticated users. In this scenario, you allow users to authenticate themselves and use their email address to send mail through your SMTP server by configuring Postfix as an outgoing mail server with SMTP authentication, TLS encryption, and sender address restrictions.

.Prerequisites

* You have the root access.

* You have configured a Postfix server.


.Procedure

. To configure Postfix as an outgoing mail server, edit the `/etc/postfix/main.cf` file and add the following:

.. Enable SMTP authentication: 
+
[subs="+quotes"]
----
smtpd_sasl_auth_enable = yes
broken_sasl_auth_clients = yes
----
+

.. Disable access without TLS:
+
[subs="+quotes"]
----
smtpd_tls_auth_only = yes
----
+

.. Allow mail relaying only for authenticated users:
+
[subs="+quotes"]
----
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
----
+

.. Optional: Restrict users to use their own email address only as a sender:
+
[subs="+quotes"]
----
smtpd_sender_restrictions = reject_sender_login_mismatch
----

+
. Reload the `postfix` service to apply the changes:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl reload postfix*
---- 


.Verification

* Authenticate in your SMTP client that supports TLS and SASL. Send an test email to verify that the SMTP authentication works correctly. 
