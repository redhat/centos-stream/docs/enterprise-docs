:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="configuring-the-squid-service-to-listen-on-a-specific-port-or-ip-address_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Configuring the Squid service to listen on a specific port or IP address
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

By default, the Squid proxy service listens on the `3128` port on all network interfaces. You can change the port and configuring Squid to listen on a specific IP address.


.Prerequisites

* The `squid` package is installed.


.Procedure

. Edit the `/etc/squid/squid.conf` file:
+
* To set the port on which the Squid service listens, set the port number in the `http_port` parameter. For example, to set the port to `8080`, set:
+
[literal,subs="+quotes,attributes"]
----
*http_port 8080*
----

* To configure on which IP address the Squid service listens, set the IP address and port number in the `http_port` parameter. For example, to configure that Squid listens only on the `192.0.2.1` IP address on port `3128`, set:
+
[literal,subs="+quotes,attributes"]
----
*http_port 192.0.2.1:3128*
----
+

Add multiple `http_port` parameters to the configuration file to configure that Squid listens on multiple ports and IP addresses:
+
[literal,subs="+quotes,attributes"]
----
*http_port 192.0.2.1:3128*
*http_port 192.0.2.1:8080*
----

. If you configured that Squid uses a different port as the default (`3128`): 
+
.. Open the port in the firewall:
+
[literal,subs="+quotes,attributes"]
....
# **firewall-cmd --permanent --add-port=__port_number__/tcp**
# **firewall-cmd --reload**
....

.. If you run SELinux in enforcing mode, assign the port to the `squid_port_t` port type definition:
+
[literal,subs="+quotes,attributes"]
----
# *semanage port -a -t squid_port_t -p tcp __port_number__*
----
+
If the `semanage` utility is not available on your system, install the `policycoreutils-python-utils` package.

. Restart the `squid` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl restart squid*
----
