:_mod-docs-content-type: PROCEDURE
[id="proc_disabling-a-chrony-dispatcher-script_{context}"]
= Disabling a chrony dispatcher script

[role="_abstract"]
The `chrony` dispatcher script manages the online and offline state of the NTP servers. As a system administrator, you can disable the dispatcher script to keep `chronyd` polling the servers constantly. 

The NetworkManager executes the `chrony` dispatcher script during interface reconfiguration, stop or start operations. However, if you configure certain interfaces or routes outside of NetworkManager, you can encounter the following situation:

. The dispatcher script might run when no route to the NTP servers exists, causing the NTP servers to switch to the offline state. 

. If you establish the route later, the script does not run again by default, and the NTP servers remain in the offline state.

To ensure that `chronyd` can synchronize with your NTP servers, which have separately managed interfaces, disable the dispatcher script.

.Procedure

ifeval::[{ProductNumber} == 8]
* To disable the `chrony` dispatcher script, edit the `/etc/NetworkManager/dispatcher.d/20-chrony-onoffline` file as follows: 
+
[literal,subs="+quotes"]
....
#!/bin/sh
exit 0
....
+
[NOTE]
====

When you upgrade or reinstall the `chrony` package, the packaged version of the dispatcher script replaces your modified dispatcher script.

====
endif::[]

ifeval::[{ProductNumber} == 9]
* To disable the `chrony` dispatcher script, create a symlink to `/dev/null`:
+
[literal,subs="+quotes"]
....
# *ln -f -s /dev/null /etc/NetworkManager/dispatcher.d/20-chrony-onoffline*
....
+
[NOTE]
====

After this change, the NTP servers remain in the online state at all times.

====
endif::[]
