:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_frequently-used-samba-command-line-utilities.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_using-the-smbstatus-utility.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_using-the-smbstatus-utility_{context}"]
= Using the smbstatus utility

The `smbstatus` utility reports on:

* Connections per PID of each `smbd` daemon to the Samba server. This report includes the user name, primary group, SMB protocol version, encryption, and signing information.

* Connections per Samba share. This report includes the PID of the `smbd` daemon, the IP of the connecting machine, the time stamp when the connection was established, encryption, and signing information.

* A list of locked files. The report entries include further details, such as opportunistic lock (oplock) types

.Prerequisites

* The [package]`samba` package is installed.
* The `smbd` service is running.

.Procedure

[literal,subs="+quotes,attributes"]
----
# *smbstatus*

Samba version 4.15.2
PID  Username              Group                Machine                            Protocol Version  Encryption  Signing
....-------------------------------------------------------------------------------------------------------------------------
963  _DOMAIN_\administrator  _DOMAIN_\domain users  client-pc  (ipv4:192.0.2.1:57786)  SMB3_02           -           AES-128-CMAC

Service  pid  Machine    Connected at                  Encryption  Signing:
....---------------------------------------------------------------------------
example  969  192.0.2.1  Thu Nov  1 10:00:00 2018 CEST  -           AES-128-CMAC

Locked files:
Pid  Uid    DenyMode   Access    R/W     Oplock      SharePath           Name      Time
....--------------------------------------------------------------------------------------------------------
969  10000  DENY_WRITE 0x120089  RDONLY  LEASE(RWH)  /srv/samba/example  file.txt  Thu Nov  1 10:00:00 2018
----

[role="_additional-resources"]
.Additional resources
* `smbstatus(1)` man page on your system
