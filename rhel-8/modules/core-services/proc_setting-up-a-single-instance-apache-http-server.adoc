:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="setting-up-a-single-instance-apache-http-server_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Setting up a single-instance Apache HTTP Server
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
You can set up a single-instance Apache HTTP Server to serve static HTML content.

ifdef::setting-apache-server[]
Follow the procedure if the web server should provide the same content for all domains associated with the server. If you want to provide different content for different domains, set up name-based virtual hosts. For details, see xref:configuring-apache-name-based-virtual-hosts_setting-apache-http-server[Configuring Apache name-based virtual hosts].
endif::setting-apache-server[]

ifdef::lightweight-sub-ca[]
Follow the procedure if the web server should provide the same content for all domains associated with the server. If you want to provide different content for different domains, set up name-based virtual hosts. For details, see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/deploying_different_types_of_servers/index#configuring-apache-name-based-virtual-hosts_setting-apache-http-server[Configuring Apache name-based virtual hosts].
endif::lightweight-sub-ca[]


.Procedure

. Install the [package]`httpd` package:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install httpd*
----

. If you use `firewalld`, open the TCP port `80` in the local firewall:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --permanent --add-port=80/tcp*
# *firewall-cmd --reload*
----

. Enable and start the [systemitem]`httpd` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl enable --now httpd*
----

. Optional: Add HTML files to the [filename]`/var/www/html/` directory.
+
[NOTE]
====
When adding content to [filename]`/var/www/html/`, files and directories must be readable by the user under which `httpd` runs by default. The content owner can be the either the `root` user and `root` user group, or another user or group of the administrator's choice. If the content owner is the `root` user and `root` user group, the files must be readable by other users. The SELinux context for all the files and directories must be `httpd_sys_content_t`, which is applied by default to all content within the [filename]`/var/www` directory.
====


ifdef::setting-apache-server[]
.Verification

* Connect with a web browser to [command]`http://__server_IP_or_host_name__/`.

+
If the [filename]`/var/www/html/` directory is empty or does not contain an [filename]`index.html` or [filename]`index.htm` file, Apache displays the [command]`Red Hat Enterprise Linux Test Page`. If [filename]`/var/www/html/` contains HTML files with a different name, you can load them by entering the URL to that file, such as [command]`http://__server_IP_or_host_name__/__example.html__`.
endif::setting-apache-server[]

ifdef::lightweight-sub-ca[]
.Verification

* Connect with a web browser to `\http://my_company.idm.example.com/` or [command]`http://__server_IP__/`.

+
If the [filename]`/var/www/html/` directory is empty or does not contain an [filename]`index.html` or [filename]`index.htm` file, Apache displays the [command]`Red Hat Enterprise Linux Test Page`. If [filename]`/var/www/html/` contains HTML files with a different name, you can load them by entering the URL to that file, such as [command]`http://__server_IP__/__example.html__` or `\http://my_company.idm.example.com/__example.html__`.
endif::lightweight-sub-ca[]


[role="_additional-resources"]
.Additional resources
ifdef::setting-apache-server[]
* Apache manual: xref:installing-the-apache-http-server-manual_setting-apache-http-server[Installing the Apache HTTP server manual].
endif::setting-apache-server[]

ifdef::lightweight-sub-ca[]
* Apache manual: link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/deploying_different_types_of_servers/index#installing-the-apache-http-server-manual_setting-apache-http-server[Installing the Apache HTTP Server manual].
endif::lightweight-sub-ca[]

* See the [citetitle]`httpd.service(8)` man page on your system.
