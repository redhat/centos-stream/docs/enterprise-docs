:_mod-docs-content-type: PROCEDURE

[id="displaying-the-current-umask-symbolic_{context}"]

= Displaying the current symbolic value of the umask

[role="_abstract"]
You can use the [command]`umask` utility to display the current value of the _umask_ in symbolic mode.

.Procedure
* To display the current symbolic value of the _umask_, use:
+
[literal,subs="+quotes,attributes"]
----
$ *umask -S*
----
* To display the current symbolic value of the _umask_ for a `root` user, use:
+
[literal,subs="+quotes,attributes"]
----
$ *sudo umask -S*
----
Or:
+
[literal,subs="+quotes,attributes"]
----
# *umask -S*
----
