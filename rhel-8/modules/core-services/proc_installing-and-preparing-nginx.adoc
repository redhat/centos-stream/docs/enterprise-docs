:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="installing-and-preparing-nginx_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Installing and preparing NGINX
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

Red Hat uses Application Streams to provide different versions of NGINX. You can do the following:

* Select a stream and install NGINX
* Open the required ports in the firewall
* Enable and start the [systemitem]`nginx` service

Using the default configuration, NGINX runs as a web server on port [systemitem]`80` and provides content from the [filename]`/usr/share/nginx/html/` directory.



.Prerequisites

* RHEL 8 is installed.
* The host is subscribed to the Red Hat Customer Portal.
* The [systemitem]`firewalld` service is enabled and started


.Procedure

ifeval::[{ProductNumber} == 8]
. Display the available NGINX module streams:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module list nginx*
Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)
Name        Stream        Profiles        Summary
nginx       1.14 [d]      common [d]      nginx webserver
nginx       1.16          common [d]      nginx webserver
...

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
....

. If you want to install a different stream than the default, select the stream:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} module enable nginx:__stream_version__*
....
endif::[]

. Install the [package]`nginx` package:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} install nginx*
....

. Open the ports on which NGINX should provide its service in the firewall. For example, to open the default ports for HTTP (port 80) and HTTPS (port 443) in [systemitem]`firewalld`, enter:
+
[literal,subs="+quotes,attributes"]
....
# *firewall-cmd --permanent --add-port={80/tcp,443/tcp}*
# *firewall-cmd --reload*
....

. Enable the [systemitem]`nginx` service to start automatically when the system boots:
+
[literal,subs="+quotes,attributes"]
....
# *systemctl enable nginx*
....

. Optional: Start the [systemitem]`nginx` service:
+
[literal,subs="+quotes,attributes"]
....
# *systemctl start nginx*
....
+
If you do not want to use the default configuration, skip this step, and configure NGINX accordingly before you start the service.

ifeval::[{ProductNumber} == 8]
[IMPORTANT]
====
The PHP module requires a specific NGINX version. Using an incompatible version can cause conflicts when upgrading to a newer NGNIX stream. When using PHP 7.2 stream and NGNIX 1.24 stream, you can resolve this issue by enabling a newer PHP stream 7.4 before installing NGINX.
====
endif::[]


.Verification

. Use the [systemitem]`{PackageManagerCommand}` utility to verify that the [package]`nginx` package is installed:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} list installed nginx*
Installed Packages
nginx.x86_64    1:1.14.1-9.module+el8.0.0+4108+af250afe    @rhel-8-for-x86_64-appstream-rpms
....

. Ensure that the ports on which NGINX should provide its service are opened in the firewalld:
+
[literal,subs="+quotes,attributes"]
....
# *firewall-cmd --list-ports*
80/tcp 443/tcp
....


. Verify that the [systemitem]`nginx` service is enabled:
+
[literal,subs="+quotes,attributes"]
....
# *systemctl is-enabled nginx*
enabled
....


[role="_additional-resources"]
.Additional resources
* For details about Subscription Manager, see the link:https://docs.redhat.com/en/documentation/subscription_central/1-latest/html-single/getting_started_with_rhel_system_registration/index#basic-reg-rhel-cli-sub-man[Subscription Manager].

ifeval::[{ProductNumber} == 8]
* For further details about Application Streams, modules, and installing packages, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_managing_and_removing_user-space_components/[Installing, managing, and removing user-space components] guide.
endif::[]

* For details about configuring firewalls, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/[Securing networks] guide.
