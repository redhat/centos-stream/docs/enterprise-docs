:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_understanding-and-configuring-samba-id-mapping.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/con_planning-samba-id-ranges.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.
[id="con_planning-samba-id-ranges_{context}"]
= Planning Samba ID ranges

Regardless of whether you store the Linux UIDs and GIDs in AD or if you configure Samba to generate them, each domain configuration requires a unique ID range that must not overlap with any of the other domains.

[WARNING]
====
If you set overlapping ID ranges, Samba fails to work correctly.
====

.Unique ID Ranges
====
The following shows non-overlapping ID mapping ranges for the default (`*`), `AD-DOM`, and the `TRUST-DOM` domains.

[literal,subs="+quotes,attributes"]
----
[global]
...
idmap config * : backend = tdb
idmap config * : range = 10000-999999

idmap config AD-DOM:backend = rid
idmap config AD-DOM:range = 2000000-2999999

idmap config TRUST-DOM:backend = rid
idmap config TRUST-DOM:range = 4000000-4999999
----
====

[IMPORTANT]
====
You can only assign one range per domain. Therefore, leave enough space between the domains ranges. This enables you to extend the range later if your domain grows.

If you later assign a different range to a domain, the ownership of files and directories previously created by these users and groups will be lost.
====
