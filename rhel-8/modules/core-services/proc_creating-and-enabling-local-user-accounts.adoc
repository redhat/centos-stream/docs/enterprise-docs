:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_setting-up-samba-as-a-standalone-server.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_creating-and-enabling-local-user-accounts.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_creating-and-enabling-local-user-accounts_{context}"]
= Creating and enabling local user accounts

To enable users to authenticate when they connect to a share, you must create the accounts on the Samba host both in the operating system and in the Samba database. Samba requires the operating system account to validate the Access Control Lists (ACL) on file system objects and the Samba account to authenticate connecting users.

If you use the `passdb backend = tdbsam` default setting, Samba stores user accounts in the [filename]`/var/lib/samba/private/passdb.tdb` database.

You can create a local Samba user named `example`.


.Prerequisites

* Samba is installed and configured as a standalone server.


.Procedure

. Create the operating system account:
+
[literal,subs="+quotes,attributes"]
----
# *useradd -M -s /sbin/nologin example*
----
+
This command adds the `example` account without creating a home directory. If the account is only used to authenticate to Samba, assign the [filename]`/sbin/nologin` command as shell to prevent the account from logging in locally.

. Set a password to the operating system account to enable it:
+
[literal,subs="+quotes,attributes"]
----
# *passwd example*
Enter new UNIX password: [command]`_password_`
Retype new UNIX password: [command]`_password_`
passwd: password updated successfully
----
+
Samba does not use the password set on the operating system account to authenticate. However, you need to set a password to enable the account. If an account is disabled, Samba denies access if this user connects.

. Add the user to the Samba database and set a password to the account:
+
[literal,subs="+quotes,attributes"]
----
# *smbpasswd -a _example_*
New SMB password: [command]`_password_`
Retype new SMB password: [command]`_password_`
Added user example.
----
+
Use this password to authenticate when using this account to connect to a Samba share.

. Enable the Samba account:
+
[literal,subs="+quotes,attributes"]
----
# *smbpasswd -e _example_*
Enabled user example.
----
