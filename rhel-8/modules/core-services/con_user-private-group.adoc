:_mod-docs-content-type: CONCEPT
[id="user-private-group_{context}"]
= User private groups

[role="_abstract"]
RHEL uses the _user private group_ (*UPG*) system configuration, which makes Linux groups easier to manage. A user private group is created whenever a new user is added to the system. The user private group has the same name as the user for which it was created and that user is the only member of the user private group.

UPGs simplify the collaboration on a project between multiple users. In addition, UPG system configuration makes it safe to set default permissions for a newly created file or directory, as it allows both the user, and the group this user is a part of, to make modifications to the file or directory.

////
allows users to create, modify, and delete files within a directory, without working around permissions. User private groups make it safe to set default permissions for a newly created file or directory, allowing both the user, and the group this user is a part of, to make modifications to the file or directory.

The setting which determines what permissions are applied to a newly created file or directory is called a _umask_ and is configured in the [filename]`/etc/bashrc` file. On most UNIX-based systems, the [command]`umask` is set to [command]`022`. This allows only the user who created the file or directory to make modifications to that file or directory and prevents other users from making modifications. Under the UPG scheme, this "group protection" is not necessary since every user has their own private group.

*Link to the umask chapter when it's up*
////

A list of all local groups is stored in the [filename]`/etc/group` configuration file.
