
:_mod-docs-content-type: PROCEDURE

[id="building-source-rpms_{context}"]
= Building source RPMs

[role="_abstract"]
Building a Source RPM (SRPM) has the following advantages: 

* You can preserve the exact source of a certain `Name-Version-Release` of an RPM file that was deployed to an environment. This includes the exact `spec` file, the source code, and all relevant patches. This is useful for tracking and debugging purposes.

* You can build a binary RPM on a different hardware platform or architecture.

.Prerequisites

* You have installed the `rpmbuild` utility on your system:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *{PackageManagerCommand} install rpm-build*
....

* The following `Hello World!` implementations were placed into the `~/rpmbuild/SOURCES/` directory:
** https://github.com/redhat-developer/rpm-packaging-guide/raw/master/example-code/bello-0.1.tar.gz[bello-0.1.tar.gz]
** https://github.com/redhat-developer/rpm-packaging-guide/raw/master/example-code/pello-0.1.2.tar.gz[pello-0.1.2.tar.gz]
** https://github.com/redhat-developer/rpm-packaging-guide/raw/master/example-code/cello-1.0.tar.gz[cello-1.0.tar.gz] ( https://raw.githubusercontent.com/redhat-developer/rpm-packaging-guide/master/example-code/cello-output-first-patch.patch[cello-output-first-patch.patch] )

* A `spec` file for the program that you want to package exists.


.Procedure

. Navigate to the `~/rpmbuild/SPECS/` directive, which contains the created `spec` file:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *cd ~/rpmbuild/SPECS/*
....

. Build the source RPM by entering the [command]`rpmbuild` command with the specified `spec` file:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *rpmbuild -bs _<specfile>_*
....
+
The [option]`-bs` option stands for the _build source_.
+
For example, to build source RPMs for the `bello`, `pello`, and `cello` programs, enter:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ **rpmbuild -bs bello.spec**
Wrote: /home/admiller/rpmbuild/SRPMS/bello-0.1-1.el8.src.rpm

$ **rpmbuild -bs pello.spec**
Wrote: /home/admiller/rpmbuild/SRPMS/pello-0.1.2-1.el8.src.rpm

$ **rpmbuild -bs cello.spec**
Wrote: /home/admiller/rpmbuild/SRPMS/cello-1.0-1.el8.src.rpm
....


.Verification

* Verify that the [filename]`rpmbuild/SRPMS` directory includes the resulting source RPMs. The directory is a part of the structure expected by [command]`rpmbuild`.


[role="_additional-resources"]
.Additional resources
* xref:working-with-spec-files_packaging-software[Working with spec files]
* xref:creating-spec-files-with-rpmdev-newspec_working-with-spec-files[Creating a new spec file for sample Bash, C, and Python programs]
* xref:modifying-a-spec-file_working-with-spec-files[Modifying an original spec file]
