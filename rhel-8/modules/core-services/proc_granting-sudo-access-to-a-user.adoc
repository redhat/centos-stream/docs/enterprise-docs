:_mod-docs-content-type: PROCEDURE
[id="granting-sudo-access-to-a-user_{context}"]
= Adding a sudo rule to allow members of a group to execute commands as root

[role="_abstract"]
System administrators can allow non-root users to execute administrative commands by granting them [command]`sudo` access. The [command]`sudo` command provides users with administrative access without using the password of the root user.

When users need to perform an administrative command, they can precede that command with [command]`sudo`. If the user has authorization for the command, the command is executed as if they were root.

Be aware of the following limitations:

* Only users listed in the sudoers configuration file can use the [command]`sudo` command.
* The command is executed in the shell of the user, not in the root shell. However, there are some exceptions such as when full `sudo` privileges are granted to any user. In such cases, users can switch to and run the commands in root shell. For example:

* `sudo -i` 
* `sudo su -` 
 

.Prerequisites
* You have root access to the system.

.Procedure
. As root, open the `/etc/sudoers` file.
+
[subs="+quotes"]
----
# *visudo*
----
The `/etc/sudoers` file defines the policies applied by the [command]`sudo` command.

. In the `/etc/sudoers` file, find the lines that grant `sudo` access to users in the administrative `wheel` group.
+
----
## Allows people in group wheel to run all commands
%wheel        ALL=(ALL)       ALL
----

. Make sure the line that starts with `%wheel` is not commented out with the number sign (`#`).

. Save any changes, and exit the editor.

. Add users you want to grant `sudo` access to into the administrative `wheel` group.
+
[subs="+quotes"]
----
 # *usermod --append -G wheel _<username>_*
----
+
Replace `_<username>_` with the name of the user.

.Verification
* Log in as a member of the `wheel` group and run:
+
[subs="+quotes"]
----
# *sudo whoami*
root
----

[role="_additional-resources"]
.Additional resources
* `sudo(8)`, `sudoers(5)` and `visudo(8)` man pages
