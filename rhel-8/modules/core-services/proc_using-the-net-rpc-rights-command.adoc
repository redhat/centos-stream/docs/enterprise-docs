:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_using-the-net-utility.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_using-the-net-rpc-rights-command.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_using-the-net-rpc-rights-command_{context}"]
= Using the net rpc rights command

In Windows, you can assign privileges to accounts and groups to perform special operations, such as setting ACLs on a share or upload printer drivers. On a Samba server, you can use the `net rpc rights` command to manage privileges.

[discrete]
== Listing privileges you can set

To list all available privileges and their owners, use the `net rpc rights list` command. For example:

[literal,subs="+quotes,attributes"]
----
# *net rpc rights list -U "_DOMAIN_\administrator"*
Enter _DOMAIN_\administrator's password:
     SeMachineAccountPrivilege  Add machines to domain
      SeTakeOwnershipPrivilege  Take ownership of files or other objects
             SeBackupPrivilege  Back up files and directories
            SeRestorePrivilege  Restore files and directories
     SeRemoteShutdownPrivilege  Force shutdown from a remote system
      SePrintOperatorPrivilege  Manage printers
           SeAddUsersPrivilege  Add users and groups to the domain
       SeDiskOperatorPrivilege  Manage disk shares
           SeSecurityPrivilege  System security
----

[discrete]
== Granting privileges

To grant a privilege to an account or group, use the `net rpc rights grant` command.

For example, grant the `SePrintOperatorPrivilege` privilege to the `_DOMAIN_\printadmin` group:

[literal,subs="+quotes,attributes"]
----
# *net rpc rights grant "_DOMAIN_\printadmin" SePrintOperatorPrivilege -U "_DOMAIN_\administrator"*
Enter _DOMAIN_\administrator's password:
Successfully granted rights.
----

[discrete]
== Revoking privileges

To revoke a privilege from an account or group, use the `net rpc rights revoke` command.

For example, to revoke the `SePrintOperatorPrivilege` privilege from the `_DOMAIN_\printadmin` group:
[literal,subs="+quotes,attributes"]
----
# *net rpc rights remoke "_DOMAIN_\printadmin" SePrintOperatorPrivilege -U "_DOMAIN_\administrator"*
Enter _DOMAIN_\administrator's password:
Successfully revoked rights.
----
