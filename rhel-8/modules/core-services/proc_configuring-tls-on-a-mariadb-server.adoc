:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-tls-on-a-mariadb-server_{context}"]
= Configuring TLS on a MariaDB server

[role="_abstract"]
To improve security, enable TLS support on the *MariaDB* server. As a result, clients can transmit data with the server using TLS encryption.


.Prerequisites

* You installed the *MariaDB* server.

* The [systemitem]`mariadb` service is running.

* The following files in Privacy Enhanced Mail (PEM) format exist on the server and are readable by the [systemitem]`mysql` user:
+
** The private key of the server: [filename]`/etc/pki/tls/private/server.example.com.key.pem`
** The server certificate: [filename]`/etc/pki/tls/certs/server.example.com.crt.pem`
** The Certificate Authority (CA) certificate [filename]`/etc/pki/tls/certs/ca.crt.pem`

* The subject distinguished name (DN) or the subject alternative name (SAN) field in the server certificate matches the server's hostname.


.Procedure

. Create the [filename]`/etc/my.cnf.d/mariadb-server-tls.cnf` file:

.. Add the following content to configure the paths to the private key, server and CA certificate:
+
[literal,subs="+quotes,attributes"]
----
[mariadb]
ssl_key = __/etc/pki/tls/private/server.example.com.key.pem__
ssl_cert = __/etc/pki/tls/certs/server.example.com.crt.pem__
ssl_ca = __/etc/pki/tls/certs/ca.crt.pem__
----

.. If you have a Certificate Revocation List (CRL), configure the *MariaDB* server to use it:
+
[literal,subs="+quotes,attributes"]
----
*ssl_crl = __/etc/pki/tls/certs/example.crl.pem__*
----

.. Optional: If you run *MariaDB 10.5.2* or later, you can reject connection attempts without encryption. To enable this feature, append:
+
[literal,subs="+quotes,attributes"]
----
*require_secure_transport = on*
----

.. Optional: If you run *MariaDB 10.4.6* or later, you can set the TLS versions the server should support. For example, to support TLS 1.2 and TLS 1.3, append:
+
[literal,subs="+quotes,attributes"]
----
*tls_version = __TLSv1.2__,__TLSv1.3__*
----
+
By default, the server supports TLS 1.1, TLS 1.2, and TLS 1.3.

. Restart the [systemitem]`mariadb` service:
+
[literal,subs="+quotes,attributes"]
----
# **systemctl restart mariadb.service**
----



.Verification

To simplify troubleshooting, perform the following steps on the *MariaDB* server before you configure the local client to use TLS encryption:

. Verify that *MariaDB* now has TLS encryption enabled:
+
[literal,subs="+quotes,attributes"]
....
# **mysql -u __root__ -p -e "SHOW GLOBAL VARIABLES LIKE 'have_ssl';"**
+---------------+-----------------+
| Variable_name | Value           |
+---------------+-----------------+
| have_ssl      | YES             |
+---------------+-----------------+
....
+
If the [parameter]`have_ssl` variable is set to [command]`yes`, TLS encryption is enabled.

. If you configured the *MariaDB* service to only support specific TLS versions, display the [parameter]`tls_version` variable:
+
[literal,subs="+quotes,attributes"]
....
# **mysql -u __root__ -p -e "SHOW GLOBAL VARIABLES LIKE 'tls_version';"**
+---------------+-----------------+
| Variable_name | Value           |
+---------------+-----------------+
| tls_version   | __TLSv1.2,TLSv1.3__ |
+---------------+-----------------+
....



[role="_additional-resources"]
.Additional resources
ifdef::differentserver-title[]
* xref:proc_placing-the-ca-certificate-server-certificate-and-private-key-on-the-mariadb-server_assembly_setting-up-tls-encryption-on-a-mariadb-server[Placing the CA certificate, server certificate, and private key on the MariaDB server]
* xref:upgrading-from-mariadb-10-3-to-mariadb-10-5_using-mariadb[Upgrading from MariaDB 10.3 to MariaDB 10.5]
* xref:upgrading-from-mariadb-10-5-to-mariadb-10-11_using-mariadb[Upgrading from MariaDB 10.5 to MariaDB 10.11]
endif::[]

ifndef::differentserver-title[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/using-databases#proc_placing-the-ca-certificate-server-certificate-and-private-key-on-the-mariadb-server_assembly_setting-up-tls-encryption-on-a-mariadb-server[Placing the CA certificate, server certificate, and private key on the MariaDB server]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/using-databases#upgrading-from-a-rhel-8-version-of-mariadb-10-3-to-mariadb-10-5_upgrading-from-mariadb-10-3-to-mariadb-10-5[Upgrading from MariaDB 10.3 to MariaDB 10.5]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/using-databases#upgrading-from-a-rhel-8-version-of-mariadb-10-5-to-mariadb-10-11_upgrading-from-mariadb-10-5-to-mariadb-10-11[Upgrading from MariaDB 10.5 to MariaDB 10.11]
endif::[]
