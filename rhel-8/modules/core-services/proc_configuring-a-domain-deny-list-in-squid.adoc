:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="configuring-a-domain-deny-list-in-squid_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Configuring a domain deny list in Squid
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
Frequently, administrators want to block access to specific domains. This section describes how to configure a domain deny list in Squid.

.Prerequisites

* Squid is configured, and users can use the proxy.


.Procedure

. Edit the `/etc/squid/squid.conf` file and add the following settings:
+
[literal,subs="+quotes,attributes"]
----
*acl domain_deny_list dstdomain "/etc/squid/domain_deny_list.txt"*
*http_access deny all domain_deny_list*
----
+
[IMPORTANT]
====
Add these entries before the first `http_access allow` statement that allows access to users or clients.
====

. Create the `/etc/squid/domain_deny_list.txt` file and add the domains you want to block. For example, to block access to `example.com` including subdomains and to block `example.net`, add:
+
[literal,subs="+quotes,attributes"]
----
*.example.com*
*example.net*
----
+
[IMPORTANT]
====
If you referred to the `/etc/squid/domain_deny_list.txt` file in the squid configuration, this file must not be empty. If the file is empty, Squid fails to start.
====

. Restart the `squid` service:
+
[literal,subs="+quotes,attributes"]
....
# **systemctl restart squid**
....

