:_mod-docs-content-type: CONCEPT
[id="con_spec-file-description-for-a-python-package_{context}"]
= The spec file description for a Python package

[role="_abstract"]
A `spec` file contains instructions that the `rpmbuild` utility uses to build an RPM. The instructions are included in a series of sections. A `spec` file has two main parts in which the sections are defined:

* Preamble (contains a series of metadata items that are used in the Body)
* Body (contains the main part of the instructions)

//TODO: add a conditional reference to Packaging; the assembly is now reused in this title too.
//For further information about SPEC files, see Packaging and distributing software.

An RPM SPEC file for Python projects has some specifics compared to non-Python RPM SPEC files. Most notably, a name of any RPM package of a Python library must always include the prefix determining the version, for example, `python3` for Python 3.6, `python38` for Python 3.8, `python39` for Python 3.9, `python3.11` for Python 3.11, or `python3.12` for Python 3.12.

Other specifics are shown in the following `spec` file *example for the [package]`python3-detox` package*. For description of such specifics, see the notes below the example.

// .SPEC file for the [package]`python3-detox` package
// =====================================================================
[source,specfile]
----
%global modname detox                                                <1>

Name:           python3-detox                                        <2>
Version:        0.12
Release:        4%{?dist}
Summary:        Distributing activities of the tox tool
License:        MIT
URL:            https://pypi.io/project/detox
Source0:        https://pypi.io/packages/source/d/%{modname}/%{modname}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python36-devel                                       <3>
BuildRequires:  python3-setuptools
BuildRequires:  python36-rpm-macros
BuildRequires:  python3-six
BuildRequires:  python3-tox
BuildRequires:  python3-py
BuildRequires:  python3-eventlet

%?python_enable_dependency_generator                                 <4>

%description

Detox is the distributed version of the tox python testing tool. It makes efficient use of multiple CPUs by running all possible activities in parallel.
Detox has the same options and configuration that tox has, so after installation you can run it in the same way and with the same options that you use for tox.

    $ detox

%prep
%autosetup -n %{modname}-%{version}

%build
%py3_build                                                           <5>

%install
%py3_install

%check
%{__python3} setup.py test                                           <6>

%files -n python3-%{modname}
%doc CHANGELOG
%license LICENSE
%{_bindir}/detox
%{python3_sitelib}/%{modname}/
%{python3_sitelib}/%{modname}-%{version}*

%changelog
...
----
// =====================================================================

<1> The *modname* macro contains the name of the Python project. In this example it is `detox`.
<2> When packaging a Python project into RPM, the `python3` prefix always needs to be added to the original name of the project. The original name here is `detox` and the *name of the RPM* is `python3-detox`.
<3> *BuildRequires* specifies what packages are required to build and test this package.
In BuildRequires, always include items providing tools necessary for building Python packages: `python36-devel` and `python3-setuptools`. The `python36-rpm-macros` package is required so that files with
`/usr/bin/python3` interpreter directives are automatically changed to `/usr/bin/python3.6`. 
//TODO xref both in Packaging and Basic system settings - does not work: For more information, see xref:assembly_handling-interpreter-directives-in-python-scripts_{context}[Handling interpreter directives in Python scripts].
<4> Every Python package requires some other packages to work correctly. Such packages need to be specified in the `spec` file as well. To specify the *dependencies*, you can use the *%python_enable_dependency_generator* macro to automatically use dependencies defined in the [filename]`setup.py` file.
If a package has dependencies that are not specified using Setuptools, specify them within additional `Requires` directives.
<5> The *%py3_build* and *%py3_install* macros run the [command]`setup.py build` and [command]`setup.py install` commands, respectively, with additional arguments to specify installation locations, the interpreter to use, and other details.
<6> The *check* section provides a macro that runs the correct version of Python.
The *%{__python3}* macro contains a path for the Python 3 interpreter, for example `/usr/bin/python3`. We recommend to always use the macro rather than a literal path.


