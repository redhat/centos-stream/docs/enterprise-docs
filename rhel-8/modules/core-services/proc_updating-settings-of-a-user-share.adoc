:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_enabling-users-to-share-directories-on-a-samba-server.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_updating-settings-of-a-user-share.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_updating-settings-of-a-user-share_{context}"]
= Updating settings of a user share

To update settings of a user share, override the share by using the `net usershare add` command with the same share name and the new settings.
ifeval::[{ProductNumber} == 8]
ifdef::differentserver-title[]
See xref:proc_adding-a-user-share_assembly_enabling-users-to-share-directories-on-a-samba-server[Adding a user share].
endif::[]
ifndef::differentserver-title[]
See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/assembly_using-samba-as-a-server_deploying-different-types-of-servers#proc_adding-a-user-share_assembly_enabling-users-to-share-directories-on-a-samba-server[Adding a user share].
endif::[]
endif::[]

ifeval::[{ProductNumber} == 9]
ifdef::network-file-services[]
See xref:proc_adding-a-user-share_assembly_enabling-users-to-share-directories-on-a-samba-server[Adding a user share].
endif::[]
ifndef::network-file-services[]
See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring-and-using-network-file-services/assembly_using-samba-as-a-server_configuring-and-using-network-file-services#proc_adding-a-user-share_assembly_enabling-users-to-share-directories-on-a-samba-server[Adding a user share].
endif::[]
endif::[]
