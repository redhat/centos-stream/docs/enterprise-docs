:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="setting-the-supported-tls-protocol-versions-on-an-apache-http-server_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Setting the supported TLS protocol versions on an Apache HTTP Server
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
By default, the Apache HTTP Server on RHEL uses the system-wide crypto policy that defines safe default values, which are also compatible with recent browsers. For example, the [systemitem]`DEFAULT` policy defines that only the [systemitem]`TLSv1.2` and [systemitem]`TLSv1.3` protocol versions are enabled in apache.


ifdef::setting-apache-server[]
You can manually configure which TLS protocol versions your Apache HTTP Server supports. Follow the procedure if your environment requires to enable only specific TLS protocol versions, for example:
endif::setting-apache-server[]

ifdef::lightweight-sub-ca[]
You can manually configure which TLS protocol versions your *my_company.idm.example.com* Apache HTTP Server supports. Follow the procedure if your environment requires to enable only specific TLS protocol versions, for example:
endif::lightweight-sub-ca[]


* If your environment requires that clients can also use the weak [systemitem]`TLS1` (TLSv1.0) or [systemitem]`TLS1.1` protocol.
* If you want to configure that Apache only supports the [systemitem]`TLSv1.2` or [systemitem]`TLSv1.3` protocol.



.Prerequisites

ifdef::setting-apache-server[]
* TLS encryption is enabled on the server as described in xref:proc_adding-tls-encryption-to-an-apache-http-server-configuration_configuring-tls-encryption-on-an-apache-http-server[Adding TLS encryption to an Apache HTTP server].
endif::setting-apache-server[]
ifdef::lightweight-sub-ca[]
* TLS encryption is enabled on the *my_company.idm.example.com* server as described in xref:proc_adding-tls-encryption-to-an-apache-http-server-configuration_{context}[Adding TLS encryption to an Apache HTTP server].
endif::lightweight-sub-ca[]
ifeval::[{ProductNumber} >= 9]
* If the server runs RHEL 9.2 or later and the FIPS mode is enabled, clients must either support the Extended Master Secret (EMS) extension or use TLS 1.3. TLS 1.2 connections without EMS fail. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/7018256[TLS extension "Extended Master Secret" enforced].
endif::[]

.Procedure

. Edit the [filename]`/etc/httpd/conf/httpd.conf` file, and add the following setting to the [parameter]`<VirtualHost>` directive for which you want to set the TLS protocol version. For example, to enable only the [systemitem]`TLSv1.3` protocol:
+
[literal,subs="+quotes,attributes"]
----
*SSLProtocol -All TLSv1.3*
----

. Restart the [systemitem]`httpd` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl restart httpd*
----


.Verification

. Use the following command to verify that the server supports [systemitem]`TLSv1.3`:
+
[literal,subs="+quotes,attributes"]
----
# *openssl s_client -connect __example.com__:443 -tls1_3*
----

. Use the following command to verify that the server does not support [systemitem]`TLSv1.2`:
+
[literal,subs="+quotes,attributes"]
----
# *openssl s_client -connect __example.com__:443 -tls1_2*
----
+
If the server does not support the protocol, the command returns an error:
+
[literal,subs="+quotes,attributes"]
----
140111600609088:error:1409442E:SSL routines:ssl3_read_bytes:tlsv1 alert protocol version:ssl/record/rec_layer_s3.c:1543:SSL alert number 70
----

. Optional: Repeat the command for other TLS protocol versions.


[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* [citetitle]`update-crypto-policies(8)` man page on your system
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[Using system-wide cryptographic policies].
endif::[]

ifeval::[{ProductNumber} == 9]
* [citetitle]`update-crypto-policies(8)` man page on your system
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[Using system-wide cryptographic policies].
endif::[]

ifdef::setting-apache-server[]
* For further details about the [parameter]`SSLProtocol` parameter, refer to the [systemitem]`mod_ssl` documentation in the Apache manual: xref:installing-the-apache-http-server-manual_setting-apache-http-server[Installing the Apache HTTP server manual].
endif::setting-apache-server[]
ifdef::lightweight-sub-ca[]
* For further details about the [parameter]`SSLProtocol` parameter, refer to the [systemitem]`mod_ssl` documentation in the Apache manual: link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/deploying_different_types_of_servers/index#installing-the-apache-http-server-manual_setting-apache-http-server[Installing the Apache HTTP Server manual].
endif::lightweight-sub-ca[]
