:_mod-docs-content-type: PROCEDURE

[id="enabling-server-side-email-filtering-using-sieve-on-a-dovecot-imap-server_{context}"]
= Enabling server-side email filtering using Sieve on a Dovecot IMAP server
You can upload Sieve scripts to a server using the ManageSieve protocol. Sieve scripts define rules and actions that a server should validate and perform on incoming emails. For example, users can use Sieve to forward emails from a specific sender, and administrators can create a global filter to move mails flagged by a spam filter into a separate IMAP folder.

The `ManageSieve` plugin adds support for Sieve scripts and the ManageSieve protocol to a Dovecot IMAP server.

[WARNING]
====
Use only clients that support using the ManageSieve protocol over TLS connections. Disabling TLS for this protocol causes clients to send credentials in plain text over the network.
====


.Prerequisites

* Dovecot is configured and provides IMAP mailboxes.
* TLS encryption is configured in Dovecot.
* The mail clients support the ManageSieve protocol over TLS connections.


.Procedure

. Install the `dovecot-pigeonhole` package:
+
[literal,subs="+quotes,attributes"]
....
# **{PackageManagerCommand} install dovecot-pigeonhole**
....

. Uncomment the following line in `/etc/dovecot/conf.d/20-managesieve.conf` to enable the `sieve` protocol:
+
[literal,subs="+quotes"]
....
**protocols = $protocols sieve**
....
+
This setting activates Sieve in addition to the other protocols that are already enabled.

. Open the ManageSieve port in `firewalld`:
+
[literal,subs="+quotes"]
....
# **firewall-cmd --permanent --add-service=managesieve**
# **firewall-cmd --reload**
....

. Reload Dovecot:
+
[literal,subs="quotes"]
....
# **systemctl reload dovecot**
....


.Verification

. Use a client and upload a Sieve script. Use the following connection settings:
+
* Port: 4190
* Connection security: SSL/TLS
* Authentication method: PLAIN

. Send an email to the user who has the Sieve script uploaded. If the email matches the rules in the script, verify that the server performs the defined actions.


[role="_additional-resources"]
.Additional resources
* `/usr/share/doc/dovecot/wiki/Pigeonhole.Sieve.Plugins.IMAPSieve.txt`
* `/usr/share/doc/dovecot/wiki/Pigeonhole.Sieve.Troubleshooting.txt`
* `firewall-cmd(1)` man page on your system

