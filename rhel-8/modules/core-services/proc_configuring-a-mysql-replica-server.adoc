:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-a-mysql-replica-server_{context}"]
= Configuring a MySQL replica server

[role="_abstract"]
You can set configuration options required for a *MySQL* replica server to ensure a successful replication.

.Prerequisites

* The replica server is installed.

* The replica server has xref:assembly_setting-up-tls-encryption-on-a-mysql-server_assembly_using-mysql[TLS set up].
+
IMPORTANT: The source and replica certificates must be signed by the same certificate authority.


.Procedure

. Include the following options in the `/etc/my.cnf.d/mysql-server.cnf` file under the `[mysqld]` section:
+
** `server-id=_id_`
+
The _id_ must be unique.
+
** `relay-log=_path_to_replica_server_log_`
+
The relay log is a set of log files created by the *MySQL* replica server during replication.
+
** `log_bin=_path_to_replica_sever_log_`
+
This option defines a path to the binary log file of the *MySQL* replica server. For example: `log_bin=/var/log/mysql/mysql-bin.log`.
+
This option is not required in a replica but strongly recommended.
+
** `gtid_mode=ON`
+
This option enables global transaction identifiers (GTIDs) on the server.
+
** `enforce-gtid-consistency=ON`
+
The server enforces GTID consistency by allowing execution of only statements that can be safely logged using a GTID.
+
** `log-replica-updates=ON`
+
This option ensures that updates received from the source server are logged in the replica's binary log.
+
** `skip-replica-start=ON`
+
This option ensures that the replica server does not start the replication threads when the replica server starts.
+
** _Optional:_ `binlog_do_db=_db_name_`
+
Use this option if you want to replicate only certain databases. To replicate more than one database, specify each of the databases separately:
+
[subs="+quotes"]
----
binlog_do_db=_db_name1_
binlog_do_db=_db_name2_
binlog_do_db=_db_name3_
----
+
** _Optional:_ `binlog_ignore_db=_db_name_`
+
Use this option to exclude a specific database from replication.

. Restart the `mysqld` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl restart mysqld.service*
----
