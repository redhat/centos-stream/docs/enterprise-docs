:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

:_module-type: PROCEDURE

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="disabling-yum-plug-ins_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Disabling YUM plug-ins

[subs="+quotes,attributes"]
* To disable all {PackageManagerCommand} plug-ins:
. Ensure a line beginning with `plugins=` is present in the `[main]` section of the `/etc/yum.conf` file.
. Set the value of `plugins=` to `0`.
+
----
plugins=0
----
+
[subs="+quotes,attributes"]
IMPORTANT: Disabling all plug-ins is *not* advised. Certain plug-ins provide important {PackageManagerCommand} services. In particular, the [application]*product-id* and [application]*subscription-manager* plug-ins provide support for the certificate-based `Content Delivery Network` (*CDN*). Disabling plug-ins globally is provided as a convenience option, and is advisable only when diagnosing a potential problem with [application]*{PackageManagerCommand}*.

[subs="+quotes,attributes"]
* To disable all {PackageManagerCommand} plug-ins for a particular command, append `--noplugins` option to the command.
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} --noplugins update*
----

* To disable certain {PackageManagerCommand} plug-ins for a single command, append `--disableplugin=_plugin-name_` option to the command.
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} update --disableplugin=_plugin-name_*
----
Replace _plugin-name_ with the name of the plug-in.
