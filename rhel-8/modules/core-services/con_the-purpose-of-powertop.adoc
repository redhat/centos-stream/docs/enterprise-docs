:_mod-docs-content-type: CONCEPT
[id="the-purpose-of-powertop_{context}"]
= The purpose of PowerTOP

[application]*PowerTOP* is a program that diagnoses issues related to power consumption and provides suggestions on how to extend battery lifetime.

The [application]*PowerTOP* tool can provide an estimate of the total power usage of the system and also individual power usage for each process, device, kernel worker, timer, and interrupt handler. The tool can also identify specific components of kernel and user-space applications that frequently wake up the CPU.

ifeval::[{ProductNumber} == 8]
{RHEL} 8 uses version 2.x of [application]*PowerTOP*.
endif::[]

ifeval::[{ProductNumber} == 9]
{RHEL} 9 uses version 2.x of [application]*PowerTOP*.
endif::[]
