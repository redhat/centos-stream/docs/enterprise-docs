:_mod-docs-content-type: PROCEDURE
[id="listing-already-installed-language-support_{context}"]
= Listing already installed language support

To list the already installed language support, use this procedure.

.Procedure

* Execute the following command:
+
[literal,subs="+quotes,verbatim,normal"]
....
# **{PackageManagerCommand} list installed langpacks***
....



