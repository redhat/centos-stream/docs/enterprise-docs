:_mod-docs-content-type: PROCEDURE

[id="adding-a-printer-to-cups-by-using-the-lpadmin-utility_{context}"]
= Adding a printer to CUPS by using the lpadmin utility

Before users can print through CUPS, you must add printers. You can use both network printers and printers that are directly attached to the CUPS host, for example over USB.

You can add printers by using the CUPS driverless feature or by using a PostScript Printer Description (PPD) file.

[NOTE]
====
CUPS prefers driverless printing, and using drivers is deprecated.
====

{ProductName} ({ProductShortName}) does not provide the name service switch multicast DNS plug-in (`nss-mdns`), which resolves requests by querying an mDNS responder. Consequently, automatic discovery and installation for local driverless printers by using mDNS is not available in {ProductShortName}. To work around this problem, install single printers manually or use `cups-browsed` to automatically install a high amount of print queues that are available on a remote print server.


.Prerequisites
* xref:installing-and-configuring-cups_configuring-printing[CUPS is configured].
* xref:determining-whether-a-printer-supports-driverless-printing_configuring-printing[The printer supports driverless printing], if you want to use this feature.
* The printer accepts data on port 631 (IPP), 9100 (socket), or 515 (LPD). The port depends on the method you use to connect to the printer.


.Procedure

* Add the printer to CUPS:

** To add a printer with driverless support, enter:
+
[literal,subs="+quotes"]
....
# *lpadmin -p _Demo-printer_ -E -v _ipp://192.0.2.200/ipp/print_ -m everywhere*
....
+
If the `-m everywhere` option does not work for your printer, try `-m driverless:__<uri>__`, for example: `-m driverless:__ipp://192.0.2.200/ipp/print__`.

** To add a queue from a remote print server with driverless support, enter:
+
[literal,subs="+quotes"]
....
# *lpadmin -p _Demo-printer_ -E -v _ipp://192.0.2.201/printers/example-queue_ -m everywhere*
....
+
If the `-m everywhere` option does not work for your printer, try `-m driverless:__<uri>__`, for example: `-m driverless:__ipp://192.0.2.200/printers/example-queue__`.

** To add a printer with a driver in file, enter:
+
[literal,subs="+quotes"]
....
# *lpadmin -p _Demo-printer_ -E -v _socket://192.0.2.200/_ -P _/root/example.ppd_*
....

** To add a queue from a remote print server with a driver in a file, enter:
+
[literal,subs="+quotes"]
....
# *lpadmin -p _Demo-printer_ -E -v _ipp://192.0.2.201/printers/example-queue_ -P _/root/example.ppd_*
....

** To add a printer with a driver in the local driver database:

... List the drivers in the database:
+
[literal,subs="+quotes"]
....
# *lpinfo -m*
...
drv:///sample.drv/generpcl.ppd Generic PCL Laser Printer
...
....

... Add the printer with the URI to the driver in the database:
+
[literal,subs="+quotes"]
....
# *lpadmin -p _Demo-printer_ -E -v _socket://192.0.2.200/_ -m _drv:///sample.drv/generpcl.ppd_*
....

+
These commands uses the following options:

** `-p __<printer_name>__`: Sets the name of the printer in CUPS.
** `-E`: Enables the printer and CUPS accepts jobs for it. Note that you must specify this option after `-p`. See the option's description in the man page for further details.
** `-v _<uri>_`: Sets the URI to the printer or remote print server queue.
** `-m _<driver_uri>_`: Sets the PPD file based on the provided driver URI obtained from the local driver database.
** `-P _<PPD_file>_`: Sets the path to the PPD file.


.Verification

. Display the available printers:
+
[literal,subs="+quotes"]
....
# *lpstat -p*
_printer Demo-printer is idle.  enabled since Fri 23 Jun 2023 09:36:40 AM CEST_
....

. Print a test page:
+
[literal,subs="+quotes"]
----
# *lp -d _Demo-printer_ _/usr/share/cups/data/default-testpage.pdf_*
----

