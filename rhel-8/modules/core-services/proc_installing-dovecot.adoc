:_mod-docs-content-type: PROCEDURE

[id="installing-dovecot_{context}"]
= Installing Dovecot

The `dovecot` package provides:

* The `dovecot` service and the utilities to maintain it
* Services that Dovecot starts on demand, such as for authentication
* Plugins, such as server-side mail filtering
* Configuration files in the `/etc/dovecot/` directory
* Documentation in the `/usr/share/doc/dovecot/` directory


.Procedure

* Install the `dovecot` package:
+
[literal,subs="+quotes,attributes"]
....
# **{PackageManagerCommand} install dovecot**
....
+
[NOTE]
====
If Dovecot is already installed and you require clean configuration files, rename or remove the `/etc/dovecot/` directory. Afterwards, reinstall the package. Without removing the configuration files, the `{PackageManagerCommand} reinstall dovecot` command does not reset the configuration files in `/etc/dovecot/`.
====


.Next step
// The xref contains {context} in the ID, because this module is used in multiple assemblies
* xref:configuring-tls-encryption-on-a-dovecot-server_{context}[Configuring TLS encryption on a Dovecot server].

