:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="example-ansible-playbook-to-resize-an-existing-lvm-file-system-using-the-storage-rhel-system-role_{context}"]
= Resizing an existing file system on LVM by using the `storage` {RHELSystemRoles}
You can use the `storage` {RHELSystemRoles} to resize an LVM logical volume with a file system.

If the logical volume you are reducing has a file system, to prevent data loss you must ensure that the file system is not using the space in the logical volume that is being reduced.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Manage local storage
  hosts: managed-node-01.example.com
  tasks:
    - name: Resize LVM logical volume with file system
      ansible.builtin.include_role:
        name: rhel-system-roles.storage
      vars:
        storage_pools:
          - name: myvg
            disks:
              - /dev/sda
              - /dev/sdb
              - /dev/sdc
            volumes:
              - name: mylv1
                size: 10 GiB
                fs_type: ext4
                mount_point: /opt/mount1
              - name: mylv2
                size: 50 GiB
                fs_type: ext4
                mount_point: /opt/mount2
....
+
This playbook resizes the following existing file systems:
+
* The Ext4 file system on the `mylv1` volume, which is mounted at `/opt/mount1`, resizes to 10 GiB.
* The Ext4 file system on the `mylv2` volume, which is mounted at `/opt/mount2`, resizes to 50 GiB.

+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....

.Verification
. Verify that the logical volume has been resized to the requested size:
+
[subs="+quotes"]
....
# *ansible managed-node-01.example.com -m command -a 'lvs myvg'*
....

. Verify file system size using file system tools. For example, for ext4, calculate the file system size by multiplying block count and block size reported by dumpe2fs tool:
+
[subs="+quotes"]
....
# *ansible managed-node-01.example.com -m command -a 'dumpe2fs -h /dev/myvg/mylv | grep -E "Block count|Block size"'*
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file
* `/usr/share/doc/rhel-system-roles/storage/` directory

