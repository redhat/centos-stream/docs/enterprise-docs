
:_mod-docs-content-type: PROCEDURE

[id="patching-a-program-written-in-c_{context}"]
= Patching a sample C program

To apply code patches on your software, you can use the `patch` utility.

.Prerequisites

* You installed the `patch` utility on your system:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *{PackageManagerCommand} install patch*
....

* You created a patch from the original source code. For instructions, see xref:creating-a-patch-for-a-program-written-in-c_patching-software[Creating a patch file for a sample C program].

.Procedure

The following steps apply a previously created `cello.patch` file on the `cello.c` file.

. Redirect the patch file to the [command]`patch` command:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ **patch < cello.patch**
patching file cello.c
....

. Check that the contents of `cello.c` now reflect the desired change:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ **cat cello.c**
#include<stdio.h>

int main(void){
    printf("Hello World from my very first patch!\n");
    return 1;
}
....

.Verification

. Build the patched `cello.c` program:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ **make**
gcc -g -o cello cello.c
....

. Run the built `cello.c` program:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ **./cello**
Hello World from my very first patch!
....
