:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_notable-changes-to-particular-components.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.

[id="notable-changes-to-particular-components_{context}"]
= Other changes to infrastructure services components

The summary of other notable changes to particular infrastructure services components follows.

.Notable changes to infrastructure services components
[options="header", cols="1,1,3"]
|====
|Name|Type of change|Additional information
|acpid|Option change|`-d` (debug) no longer implies `-f` (foreground)
|bind|Configuration option removal|`dnssec-lookaside auto` removed; use `no` instead
|brltty|Configuration option change|`--message-delay brltty` renamed to `--message-timeout`
|brltty|Configuration option removal|`-U [--update-interval=]` removed
|brltty|Configuration option change|A Bluetooth device address may now contain dashes (-) instead of colons (:). The `bth:` and `bluez:` device qualifier aliases are no longer supported.
|cups|Functionality removal|Upstream removed support of interface scripts because of security reasons. Use ppds and drivers provided by OS or proprietary ones.
|cups|Directive options removal|Removed `Digest` and `BasicDigest` authentication types for AuthType and DefaultAuthType directives in `/etc/cups/cupsd.conf`. Migrate to `Basic`.
|cups|Directive options removal|Removed `Include` from `cupsd.conf`
|cups|Directive options removal|Removed `ServerCertificate` and `ServerKey` from `cups-files.conf` use `Serverkeychain` instead
|cups|Directives moved between conf files|`SetEnv` and `PassEnv` moved from `cupsd.conf` to `cups-files.conf`
|cups|Directives moved between conf files|`PrintcapFormat` moved from `cupsd.conf` to `cups-files.conf`
|cups-filters|Default configuration change|Names of remote print queues discovered by cups-browsed are now created based on device ID of printer, not on the name of remote print queue.
|cups-filters|Default configuration change|`CreateIPPPrinterQueues` must be set to `All` for automatic creation of queues of IPP printers
|cyrus-imapd|Data format change|Cyrus-imapd 3.0.7 has different data format.
|dhcp|Behavior change|`dhclient` sends the hardware address as a client identifier by default. The `client-id` option is configurable. For more information, see the `/etc/dhcp/dhclient.conf` file.
|dhcp|Options incompatibility|The `-I` option is now used for standard-ddns-updates. For the previous functionality (dhcp-client-identifier), use the new `-C` option.
|dosfstools|Behavior change|Data structures are now automatically aligned to cluster size. To disable the alignment, use the `-a` option. `fsck.fat` now defaults to interactive repair mode which previously
had to be selected with the `-r` option.
|finger|Functionality removal|
|GeoIP|Functionality removal|
|grep|Behavior change|`grep` now treats files containining data improperly encoded for the current locale as binary.
|grep|Behavior change|`grep -P` no longer reports an error and exits when given invalid UTF-8 data
|grep|Behavior change|`grep` now warns if the GREP_OPTIONS environment variable is now used. Use an alias or script instead.
|grep|Behavior change|`grep -P` eports an error and exits in locales with multibyte character encodings other than UTF-8
|grep|Behavior change|When searching binary data, `grep` may treat non-text bytes as line terminators, which impacts performance significantly.
|grep|Behavior change|`grep -z` no longer automatically treats the byte '\200' as binary data.
|grep|Behavior change|Context no longer excludes selected lines omitted because of `-m`.
|irssi|Behavior change|`SSLv2` and `SSLv3` no longer supported
|lftp|Change of options|`xfer:log` and `xfer:log-file` deprecated; now available under `log:enabled` and `log:file` commands
|ntp|Functionality removal|ntp has been removed; use chrony instead
|postfix|Configuration change|3.x version have compatibility safety net that runs Postfix programs with backwards-compatible default settings after an upgrade.
|postfix|Configuration change|In the Postfix MySQL database client, the default option_group value has changed to `client`, set it to empty value for backward compatible behavior.
|postfix|Configuration change|The postqueue command no longer forces all message arrival times to be reported in UTC.
To get the old behavior, set `TZ=UTC` in `main.cf:import_environment`. For example,

`import_environment = MAIL_CONFIG MAIL_DEBUG MAIL_LOGTAG TZ=UTC XAUTHORITY DISPLAY LANG=C.`
|postfix|Configuration change|ECDHE - `smtpd_tls_eecdh_grade` defaults to `auto`; new parameter `tls_eecdh_auto_curves` with the names of curves that may be negotiated
|postfix|Configuration change|Changed defaults for `append_dot_mydomain` (new: no, old: yes), `master.cf chroot` (new: n, old: y), `smtputf8` (new: yes, old: no).
|postfix|Configuration change|Changed defaults for `relay_domains` (new: empty, old: $mydestination).
|postfix|Configuration change|The `mynetworks_style` default value has changed from `subnet` to `host`.
|powertop|Option removal|`-d` removed
|powertop|Option change|`-h` is no longer alias for `--html`. It is now an alias for `--help`.
|powertop|Option removal|`-u` removed
|quagga|Functionality removal|
|sendmail|Configuration change| sendmail uses uncompressed IPv6 addresses by default, which permits a zero subnet to have a more specific match. Configuration data must use the same format, so make sure patterns such as `IPv6:[0-9a-fA-F:]*::` and `IPv6::` are updated before using 8.15.
|spamassasin|Command line option removal|Removed `--ssl-version` in spamd.
|spamassasin|Command line option change|In spamc, the command line option `-S/--ssl` can no longer be used to specify SSL/TLS version. The option can now only be used without an argument to enable TLS.
|spamassasin|Change in supported SSL versions|In spamc and spamd, SSLv3 is no longer supported.
|spamassasin|Functionality removal|`sa-update` no longer supports SHA1 validation of filtering rules, and uses SHA256/SHA512 validation instead.
|vim|Default settings change|Vim runs default.vim script, if no ~/.vimrc file is available.
|vim|Default settings change|Vim now supports bracketed paste from terminal. Include 'set t_BE=' in vimrc for the previous behavior.
|vsftpd|Default configuration change|`anonymous_enable` disabled
|vsftpd|Default configuration change|`strict_ssl_read_eof` now defaults to YES
|vsftpd|Functionality removal|`tcp_wrappers` no longer supported
|vsftpd|Default configuration change|TLSv1 and TLSv1.1 are disabled by default
|wireshark|Python bindings removal|Dissectors can no longer be written in Python, use C instead.
|wireshark|Option removal|`-C` suboption for `-N` option for asynchronous DNS name resolution removed
|wireshark|Ouput change|With the `-H` option, the output no longer shows SHA1, RIPEMD160 and MD5 hashes. It now shows SHA256, RIPEMD160 and SHA1 hashes.
|wvdial|Functionality removal|
|====


////
[options="header", cols="2,1,1,2,4"]
|====
|Name|Latest RHEL 7|RHEL 8|Type of change|Additional information
|bind|9.9.4|9.11.4|Configuration option removal|`dnssec-lookaside auto` removed; use `no` instead
|brltty|4.5|5.6|Configuration option change|`--message-delay brltty` renamed to `--message-timeout`
|brltty|4.5|5.6|Configuration option removal|`-U [--update-interval=]` removed
|brltty|4.5|5.6|Configuration option change|A Bluetooth device address may now contain dashes (-) instead of colons (:). The `bth:` and `bluez:` device qualifier aliases are no longer supported.
|cups|1.6.3|2.2.6|Functionality removal|Upstream removed support of interface scripts because of security reasons. Use ppds and drivers provided by OS or proprietary ones.
|cups|1.6.3|2.2.6|Directive options removal|Removed `Digest` and `BasicDigest` authentication types for AuthType and DefaultAuthType directives in `/etc/cups/cupsd.conf`. Migrate to `Basic`.
|cups|1.6.3|2.2.6|Directive options removal|Removed `Include` from `cupsd.conf`
|cups|1.6.3|2.2.6|Directive options removal|Removed `ServerCertificate` and `ServerKey` from `cups-files.conf` use `Serverkeychain` instead
|cups|1.6.3|2.2.6|Directives moved between conf files|`SetEnv` and `PassEnv` moved from `cupsd.conf` to `cups-files.conf`
|cups|1.6.3|2.2.6|Directives moved between conf files|`PrintcapFormat` moved from `cupsd.conf` to `cups-files.conf`
|cups-filters|1.0.35|1.20.0|Default configuration change|Names of remote print queues discovered by cups-browsed are now created based on device ID of printer, not on the name of remote print queue.
|cups-filters|1.0.35|1.20.0|Default configuration change|`CreateIPPPrinterQueues` must be set to `All` for automatic creation of queues of IPP printers
|cyrus-imapd|2.4.17|3.0.7|Data format change|Cyrus-imapd 3.0.7 has different data format.
|dhcp|4.2.5|4.3.6|Options incompatibility|The `-I` option is now used for standard-ddns-updates. For the previous functionality (dhcp-client-identifier), use the new `-C` option.
|finger|0.17||Functionality removal|
|GeoIP|1.5.0||Functionality removal|
|grep|2.20|3.1|Behavior change|`grep` now treats files containining data improperly encoded for the current locale as binary.
|grep|2.20|3.1|Behavior change|`grep -P` no longer reports an error and exits when given invalid UTF-8 data
|grep|2.20|3.1|Behavior change|`grep` now warns if the GREP_OPTIONS environment variable is now used. Use an alias or script instead.
|grep|2.20|3.1|Behavior change|`grep -P` eports an error and exits in locales with multibyte character encodings other than UTF-8
|grep|2.20|3.1|Behavior change|When searching binary data, `grep` may treat non-text bytes as line terminators, which impacts performance significantly.
|grep|2.20|3.1|Behavior change|`grep -z` no longer automatically treats the byte '\200' as binary data.
|grep|2.20|3.1|Behavior change|Context no longer excludes selected lines omitted because of `-m`.
|irssi|0.8.15|1.1.1|Behavior change|`SSLv2` and `SSLv3` no longer supported
|lftp|4.4.8|4.8.4.|Change of options|`xfer:log` and `xfer:log-file`deprecated; now available under `log:enabled` and `log:file` commands
|ntp|4.2.6p5||Functionality removal|ntp has been removed; use chrony instead
|postfix|2.10.1|3.3.1|Configuration change|3.x version have compatibility safety net that runs Postfix programs with backwards-compatible default settings after an upgrade.
|postfix|2.10.1|3.3.1|Configuration change|In the Postfix MySQL database client, the default option_group value has changed to `client`, set it to empty value for backward compatible behavior.
|postfix|2.10.1|3.3.1|Configuration change|The `postqueue` command no longer forces all message arrival times to be reported in UTC. To get the old behavior, set `TZ=UTC` in main.cf.
|postfix|2.10.1|3.3.1|Configuration change|ECDHE - `smtpd_tls_eecdh_grade` defaults to `auto`; new parameter `tls_eecdh_auto_curves` with the names of curves that may be negotiated
|postfix|2.10.1|3.3.1|Configuration change|Changed defaults for `append_dot_mydomain` (new: no, old: yes), `master.cf chroot` (new: n, old: y), `smtputf8` (new: yes, old: no).
|postfix|2.10.1|3.3.1|Configuration change|Changed defaults for `relay_domains` (new: empty, old: $mydestination).
|postfix|2.10.1|3.3.1|Configuration change|The `mynetworks_style` default value has changed from `subnet` to `host`.
|powertop|2.9|2.9|Option removal|`-d` removed
|quagga|0.99.22.4||Functionality removal|
|sendmail|8.14.7|8.15.2|Configuration change| sendmail uses uncompressed IPv6 addresses by default, which permits a zero subnet to have a more specific match. Configuration data must use the same format, so make sure patterns such as `IPv6:[0-9a-fA-F:]*::` and `IPv6::` are updated before using 8.15.
|spamassasin|3.4.0|3.4.2|Command line option removal|Removed `--ssl-version` in spamd
|spamassasin|3.4.0|3.4.2|Change in supported SSL versions|In spamc and spamd, SSLv3 is no longer supported.
|spamassasin|3.4.0|3.4.2|Functionality removal|`sa-update` no longer supports SHA1 validation of filtering rules, and uses SHA256/SHA512 validation instead.
|vim|7.4.160|8.0.1763|Default settings change|Vim runs default.vim script, if no ~/.vimrc file is available.
|vim|7.4.160|8.0.1763|Default settings change|Vim now supports bracketed paste from terminal. Include 'set t_BE=' in vimrc for the previous behavior.
|vsftpd|3.0.2|3.0.3|Default configuration change|`anonymous_enable` disabled
|vsftpd|3.0.2|3.0.3|Default configuration change|`strict_ssl_read_eof` now defaults to YES
|vsftpd|3.0.2|3.0.3|Functionality removal|`tcp_wrappers` no longer supported
|vsftpd|3.0.2|3.0.3|Default configuration change|TLSv1 and TLSv1.1 are disabled by default
|wireshark|1.10.14|2.6.2|Python bindings removal|dissector can no longer be written tin Python, use C instead.
|wireshark|1.10.14|2.6.2||`-C` suboption for `-N` option for asynchronous DNS name resolution removed
|wireshark|1.10.14|2.6.2|Ouput change|With the `-H` option, the output no longer shows SHA1, RIPEMD160 and MD5 hashes. It now shows SHA256, RIPEMD160 and SHA1 hashes.
|wvdial|1.61||Functionality removal|
|====
////
