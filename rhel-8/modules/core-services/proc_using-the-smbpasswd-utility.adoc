:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_frequently-used-samba-command-line-utilities.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_using-the-smbpasswd-utility.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_using-the-smbpasswd-utility_{context}"]
= Using the smbpasswd utility

The `smbpasswd` utility manages user accounts and passwords in the local Samba database.

.Prerequisites

* The [package]`samba-common-tools` package is installed.

.Procedure

. If you run the command as a user, `smbpasswd` changes the Samba password of the user who run the command. For example:
+
[literal,subs="+quotes,attributes"]
----
[user@server ~]$ *smbpasswd*
New SMB password: _password_
Retype new SMB password: _password_
----

. If you run `smbpasswd` as the `root` user, you can use the utility, for example, to:
+
* Create a new user:
+
[literal,subs="+quotes,attributes"]
----
[root@server ~]# *smbpasswd -a _user_name_*
New SMB password: *`_password_`*
Retype new SMB password: *`_password_`*
Added user _user_name_.
----
+
[NOTE]
====
Before you can add a user to the Samba database, you must create the account in the local operating system. See the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/managing-users-and-groups_configuring-basic-system-settings#adding-a-user-from-the-command-line_managing-users-from-the-command-line[Adding a new user from the command line] section in the Configuring basic system settings guide.
====

* Enable a Samba user:
+
[literal,subs="+quotes,attributes"]
----
[root@server ~]# *smbpasswd -e _user_name_*
Enabled user _user_name_.
----

* Disable a Samba user:
+
[literal,subs="+quotes,attributes"]
----
[root@server ~]# *smbpasswd -x _user_name_*
Disabled user _user_name_
----

* Delete a user:
+
[literal,subs="+quotes,attributes"]
----
[root@server ~]# *smbpasswd -x _user_name_*
Deleted user _user_name_.
----

[role="_additional-resources"]
.Additional resources
* `smbpasswd(8)` man page on your system
