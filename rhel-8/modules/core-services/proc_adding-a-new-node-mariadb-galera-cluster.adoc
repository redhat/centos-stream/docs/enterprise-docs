:_mod-docs-content-type: PROCEDURE
[id="adding-a-new-node-to-mariadb-galera-cluster_{context}"]
= Adding a new node to MariaDB Galera Cluster

[role="_abstract"]
To add a new node to *MariaDB Galera Cluster*, use the following procedure.

Note that you can also use this procedure to reconnect an already existing node.

.Procedure

* On the particular node, provide an address to one or more existing cluster members in the [option]`wsrep_cluster_address` option within the `[mariadb]` section of the [filename]`/etc/my.cnf.d/galera.cnf` configuration file :
+
[literal,subs="+quotes,verbatim,normal"]
----
[mariadb]
wsrep_cluster_address="_gcomm://192.168.0.1_"
----
+
When a new node connects to one of the existing cluster nodes, it is able to see all nodes in the cluster.
+
However, preferably list all nodes of the cluster in [option]`wsrep_cluster_address`.
+
As a result, any node can join a cluster by connecting to any other cluster node, even if one or more cluster nodes are down. When all members agree on the membership, the cluster's state is changed. If the new node's state is different from the state of the cluster, the new node requests either an Incremental State Transfer (IST) or a State Snapshot Transfer (SST) to ensure consistency with the other nodes.

[role="_additional-resources"]
.Additional resources
* link:https://mariadb.com/kb/en/library/getting-started-with-mariadb-galera-cluster/[Getting started with MariaDB Galera Cluster]
* link:https://mariadb.com/kb/en/library/documentation/replication/galera-cluster/state-snapshot-transfers-ssts-in-galera-cluster/introduction-to-state-snapshot-transfers-ssts/[Introduction to State Snapshot Transfers]
* link:https://mariadb.com/kb/en/securing-communications-in-galera-cluster/[Securing Communications in Galera Cluster]
