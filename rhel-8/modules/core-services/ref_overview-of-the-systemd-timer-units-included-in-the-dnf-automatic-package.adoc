:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// assembly_automatically-refreshing-package-database.adoc

:_module-type: REFERENCE

[id="overview-of-the-systemd-timer-units-included-in-the-dnf-automatic-package_{context}"]

= Overview of the systemd timer units included in the dnf-automatic package

[role="_abstract"]
The systemd timer units take precedence and override the settings in the `/etc/dnf/automatic.conf` configuration file concerning downloading and applying updates.

For example, if you set the `download_updates = yes` option in the `/etc/dnf/automatic.conf` configuration file, but you have activated the `dnf-automatic-notifyonly.timer` unit, the packages will not be downloaded.



[options="header"]
|====
|Timer unit|Function|Overrides the `apply_updates` and `download_updates` settings in the `[commands]` section of the `/etc/dnf/automatic.conf` file?
|`dnf-automatic-download.timer`|Downloads packages to cache and makes them available for updating.

This timer unit does not install the updated packages. To perform the installation, you must run the `{PackageManagerCommand} update` command.
|Yes
|`dnf-automatic-install.timer`|Downloads and installs updated packages.|Yes
|`dnf-automatic-notifyonly.timer`|Downloads only repository data to keep repository cache up-to-date and notifies you about available updates.

This timer unit does not download or install the updated packages|Yes
|`dnf-automatic.timer`|The behavior of this timer when downloading and applying updates is specified by the settings in the `/etc/dnf/automatic.conf` file.

This timer downloads packages, but does not install them. 
|No
|====


[role="_additional-resources"]
.Additional resources
* `dnf-automatic(8)` man page on your system 
