:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="limiting-a-package-to-a-specific-perl-version_{context}"]
= Limiting a package to a specific Perl version

[role="_abstract"]
To limit your package to a specific Perl version, follow this procedure:

.Procedure
* Use the `perl(:VERSION)` dependency with the desired version constraint in your RPM `spec` file:
ifeval::[{ProductNumber} == 8]
+
For example, to limit a package to Perl version 5.22 and later, use:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
BuildRequires: perl(:VERSION) >= 5.22
....
endif::[]

ifeval::[{ProductNumber} == 9]
+
For example, to limit a package to Perl version 5.30 and higher, use:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
BuildRequires: perl(:VERSION) >= 5.30
....
endif::[]


WARNING: Do not use a comparison against the version of the [package]`perl` package because it includes an epoch number.
