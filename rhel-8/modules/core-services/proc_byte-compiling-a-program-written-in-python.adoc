
:_mod-docs-content-type: PROCEDURE

[id="byte-compiling-a-program-written-in-python_{context}"]
= Byte-compiling a sample Python program

By choosing byte-compiling over raw-interpreting of Python source code, you can create faster software.

A sample `Hello World` program written in the link:https://www.python.org/[Python] programming language (`pello.py`) has the following contents:

[literal,subs="+quotes,verbatim,macros,normal"]
....
print("Hello World")
....

.Procedure

. Byte-compile the [filename]`pello.py` file:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ **python -m compileall pello.py**
....

. Verify that a byte-compiled version of the file is created:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ **ls pass:[__pycache__]**
pello.cpython-311.pyc
....
+
Note that the package version in the output might differ depending on which Python version is installed. 


. Run the program in `pello.py`:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ **python pello.py**
Hello World
....
