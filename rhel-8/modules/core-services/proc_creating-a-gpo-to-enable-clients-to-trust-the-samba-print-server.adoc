:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_setting-up-automatic-printer-driver-downloads-for-windows-clients.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_creating-a-gpo-to-enable-clients-to-trust-the-samba-print-server.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.

:experimental:

[id="proc_creating-a-gpo-to-enable-clients-to-trust-the-samba-print-server_{context}"]
= Creating a GPO to enable clients to trust the Samba print server

For security reasons, recent Windows operating systems prevent clients from downloading non-package-aware printer drivers from an untrusted server. If your print server is a member in an AD, you can create a Group Policy Object (GPO) in your domain to trust the Samba server.

.Prerequisites
* The Samba print server is a member of an AD domain.
* The Windows computer you are using to create the GPO must have the Windows Remote Server Administration Tools (RSAT) installed. For details, see the Windows documentation.

.Procedure

. Log into a Windows computer using an account that is allowed to edit group policies, such as the AD domain `Administrator` user.

. Open the `Group Policy Management Console`.

. Right-click to your AD domain and select `Create a GPO in this domain, and Link it here`.
+
image::samba_create_new_GPO.png[]

. Enter a name for the GPO, such as `Legacy Printer Driver Policy` and click `OK`. The new GPO will be displayed under the domain entry.

. Right-click to the newly-created GPO and select `Edit` to open the `Group Policy Management Editor`.

. Navigate to menu:Computer Configuration[Policies>Administrative Templates>Printers].
+
image::samba_select_printer_GPO_group.png[]

. On the right side of the window, double-click `Point and Print Restriction` to edit the policy:

.. Enable the policy and set the following options:

... Select `Users can only point and print to these servers` and enter the fully-qualified domain name (FQDN) of the Samba print server to the field next to this option.

... In both check boxes under `Security Prompts`, select `Do not show warning or elevation prompt`.
+
image::samba_GPO_point_and_print.png[]

.. Click OK.

. Double-click `Package Point and Print - Approved servers` to edit the policy:

.. Enable the policy and click the `Show` button.

.. Enter the FQDN of the Samba print server.
+
image::samba_GPO_approved_servers.png[]

.. Close both the `Show Contents` and the policy's properties window by clicking `OK`.

. Close the `Group Policy Management Editor`.

. Close the `Group Policy Management Console`.

After the Windows domain members applied the group policy, printer drivers are automatically downloaded from the Samba server when a user connects to a printer.

[role="_additional-resources"]
.Additional resources
* For using group policies, see the Windows documentation.
