:_newdoc-version: 2.18.3
:_template-generated: 2024-12-16

:_mod-docs-content-type: REFERENCE

[id="advantages-and-disadvantages-of-continuous-archiving_{context}"]
= Advantages and disadvantages of continuous archiving

Continuous archiving has the following advantages compared to other *PostgreSQL* backup methods:

* With the continuous backup method, it is possible to use a base backup that is not entirely consistent because any internal inconsistency in the backup is corrected by the log replay. Therefore you can perform a base backup on a running *PostgreSQL* server.

* A file system snapshot is not needed; `tar` or a similar archiving utility is sufficient.

* Continuous backup can be achieved by continuing to archive the WAL files because the sequence of WAL files for the log replay can be indefinitely long. This is particularly valuable for large databases.

* Continuous backup supports point-in-time recovery. It is not necessary to replay the WAL entries to the end. The replay can be stopped at any point and the database can be restored to its state at any time since the base backup was taken.

* If the series of WAL files are continuously available to another machine that has been loaded with the same base backup file, it is possible to restore the other machine with a nearly-current copy of the database at any point.

Continuous archiving has the following disadvantages compared to other *PostgreSQL* backup methods:

* Continuous backup method supports only restoration of an entire database cluster, not a subset.

* Continuous backup requires extensive archival storage.


