:_mod-docs-content-type: PROCEDURE

[id="checking-the-pello-spec-file_{context}"]
= Checking the pello spec file for common errors

Inspect the outputs of the following examples to learn how to check a `pello` `spec` file for common errors.

.Output of running the [command]`rpmlint` command on the `pello` `spec` file

[literal,subs="+quotes,attributes"]
....
$ *rpmlint pello.spec*
pello.spec:30: E: *hardcoded-library-path* in %{buildroot}/usr/lib/%{name}
pello.spec:34: E: *hardcoded-library-path* in /usr/lib/%{name}/%{name}.pyc
pello.spec:39: E: *hardcoded-library-path* in %{buildroot}/usr/lib/%{name}/
pello.spec:43: E: *hardcoded-library-path* in /usr/lib/%{name}/
pello.spec:45: E: *hardcoded-library-path* in /usr/lib/%{name}/%{name}.py*
pello.spec: W: *invalid-url Source0*: https://www.example.com/pello/releases/pello-0.1.2.tar.gz HTTP Error 404: Not Found
0 packages and 1 specfiles checked; 5 errors, 1 warnings.
....

* The `invalid-url Source0` warning means that the URL listed in the `Source0` directive is unreachable. This is expected, because the specified `example.com` URL does not exist. Assuming that this URL will be valid in the future, you can ignore this warning.

// There are many errors, because we intentionally wrote this SPEC file to be uncomplicated and to show what errors ``rpmlint`` can report.

* The `hardcoded-library-path` errors suggest using the `%{_libdir}` macro instead of hard-coding the library path. For the sake of this example, you can safely ignore these errors. However, for packages going into production, check all errors carefully.

.Output of running the [command]`rpmlint` command on the SRPM for pello

[literal,subs="+quotes,attributes"]
....
$ *rpmlint ~/rpmbuild/SRPMS/pello-0.1.2-1.el8.src.rpm*
pello.src: W: *invalid-url URL*: https://www.example.com/pello HTTP Error 404: Not Found
pello.src:30: E: hardcoded-library-path in %{buildroot}/usr/lib/%{name}
pello.src:34: E: hardcoded-library-path in /usr/lib/%{name}/%{name}.pyc
pello.src:39: E: hardcoded-library-path in %{buildroot}/usr/lib/%{name}/
pello.src:43: E: hardcoded-library-path in /usr/lib/%{name}/
pello.src:45: E: hardcoded-library-path in /usr/lib/%{name}/%{name}.py*
pello.src: W: invalid-url Source0: https://www.example.com/pello/releases/pello-0.1.2.tar.gz HTTP Error 404: Not Found
1 packages and 0 specfiles checked; 5 errors, 2 warnings.
....


The `invalid-url URL` error means that the URL mentioned in the `URL` directive is unreachable. Assuming that this URL will be valid in the future, you can ignore this warning.

