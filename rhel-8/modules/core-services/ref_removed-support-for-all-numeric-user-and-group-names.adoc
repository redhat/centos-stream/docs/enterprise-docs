:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_removed-support-for-all-numeric-user-and-group-names.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.

[id="removed-support-for-all-numeric-user-and-group-names_{context}"]
= Removed support for all-numeric user and group names

In {RHEL} (RHEL) 8, the [command]`useradd` and [command]`groupadd` commands does not allow you to use user and group names consisting purely of numeric characters. The reason for not allowing such names is that this can confuse tools that work with user and group names and user and group ids, which are numbers.

See more information about link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/managing-users-and-groups_configuring-basic-system-settings#managing-users-from-the-command-line_managing-users-and-groups[Adding a new user from the command line]. 

