:_mod-docs-content-type: PROCEDURE
[id="performing-physical-online-backup_{context}"]
= Performing physical online backup using the Mariabackup utility

[role="_abstract"]
[application]*Mariabackup* is a utility based on the Percona XtraBackup technology, which enables performing physical online backups of InnoDB, Aria, and MyISAM tables. This utility is provided by the [package]`mariadb-backup` package from the AppStream repository.

[application]*Mariabackup* supports full backup capability for *MariaDB* server, which includes encrypted and compressed data.

.Prerequisites

* The [package]`mariadb-backup` package is installed on the system:
+
[literal,subs="+quotes,verbatim,normal,normal,attributes"]
----
# *{PackageManagerCommand} install mariadb-backup*
----

// Optionally, you can configure [application]*Mariabackup* with the user name and the password. To set the user name and the password, add the following lines into the `[xtrabackup]` or `[mysqld]` section of the [filename]`*.cnf` configuration file that you need to create (for example, [filename]`/etc/my.cnf.d/mariabackup.cnf`):

* You must provide [application]*Mariabackup* with credentials for the user under which the backup will be run. You can provide the credentials either on the command line or by a configuration file.

* Users of [application]*Mariabackup* must have the `RELOAD`, `LOCK TABLES`, and `REPLICATION CLIENT` privileges.


To create a backup of a database using [application]*Mariabackup*, use the following procedure.

.Procedure

* To create a backup while providing credentials on the command line, run:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *mariabackup --backup --target-dir <backup_directory> --user <backup_user> --password <backup_passwd>*
----
+
The [option]`target-dir` option defines the directory where the backup files are stored. If you want to perform a full backup, the target directory must be empty or not exist.
+
The [option]`user` and [option]`password` options allow you to configure the user name and the password.

* To create a backup with credentials set in a configuration file:
+
. Create a configuration file in the `/etc/my.cnf.d/` directory, for example, [filename]`/etc/my.cnf.d/mariabackup.cnf`.
. Add the following lines into the `[xtrabackup]` or `[mysqld]` section of the new file:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
[xtrabackup]
user=myuser
password=mypassword
----
+
. Perform the backup:
+
[literal,subs="+quotes,verbatim,normal,normal"]
----
$ *mariabackup --backup --target-dir <backup_directory>*
----



[role="_additional-resources"]
.Additional resources
* link:https://mariadb.com/kb/en/library/full-backup-and-restore-with-mariadb-backup/[Full Backup and Restore with Mariabackup]
