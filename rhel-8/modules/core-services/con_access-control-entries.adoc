:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_managing-acls-on-an-smb-share-using-smbcacls.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/con_access-control-entries.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.
[id="con_access-control-entries_{context}"]
= Access control entries

Each ACL entry of a file system object contains Access Control Entries (ACE) in the following format:

[literal,subs="+quotes,attributes"]
----
_security_principal_:__access_right__/_inheritance_information_/_permissions_
----

.Access control entries
====
If the `AD\Domain Users` group has `Modify` permissions that apply to `This folder, subfolders, and files` on Windows, the ACL contains the following ACE:
[literal,subs="+quotes,attributes"]
----
AD\Domain Users:ALLOWED/OI|CI/CHANGE
----
====

An ACE contains the following parts:

Security principal::
  The security principal is the user, group, or SID the permissions in the ACL are applied to.

Access right::
  Defines if access to an object is granted or denied. The value can be `ALLOWED` or `DENIED`.

Inheritance information::
  The following values exist:
+
[cols="1,2,4",options="header"]
.Inheritance settings
|===
| Value | Description | Maps to
| `OI` | Object Inherit | This folder and files
| `CI` | Container Inherit | This folder and subfolders
| `IO` | Inherit Only | The ACE does not apply to the current file or directory
| `ID` | Inherited | The ACE was inherited from the parent directory
|===
+
Additionally, the values can be combined as follows:
+
[cols="1,2",options="header"]
.Inheritance settings combinations
|===
| Value combinations | Maps to the Windows `Applies to` setting
| `OI\|CI` | This folder, subfolders, and files
| `OI\|CI\|IO` | Subfolders and files only
| `CI\|IO` | Subfolders only
| `OI\|IO` | Files only
|===

Permissions::
  This value can be either a hex value that represents one or more Windows permissions or an `smbcacls` alias:
+
* A hex value that represents one or more Windows permissions.
+
The following table displays the advanced Windows permissions and their corresponding value in hex format:
+
[id="table_windows-permissions-hex-values_{context}"]
[cols="2,1",options="header"]
.Windows permissions and their corresponding smbcacls value in hex format
|===
| Windows permissions | Hex values
| Full control | `0x001F01FF`
| Traverse folder / execute file | `0x00100020`
| List folder / read data | `0x00100001`
| Read attributes | `0x00100080`
| Read extended attributes | `0x00100008`
| Create files / write data | `0x00100002`
| Create folders / append data | `0x00100004`
| Write attributes | `0x00100100`
| Write extended attributes | `0x00100010`
| Delete subfolders and files | `0x00100040`
| Delete | `0x00110000`
| Read permissions | `0x00120000`
| Change permissions | `0x00140000`
| Take ownership | `0x00180000`
|===
+
Multiple permissions can be combined as a single hex value using the bit-wise `OR` operation.
ifeval::[{ProductNumber} == 8]
ifdef::differentserver-title[]
For details, see xref:con_ace-mask-calculation_assembly_managing-acls-on-an-smb-share-using-smbcacls[ACE mask calculation].
endif::[]
ifndef::differentserver-title[]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/assembly_using-samba-as-a-server_deploying-different-types-of-servers#con_ace-mask-calculation_assembly_managing-acls-on-an-smb-share-using-smbcacls[ACE mask calculation].
endif::[]
endif::[]

ifeval::[{ProductNumber} == 9]
ifdef::network-file-services[]
For details, see xref:con_ace-mask-calculation_assembly_managing-acls-on-an-smb-share-using-smbcacls[ACE mask calculation].
endif::[]
ifndef::network-file-services[]
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring-and-using-network-file-services/assembly_using-samba-as-a-server_configuring-and-using-network-file-services#con_ace-mask-calculation_assembly_managing-acls-on-an-smb-share-using-smbcacls[ACE mask calculation].
endif::[]
endif::[]

* An `smbcacls` alias. The following table displays the available aliases:
+
[id="table_smbcacls-aliases_{context}"]
[cols="1,2a",options="header"]
.Existing smbcacls aliases and their corresponding Windows permission
|===
| `smbcacls` alias | Maps to Windows permission
| `R` | Read
| `READ` | Read & execute
| `W` | Special:

* Create files / write data

* Create folders / append data

* Write attributes

* Write extended attributes

* Read permissions

| `D` | Delete
| `P` | Change permissions
| `O` | Take ownership
| `X` | Traverse / execute
| `CHANGE` | Modify
| `FULL` | Full control
|===
+
[NOTE]
====
You can combine single-letter aliases when you set permissions. For example, you can set `RD` to apply the Windows permission `Read` and `Delete`. However, you can neither combine multiple non-single-letter aliases nor combine aliases and hex values.
====
