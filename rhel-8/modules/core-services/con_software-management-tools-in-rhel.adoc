:_mod-docs-content-type: CONCEPT
[id="software-management-tools-in-rhel_{context}"]
= Software management tools in RHEL 8

[role="_abstract"]
In RHEL 8, software installation is enabled by the new version of the [application]*YUM* tool (*YUM v4*), which is based on the [application]*DNF* technology.

NOTE: Upstream documentation identifies the technology as [application]*DNF* and the tool is referred to as [application]*DNF* in the upstream. As a result, some output returned by the new [application]*YUM* tool in RHEL 8 mentions [application]*DNF*.

[subs="+attributes"]
Although [application]*YUM v4* used in RHEL 8 is based on [application]*DNF*, it is compatible with [application]*YUM v3* used in RHEL 7. For software installation, the [command]`{PackageManagerCommand}` command and most of its options work the same way in RHEL 8 as they did in RHEL 7.

[subs="+attributes"]
Selected [application]*{PackageManagerCommand}* plug-ins and utilities have been ported to the new DNF back end, and can be installed under the same names as in RHEL 7. Packages also provide compatibility symlinks, so the binaries, configuration files, and directories can be found in usual locations.

Note that the legacy Python API provided by *YUM v3* is no longer available. You can migrate your plug-ins and scripts to the new API provided by *YUM v4* (DNF Python API), which is stable and fully supported. See link:https://dnf.readthedocs.io/en/latest/api.html[DNF API Reference] for more information.

//The Libdnf and Hawkey APIs (both C and Python) are unstable, and will likely change during the RHEL 8 life cycle.
