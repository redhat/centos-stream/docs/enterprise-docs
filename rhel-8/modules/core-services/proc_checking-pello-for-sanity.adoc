
:_mod-docs-content-type: PROCEDURE

[id="checking-pello-for-sanity_{context}"]
= Checking pello for sanity

[role="_abstract"]
This section shows possible warnings and errors that can occur when checking RPM sanity on the example of the pello SPEC file and pello binary RPM.

[id="checking-pello-spec-file_{context}"]
== Checking the pello SPEC File

.Output of running the [command]`rpmlint` command on the SPEC file for pello
====
[literal,subs="+quotes,attributes"]
....
$ *rpmlint pello.spec*
pello.spec:30: E: hardcoded-library-path in %{buildroot}/usr/lib/%{name}
pello.spec:34: E: hardcoded-library-path in /usr/lib/%{name}/%{name}.pyc
pello.spec:39: E: hardcoded-library-path in %{buildroot}/usr/lib/%{name}/
pello.spec:43: E: hardcoded-library-path in /usr/lib/%{name}/
pello.spec:45: E: hardcoded-library-path in /usr/lib/%{name}/%{name}.py*
pello.spec: W: invalid-url Source0: https://www.example.com/pello/releases/pello-0.1.2.tar.gz HTTP Error 404: Not Found
0 packages and 1 specfiles checked; 5 errors, 1 warnings.
....
====

The `invalid-url Source0` warning says that the URL listed in the `Source0` directive is unreachable. This is expected, because the specified `example.com` URL does not exist. Presuming that this URL will work in the future, you can ignore this warning.

// There are many errors, because we intentionally wrote this SPEC file to be uncomplicated and to show what errors ``rpmlint`` can report.

The `hardcoded-library-path` errors suggest to use the `%{_libdir}` macro instead of hard-coding the library path. For the sake of this example, you can safely ignore these errors. However, for packages going into production make sure to check all errors carefully.

.Output of running the [command]`rpmlint` command on the SRPM for pello
====
[literal,subs="+quotes,attributes"]
....
$ *rpmlint ~/rpmbuild/SRPMS/pello-0.1.2-1.el8.src.rpm*
pello.src: W: invalid-url URL: https://www.example.com/pello HTTP Error 404: Not Found
pello.src:30: E: hardcoded-library-path in %{buildroot}/usr/lib/%{name}
pello.src:34: E: hardcoded-library-path in /usr/lib/%{name}/%{name}.pyc
pello.src:39: E: hardcoded-library-path in %{buildroot}/usr/lib/%{name}/
pello.src:43: E: hardcoded-library-path in /usr/lib/%{name}/
pello.src:45: E: hardcoded-library-path in /usr/lib/%{name}/%{name}.py*
pello.src: W: invalid-url Source0: https://www.example.com/pello/releases/pello-0.1.2.tar.gz HTTP Error 404: Not Found
1 packages and 0 specfiles checked; 5 errors, 2 warnings.
....
====

The new `invalid-url URL` error here is about the `URL` directive, which is unreachable. Assuming that the URL will be valid in the future, you can safely ignore this error.

[id="checking-pello-binary-rpm_{context}"]
== Checking the pello binary RPM

When checking binary RPMs, [command]`rpmlint` checks for the following items:

* Documentation
* Manual pages
* Consistent use of the Filesystem Hierarchy Standard

.Output of running the [command]`rpmlint` command on the binary RPM for pello
====
[literal,subs="+quotes,attributes"]
....
$ *rpmlint ~/rpmbuild/RPMS/noarch/pello-0.1.2-1.el8.noarch.rpm*
pello.noarch: W: invalid-url URL: https://www.example.com/pello HTTP Error 404: Not Found
pello.noarch: W: only-non-binary-in-usr-lib
pello.noarch: W: no-documentation
pello.noarch: E: non-executable-script /usr/lib/pello/pello.py 0644L /usr/bin/env
pello.noarch: W: no-manual-page-for-binary pello
1 packages and 0 specfiles checked; 1 errors, 4 warnings.
....
====

The `no-documentation` and `no-manual-page-for-binary` warnings say that the RPM has no documentation or manual pages, because you did not provide any.

The `only-non-binary-in-usr-lib` warning says that you provided only non-binary artifacts in [filename]`/usr/lib/`. This directory is normally reserved for shared object files, which are binary files. Therefore, [command]`rpmlint` expects at least one or more files in [filename]`/usr/lib/` directory to be binary.

This is an example of an [command]`rpmlint` check for compliance with Filesystem Hierarchy Standard. Normally, use RPM macros to ensure the correct placement of files. For the sake of this example, you can safely ignore this warning.

The `non-executable-script` error warns that the [filename]`/usr/lib/pello/pello.py` file has no execute permissions. The `rpmlint` tool expects the file to be executable, because the file contains the shebang. For the purpose of this example, you can leave this file without execute permissions and ignore this error.

Apart from the above warnings and errors, the RPM passed [command]`rpmlint` checks.
