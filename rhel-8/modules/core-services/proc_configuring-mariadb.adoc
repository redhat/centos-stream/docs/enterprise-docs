:_mod-docs-content-type: PROCEDURE
[id="configuring-mariadb_{context}"]
= Configuring MariaDB

[role="_abstract"]
To configure the *MariaDB* server for networking, use the following procedure.

.Procedure

. Edit the `[mysqld]` section of the [filename]`/etc/my.cnf.d/mariadb-server.cnf` file. You can set the following configuration directives:
+
* `bind-address` - is the address on which the server listens. Possible options are:
** a host name
** an IPv4 address
** an IPv6 address
+
* `skip-networking` - controls whether the server listens for TCP/IP connections. Possible values are:
** 0 - to listen for all clients
** 1 - to listen for local clients only
+
* `port` - the port on which *MariaDB* listens for TCP/IP connections.

. Restart the `mariadb` service:
+
[literal,subs="+quotes,verbatim,normal"]
----
# *systemctl restart mariadb.service*
----


////
Q: was the [mysqld] section also renamed in MariaDB 10.5+?
A: There are many section headers which are processed only by a specific pieces of the DB.
You can use e.g. [mariadb-10.5] for option which should be only read by a server of version 10.5
or
[mysqldump] for options only read by the mysqldump command line tool.
The [mysqld] is the most general for the server part. It is read by all server versions, both MariaDB and MySQL (e.g. in case the same config is reused with different DB) ...
////
