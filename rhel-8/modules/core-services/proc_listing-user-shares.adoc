:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_enabling-users-to-share-directories-on-a-samba-server.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_listing-user-shares.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_listing-user-shares_{context}"]
= Listing user shares

If you want to list only the available user shares without their settings on a Samba server, use the `net usershare list` command.

.Prerequisites

* A user share is configured on the Samba server.

.Procedure

. To list the shares created by any user:
+
[literal,subs="+quotes,attributes"]
----
$ *net usershare list -l*
_share_1_
_share_2_
...
----
+
To list only shares created by the user who runs the command, omit the `-l` parameter.

. To list only specific shares, pass the share name or wild cards to the command. For example, to list only shares whose name starts with `share_`:
+
[literal,subs="+quotes,attributes"]
----
$ **net usershare list -l share_* **
----
