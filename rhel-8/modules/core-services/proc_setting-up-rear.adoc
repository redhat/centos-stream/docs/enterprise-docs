:_mod-docs-content-type: PROCEDURE
[id="proc_setting-up-rear_{context}"]
= Setting up ReaR and manually creating a backup

[role="_abstract"]
Use the following steps to install the package for using the Relax-and-Recover (ReaR) utility, create a rescue system, configure and generate a backup.


.Prerequisites

* Necessary configurations as per the backup restore plan are ready.
+
Note that you can use the `NETFS` backup method, a fully-integrated and built-in method with ReaR.


.Procedure

. Install the ReaR utility:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install rear*
----

. Modify the ReaR configuration file in an editor of your choice, for example:
+
[literal,subs="+quotes,attributes"]
----
# *vi /etc/rear/local.conf*
----

. Add the backup setting details to `/etc/rear/local.conf`. For example, in the case of the `NETFS` backup method, add the following lines:
+
[subs="quotes,attributes"]
----
BACKUP=NETFS
BACKUP_URL=_backup.location_
----
+
Replace _backup.location_ by the URL of your backup location.

. To configure ReaR to keep the previous backup archive when the new one is created, also add the following line to the configuration file:
+
----
NETFS_KEEP_OLD_BACKUP_COPY=y
----

. To make the backups incremental, meaning that only the changed files are backed up on each run, add the following line:
+
----
BACKUP_TYPE=incremental
----

. Create a rescue system:
+
[literal,subs="+quotes,attributes"]
----
# *rear mkrescue*
----

. Create a backup as per the restore plan. For example, in the case of the `NETFS` backup method, run the following command:
+
[literal,subs="+quotes,attributes"]
----
# *rear mkbackuponly*
----
+
Alternatively, you can create the rescue system and the backup in a single step by running the following command:
+
[literal,subs="+quotes,attributes"]
----
# *rear mkbackup*
----
+
This command combines the functionality of the [command]`rear mkrescue` and [command]`rear mkbackuponly` commands.
