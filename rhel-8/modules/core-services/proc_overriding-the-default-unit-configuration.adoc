:_mod-docs-content-type: PROCEDURE
[id="proc_overriding-the-default-unit-configuration_{context}"]
= Overriding the default unit configuration

[role="_abstract"]
You can make changes to the unit file configuration that will persist after updating the package that provides the unit file.

.Procedure

. Copy the unit file to the `/etc/systemd/system/` directory by entering the following command as `root`:
+
[literal,subs="+quotes,attributes"]
----
# *cp /usr/lib/systemd/system/_<name>_.service /etc/systemd/system/_<name>_.service*
----

. Open the copied file with a text editor, and make changes.

. Apply unit changes:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl daemon-reload*
# *systemctl restart _<name>_.service*
----
