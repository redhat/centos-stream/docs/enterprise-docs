:_mod-docs-content-type: PROCEDURE
[id="proc_changing-the-language-using-desktop-gui_{context}"]
= Changing the language by using desktop GUI

[role="_abstract"]
You can change the system language using the desktop GUI.

.Prerequisites

* Required language packages are installed on your system

.Procedure

. Open the *Settings* application from the system menu by clicking on its icon.
+
image:gnome-system-menu.png[System menu]

. In *Settings*, choose *Region & Language* from the left vertical bar.

. Click the *Language* menu.
+
image:cs_language_menu.png[]

. Select the required region and language from the menu.
+
image:cs_select_region_language.png[]
+
If your region and language are not listed, scroll down, and click *More* to select from available regions and languages.
+
image:cs_available_region_language.png[]

. Click *Done*.

. Click *Restart* for changes to take effect.
+
image:cs_restart_region_language.png[]

[NOTE]
====
Some applications do not support certain languages. The text of an application that cannot be translated into the selected language remains in US English.
====

[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_the_desktop_environment_in_rhel_8/getting-started-with-gnome_using-the-desktop-environment-in-rhel-8#launching-applications-in-gnome_getting-started-with-gnome[Launching applications in GNOME]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/getting_started_with_the_gnome_desktop_environment/assembly_launching-applications-in-gnome_getting-started-with-the-gnome-desktop-environment[Launching applications in GNOME]
endif::[]
