:_mod-docs-content-type: PROCEDURE
[id="installing-mysql_{context}"]
= Installing MySQL

In RHEL 8, the *MySQL 8.0* server is available as the `mysql:8.0` module stream.

[NOTE]
====
The *MySQL* and *MariaDB* database servers cannot be installed in parallel in RHEL 8 due to conflicting RPM packages. You can use the *MySQL* and *MariaDB* database servers in parallel in containers, see xref:running-multiple-mysql-mariadb-versions-in-containers_{context}[Running multiple MySQL and MariaDB versions in containers].
====

To install *MySQL*, use the following procedure.

.Procedure

. Install *MySQL* server packages by selecting the `8.0` stream (version) from the `mysql` module and specifying the `server` profile:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} module install mysql:8.0/server*
----

. Start the `mysqld` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl start mysqld.service*
----

. Enable the `mysqld` service to start at boot:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl enable mysqld.service*
----

. _Recommended:_ To improve security when installing *MySQL*, run the following command:
+
[literal,subs="+quotes,attributes"]
----
$ *mysql_secure_installation*
----
+
The command launches a fully interactive script, which prompts for each step in the process. The script enables you to improve security in the following ways:
+
* Setting a password for root accounts
* Removing anonymous users
* Disallowing remote root logins (outside the local host)



