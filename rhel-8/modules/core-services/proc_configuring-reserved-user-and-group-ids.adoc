:_mod-docs-content-type: PROCEDURE
[id="configuring-reserved-user-and-group-ids_{context}"]
= Configuring reserved user and group IDs

[role="_abstract"]
By default, RHEL reserves user and group IDs below 1000 for system users and groups. You can find the reserved user and group IDs in the [package]`setup` package. The UID's and GID's of users and groups created before you changed the `UID_MIN` and `GID_MIN` values do not change. The reserved user and group IDs are documented in the:

----
/usr/share/doc/setup/uidgid
----

To assign IDs to the new users and groups starting at 5000, as the reserved range can increase in the future.

Modify the `UID_MIN` and `GID_MIN` parameters in the [filename]`/etc/login.defs` file to define a start ID other than the defaults (1000).

[WARNING]
====
Do not raise IDs reserved by the system above 1000 by changing `SYS_UID_MAX` to avoid conflict with systems that retain the 1000 limit.
====

.Procedure

. Open the [filename]`/etc/login.defs` file in an editor.
. Set the `UID_MIN` variable, for example:
+
[subs=+quotes]
----
# Min/max values for automatic uid selection in useradd
#
UID_MIN                  *5000*
----

. Set the `GID_MIN` variable, for example:
+
[subs=+quotes]
----
# Min/max values for automatic gid selection in groupadd
#
GID_MIN                  *5000*
----
The dynamically assigned UIDs and GIDs for the regular users now start at 5000.



