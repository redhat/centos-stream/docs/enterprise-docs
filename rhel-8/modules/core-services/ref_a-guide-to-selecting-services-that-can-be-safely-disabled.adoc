:_mod-docs-content-type: REFERENCE
[id="ref_a-guide-to-selecting-services-that-can-be-safely-disabled_{context}"]
= A guide to selecting services that can be safely disabled

[role="_abstract"]
You can shorten the boot time of your system by disabling certain services that are enabled on boot by default.

* List enabled services:
+
[literal,subs="+quotes,attributes"]
----
$ *systemctl list-unit-files --state=enabled*
----

* Disable a service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl disable _<service_name>_*
----

Certain services must stay enabled so that your operating system is safe and functions in the way you need.

Refer to the following table as a guide to selecting the services that you can safely disable. The table lists all services enabled by default on a minimal installation of {RHEL}.


.Services enabled by default on a minimal installation of RHEL 
[cols="3,2,8"]
|===
|Service name |Can it be disabled? |More information

|auditd.service
|yes
|Disable `auditd.service` only if you do not need audit messages from the kernel. Be aware that if you disable `auditd.service`, the [filename]`/var/log/audit/audit.log` file is not produced. Consequently, you are not able to retroactively review some commonly-reviewed actions or events, such as user logins, service starts or password changes. Also note that auditd has two parts: a kernel part, and a service itself. By using the [command]`systemctl disable auditd` command, you only disable the service, but not the kernel part. To disable system auditing in its entirety, set `audit=0` on kernel command line.
//Be aware that such solution lowers system security. Also note that auditd has two parts: a kernel part, and a service itself. By using the [command]`systemctl disable auditd` command, you only disable the service, but not the kernel part. To disable system auditing in its entirety, set `audit=0` on kernel command line.

|autovt@.service
|no
|This service runs only when it is really needed, so it does not need to be disabled.

|crond.service
|yes
|Be aware that no items from crontab will run if you disable crond.service.

|dbus-org.fedoraproject.FirewallD1.service
|yes
|A symlink to `firewalld.service`

|dbus-org.freedesktop.NetworkManager.service
|yes
|A symlink to `NetworkManager.service`

|dbus-org.freedesktop.nm-dispatcher.service
|yes
|A symlink to `NetworkManager-dispatcher.service`

|firewalld.service
|yes
|Disable `firewalld.service` only if you do not need firewall.

|getty@.service
|no
|This service runs only when it is really needed, so it does not need to be disabled.

|import-state.service
|yes
|Disable `import-state.service` only if you do not need to boot from a network storage.

|irqbalance.service
|yes
|Disable `irqbalance.service` only if you have just one CPU. Do not disable `irqbalance.service` on systems with multiple CPUs.

|kdump.service
|yes
|Disable `kdump.service` only if you do not need reports from kernel crashes.

|loadmodules.service
|yes
|This service is not started unless the [filename]`/etc/rc.modules` or [filename]`/etc/sysconfig/modules` directory exists, which means that it is not started on a minimal RHEL installation.

|lvm2-monitor.service
|yes
|Disable `lvm2-monitor.service` only if you do not use Logical Volume Manager (LVM).

|microcode.service
|no
|Do not be disable the service because it provides updates of the microcode software in CPU.

|NetworkManager-dispatcher.service
|yes
|Disable `NetworkManager-dispatcher.service` only if you do not need notifications on network configuration changes (for example in static networks).

|NetworkManager-wait-online.service
|yes
|Disable `NetworkManager-wait-online.service` only if you do not need working network connection available right after the boot. If the service is enabled, the system does not finish the boot before the network connection is working. This may prolong the boot time significantly.

|NetworkManager.service
|yes
|Disable `NetworkManager.service` only if you do not need connection to a network.

|nis-domainname.service
|yes
|Disable `nis-domainname.service` only if you do not use Network Information Service (NIS).

|rhsmcertd.service
|no
|

|rngd.service
|yes
|Disable `rngd.service` only if you do not need much entropy on your system, or you do not have any sort of hardware generator. Note that the service is necessary in environments that require a lot of good entropy, such as systems used for generation of X.509 certificates (for example the FreeIPA server).
//Note that the service is necessary on certification systems, or in environments with a high level of security required.

|rsyslog.service
|yes
|Disable `rsyslog.service` only if you do not need persistent logs, or you set `systemd-journald` to persistent mode.

|selinux-autorelabel-mark.service
|yes
|Disable `selinux-autorelabel-mark.service` only if you do not use SELinux.

|sshd.service
|yes
|Disable `sshd.service` only if you do not need remote logins by OpenSSH server.

|sssd.service
|yes
|Disable `sssd.service` only if there are no users who log in the system over the network (for example by using LDAP or Kerberos). {RH} recommends to disable all `sssd-*` units if you disable `sssd.service`.

|syslog.service
|yes
|An alias for `rsyslog.service`

|tuned.service
|yes
|Disable `tuned.service` only if you do need to use performance tuning.

//|dm-event.socket
//|no
//|Device-mapper event daemon FIFOs ???

|lvm2-lvmpolld.socket
|yes
|Disable `lvm2-lvmpolld.socket` only if you do not use Logical Volume Manager (LVM).

// |sssd-kcm.socket
// |??
// |

// |remote-fs.target
// |no
// |Remote File Systems ???

|dnf-makecache.timer
|yes
|Disable `dnf-makecache.timer` only if you do not need your package metadata to be updated automatically.

|unbound-anchor.timer
|yes
|Disable `unbound-anchor.timer` only if you do not need daily update of the root trust anchor for DNS Security Extensions (DNSSEC). This root trust anchor is used by Unbound resolver and resolver library for DNSSEC validation.

|===

To find more information about a service, use one of the following commands:

[literal,subs="+quotes,attributes"]
----
$ *systemctl cat _<service_name>_*
----


[literal,subs="+quotes,attributes"]
----
$ *systemctl help _<service_name>_*
----

The `systemctl cat` command provides the content of the respective `/usr/lib/systemd/system/_<service>_` service file, as well as all applicable overrides. The applicable overrides include unit file overrides from the `/etc/systemd/system/_<service>_` file or drop-in files from a corresponding `unit.type.d` directory.


[role="_additional-resources"]
.Additional resources
* `systemd.unit(5)` man page on your system
* The `systemd help` command that shows the man page of a particular service


