
:_mod-docs-content-type: PROCEDURE

[id="putting-the-cello-program-into-tarball_{context}"]
= Creating a source code archive for a sample C program

// The __cello__ project implements `Hello World` in
// ifdef::community[https://en.wikipedia.org/wiki/C_%28programming_language%29[C]]
// ifdef::rhel[C].

The _cello_ project is a `Hello World` file in C. 

The following example contains only the [filename]`cello.c` and the [filename]`Makefile` files. Therefore, the resulting [filename]`tar.gz` archive has two files in addition to the [filename]`LICENSE` file.

NOTE: The [filename]`patch` file is not distributed in the archive with the program. The RPM package manager applies the patch when the RPM is built. The patch will be placed into the [filename]`~/rpmbuild/SOURCES/` directory together with the [filename]`tar.gz` archive.

.Prerequisites

* Assume that the `1.0` version of the `cello` program is used.
* You created a `LICENSE` file. For instructions, see xref:creating-a-license-file-for-packaging_preparing-software-for-rpm-packaging[Creating a LICENSE file].


.Procedure

. Move all required files into a single directory:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ **mkdir cello-1.0**

$ **mv cello.c cello-1.0/**

$ **mv Makefile cello-1.0/**

$ **mv LICENSE cello-1.0/**
....

. Create the archive for distribution:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ **tar -cvzf cello-1.0.tar.gz cello-1.0**
cello-1.0/
cello-1.0/Makefile
cello-1.0/cello.c
cello-1.0/LICENSE
....

. Move the created archive to the [filename]`~/rpmbuild/SOURCES/` directory, which is the default directory where the [command]`rpmbuild` command stores the files for building packages:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ **mv cello-1.0.tar.gz ~/rpmbuild/SOURCES/**
....


// Now the source code is ready for packaging into an RPM.

[role="_additional-resources"]
.Additional resources
* xref:hello-world-c_creating-software-for-rpm-packaging[Hello World written in C]