:_mod-docs-content-type: CONCEPT

[id="developing-mysql-client-applications_{context}"]
= Developing MySQL client applications

[role="_abstract"]
Red Hat recommends developing your *MySQL* client applications against the *MariaDB* client library. The communication protocol between client and server is compatible between *MariaDB* and *MySQL*. The *MariaDB* client library works for most common *MySQL* scenarios, with the exception of a limited number of features specific to the *MySQL* implementation.

The development files and programs necessary to build applications against the MariaDB client library are provided by the `mariadb-connector-c-devel` package.

Instead of using a direct library name, use the `mariadb_config` program, which is distributed in the `mariadb-connector-c-devel` package. This program ensures that the correct build flags are returned.
