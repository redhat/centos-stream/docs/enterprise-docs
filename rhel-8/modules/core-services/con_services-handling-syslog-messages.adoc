:_mod-docs-content-type: CONCEPT
[id="con_services-handling-syslog-messages_{context}"]
= Services handling syslog messages
////
In the title of concept modules, include nouns or noun phrases that are used in the body text. This helps readers and search engines find the information quickly. Do not start the title of concept modules with a verb. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
The following two services handle `syslog` messages:

* The `systemd-journald` daemon

The `systemd-journald` daemon collects messages from various sources and forwards them to `Rsyslog` for further processing. The `systemd-journald` daemon collects messages from the following sources:

* Kernel
* Early stages of the boot process
* Standard and error output of daemons as they start up and run
* `Syslog`

* The `Rsyslog` service

The `Rsyslog` service sorts the `syslog` messages by type and priority and writes them to the files in the `/var/log` directory. The `/var/log` directory persistently stores the log messages.
