:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="manually-adding-an-id-mapping-configuration-if-idm-trusts-a-new-domain_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Manually adding an ID mapping configuration if IdM trusts a new domain
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
Samba requires an ID mapping configuration for each domain from which users access resources. On an existing Samba server running on an IdM client, you must manually add an ID mapping configuration after the administrator added a new trust to an Active Directory (AD) domain.


.Prerequisites

// Only display the following if this is the RHEL 8 Conf&Man guide
ifeval::[{ProductNumber} == 8]
* You configured Samba on an IdM client. Afterward, a new trust was added to IdM.
* The DES and RC4 encryption types for Kerberos must be disabled in the trusted AD domain. For security reasons, RHEL 8 does not support these weak encryption types.
endif::[]
// Only display the following if this is the RHEL 9 Conf&Man guide
ifeval::[{ProductNumber} == 9]
* You configured Samba on an IdM client. Afterward, a new trust was added to IdM.
* The DES and RC4 encryption types for Kerberos must be disabled in the trusted AD domain. For security reasons, RHEL 9 does not support these weak encryption types.
endif::[]

.Procedure

. Authenticate using the host's keytab:
+
[literal,subs="+quotes,attributes"]
----
[root@idm_client]# *kinit -k*
----

. Use the [command]`ipa idrange-find` command to display both the base ID and the ID range size of the new domain. For example, the following command displays the values for the [systemitem]`ad.example.com` domain:
+
[literal,subs="+quotes,attributes"]
----
[root@idm_client]# *ipa idrange-find --name="__AD.EXAMPLE.COM___id_range" --raw*
---------------
1 range matched
---------------
  cn: __AD.EXAMPLE.COM___id_range
  ipabaseid: __1918400000__
  ipaidrangesize: __200000__
  ipabaserid: 0
  ipanttrusteddomainsid: __S-1-5-21-968346183-862388825-1738313271__
  iparangetype: ipa-ad-trust
----------------------------
Number of entries returned 1
----------------------------
----
+
You need the values from the [parameter]`ipabaseid` and [parameter]`ipaidrangesize` attributes in the next steps.

. To calculate the highest usable ID, use the following formula:
+
[literal,subs="+quotes,attributes"]
----
maximum_range = ipabaseid + ipaidrangesize - 1
----
+
With the values from the previous step, the highest usable ID for the [systemitem]`ad.example.com` domain is [command]`1918599999` (1918400000 + 200000 - 1).

. Edit the [filename]`/etc/samba/smb.conf` file, and add the ID mapping configuration for the domain to the [parameter]`[global]` section:
+
[literal,subs="+quotes,attributes"]
----
idmap config _AD_ : range = __1918400000__ - __1918599999__
idmap config _AD_ : backend = sss
----
+
Specify the value from [parameter]`ipabaseid` attribute as the lowest and the computed value from the previous step as the highest value of the range.

. Restart the [systemitem]`smb` and `winbind` services:
+
[literal,subs="+quotes,attributes"]
----
[root@idm_client]# *systemctl restart smb winbind*
----


.Verification

* List the shares on the Samba server using Kerberos authentication:
+
[literal,subs="+quotes,attributes"]
....
$ [command]`smbclient -L __idm_client.idm.example.com__ -U __user_name__ --use-kerberos=required`
lp_load_ex: changing to config backend registry

    Sharename       Type      Comment
    ---------       ----      -------
    _example_         Disk
    IPC$            IPC       IPC Service (Samba 4.15.2)
...
....

