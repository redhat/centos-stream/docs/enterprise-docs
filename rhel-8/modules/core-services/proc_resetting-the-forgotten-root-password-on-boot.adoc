:_mod-docs-content-type: PROCEDURE
:experimental:

[id="resetting-the-forgotten-root-password-on-boot_{context}"]
= Resetting the root password 

[role="_abstract"]
If you are unable to log in as root user and have no non-root user with sudo permissions, you can reset the root password or do not belong to the administrative `wheel` group, you can reset the root password by booting the system into the special mode. In this mode, the boot process stops before the system hands over the control from the `initramfs` to the actual system.

.Procedure

. Reboot the system and, on the GRUB boot screen, press the btn:[e] key to interrupt the boot process.
+
The kernel boot parameters appear.
+
ifeval::[{ProductNumber} == 8]
[subs=+quotes]
----
load_video
set gfx_payload=keep
insmod gzio
linux ($root)/vmlinuz-4.18.0-80.e18.x86_64 root=/dev/mapper/rhel-root ro crash\
kernel=auto resume=/dev/mapper/rhel-swap rd.lvm.lv/swap rhgb quiet
initrd ($root)/initramfs-4.18.0-80.e18.x86_64.img $tuned_initrd
----
endif::[]
ifeval::[{ProductNumber} == 9]
[subs=+quotes]
----
load_video
set gfx_payload=keep
insmod gzio
linux ($root)/vmlinuz-5.14.0-70.22.1.e19_0.x86_64 root=/dev/mapper/rhel-root ro crash\
kernel=auto resume=/dev/mapper/rhel-swap rd.lvm.lv/swap rhgb quiet
initrd ($root)/initramfs-5.14.0-70.22.1.e19_0.x86_64.img $tuned_initrd
----
endif::[]

. Set the cursor to the end of the line that starts with *linux*.
. Append `rd.break` to the end of the line that starts with `linux`.
. Press btn:[Ctrl+x] to start the system with the changed parameters.
+
The `switch_root` prompt appears.

. Remount the file system as writable:
+
[literal,subs="+quotes,attributes"]
----
# *mount -o remount,rw /sysroot*
----
+
By default, the file system is mounted as read-only in the `/sysroot` directory. Remounting the file system as writable allows you to change the password.

. Enter the `chroot` environment:
+
[literal,subs="+quotes,attributes"]
----
# *chroot /sysroot*
----
+

. Reset the `root` password:
+
[literal,subs="+quotes,attributes"]
----
# *passwd*
----
+
Follow the instructions displayed by the command line to finalize the change of the `root` password.

. Enable the SELinux relabeling process on the next system boot:
+
[literal,subs="+quotes,attributes"]
----
# *touch /.autorelabel*
----

. Exit the `chroot` environment:
+
[literal,subs="+quotes,attributes"]
----
# *exit*
----

. Exit the `switch_root` prompt, to reboot the system:
+
[literal,subs="+quotes,attributes"]
----
*exit*
----

. Wait until the SELinux relabeling process is finished. Note that relabeling a large disk can take a long time. The system reboots automatically when the process is complete.

.Verification
. Log in as the `root` user by using the new root  password.

. Optional: Display the user name associated with the current effective user ID:
+
[literal,subs="+quotes,attributes"]
----
# *whoami*
----


