:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_implementation-of-ntp.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.

[id="implementation-of-ntp_{context}"]
= Implementation of NTP

RHEL 7 supported two implementations of the `NTP` protocol: [application]*ntp* and [application]*chrony*.

In RHEL 8, the `NTP` protocol is implemented only by the `chronyd` daemon,  provided by the [package]`chrony` package.

The `ntp` daemon is no longer available. If you used `ntp` on your RHEL 7 system, you might need to link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/configuring_basic_system_settings/index#proc_migrating-to-chrony_using-chrony-to-configure-ntp[migrate to chrony].

Possible replacements for previous [application]*ntp* features that are not supported by [application]*chrony* are documented in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/configuring-time-synchronization_configuring-basic-system-settings#assembly_achieving-some-settings-previously-supported-by-ntp-in-chrony_configuring-time-synchronization[Achieving some settings previously supported by ntp in chrony].
