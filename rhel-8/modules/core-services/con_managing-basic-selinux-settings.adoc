:_mod-docs-content-type: CONCEPT
[id="con_managing-basic-selinux-settings_{context}"]
= Managing basic SELinux settings

[role="_abstract"]
Security-Enhanced Linux (SELinux) is an additional layer of system security that determines which processes can access which files, directories, and ports. These permissions are defined in SELinux policies. A policy is a set of rules that guide the SELinux security engine.

//Removed Duplicate Content

SELinux has two possible states:

* Disabled
* Enabled

When SELinux is enabled, it runs in one of the following modes:

* Enabled
** Enforcing
** Permissive

In *enforcing mode*, SELinux enforces the loaded policies. SELinux denies access based on SELinux policy rules and enables only the interactions that are explicitly allowed. Enforcing mode is the safest SELinux mode and is the default mode after installation.

In *permissive mode*, SELinux does not enforce the loaded policies. SELinux does not deny access, but reports actions that break the rules to the `/var/log/audit/audit.log` log. Permissive mode is the default mode during installation. Permissive mode is also useful in some specific cases, for example when troubleshooting problems.


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/[Using SELinux]
