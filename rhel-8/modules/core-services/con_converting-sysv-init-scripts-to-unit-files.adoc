:_mod-docs-content-type: CONCEPT
[id="con_converting-sysv-init-scripts-to-unit-files_{context}"]
= Converting SysV init scripts to unit files

[role="_abstract"]
Before taking time to convert a SysV init script to a unit file, make sure that the conversion was not already done elsewhere. All core services installed on {RHEL} come with default unit files, and the same applies for many third-party software packages.

To convert an init script to a unit file, you must analyze the script and extract the necessary information from it. Because init scripts can vary depending on the type of the service, you might need to employ more configuration options for translation.

[NOTE]
====
Certain levels of customization that were available with init scripts are no longer supported by systemd units.
====

The majority of information needed for conversion is provided in the script's header. The following example shows the opening section of the init script used to start the `postfix` service on {RHEL} 6:

[literal,subs="none"]
----
#!/bin/bash
# postfix      Postfix Mail Transfer Agent
# chkconfig: 2345 80 30
# description: Postfix is a Mail Transport Agent, which is the program that moves mail from one machine to another.
# processname: master
# pidfile: /var/spool/postfix/pid/master.pid
# config: /etc/postfix/main.cf
# config: /etc/postfix/master.cf
### BEGIN INIT INFO
# Provides: postfix MTA
# Required-Start: $local_fs $network $remote_fs
# Required-Stop: $local_fs $network $remote_fs
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: start and stop postfix
# Description: Postfix is a Mail Transport Agent, which is the program that moves mail from one machine to another.
### END INIT INFO
----

In the above example, only lines starting with *# chkconfig* and *# description* are mandatory, so you might not find the rest in different init files. The text enclosed between the *BEGIN INIT INFO* and *END INIT INFO* lines is called *Linux Standard Base (LSB) header*. If specified, LSB headers contain directives defining the service description, dependencies, and default runlevels. What follows is an overview of analytic tasks aiming to collect the data needed for a new unit file. The postfix init script is used as an example.
