:_mod-docs-content-type: PROCEDURE
[id="edit-a-users-language-with-the-users-settings-tools_{context}"]
= Editing a user's language with the Users settings tools

.Prerequisites

* Open the [application]*Users* settings tool as described in xref:opening-the-users-settings-tool_managing-users-in-a-graphical-environment[].

.Procedure

* To edit a user's language setting, select the language and a drop-down menu appears.


