:_mod-docs-content-type: PROCEDURE
[id="changing-file-permissions-using-symbolic-values_{context}"]

= Changing file permissions using symbolic values

[role="_abstract"]
You can use the `chmod` utility with symbolic values (a combination of letters and signs) to change file permissions for a file or directory.

[[permissions]]
You can assign the following _permissions_:

* Read (*r*)
* Write (*w*)
* Execute (*x*)

[[ownership]]
Permissions can be assigned to the following _levels of ownership_:

* User owner (*u*)
* Group owner (*g*)
* Other (*o*)
* All (*a*)

[[signs]]
To add or remove permissions you can use the following _signs_:

* `+` to add the permissions on top of the existing permissions
* `-` to remove the permissions from the existing permission
* `=` to remove the existing permissions and explicitly define the new ones

.Procedure

* To change the permissions for a file or directory, use:
+
[literal,subs="+quotes,attributes"]
----
$ *chmod _<level><operation><permission> file-name_*
----
+
Replace `_<level>_` with the xref:ownership[level of ownership] you want to set the permissions for. Replace `_<operation>_` with one of the xref:signs[signs]. Replace `_<permission>_` with the xref:permissions[permissions] you want to assign. Replace _file-name_ with the name of the file or directory. For example, to grant everyone the permissions to read, write, and execute (`rwx`) `my-script.sh`, use the `chmod a=rwx my-script.sh` command.
+
See xref:base-permissions_assembly_managing-file-permissions[Base file permissions] for more details.

.Verification

* To see the permissions for a particular file, use:
+
[literal,subs="+quotes,attributes"]
----
$ *ls -l _file-name_*
----
Replace _file-name_ with the name of the file.

* To see the permissions for a particular directory, use:
+
[literal,subs="+quotes,attributes"]
----
$ *ls -dl _directory-name_*
----
Replace _directory-name_ with the name of the directory.

* To see the permissions for all the files within a particular directory, use:
+
[literal,subs="+quotes,attributes"]
----
$ *ls -l _directory-name_*
----
Replace _directory-name_ with the name of the directory.


.Changing permissions for files and directories
====
* To change file permissions for `my-file.txt` from `-rw-rw-r--` to `-rw------`, use:
+
. Display the current permissions for `my-file.txt`:
+
[literal,subs="+quotes,attributes"]
----
$ *ls -l my-file.txt*
-rw-rw-r--. 1 username username 0 Feb 24 17:56 my-file.txt
----
+
. Remove the permissions to read, write, and execute (`rwx`) the file from group owner (`g`) and others (`o`):
+
[literal,subs="+quotes,attributes"]
----
$ *chmod go= my-file.txt*
----
+
Note that any permission that is not specified after the equals sign (`=`) is automatically prohibited.
+
. Verify that the permissions for `my-file.txt` were set correctly:
+
[literal,subs="+quotes,attributes"]
----
$ *ls -l my-file.txt*
-rw-------. 1 username username 0 Feb 24 17:56 my-file.txt
----

* To change file permissions for `my-directory` from `drwxrwx---` to `drwxrwxr-x`, use:
+
. Display the current permissions for `my-directory`:
+
[literal,subs="+quotes,attributes"]
----
$ *ls -dl my-directory*
drwxrwx---. 2 username username 4096 Feb 24 18:12 my-directory
----
+
. Add the read and execute (`r-x`) access for all users (`a`):
+
[literal,subs="+quotes,attributes"]
----
$ *chmod o+rx my-directory*
----
+
. Verify that the permissions for `my-directory` and its content were set correctly:
+
[literal,subs="+quotes,attributes"]
----
$ *ls -dl my-directory*
drwxrwxr-x. 2 username username 4096 Feb 24 18:12 my-directory
----
====
