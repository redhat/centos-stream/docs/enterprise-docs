:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_the-different-samba-id-mapping-back-ends.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_using-the-rid-id-mapping-back-end.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_using-the-rid-id-mapping-back-end_{context}"]
= Using the rid ID mapping back end

You can configure a Samba domain member to use the `rid` ID mapping back end.

Samba can use the relative identifier (RID) of a Windows SID to generate an ID on {RHEL}.

[NOTE]
====
The RID is the last part of a SID. For example, if the SID of a user is `S-1-5-21-5421822485-1151247151-421485315-30014`, then `30014` is the corresponding RID.
====

The `rid` ID mapping back end implements a read-only API to calculate account and group information based on an algorithmic mapping scheme for AD and NT4 domains. When you configure the back end, you must set the lowest and highest RID in the `idmap config _DOMAIN_ : _range_` parameter. Samba will not map users or groups with a lower or higher RID than set in this parameter.

[IMPORTANT]
====
As a read-only back end, `rid` cannot assign new IDs, such as for `BUILTIN` groups. Therefore, do not use this back end for the `*` default domain.
====

.Benefits of using the rid back end

* All domain users and groups that have an RID within the configured range are automatically available on the domain member.
* You do not need to manually assign IDs, home directories, and login shells.

.Drawbacks of using the rid back end

* All domain users get the same login shell and home directory assigned. However, you can use variables.
* User and group IDs are only the same across Samba domain members if all use the `rid` back end with the same ID range settings.
* You cannot exclude individual users or groups from being available on the domain member. Only users and groups outside of the configured range are excluded.
* Based on the formulas the `winbindd` service uses to calculate the IDs, duplicate IDs can occur in multi-domain environments if objects in different domains have the same RID.


.Prerequisites

* You installed Samba.
* The Samba configuration, except ID mapping, exists in the [filename]`/etc/samba/smb.conf` file.


.Procedure

. Edit the `[global]` section in the [filename]`/etc/samba/smb.conf` file:

.. Add an ID mapping configuration for the default domain (`*`) if it does not exist. For example:
+
[literal,subs="+quotes,attributes"]
----
idmap config * : backend = tdb
idmap config * : range = _10000-999999_
----

.. Enable the `rid` ID mapping back end for the domain:
+
[literal,subs="+quotes,attributes"]
----
idmap config _DOMAIN_ : backend = rid
----

.. Set a range that is big enough to include all RIDs that will be assigned in the future. For example:
+
[literal,subs="+quotes,attributes"]
----
idmap config _DOMAIN_ : range = _2000000-2999999_
----
+
Samba ignores users and groups whose RIDs in this domain are not within the range.
+
[IMPORTANT]
====
The range must not overlap with any other domain configuration on this server. Additionally, the range must be set big enough to include all IDs assigned in the future.
ifeval::[{ProductNumber} == 8]
ifdef::differentserver-title[]
For further details, see xref:con_planning-samba-id-ranges_assembly_understanding-and-configuring-samba-id-mapping[Planning Samba ID ranges].
endif::[]
ifndef::differentserver-title[]
For further details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/assembly_using-samba-as-a-server_deploying-different-types-of-servers#con_planning-samba-id-ranges_assembly_understanding-and-configuring-samba-id-mapping[Planning Samba ID ranges].
endif::[]
endif::[]
ifeval::[{ProductNumber} == 9]
ifdef::network-file-services[]
For further details, see xref:con_planning-samba-id-ranges_assembly_understanding-and-configuring-samba-id-mapping[Planning Samba ID ranges].
endif::[]
ifndef::network-file-services[]
For further details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring-and-using-network-file-services/assembly_using-samba-as-a-server_configuring-and-using-network-file-services#con_planning-samba-id-ranges_assembly_understanding-and-configuring-samba-id-mapping[Planning Samba ID ranges].
endif::[]
endif::[]
====

.. Set a shell and home directory path that will be assigned to all mapped users. For example:
+
[literal,subs="+quotes,attributes"]
----
template shell = _/bin/bash_
template homedir = _/home/%U_
----

. Verify the [filename]`/etc/samba/smb.conf` file:
+
[literal,subs="+quotes,attributes"]
----
# *testparm*
----

. Reload the Samba configuration:
+
[literal,subs="+quotes,attributes"]
----
# *smbcontrol all reload-config*
----



[role="_additional-resources"]
.Additional resources
//ifdef::differentserver-title[]
* xref:con_the-asterisk-default-domain_assembly_understanding-and-configuring-samba-id-mapping[The * default domain]
//endif::[]
//ifndef::differentserver-title[]
//* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/assembly_using-samba-as-a-server_deploying-different-types-of-servers#con_the-asterisk-default-domain_assembly_understanding-and-configuring-samba-id-mapping[The * default domain]
//endif::[]
* `VARIABLE SUBSTITUTIONS` section in the `smb.conf(5)` man page on your system
* Calculation of the local ID from a RID, see the `idmap_rid(8)` man page on your system
