:_mod-docs-content-type: REFERENCE
[id="unit-files-locations_{context}"]
= Systemd unit files locations

[role="_abstract"]
You can find the unit configuration files in one of the following directories:

[[tabl-introduction-to-systemd]]
.systemd unit files locations
[options="header"]
|===
|Directory|Description
|`/usr/lib/systemd/system/`|`systemd` unit files distributed with installed RPM packages.
|`/run/systemd/system/`|`systemd` unit files created at run time. This directory takes precedence over the directory with installed service unit files.
|`/etc/systemd/system/`|`systemd` unit files created by using the `systemctl enable` command as well as unit files added for extending a service. This directory takes precedence over the directory with runtime unit files.
|===

The default configuration of `systemd` is defined during the compilation and you can find the configuration in the `/etc/systemd/system.conf` file. By editing this file, you can modify the default configuration by overriding values for `systemd` units globally.
====
For example, to override the default value of the timeout limit, which is set to 90 seconds, use the `DefaultTimeoutStartSec` parameter to input the required value in seconds.
[subs="+quotes,attributes"]
----
DefaultTimeoutStartSec=_required value_
----
====
