:_mod-docs-content-type: PROCEDURE
[id="enabling-dnf-automatic_{context}"]
= Enabling DNF Automatic

[role="_abstract"]

To run *DNF Automatic* once, you must start a systemd timer unit. However, if you want to run *DNF Automatic* periodically, you must enable the timer unit. You can use one of the timer units provided in the `dnf-automatic` package, or you can create a drop-in file for the timer unit to adjust the execution time. 


.Prerequisites
* You specified the behavior of *DNF Automatic* by modifying the `/etc/dnf/automatic.conf` configuration file.

.Procedure

* Enable and execute a systemd timer unit immediately:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl enable --now _<timer_name>_*
----
+
If you want to only enable the timer without executing it immediately, omit the `--now` option. 
+
You can use the following timers:

** `dnf-automatic-download.timer`: Downloads available updates.
** `dnf-automatic-install.timer`: Downloads and installs available updates.
** `dnf-automatic-notifyonly.timer`: Reports available updates.
** `dnf-automatic.timer`: Downloads, downloads and installs, or reports available updates. 


.Verification

* Verify that the timer is enabled:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl status _<timer_name>_*
----

* Optional: Check when each of the timers on your system ran the last time: 
+
[literal,subs="+quotes,attributes"]
----
# *systemctl list-timers --all*
----

[role="_additional-resources"]
.Additional resources
* `dnf-automatic(8)` man page on your system 
* xref:overview-of-the-systemd-timer-units-included-in-the-dnf-automatic-package_automating-software-updates[Overview of the systemd timer units included in the dnf-automatic package]

