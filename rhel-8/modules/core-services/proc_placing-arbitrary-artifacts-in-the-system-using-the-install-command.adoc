:_mod-docs-content-type: PROCEDURE

[id="placing-arbitrary-artifacts-in-the-system-using-the-install-command_{context}"]
= Installing the `bello` file into the system by using the `install` command

[role="_abstract"]
The `install` command, provided by the link:http://www.gnu.org/software/coreutils/coreutils.html[GNU Core Utilities] (Coreutils), places files into the specified directory in the file system with a specified set of permissions.

Packagers often use the [command]`install` command in SPEC files in cases when build automation tooling, such as link:http://www.gnu.org/software/make/[GNU make], is not optimal, for example, if the packaged program is too simple to warrant the use of more advanced tooling.


// Note that you will either need link:http://www.sudo.ws/[sudo] permissions or run this command as root excluding the ``sudo`` portion of the command.

.Procedure

. Install the [filename]`bello` file into the [filename]`/usr/bin` directory with permissions common for executable scripts:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ **sudo install -m 0755 bello /usr/bin/bello**
....
+
This command places [filename]`bello` in the directory listed in the
ifdef::community[https://en.wikipedia.org/wiki/PATH_%28variable%29[$PATH]]
ifdef::rhel[[variable]`$PATH`]
variable.

. Run [filename]`bello` from any directory without specifying its full path:
+
[literal,subs="+quotes,verbatim,macros,normal"]
....
$ **cd ~**

$ **bello**
Hello World
....
