:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_dynamic-programming-languages.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
[id="dynamic-programming-languages_{context}"]
= Dynamic programming languages

[id="python_{context}"]
== Notable changes in Python


[id="python-3-default_{context}"]
=== `Python 3` is the default `Python` implementation in RHEL 8

Red Hat Enterprise Linux 8 is distributed with several versions of `Python 3`. `Python 3.6` is going to be supported for the whole life cycle of RHEL 8. The respective package might not be installed by default.

`Python 2.7` is available in the `python2` package. However, `Python 2` will have a shorter life cycle and its aim is to facilitate a smoother transition to `Python 3` for customers.

For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/configuring_basic_system_settings/index#con_python-versions_assembly_introduction-to-python[Python versions].

Neither the default `python` package nor the unversioned `/usr/bin/python` executable is distributed with RHEL 8. Customers are advised to use `python3` or `python2` directly. Alternatively, administrators can configure the unversioned `python` command using the `alternatives` command. See xref:assembly_configuring-the-unversioned-python_{context}[Configuring the unversioned Python].


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/installing-and-using-dynamic-programming-languages_configuring-basic-system-settings#assembly_installing-and-using-python_installing-and-using-dynamic-programming-languages[Installing and using Python]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/installing-and-using-dynamic-programming-languages_configuring-basic-system-settings#assembly_packaging-python-3-rpms_installing-and-using-dynamic-programming-languages[Packaging Python 3 RPMs]


//https://bugzilla.redhat.com/show_bug.cgi?id=1580387

include::ref_migrating-from-python-2-to-python-3.adoc[leveloffset=+2]

include::../../../assemblies/assembly_configuring-the-unversioned-python.adoc[leveloffset=+2]

include::../../../assemblies/assembly_handling-interpreter-directives-in-python-scripts.adoc[leveloffset=+2]

//https://bugzilla.redhat.com/show_bug.cgi?id=1583620


[id="python-binding-net-snmp_{context}"]
=== `Python` binding of the `net-snmp` package is unavailable

The `Net-SNMP` suite of tools does not provide binding for `Python 3`, which is the default `Python` implementation in RHEL 8. Consequently, `python-net-snmp`, `python2-net-snmp`, or `python3-net-snmp` packages are unavailable in RHEL 8.
//https://bugzilla.redhat.com/show_bug.cgi?id=1584510




[id="php_{context}"]
== Notable changes in `PHP`

Red Hat Enterprise Linux 8 is distributed with `PHP 7.2`. This version introduces the following major changes over `PHP 5.4`, which is available in RHEL 7:

* `PHP` uses FastCGI Process Manager (FPM) by default (safe for use with a threaded `httpd`)
* The `php_value` and `php-flag` variables should no longer be used in the `httpd` configuration files; they should be set in pool configuration instead: `/etc/php-fpm.d/*.conf`
* `PHP` script errors and warnings are logged to the `/var/log/php-fpm/www-error.log` file instead of `/var/log/httpd/error.log`
* When changing the PHP `max_execution_time` configuration variable, the `httpd` `ProxyTimeout` setting should be increased to match
* The user running `PHP` scripts is now configured in the FPM pool configuration (the `/etc/php-fpm.d/www.conf` file; the `apache` user is the default)
* The `php-fpm` service needs to be restarted after a configuration change or after a new extension is installed
* The `zip` extension has been moved from the `php-common` package to a separate package, `php-pecl-zip`

The following extensions have been removed:

* `aspell`
* `mysql` (note that the `mysqli` and `pdo_mysql` extensions are still available, provided by `php-mysqlnd` package)
* `memcache`
//https://bugzilla.redhat.com/show_bug.cgi?id=1580430



[id="perl_{context}"]
== Notable changes in `Perl`

`Perl 5.26`, distributed with RHEL 8, introduces the following changes over the version available in RHEL 7:

* `Unicode 9.0` is now supported.
* New `op-entry`, `loading-file`, and `loaded-file` `SystemTap` probes are provided.
* Copy-on-write mechanism is used when assigning scalars for improved performance.
* The `IO::Socket::IP` module for handling IPv4 and IPv6 sockets transparently has been added.
* The `Config::Perl::V` module to access `perl -V` data in a structured way has been added.
* A new `perl-App-cpanminus` package has been added, which contains the `cpanm` utility for getting, extracting, building, and installing modules from the Comprehensive Perl Archive Network (CPAN) repository.
* The current directory `.` has been removed from the `@INC` module search path for security reasons.
* The `do` statement now returns a deprecation warning when it fails to load a file because of the behavioral change described above.
* The `do subroutine(LIST)` call is no longer supported and results in a syntax error.
* Hashes are randomized by default now. The order in which keys and values are returned from a hash changes on each `perl` run. To disable the randomization, set the `PERL_PERTURB_KEYS` environment variable to `0`.
* Unescaped literal `{` characters in regular expression patterns are no longer permissible.
* Lexical scope support for the `$_` variable has been removed.
* Using the `defined` operator on an array or a hash results in a fatal error.
* Importing functions from the `UNIVERSAL` module results in a fatal error.
* The `find2perl`, `s2p`, `a2p`, `c2ph`, and `pstruct` tools have been removed.
* The `${^ENCODING}` facility has been removed. The `encoding` pragma's default mode is no longer supported. To write source code in other encoding than `UTF-8`, use the encoding's `Filter` option.
* The `perl` packaging is now aligned with upstream. The `perl` package installs also core modules and is suitable for development. On production systems, use the `perl-interpreter` package, which contains the main `/usr/bin/perl` interpreter.  In previous releases, the `perl` package included just a minimal interpreter, whereas the `perl-core` package included both the interpreter and the core modules.
*  The `IO::Socket::SSL` Perl module no longer loads a certificate authority certificate from the `./certs/my-ca.pem` file or the `./ca` directory, a server private key from the `./certs/server-key.pem` file, a server certificate from the `./certs/server-cert.pem` file, a client private key from the `./certs/client-key.pem` file, and a client certificate from the `./certs/client-cert.pem` file. Specify the paths to the files explicitly instead.

//https://bugzilla.redhat.com/show_bug.cgi?id=1511131
//https://issues.redhat.com/browse/RHELDOCS-18868



[id="ruby_{context}"]
== Notable changes in `Ruby`

RHEL 8 provides `Ruby 2.5`, which introduces numerous new features and enhancements over `Ruby 2.0.0` available in RHEL 7. Notable changes include:

* Incremental garbage collector has been added.
* The `Refinements` syntax has been added.
* Symbols are now garbage collected.
* The `$SAFE=2` and `$SAFE=3` safe levels are now obsolete.
* The `Fixnum` and `Bignum` classes have been unified into the `Integer` class.
* Performance has been improved by optimizing the `Hash` class, improved access to instance variables, and the `Mutex` class being smaller and faster.
* Certain old APIs have been deprecated.
* Bundled libraries, such as `RubyGems`, `Rake`, `RDoc`, `Psych`, `Minitest`, and `test-unit`, have been updated.
* Other libraries, such as `mathn`, `DL`, `ext/tk`, and `XMLRPC`, which were previously distributed with `Ruby`, are deprecated or no longer included.
* The `SemVer` versioning scheme is now used for `Ruby` versioning.
//https://bugzilla.redhat.com/show_bug.cgi?id=1648843




[id="swig_{context}"]
== Notable changes in `SWIG`

RHEL 8 includes the Simplified Wrapper and Interface Generator (SWIG) version 3.0, which provides numerous new features, enhancements, and bug fixes over the version 2.0 distributed in RHEL 7. Most notably, support for the C++11 standard has been implemented. `SWIG` now supports also `Go 1.6`, `PHP 7`, `Octave 4.2`, and `Python 3.5`.

//https://bugzilla.redhat.com/show_bug.cgi?id=1660051



[id="nodejs_{context}"]
== `Node.js` new in RHEL

`Node.js`, a software development platform for building fast and scalable network applications in the JavaScript programming language, is provided for the first time in RHEL. It was previously available only as a Software Collection. RHEL 8 provides `Node.js 10`.
//https://bugzilla.redhat.com/show_bug.cgi?id=1622118


