:_mod-docs-content-type: PROCEDURE

[id="configuring-tls-encryption-on-a-dovecot-server_{context}"]
= Configuring TLS encryption on a Dovecot server

Dovecot provides a secure default configuration. For example, TLS is enabled by default to transmit credentials and data encrypted over networks. To configure TLS on a Dovecot server, you only need to set the paths to the certificate and private key files. Additionally, you can increase the security of TLS connections by generating and using Diffie-Hellman parameters to provide perfect forward secrecy (PFS).


.Prerequisites

* Dovecot is installed.

* The following files have been copied to the listed locations on the server:
** The server certificate: `/etc/pki/dovecot/certs/server.example.com.crt`
** The private key: `/etc/pki/dovecot/private/server.example.com.key`
** The Certificate Authority (CA) certificate: `/etc/pki/dovecot/certs/ca.crt`

* The hostname in the `Subject DN` field of the server certificate matches the server's Fully-qualified Domain Name (FQDN).

ifeval::[{ProductNumber} >= 9]
* If the server runs RHEL 9.2 or later and the FIPS mode is enabled, clients must either support the Extended Master Secret (EMS) extension or use TLS 1.3. TLS 1.2 connections without EMS fail. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/7018256[TLS extension "Extended Master Secret" enforced].
endif::[]

.Procedure

. Set secure permissions on the private key file:
+
[literal,subs="+quotes"]
....
# **chown root:root /etc/pki/dovecot/private/server.example.com.key**
# **chmod 600 /etc/pki/dovecot/private/server.example.com.key**
....

. Generate a file with Diffie-Hellman parameters:
+
[literal,subs="+quotes"]
....
# **openssl dhparam -out __/etc/dovecot/dh.pem__ __4096__**
....
+
Depending on the hardware and entropy on the server, generating Diffie-Hellman parameters with 4096 bits can take several minutes.

. Set the paths to the certificate and private key files in the `/etc/dovecot/conf.d/10-ssl.conf` file:

.. Update the `ssl_cert` and `ssl_key` parameters, and set them to use the paths of the server's certificate and private key:
+
[literal,subs="+quotes"]
....
**ssl_cert = <__/etc/pki/dovecot/certs/server.example.com.crt__**
**ssl_key = <__/etc/pki/dovecot/private/server.example.com.key__**
....

.. Uncomment the `ssl_ca` parameter, and set it to use the path to the CA certificate:
+
[literal,subs="+quotes"]
....
**ssl_ca = <__/etc/pki/dovecot/certs/ca.crt__**
....

.. Uncomment the `ssl_dh` parameter, and set it to use the path to the Diffie-Hellman parameters file:
+
[literal,subs="+quotes"]
....
**ssl_dh = <__/etc/dovecot/dh.pem__**
....

+
[IMPORTANT]
====
To ensure that Dovecot reads the value of a parameter from a file, the path must start with a leading `<` character.
====


.Next step
* xref:preparing-dovecot-to-use-virtual-users_{context}[Preparing Dovecot to use virtual users]


[role="_additional-resources"]
.Additional resources
* `/usr/share/doc/dovecot/wiki/SSL.DovecotConfiguration.txt`

