:_mod-docs-content-type: PROCEDURE

[id="setting-the-umask-using-symbolic-values_{context}"]

= Setting the umask using symbolic values

[role="_abstract"]
You can use the [command]`umask` utility with symbolic values (a combination letters and signs) to set the _umask_ for the current shell session

[[permissions1]]
You can assign the following _permissions_:

* Read (*r*)
* Write (*w*)
* Execute (*x*)

[[ownership1]]
Permissions can be assigned to the following _levels of ownership_:

* User owner (*u*)
* Group owner (*g*)
* Other (*o*)
* All (*a*)

[[signs1]]
To add or remove permissions you can use the following _signs_:

* `+` to add the permissions on top of the existing permissions
* `-` to remove the permissions from the existing permission
* `=` to remove the existing permissions and explicitly define the new ones
+
NOTE: Any permission that is not specified after the equals sign (`=`) is automatically prohibited.

.Procedure
* To set the _umask_ for the current shell session, use:
+
[literal,subs="+quotes,attributes"]
----
$ *umask -S _<level><operation><permission>_*
----
+
Replace `_<level>_` with the xref:ownership1[level of ownership] you want to set the umask for. Replace `_<operation>_` with one of the xref:signs1[signs]. Replace `_<permission>_` with the xref:permissions1[permissions] you want to assign. For example, to set the _umask_ to `u=rwx,g=rwx,o=rwx`, use `umask -S a=rwx`.
+
See xref:user-file-creation-mode-mask_assembly_managing-file-permissions[User file-creation mode] for more details.
+
NOTE: The _umask_ is only valid for the current shell session.

////
+
.Changing the umask for the current shell session
====
* To set the _umask_ to `u=rwx,g=rwx,o=rwx`, use:
+
----
$ umask -S a=rwx
----
====
////
////
* To set the same permissions for user, group, and others, use:
+
[subs=+quotes]
----
$ umask a=_symbolic_value_
----
Replace _symbolic_value_ with a symbolic value. See xref:user-file-creation-mode-mask_assembly_intro-to-file-permissions[User file-creation mode] for more details.
+
.Changing the umask to enable all permissions
====
To set the _umask_ to `000`  (`u=rwx,g=rwx,o=rwx`), use:
----
$ umask a=rwx
----

Note that the _umask_ is only valid for the current shell session.
====
////
