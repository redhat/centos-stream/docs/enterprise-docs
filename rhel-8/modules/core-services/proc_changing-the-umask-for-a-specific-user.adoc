:_mod-docs-content-type: PROCEDURE
[id="changing-the-umask-for-a-specific-user_{context}"]

= Changing the default umask for a specific user

[role="_abstract"]
You can change the default _umask_ for a specific user by modifying the `.bashrc` for that user.

.Procedure
* Append the line that specifies the octal value of the _umask_ into the `.bashrc` file for the particular user.
+
[literal,subs="+quotes,attributes"]
----
$ *echo 'umask _octal_value_' >> /home/_username_/.bashrc*
----
+
Replace _octal_value_ with an octal value and replace _username_ with the name of the user. See xref:user-file-creation-mode-mask_assembly_managing-file-permissions[User file-creation mode mask] for more details.
//Changes take effect after next login.
