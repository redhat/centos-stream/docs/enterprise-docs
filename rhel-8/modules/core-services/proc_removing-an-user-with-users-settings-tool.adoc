:_mod-docs-content-type: PROCEDURE

:experimental:

[id="removing-an-user-with-users-settings-tool_{context}"]
= Removing a user with Users settings tool

.Prerequisites

* Open the [application]*Users* settings tool as described in xref:opening-the-users-settings-tool_managing-users-in-a-graphical-environment[].

.Procedure

* To remove a user, select the btn:[-] button.

