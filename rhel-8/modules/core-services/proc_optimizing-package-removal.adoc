:_newdoc-version: 2.18.3
:_template-generated: 2024-10-16
:_mod-docs-content-type: PROCEDURE

[id="optimizing-package-removal_{context}"]
= Specifying package details for removal

You can specify package details for a precise package removal process. To do so, append the following suffixes to the `{PackageManagerCommand} remove` command to explicitly define how to parse an argument:

* Use `-n` to specify the exact name of the package.
* Use `-na` to specify the exact package name and architecture.
* Use `-nevra` to specify the exact package name, epoch, version, release, and architecture.

.Procedure

* Depending on your scenario, use one of the following options to optimize the package removal process: 

** To remove a package by using its exact name, enter: 
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} remove-n _<package_name>_*
----

** To remove a package by using its exact name and architecture, enter:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} remove-na _<package_name>.<architecture>_*
----

** To remove a package by using its exact name, epoch, version, release, and architecture, enter:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} remove-nevra _<package_name>-<epoch>:<version>-<release>.<architecture>_*
----