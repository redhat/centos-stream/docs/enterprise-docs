:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_working-with-apache-modules.adoc

[id="compiling-a-custom-apache-module_{context}"]
= Compiling a custom Apache module

[role="_abstract"]
You can create your own module and build it with the help of the [package]`httpd-devel` package, which contains the include files, the header files, and the [application]`*APache eXtenSion*` ([command]`apxs`) utility required to compile a module.

.Prerequisites

* You have the [package]`httpd-devel` package installed.

.Procedure

* Build a custom module with the following command:
+
[literal,subs="quotes,attributes"]
----
# *apxs -i -a -c module_name.c*
----

.Verification

* Load the module the same way as described in xref:loading-a-dso-module_working-with-apache-modules[Loading a DSO module].
