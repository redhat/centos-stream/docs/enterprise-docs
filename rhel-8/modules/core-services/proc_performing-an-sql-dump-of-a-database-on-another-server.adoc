:_newdoc-version: 2.18.3
:_template-generated: 2024-12-13
:_mod-docs-content-type: PROCEDURE

[id="performing-an-sql-dump-of-a-database-on-another-server_{context}"]
= Performing an SQL dump of a database on another server

Dumping a database directly from one server to another is possible because [application]*pg_dump* and [application]*psql* can write to and read from pipes.

.Procedure
* To dump a database from one server to another, run:
+
[literal,subs="+quotes"]
----
$ *pg_dump -h host1 dbname | psql -h host2 dbname*
----

