:_mod-docs-content-type: PROCEDURE
[id="changing-the-default-bash-umask_{context}"]
= Changing the default bash umask

To change the default `umask` for `bash`, use this procedure.

.Procedure

* Change the [command]`umask` command call or the `UMASK` variable assignment in [filename]`/etc/bashrc` to the required value of _umask_. This example changes the default _umask_ to `0227`: 
+
[literal,subs="+quotes,verbatim,normal,macros"]
....
    if [ $UID -gt 199 ] && [ "`id -gn`" = "`id -un`" ]; then
       umask 002
    else
       pass:quotes[*umask 227*]
....



