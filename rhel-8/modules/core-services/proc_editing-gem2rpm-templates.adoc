
:_mod-docs-content-type: PROCEDURE

[id="proc_editing-gem2rpm-templates_{context}"]
= Editing gem2rpm templates

[role="_abstract"]
You can edit the template from which the RPM `spec` file is generated instead of editing the generated `spec` file.

Use the following procedure to edit the `gem2rpm` templates.

.Procedure

. Save the default template:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
$ *gem2rpm -T > rubygem-<gem_name>.spec.template*
....

. Edit the template as needed.

. Generate the `spec` file by using the edited template:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
$ *gem2rpm -t rubygem-<gem_name>.spec.template <gem_name>-<latest_version.gem > <gem_name>-GEM.spec*
....


You can now build an RPM package by using the edited template as described in
ifdef::packaging-title[]
xref:building-rpms_packaging-software[Building RPMs].
endif::[]
ifndef::packaging-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/packaging_and_distributing_software/packaging-software_packaging-and-distributing-software#con_building-rpms_packaging-software[Building RPMs].
endif::[]


// The resulting RPMs should follow the naming convention 'rubygem-$GEM' where GEM is the name of the packaged gem
