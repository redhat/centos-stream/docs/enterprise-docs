:_mod-docs-content-type: PROCEDURE
[id="configuring-mysql_{context}"]
= Configuring MySQL

[role="_abstract"]
To configure the *MySQL* server for networking, use the following procedure.

.Procedure

1. Edit the `[mysqld]` section of the [filename]`/etc/my.cnf.d/mysql-server.cnf` file. You can set the following configuration directives:
+
* `bind-address` - is the address on which the server listens. Possible options are:
** a host name
** an IPv4 address
** an IPv6 address
+
* `skip-networking` - controls whether the server listens for TCP/IP connections. Possible values are:
** 0 - to listen for all clients
** 1 - to listen for local clients only
+
* `port` - the port on which *MySQL* listens for TCP/IP connections.

2. Restart the `mysqld` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl restart mysqld.service*
----
