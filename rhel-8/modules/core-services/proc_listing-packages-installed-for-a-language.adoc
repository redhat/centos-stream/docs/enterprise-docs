:_mod-docs-content-type: PROCEDURE
[id="listing-packages-installed-for-a-language_{context}"]
= Listing packages installed for a language

To list what packages get installed for any language, use the following procedure:

.Procedure

* Execute the following command:
+
[literal,subs="+quotes,verbatim,normal"]
....
# **{PackageManagerCommand} repoquery --whatsupplements langpacks-<locale_code>**
....
