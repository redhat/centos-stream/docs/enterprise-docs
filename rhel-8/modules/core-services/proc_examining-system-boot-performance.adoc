:_mod-docs-content-type: PROCEDURE
[id="proc_examining-system-boot-performance_{context}"]
= Examining system boot performance

[role="_abstract"]
To examine system boot performance, you can use the [command]`systemd-analyze` command. By using certain options, you can tune systemd to shorten the boot time. 


.Prerequisites

* Optional: Before you examine `systemd` to tune the boot time, list all enabled services:
+
[literal,subs="+quotes,attributes"]
----
$ *systemctl list-unit-files --state=enabled*
----

.Procedure

Choose the information you want to analyze:

* Analyze the information about the time that the last successful boot took:
+
[literal,subs="+quotes,attributes"]
----
$ *systemd-analyze*
----
+

* Analyze the unit initialization time of each `systemd` unit:
+
[literal,subs="+quotes,attributes"]
----
$ *systemd-analyze blame*
----
+
The output lists the units in descending order according to the time they took to initialize during the last successful boot.
+

* Identify critical units that took the longest time to initialize at the last successful boot:
+
[literal,subs="+quotes,attributes"]
----
$ *systemd-analyze critical-chain*
----
+
The output highlights the units that critically slow down the boot with the red color.
+
.The output of the systemd-analyze critical-chain command
image::systemd-analyze-critical.png[]

[role="_additional-resources"]
.Additional resources
* `systemd-analyze (1)` man page on your system
