:_mod-docs-content-type: PROCEDURE
[id="proc_configuring-the-unversioned-python-command-interactively_{context}"]
= Configuring the unversioned python command to the required Python version interactively

[role="_abstract"]
You can configure the unversioned [command]`python` command to the required Python version interactively.

.Prerequisites

* Ensure that the required version of Python is installed.

.Procedure

. To configure the unversioned [command]`python` command interactively, use:

+
[literal,subs="+quotes,verbatim,normal,normal"]
----
# *alternatives --config python*
----

. Select the required version from the provided list.

. To reset this configuration and remove the unversioned [command]`python` command, use:

+
[literal,subs="+quotes,verbatim,normal,normal"]
----
# *alternatives --auto python*
----
