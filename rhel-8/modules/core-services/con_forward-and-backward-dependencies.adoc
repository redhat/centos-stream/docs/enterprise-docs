
:_mod-docs-content-type: CONCEPT


[id="con_forward-and-backward-dependencies_{context}"]
= Forward and Backward dependencies

`Forward dependencies` are, similarly to `Requires`, evaluated for packages that are being installed. The best of the matching packages are also installed.

In general, prefer `Forward dependencies`. Add the dependency to the package when getting the other package added to the system.

For `Backward dependencies`, the packages containing the dependency are installed if a matching package is installed as well.

`Backward dependencies` are mainly designed for third party vendors who can attach their plug-ins, add-ons, or extensions to distribution or other third party packages.

// Within Fedora the control over which packages a package requires should stay with the package maintainer. There are, however, cases when it is easier for the requiring package not needing to care about all add-ons. In this cases reverse dependencies may be used with the agreement of the package maintainer of the targeted package.

// Note, that EPEL or other third party repositories may have (and are encouraged to have) a different policy.
