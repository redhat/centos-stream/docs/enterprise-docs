:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_setting-permissions-on-a-share-that-uses-posix-acls.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_configuring-host-based-share-access.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_configuring-host-based-share-access_{context}"]
= Configuring host-based share access

Host-based access control enables you to grant or deny access to a share based on client's host names, IP addresses, or IP range.

The following procedure explains how to enable the `127.0.0.1` IP address, the `192.0.2.0/24` IP range, and the `client1.example.com` host to access a share, and additionally deny access for the `client2.example.com` host:


.Prerequisites

* The Samba share on which you want to set host-based access exists.


.Procedure

. Add the following parameters to the configuration of the share in the [filename]`/etc/samba/smb.conf` file:
+
[literal,subs="+quotes,attributes"]
----
hosts allow = 127.0.0.1 192.0.2.0/24 client1.example.com
hosts deny = client2.example.com
----
+
The `hosts deny` parameter has a higher priority than `hosts allow`. For example, if `client1.example.com` resolves to an IP address that is listed in the `hosts allow` parameter, access for this host is denied.

. Reload the Samba configuration:
+
[literal,subs="+quotes,attributes"]
----
# *smbcontrol all reload-config*
----


[role="_additional-resources"]
.Additional resources
* `smb.conf(5)` man page on your system
