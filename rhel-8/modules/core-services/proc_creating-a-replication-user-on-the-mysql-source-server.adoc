:_mod-docs-content-type: PROCEDURE

[id="proc_creating-a-replication-user-on-the-mysql-source-server_{context}"]
= Creating a replication user on the MySQL source server

[role="_abstract"]
You must create a replication user and grant this user permissions required for replication traffic. This procedure shows how to create a replication user with appropriate permissions. Execute these steps only on the source server.


.Prerequisites

* The source server is installed and configured as described in xref:proc_configuring-a-mysql-source-server_{context}[Configuring a MySQL source server].

.Procedure

. Create a replication user:
+
[literal,subs="+quotes"]
----
mysql> *CREATE USER '_replication_user'@'replica_server_hostname_' IDENTIFIED WITH mysql_native_password BY '_password_';*
----
+
. Grant the user replication permissions:
+
[literal,subs="+quotes,macros"]
----
mysql> *GRANT REPLICATION SLAVE ON pass:[*.*] TO '_replication_user_'@'_replica_server_hostname_';*
----
+
. Reload the grant tables in the *MySQL* database:
+
[literal,subs="+quotes"]
----
mysql> *FLUSH PRIVILEGES;*
----
+
. Set the source server to read-only state:
+
[literal,subs="+quotes"]
----
mysql> *SET @@GLOBAL.read_only = ON;*
----
