:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="exporting-a-private-key-and-certificates-from-an-nss-database-to-use-them-in-an-apache-web-server-configuration_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Exporting a private key and certificates from an NSS database to use them in an Apache web server configuration
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
ifeval::[{ProductNumber} == 8]
RHEL 8 no longer provides the [systemitem]`mod_nss` module for the Apache web server, and Red Hat recommends using the `mod_ssl` module. If you store your private key and certificates in a Network Security Services (NSS) database, for example, because you migrated the web server from RHEL 7 to RHEL 8, follow this procedure to extract the key and certificates in Privacy Enhanced Mail (PEM) format. You can then use the files in the `mod_ssl` configuration as described in xref:configuring-tls-encryption-on-an-apache-http-server_setting-apache-http-server[Configuring TLS encryption on an Apache HTTP server].

This procedure assumes that the NSS database is stored in [filename]`/etc/httpd/alias/` and that you store the exported private key and certificates in the [filename]`/etc/pki/tls/` directory.


.Prerequisites

* The private key, the certificate, and the certificate authority (CA) certificate are stored in an NSS database.


.Procedure

. List the certificates in the NSS database:
+
[literal,subs="+quotes,attributes"]
----
# *certutil -d /etc/httpd/alias/ -L*
Certificate Nickname           Trust Attributes
                               SSL,S/MIME,JAR/XPI

__Example CA__                     __C,,__
__Example Server Certificate__     __u,u,u__
----
+
You need the nicknames of the certificates in the next steps.

. To extract the private key, you must temporarily export the key to a PKCS #12 file:

.. Use the nickname of the certificate associated with the private key, to export the key to a PKCS #12 file:
+
[literal,subs="+quotes,attributes"]
----
# *pk12util -o /etc/pki/tls/private/export.p12 -d /etc/httpd/alias/ -n "Example Server Certificate"*
Enter password for PKCS12 file: __password__
Re-enter password: __password__
pk12util: PKCS12 EXPORT SUCCESSFUL
----
+
Note that you must set a password on the PKCS #12 file. You need this password in the next step.

.. Export the private key from the PKCS #12 file:
+
[literal,subs="+quotes,attributes"]
----
# *openssl pkcs12 -in /etc/pki/tls/private/export.p12 -out /etc/pki/tls/private/server.key -nocerts -nodes*
Enter Import Password: __password__
MAC verified OK
----

.. Delete the temporary PKCS #12 file:
+
[literal,subs="+quotes,attributes"]
----
# *rm /etc/pki/tls/private/export.p12*
----

. Set the permissions on [filename]`/etc/pki/tls/private/server.key` to ensure that only the [systemitem]`root` user can access this file:
+
[literal,subs="+quotes,attributes"]
----
# *chown root:root /etc/pki/tls/private/server.key*
# *chmod 0600 /etc/pki/tls/private/server.key*
----

. Use the nickname of the server certificate in the NSS database to export the CA certificate:
+
[literal,subs="+quotes,attributes"]
----
# *certutil -d /etc/httpd/alias/ -L -n "Example Server Certificate" -a -o /etc/pki/tls/certs/server.crt*
----

. Set the permissions on [filename]`/etc/pki/tls/certs/server.crt` to ensure that only the [systemitem]`root` user can access this file:
+
[literal,subs="+quotes,attributes"]
----
# *chown root:root /etc/pki/tls/certs/server.crt*
# *chmod 0600 /etc/pki/tls/certs/server.crt*
----

. Use the nickname of the CA certificate in the NSS database to export the CA certificate:
+
[literal,subs="+quotes,attributes"]
----
# *`certutil -d /etc/httpd/alias/ -L -n "__Example CA__" -a -o /etc/pki/tls/certs/ca.crt`*
----

. Follow xref:configuring-tls-encryption-on-an-apache-http-server_setting-apache-http-server[Configuring TLS encryption on an Apache HTTP server] to configure the Apache web server, and:
+
* Set the [parameter]`SSLCertificateKeyFile` parameter to [filename]`/etc/pki/tls/private/server.key`.
* Set the [parameter]`SSLCertificateFile` parameter to [filename]`/etc/pki/tls/certs/server.crt`.
* Set the [parameter]`SSLCACertificateFile` parameter to [filename]`/etc/pki/tls/certs/ca.crt`.


[role="_additional-resources"]
.Additional resources
* `certutil(1)`, `pk12util(1)`, and `pkcs12(1ssl)` man pages on your system
endif::[]

ifeval::[{ProductNumber} == 9]
Since RHEL 8 we no longer provide the [systemitem]`mod_nss` module for the Apache web server, and Red Hat recommends using the `mod_ssl` module. If you store your private key and certificates in a Network Security Services (NSS) database, follow this link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/setting-apache-http-server_deploying-different-types-of-servers#exporting-a-private-key-and-certificates-from-an-nss-database-to-use-them-in-an-apache-web-server-configuration_setting-apache-http-server[procedure to extract the key and certificates in Privacy Enhanced Mail (PEM) format].
endif::[]
