:_mod-docs-content-type: PROCEDURE
[id="monitoring-by-ntpq-and-ntpdc_{context}"]

= Monitoring by ntpq and ntpdc

[role="_abstract"]
`chronyd` cannot be monitored by the [application]*ntpq* and [application]*ntpdc* utilities from the [appplication]*ntp* distribution, because [application]*chrony* does not support the `NTP` modes 6 and 7. It supports a different protocol and [application]*chronyc* is the client implementation. For more information, see the `chronyc(1)` man page on your system.

// Removed based on sME comment from RHELPLAN-3793, repalced by the below
// To monitor `chronyd` with [application]*ntpq* or [application]*ntpdc*, you can:
//
// * use the tracking command to check if and how well the system clock is synchronized
//
// * use the [application]*ntpstat* utility, which supports [application]*chrony* and provides a similar output as it used to with `ntpd`


To monitor the status of the system clock sychronized by `chronyd`, you can:

* Use the tracking command
* Use the [application]*ntpstat* utility, which supports [application]*chrony* and provides a similar output as it used to with `ntpd`

[[exam-use-tracking-command]]
.Using the tracking command
====

[literal,subs="+quotes,verbatim,normal"]
....

$ *chronyc -n tracking*
Reference ID    : 0A051B0A (10.5.27.10)
Stratum         : 2
Ref time (UTC)  : Thu Mar 08 15:46:20 2018
System time     : 0.000000338 seconds slow of NTP time
Last offset     : +0.000339408 seconds
RMS offset      : 0.000339408 seconds
Frequency       : 2.968 ppm slow
Residual freq   : +0.001 ppm
Skew            : 3.336 ppm
Root delay      : 0.157559142 seconds
Root dispersion : 0.001339232 seconds
Update interval : 64.5 seconds
Leap status     : Normal

....

====


[[exam-use-ntpstat-utility]]
.Using the ntpstat utility
====

[literal,subs="+quotes,verbatim,normal"]
....

$ *ntpstat*
synchronised to NTP server (10.5.27.10) at stratum 2
   time correct to within 80 ms
   polling server every 64 s

....

====
