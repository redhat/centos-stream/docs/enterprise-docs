:_mod-docs-content-type: REFERENCE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-reference-a.adoc
// * ID: [id="my-reference-a"]
// * Title: = My reference A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="changes-in-the-transaction-history-log-files_{context}"]
= Changes in the transaction history log files

The changes in the transaction history log files between RHEL 7 and RHEL 8 are documented in the following summary.

In RHEL 7, the `/var/log/yum.log` file stores:

* Registry of installations, updates, and removals of the software packages
* Transactions from {PackageManagerCommand} and **PackageKit**

In RHEL 8, there is no direct equivalent to the `/var/log/yum.log` file.
To display the information about the transactions, including the **PackageKit** and **microdnf**, use the `{PackageManagerCommand} history` command.

Alternatively, you can search the `/var/log/dnf.rpm.log` file, but this log file does not include the transactions from PackageKit and microdnf, and it has a log rotation which provides the periodic removal of the stored information.
