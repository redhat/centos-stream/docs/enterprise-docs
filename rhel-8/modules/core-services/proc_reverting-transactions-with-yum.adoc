:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
:_module-type: PROCEDURE

[id="reverting-transactions-with-yum_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Reverting a single {PackageManagerName} transaction

[role="_abstract"]

You can revert steps performed within a single transaction by using the `{PackageManagerCommand} history undo` command:

* If the transaction installed a new package, `{PackageManagerCommand} history undo` uninstalls the package.
* If the transaction uninstalled a package, `{PackageManagerCommand} history undo` reinstalls the package.
* The `{PackageManagerCommand} history undo` command also attempts to downgrade all updated packages to their previous versions if the older packages are still available. 


NOTE: If an older package version is not available, the downgrade by using the `{PackageManagerCommand} history undo` command fails. 

.Procedure

. Identify the ID of a transaction you want to revert: 
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} history*
ID | Command line     | Date and time     | Action(s)      | Altered
--------------------------------------------------------------------
13 | install zip      | 2022-11-03 10:49  | Install        |    1   
12 | install unzip    | 2022-11-03 10:49  | Install        |    1   
----

. Optional: Verify that this is the transaction you want to revert by displaying its details: 
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} history info _<transaction_id>_*
----

. Revert the transaction:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} history undo _<transaction_id>_*
----
+
For example, if you want to uninstall the previously installed `unzip` package, enter:
+
[literal,subs="+quotes,attributes"]
....
# *{PackageManagerCommand} history undo 12*
....
+
If you want to revert the last transaction, enter:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} history undo last*
----
