// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_tuning-shares-with-directories-that-contain-a-large-number-of-files.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
:_mod-docs-content-type: PROCEDURE
[id="proc_tuning-shares-with-directories-that-contain-a-large-number-of-files_{context}"]
= Tuning shares with directories that contain a large number of files

[role="_abstract"]
Linux supports case-sensitive file names. For this reason, Samba needs to scan directories for uppercase and lowercase file names when searching or accessing a file. You can configure a share to create new files only in lowercase or uppercase, which improves the performance.

.Prerequisites

* Samba is configured as a file server

.Procedure

. Rename all files on the share to lowercase.
+
[NOTE]
====
Using the settings in this procedure, files with names other than in lowercase will no longer be displayed.
====

. Set the following parameters in the share's section:
+
[literal,subs="+quotes,attributes"]
----
case sensitive = true
default case = lower
preserve case = no
short preserve case = no
----
+
For details about the parameters, see their descriptions in the `smb.conf(5)` man page on your system.

. Verify the [filename]`/etc/samba/smb.conf` file:
+
[literal,subs="+quotes,attributes"]
----
# *testparm*
----

. Reload the Samba configuration:
+
[literal,subs="+quotes,attributes"]
----
# *smbcontrol all reload-config*
----

After you applied these settings, the names of all newly created files on this share use lowercase. Because of these settings, Samba no longer needs to scan the directory for uppercase and lowercase, which improves the performance.
