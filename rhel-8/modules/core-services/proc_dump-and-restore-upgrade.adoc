:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_migrating-to-a-rhel-8-version-of-postgresql.adoc

[id="dump-and-restore-upgrade_{context}"]
= Dump and restore upgrade

When using the dump and restore upgrade, you must dump all databases contents into an SQL file dump file.

Note that the dump and restore upgrade is slower than the fast upgrade method and it might require some manual fixing in the generated SQL file.

ifeval::[{ProductNumber} == 8]
You can use this method for migrating data from:

* The {RHEL} 7 system version of *PostgreSQL 9.2*
* Any earlier {RHEL} 8 version of *PostgreSQL*
* An earlier or equal version of *PostgreSQL* from {RH} Software Collections:
** *PostgreSQL 9.2* (no longer supported)
** *PostgreSQL 9.4* (no longer supported)
** *PostgreSQL 9.6* (no longer supported)
** *PostgreSQL 10*
** *PostgreSQL 12*
** *PostgreSQL 13*

On RHEL 7 and RHEL 8 systems, *PostgreSQL* data is stored in the [filename]`/var/lib/pgsql/data/` directory by default. In case of {RH} Software Collections versions of *PostgreSQL*, the default data directory is [filename]`/var/opt/rh/collection_name/lib/pgsql/data/` (with the exception of `postgresql92`, which uses the [filename]`/opt/rh/postgresql92/root/var/lib/pgsql/data/` directory).

If you want to upgrade from an earlier `postgresql` stream within RHEL 8, follow the procedure described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/installing_managing_and_removing_user-space_components/index#switching-to-a-later-stream_managing-versions-of-appstream-content[Switching to a later stream] and then migrate your *PostgreSQL* data.

To perform the dump and restore upgrade, change the user to `root`.

The following procedure describes migration from the RHEL 7 system version of *PostgreSQL 9.2* to a RHEL 8 version of *PostgreSQL*.
endif::[]

ifeval::[{ProductNumber} == 9]
You can use this method for migrating data from any RHEL 8 version of PostgreSQL to the RHEL 9 version of *PostgreSQL 13*.

On RHEL 8 and RHEL 9 systems, *PostgreSQL* data is stored in the [filename]`/var/lib/pgsql/data/` directory by default.

To perform the dump and restore upgrade, change the user to `root`.

The following procedure describes migration from the RHEL 8 default version of *PostgreSQL 10* to the RHEL 9 version of *PostgreSQL 13*.
endif::[]

.Procedure

ifeval::[{ProductNumber} == 8]
. On your RHEL 7 system, start the *PostgreSQL 9.2* server:
endif::[]

ifeval::[{ProductNumber} == 9]
. On your RHEL 8 system, start the *PostgreSQL 10* server:
endif::[]
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **systemctl start postgresql.service**
....

ifeval::[{ProductNumber} == 8]
. On the RHEL 7 system, dump all databases contents into the [filename]`pgdump_file.sql` file:
endif::[]

ifeval::[{ProductNumber} == 9]
. On the RHEL 8 system, dump all databases contents into the [filename]`pgdump_file.sql` file:
endif::[]
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
**pass:[su - postgres -c "pg_dumpall &gt; ~/pgdump_file.sql"]**
....

. Verify that the databases were dumped correctly:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
**su - postgres -c 'less "$HOME/pgdump_file.sql"'**
....
+
As a result, the path to the dumped sql file is displayed: [filename]`/var/lib/pgsql/pgdump_file.sql`.

ifeval::[{ProductNumber} == 8]
. On the RHEL 8 system, enable the stream (version) to which you wish to migrate:
+
[literal,subs="+quotes,attributes"]
....
# **{PackageManagerCommand} module enable postgresql:__stream__**
....
+
Replace _stream_ with the selected version of the *PostgreSQL* server.
+
You can omit this step if you want to use the default stream, which provides *PostgreSQL 10*.
endif::[]

ifeval::[{ProductNumber} == 8]
. On the RHEL 8 system, install the [package]`postgresql-server` package:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **{PackageManagerCommand} install postgresql-server**
....
+
Optionally, if you used any *PostgreSQL* server modules on RHEL 7, install them also on the RHEL 8 system. If you need to compile a third-party *PostgreSQL* server module, build it against the [package]`postgresql-devel` package.
endif::[]

ifeval::[{ProductNumber} == 9]
. On the RHEL 9 system, install the [package]`postgresql-server` package:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **{PackageManagerCommand} install postgresql-server**
....
+
Optionally, if you used any *PostgreSQL* server modules on RHEL 8, install them also on the RHEL 9 system. If you need to compile a third-party *PostgreSQL* server module, build it against the [package]`postgresql-devel` package.
endif::[]

ifeval::[{ProductNumber} == 8]
. On the RHEL 8 system, initialize the data directory for the new *PostgreSQL* server:
endif::[]

ifeval::[{ProductNumber} == 9]
. On the RHEL 9 system, initialize the data directory for the new *PostgreSQL* server:
endif::[]
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **postgresql-setup --initdb**
....

ifeval::[{ProductNumber} == 8]
. On the RHEL 8 system, copy the [filename]`pgdump_file.sql` into the *PostgreSQL* home directory, and check that the file was copied correctly:
endif::[]

ifeval::[{ProductNumber} == 9]
. On the RHEL 9 system, copy the [filename]`pgdump_file.sql` into the *PostgreSQL* home directory, and check that the file was copied correctly:
endif::[]
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
**su - postgres -c 'test -e "$HOME/pgdump_file.sql" && echo exists'**
....

ifeval::[{ProductNumber} == 8]
. Copy the configuration files from the RHEL 7 system:
endif::[]

ifeval::[{ProductNumber} == 9]
. Copy the configuration files from the RHEL 8 system:
endif::[]
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
**su - postgres -c 'ls -1 $PGDATA/*.conf'**
....
+
The configuration files to be copied are:
+
* [filename]`/var/lib/pgsql/data/pg_hba.conf`
* [filename]`/var/lib/pgsql/data/pg_ident.conf`
* [filename]`/var/lib/pgsql/data/postgresql.conf`

. On the RHEL 8 system, start the new *PostgreSQL* server:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
# **systemctl start postgresql.service**
....

. On the RHEL 8 system, import data from the dumped sql file:
+
[literal,subs="+quotes,verbatim,normal,normal"]
....
**su - postgres -c 'psql -f ~/pgdump_file.sql postgres'**
....

ifeval::[{ProductNumber} == 8]
[NOTE]
====

When upgrading from a {RH} Software Collections version of *PostgreSQL*, adjust the commands to include `scl enable collection_name.`
For example, to dump data from the `rh-postgresql96` Software Collection, use the following command:

[literal,subs="+quotes,verbatim,normal,normal"]
....
**pass:[su - postgres -c 'scl enable rh-postgresql96 "pg_dumpall &gt; ~/pgdump_file.sql"']**
....

====
endif::[]
