:_mod-docs-content-type: PROCEDURE

[id="proc_configuring-postfix-as-a-destination-for-multiple-domains_{context}"]
= Configuring Postfix as a destination for multiple domains

[role="_abstract"]
You can configure Postfix as a mail server that can receive emails for multiple domains. In this configuration, Postfix acts as the final destination for emails sent to addresses within the specified domains. You can configure the following:

* Set up multiple email addresses that point to the same email destination
* Route incoming email for multiple domains to the same Postfix server


.Prerequisites

* You have the root access.

* You have configured a Postfix server.


.Procedure

. In the `/etc/postfix/virtual` virtual alias file, specify the email addresses for each domain. Add each email address on a new line:
[literal,subs="+quotes,attributes"]
+
----
_<info@example.com>_  _<user22@example.net>_
_<sales@example.com>_  _<user11@example.org>_
----
+
In this example, Postfix redirects all emails sent to \info@example.com to \user22@example.net and email sent to \sales@example.com to \user11@example.org.

. Create a hash file for the virtual alias map:
[literal,subs="+quotes,attributes"]
+
----
# *postmap /etc/postfix/virtual*
----
+
This command creates the `/etc/postfix/virtual.db` file. Note that you must always re-run this command after you update the `/etc/postfix/virtual` file.
+

. In the Postfix `/etc/postfix/main.cf` configuration file, add the `virtual_alias_maps` parameter and point it to the hash file:
+
[literal,subs="+quotes,attributes"]
----
virtual_alias_maps = hash:/etc/postfix/virtual
----
+

. Reload the `postfix` service to apply the changes:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl reload postfix*
---- 

.Verification

* Test the configuration by sending an email to one of the virtual email addresses.

.Troubleshooting

* In case of errors, check the `/var/log/maillog` file.
