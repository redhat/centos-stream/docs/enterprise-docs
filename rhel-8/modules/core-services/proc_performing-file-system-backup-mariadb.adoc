:_mod-docs-content-type: PROCEDURE
[id="performing-file-system-backup_{context}"]
= Performing file system backup

To create a file system backup of *MariaDB* data files, copy the content of the *MariaDB* data directory to your backup location.

To back up also your current configuration or the log files, use the optional steps of the following procedure.

.Procedure

. Stop the `mariadb` service:
+
[literal,subs="+quotes",attributes]
----
# *systemctl stop mariadb.service*
----
. Copy the data files to the required location:
+
[literal,subs="+quotes",attributes]
----
# *cp -r /var/lib/mysql /_backup-location_*
----
. Optional: Copy the configuration files to the required location:
+
[literal,subs="+quotes",attributes]
----
# *cp -r /etc/my.cnf /etc/my.cnf.d /_backup-location_/configuration*
----
. Optional: Copy the log files to the required location:
+
[literal,subs="+quotes",attributes]
----
# **cp /var/log/mariadb/* _/backup-location_/logs**
----
. Start the `mariadb` service:
+
[literal,subs="+quotes",attributes]
----
# *systemctl start mariadb.service*
----
. When loading the backed up data from the backup location to the `/var/lib/mysql` directory, ensure that `mysql:mysql` is an owner of all data in `/var/lib/mysql`:
+
[literal,subs="+quotes",attributes]
----
# *chown -R mysql:mysql /var/lib/mysql*
----
