:_mod-docs-content-type: CONCEPT
[id="con_scenarios-when-samba-services-and-samba-client-utilities-load-and-reload-their-configuration_{context}"]
= Scenarios when Samba services and Samba client utilities load and reload their configuration

[role="_abstract"]
The following describes when Samba services and utilities load and reload their configuration:

* Samba services reload their configuration:

** Automatically every 3 minutes
** On manual request, for example, when you run the [command]`smbcontrol all reload-config` command.

* Samba client utilities read their configuration only when you start them.

Note that certain parameters, such as [parameter]`security` require a restart of the [systemitem]`smb` service to take effect and a reload is not sufficient.


[role="_additional-resources"]
.Additional resources
* The [citetitle]`How configuration changes are applied` section in the [citetitle]`smb.conf(5)` man page on your system
* [citetitle]`smbd(8)`, [citetitle]`nmbd(8)`, and [citetitle]`winbindd(8)` man pages on your system
