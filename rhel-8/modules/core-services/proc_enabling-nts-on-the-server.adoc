:_module-type: PROCEDURE

[id="proc_enabling-nts-on-the-server_{context}"]
= Enabling Network Time Security (NTS) on a time server

[role="_abstract"]
If you run your own Network Time Protocol (NTP) server, you can enable the server Network Time Security (NTS) support to facilitate its clients to synchronize securely.

If the NTP server is a client of other servers, that is, it is not a Stratum 1 server, it should use NTS or symmetric key for its synchronization.


.Prerequisites

* Server private key in `PEM` format
* Server certificate with required intermediate certificates in `PEM` format


.Procedure

. Edit the `/etc/chrony.conf` file, and make the following changes: 
+
[literal,subs="+quotes,attributes"]
----
ntsserverkey /etc/pki/tls/private/_<ntp-server.example.net>_.key
ntsservercert /etc/pki/tls/certs/_<ntp-server.example.net>_.crt
----
+
. Set permissions on both the private key and the certificate file that allow the chrony user to read the files, for example
+
[literal,subs="+quotes,attributes"]
----
# *chown root:chrony /etc/pki/tls/private/<ntp-server.example.net>.key /etc/pki/tls/certs/<ntp-server.example.net>.crt*

# *chmod 644  /etc/pki/tls/private/<ntp-server.example.net>.key /etc/pki/tls/certs/<ntp-server.example.net>.crt*
----
+
. Ensure that the `ntsdumpdir /var/lib/chrony` setting is present.

. Open the required ports in firewalld:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd –permannent --add-port={323/udp,4460/tcp}*
# *firewall-cmd --reload*
----

. Restart the `chronyd` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl restart chronyd*
----


.Verification

. Perform a test from a client machine:
+
[literal,subs="+quotes,attributes"]
----
$ *chronyd -Q -t 3 'server*

_ntp-server.example.net_ iburst nts maxsamples 1'
2021-09-15T13:45:26Z chronyd version 4.1 starting (+CMDMON +NTP +REFCLOCK +RTC +PRIVDROP +SCFILTER +SIGND +ASYNCDNS +NTS +SECHASH +IPV6 +DEBUG)
2021-09-15T13:45:26Z Disabled control of system clock
2021-09-15T13:45:28Z System clock wrong by 0.002205 seconds (ignored)
2021-09-15T13:45:28Z chronyd exiting
----
+
The `System clock wrong` message indicates the NTP server is accepting NTS-KE connections and responding with NTS-protected NTP messages.

. Verify the NTS-KE connections and authenticated NTP packets observed on  the server:
+
[literal,subs="+quotes,attributes"]
----
# *chronyc serverstats*

NTP packets received       : 7
NTP packets dropped        : 0
Command packets received   : 22
Command packets dropped    : 0
Client log records dropped : 0
NTS-KE connections accepted: 1
NTS-KE connections dropped : 0
Authenticated NTP packets: 7
----
+
If the value of the `NTS-KE connections accepted` and `Authenticated NTP packets` field is a non-zero value, it means that at least one client was able to connect to the NTS-KE port and send an authenticated NTP request.
