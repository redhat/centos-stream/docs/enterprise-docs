:_mod-docs-content-type: PROCEDURE
[id="proc_proc_migrating-to-a-rhel-8-version-of-mysql-8-0_{context}"]
= Migrating to a RHEL 8 version of MySQL 8.0

[role="_abstract"]

RHEL 7 contains *MariaDB 5.5* as the default implementation of a server from the *MySQL* databases family. The Red Hat Software Collections offering for RHEL 7 provides *MySQL 8.0* and several versions of *MariaDB*. RHEL 8 provides *MySQL 8.0*, *MariaDB 10.3*, and *MariaDB 10.5*.

This procedure describes migration from a Red Hat Software Collections version of *MySQL 8.0* to a RHEL 8 version of *MySQL 8.0* using the `mysql_upgrade` utility. The `mysql_upgrade` utility is provided by the `mysql-server` package.

NOTE: In the Red Hat Software Collections version of *MySQL*, the source data directory is `/var/opt/rh/rh-mysql80/lib/mysql/`. In RHEL 8, *MySQL* data is stored in the `/var/lib/mysql/` directory.

.Prerequisites

* Before performing the upgrade, back up all your data stored in the *MySQL* databases.

.Procedure

. Ensure that the `mysql-server` package is installed on the RHEL 8 system:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install mysql-server*
----

. Ensure that the `mysqld` service is not running on either of the source and target systems at the time of copying data:
+
[literal,subs="+quotes"]
----
# *systemctl stop mysqld.service*
----

. Copy the data from the `/var/opt/rh/rh-mysql80/lib/mysql/` directory on the RHEL 7 source system to the `/var/lib/mysql/` directory on the RHEL 8 target system.

. Set the appropriate permissions and SELinux context for copied files on the target system:
+
[literal,subs="+quotes"]
----
# *restorecon -vr /var/lib/mysql*
----

. Ensure that `mysql:mysql` is an owner of all data in the `/var/lib/mysql` directory:
+
[literal,subs="+quotes"]
----
# *chown -R mysql:mysql /var/lib/mysql*
----

. Start the *MySQL* server on the target system:
+
[literal,subs="+quotes"]
----
# *systemctl start mysqld.service*
----
+
Note: In earlier versions of *MySQL*, the `mysql_upgrade` command was needed to check and repair internal tables. This is now done automatically when you start the server.
// This is not true for MariaDB ^

////
Not needed now that we migrate from version 8.0 to 8.0 - might be needed in the future with a new MySQL version
[IMPORTANT]
====
There are certain risks and known problems related to an in-place upgrade. For example, some queries might not work or they will be run in a different order than before the upgrade. For more information about these risks and problems, and for general information about an in-place upgrade, see link:https://dev.mysql.com/doc/refman/8.0/en/[MySQL 8.0 Release Notes].
====
////
