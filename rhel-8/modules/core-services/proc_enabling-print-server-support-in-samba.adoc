:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_setting-up-samba-as-a-print-server.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_enabling-print-server-support-in-samba.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_enabling-print-server-support-in-samba_{context}"]
= Enabling print server support in Samba

By default, print server support is not enabled in Samba. To use Samba as a print server, you must configure Samba accordingly.


[NOTE]
====
Print jobs and printer operations require remote procedure calls (RPCs). By default, Samba starts the `rpcd_spoolss` service on demand to manage RPCs. During the first RPC call, or when you update the printer list in CUPS, Samba retrieves the printer information from CUPS. This can require approximately 1 second per printer. Therefore, if you have more than 50 printers, tune the `rpcd_spoolss` settings.
====


.Prerequisites

* The printers are configured in a CUPS server.
+
For details about configuring printers in CUPS, see the documentation provided in the CUPS web console (https://_printserver_:631/help) on the print server.


.Procedure

. Edit the [filename]`/etc/samba/smb.conf` file:

.. Add the `[printers]` section to enable the printing backend in Samba:
+
[literal,subs="+quotes,attributes"]
----
**[printers]**
        **comment = All Printers**
        **path = /var/tmp/**
        **printable = yes**
        **create mask = 0600**
----
+
[IMPORTANT]
====
The `[printers]` share name is hard-coded and cannot be changed.
====

.. If the CUPS server runs on a different host or port, specify the setting in the `[printers]` section:
+
[literal,subs="+quotes"]
....
**cups server = __printserver.example.com__:__631__**
....

.. If you have many printers, set the number of idle seconds to a higher value than the numbers of printers connected to CUPS. For example, if you have 100 printers, set in the `[global]` section:
+
[literal,subs="+quotes"]
....
**rpcd_spoolss:idle_seconds = 200**
....
+
If this setting does not scale in your environment, also increase the number of `rpcd_spoolss` workers in the `[global]` section:
+
[literal,subs="+quotes"]
....
**rpcd_spoolss:num_workers = 10**
....
+
By default, `rpcd_spoolss` starts 5 workers.

. Verify the [filename]`/etc/samba/smb.conf` file:
+
[literal,subs="+quotes,attributes"]
----
# *testparm*
----

. Open the required ports and reload the firewall configuration using the `firewall-cmd` utility:
+
[literal,subs="+quotes,attributes"]
----
# *firewall-cmd --permanent --add-service=samba*
# *firewall-cmd --reload*
----

. Restart the `smb` service:
+
[literal,subs="+quotes,attributes"]
----
# *systemctl restart smb*
----
+
After restarting the service, Samba automatically shares all printers that are configured in the CUPS back end. If you want to manually share only specific printers, see
ifdef::differentserver-title[]
xref:proc_manually-sharing-specific-printers_assembly_setting-up-samba-as-a-print-server[Manually sharing specific printers].
endif::[]
ifdef::network-file-services[]
xref:proc_manually-sharing-specific-printers_assembly_setting-up-samba-as-a-print-server[Manually sharing specific printers].
endif::[]


.Verification

* Submit a print job. For example, to print a PDF file, enter:
+
[literal,subs="+quotes"]
....
# **smbclient -U__user__ //__sambaserver.example.com__/__printer_name__ -c "print __example.pdf__"**
....


