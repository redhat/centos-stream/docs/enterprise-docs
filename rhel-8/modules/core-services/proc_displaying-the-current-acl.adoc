:_mod-docs-content-type: PROCEDURE
[id="displaying-the-current-acl_{context}"]

= Displaying the current Access Control List

[role="_abstract"]
You can use the `getfacl` utility to display the current ACL.

.Procedure

* To display the current ACL for a particular file or directory, use:
+
[literal,subs="+quotes,attributes"]
----
$ **getfacl _file-name_**
----
+
Replace _file-name_ with the name of the file or directory.
