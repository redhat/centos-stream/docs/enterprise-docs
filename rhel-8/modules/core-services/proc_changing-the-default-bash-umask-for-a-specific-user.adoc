:_mod-docs-content-type: PROCEDURE
[id="changing-the-default-bash-umask-for-a-specific-user_{context}"]
= Changing the default bash umask for a specific user

By default, `bash` _umask_ of a new user defaults to the one defined in [filename]`/etc/bashrc`.

To change `bash` _umask_pass:attributes[{blank}] for a particular user, use this procedure.

.Procedure 

* Add a call to the [command]`umask` command in [filename]`$HOME/.bashrc` file of that user. For example, to change `bash` _umask_ of user `john` to `0227`:
+
[literal,subs="+quotes,verbatim,normal,macros"]
....
john@server ~]$ *echo 'umask 227' >> /home/john/.bashrc*
....

