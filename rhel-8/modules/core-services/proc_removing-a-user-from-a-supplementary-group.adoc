:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// rhel-8/asseblies/assembly_editing-user-groups-using-the-command-line.adoc

[id="removing-a-user-from-a-supplementary-group_{context}"]
= Removing a user from a supplementary group

[role="_abstract"]
You can remove an existing user from a supplementary group to limit their permissions or access to files and devices.

.Prerequisites
* You have `root` access

.Procedure
* Remove a user from a supplementary group:
+
[literal,subs="+quotes,attributes"]
----
# *gpasswd -d <user-name> <group-name>*
----

.Verification

* Verify that you removed the user sarah from the secondary group developers:
+
[literal,subs="+quotes,attributes"]
----
$ *groups <username>*
----

