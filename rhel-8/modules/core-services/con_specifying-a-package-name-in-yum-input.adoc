:_mod-docs-content-type: CONCEPT

:_module-type: CONCEPT
[id="specifying-a-package-name-in-yum-input_{context}"]

= Specifying a package name in YUM input

[role="_abstract"]

To optimize the installation and removal process, you can append `-n`, `-na`, or `-nevra` suffixes to [command]`{PackageManagerCommand} install` and [command]`{PackageManagerCommand} remove` commands to explicitly define how to parse an argument:

* To install a package using its exact name, use:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install-n _name_*
----
Replace _name_ with the exact name of the package.
* To install a package using its exact name and architecture, use:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install-na _name.architecture_*
----
Replace _name_ and _architecture_ with the exact name and architecture of the package.
* To install a package using its exact name, epoch, version, release, and architecture, use:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install-nevra _name-epoch:version-release.architecture_*
----
Replace _name_, _epoch_, _version_, _release_, and _architecture_ with the exact name, epoch, version, release, and architecture of the package.
