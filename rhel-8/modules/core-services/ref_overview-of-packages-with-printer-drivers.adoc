:_mod-docs-content-type: REFERENCE

[id="overview-of-packages-with-printer-drivers_{context}"]
= Overview of packages with printer drivers

{ProductName} ({ProductShortName}) provides different packages with printer drivers for CUPS. The following is a general overview of these packages and for which vendors they contain drivers:

[cols="1,4",options="header"]
.Driver package list
|===
| Package name | Drivers for printers
| `cups` | Zebra, Dymo
| `c2esp` | Kodak
| `foomatic` | Brother, Canon, Epson, Gestetner, HP, Infotec, Kyocera, Lanier, Lexmark, NRG, Ricoh, Samsung, Savin, Sharp, Toshiba, Xerox, and others
| `gutenprint-cups` | Brother, Canon, Epson, Fujitsu, HP, Infotec, Kyocera, Lanier, NRG, Oki, Minolta, Ricoh, Samsung, Savin, Xerox, and others
| `hplip` | HP
| `pnm2ppa` | HP
| `splix` | Samsung, Xerox, and others
|===

Note that some packages can contain drivers for the same printer vendor or model but with different functionality.

After installing the required package, you can display the list of drivers in the CUPS web interface or by using the `lpinfo -m` command.

