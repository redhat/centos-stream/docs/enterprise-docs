:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_automatically-refreshing-package-database.adoc

:_module-type: PROCEDURE

[id="installing-dnf-automatic_{context}"]

= Installing DNF Automatic

[role="_abstract"]

To check and download package updates automatically and regularly, you can use the *DNF Automatic* tool that is provided by the `dnf-automatic` package.

.Procedure

* Install the `dnf-automatic` package:
+
[literal,subs="+quotes,attributes"]
----
# *{PackageManagerCommand} install dnf-automatic*
----


.Verification

* Verify the successful installation by confirming the presence of the `dnf-automatic` package:
+
[literal,subs="+quotes,attributes"]
----
# *rpm -qi dnf-automatic*
----
