// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:experimental:

:_mod-docs-content-type: PROCEDURE

[id="enforcing-password-expiration-in-the-web-console_{context}"]
= Enforcing password expiration in the web console

[role="_abstract"]
By default, user accounts have set passwords to never expire. You can set system passwords to expire after a defined number of days. When the password expires, the user must change its password at the next login attempt before the user can access the system.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

.Procedure

. Log in to the {ProductShortName}{nbsp}{ProductNumber} web console.

. Click btn:[Accounts].

. Select the user account for which you want to enforce password expiration.

. Click btn:[edit] on the *Password* line.
+
image:cockpit-edit-password-change.png[]

. In the *Password expiration* dialog box, select *Require password change every ... days* and enter a positive whole number representing the number of days after which the password expires.

. Click btn:[Change].
+
The web console immediately shows the date of the future password change request on the *Password* line.


