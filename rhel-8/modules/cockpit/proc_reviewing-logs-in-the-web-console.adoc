ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="reviewing-logs-in-the-web-console_{context}"]
= Reviewing logs in the web console

[role="_abstract"]
The {ProductShortName}{nbsp}{ProductNumber} web console Logs section is a UI for the `journalctl` utility. You can access system logs in the web console interface.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. Click *Logs*.
+
image:cockpit-logs-new.png[Logs page]
. Open log entry details by clicking on your selected log entry in the list.

NOTE: You can use the btn:[Pause] button to pause new log entries from appearing. Once you resume new log entries, the web console will load all log entries that were reported after you used the btn:[Pause] button.

You can filter the logs by time, priority or identifier. For more information, see
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/reviewing-logs_system-management-using-the-rhel-{ProductNumber}-web-console#filtering-logs-in-the-web-console_reviewing-logs[Filtering logs in the web console].

