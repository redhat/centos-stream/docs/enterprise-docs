// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE


[id="creating-volume-groups-in-the-web-console_{context}"]
= Creating volume groups in the web console

Create volume groups from one or more physical drives or other storage devices.

Logical volumes are created from volume groups. Each volume group can include multiple logical volumes.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]
* The `cockpit-storaged` package is installed on your system.
* Physical drives or other types of storage devices from which you want to create volume groups.

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. Click btn:[Storage].

. In the *Storage* table, click the menu button.

. From the drop-down menu, select *Create LVM2 volume group*.
+
image:cockpit-adding-volume-groups-create-lvm2.png[Image displaying the available options in the Storage table drop-down menu. Selecting Create LVM2 volume group.]

. In the *Name* field, enter a name for the volume group. The name must not include spaces.

. Select the drives you want to combine to create the volume group.
+
image:cockpit-create-volume-group.png[]
+
The {ProductShortName} web console displays only unused block devices. If you do not see your device in the list, make sure that it is not being used by your system, or format it to be empty and unused. Used devices include, for example:

* Devices formatted with a file system
* Physical volumes in another volume group
* Physical volumes being a member of another software RAID device

. Click btn:[Create].
+
The volume group is created.

.Verification

* On the *Storage* page, check whether the new volume group is listed in the *Storage* table.
