:_module-type: PROCEDURE

[id="proc_disabling-basic-authentication-in-the-web-console_{context}"]
= Disabling basic authentication in the web console

You can modify the behavior of an authentication scheme by modifying the `cockpit.conf` file. Use the `none` action to disable an authentication scheme and only allow authentication through GSSAPI and forms.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* You have `root` privileges or permissions to enter administrative commands with `sudo`.


.Procedure

. Open or create the `cockpit.conf` file in the `/etc/cockpit/` directory in a text editor of your preference, for example:
+
[subs="quotes"]
----
# *vi cockpit.conf*
----

. Add the following text:
+
----
[basic]
action = none
----

. Save the file.

. Restart the web console for changes to take effect.
+
[subs="quotes"]
----
# *systemctl try-restart cockpit*
----



