:_mod-docs-content-type: PROCEDURE

[id="proc_enabling-ssh-login-for-a-new-host_{context}"]
= Enabling SSH login for a new host

When you add a new host to the web console, you can also log in to the host with an SSH key. If you already have an SSH key on your system, the web console uses the existing one; otherwise, the web console can create a key.


.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. In the {ProductShortName}{nbsp}{ProductNumber} web console, click your `_<username>_@_<hostname>_` in the top left corner of the *Overview* page.
+
image:cockpit-username-dropdown.png[username@hostname drop-down menu]

. From the drop-down menu, click btn:[Add new host].

. In the *Add new host* dialog box, specify the host you want to add.

. Add the user name for the account to which you want to connect.
+
You can use any user account of the remote system. However, if you use a user account without administration privileges, you cannot perform administration tasks.

. Optional: Click the *Color* field to change the color of the system.

. Click btn:[Add].
+
A new dialog window appears asking for a password.

. Enter the user account password.

. Check *Authorize SSH key* if you already have an SSH key.
+
image:cockpit-authorize-ssh-key.png[Log in to a host dialog windows]

. Check *Create a new SSH key and authorize it* if you do not have an SSH key. The web console creates the key.
+
image:cockpit-ssh-key-add-from-login.png[SSH key created for the host]

.. Add a password for the SSH key.

.. Confirm the password.

. Click btn:[Log in].


.Verification

. Log out.

. Log back in.

. Click btn:[Log in] in the *Not connected to host* screen.

. Select *SSH key* as your authentication option.
+
image::cockpit-ssh-login-dialog.png[Log in dialog window with the SSH key option selected]

. Enter your key password.

. Click btn:[Log in].


[role="_additional-resources"]
.Additional resources
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/assembly_using-secure-communications-between-two-systems-with-openssh_securing-networks[Using secure communications between two systems with OpenSSH]
