// included in assembly_optimizing-the-system-performance-using-the-web-console.adoc

:_mod-docs-content-type: CONCEPT

[id="performance-tuning-options-in-the-web-console_{context}"]
= Performance tuning options in the web console

[role="_abstract"]
ifeval::[{ProductNumber} == 8]
{RHEL8} provides several performance profiles that optimize the system for the following tasks:
endif::[]

ifeval::[{ProductNumber} == 9]
{RHEL9} provides several performance profiles that optimize the system for the following tasks:
endif::[]


* Systems using the desktop
* Throughput performance
* Latency performance
* Network performance
* Low power consumption
* Virtual machines

The `TuneD` service optimizes system options to match the selected profile.

In the web console, you can set which performance profile your system uses.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/getting-started-with-tuned_monitoring-and-managing-system-status-and-performance[Getting started with TuneD]

