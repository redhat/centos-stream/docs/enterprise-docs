:_content-type: REFERENCE

[id="ref_satellite-host-management-and-monitoring_{context}"]
= Satellite host management and monitoring in the web console

After enabling {ProductShortName} web console integration on a Red&nbsp;Hat Satellite Server, you manage many hosts at scale in the web console.

Red&nbsp;Hat Satellite is a system management solution for deploying, configuring, and maintaining your systems across physical, virtual, and cloud environments. Satellite provides provisioning, remote management and monitoring of multiple {ProductName} deployments with a centralized tool.

By default, {ProductShortName} web console integration is disabled in Red&nbsp;Hat Satellite. To access {ProductShortName} web console features for your hosts from within Red&nbsp;Hat Satellite, you must first enable {ProductShortName} web console integration on a Red&nbsp;Hat Satellite Server.

To enable the {ProductShortName} web console on your Satellite Server, enter the following command as `root`:
[subs="+quotes"]
----
# *satellite-installer --enable-foreman-plugin-remote-execution-cockpit --reset-foreman-plugin-remote-execution-cockpit-ensure*
----

[role="_additional-resources"]
.Additional resources
* link:https://docs.redhat.com/en/documentation/red_hat_satellite/6.16/html/managing_hosts/host-management-and-monitoring-by-using-cockpit[Host management and monitoring by using the RHEL web console] in the Managing hosts in Red&nbsp;Hat Satellite guide
