// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="extending-lvm-vdo-volumes-in-the-web-console_{context}"]
= Extending LVM-VDO volumes in the web console
[[extending-vdo-volumes-in-the-web-console_{context}]]

[role="_abstract"]
Extend LVM-VDO volumes in the {ProductShortName}{nbsp}{ProductNumber} web console.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* You have installed the `cockpit-storaged` package on your system.

* You have created an LVM-VDO volume.

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. In the panel, click *Storage*.

. Click your LVM-VDO volume in the *VDO Devices* box.

. In the LVM-VDO volume details, click btn:[Grow].

. In the *Grow logical size of VDO* dialog box, extend the logical size of the LVM-VDO volume.

. Click *Grow*.

.Verification

* Check the LVM-VDO volume details for the new size to verify that your changes have been successful.
