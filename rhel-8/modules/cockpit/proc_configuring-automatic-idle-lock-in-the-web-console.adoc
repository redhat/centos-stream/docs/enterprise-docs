:_mod-docs-content-type: PROCEDURE

[id="configuring-automatic-idle-lock-in-the-web-console_{context}"]
= Configuring automatic idle lock in the web console

You can enable the automatic idle lock and set the idle timeout for your system through the web console interface.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* You have `root` privileges or permissions to enter administrative commands with `sudo`.


.Procedure

. Open the `cockpit.conf` file in the `/etc/cockpit/` directory in a text editor of your preference, for example:
+
[subs="quotes"]
----
# *vi /etc/cockpit/cockpit.conf*
----

. Add the following text to the file:
+
[subs="quotes,attributes"]
----
[Session]
IdleTimeout=_&lt;X&gt;_
----
+
Substitute _&lt;X&gt;_ with a number for a time period of your choice in minutes.

. Save the file.

. Restart the web console for changes to take effect.
+
[subs="quotes"]
----
# *systemctl try-restart cockpit*
----

.Verification

* Check if the session logs you out after a set period of time.
