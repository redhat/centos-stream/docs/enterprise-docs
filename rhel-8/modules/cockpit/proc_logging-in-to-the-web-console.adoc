// Module included in the following assemblies:
//
// enterprise/assemblies/assembly_getting-started-with-the-web-console.adoc

:_mod-docs-content-type: PROCEDURE

[id="logging-in-to-the-web-console_{context}"]
= Logging in to the web console

When the `cockpit.socket` service is running and the corresponding firewall port is open, you can log in to the web console in your browser for the first time.

.Prerequisites

* Use one of the following browsers to open the web console:
** Mozilla Firefox 52 and later
** Google Chrome 57 and later
** Microsoft Edge 16 and later
* System user account credentials
+
The {ProductShortName} web console uses a specific pluggable authentication modules (PAM) stack at `/etc/pam.d/cockpit`. The default configuration allows logging in with the user name and password of any local account on the system.
* Port 9090 is open in your firewall.

.Procedure

. In your web browser, enter the following address to access the web console:
+
----
https://localhost:9090
----
+
[NOTE]
====
This provides a web-console login on your local machine. If you want to log in to the web console of a remote system, see xref:connecting-to-the-web-console-from-a-remote-machine_getting-started-with-the-rhel-{ProductNumber}-web-console[]
====
+
If you use a self-signed certificate, the browser displays a warning. Check the certificate, and accept the security exception to proceed with the login.
+
The console loads a certificate from the `/etc/cockpit/ws-certs.d` directory and uses the last file with a `.cert` extension in alphabetical order.
To avoid having to grant security exceptions, install a certificate signed by a certificate authority (CA).

. In the login screen, enter your system user name and password.


. Click *Log In*.

After successful authentication, the {ProductShortName} web console interface opens.

[IMPORTANT]
====
To switch between limited and administrative access, click *Administrative access* or *Limited access* in the top panel of the web console page. You must provide your user password to gain administrative access.
====
