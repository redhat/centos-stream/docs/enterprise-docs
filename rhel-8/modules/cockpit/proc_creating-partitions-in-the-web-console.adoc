ifndef::imagesdir[:imagesdir: ../images]

:_mod-docs-content-type: PROCEDURE

[id="creating-partitions-in-the-web-console_{context}"]
= Creating partitions in the web console

[role="_abstract"]
To create a new partition:

* Use an existing partition table

* Create a partition


.Prerequisites

* The `cockpit-storaged` package is installed on your system.

* The web console must be installed and accessible. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].

* An unformatted volume connected to the system is visible in the *Storage* table of the *Storage* tab.

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. Click the *Storage* tab.

. In the *Storage* table, click the device which you want to partition to open the page and options for that device.

. On the device page, click the menu button, btn:[⋮], and select *Create partition table*.

. In the *Initialize disk* dialog box, select the following:
.. *Partitioning*:
*** Compatible with all systems and devices (MBR)
*** Compatible with modern system and hard disks > 2TB (GPT)
*** No partitioning
.. *Overwrite*:
*** Select the *Overwrite existing data with zeros* checkbox if you want the {ProductShortName} web console to rewrite the whole disk with zeros. This option is slower because the program has to go through the whole disk, but it is more secure.
Use this option if the disk includes any data and you need to overwrite it.
+
If you do not select the *Overwrite existing data with zeros* checkbox, the {ProductShortName} web console rewrites only the disk header. This increases the speed of formatting.

. Click btn:[Initialize].

. Click the menu button, btn:[⋮], next to the partition table you created. It is named *Free space* by default.

. Click btn:[Create partition].

. In the *Create partition* dialog box, enter a *Name* for the file system.

. Add a *Mount point*.

. In the *Type* drop-down menu, select a file system:
+
--
* *XFS* file system supports large logical volumes, switching physical drives online without outage, and growing an existing file system.
Leave this file system selected if you do not have a different strong preference.

* *ext4* file system supports:
 ** Logical volumes
 ** Switching physical drives online without outage
 ** Growing a file system
 ** Shrinking a file system
--
+
Additional option is to enable encryption of partition done by LUKS (Linux Unified Key Setup), which allows you to encrypt the volume with a passphrase.

. Enter the *Size* of the volume you want to create.

. Select the *Overwrite existing data with zeros* checkbox if you want the {ProductShortName} web console to rewrite the whole disk with zeros. This option is slower because the program has to go through the whole disk, but it is more secure.
Use this option if the disk includes any data and you need to overwrite it.
+
If you do not select the *Overwrite existing data with zeros* checkbox, the {ProductShortName} web console rewrites only the disk header. This increases the speed of formatting.


. If you want to encrypt the volume, select the type of encryption in the *Encryption* drop-down menu.
+
If you do not want to encrypt the volume, select *No encryption*.

. In the *At boot* drop-down menu, select when you want to mount the volume.

. In *Mount options* section:
.. Select the *Mount read only* checkbox if you want the to mount the volume as a read-only logical volume.
.. Select the *Custom mount options* checkbox and add the mount options if you want to change the default mount option.

. Create the partition:
** If you want to create and mount the partition, click the btn:[Create and mount] button.
** If you want to only create the partition, click the btn:[Create only] button.
+
Formatting can take several minutes depending on the volume size and which formatting options are selected.

.Verification

* To verify that the partition has been successfully added, switch to the *Storage* tab and check the *Storage* table and verify whether the new partition is listed.
