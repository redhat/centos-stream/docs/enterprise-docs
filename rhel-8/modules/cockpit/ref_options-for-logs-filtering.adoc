:_mod-docs-content-type: REFERENCE

[id="options-for-logs-filtering_{context}"]
= Options for logs filtering

[role="_abstract"]
There are several `journalctl` options, which you can use for filtering logs in the web console, that may be useful. Some of these are already covered as part of the drop-down menus in the web console interface.


.Table
[options="header"]
|====
|Option name|Usage|Notes
|`priority`|Filter output by message priorities. Takes a single numeric or textual log level. The log levels are the usual syslog log levels. If a single log level is specified, all messages with this log level or a lower (therefore more important) log level are shown.|Covered in the *Priority* drop-down menu.
|`identifier`|Show messages for the specified syslog identifier SYSLOG_IDENTIFIER. Can be specified multiple times. |Covered in the *Identifier* drop-down menu.
|`follow`|Shows only the most recent journal entries, and continuously prints new entries as they are appended to the journal.|Not covered in a drop-down.
|`service`|Show messages for the specified `systemd` unit. Can be specified multiple times. |Is not covered in a drop-down. Corresponds to the `journalctl --unit` parameter.
|`boot`|Show messages from a specific boot.

A positive integer will look up the boots starting from the beginning of the journal, and an equal-or-less-than zero integer will look up boots starting from the end of the journal. Therefore, 1 means the first boot found in the journal in chronological order, 2 the second and so on; while -0 is the last boot, -1 the boot before last, and so on. |Covered only as *Current boot* or *Previous boot* in the *Time* drop-down menu. Other options need to be written manually.
|`since`|Start showing entries on or newer than the specified date, or on or older than the specified date, respectively. Date specifications should be of the format "2012-10-30 18:17:16". If the time part is omitted, "00:00:00" is assumed. If only the seconds component is omitted, ":00" is assumed. If the date component is omitted, the current day is assumed. Alternatively the strings "yesterday", "today", "tomorrow" are understood, which refer to 00:00:00 of the day before the current day, the current day, or the day after the current day, respectively. "now" refers to the current time. Finally, relative times may be specified, prefixed with "-" or "+", referring to times before or after the current time, respectively.|Not covered in a drop-down.
|====
