// This defines images directory relative to pwd
ifndef::imagesdir[:imagesdir: ../images]

:experimental:

:_mod-docs-content-type: PROCEDURE

[id="formatting-logical-volumes-in-the-web-console_{context}"]
= Formatting logical volumes in the web console

Logical volumes act as physical drives. To use them, you must format them with a file system.

WARNING: Formatting logical volumes erases all data on the volume.

The file system you select determines the configuration parameters you can use for logical volumes.
For example, the XFS file system does not support shrinking volumes.


.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* The `cockpit-storaged` package is installed on your system.

* The logical volume created.

* You have root access privileges to the system.

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. Click btn:[Storage].

. In the *Storage* table, click the volume group in the logical volumes is created.

. On the *Logical volume group* page, scroll to the *LVM2 logical volumes* section.

. Click the menu button, btn:[⋮], next to the volume group you want to format.

. From the drop-down menu, select btn:[Format].
+
image:cockpit-lv-details.png[Image displaying the details of the existing logical volume.]

. In the *Name* field, enter a name for the file system.

. In the *Mount Point* field, add the mount path.
+
image:cockpit-lv-format.png[The format a logical volume dialog box with configurable fields.]

. In the menu:Type[] drop-down menu, select a file system:
+
--
* *XFS* file system supports large logical volumes, switching physical drives online without outage, and growing an existing file system.
Leave this file system selected if you do not have a different strong preference.
+
XFS does not support reducing the size of a volume formatted with an XFS file system

* *ext4* file system supports:
 ** Logical volumes
 ** Switching physical drives online without an outage
 ** Growing a file system
 ** Shrinking a file system
--

. Select the *Overwrite existing data with zeros* checkbox if you want the {ProductShortName} web console to rewrite the whole disk with zeros. This option is slower because the program has to go through the whole disk, but it is more secure.
Use this option if the disk includes any data and you need to overwrite it.
+
If you do not select the *Overwrite existing data with zeros* checkbox, the {ProductShortName} web console rewrites only the disk header. This increases the speed of formatting.

. From the menu:Encryption[] drop-down menu, select the type of encryption if you want to enable it on the logical volume.
+
You can select a version with either the LUKS1 (Linux Unified Key Setup) or LUKS2 encryption, which allows you to encrypt the volume with a passphrase.

. In the menu:At boot[] drop-down menu, select when you want the logical volume to mount after the system boots.

. Select the required *Mount options*.

. Format the logical volume:
* If you want to format the volume and immediately mount it, click btn:[Format and mount].
* If you want to format the volume without mounting it, click btn:[Format only].
+
Formatting can take several minutes depending on the volume size and which formatting options are selected.

.Verification
. On the *Logical volume group* page, scroll to the *LVM2 logical volumes* section and click the logical volume to check the details and additional options.
+
image:cockpit-lv-formatted.png[]

. If you selected the btn:[Format only] option, click the menu button at the end of the line of the logical volume, and select btn:[Mount] to use the logical volume.
