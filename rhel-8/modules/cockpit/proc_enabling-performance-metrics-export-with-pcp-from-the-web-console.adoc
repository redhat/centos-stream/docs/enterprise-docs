:_mod-docs-content-type: PROCEDURE

[id="proc_enabling-performance-metrics-export-with-pcp-from-the-web-console_{context}"]
= Monitoring performance on several systems by using the web console and Grafana

Grafana enables you to collect data from several systems at once and review a graphical representation of their collected Performance Co-Pilot (PCP) metrics. You can set up performance metrics monitoring and export for several systems in the web console interface.

.Prerequisites

include::common-content/snip_web-console-prerequisites.adoc[]

* You have installed the `cockpit-pcp` package.

* You have enabled the PCP service:
+
[subs="+quotes,attributes"]
----
# *systemctl enable --now pmlogger.service pmproxy.service*
----

* You have set up the Grafana dashboard. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#setting-up-a-grafana-server_setting-up-graphical-representation-of-pcp-metrics[Setting up a grafana-server].

* You have installed the `redis` package.
+
Alternatively, you can install the package from the web console interface later in the procedure.

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. In the *Overview* page, click *View metrics and history* in the *Usage* table.

. Click the btn:[Metrics settings] button.

. Move the *Export to network* slider to active position.
+
image:cockpit-export-to-network-slider.png[Metrics settings]
+
If you do not have the `redis` package installed, the web console prompts you to install it.

. To open the `pmproxy` service, select a zone from a drop-down list and click the btn:[Add pmproxy] button. 

. Click *Save*.

.Verification

. Click *Networking*.

. In the *Firewall* table, click the btn:[Edit rules and zones] button.

. Search for `pmproxy` in your selected zone.


IMPORTANT: Repeat this procedure on all the systems you want to watch.

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#setting-up-pcp-with-pcp-zeroconf_setting-up-graphical-representation-of-pcp-metrics[Setting up graphical representation of PCP metrics]

