:_mod-docs-content-type: PROCEDURE

[id="registering-subscriptions-with-activation-key-in-the-web-console_{context}"]
= Registering subscriptions with activation keys in the web console

You can register a newly installed {ProductName} with an activation key in the {ProductShortName} web console.

.Prerequisites

* An activation key of your {RH} product subscription.

include::common-content/snip_web-console-prerequisites.adoc[]

.Procedure

include::common-content/snip_web-console-log-in-step.adoc[]

. In the *Health* field on the *Overview* page, click the *Not registered* warning, or click *Subscriptions* in the main menu to move to the page with your subscription information.
+
image:cockpit-subscription-Health.png[Health status - Not registered]
.
. In the *Overview* filed, click btn:[Register].

. In the *Register system* dialog box, select *Activation key* to register using an activation key.
+
image:cockpit-subscriptions-key.png[Register system dialog window]

. Enter your key or keys.

. Enter your organization's name or ID.
+
To get the organization ID, go to your Red Hat contact point.

* If you do not want to connect your system to Red Hat Insights, clear the *Insights* check box.

. Click btn:[Register].
