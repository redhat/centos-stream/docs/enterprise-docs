:_mod-docs-content-type: REFERENCE
[id="removed-hardware-support_{context}"]
= Removed hardware support

The following device drivers and adapters were supported in RHEL 7 but are no longer available in RHEL 8.0.

[id="removed-device-drivers_{context}"]
== Removed device drivers

Support for the following device drivers has been removed in RHEL 8:

* 3w-9xxx
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1461124++[1461124]

* 3w-sas
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1461124++[1461124]

* aic79xx
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1513562++[1513562]

* aoe
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1528355++[1528355]

* arcmsr
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1528317++[1528317]

* ata drivers:
// BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1526512++[1526512]
+
** acard-ahci
+
** sata_mv
+
** sata_nv
+
** sata_promise
+
** sata_qstor
+
** sata_sil
+
** sata_sil24
+
** sata_sis
+
** sata_svw
+
** sata_sx4
+
** sata_uli
+
** sata_via
+
** sata_vsc

* bfa
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1513577++[1513577]

* cxgb3
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1513595++[1513595]

* cxgb3i
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1513595++[1513595]

* e1000
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1645643++[1645643]

* floppy
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1637250++[1637250]

* hptiop
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1513056++[1513056]

* initio
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1572256++[1572256]

* isci
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1514090++[1514090]

* iw_cxgb3
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1513595++[1513595]

* mptbase - This driver is left in place for virtualization use case and easy developer transition. However it is not supported.

* mptctl

* mptsas - This driver is left in place for virtualization use case and easy developer transition. However it is not supported.

* mptscsih - This driver is left in place for virtualization use case and easy developer transition. However it is not supported.

* mptspi - This driver is left in place for virtualization use case and easy developer transition. However it is not supported.

* mthca
// https://bugzilla.redhat.com/show_bug.cgi?id=1585697

* mtip32xx
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1528351++[1528351]

* mvsas

* mvumi
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1517361++[1517361]

* OSD drivers:
// BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1517315++[1517315]
+
** osd
+
** libosd

* osst
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1517300++[1517300]

* pata drivers:
// BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1523648++[1523648]
+
** pata_acpi
+
** pata_ali
+
** pata_amd
+
** pata_arasan_cf
+
** pata_artop
+
** pata_atiixp
+
** pata_atp867x
+
** pata_cmd64x
+
** pata_cs5536
+
** pata_hpt366
+
** pata_hpt37x
+
** pata_hpt3x2n
+
** pata_hpt3x3
+
** pata_it8213
+
** pata_it821x
+
** pata_jmicron
+
** pata_marvell
+
** pata_netcell
+
** pata_ninja32
+
** pata_oldpiix
+
** pata_pdc2027x
+
** pata_pdc202xx_old
+
** pata_piccolo
+
** pata_rdc
+
** pata_sch
+
** pata_serverworks
+
** pata_sil680
+
** pata_sis
+
** pata_via
+
** pdc_adma

* pm80xx(pm8001)
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1517331++[1517331]

* pmcraid
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1517365++[1517365]

* qla3xxx - This driver is left in place for virtualization use case and easy developer transition. However it is not supported.
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1373445++[1373445]

* qlcnic
// https://bugzilla.redhat.com/show_bug.cgi?id=1835771

* qlge
// https://bugzilla.redhat.com/show_bug.cgi?id=1835771

* stex
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1513055++[1513055]

* sx8
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1528363++[1528363]

* tulip
// https://bugzilla.redhat.com/show_bug.cgi?id=1534870

* ufshcd
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1517356++[1517356]

* wireless drivers:
+
** carl9170
+
** iwl4965
+
** iwl3945
+
** mwl8k
+
** rt73usb
+
** rt61pci
+
** rtl8187
+
** wil6210
//, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1559336++[1559336]


[id="removed-adapters_{context}"]
== Removed adapters

Support for the adapters listed below has been removed in RHEL 8. Support for other than listed adapters from the mentioned drivers remains unchanged.

PCI IDs are in the format of _vendor:device:subvendor:subdevice_. If the _subdevice_ or _subvendor:subdevice_ entry is not listed, devices with any values of such missing entries have been removed.

To check the PCI IDs of the hardware on your system, run the `lspci -nn` command.

* The following adapters from the `aacraid` driver have been removed:
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1495307++[1495307]
+
** PERC 2/Si (Iguana/PERC2Si), PCI ID 0x1028:0x0001:0x1028:0x0001
+
** PERC 3/Di (Opal/PERC3Di), PCI ID 0x1028:0x0002:0x1028:0x0002
+
** PERC 3/Si (SlimFast/PERC3Si), PCI ID 0x1028:0x0003:0x1028:0x0003
+
** PERC 3/Di (Iguana FlipChip/PERC3DiF), PCI ID 0x1028:0x0004:0x1028:0x00d0
+
** PERC 3/Di (Viper/PERC3DiV), PCI ID 0x1028:0x0002:0x1028:0x00d1
+
** PERC 3/Di (Lexus/PERC3DiL), PCI ID 0x1028:0x0002:0x1028:0x00d9
+
** PERC 3/Di (Jaguar/PERC3DiJ), PCI ID 0x1028:0x000a:0x1028:0x0106
+
** PERC 3/Di (Dagger/PERC3DiD), PCI ID 0x1028:0x000a:0x1028:0x011b
+
** PERC 3/Di (Boxster/PERC3DiB), PCI ID 0x1028:0x000a:0x1028:0x0121
+
** catapult, PCI ID 0x9005:0x0283:0x9005:0x0283
+
** tomcat, PCI ID 0x9005:0x0284:0x9005:0x0284
+
** Adaptec 2120S (Crusader), PCI ID 0x9005:0x0285:0x9005:0x0286
+
** Adaptec 2200S (Vulcan), PCI ID 0x9005:0x0285:0x9005:0x0285
+
** Adaptec 2200S (Vulcan-2m), PCI ID 0x9005:0x0285:0x9005:0x0287
+
** Legend S220 (Legend Crusader), PCI ID 0x9005:0x0285:0x17aa:0x0286
+
** Legend S230 (Legend Vulcan), PCI ID 0x9005:0x0285:0x17aa:0x0287
+
** Adaptec 3230S (Harrier), PCI ID 0x9005:0x0285:0x9005:0x0288
+
** Adaptec 3240S (Tornado), PCI ID 0x9005:0x0285:0x9005:0x0289
+
** ASR-2020ZCR SCSI PCI-X ZCR (Skyhawk), PCI ID 0x9005:0x0285:0x9005:0x028a
+
** ASR-2025ZCR SCSI SO-DIMM PCI-X ZCR (Terminator), PCI ID 0x9005:0x0285:0x9005:0x028b
+
** ASR-2230S + ASR-2230SLP PCI-X (Lancer), PCI ID 0x9005:0x0286:0x9005:0x028c
+
** ASR-2130S (Lancer), PCI ID 0x9005:0x0286:0x9005:0x028d
+
** AAR-2820SA (Intruder), PCI ID 0x9005:0x0286:0x9005:0x029b
+
** AAR-2620SA (Intruder), PCI ID 0x9005:0x0286:0x9005:0x029c
+
** AAR-2420SA (Intruder), PCI ID 0x9005:0x0286:0x9005:0x029d
+
** ICP9024RO (Lancer), PCI ID 0x9005:0x0286:0x9005:0x029e
+
** ICP9014RO (Lancer), PCI ID 0x9005:0x0286:0x9005:0x029f
+
** ICP9047MA (Lancer), PCI ID 0x9005:0x0286:0x9005:0x02a0
+
** ICP9087MA (Lancer), PCI ID 0x9005:0x0286:0x9005:0x02a1
+
** ICP5445AU (Hurricane44), PCI ID 0x9005:0x0286:0x9005:0x02a3
+
** ICP9085LI (Marauder-X), PCI ID 0x9005:0x0285:0x9005:0x02a4
+
** ICP5085BR (Marauder-E), PCI ID 0x9005:0x0285:0x9005:0x02a5
+
** ICP9067MA (Intruder-6), PCI ID 0x9005:0x0286:0x9005:0x02a6
+
** Themisto Jupiter Platform, PCI ID 0x9005:0x0287:0x9005:0x0800
+
** Themisto Jupiter Platform, PCI ID 0x9005:0x0200:0x9005:0x0200
+
** Callisto Jupiter Platform, PCI ID 0x9005:0x0286:0x9005:0x0800
+
** ASR-2020SA SATA PCI-X ZCR (Skyhawk), PCI ID 0x9005:0x0285:0x9005:0x028e
+
** ASR-2025SA SATA SO-DIMM PCI-X ZCR (Terminator), PCI ID 0x9005:0x0285:0x9005:0x028f
+
** AAR-2410SA PCI SATA 4ch (Jaguar II), PCI ID 0x9005:0x0285:0x9005:0x0290
+
** CERC SATA RAID 2 PCI SATA 6ch (DellCorsair), PCI ID 0x9005:0x0285:0x9005:0x0291
+
** AAR-2810SA PCI SATA 8ch (Corsair-8), PCI ID 0x9005:0x0285:0x9005:0x0292
+
** AAR-21610SA PCI SATA 16ch (Corsair-16), PCI ID 0x9005:0x0285:0x9005:0x0293
+
** ESD SO-DIMM PCI-X SATA ZCR (Prowler), PCI ID 0x9005:0x0285:0x9005:0x0294
+
** AAR-2610SA PCI SATA 6ch, PCI ID 0x9005:0x0285:0x103C:0x3227
+
** ASR-2240S (SabreExpress), PCI ID 0x9005:0x0285:0x9005:0x0296
+
** ASR-4005, PCI ID 0x9005:0x0285:0x9005:0x0297
+
** IBM 8i (AvonPark), PCI ID 0x9005:0x0285:0x1014:0x02F2
+
** IBM 8i (AvonPark Lite), PCI ID 0x9005:0x0285:0x1014:0x0312
+
** IBM 8k/8k-l8 (Aurora), PCI ID 0x9005:0x0286:0x1014:0x9580
+
** IBM 8k/8k-l4 (Aurora Lite), PCI ID 0x9005:0x0286:0x1014:0x9540
+
** ASR-4000 (BlackBird), PCI ID 0x9005:0x0285:0x9005:0x0298
+
** ASR-4800SAS (Marauder-X), PCI ID 0x9005:0x0285:0x9005:0x0299
+
** ASR-4805SAS (Marauder-E), PCI ID 0x9005:0x0285:0x9005:0x029a
+
** ASR-3800 (Hurricane44), PCI ID 0x9005:0x0286:0x9005:0x02a2
+
** Perc 320/DC, PCI ID 0x9005:0x0285:0x1028:0x0287
+
** Adaptec 5400S (Mustang), PCI ID 0x1011:0x0046:0x9005:0x0365
+
** Adaptec 5400S (Mustang), PCI ID 0x1011:0x0046:0x9005:0x0364
+
** Dell PERC2/QC, PCI ID 0x1011:0x0046:0x9005:0x1364
+
** HP NetRAID-4M, PCI ID 0x1011:0x0046:0x103c:0x10c2
+
** Dell Catchall, PCI ID 0x9005:0x0285:0x1028
+
** Legend Catchall, PCI ID 0x9005:0x0285:0x17aa
+
** Adaptec Catch All, PCI ID 0x9005:0x0285
+
** Adaptec Rocket Catch All, PCI ID 0x9005:0x0286
+
** Adaptec NEMER/ARK Catch All, PCI ID 0x9005:0x0288

* The following Mellanox Gen2 and ConnectX-2 adapters from the `mlx4_core` driver have been removed:
//https://bugzilla.redhat.com/show_bug.cgi?id=1887141
+
** PCI ID 0x15B3:0x1002
+
** PCI ID 0x15B3:0x676E
+
** PCI ID 0x15B3:0x6746
+
** PCI ID 0x15B3:0x6764
+
** PCI ID 0x15B3:0x675A
+
** PCI ID 0x15B3:0x6372
+
** PCI ID 0x15B3:0x6750
+
** PCI ID 0x15B3:0x6368
+
** PCI ID 0x15B3:0x673C
+
** PCI ID 0x15B3:0x6732
+
** PCI ID 0x15B3:0x6354
+
** PCI ID 0x15B3:0x634A
+
** PCI ID 0x15B3:0x6340

* The following adapters from the `mpt2sas` driver have been removed:
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1461123++[1461123], thenzl
+
** SAS2004, PCI ID 0x1000:0x0070
+
** SAS2008, PCI ID 0x1000:0x0072
+
** SAS2108_1, PCI ID 0x1000:0x0074
+
** SAS2108_2, PCI ID 0x1000:0x0076
+
** SAS2108_3, PCI ID 0x1000:0x0077
+
** SAS2116_1, PCI ID 0x1000:0x0064
+
** SAS2116_2, PCI ID 0x1000:0x0065
+
** SSS6200, PCI ID 0x1000:0x007E

* The following adapters from the `megaraid_sas` driver have been removed:
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1306450++[1306450], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1461122++[1461122] thenzl
+
** Dell PERC5, PCI ID 0x1028:0x0015
+
** SAS1078R, PCI ID 0x1000:0x0060
+
** SAS1078DE, PCI ID 0x1000:0x007C
+
** SAS1064R, PCI ID 0x1000:0x0411
+
** VERDE_ZCR, PCI ID 0x1000:0x0413
+
** SAS1078GEN2, PCI ID 0x1000:0x0078
+
** SAS0079GEN2, PCI ID 0x1000:0x0079
+
** SAS0073SKINNY, PCI ID 0x1000:0x0073
+
** SAS0071SKINNY, PCI ID 0x1000:0x0071

* The following adapters from the `qla2xxx` driver have been removed:
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1412339++[1412339]
+
** ISP24xx, PCI ID 0x1077:0x2422
+
** ISP24xx, PCI ID 0x1077:0x2432
+
** ISP2422, PCI ID 0x1077:0x5422
+
** QLE220, PCI ID 0x1077:0x5432
+
** QLE81xx, PCI ID 0x1077:0x8001
+
** QLE10000, PCI ID 0x1077:0xF000
+
** QLE84xx, PCI ID 0x1077:0x8044
+
** QLE8000, PCI ID 0x1077:0x8432
+
** QLE82xx, PCI ID 0x1077:0x8021

* The following adapters from the `qla4xxx` driver have been removed:
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1517346++[1517346]
+
** QLOGIC_ISP8022, PCI ID 0x1077:0x8022
+
** QLOGIC_ISP8324, PCI ID 0x1077:0x8032
+
** QLOGIC_ISP8042, PCI ID 0x1077:0x8042

* The following adapters from the `be2iscsi` driver have been removed:
//mlombard, revers, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1258044++[1258044], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1312460++[1312460]
+
** BladeEngine 2 (BE2) devices
+
*** BladeEngine2 10Gb iSCSI Initiator (generic), PCI ID 0x19a2:0x212
+
*** OneConnect OCe10101, OCm10101, OCe10102, OCm10102 BE2 adapter family, PCI ID 0x19a2:0x702
+
*** OCe10100 BE2 adapter family, PCI ID 0x19a2:0x703
+
** BladeEngine 3 (BE3) devices
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1573599++[1573599]
+
*** OneConnect TOMCAT iSCSI, PCI ID 0x19a2:0x0712
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1573599++[1573599], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1572692++[1572692]
+
*** BladeEngine3 iSCSI, PCI ID 0x19a2:0x0222
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1572692++[1572692]

* The following Ethernet adapters controlled by the `be2net` driver have been removed:
//revers, BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1258044++[1258044], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1312460++[1312460], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1611768++[1611768]
+
** BladeEngine 2 (BE2) devices
+
*** OneConnect TIGERSHARK NIC, PCI ID 0x19a2:0x0700
+
*** BladeEngine2 Network Adapter, PCI ID 0x19a2:0x0211
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1611768++[1611768]
+
** BladeEngine 3 (BE3) devices
+
***  OneConnect TOMCAT NIC, PCI ID 0x19a2:0x0710
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1573599++[1573599], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1572692++[1572692]
+
*** BladeEngine3 Network Adapter, PCI ID 0x19a2:0x0221
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1611768++[1611768]

* The following adapters from the `lpfc` driver have been removed:
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1258044++[1258044], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1312460++[1312460]
+
** BladeEngine 2 (BE2) devices
+
*** OneConnect TIGERSHARK FCoE, PCI ID 0x19a2:0x0704
+
** BladeEngine 3 (BE3) devices
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1572692++[1572692], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1573599++[1573599]
+
*** OneConnect TOMCAT FCoE, PCI ID 0x19a2:0x0714
+
** Fibre Channel (FC) devices
+
*** FIREFLY, PCI ID  0x10df:0x1ae5
+
*** PROTEUS_VF, PCI ID  0x10df:0xe100
+
*** BALIUS, PCI ID  0x10df:0xe131
+
*** PROTEUS_PF, PCI ID  0x10df:0xe180
+
*** RFLY, PCI ID  0x10df:0xf095
+
*** PFLY, PCI ID  0x10df:0xf098
+
*** LP101, PCI ID  0x10df:0xf0a1
+
*** TFLY, PCI ID  0x10df:0xf0a5
+
*** BSMB, PCI ID  0x10df:0xf0d1
+
*** BMID, PCI ID  0x10df:0xf0d5
+
*** ZSMB, PCI ID  0x10df:0xf0e1
+
*** ZMID, PCI ID  0x10df:0xf0e5
+
*** NEPTUNE, PCI ID  0x10df:0xf0f5
+
*** NEPTUNE_SCSP, PCI ID  0x10df:0xf0f6
+
*** NEPTUNE_DCSP, PCI ID  0x10df:0xf0f7
+
*** FALCON, PCI ID  0x10df:0xf180
+
*** SUPERFLY, PCI ID  0x10df:0xf700
+
*** DRAGONFLY, PCI ID  0x10df:0xf800
+
*** CENTAUR, PCI ID  0x10df:0xf900
+
*** PEGASUS, PCI ID  0x10df:0xf980
+
*** THOR, PCI ID  0x10df:0xfa00
+
*** VIPER, PCI ID  0x10df:0xfb00
+
*** LP10000S, PCI ID  0x10df:0xfc00
+
*** LP11000S, PCI ID  0x10df:0xfc10
+
*** LPE11000S, PCI ID  0x10df:0xfc20
+
*** PROTEUS_S, PCI ID  0x10df:0xfc50
+
*** HELIOS, PCI ID  0x10df:0xfd00
+
*** HELIOS_SCSP, PCI ID  0x10df:0xfd11
+
*** HELIOS_DCSP, PCI ID  0x10df:0xfd12
+
*** ZEPHYR, PCI ID  0x10df:0xfe00
+
*** HORNET, PCI ID  0x10df:0xfe05
+
*** ZEPHYR_SCSP, PCI ID  0x10df:0xfe11
+
*** ZEPHYR_DCSP, PCI ID  0x10df:0xfe12
+
** Lancer FCoE CNA devices
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1573217++[1573217], BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1573599++[1573599]
+
*** OCe15104-FM, PCI ID 0x10df:0xe260
+
*** OCe15102-FM, PCI ID 0x10df:0xe260
+
*** OCm15108-F-P, PCI ID 0x10df:0xe260


[id="other-removed-hw-support_{context}"]
== Other removed hardware support

[id="agp_bus_support_removal_{context}"]
=== AGP graphics cards are no longer supported
Graphics cards using the Accelerated Graphics Port (AGP) bus are not supported in {RHEL} 8. Use the graphics cards with the PCI Express bus as the recommended replacement.
//(BZ#1569610)^

[id="fcoe-sw-removal_{context}"]
=== FCoE software removal
//BZ#link:++https://bugzilla.redhat.com/show_bug.cgi?id=1572378++[1572378]
Fibre Channel over Ethernet (FCoE) software has been removed from Red Hat Enterprise Linux 8. Specifically, the `fcoe.ko` kernel module is no longer available for creating software FCoE interfaces over Ethernet adapters and drivers. This change is due to a lack of industry adoption for software-managed FCoE.

Specific changes to Red Hat Enterprise 8 include:

* The `fcoe.ko` kernel module is no longer available. This removes support for software FCoE with Data Center Bridging enabled Ethernet adapters and drivers.

* Link-level software configuration via Data Center Bridging eXchange (DCBX) using `lldpad` is no longer supported for FCoE.
+
** The `fcoe-utils` tools (specifically `fcoemon`) is configured by default to not validate DCB configuration or communicate with `lldpad`.
+
** The `lldpad` integration in `fcoemon` might be permanently disabled.

* The `libhbaapi` and `libhbalinux` libraries are no longer used by `fcoe-utils`, and will not undergo any direct testing from Red Hat.

Support for the following remains unchanged:

* Currently supported offloading FCoE adapters that appear as Fibre Channel adapters to the operating system and do not use the `fcoe-utils` management tools, unless stated in a separate note. This applies to select adapters supported by the `lpfc` FC driver. Note that the `bfa` driver is not included in Red Hat Enterprise Linux 8.

* Currently supported offloading FCoE adapters that do use the `fcoe-utils` management tools but have their own kernel drivers instead of `fcoe.ko` and manage DCBX configuration in their drivers and/or firmware, unless stated in a separate note. The `fnic`, `bnx2fc`, and `qedf` drivers will continue to be fully supported in Red Hat Enterprise Linux 8.

* The `libfc.ko` and `libfcoe.ko` kernel modules that are required for some of the supported drivers covered by the previous statement.

For more information, see xref:software-fcoe-and-fibre-channel-no-longer-support-the-target-mode_file-systems-and-storage[Software FCoE and Fibre Channel no longer support the target mode].

[id="e1000_{context}"]
=== The *e1000* network driver is not supported in RHEL 8
In Red Hat Enterprise Linux 8, the *e1000* network driver is not supported. This affects both bare metal and virtual environments. However, the newer *e1000e* network driver continues to be fully supported in RHEL 8.
//(BZ#1596240)


[id="tulip_{context}"]
=== RHEL 8 does not support the *tulip* driver
With this update, the *tulip* network driver is no longer supported. As a consequence, when using RHEL 8 on a Generation 1 virtual machine (VM) on the Microsoft Hyper-V hypervisor, the "Legacy Network Adapter" device does not work, which causes PXE installation of such VMs to fail.

For the PXE installation to work, install RHEL 8 on a Generation 2 Hyper-V VM. If you require a RHEL 8 Generation 1 VM, use ISO installation.
//(BZ#1534870)


[id="qla2xxx-target-mode_{context}"]
=== The `qla2xxx` driver no longer supports target mode
Support for target mode with the `qla2xxx` QLogic Fibre Channel driver has been disabled. The effects of this change are:

* The kernel no longer provides the `tcm_qla2xxx` module.
* The `rtslib` library and the `targetcli` utility no longer support `qla2xxx`.

Initiator mode with `qla2xxx` is still supported.
//(link:https://bugzilla.redhat.com/show_bug.cgi?id=1666377[BZ#1666377])
