
:_mod-docs-content-type: CONCEPT


[id="con_supported-upgrade-paths-rhel-7-to-rhel-8_{context}"]
= Supported upgrade paths

[role="_abstract"]
The in-place upgrade replaces the RHEL 7 operating system (OS) on your system with a RHEL 8 version.

Currently, it is possible to perform an in-place upgrade from RHEL 7 to the following target RHEL 8 minor versions:

.Supported upgrade paths
[options="header"]
|====
| System configuration | Source OS version | Target OS version

.2+|RHEL
.2+| RHEL 7.9

| RHEL 8.8 

| RHEL 8.10 (default)

.2+| RHEL with SAP HANA
.2+| RHEL 7.9

| RHEL 8.8 (default)

| RHEL 8.10
|====

For more information about supported upgrade paths, see link:https://access.redhat.com/articles/4263361[Supported in-place upgrade paths for Red Hat Enterprise Linux] and the link:https://access.redhat.com/support/policy/ipu-support[In-place upgrade Support Policy].

