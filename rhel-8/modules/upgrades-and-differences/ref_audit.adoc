:_mod-docs-content-type: REFERENCE
// included in security assembly_security.adoc

[id="audit_{context}"]
= Audit

[id="audispd_{context}"]
== Audit 3.0 replaces `audispd` with `auditd`
With this update, functionality of `audispd` has been moved to `auditd`. As a result, `audispd` configuration options are now part of `auditd.conf`. In addition, the `plugins.d` directory has been moved under `/etc/audit`. The current status of `auditd` and its plug-ins can now be checked by running the `service auditd state` command.
// (BZ#1616428)
