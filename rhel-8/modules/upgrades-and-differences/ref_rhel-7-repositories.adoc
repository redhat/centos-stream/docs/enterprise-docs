:_mod-docs-content-type: REFERENCE

[appendix,id="appendix-rhel-7-repositories_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= RHEL 7 repositories
// In the title of a reference module, include nouns that are used in the body text. For example, "Keyboard shortcuts for ___" or "Command options for ___." This helps readers and search engines find the information quickly.

[role="_abstract"]
Before the upgrade, ensure you have appropriate repositories enabled as described in step 4 of the procedure in
ifdef::upgrading-to-rhel-8[]
xref:preparing-a-rhel-7-system-for-the-upgrade_{context}[Preparing a RHEL 7 system for the upgrade].
endif::[]
ifndef::upgrading-to-rhel-8[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/upgrading_from_rhel_7_to_rhel_8/index#preparing-a-rhel-7-system-for-the-upgrade_upgrading-from-rhel-7-to-rhel-8[Preparing a RHEL 7 system for the upgrade].
endif::[]

////
It is especially necessary to have the appropriate link:https://access.redhat.com/support/policy/updates/errata#Extended_Update_Support[Extended Update Support] (EUS) repositories enabled if they are available for the given architecture and repository. Make sure your subscription includes the _Red Hat Enterprise Linux for <architecture> - Extended Update Support_ product, where _<architecture>_ is one of the following: _x86_64, Power, little endian, IBM z Systems_. You can purchase the link:https://www.redhat.com/en/store/extended-update-support-add#?sku=RH00030[EUS add-on for the 64-bit Intel architecture] or link:https://access.redhat.com/support/contact/sales[contact a sales representative].

NOTE: EUS repositories are not available for the following architectures: 64-bit ARM, IBM POWER9 (little endian), IBM Z (Structure A).
//EUS repositories are not available for architectures that require kernel version 4.14, which is provided by the kernel-alt package in RHEL 7.
////

If you plan to use Red Hat Subscription Manager during the upgrade, you *must enable* the following repositories before the upgrade by using the `subscription-manager repos --enable _repository_id_` command:

//[id="table_compulsory_repositories"]
//.Compulsory repositories
[options="header",cols="4,3,7"]
|===
|Architecture|Repository|Repository ID
.2+|64-bit Intel|Base|`rhel-7-server-rpms`
|Extras|`rhel-7-server-extras-rpms`
.2+|IBM POWER8 (little endian)|Base|`rhel-7-for-power-le-rpms`
|Extras|`rhel-7-for-power-le-extras-rpms`
.2+|IBM Z|Base|`rhel-7-for-system-z-rpms`
|Extras|`rhel-7-for-system-z-extras-rpms`
|===


You *can enable* the following repositories before the upgrade by using the `subscription-manager repos --enable _repository_id_` command:
//[id="table_voluntary_repositories"]
//.Voluntary repositories
[options="header",cols="4,3,7"]
|===
|Architecture|Repository|Repository ID
.2+|64-bit Intel|Optional|`rhel-7-server-optional-rpms`
|Supplementary|`rhel-7-server-supplementary-rpms`
.2+|IBM POWER8 (little endian)|Optional|`rhel-7-for-power-le-optional-rpms`
|Supplementary|`rhel-7-for-power-le-supplementary-rpms`
.2+|IBM Z|Optional|`rhel-7-for-system-z-optional-rpms`
|Supplementary|`rhel-7-for-system-z-supplementary-rpms`
|===

////
The following repositories *should be disabled* by using the `subscription-manager repos --disable _repoid_` command, and their *corresponding EUS repositories enabled* (see the tables above):
//[id="table_repositories_to_be_replaced_by_eus"]
//.Repositories to be replaced by EUS
[id="repositories-to-be-disabled",options="header",cols="4,3,7"]
|===
|Architecture|Repository|Repository ID
.3+|64-bit Intel|Base|`rhel-7-server-rpms`
|Optional|`rhel-7-server-optional-rpms`
|Supplementary|`rhel-7-server-supplementary-rpms`
.3+|IBM POWER8 (little endian)|Base|`rhel-7-for-power-le-rpms`
|Optional|`rhel-7-for-power-le-optional-rpms`
|Supplementary|`rhel-7-for-power-le-supplementary-rpms`
.3+|IBM Z|Base|`rhel-7-for-system-z-rpms`
|Optional|`rhel-7-for-system-z-optional-rpms`
|Supplementary|`rhel-7-for-system-z-supplementary-rpms`
|===
////

NOTE: If you have enabled a RHEL 7 Optional or a RHEL 7 Supplementary repository before an in-place upgrade, [application]`Leapp` enables the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/package_manifest/codereadylinuxbuilder-repository[RHEL 8 CodeReady Linux Builder] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/package_manifest/supplementary-repository[RHEL 8 Supplementary] repositories, respectively.

If you decide to use custom repositories, enable them per instructions in link:https://access.redhat.com/articles/4977891#repos-config[Configuring custom repositories].
