:_mod-docs-content-type: REFERENCE
[id="virtualization_{context}"]
= Virtualization

// As a system/virt administrator, I want to know what's different with virtualization in RHEL 8 as opposed to RHEL 7, so I know whether it's a good idea to upgrade my RHEL.

[id="Virtual-machines-can-now-be-managed-using-the-web-console_{context}"]
== Virtual machines can now be managed using the web console

The Virtual Machines page can now be added to the RHEL 8 web console interface, which enables the user to create and manage libvirt-based virtual machines (VMs).

In addition, the Virtual Machine Manager (`virt-manager`) application has been deprecated, and may become unsupported in a future major release of RHEL.

Note, however, that the web console currently does not provide all of the virtual management features that `virt-manager` does. For details about the differences in available features between the RHEL 8 web console and the Virtual Machine Manager, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-machines-in-the-web-console_configuring-and-managing-virtualization#differences-between-virtualization-features-in-virtual-machine-manager-and-the-rhel-8-web-console_managing-virtual-machines-in-the-web-console[Configuring and managing virtualization] document.


[id="The-q35-machine-type-is-now-supported-by-virtualization_{context}"]
== The Q35 machine type is now supported by virtualization

Red hat Enterprise Linux 8 introduces the support for `Q35`, a more modern PCI Express-based machine type. This provides a variety of improvements in features and performance of virtual devices, and ensures that a wider range of modern devices are compatible with virtualization. In addition, virtual machines created in Red Hat Enterprise Linux 8 are set to use Q35 by default.

Note that the previously default `PC` machine type has become deprecated and may become unsupported in a future major release of RHEL. However, changing the machine type of existing VMs from `PC` to `Q35` is not recommended.

Notable differences between `PC` and `Q35` include:

* Older operating systems, such as Windows XP, do not support Q35 and will not boot if used on a Q35 VM.

* Currently, when using RHEL 6 as the operating system on a Q35 VM, hot-plugging a PCI device to that VM in some cases does not work. In addition, certain legacy virtio devices do not work properly on RHEL 6 Q35 VMs.
+
Therefore, using the PC machine type is recommended for RHEL 6 VMs.
+
// KI https://bugzilla.redhat.com/show_bug.cgi?id=1699213 + https://bugzilla.redhat.com/show_bug.cgi?id=1614127

* Q35 emulates PCI Express (PCI-e) buses instead of PCI. As a result, a different device topology and addressing scheme is presented to the guest OS.

* Q35 has a built-in SATA/AHCI controller, instead of an IDE controller.
+
//If the transitioned VM is using a guest operating system that does not support SATA, its attached virtual devices may stop working.

// * If using a Q35 VM with Open Virtual Machine Firmware (OVMF), the VM fails to boot if multiple virtual RAM devices with over 4 GB RAM are assigned to it.

* The SecureBoot feature only works on Q35 VMs.

[id="Removed-virtualization-functionality_{context}"]
== Removed virtualization functionality

[discrete]
=== The `cpu64-rhel6` CPU model has been deprecated and removed
The `cpu64-rhel6` QEMU virtual CPU model has been deprecated in RHEL 8.1, and has been removed from RHEL 8.2. It is recommended that you use the other CPU models provided by QEMU and `libvirt`, according to the CPU present on the host machine.
//(BZ#1741346)

[discrete]
=== IVSHMEM has been disabled
The inter-VM shared memory device (IVSHMEM) feature, which provides shared memory between multiple virtual machines, is now disabled in Red Hat Enterprise Linux 8. A virtual machine configured with this device will fail to boot. Similarly, attempting to hot-plug such a device device will fail as well.
//(BZ#1621817)^

[discrete]
=== `virt-install` can no longer use NFS locations
With this update, the `virt-install` utility cannot mount NFS locations. As a consequence, attempting to install a virtual machine using `virt-install` with a NFS address as a value of the `--location` option fails. To work around this change, mount your NFS share prior to using `virt-install`, or use a HTTP location.
//(BZ#1643609)^

[discrete]
=== RHEL 8 does not support the tulip driver

With this update, the tulip network driver is no longer supported. As a consequence, when using RHEL 8 on a Generation 1 virtual machine (VM) on the Microsoft Hyper-V hypervisor, the "Legacy Network Adapter" device does not work, which causes PXE installation of such VMs to fail.

For the PXE installation to work, install RHEL 8 on a Generation 2 Hyper-V VM. If you require a RHEL 8 Generation 1 VM, use ISO installation.
// (BZ#1534870)^

[discrete]
=== LSI Logic SAS and Parallel SCSI drivers are not supported

The LSI Logic SAS driver (`mptsas`) and LSI Logic Parallel driver (`mptspi`) for SCSI are no longer supported. As a consequence, the drivers can be used for installing RHEL 8 as a guest operating system on a VMWare hypervisor to a SCSI disk, but the created VM will not be supported by Red Hat.
// (BZ#1651803)^

[discrete]
=== Installing virtio-win no longer creates a floppy disk image with the Windows drivers

Due to the limitation of floppy drives, virtio-win drivers are no longer provided as floppy images. Users should use the ISO image instead.
// (BZ#1533540)^
