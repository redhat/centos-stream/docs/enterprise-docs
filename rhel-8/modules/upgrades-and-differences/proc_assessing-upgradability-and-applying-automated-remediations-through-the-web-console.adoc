:_mod-docs-content-type: PROCEDURE
// Module included in the following modules (= context is still "upgrading-from-rhel-7-to-rhel-8"):
//
// proc_reviewing-the-pre-upgrade-report

:experimental:

[id="assessing-upgradability-and-applying-automated-remediations-through-the-web-console_{context}"]
= Assessing upgradability and applying automated remediations through the web console

[role="_abstract"]
Identify potential problems in the pre-upgrade phase and apply automated remediations by using the web console.


.Prerequisites

* You have completed the steps listed in
ifdef::upgrading-to-rhel-8[]
xref:assembly_preparing-for-the-upgrade_{context}[Preparing for the upgrade].
endif::[]
ifndef::upgrading-to-rhel-8[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/assembly_preparing-for-the-upgrade_upgrading-from-rhel-7-to-rhel-8[Preparing for the upgrade].
endif::[]

.Procedure
. Install the `cockpit-leapp` plug-in:
+
[subs="quotes,attributes"]
----
# *dnf install cockpit-leapp*
----
+

Log in to the web console as `root` or as a user that has permissions to enter administrative commands with `sudo`. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/managing_systems_using_the_rhel_7_web_console/index[Managing systems using the RHEL web console] for more information about the web console.


. On your RHEL 7 system, perform the pre-upgrade phase either from the command line or from the web console terminal:
+
[subs="quotes,attributes"]
----
# *leapp preupgrade  --target _&lt;target_os_version&gt;_*
----
+
Replace _&lt;target_os_version&gt;_ with the target OS version, for example 8.10. If no target OS version is defined, `Leapp` uses the default target OS version specified in the table 1.1 in
ifdef::upgrading-to-rhel-8[]
xref:con_supported-upgrade-paths-rhel-7-to-rhel-8_upgrading-from-rhel-7-to-rhel-8[Supported upgrade paths].
endif::[]
ifndef::upgrading-to-rhel-8[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/con_supported-upgrade-paths-rhel-7-to-rhel-8_upgrading-from-rhel-7-to-rhel-8[Supported upgrade paths].
endif::[]
+
* If you are using link:https://access.redhat.com/articles/4977891#repos[custom repositories] from the `/etc/yum.repos.d/` directory for the upgrade, enable the selected repositories as follows:
+
[subs="+quotes,attributes"]
----
# *leapp preupgrade --enablerepo _<repository_id1>_ --enablerepo _<repository_id2>_ ...*
----

* If you are link:https://access.redhat.com/articles/4977891#upgrade-without-rhsm[upgrading without RHSM] or using RHUI, add the `--no-rhsm` option.

* If you have an link:https://access.redhat.com/articles/rhel-eus[Extended Upgrade Support (EUS)], Advanced Update Support (AUS), or link:https://access.redhat.com/solutions/3082481[Update Services for SAP Solutions (E4S)] (Red Hat Knowledgebase) subscription, add the `--channel _<channel>_` option: 
** If you are upgrading to RHEL 8.8, replace _channel_ with the channel name, for example, `eus`, `aus`, or `e4s`. Note that SAP HANA customers should perform the in-place upgrade by using the link:https://access.redhat.com/solutions/5154031[How to in-place upgrade SAP environments from RHEL 7 to RHEL 8] Red Hat Knowledgebase solution.
** If you are upgrading to RHEL 8.10, replace _channel_ with `ga`.

. In the web console, select *Upgrade Report* from the navigation menu to review all reported problems. *Inhibitor* problems prevent you from upgrading until you have resolved them. For more information on the different problems that can appear in the report, see the pre-upgrade steps in the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/4120411[How do I upgrade from Red Hat Enterprise Linux 7 to Red Hat Enterprise Linux 8?] 
+
To view a problem in detail, select the row to open the Detail pane.
+
[id="image_web_console_report"]
.In-place upgrade report in the web console
image::leapp-web-console-report.png[In-place upgrade report in the web console]
+
The report contains the following risk factor levels:

High:: Very likely to result in a deteriorated system state.
Medium:: Can impact both the system and applications.
Low:: Should not impact the system but can have an impact on applications.
Info:: Informational with no expected impact to the system or applications.
+
[NOTE]
====
Depending on the severity and impact of the found problems and the amount of work required to resolve them, it might be preferable to perform a clean install of RHEL 8 instead of proceeding with the in-place upgrade.
====

. In certain configurations, the `Leapp` utility generates true or false questions that you must answer manually. If the Upgrade Report contains a *Missing required answers in the answer file* row, complete the following steps:
.. Select the *Missing required answers in the answer file* row to open the *Detail* pane. The default answer is stated at the end of the remediation command.
.. To confirm the default answer, select *Add to Remediation Plan* to execute the remediation later or *Run Remediation* to execute the remediation immediately. 
.. To select the non-default answer instead, execute the `leapp answer` command in the terminal, specifying the question you are responding to and your confirmed answer.
+
[subs="+quotes,attributes"]
----
# *leapp answer --section _<question_section>_._<field_name>_=_<answer>_*
----
+
For example, to confirm a `False` response to the question *Disable pam_pkcs11 module in PAM configuration?*, execute the following command
+
[subs="quotes,attributes"]
----
# *leapp answer --section remove_pam_pkcs11_module_check.confirm=False*
----
+
[NOTE]
====
You can also manually edit the `/var/log/leapp/answerfile` file, uncomment the confirm line of the file by deleting the `#` symbol, and confirm your answer as `True` or `False`. For more information, see the 
ifdef::upgrading-to-rhel-8[]
xref:example-leapp_answerfile[Leapp answerfile example].
endif::[]
ifndef::upgrading-to-rhel-8[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/troubleshooting_upgrading-from-rhel-7-to-rhel-8#troubleshooting-tips_troubleshooting[Leapp answerfile example]  in the Troubleshooting tips section.
endif::[]
====

. Some problems have remediation commands that you can run to automatically resolve the problems. You can run remediation commands individually or all together in the remediation command.
.. To run a single remediation command, open the *Detail* pane for the problem and click *Run Remediation*. 
.. To add a remediation command to the remediation plan, open the *Detail* pane for the problem and click *Add to Remediation Plan*.
+
[id="image_detail_pane"]
.Detail pane
image::leapp-detail-pane.png[Detail pane,width=53%]

.. To run the remediation plan containing all added remediation commands, click the *Remediation plan* link in the top right corner above the report. Click *Execute Remediation Plan* to execute all listed commands.

. After reviewing the report and resolving all reported problems, repeat steps 3-7 to rerun the report to verify that you have resolved all critical issues.


