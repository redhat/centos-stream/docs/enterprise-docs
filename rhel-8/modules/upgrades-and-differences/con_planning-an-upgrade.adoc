:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_upgrading-from-rhel-7-to-rhel-8


[id="planning-an-upgrade_{context}"]
= Planning an upgrade

[role="_abstract"]
*An in-place upgrade is the recommended and supported way to upgrade your system to the next major version of RHEL.*

Consider the following before upgrading to RHEL 8:

* *Operating system* - The operating system is upgraded by the [application]`Leapp` utility under the following conditions:
** *RHEL 7.9* of the Server variant on the 64-bit Intel, IBM POWER 8 (little endian), and 64-bit IBM Z architectures and, when on SAP HANA, on the 64-bit Intel architecture, is installed.
+
////
*** *RHEL 7.6* on architectures that *require kernel version 4.14*: IBM POWER 9 (little endian) or 64-bit IBM Z (Structure A)
+
[NOTE]
====
The IBM POWER 9 (little endian) and 64-bit IBM Z (Structure A) architectures have reached end of life. The final upgrade path for these architectures is from RHEL 7.6 to RHEL 8.4. Later releases to the in-place upgrade, including new upgrade paths, features, and bug fixes, will not include these architectures.
====
////
See link:https://access.redhat.com/articles/4263361[Supported in-place upgrade paths for Red Hat Enterprise Linux] for more information.
** Minimum link:https://access.redhat.com/articles/rhel-limits[hardware requirements] for RHEL 8 are met.
** You have access to up-to-date RHEL 7.9 and the target operating system (OS) version (for example, RHEL 8.10) content. See
ifdef::upgrading-to-rhel-8[]
xref:preparing-a-rhel-7-system-for-the-upgrade_upgrading-from-rhel-7-to-rhel-8[Preparing a RHEL 7 system for the upgrade]
endif::[]
ifndef::upgrading-to-rhel-8[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/assembly_preparing-for-the-upgrade_upgrading-from-rhel-7-to-rhel-8#preparing-a-rhel-7-system-for-the-upgrade_upgrading-from-rhel-7-to-rhel-8[Preparing a RHEL 7 system for the upgrade]
endif::[]
for more information.
* *Applications* - You can migrate applications installed on your system by using [application]`Leapp`. However, in certain cases, you have to create custom actors, which specify actions to be performed by [application]`Leapp` during the upgrade, for example, reconfiguring an application or installing a specific hardware driver. For more information, see link:https://access.redhat.com/articles/4977891#actors[Handling the migration of your custom and third-party applications]. Note that Red Hat does not support custom actors.
* *Security* - You should evaluate this aspect before the upgrade and take additional steps when the upgrade process completes. Consider especially the following:
** Before the upgrade, define the security standard your system needs to comply with and understand the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/considerations_in_adopting_rhel_8/security_considerations-in-adopting-rhel-8[security changes in RHEL 8].
** During the upgrade process, the [application]`Leapp` utility sets SELinux mode to permissive.
** In-place upgrades of systems in Federal Information Processing Standard (FIPS) mode cannot be fully automated by `Leapp`. If your scenario requires upgrading RHEL 7 systems running in *FIPS mode*, you must:
+
[IMPORTANT]
====
To ensure that all cryptographic keys conform to the FIPS 140-2 standard, start a link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/assembly_installing-a-rhel-8-system-with-fips-mode-enabled_security-hardening[new installation in FIPS mode] instead of performing an in-place upgrade of an already deployed system. Use the following steps only if the security policy of your company allows this alternative upgrade process and if you can ensure the regeneration and reevaluation of all cryptographic keys on the upgraded system.
====
+
--
. link:https://access.redhat.com/solutions/2422061[Disable FIPS mode] (Red Hat Knowledgebase) in RHEL 7.
. Upgrade the system using `Leapp`. You must follow the pre-upgrade, upgrade, and post-upgrade instructions as in any other in-place upgrade.
. Enable FIPS mode in RHEL 8. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening#switching-the-system-to-fips-mode_using-the-system-wide-cryptographic-policies[Switching the system to FIPS mode in the RHEL 8 Security hardening document] for details.
. Re-generate cryptographic keys on your system. See 
ifdef::upgrading-to-rhel-8[]
xref:locations-of-cryptographic-keys-in-rhel-8_upgrading-from-rhel-7-to-rhel-8[]
endif::[]
ifndef::upgrading-to-rhel-8[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/assembly_preparing-for-the-upgrade_upgrading-from-rhel-7-to-rhel-8#locations-of-cryptographic-keys-in-rhel-8[Locations of cryptographic keys in RHEL 8]
endif::[]
for more information.
--
+
** After the upgrade is finished, re-evaluate and re-apply your security policies. For information about applying security policies that have been disabled during the upgrade or newly introduced in RHEL 8, see
ifdef::upgrading-to-rhel-8[]
xref:applying-security-policies_{context}[Applying security policies].
endif::[]
ifndef::upgrading-to-rhel-8[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/applying-security-policies_upgrading-from-rhel-7-to-rhel-8[Applying security policies].
endif::[]
* *Storage and file systems* - Always back up your system prior to upgrading. For example, you can use the link:https://access.redhat.com/solutions/2115051[Relax-and-Recover (ReaR) utility] (Red Hat Knowledgebase), link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/logical_volume_manager_administration/lv#snapshot_command[LVM snapshots], link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/logical_volume_manager_administration/lv#raid-splitmerge[RAID splitting], or a virtual machine snapshot. 
+
[NOTE]
====
File systems formats are intact. As a result, file systems have the same limitations as when they were originally created.
====
* *High Availability* -  If you are using the High Availability add-on, follow the link:https://access.redhat.com/articles/2059253[Recommended Practices for Applying Software Updates to a RHEL High Availability or Resilient Storage Cluster] Knowledgebase article.
* *Downtime* - The upgrade process can take from several minutes to several hours.
* *Satellite*
** *Client* - If you manage your hosts through Satellite, you can upgrade multiple hosts simultaneously from RHEL 7 to RHEL 8 by using the Satellite web UI. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_satellite/6.16/html/managing_hosts/upgrading_hosts_to_next_major_release_managing-hosts[Upgrading hosts to next major Red Hat Enterprise Linux release].
** *Server and Capsule* - You can upgrade Satellite Servers and Capsules starting in Satellite 6.11. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_satellite/6.11/html/upgrading_and_updating_red_hat_satellite/upgrading-satellite-or-proxy-in-place-using-leapp_upgrade-guide#upgrading-satellite-or-proxy-in-place-using-leapp_upgrade-guide[Upgrading Satellite or Capsule to Red Hat Enterprise Linux 8 In-Place Using Leapp].
* *SAP HANA* - If you are using SAP HANA, see the Knowledgebase solution link:https://access.redhat.com/solutions/5154031[How to in-place upgrade SAP environments from RHEL 7 to RHEL 8]. Note that the upgrade path for RHEL with SAP HANA might differ.
* *RHEL for Real Time* - Upgrades on real-time systems are supported.
* *Real Time for Network Functions Virtualization (NFV) in Red Hat OpenStack Platform* - Upgrades on real-time systems are supported. 
* *Red Hat Software Collections (RHSCLs)* - RHSCLs are not fully migrated during the in-place upgrade. RHEL 8 packages usually automatically replace RHSCL packages, but customized configurations and data must be migrated and configured manually. For example, if you have installed databases from RHSCL, you should dump all data before the upgrade to prevent data loss during RHSCL package removal and then restore the data as needed after the system upgrade. Note that when upgrading the Red Hat Satellite server, RHSCL packages required by the project are migrated automatically.
* *Red Hat JBoss Enterprise Application Platform (EAP)* - JBoss EAP is not supported for the upgrade to RHEL 9. You must manually install and configure JBoss EAP on your system after the upgrade. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/7002072[In-place Migrating of Jboss EAP and websphere servers along with Linux using leapp utility].
* *Public clouds* - The in-place upgrade is supported for on-demand Pay-As-You-Go (PAYG) instances using link:https://access.redhat.com/documentation/en-us/red_hat_update_infrastructure/4/html/configuring_and_managing_red_hat_update_infrastructure/assembly_cmg-about-rhui4_configuring-and-managing-red-hat-update-infrastructure[Red Hat Update Infrastructure (RHUI)] only on Amazon Web Services (AWS), Microsoft Azure, and  Google Cloud Platform. The in-place upgrade is also supported for Bring Your Own Subscription instances on all public clouds that use Red Hat Subscription Manager (RHSM) for a RHEL subscription.
* *Language* - All `Leapp` reports, logs, and other generated documentation are in English, regardless of the language configuration.
* *Boot loader* - It is not possible to switch the boot loader from BIOS to UEFI on RHEL 7 or RHEL 8. If your RHEL 7 system uses BIOS and you want your RHEL 8 system to use UEFI, perform a fresh install of RHEL 8 instead of an in-place upgrade. For more information, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/1990803[Is it possible to switch the BIOS boot to UEFI boot on preinstalled Red Hat Enterprise Linux machine?]
* *Known limitations* - Notable known limitations of [application]`Leapp` currently include:
** Encryption of the whole disk or a partition, or file-system encryption currently cannot be used on a system targeted for an in-place upgrade.
** Network based multipath and network storage that use Ethernet or Infiniband are not supported for the upgrade. This includes SAN using FCoE and booting from SAN using FC. Note that SAN using FC are supported.
** The in-place upgrade is currently unsupported for on-demand PAYG instances on the remaining public clouds that use Red Hat Update Infrastructure but not RHSM for a RHEL subscription.
** The in-place upgrade is not supported for systems with any Ansible products, including Ansible Tower, installed. To use a RHEL 7 Ansible Tower installation on RHEL 8, see the Red Hat Knowledgebase solution link:https://access.redhat.com/solutions/5994961[How do I migrate my Ansible Automation Platform installation from one environment to another?].

ifdef::upgrading-to-rhel-8[]
See also xref:known-issues_troubleshooting[Known Issues].
endif::[]
ifndef::upgrading-to-rhel-8[]
See also link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/troubleshooting_upgrading-from-rhel-7-to-rhel-8#known-issues_troubleshooting[Known Issues].
endif::[]

You can use *link:https://console.redhat.com/insights/dashboard[Red Hat Insights]* to determine which of the systems you have registered to Insights is on a supported upgrade path to RHEL 8. To do so, go to the respective link:https://console.redhat.com/insights/advisor/recommendations/el7_to_el8_upgrade%7CRHEL7_TO_RHEL8_UPGRADE_AVAILABLE_V4?limit=20&offset=0&sort=-last_seen&name=#SIDs=&tags=[Advisor recommendation] in Insights, enable the recommendation under the _Actions_ drop-down menu, and inspect the list under the _Affected systems_ heading. Note that the Advisor recommendation considers only the RHEL 7 minor version and does not perform a pre-upgrade assessment of the system. See also link:https://access.redhat.com/documentation/en-us/red_hat_insights/2023/html/assessing_rhel_configuration_issues_using_the_red_hat_insights_advisor_service/assembly-adv-assess-recommendations[Advisor-service recommendations overview].

[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/articles/7012979[The best practices and recommendations for performing RHEL Upgrade using Leapp] (Red Hat Knowledgebase)
* link:https://access.redhat.com/articles/7013172[Leapp upgrade FAQ] (Red Hat Knowledgebase)
