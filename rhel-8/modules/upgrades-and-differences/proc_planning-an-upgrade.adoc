:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="planning-an-upgrade_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Planning an upgrade
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

An in-place upgrade is the recommended way to upgrade your system to a later major version of RHEL.

[IMPORTANT]
====
Before you start, Red{nbsp}Hat recommends that you read this reference, including the linked documents, to prevent a situation when certain changes to your system have been done but you are unable to proceed with the upgrading process. Especially, ensure that your system meets the requirements described in this document and that you are aware of the known limitations.
====


== Requirements

The following are the general criteria a system must meet to upgrade from RHEL 6 to RHEL 8:

* The architecture is Intel 64 or 64-bit IBM Z.
* The RHEL Server variant is installed.
* The FIPS mode is disabled.
* The system contains no LUKS-encrypted partitions or volumes.
* Minimum link:https://access.redhat.com/articles/rhel-limits[hardware requirements] for RHEL 8 are met.
* Access to repositories with RHEL 6, RHEL 7 and RHEL 8 content is provided.

Note that there can be further requirements and limitations. For details, see:

* The link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/upgrading_from_rhel_6_to_rhel_7/index#planning-an-upgrade-from-rhel-6-to-rhel-7upgrading-from-rhel-6-to-rhel-7[Planning an upgrade section in the Upgrading from RHEL 6 to RHEL 7] documentation
* The link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/planning-an-upgrade_upgrading-from-rhel-7-to-rhel-8[Planning an upgrade section in the Upgrading from RHEL 7 to RHEL 8] documentation


== Considerations

You should consider the following before upgrading:

* Significant changes between the RHEL major releases
+
For details, see:
+
** The link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/migration_planning_guide/chap-red_hat_enterprise_linux-migration_planning_guide-major_changes_and_migration_considerations[Major changes and migration considerations] chapter in the [citetitle]`RHEL 7 Migration Planning Guide`
+
** The link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/considerations_in_adopting_rhel_8/index[Considerations in adopting RHEL 8] document

* Applications not distributed by Red Hat
+
If you run applications on a system you want to upgrade that are not provided by Red Hat, consider the following:
+
** For RPM-based applications:
+
*** You require packages that are compatible with the particular target version.
*** The packages, including all dependencies, are available in a repository to which you have access.
+
** For non-RPM-based applications:
*** Dependencies and libraries might have changed during these two major version upgrades. Make sure that the dependencies and libraries are available on the particular target version.
*** If the applications are written in an interpreted language, such as Python or Ruby, review whether all libraries are available on the target version.


== Overview of the upgrading process

An in-place upgrade from RHEL 6 to RHEL 8 requires the following major steps:

. Prepare the RHEL 6 system for the upgrade and update the RHEL 6 system to the latest version of RHEL 6.10.
. Perform a pre-upgrade assessment of the RHEL 6 system and resolve reported problems.
. Perform an in-place upgrade to RHEL 7.9.
. Prepare the RHEL 7 system for the upgrade to RHEL 8 and update the RHEL 7 system to the latest version of RHEL 7.9.
. Perform a pre-upgrade assessment of the RHEL 7 system and resolve problems identified in this phase.
. Perform an in-place upgrade to RHEL 8.
. Verify the state of the upgraded system.

For detailed instructions, see the following chapters.
