:_mod-docs-content-type: REFERENCE

[appendix,id="locations-of-cryptographic-keys-in-rhel-8_{context}"]
= Locations of cryptographic keys in RHEL 8

After you upgrade a system that is running in Federal Information Processing Standard (FIPS) mode, you must regenerate and otherwise ensure the FIPS compliance of all cryptographic keys. Some well-known locations for such keys are in the following table. Note that the list is not complete, and you might check also other locations.

.Locations of cryptographic keys in RHEL 8
[options="header"]
|====
|Application|Locations of keys|Notes
|Apache mod_ssl|`/etc/pki/tls/private/localhost.key`|The `/usr/lib/systemd/system/httpd-init.service` service runs the `/usr/libexec/httpd-ssl-gencerts` file if the `/etc/pki/tls/private/localhost.key` does not exist.
|Bind9 RNDC|`/etc/rndc.key`|The `named-setup-rndc.service` service runs the `/usr/libexec/generate-rndc-key.sh` script, which generates the `/etc/rndc.key` file.
|Cyrus IMAPd|`/etc/pki/cyrus-imapd/cyrus-imapd-key.pem`|The `cyrus-imapd-init.service` service generates the `/etc/pki/cyrus-imapd/cyrus-imapd-key.pem` file on its startup.
|DNSSEC-Trigger|`/etc/dnssec-trigger/dnssec_trigger_control.key`| The `dnssec-triggerd-keygen.service`  service generates the `/etc/dnssec-trigger/dnssec_trigger_control.key` file.
|Dovecot|`/etc/pki/dovecot/private/dovecot.pem`|The `dovecot-init.service` service generates the `/etc/pki/dovecot/private/dovecot.pem` file on its startup.
|OpenPegasus|`/etc/pki/Pegasus/file.pem`|The `tog-pegasus.service` service generates the `/etc/pki/Pegasus/file.pem` private key file.
|OpenSSH|`/etc/ssh/ssh_host*_key`| Ed25519 and DSA keys are not FIPS-compliant. +
 +
Custom Diffie-Hellman (DH) parameters are not supported in FIPS mode. Comment out the `ModuliFile` option in the `sshd_config` file to ensure compatibility with FIPS mode. You can keep the `moduli` file (`/etc/ssh/moduli` by default) in place.
|Postfix|`/etc/pki/tls/private/postfix.key`|The post-installation script contained in the `postfix` package generates the `/etc/pki/tls/private/postfix.key` file.
|RHEL web console|`/etc/cockpit/ws-certs.d/`|The web console runs the `/usr/libexec/cockpit-certificate-ensure –for-cockpit-tls` file, which creates keys in the `/etc/cockpit/ws-certs.d/` directory.
|Sendmail|`/etc/pki/tls/private/sendmail.key`|The post-installation script contained in the `sendmail` package generates the `/etc/pki/tls/private/sendmail.key` file.
|====

To ensure the FIPS compliance of cryptographic keys of third-party applications, refer to the corresponding documentation of the respective applications. Furthermore:

* Any service that opens a port might use a TLS certificate.
** Not all services generate cryptographic keys automatically, but many services that start up automatically by default often do so. 
* Focus also on services that use any cryptographic libraries such as NSS, GnuTLS, OpenSSL, and libgcrypt.
* Check also backup, disk-encryption, file-encryption, and similar applications.

[IMPORTANT]
====
Because FIPS mode in RHEL 8 restricts DSA keys, DH parameters, RSA keys shorter than 1024 bits, and some other ciphers, old cryptographic keys stop working after the upgrade from RHEL 7. See the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/considerations_in_adopting_rhel_8/security_considerations-in-adopting-rhel-8#changes-in-core-cryptographic-components_security[Changes in core cryptographic components] section in the Considerations in adopting RHEL 8 document and the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[Using system-wide cryptographic policies] chapter in the RHEL 8 Security hardening document for more information. 
====


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening#switching-the-system-to-fips-mode_using-the-system-wide-cryptographic-policies[Switching the system to FIPS mode in the RHEL 8 Security hardening document]
* `update-crypto-policies(8)` and `fips-mode-setup(8)` man pages on your system