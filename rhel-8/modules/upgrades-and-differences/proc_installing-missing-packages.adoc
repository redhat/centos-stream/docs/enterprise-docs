:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="installing-missing-packages_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Installing missing packages
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

Certain packages might be missing after the upgrade from RHEL 6 to RHEL 7. This problem can occur for several reasons:

* You did not provide a repository to the Red Hat Upgrade Tool that contained these packages. Install missing packages manually.
* Certain problems are preventing some RPMs from being installed. Resolve these problems before installing missing packages.
* You are missing [application]*NetworkManager* because the service was not configured and running before the upgrade. Install and configure [application]*NetworkManager* manually. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/getting_started_with_networkmanager[Getting started with NetworkManager].

.Procedure

. Review which packages are missing from your RHEL 7 system using one of the following methods:
* Review the pre-upgrade report.
* Run the following command to generate a list of expected packages in RHEL 7 and compare with the packages that are currently installed to determine which packages are missing.
+
[literal,subs="+quotes,attributes"]
....
$ /root/preupgrade/kickstart/RHRHEL7rpmlist* | grep -v "#" | cut -d "|" -f 3 | sort | uniq
....
. Install missing packages using one of the following methods:
* Locate and install all missing packages at once. This is the quickest method of getting all missing packages.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# cd /root/preupgrade
# bash noauto_postupgrade.d/install_rpmlist.sh kickstart/RHRHEL7rpmlist_kept
....
* If you know that you want to install only some of the missing packages, install each package individually.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# {PackageManagerCommand} install pass:quotes[_package_]
....

[NOTE]
====
For further details about other files with lists of packages you should install on the upgraded system, see the [filename]`/root/preupgrade/kickstart/README` file and the pre-upgrade report.
====

