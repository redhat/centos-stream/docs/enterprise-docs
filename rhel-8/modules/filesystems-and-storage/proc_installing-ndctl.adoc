:_mod-docs-content-type: PROCEDURE

[id="installing-ndctl_{context}"]
= Installing ndctl

You can install the `ndctl` utility to configure and monitor Non-Volatile Dual In-line Memory Modules (NVDIMM) devices.


.Procedure

* Install the `ndctl` utility:
+
[subs="+attributes,quotes"]
----
# *{PackageManagerCommand} install ndctl*
----
