:_mod-docs-content-type: PROCEDURE
[id="displaying-information-about-stratis-volumes_{context}"]
= Displaying information about Stratis volumes

[role="_abstract"]
You can lists statistics about your Stratis volumes, such as the total, used, and free size or file systems and block devices belonging to a pool, by using the `stratis` utility.

Standard Linux utilities such as `df` report the size of the XFS file system layer on Stratis, which is 1 TiB. This is not useful information, because the actual storage usage of Stratis is less due to thin provisioning, and also because Stratis automatically grows the file system when the XFS layer is close to full.

[IMPORTANT]
====
Regularly monitor the amount of data written to your Stratis file systems, which is reported as the _Total Physical Used_ value. Make sure it does not exceed the _Total Physical Size_ value.
====

.Prerequisites

* Stratis is installed. See
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#installing-stratis_setting-up-stratis-file-systems[Installing Stratis].

* The `stratisd` service is running.

.Procedure

* To display information about all *block devices* used for Stratis on your system:
+
[subs="+quotes"]
----
# *stratis blockdev*

Pool Name  Device Node    Physical Size   State  Tier
[replaceable]_my-pool_    [replaceable]_/dev/sdb_            [replaceable]_9.10 TiB_  [replaceable]_In-use_  [replaceable]_Data_
----

* To display information about all Stratis *pools* on your system:
+
[subs="+quotes"]
----
# *stratis pool*

Name    Total Physical Size  Total Physical Used
[replaceable]_my-pool_            [replaceable]_9.10 TiB_              [replaceable]_598 MiB_
----

* To display information about all Stratis *file systems* on your system:
+
[subs="+quotes"]
----
# *stratis filesystem*

Pool Name  Name  Used     Created            Device
[replaceable]_my-pool_    [replaceable]_my-fs_ [replaceable]_546 MiB_  [replaceable]_Nov 08 2018 08:03_  /dev/stratis/[replaceable]__my-pool/my-fs__
----

// Alternative commands. Consider which is more useful.
////
* To list all Stratis pools on your system, use:
+
----
# stratis pool list
----

* To list all block devices that make up a pool, use:
+
[subs=+quotes]
----
# stratis blockdev list [replaceable]_my-pool_
----

* To list all Stratis file systems within a pool and display their actual space usage, use:
+
[subs=+quotes]
----
# stratis filesystem list [replaceable]_my-pool_
----
////
[role="_additional-resources"]
.Additional resources
* `stratis(8)` man page on your system
