:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//
// assembly_disk-quotas.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/con_the-xfs_quota-tool.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.
[id="the-xfs_quota-tool_{context}"]
= The `xfs_quota` tool

[role="_abstract"]
You can use the `xfs_quota` tool to manage quotas on XFS file systems. In addition, you can use XFS file systems with limit enforcement turned off as an effective disk usage accounting system.

The XFS quota system differs from other file systems in a number of ways. Most importantly, XFS considers quota information as file system metadata and uses journaling to provide a higher level guarantee of consistency.

[role="_additional-resources"]
.Additional resources
* `xfs_quota(8)` man page on your system
