:_mod-docs-content-type: CONCEPT
// Module included in the following assemblies:
//deploying-vdo
//managing-free-space-on-vdo-volumes
// <List assemblies here, each on a new line>
[id="the-physical-and-logical-size-of-a-vdo-volume_{context}"]
= The physical and logical size of a VDO volume

VDO utilizes physical, available physical, and logical size in the following ways:

Physical size::
+
This is the same size as the underlying block device. VDO uses this storage for:

* User data, which might be deduplicated and compressed
* VDO metadata, such as the UDS index

Available physical size::
+
This is the portion of the physical size that VDO is able to use for user data
+
It is equivalent to the physical size minus the size of the metadata, minus the remainder after dividing the volume into slabs by the given slab size.

Logical Size::
+
This is the provisioned size that the VDO volume presents to applications. It is usually larger than the available physical size. If the `--vdoLogicalSize` option is not specified, then the provisioning of the logical volume is now provisioned to a `1:1` ratio. For example, if a VDO volume is put on top of a 20 GB block device, then 2.5 GB is reserved for the UDS index (if the default index size is used). The remaining 17.5 GB is provided for the VDO metadata and user data. As a result, the available storage to consume is not more than 17.5 GB, and can be less due to metadata that makes up the actual VDO volume.
+
VDO currently supports any logical size up to 254 times the size of the physical volume with an absolute maximum logical size of 4PB.


.VDO disk organization
image::vdo-disk-organization.png[VDO disk organization]

In this figure, the VDO deduplicated storage target sits completely on top of the block device, meaning the physical size of the VDO volume is the same size as the underlying block device.

[role="_additional-resources"]
.Additional resources
* For more information about how much storage VDO metadata requires on block devices of different sizes, see xref:examples-of-vdo-requirements-by-physical-size_vdo-requirements[].
