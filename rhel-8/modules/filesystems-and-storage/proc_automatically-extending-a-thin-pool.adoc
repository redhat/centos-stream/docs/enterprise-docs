:_newdoc-version: 2.16.0
:_template-generated: 2024-05-08
:_mod-docs-content-type: PROCEDURE

[id="automatically-extending-a-thin-pool_{context}"]
= Automatically extending a thin pool

[role="_abstract"]
You can automate the expansion of your thin pool by enabling monitoring and setting the `thin_pool_autoextend_threshold` and the `thin_pool_autoextend_percent` configuration parameters.

.Prerequisites
* Administrative access.

.Procedure

. Check if the thin pool is monitored:
+
[subs=quotes]
----
# *lvs -o lv_name,vg_name,seg_monitor*

  LV                VG              Monitor       
  ThinPoolName      VolumeGroupName not monitored
----

. Enable thin pool monitoring with the `dmeventd` daemon:
+
[subs=quotes]
----
# *lvchange --monitor y _VolumeGroupName/ThinPoolName_*
----
+
Replace _VolumeGroupName_ with the name of the volume group. Replace _ThinPoolName_ with the name of the thin pool.

. As the `root` user, open the `/etc/lvm/lvm.conf` file in an editor of your choice.


. Uncomment the `thin_pool_autoextend_threshold` and `thin_pool_autoextend_percent` lines and set each parameter to a required value:
+
----
thin_pool_autoextend_threshold = 70
thin_pool_autoextend_percent = 20
----
+
`thin_pool_autoextend_threshold` determines the percentage at which LVM starts to auto-extend the thin pool. For example, setting it to 70 means LVM will try to extend the thin pool when it reaches 70% capacity.
+
`thin_pool_autoextend_percent` specifies by what percentage the thin pool should be extended when it reaches threshold. For example, setting it to 20 means the thin pool will be increased by 20% of its current size.

. Save the changes and exit the editor.

. Restart the `lvm2-monitor`:
+
[subs=quotes]
----
# *systemctl restart lvm2-monitor*
----

[role="_additional-resources"]
.Additional resources
* The `lvs(8)`, `lvchange(8)`, `dmeventd(8)` man pages


