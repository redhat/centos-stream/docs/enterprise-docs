:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
////

[id="create-unencrypted-stratis-pool_{context}"]

= Creating an unencrypted Stratis pool

[role="_abstract"]
You can create an unencrypted Stratis pool from one or more block devices.

.Prerequisites
* Stratis is installed. For more information, see
xref:installing-stratis_setting-up-stratis-file-systems[Installing Stratis].

* The `stratisd` service is running.
* The block devices on which you are creating a Stratis pool are not in use and are not mounted.
* Each block device on which you are creating a Stratis pool is at least 1 GB.
* On the IBM Z architecture, the `/dev/dasd*` block devices must be partitioned. Use the partition device for creating the Stratis pool.
+
For information about partitioning DASD devices, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/interactively_installing_rhel_over_the_network/configuring-a-linux-instance-on-ibm-z_rhel-installer[Configuring a Linux instance on IBM Z].

[NOTE]
====
You cannot encrypt an unencrypted Stratis pool.
====

.Procedure
. Erase any file system, partition table, or RAID signatures that exist on each block device that you want to use in the Stratis pool:
+
[subs="+quotes"]
----
# *wipefs --all [replaceable]_block-device_*
----
+
where [replaceable]`_block-device_` is the path to the block device; for example, `/dev/sdb`.

. Create the new unencrypted Stratis pool on the selected block device:
+
[subs="+quotes"]
----
# *stratis pool create [replaceable]_my-pool_ [replaceable]_block-device_*
----
+
where [replaceable]`_block-device_` is the path to an empty or wiped block device.
+
You can also specify multiple block devices on a single line by using the following command:
+
[subs="+quotes"]
----
# *stratis pool create [replaceable]_my-pool_ [replaceable]_block-device-1_ [replaceable]_block-device-2_*
----

.Verification
* Verify that the new Stratis pool was created:
+
[subs="+quotes"]
----
# *stratis pool list*
----

//[role="_additional-resources"]
//.Additional resources
//* `stratis(8)` man page on your system
