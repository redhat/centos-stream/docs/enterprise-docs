:_mod-docs-content-type: PROCEDURE
[id="removing-lvm-volume-groups_{context}"]
= Removing LVM volume groups

You can remove an existing volume group using the `vgremove` command.  Only volume groups that do not contain logical volumes can be removed.

.Prerequisites

* Administrative access.
// * The volume group contains no logical volumes. To remove logical volumes from a volume group, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/managing-lvm-logical-volumes_configuring-and-managing-logical-volumes#removing-lvm-logical-volumes_managing-lvm-logical-volumes[Removing LVM logical volumes]. (Note: This module is not included in RHEL 8 and 9 any longer. To be discussed with the developers as the first step suggests ensuring the volume group does not contain logical volume).

.Procedure

. Ensure the volume group does not contain logical volumes:
+
[subs=quotes]
----
# *vgs -o vg_name,lv_count _VolumeGroupName_*

  VG               #LV
  _VolumeGroupName_    0
----
+
Replace _VolumeGroupName_ with the name of the volume group.

. Remove the volume group:
+
[subs=quotes]
....
# *vgremove _VolumeGroupName_*
....
+
Replace _VolumeGroupName_ with the name of the volume group.

[role="_additional-resources"]
.Additional resources
* `vgs(8)`, `vgremove(8)` man pages on your system
