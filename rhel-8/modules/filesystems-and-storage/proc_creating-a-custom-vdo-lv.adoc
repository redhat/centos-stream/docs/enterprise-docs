:_newdoc-version: 2.17.0
:_mod-docs-content-type: PROCEDURE

[id="creating-a-custom-vdo-logical-volume_{context}"]
= Creating a custom VDO logical volume
[[creating-a-custom-vdo-lv_{context}]]

With Logical Volume Manager (LVM), you can create a custom LV that uses Virtual Data Optimizer (VDO) pool for data storage.

.Prerequisites
* Administrative access.

.Procedure

. Display the VGs:
+
[subs=quotes]
----
*# vgs*

  VG               #PV #LV #SN Attr   VSize   VFree 
  VolumeGroupName   1   0   0 wz--n-  28.87g 28.87g
----

. Create a LV to be converted to a VDO pool:
+
[subs=quotes]
----
*# lvcreate --name _VDOPoolName_ --size _Size_ _VolumeGroupName_*
----
+
Replace _VDOPoolName_ with the name for your VDO pool. Replace _Size_ with the size for your VDO pool. Replace _VolumeGroupName_ with the name of the VG.

. Convert this LV to a VDO pool. In this conversion, you are creating a new VDO LV that uses the VDO pool. Because `lvcreate` is creating a new VDO LV, you must specify parameters for the new VDO LV. Use `--name|-n` to specify the name of the new VDO LV, and `--virtualsize|-V` to specify the size of the new VDO LV.
+
[subs=quotes]
----
*# lvconvert --type vdo-pool --name _VDOVolumeName_ --virtualsize _VDOVolumeSize_ _VolumeGroupName/VDOPoolName_*
----
+
Replace _VDOVolumeName_ with the name for your VDO volume. Replace _VDOVolumeSize_ with the size for your VDO volume. Replace _VolumeGroupName/VDOPoolName_ with the names for your VG and your VDO pool.

.Verification

. Verify that the LV is converted to the VDO pool:
+
----
*# lvs -o lv_name,vg_name,seg_type*

  LV                     VG              Type    
  VDOPoolName            VolumeGroupName vdo-pool
  VDOVolumeName          VolumeGroupName vdo
----


[role="_additional-resources"]
.Additional resources
* The `vgs(8)`, `lvs(8)`, `lvconvert(8)` man pages


