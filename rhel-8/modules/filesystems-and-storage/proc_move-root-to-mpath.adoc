:_mod-docs-content-type: PROCEDURE

[id="moving-a-root-file-system-from-a-single-path-device-to-a-multipath-device_{context}"]
= Moving a root file system from a single path device to a multipath device

If you have installed your system on a single-path device and later added another path to the root file system, move your root file system to a multipathed device.

.Prerequisites

* You have installed the `device-mapper-multipath`
package.

.Procedure

. Create the `/etc/multipath.conf` configuration file, load the multipath module, and set `chkconfig` for the `multipathd` to `on`:
+
[subs=+quotes]
----
# *mpathconf --enable*
----

. If the `find_multipaths` configuration parameter is not set to `yes`, edit the `blacklist` and `blacklist_exceptions` sections of the `/etc/multipath.conf` file, as described in xref:preventing-devices-from-multipathing_configuring-device-mapper-multipath[Preventing devices from multipathing].

. Build a multipath device on top of the root device as soon as it is discovered. This command also ensures that `find_multipaths` allows the device, even if it only has one path.
+
[subs=+quotes]
----
# *multipath -a _/dev/sdb_`*
wwid '3600d02300069c9ce09d41c4ac9c53200' added
----
+
Replace _/dev/sdb_ with the root device name.

. Confirm that your configuration file is set up correctly by executing the `multipath` command and search the output for a line of the following format. This indicates that the command failed to create the multipath device.
+
[subs=+quotes]
----
_date_  _wwid_: ignoring map
----
+
For example, if the WWID of the device is _3600d02300069c9ce09d41c4ac9c53200_,
you would see a line in the output such as the
following:
+
[subs=+quotes]
----
# *multipath*
Oct 21 09:37:19 | 3600d02300069c9ce09d41c4ac9c53200: ignoring map
----

. Rebuild the `initramfs` file system with `multipath`:
+
[subs=+quotes]
----
# *dracut --force -H --add multipath*
----

. Shut the machine down.

. Boot the machine.

. Make the other paths visible to the machine.

.Verification

* Check whether the multipath device is created by running the following command:
+
[subs=+quotes]
----
# *multipath -l | grep 3600d02300069c9ce09d41c4ac9c53200*
mpatha (3600d02300069c9ce09d41c4ac9c53200) dm-0 3PARdata,VV
----
