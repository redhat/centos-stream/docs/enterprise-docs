:_mod-docs-content-type: CONCEPT
[id="the-internal-processing-of-vdo-write-modes_{context}"]
= The internal processing of VDO write modes

The write modes for VDO are `sync` and `async`. The following information describes the operations of these modes.

If the `kvdo` module is operating in synchronous (`synch`) mode:

. It temporarily writes the data in the request to the allocated block and then acknowledges the request.
. Once the acknowledgment is complete, an attempt is made to deduplicate the block by computing a MurmurHash-3 signature of the block data, which is sent to the VDO index.
. If the VDO index contains an entry for a block with the same signature, `kvdo` reads the indicated block and does a byte-by-byte comparison of the two blocks to verify that they are identical.
. If they are indeed identical, then `kvdo` updates its block map so that the logical block points to the corresponding physical block and releases the allocated physical block.
. If the VDO index did not contain an entry for the signature of the block being written, or the indicated block does not actually contain the same data, `kvdo` updates its block map to make the temporary physical block permanent. 

If `kvdo` is operating in asynchronous (`async`) mode:

. Instead of writing the data, it will immediately acknowledge the request.
. It will then attempt to deduplicate the block in same manner as described above.
. If the block turns out to be a duplicate, `kvdo` updates its block map and releases the allocated block. Otherwise, it writes the data in the request to the allocated block and updates the block map to make the physical block permanent.

// .Additional resources
// 
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
