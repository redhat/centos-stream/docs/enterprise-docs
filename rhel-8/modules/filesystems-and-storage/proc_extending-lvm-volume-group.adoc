:_newdoc-version: 2.16.0
:_template-generated: 2024-02-14
:_mod-docs-content-type: PROCEDURE

[id="extending-lvm-volume-group_{context}"]
= Extending an LVM volume group

[role="_abstract"]
You can use the `vgextend` command to add physical volumes (PVs) to a volume group (VG).

.Prerequisites

* Administrative access.
* The `lvm2` package is installed.
* One or more physical volumes are created. For more information about creating physical volumes, see
ifdef::configuring-and-managing-logical-volumes[]
xref:creating-lvm-physical-volume_managing-lvm-physical-volumes[Creating LVM physical volume].
endif::[]
ifndef::configuring-and-managing-logical-volumes[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/managing-lvm-physical-volumes_configuring-and-managing-logical-volumes#creating-lvm-physical-volume_managing-lvm-physical-volumes[Creating LVM physical volume].
endif::[]
* The volume group is created. For more information about creating volume groups, see xref:creating-lvm-volume-group_managing-lvm-volume-groups[].

.Procedure

. List and identify the VG that you want to extend:
+
[subs=quotes]
----
# *vgs*
----

. List and identify the PVs that you want to add to the VG:
+
[subs=quotes]
----
# *pvs*
----

. Extend the VG:
+
[subs=quotes]
----
# *vgextend _VolumeGroupName PhysicalVolumeName_*
----
+
Replace _VolumeGroupName_ with the name of the VG. Replace _PhysicalVolumeName_ with the name of the PV.


.Verification

* Verify that the VG now includes the new PV:
+
[subs=quotes]
----
# *pvs*

  PV         VG              Fmt  Attr PSize  PFree 
  /dev/sda   VolumeGroupName lvm2 a--  28.87g 28.87g
  /dev/sdd   VolumeGroupName lvm2 a--   1.88g  1.88g
----

[role="_additional-resources"]
.Additional resources
* `vgextend(8)`, `vgs(8)`, `pvs(8)` man pages


