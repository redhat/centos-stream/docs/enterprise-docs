// Module included in the following assemblies:
//
// assembly_performing-a-multi-user-smb-mount.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_mounting-a-share-with-the-multiuser-option.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="proc_mounting-a-share-with-the-multiuser-option_{context}"]
= Mounting a share with the multiuser option

[role="_abstract"]
Before users can access the share with their own credentials, mount the share as the `root` user using an account with limited permissions.

.Procedure

To mount a share automatically with the `multiuser` option when the system boots:

. Create the entry for the share in the [filename]`/etc/fstab` file. For example:
+
[literal,subs="+quotes,attributes"]
----
_//server_name/share_name_  _/mnt_  cifs  `multiuser,sec=ntlmssp`,credentials=_/root/smb.cred_  0 0
----

. Mount the share:
+
[literal,subs="+quotes,attributes"]
----
# *mount /mnt/*
----

If you do not want to mount the share automatically when the system boots, mount it manually by passing `-o multiuser,sec=security_type` to the `mount` command.
For details about mounting an SMB share manually, see
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_file_systems/index#proc_manually-mounting-an-smb-share_assembly_mounting-an-smb-share-on-red-hat-enterprise-linux[Manually mounting an SMB share].

