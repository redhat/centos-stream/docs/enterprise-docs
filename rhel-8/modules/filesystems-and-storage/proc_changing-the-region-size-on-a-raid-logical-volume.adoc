:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
// assembly_configuring-raid-logical-volumes.adoc

[id="changing-the-region-size-on-a-raid-logical-volume_{context}"]
= Changing the region size on a RAID logical volume

When you create a RAID logical volume, the `raid_region_size` parameter from the `/etc/lvm/lvm.conf` file represents the region size for the RAID logical volume. After you created a RAID logical volume, you can change the region size of the volume. This parameter defines the granularity to keep track of the dirty or clean state. Dirty bits in the bitmap define the work set to synchronize after a dirty shutdown of a RAID volume, for example, a system failure.

If you set `raid_region_size` to a higher value, it reduces the size of bitmap as well as the congestion. But it impacts the `write` operation during resynchronizing the region because writes to RAID are postponed until synchronizing the region finishes.

.Procedure

. Create a RAID logical volume:
+
[subs="+quotes"]
----
# **lvcreate --type raid1 -m 1 -L 10G _test_**
  Logical volume "lvol0" created.
----

. View the RAID logical volume:
+
[literal,subs="+quotes,attributes"]
----
# **lvs -a -o +devices,region_size**

LV                VG      Attr     LSize Pool Origin Data% Meta% Move Log   Cpy%Sync Convert Devices                              Region
lvol0             test rwi-a-r--- 10.00g                                    100.00           lvol0_rimage_0(0),lvol0_rimage_1(0)  2.00m
[lvol0_rimage_0]  test iwi-aor--- 10.00g                                                     /dev/sde1(1)                            0 
[lvol0_rimage_1]  test iwi-aor--- 10.00g                                                     /dev/sdf1(1)                            0 
[lvol0_rmeta_0]   test ewi-aor---  4.00m                                                     /dev/sde1(0)                            0 
[lvol0_rmeta_1]   test ewi-aor---  4.00m  
----
The *Region* column indicates the raid_region_size parameter's value.


. Optional: View the `raid_region_size` parameter's value:
+
[subs="+quotes"]
----
# **cat /etc/lvm/lvm.conf | grep raid_region_size**

# Configuration option activation/raid_region_size.
	# raid_region_size = 2048
----

. Change the region size of a RAID logical volume:
+
[subs="+quotes"]
----
# **lvconvert -R 4096K _my_vg/my_lv_**

Do you really want to change the region_size 512.00 KiB of LV my_vg/my_lv to 4.00 MiB? [y/n]: y
  Changed region size on RAID LV my_vg/my_lv to 4.00 MiB.
----

. Resynchronize the RAID logical volume:
+
[subs="+quotes"]
----
# **lvchange --resync _my_vg/my_lv_**

Do you really want to deactivate logical volume my_vg/my_lv to resync it? [y/n]: y
----

.Verification

. View the RAID logical volume:
+
[literal,subs="+quotes,attributes"]
----
# **lvs -a -o +devices,region_size**

LV               VG   Attr        LSize Pool Origin Data% Meta% Move Log Cpy%Sync Convert Devices                              Region
lvol0            test rwi-a-r--- 10.00g                                    6.25           lvol0_rimage_0(0),lvol0_rimage_1(0)  4.00m
[lvol0_rimage_0] test iwi-aor--- 10.00g                                                   /dev/sde1(1)                            0 
[lvol0_rimage_1] test iwi-aor--- 10.00g                                                   /dev/sdf1(1)                            0 
[lvol0_rmeta_0]  test ewi-aor---  4.00m                                                   /dev/sde1(0)                            0
----
+
The *Region* column indicates the changed value of the `raid_region_size` parameter.

. View the `raid_region_size` parameter's value in the `lvm.conf` file:
+
[subs="+quotes"]
----
# **cat /etc/lvm/lvm.conf | grep raid_region_size**

# Configuration option activation/raid_region_size.
	# raid_region_size = 4096
----

[role="_additional-resources"]
.Additional resources
* `lvconvert(8)`  man page on your system

