:_mod-docs-content-type: PROCEDURE

[id="enabling-discard-mount-option-on-lvm-vdo-volume_{context}"]
= Enabling discard mount option on an LVM-VDO volume

[role="_abstract"]
This procedure enables the `discard` option on your LVM-VDO volume.

.Prerequisites

* An LVM-VDO volume exists on your system.

.Procedure

* Enable the `discard` option on your volume:
+
[subs=+quotes]
----
# *mount -o discard /dev/_vg-name/vdo-name_ _mount-point_*
----


[role="_additional-resources"]
.Additional resources
* `xfs(5)`, `mount(8)`, and `lvmvdo(7)` man pages on your system
