:_mod-docs-content-type: CONCEPT
[id="started-and-activated-vdo-volumes_{context}"]
= Started and activated VDO volumes

During the system boot, the `vdo` `systemd` unit automatically _starts_ all VDO devices that are configured as _activated_.

The `vdo` `systemd` unit is installed and enabled by default when the `vdo` package is installed. This unit automatically runs the `vdo start --all` command at system startup to bring up all activated VDO volumes.

You can also create a VDO volume that does not start automatically by adding the `--activate=disabled` option to the [command]`vdo create` command.

.The starting order
Some systems might place LVM volumes both above VDO volumes and below them. On these systems, it is necessary to start services in the right order:

. The lower layer of LVM must start first. In most systems, starting this layer is configured automatically when the LVM package is installed.
. The `vdo` `systemd` unit must start then.
. Finally, additional scripts must run in order to start LVM volumes or other services on top of the running VDO volumes. 

.How long it takes to stop a volume
Stopping a VDO volume takes time based on the speed of your storage device and the amount of data that the volume needs to write:

* The volume always writes around 1GiB for every 1GiB of the UDS index.
* The volume additionally writes the amount of data equal to the block map cache size plus up to 8MiB per slab.
* The volume must finish processing all outstanding IO requests.


// .Additional resources
// 
// Moved to the assembly references:
// * If restarted after an unclean shutdown, VDO performs a rebuild to verify the consistency of its metadata and repairs it if necessary. See xref:recovering-a-vdo-volume-after-an-unclean-shutdown_maintaining-vdo[] for more information about the rebuild process.

