:_mod-docs-content-type: CONCEPT
[id="types-of-shared-mounts_{context}"]
= Types of shared mounts

[role="_abstract"]
There are multiple types of shared mounts that you can use. The difference between them is what happens when you mount another file system under one of the shared mount points. The shared mounts are implemented using the _shared subtrees_ functionality.

The following mount types are available:

`private`::
This type does not receive or forward any propagation events.
+
When you mount another file system under either the duplicate or the original mount point, it is not reflected in the other.

`shared`::
This type creates an exact replica of a given mount point.
+
When a mount point is marked as a `shared` mount, any mount within the original mount point is reflected in it, and vice versa.
+
This is the default mount type of the root file system.

`slave`::
This type creates a limited duplicate of a given mount point.
+
When a mount point is marked as a `slave` mount, any mount within the original mount point is reflected in it, but no mount within a `slave` mount is reflected in its original.

`unbindable`::
This type prevents the given mount point from being duplicated whatsoever.

[role="_additional-resources"]
.Additional resources
* link:https://lwn.net/Articles/159077/[The _Shared subtrees_ article on Linux Weekly News]
