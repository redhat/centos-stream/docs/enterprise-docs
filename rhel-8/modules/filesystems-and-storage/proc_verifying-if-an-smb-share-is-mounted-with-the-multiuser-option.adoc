:_mod-docs-content-type: PROCEDURE
[id="proc_verifying-if-an-smb-share-is-mounted-with-the-multiuser-option_{context}"]
= Verifying if an SMB share is mounted with the multiuser option

[role="_abstract"]
To verify if a share is mounted with the `multiuser` option, display the mount options.

.Procedure

[literal,subs="+quotes,attributes"]
----
# *mount*
...
_//server_name/share_name_ on _/mnt_ type cifs (sec=ntlmssp,`multiuser`,...)
----

If the `multiuser` entry is displayed in the list of mount options, the feature is enabled.
