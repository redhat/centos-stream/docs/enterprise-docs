:_mod-docs-content-type: PROCEDURE

[id="gathering-diagnostic-data-on-lvm_{context}"]
= Gathering diagnostic data on LVM

[role="_abstract"]
If an LVM command is not working as expected, you can gather diagnostics in the following ways.

.Procedure

* Use the following methods to gather different kinds of diagnostic data:

** Add the `-v` argument to any LVM command to increase the verbosity level of the command output. Verbosity can be further increased by adding additional `v’s`.  A maximum of four such `v’s` is allowed, for example, `-vvvv`.

** In the `log` section of the `/etc/lvm/lvm.conf` configuration file, increase the value of the `level` option. This causes LVM to provide more details in the system log.

** If the problem is related to the logical volume activation, enable LVM to log messages during the activation:
... Set the `activation = 1` option in the `log` section of the `/etc/lvm/lvm.conf` configuration file.
... Execute the LVM command with the `-vvvv` option.
... Examine the command output.
... Reset the `activation` option to `0`.
+
If you do not reset the option to `0`, the system might become unresponsive during low memory situations.

** Display an information dump for diagnostic purposes:
+
[subs=+quotes]
----
# *lvmdump*
----

** Display additional system information:
+
[subs=+quotes]
----
# *lvs -v*
----
+
[subs=+quotes]
----
# *pvs --all*
----
+
[subs=+quotes]
----
# *dmsetup info --columns*
----

** Examine the last backup of the LVM metadata in the `/etc/lvm/backup/` directory and archived versions in the `/etc/lvm/archive/` directory.

** Check the current configuration information:
+
[subs=+quotes]
----
# *lvmconfig*
----

** Check the `/run/lvm/hints` cache file for a record of which devices have physical volumes on them.


[role="_additional-resources"]
.Additional resources
* `lvmdump(8)` man page on your system
