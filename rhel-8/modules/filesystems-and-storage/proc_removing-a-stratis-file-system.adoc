:_mod-docs-content-type: PROCEDURE
[id="removing-a-stratis-file-system_{context}"]
= Removing a Stratis file system

[role="_abstract"]
You can remove an existing Stratis file system. Data stored on it are lost.

.Prerequisites

* Stratis is installed. See
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#installing-stratis_setting-up-stratis-file-systems[Installing Stratis].

* The `stratisd` service is running.
* You have created a Stratis file system. See
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#creating-a-stratis-file-system_setting-up-stratis-file-systems[Creating a Stratis file system].


.Procedure

. Unmount the file system:
+
[subs="+quotes"]
----
# *umount /dev/stratis/[replaceable]__my-pool__/[replaceable]__my-fs__*
----

. Destroy the file system:
+
[subs="+quotes"]
----
# *stratis filesystem destroy [replaceable]_my-pool_ [replaceable]_my-fs_*
----

.Verification
* Verify that the file system no longer exists:
+
[subs="+quotes"]
----
# *stratis filesystem list [replaceable]_my-pool_*
----

[role="_additional-resources"]
.Additional resources
* `stratis(8)` man page on your system
