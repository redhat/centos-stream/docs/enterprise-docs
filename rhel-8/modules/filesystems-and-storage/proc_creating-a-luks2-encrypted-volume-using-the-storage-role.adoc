:_newdoc-version: 2.17.0
:_template-generated: 2024-03-08
:_mod-docs-content-type: PROCEDURE


[id="creating-a-luks2-encrypted-volume-using-the-storage-role_{context}"]
= Creating a LUKS2 encrypted volume by using the `storage` {RHELSystemRoles}
You can use the `storage` role to create and configure a volume encrypted with LUKS by running an Ansible playbook.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]


.Procedure
. Store your sensitive variables in an encrypted file:

.. Create the vault:
+
[subs="+quotes"]
....
$ *ansible-vault create vault.yml*
New Vault password: _<vault_password>_
Confirm New Vault password: _<vault_password>_
....

.. After the `ansible-vault create` command opens an editor, enter the sensitive data in the `_<key>_: _<value>_` format:
+
[source,ini,subs="+quotes"]
....
luks_password: _<password>_
....

.. Save the changes, and close the editor. Ansible encrypts the data in the vault.


. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Manage local storage
  hosts: managed-node-01.example.com
  vars_files:
    - vault.yml
  tasks:
    - name: Create and configure a volume encrypted with LUKS
      ansible.builtin.include_role:
        name: rhel-system-roles.storage
      vars:
        storage_volumes:
          - name: barefs
            type: disk
            disks:
              - sdb
            fs_type: xfs
            fs_label: _<label>_
            mount_point: /mnt/data
            encryption: true
            encryption_password: "{{ luks_password }}"
....
+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook --ask-vault-pass ~/playbook.yml*
....


.Verification

. Find the `luksUUID` value of the LUKS encrypted volume:
+
[subs="quotes"]
....
# *ansible managed-node-01.example.com -m command -a 'cryptsetup luksUUID /dev/sdb'*

4e4e7970-1822-470e-b55a-e91efe5d0f5c
....

. View the encryption status of the volume:
+
[subs="quotes"]
....
# *ansible managed-node-01.example.com -m command -a 'cryptsetup status luks-4e4e7970-1822-470e-b55a-e91efe5d0f5c'*

/dev/mapper/luks-4e4e7970-1822-470e-b55a-e91efe5d0f5c is active and is in use.
  type:    LUKS2
  cipher:  aes-xts-plain64
  keysize: 512 bits
  key location: keyring
  device:  /dev/sdb
...
....

. Verify the created LUKS encrypted volume:
+
[subs="quotes"]
....
# *ansible managed-node-01.example.com -m command -a 'cryptsetup luksDump /dev/sdb'*

LUKS header information
Version:        2
Epoch:          3
Metadata area:  16384 [bytes]
Keyslots area:  16744448 [bytes]
UUID:           4e4e7970-1822-470e-b55a-e91efe5d0f5c
Label:          (no label)
Subsystem:      (no subsystem)
Flags:          (no flags)

Data segments:
  0: crypt
        offset: 16777216 [bytes]
        length: (whole device)
        cipher: aes-xts-plain64
        sector: 512 [bytes]
...
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file
* `/usr/share/doc/rhel-system-roles/storage/` directory
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/encrypting-block-devices-using-luks_managing-storage-devices[Encrypting block devices by using LUKS]
ifdef::system-roles-ansible[]
* xref:ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]
endif::[]
ifndef::system-roles-ansible[]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/ansible-vault_automating-system-administration-by-using-rhel-system-roles[Ansible vault]
endif::[]

