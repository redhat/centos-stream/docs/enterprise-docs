:_mod-docs-content-type: PROCEDURE

[id="disabling-multipathing-by-device-name_{context}"]
= Disabling multipathing by device name

You can disable multipathing on device types by device name, so that DM Multipath will not group them into a multipath device.


.Procedure

. Display device information:
+
[subs=+quotes]
----
# **udevadm info --query=all -n /dev/mapper/sd* **
----

. Disable devices in the `/etc/multipath.conf` configuration file using the `devnode` entry.
+
The following example shows the lines in the DM Multipath configuration file that disable all SCSI devices, because it disables all `sd*` devices as well:
+
----
blacklist {
       devnode "^sd[a-z]"
}
----
+
You can use a `devnode` entry to disable individual devices rather than all devices of a specific type.  However, this is not recommended because unless it is statically mapped by `udev` rules, there is no guarantee that a specific device will have the same name on reboot.  For example, a device name could change from `/dev/sda` to `/dev/sdb` on reboot.
+
By default, DM Multipath disables all devices that are not SCSI, NVMe, or DASD, using the following `devnode` entry:
+
----
blacklist {
       devnode "!^(sd[a-z]|dasd[a-z]|nvme[0-9])"
}
----
+
The devices that this entry disables do not generally support DM Multipath.

. Validate the `/etc/multipath.conf` file after modifying the multipath configuration file by running one of the following commands:

* To display any configuration errors, run:
+
[subs=+quotes]
----
# *multipath -t > /dev/null*
----

* To display the new configuration with the changes added, run:
+
[subs=+quotes]
----
# *multipath -t*
----

. Reload the `/etc/multipath.conf` file and reconfigure the `multipathd` daemon for changes to take effect:
+
[subs=+quotes]
----
# *service multipathd reload*
----

// .Verification

// . Start each step with an active verb.

// . Include one command or action per step.

// . Use an unnumbered bullet (*) if the procedure includes only one step.


[role="_additional-resources"]
.Additional resources
* xref:adding-exceptions-for-devices-with-disabled-multipathing_preventing-devices-from-multipathing[Adding exceptions for devices with disabled multipathing]
