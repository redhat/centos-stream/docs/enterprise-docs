:_mod-docs-content-type: PROCEDURE
[id="checking-for-a-volatile-cache_{context}"]
= Checking for a volatile cache

This procedure determines if a block device has a volatile cache or not. You can use the information to choose between the `sync` and `async` VDO write modes.

// .Prerequisites
// 
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

.Procedure

// TODO: Add this paragraph with a reference to the procedure when there is the content on Access.
// See the Viewing and Managing Log Files chapter in the System Administrator's Guide for more information about reading the system log.

. Use either of the following methods to determine if a device has a writeback cache:
+
** Read the [filename]`/sys/block/[replaceable]__block-device__/device/scsi_disk/[replaceable]__identifier__/cache_type` `sysfs` file. For example:
+
----
$ cat '/sys/block/sda/device/scsi_disk/7:0:0:0/cache_type'

write back
----
+
----
$ cat '/sys/block/sdb/device/scsi_disk/1:2:0:0/cache_type'

None
----

** Alternatively, you can find whether the above mentioned devices have a write cache or not in the kernel boot log:
+
----
sd 7:0:0:0: [sda] Write cache: enabled, read cache: enabled, does not support DPO or FUA
sd 1:2:0:0: [sdb] Write cache: disabled, read cache: disabled, supports DPO and FUA
----

. In the previous examples:
+
--
** Device `sda` indicates that it _has_ a writeback cache. Use `async` mode for it.
** Device `sdb` indicates that it _does not have_ a writeback cache. Use `sync` mode for it.
--
+
You should configure VDO to use the `sync` write mode if the `cache_type` value is `None` or `write through`. 


// .Additional resources
// 
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
