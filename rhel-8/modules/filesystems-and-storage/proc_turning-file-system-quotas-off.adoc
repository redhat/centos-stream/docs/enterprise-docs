:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_configuring-disk-quotas.adoc

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="turning-file-system-quotas-off_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Turning file system quotas off
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
Use `quotaoff` to turn disk quota enforcement off on the specified file systems. Quota accounting stays enabled after executing this command.

.Procedure

* To turn all user and group quotas off:
+
[subs=+quotes]
----
# *quotaoff -vaugP*
----
+
** If neither of the `-u`, `-g`, or `-P` options are specified, only the user quotas are disabled.
** If only `-g` option is specified, only group quotas are disabled.
** If only `-P` option is specified, only project quotas are disabled.
** The `-v` switch causes verbose status information to display as the command executes.

[role="_additional-resources"]
.Additional resources
* `quotaoff(8)` man page on your system
