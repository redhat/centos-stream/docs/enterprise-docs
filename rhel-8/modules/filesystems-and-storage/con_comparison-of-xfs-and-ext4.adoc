:_mod-docs-content-type: CONCEPT
[id="comparison-of-xfs-and-ext4_{context}"]
= Comparison of XFS and ext4

[role="_abstract"]
XFS is the default file system in RHEL. This section compares the usage and features of XFS and ext4.

Metadata error behavior::

In ext4, you can configure the behavior when the file system encounters metadata errors. The default behavior is to simply continue the operation. When XFS encounters an unrecoverable metadata error, it shuts down the file system and returns the `EFSCORRUPTED` error.

Quotas::

In ext4, you can enable quotas when creating the file system or later on an existing file system. You can then configure the quota enforcement using a mount option.
+
XFS quotas are not a remountable option. You must activate quotas on the initial mount.
+
Running the `quotacheck` command on an XFS file system has no effect. The first time you turn on quota accounting, XFS checks quotas automatically.

File system resize::

XFS has no utility to reduce the size of a file system. You can only increase the size of an XFS file system. In comparison, ext4 supports both extending and reducing the size of a file system.

Inode numbers::

The ext4 file system does not support more than 2^32^ inodes.
+
XFS supports dynamic inode allocation. The amount of space inodes can consume on an XFS filesystem is calculated as a percentage of the total filesystem space. To prevent the system from running out of inodes, an administrator can tune this percentage after the filesystem has been created, given there is free space left on the file system.
+
Certain applications cannot properly handle inode numbers larger than 2^32^ on an XFS file system. These applications might cause the failure of 32-bit stat calls with the `EOVERFLOW` return value. Inode number exceed 2^32^ under the following conditions:
+
--
** The file system is larger than 1 TiB with 256-byte inodes.
** The file system is larger than 2 TiB with 512-byte inodes.
--
+
If your application fails with large inode numbers, mount the XFS file system with the `-o inode32` option to enforce inode numbers below 2^32^. Note that using `inode32` does not affect inodes that are already allocated with 64-bit numbers.
+
IMPORTANT: Do _not_ use the `inode32` option unless a specific environment requires it. The `inode32` option changes allocation behavior. As a consequence, the `ENOSPC` error might occur if no space is available to allocate inodes in the lower disk blocks.

// .Additional resources
//
// * empty
