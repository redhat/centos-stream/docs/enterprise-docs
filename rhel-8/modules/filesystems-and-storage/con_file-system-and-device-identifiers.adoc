:_mod-docs-content-type: CONCEPT
[id="file-system-and-device-identifiers_{context}"]
= File system and device identifiers

[role="_abstract"]
File system identifiers are tied to the file system itself, while device identifiers are linked to the physical block device. Understanding the difference is important for proper storage management.

[discrete]
== File system identifiers

File system identifiers are tied to a particular file system created on a block device. The identifier is also stored as part of the file system. If you copy the file system to a different device, it still carries the same file system identifier. However, if you rewrite the device, such as by formatting it with the `mkfs` utility, the device loses the attribute.

File system identifiers include:

* Unique identifier (UUID)
* Label

[discrete]
== Device identifiers

Device identifiers are tied to a block device: for example, a disk or a partition. If you rewrite the device, such as by formatting it with the `mkfs` utility, the device keeps the attribute, because it is not stored in the file system.

Device identifiers include:

* World Wide Identifier (WWID)
* Partition UUID
* Serial number

[discrete]
== Recommendations

* Some file systems, such as logical volumes, span multiple devices. {RH} recommends accessing these file systems using file system identifiers rather than device identifiers.


// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
