// Module included in the following assemblies:
//deploying-vdo
//creating-a-deduplicated-and-compressed-logical-volume
// <List assemblies here, each on a new line>
:_mod-docs-content-type: CONCEPT

[id="slab-size-in-vdo_{context}"]
= Slab size in VDO

[role="_abstract"]
The physical storage of the VDO volume is divided into a number of slabs. Each slab is a contiguous region of the physical space. All of the slabs for a given volume have the same size, which can be any power of 2 multiple of 128 MB up to 32 GB.

The default slab size is 2 GB to facilitate evaluating VDO on smaller test systems. A single VDO volume can have up to 8192 slabs. Therefore, in the default configuration with 2 GB slabs, the maximum allowed physical storage is 16 TB. When using 32 GB slabs, the maximum allowed physical storage is 256 TB. VDO always reserves at least one entire slab for metadata, and therefore, the reserved slab cannot be used for storing user data.

Slab size has no effect on the performance of the VDO volume.

.Recommended VDO slab sizes by physical volume size
[cols="100%,100%",options="header"]
|===
| Physical volume size | Recommended slab size
| 10–99 GB |	1 GB
| 100 GB – 1 TB |	2 GB
| 2–256 TB | 32 GB
|===

The minimal disk usage for a VDO volume using default settings of 2 GB slab size and 0.25 dense index, requires approx 4.7 GB. This provides slightly less than 2 GB of physical data to write at 0% deduplication or compression.

Here, the minimal disk usage is the sum of the default slab size and dense index.

You can control the slab size by providing the `--vdosettings 'vdo_slab_size_mb=_size-in-megabytes_'` option to the `lvcreate` command.


