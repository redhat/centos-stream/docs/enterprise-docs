:_mod-docs-content-type: PROCEDURE
[id="disabling-caching-for-a-logical-volume_{context}"]
= Disabling caching for a logical volume

This procedure disables `dm-cache` or `dm-writecache` caching that is currently enabled on a logical volume.

.Prerequisites

* Caching is enabled on a logical volume.

.Procedure

. Deactivate the logical volume:
+
[subs=+quotes,options=nowrap]
----
# *lvchange --activate n __<vg>__/__<main-lv>__*
----
+
Replace _vg_ with the volume group name, and _main-lv_ with the name of the logical volume where caching is enabled.

. Detach the `cachevol` or `cachepool` volume:
+
[subs=+quotes,options=nowrap]
....
# *lvconvert --splitcache __<vg>__/__<main-lv>__*
....
+
Replace the following values:
+
Replace _vg_ with the volume group name, and _main-lv_ with the name of the logical volume where caching is enabled.
+
.Detaching the `cachevol` or `cachepool` volume
+
====
----
# lvconvert --splitcache vg/main-lv
Detaching writecache already clean.
Logical volume vg/main-lv writecache has been detached.
----
====

.Verification

* Check that the logical volumes are no longer attached together:
+
[subs=+quotes,options=nowrap]
....
# lvs --all --options +devices _<vg>_

LV      Attr       Type   Devices
fastvol -wi------- linear /dev/fast-pv
main-lv -wi------- linear /dev/slow-pv
....

[role="_additional-resources"]
.Additional resources
* `lvmcache(7)` man page on your system

