// Module included in the following assemblies:
//using-fibre-channel-devices
// <List assemblies here, each on a new line>
:_mod-docs-content-type: PROCEDURE

[id="resizing-fibre-channel-logical-units_{context}"]
= Resizing Fibre Channel logical units

[role="_abstract"]
As a system administrator, you can resize Fibre Channel logical units.

.Procedure

. Determine which devices are paths for a `multipath` logical unit:
+
[subs="+quotes"]
----
# *multipath -ll*
----

. Re-scan Fibre Channel logical units on a system that uses multipathing:
+
[subs="+quotes"]
----
$ *echo 1 > /sys/block/_sdX_/device/rescan*
----

[role="_additional-resources"]
.Additional resources
* `multipath(8)` man page on your system
