:_mod-docs-content-type: CONCEPT
[id="con_limitations-of-the-udev-device-naming-convention_{context}"]
= Limitations of the udev device naming convention

[role="_abstract"]
The following are some limitations of the `udev` naming convention:

* It is possible that the device might not be accessible at the time the query is performed because the `udev` mechanism might rely on the ability to query the storage device when the `udev` rules are processed for a `udev` event. This is more likely to occur with Fibre Channel, iSCSI or FCoE storage devices when the device is not located in the server chassis.
* The kernel might send `udev` events at any time, causing the rules to be processed and possibly causing the [filename]`/dev/disk/by-*/` links to be removed if the device is not accessible.
* There might be a delay between when the `udev` event is generated and when it is processed, such as when a large number of devices are detected and the user-space `udevd` service takes some amount of time to process the rules for each one. This might cause a delay between when the kernel detects the device and when the [filename]`/dev/disk/by-*/` names are available.
* External programs such as `blkid` invoked by the rules might open the device for a brief period of time, making the device inaccessible for other uses.
* The device names managed by the `udev` mechanism in /dev/disk/ may change between major releases, requiring you to update the links.

// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
