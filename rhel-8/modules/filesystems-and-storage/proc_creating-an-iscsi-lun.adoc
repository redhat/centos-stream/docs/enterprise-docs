// Module included in the following assemblies:
//configuring-an-iscsi-target

:_mod-docs-content-type: PROCEDURE

[id="creating-an-iscsi-lun_{context}"]
= Creating an iSCSI LUN

[role="_abstract"]
Logical unit number (LUN) is a  physical device that is backed by the iSCSI backstore. Each LUN has a unique number.

.Prerequisites

* Installed and running `targetcli`. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#installing-targetcli_configuring-an-iscsi-target[Installing targetcli].

* An iSCSI target associated with a Target Portal Groups (TPG). For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#creating-an-iscsi-target_configuring-an-iscsi-target[Creating an iSCSI target].

* Created storage objects. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#iscsi-backstore_configuring-an-iscsi-target[iSCSI Backstore].

.Procedure

. Create LUNs of already created storage objects:
+
[subs="+quotes"]
----
/iscsi/iqn.20...mple:444/tpg1> *luns/ create /backstores/ramdisk/rd_backend*
Created LUN 0.

/iscsi/iqn.20...mple:444/tpg1> *luns/ create /backstores/block/block_backend*
Created LUN 1.

/iscsi/iqn.20...mple:444/tpg1> *luns/ create /backstores/fileio/file1*
Created LUN 2.

----

. Verify the created LUNs:
+
[subs="+quotes"]
----
/iscsi/iqn.20...mple:444/tpg1> *ls*

o- tpg.................................. [enambled, auth]
    o- acls ......................................[0 ACL]
    o- luns .....................................[3 LUNs]
    |  o- lun0.........................[ramdisk/ramdisk1]
    |  o- lun1.................[block/block1 (/dev/vdb1)]
    |  o- lun2...................[fileio/file1 (/foo.img)]
    o- portals ................................[1 Portal]
        o- 192.168.122.137:3260......................[OK]
----
+
Default LUN name starts at `0`.
+
IMPORTANT: By default, LUNs are created with read-write permissions. If a new LUN is added after ACLs are created, LUN automatically maps to all available ACLs and can cause a security risk. To create a LUN with read-only permissions, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#creating-a-read-only-iscsi-lun_configuring-an-iscsi-target[Creating a read-only iSCSI LUN].

. Configure ACLs. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#creating-an-iscsi-acl_configuring-an-iscsi-target[Creating an iSCSI ACL].

[role="_additional-resources"]
.Additional resources
* `targetcli(8)` man page on your system
