// Module included in the following assemblies:
//configuring-an-iscsi-initiator

:_mod-docs-content-type: PROCEDURE

[id="creating-an-iscsi-initiator_{context}"]
= Creating an iSCSI initiator

[role="_abstract"]
Create an iSCSI initiator to connect to the iSCSI target to access the storage devices on the server.


.Prerequisites

* You have an iSCSI target's hostname and IP address:

** If you are connecting to a storage target that the external software created, find the target's hostname and IP address from the storage administrator.

** If you are creating an iSCSI target, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#creating-an-iscsi-target_configuring-an-iscsi-target[Creating an iSCSI target].


.Procedure

. Install `iscsi-initiator-utils` on client machine:
+
[subs="+attributes,quotes"]
----
# *{PackageManagerCommand} install iscsi-initiator-utils*
----

. Check the initiator name:
+
[subs="+quotes"]
----
# *cat /etc/iscsi/initiatorname.iscsi*

InitiatorName=iqn.2006-04.com.example:888
----
. If the ACL was given a custom name in link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#creating-an-iscsi-acl_configuring-an-iscsi-target[Creating an iSCI ACL], update the initiator name to match the ACL:

.. Open the `/etc/iscsi/initiatorname.iscsi` file and modify the initiator name:
+
[subs="+quotes"]
----
# *vi /etc/iscsi/initiatorname.iscsi*

InitiatorName=_custom-name_
----

.. Restart the `iscsid` service:
+
[subs="+quotes"]
----
# *systemctl restart iscsid*
----

. Discover the target and log in to the target with the displayed target IQN:
+
[subs="+quotes"]
----
# *iscsiadm -m discovery -t st -p _10.64.24.179_*
    10.64.24.179:3260,1 iqn.2006-04.example:444

# *iscsiadm -m node -T _iqn.2006-04.example:444_ -l*
    Logging in to [iface: default, target: iqn.2006-04.example:444, portal: 10.64.24.179,3260] (multiple)
    Login to [iface: default, target: iqn.2006-04.example:444, portal: 10.64.24.179,3260] successful.
----
+
Replace _10.64.24.179_ with the target-ip-address.
+
You can use this procedure for any number of initiators connected to the same target if their respective initiator names are added to the ACL as described in the
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#creating-an-iscsi-acl_configuring-an-iscsi-target[Creating an iSCSI ACL].

. Find the iSCSI disk name and create a file system on this iSCSI disk:
+
[subs="+quotes"]
----
# *grep "Attached SCSI" /var/log/messages*

# *mkfs.ext4 /dev/_disk_name_*
----
+
Replace _disk_name_ with the iSCSI disk name displayed in the `/var/log/messages` file.

. Mount the file system:
+
[subs="+quotes"]
----
# *mkdir _/mount/point_*

# *mount /dev/_disk_name /mount/point_*
----
+
Replace _/mount/point_ with the mount point of the partition.

. Edit the `/etc/fstab` file to mount the file system automatically when the system boots:
+
[subs="+quotes"]
----
# *vi /etc/fstab*

/dev/_disk_name /mount/point_ ext4 _netdev 0 0
----
+
Replace _disk_name_ with the iSCSI disk name and _/mount/point_ with the mount point of the partition.

[role="_additional-resources"]
.Additional resources
* `targetcli(8)` and `iscsiadm(8)` man pages on your system
