:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_file-system-quota-management-in-xfs.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_setting-project-limits-for-xfs.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="setting-project-limits-for-xfs_{context}"]
= Setting project limits for XFS

[role="_abstract"]
Configure limits for project-controlled directories.

.Procedure

. Add the project-controlled directories to `/etc/projects`. For example, the following adds the `/var/log` path with a unique ID of 11 to `/etc/projects`. Your project ID can be any numerical value mapped to your project.
+
[subs=+quotes]
----
# *echo 11:/var/log >> /etc/projects*
----
+

. Add project names to `/etc/projid` to map project IDs to project names. For example, the following associates a project called `logfiles` with the project ID of 11 as defined in the previous step.
+
[subs=+quotes]
----
# *echo logfiles:11 >> /etc/projid*
----
+

. Initialize the project directory. For example, the following initializes the project directory `/var`:
+
[subs=+quotes]
----
# *xfs_quota -x -c 'project -s logfiles' /var*
----
+
. Configure quotas for projects with initialized directories:
+
[subs=+quotes]
----
# *xfs_quota -x -c 'limit -p bhard=1g logfiles' /var*
----

[role="_additional-resources"]
.Additional resources
* `xfs_quota(8)`, `projid(5)`, and `projects(5)` man pages on your system