:_newdoc-version: 2.16.0
:_template-generated: 2024-02-21
:_mod-docs-content-type: PROCEDURE

[id="creating-thin-logical-volume-snapshots_{context}"]
= Creating thin logical volume snapshots
[[creating-thin-lv-snapshots_{context}]]

[role="_abstract"]
You can create a thin LV snapshot with the `lvcreate` command. When creating a thin LV snapshot, avoid specifying the snapshot size. Including a size parameter results in the creation of a thick snapshot instead.

.Prerequisites

* Administrative access.
* You have created a physical volume. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/managing-lvm-physical-volumes_configuring-and-managing-logical-volumes#creating-lvm-physical-volume_managing-lvm-physical-volumes[Creating LVM physical volume].
* You have created a volume group. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/managing-lvm-volume-groups_configuring-and-managing-logical-volumes#creating-lvm-volume-group_managing-lvm-volume-groups[Creating LVM volume group].
* You have created a logical volume. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/basic-logical-volume-management_configuring-and-managing-logical-volumes#creating-logical-volumes_basic-logical-volume-management[Creating logical volumes].

.Procedure

. Identify the LV of which you want to create a snapshot:
+
[subs=quotes]
....
# *lvs -o lv_name,vg_name,pool_lv,lv_size*

  LV                VG              Pool       LSize  
  PoolName          VolumeGroupName            152.00m
  ThinVolumeName    VolumeGroupName PoolName   100.00m                
....

. Create a thin LV snapshot:
+
[subs=quotes]
....
# *lvcreate --snapshot --name _SnapshotName_ _VolumeGroupName_/_ThinVolumeName_*
....
+
Replace _SnapshotName_ with the name you want to give to the snapshot logical volume. Replace _VolumeGroupName_ with the name of the volume group that contains the original logical volume.
Replace _ThinVolumeName_ with the name of the thin logical volume that you want to create a snapshot of.

.Verification
* Verify that the snapshot is created:
+
[subs=quotes]
----
# *lvs -o lv_name,origin*

  LV                Origin                
  PoolName                      
  SnapshotName      ThinVolumeName
  ThinVolumeName
----

[role="_additional-resources"]
.Additional resources
* `lvcreate(8)` and `lvs(8)` man pages
