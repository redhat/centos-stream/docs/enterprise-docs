:_mod-docs-content-type: PROCEDURE
[id="performing-batch-block-discard_{context}"]
= Performing batch block discard

[role="_abstract"]
You can perform a batch block discard operation to discard unused blocks on a mounted file system.

.Prerequisites

* The file system is mounted.
* The block device underlying the file system supports physical discard operations.

.Procedure

* Use the `fstrim` utility:

** To perform discard only on a selected file system, use:
+
[subs="+quotes"]
----
# *fstrim [replaceable]_mount-point_*
----

** To perform discard on all mounted file systems, use:
+
[subs="+quotes"]
----
# *fstrim --all*
----

If you execute the `fstrim` command on:

* a device that does not support discard operations, or
* a logical device (LVM or MD) composed of multiple devices, where any one of the device does not support discard operations,

the following message displays:

[subs="+quotes"]
----
# *fstrim [replaceable]_/mnt/non_discard_*

fstrim: [replaceable]__/mnt/non_discard__: the discard operation is not supported
----

[role="_additional-resources"]
.Additional resources
* `fstrim(8)` man page on your system
