// Module included in the following assemblies:
//configuring-an-iscsi-target

:_mod-docs-content-type: PROCEDURE

[id="creating-an-iscsi-portal_{context}"]
= Creating an iSCSI portal

[role="_abstract"]
You can create an iSCSI portal. This adds an IP address and a port to the target that keeps the target enabled.


.Prerequisites

* Installed and running `targetcli`. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#installing-targetcli_configuring-an-iscsi-target[Installing targetcli].

* An iSCSI target associated with a Target Portal Groups (TPG). For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#creating-an-iscsi-target_configuring-an-iscsi-target[Creating an iSCSI target].

.Procedure

. Navigate to the TPG directory:
+
[subs="+quotes"]
----
/iscsi> *iqn.2006-04.example:444/tpg1/*
----

. Use one of the following options to create an iSCSI portal:

.. Creating a default portal uses the default iSCSI port `3260` and allows the target to listen to all IP addresses on that port:
+
[subs="+quotes"]
----
/iscsi/iqn.20...mple:444/tpg1> *portals/ create*

Using default IP port 3260
Binding to INADDR_Any (0.0.0.0)
Created network portal 0.0.0.0:3260
----

// [NOTE]
// ====
// When an iSCSI target is created, a default portal is also created. This portal is set to listen to all IP addresses with the default port number that is: `0.0.0.0:3260`.

// To remove the default portal, use the following command:
// ----
// /iscsi/iqn-name/tpg1/portals delete ip_address=0.0.0.0 ip_port=3260
// ----
// ====

.. Creating a portal using a specific IP address:
+
[subs="+quotes"]
----
/iscsi/iqn.20...mple:444/tpg1> *portals/ create _192.168.122.137_*

Using default IP port 3260
Created network portal 192.168.122.137:3260
----

.Verification

* Verify the newly created portal:
+
[subs="+quotes"]
----
/iscsi/iqn.20...mple:444/tpg1> *ls*

o- tpg.................................. [enambled, auth]
    o- acls ......................................[0 ACL]
    o- luns ......................................[0 LUN]
    o- portals ................................[1 Portal]
       o- 192.168.122.137:3260......................[OK]
----

[role="_additional-resources"]
.Additional resources
* `targetcli(8)` man page on your system
