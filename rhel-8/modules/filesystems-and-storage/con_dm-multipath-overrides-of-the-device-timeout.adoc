// Module included in the following assemblies:
//configuring-an-iscsi-initiator
//using-fibre-channel-devices
:_mod-docs-content-type: CONCEPT

[id="dm-multipath-overrides-of-the-device-timeout_{context}"]
= DM Multipath overrides of the device timeout

The `recovery_tmo` `sysfs` option controls the timeout for a particular iSCSI device. The following options globally override the `recovery_tmo` values:

* The `replacement_timeout` configuration option globally overrides the `recovery_tmo` value for all iSCSI devices.

* For all iSCSI devices that are managed by DM Multipath, the `fast_io_fail_tmo` option in DM Multipath globally overrides the `recovery_tmo` value.
+
The `fast_io_fail_tmo` option in DM Multipath also overrides the `fast_io_fail_tmo` option in Fibre Channel devices.

The DM Multipath `fast_io_fail_tmo` option takes precedence over `replacement_timeout`. {RH} does not recommend using `replacement_timeout` to override `recovery_tmo` in devices managed by DM Multipath because DM Multipath always resets `recovery_tmo`, when the `multipathd` service reloads.
