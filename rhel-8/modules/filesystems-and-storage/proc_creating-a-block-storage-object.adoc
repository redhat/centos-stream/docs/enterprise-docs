// Module included in the following assemblies:
//configuring-an-iscsi-target

:_mod-docs-content-type: PROCEDURE

[id="creating-a-block-storage-object_{context}"]
= Creating a block storage object

[role="_abstract"]
The block driver allows the use of any block device that appears in the `/sys/block/` directory to be used with Linux-IO (LIO). This includes physical devices such as, HDDs, SSDs, CDs, and DVDs, and logical devices such as, software or hardware RAID volumes, or LVM volumes.

.Prerequisites

* Installed and running `targetcli`. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#installing-targetcli_configuring-an-iscsi-target[Installing targetcli].

.Procedure

. Navigate to the `block/` from the `backstores/` directory:
+
[subs="+quotes"]
----
/> *backstores/block/*
----

. Create a `block` backstore:
+
[subs="+quotes"]
----
/backstores/block> *create name=_block_backend_ dev=/dev/_sdb_*

Generating a wwn serial.
Created block storage object block_backend using /dev/vdb.
----

.Verification

* Verify the created `block` storage object:
+
[subs="+quotes"]
----
/backstores/block> *ls*
----

[role="_additional-resources"]
.Additional resources
* `targetcli(8)` man page on your system
