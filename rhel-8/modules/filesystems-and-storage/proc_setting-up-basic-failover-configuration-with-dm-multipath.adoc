:_mod-docs-content-type: PROCEDURE

[id="setting-up-basic-failover-configuration-with-dm-multipath_{context}"]
= Setting up basic failover configuration with DM Multipath

You can set up DM Multipath for a basic failover configuration and edit the `/etc/multipath.conf` file before starting the multipathd daemon.


.Prerequisites

* Administrative access.

.Procedure

. Enable and initialize the multipath configuration file:
+
[subs=+quotes]
----
# *mpathconf --enable*
----

. Optional: Edit the `/etc/multipath.conf` file.
+
Most default settings are already configured, including `path_grouping_policy` which is set to `failover`.

. Optional: The default naming format of multipath devices is set to `/dev/mapper/mpathn` format. If you prefer a different naming format:

.. Configure DM Multipath to use the multipath device WWID as its name, instead of the mpath_n_ user-friendly naming scheme:
+
[subs=+quotes]
----
# *mpathconf --enable --user_friendly_names n*
----

.. Reload the configuration of the DM Multipath daemon:
+
[subs=+quotes]
----
# *systemctl reload multipathd.service*
----

. Start the DM Multipath daemon:
+
[subs=+quotes]
----
# *systemctl start multipathd.service*
----

.Verification

* Confirm that the DM Multipath daemon is running without issues:
+
[subs=+quotes]
----
# *systemctl status multipathd.service*
----

* Verify the naming format of multipath devices:
+
[subs=+quotes]
----
# *ls /dev/mapper/*
----

