:_mod-docs-content-type: PROCEDURE

[id="reducing-swap-on-an-lvm2-logical-volume_{context}"]
= Reducing swap on an LVM2 logical volume

[role="_abstract"]
You can reduce swap on an LVM2 logical volume. Assuming _/dev/VolGroup00/LogVol01_ is the volume you want to reduce.

.Procedure

. Disable swapping for the associated logical volume:
+
[subs="+quotes"]
----
# *swapoff -v _/dev/VolGroup00/LogVol01_*
----

. Clean the swap signature:
+
[subs="+quotes"]
----
# *wipefs -a _/dev/VolGroup00/LogVol01_*
----

. Reduce the LVM2 logical volume by 512 MB:
+
[subs="+quotes"]
----
# *lvreduce _/dev/VolGroup00/LogVol01_ -L _-512M_*
----

. Format the new swap space:
+
[subs="+quotes"]
----
# *mkswap _/dev/VolGroup00/LogVol01_*
----

. Activate swap on the logical volume:
+
[subs="+quotes"]
----
# *swapon -v _/dev/VolGroup00/LogVol01_*
----

.Verification

* To test if the swap logical volume was successfully reduced, inspect active swap space by using the following command:
+
[subs="+quotes"]
----
$ *cat /proc/swaps*
----
+
[subs="+quotes"]
----
$ *free -h*
----
