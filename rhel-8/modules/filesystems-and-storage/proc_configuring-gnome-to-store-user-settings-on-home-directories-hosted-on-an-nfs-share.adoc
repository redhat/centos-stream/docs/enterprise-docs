:_newdoc-version: 2.17.0
:_template-generated: 2024-04-18
:_mod-docs-content-type: PROCEDURE

[id="configuring-gnome-to-store-user-settings-on-home-directories-hosted-on-an-nfs-share_{context}"]
= Configuring GNOME to store user settings on home directories hosted on an NFS share

If you use GNOME on a system with home directories hosted on an NFS server, you must change the `keyfile` backend of the `dconf` database. Otherwise, `dconf` might not work correctly. 

This change affects all users on the host because it changes how `dconf` manages user settings and configurations stored in the home directories.

ifeval::[{ProductNumber}==8]
Note that the dconf keyfile backend only works if the `glib2-fam` package is installed. Without this package, notifications on configuration changes made on remote machines are not displayed properly.

With Red Hat Enterprise Linux 8, `glib2-fam` package is available in the BaseOS repository.

.Prerequisites

* The `glib2-fam` package is installed:
+
[subs=+quotes]
----
# yum install glib2-fam
----
endif::[]

.Procedure

. Add the following line to the beginning of the `/etc/dconf/profile/user` file. If the file does not exist, create it.
+
[subs=+quotes]
----
service-db:keyfile/user
----
+
With this setting, `dconf` polls the `keyfile` back end to determine whether updates have been made, so settings might not be updated immediately.

. The changes take effect when the users logs out and in.