:_mod-docs-content-type: PROCEDURE

// Module included in the following assemblies:
// <List assemblies here, each on a new line>
// assembly_configuring-raid-logical-volumes.adoc

[id="creating-raid-logical-volumes_{context}"]
= Creating RAID logical volumes

You can create RAID1 arrays with multiple numbers of copies, according to the value you specify for the `-m` argument. Similarly, you can specify the number of stripes for a RAID 0, 4, 5, 6, and 10 logical volume with the `-i` argument. You can also specify the stripe size with the `-I` argument. The following procedure describes different ways to create different types of RAID logical volume.

.Procedure

* Create a 2-way RAID. The following command creates a 2-way RAID1 array, named _my_lv_, in the volume group _my_vg_, that is _1G_ in size:
+
[subs=quotes]
....
# *lvcreate --type raid1 -m _1_ -L _1G_ -n _my_lv my_vg_*
Logical volume "_my_lv_" created.
....

* Create a RAID5 array with stripes. The following command creates a RAID5 array with three stripes and one implicit parity drive, named _my_lv_, in the volume group _my_vg_, that is _1G_ in size. Note that you can specify the number of stripes similar to an LVM striped volume. The correct number of parity drives is added automatically.
+
[subs=quotes]
....
# *lvcreate --type raid5 -i _3_ -L _1G_ -n _my_lv my_vg_*
....

* Create a RAID6 array with stripes. The following command creates a RAID6 array with three 3 stripes and two implicit parity drives, named _my_lv_, in the volume group _my_vg_, that is _1G_ one gigabyte in size:
+
[subs=quotes]
....
# *lvcreate --type raid6 -i _3_ -L _1G_ -n _my_lv my_vg_*
....

.Verification

* Display the LVM device _my_vg/my_lv_, which is a 2-way RAID1 array:
+
[subs=quotes]
....
# *lvs -a -o name,copy_percent,devices _my_vg_*
  LV                Copy%  Devices
  my_lv             6.25    my_lv_rimage_0(0),my_lv_rimage_1(0)
  [my_lv_rimage_0]         /dev/sde1(0)
  [my_lv_rimage_1]         /dev/sdf1(1)
  [my_lv_rmeta_0]          /dev/sde1(256)
  [my_lv_rmeta_1]          /dev/sdf1(0)
....

[role="_additional-resources"]
.Additional resources
* `lvcreate(8)` and `lvmraid(7)` man pages on your system