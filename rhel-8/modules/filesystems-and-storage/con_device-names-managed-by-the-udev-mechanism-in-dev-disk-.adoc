:_mod-docs-content-type: CONCEPT
[id="con_device-names-managed-by-the-udev-mechanism-in-dev-disk-_{context}"]
= Device names managed by the udev mechanism in /dev/disk/

[role="_abstract"]
The `udev` mechanism is used for all types of devices in Linux, and is not limited only for storage devices. It provides different kinds of persistent naming attributes in the `/dev/disk/` directory. In the case of storage devices, {RHEL} contains `udev` rules that create symbolic links in the `/dev/disk/` directory. This enables you to refer to storage devices by:

* Their content
* A unique identifier
* Their serial number.

Although `udev` naming attributes are persistent, in that they do not change on their own across system reboots, some are also configurable.

[id="file-system-identifiers_{context}"]
== File system identifiers

[discrete]
== The UUID attribute in /dev/disk/by-uuid/

Entries in this directory provide a symbolic name that refers to the storage device by a *unique identifier* (UUID) in the content (that is, the data) stored on the device. For example:

[subs=+quotes]
----
/dev/disk/by-uuid/__3e6be9de-8139-11d1-9106-a43f08d823a6__
----

You can use the UUID to refer to the device in the [filename]`/etc/fstab` file using the following syntax:

[subs=+quotes]
----
UUID=__3e6be9de-8139-11d1-9106-a43f08d823a6__
----

You can configure the UUID attribute when creating a file system, and you can also change it later on.

[discrete]
== The Label attribute in /dev/disk/by-label/

Entries in this directory provide a symbolic name that refers to the storage device by a *label* in the content (that is, the data) stored on the device.

For example:

[subs=+quotes]
----
/dev/disk/by-label/__Boot__
----

You can use the label to refer to the device in the [filename]`/etc/fstab` file using the following syntax:

[subs=+quotes]
----
LABEL=__Boot__
----

You can configure the Label attribute when creating a file system, and you can also change it later on.


[id="device-identifiers_{context}"]
== Device identifiers

[discrete]
== The WWID attribute in /dev/disk/by-id/

The World Wide Identifier (WWID) is a persistent, *system-independent identifier* that the SCSI Standard requires from all SCSI devices. The WWID identifier is guaranteed to be unique for every storage device, and independent of the path that is used to access the device. The identifier is a property of the device but is not stored in the content (that is, the data) on the devices.

This identifier can be obtained by issuing a SCSI Inquiry to retrieve the Device Identification Vital Product Data (page `0x83`) or Unit Serial Number (page `0x80`).

{RHEL} automatically maintains the proper mapping from the WWID-based device name to a current `/dev/sd` name on that system. Applications can use the `/dev/disk/by-id/` name to reference the data on the disk, even if the path to the device changes, and even when accessing the device from different systems.

ifeval::[{ProductNumber} == 9]
[NOTE]
====
If your are using an NVMe device, you might run into a disk by-id naming change for some vendors, if the serial number of your device has leading whitespace.
====
endif::[]

.WWID mappings
====

[options="header",cols="2,1,1"]
|====
| WWID symlink | Non-persistent device | Note
| [filename]`/dev/disk/by-id/scsi-3600508b400105e210000900000490000` | [filename]`/dev/sda`  | A device with a page `0x83` identifier
| [filename]`/dev/disk/by-id/scsi-SSEAGATE_ST373453LW_3HW1RHM6`      | [filename]`/dev/sdb`  | A device with a page `0x80` identifier
| [filename]`/dev/disk/by-id/ata-SAMSUNG_MZNLN256HMHQ-000L7_S2WDNX0J336519-part3`    | [filename]`/dev/sdc3` | A disk partition
|====

====

In addition to these persistent names provided by the system, you can also use `udev` rules to implement persistent names of your own, mapped to the WWID of the storage.

[discrete]
== The Partition UUID attribute in /dev/disk/by-partuuid

The Partition UUID (PARTUUID) attribute identifies partitions as defined by GPT partition table.

.Partition UUID mappings
====

[options="header",cols="3,2"]
|====
| PARTUUID symlink | Non-persistent device
| [filename]`/dev/disk/by-partuuid/4cd1448a-01` | [filename]`/dev/sda1`
| [filename]`/dev/disk/by-partuuid/4cd1448a-02` | [filename]`/dev/sda2`
| [filename]`/dev/disk/by-partuuid/4cd1448a-03` | [filename]`/dev/sda3`
|====

====

[discrete]
== The Path attribute in /dev/disk/by-path/

This attribute provides a symbolic name that refers to the storage device by the *hardware path* used to access the device.

The Path attribute fails if any part of the hardware path (for example, the PCI ID, target port, or LUN number) changes. The Path attribute is therefore unreliable. However, the Path attribute may be useful in one of the following scenarios:

* You need to identify a disk that you are planning to replace later.
* You plan to install a storage service on a disk in a specific location.

// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing concept modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
