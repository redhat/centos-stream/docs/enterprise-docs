:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::modules/<subsystem>/proc_repairing-an-xfs-file-system-with-xfs_repair.adoc[leveloffset=+1]
:_module-type: PROCEDURE
[id="proc_repairing-an-xfs-file-system-with-xfs_repair_{context}"]
= Repairing an XFS file system with xfs_repair

[role="_abstract"]
This procedure repairs a corrupted XFS file system using the `xfs_repair` utility.

.Procedure

. Create a metadata image prior to repair for diagnostic or testing purposes using the `xfs_metadump` utility. A pre-repair file system metadata image can be useful for support investigations if the corruption is due to a software bug. Patterns of corruption present in the pre-repair image can aid in root-cause analysis.
+
--
** Use the `xfs_metadump` debugging tool to copy the metadata from an XFS file system to a file. The resulting `metadump` file can be compressed using standard compression utilities to reduce the file size if large `metadump` files need to be sent to support.
+
[subs=+quotes]
----
# *xfs_metadump [replaceable]_block-device_ [replaceable]_metadump-file_*
----
--

. Replay the log by remounting the file system:
+
[subs=+quotes]
----
# *mount [replaceable]_file-system_*
# *umount [replaceable]_file-system_*
----

. Use the `xfs_repair` utility to repair the unmounted file system:
+
--
** If the mount succeeded, no additional options are required:
+
[subs=+quotes]
----
# *xfs_repair [replaceable]_block-device_*
----

** If the mount failed with the _Structure needs cleaning_ error, the log is corrupted and cannot be replayed. Use the [option]`-L` option (_force log zeroing_) to clear the log:
+
WARNING: This command causes all metadata updates in progress at the time of the crash to be lost, which might cause significant file system damage and data loss. This should be used only as a last resort if the log cannot be replayed.
+
[subs=+quotes]
----
# *xfs_repair -L [replaceable]_block-device_*
----
--

. Mount the file system:
+
[subs=+quotes]
----
# *mount [replaceable]_file-system_*
----

[role="_additional-resources"]
.Additional resources
* `xfs_repair(8)` man page on your system
