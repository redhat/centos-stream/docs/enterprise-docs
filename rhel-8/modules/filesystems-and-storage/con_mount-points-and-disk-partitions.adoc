:_mod-docs-content-type: CONCEPT

[id="mount-points-and-disk-partitions_{context}"]
= Mount points and disk partitions

In {RHEL}, each partition forms a part of the storage, necessary to support a single set of files and directories. Mounting a partition makes the storage of that partition available, starting at the specified directory known as a _mount point_.

For example, if partition `/dev/sda5` is mounted on `/usr/`, it means that all files and directories under `/usr/` physically reside on `/dev/sda5`. The file
`/usr/share/doc/FAQ/txt/Linux-FAQ` resides on `/dev/sda5`, while the file `/etc/gdm/custom.conf` does not.

Continuing the example, it is also possible that one or more directories below `/usr/` would be mount points for other partitions. For example, `/usr/local/man/whatis` resides on `/dev/sda7`, rather than on `/dev/sda5`, if `/usr/local` includes a mounted `/dev/sda7` partition.
