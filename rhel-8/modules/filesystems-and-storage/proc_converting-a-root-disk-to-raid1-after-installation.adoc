:_mod-docs-content-type: PROCEDURE

// Module included in the following assemblies:
// assembly_managing-raid.adoc

[id="converting-a-root-disk-to-raid1-after-installation_{context}"]
= Converting a root disk to RAID1 after installation

You can convert a non-RAID root disk to a RAID1 mirror after installing {RHEL} {ProductNumber}.

On the PowerPC (PPC) architecture, take the following additional steps:

.Prerequisites

* Completed the steps in the Red{nbsp}Hat Knowledgebase solution link:https://access.redhat.com/solutions/2390831[How do I convert my root disk to RAID1 after installation of {RHEL} 7?].
+
[NOTE]
====
Executing the `grub2-install _/dev/sda_` command does not work on a PowerPC machine and returns an error, but the system boots as expected.
====

.Procedure

. Copy the contents of the PowerPC Reference Platform (PReP) boot partition from _/dev/sda1_ to _/dev/sdb1_:
+
[subs="+quotes"]
----
# *dd if=_/dev/sda1_ of=_/dev/sdb1_*
----

. Update the `prep` and `boot` flag on the first partition on both disks:
+
[subs="+quotes"]
----
$ *parted _/dev/sda_ set 1 prep on*
$ *parted _/dev/sda_ set 1 boot on*
 
$ *parted _/dev/sdb_ set 1 prep on*
$ *parted _/dev/sdb_ set 1 boot on*
----
