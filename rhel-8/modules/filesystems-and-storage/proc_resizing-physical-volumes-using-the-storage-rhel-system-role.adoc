:_newdoc-version: 2.18.3
:_template-generated: 2024-10-02
:_mod-docs-content-type: PROCEDURE

[id="resizing-physical-volumes-using-the-storage-rhel-system-role_{context}"]
= Resizing physical volumes by using the `storage` {RHELSystemRoles}

With the `storage` system role, you can resize LVM physical volumes after resizing the underlying storage or disks from outside of the host. For example, you increased the size of a virtual disk and want to use the extra space in an existing LVM.


.Prerequisites
// Always include prerequisites that are the same for all system role procedures:
include::common-content/snip_common-prerequisites.adoc[]
* The size of the underlying block storage has been changed.


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Manage local storage
  hosts: managed-node-01.example.com
  tasks:
    - name: Resize LVM PV size
      ansible.builtin.include_role:
        name: rhel-system-roles.storage
      vars:
        storage_pools:
           - name: _myvg_
             disks: _["sdf"]_
             type: lvm
             *grow_to_fill: true*
....

+
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file on the control node.

. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification
* Display the new physical volume size:
+
[subs="+quotes"]
....
$ *ansible managed-node-01.example.com -m command -a 'pvs'*
PV         VG   Fmt Attr PSize PFree
_/dev/sdf1_ _myvg_ lvm2 a-- 1,99g 1,99g
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file
* `/usr/share/doc/rhel-system-roles/storage/` directory