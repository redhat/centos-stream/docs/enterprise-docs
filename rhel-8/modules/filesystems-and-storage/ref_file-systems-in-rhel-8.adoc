:_mod-docs-content-type: REFERENCE
[id="file-systems_{context}"]
= File systems

[id="btrfs-has-been-removed_{context}"]
== Btrfs has been removed

The Btrfs file system has been removed in Red Hat Enterprise Linux 8. This includes the following components:

* The `btrfs.ko` kernel module
* The `btrfs-progs` package
* The `snapper` package

You can no longer create, mount, or install on Btrfs file systems in Red Hat Enterprise Linux 8. The Anaconda installer and the Kickstart commands no longer support Btrfs.

//(BZ#1582530)
//(BZ#1533904)


[id="xfs-now-supports-shared-copy-on-write-data-extents_{context}"]
== XFS now supports shared copy-on-write data extents

The XFS file system supports shared copy-on-write data extent functionality. This feature enables two or more files to share a common set of data blocks. When either of the files sharing common blocks changes, XFS breaks the link to common blocks and creates a new file. This is similar to the copy-on-write (COW) functionality found in other file systems.

Shared copy-on-write data extents are:

Fast:: Creating shared copies does not utilize disk I/O.
Space-efficient:: Shared blocks do not consume additional disk space.
Transparent:: Files sharing common blocks act like regular files.

Userspace utilities can use shared copy-on-write data extents for:

* Efficient file cloning, such as with the `cp --reflink` command
* Per-file snapshots

This functionality is also used by kernel subsystems such as Overlayfs and NFS for more efficient operation.

Shared copy-on-write data extents are now enabled by default when creating an XFS file system, starting with the `xfsprogs` package version `4.17.0-2.el8`.

Note that Direct Access (DAX) devices currently do not support XFS with shared copy-on-write data extents. To create an XFS file system without this feature, use the following command:

[subs=+quotes]
----
# mkfs.xfs -m reflink=0 _block-device_
----

Red Hat Enterprise Linux 7 can mount XFS file systems with shared copy-on-write data extents only in the read-only mode.


[id="the-ext4-file-system-now-supports-metadata-checksums_{context}"]
== The ext4 file system now supports metadata checksums

With this update, ext4 metadata is protected by checksums. This enables the file system to recognize the corrupt metadata, which avoids damage and increases the file system resilience.


[id="the-etc-sysconfig-nfs-file-and-legacy-nfs-service-names-are-no-longer-available_{context}"]
== The `/etc/sysconfig/nfs` file and legacy NFS service names are no longer available

In Red Hat Enterprise Linux 8.0, the NFS configuration has moved from the `/etc/sysconfig/nfs` configuration file, which was used in Red Hat Enterprise Linux 7, to `/etc/nfs.conf`.

The `/etc/nfs.conf` file uses a different syntax. Red Hat Enterprise Linux 8 attempts to automatically convert all options from `/etc/sysconfig/nfs` to `/etc/nfs.conf` when upgrading from Red Hat Enterprise Linux 7.

Both configuration files are supported in Red Hat Enterprise Linux 7. Red Hat recommends that you use the new `/etc/nfs.conf` file to make NFS configuration in all versions of Red Hat Enterprise Linux compatible with automated configuration systems.

Additionally, the following NFS service aliases have been removed and replaced by their upstream names:

* `nfs.service`, replaced by `nfs-server.service`
* `nfs-secure.service`, replaced by `rpc-gssd.service`
* `rpcgssd.service`, replaced by `rpc-gssd.service`
* `nfs-idmap.service`, replaced by `nfs-idmapd.service`
* `rpcidmapd.service`, replaced by `nfs-idmapd.service`
* `nfs-lock.service`, replaced by `rpc-statd.service`
* `nfslock.service`, replaced by `rpc-statd.service`

//(BZ#1639432)

////
//duplicate of a description in shells
[id="the-nobody-user-replaces-nfsnobody_{context}"]
== The `nobody` user replaces `nfsnobody`

In Red Hat Enterprise Linux 7, there was:

* the `nobody` user and group pair with the ID of 99, and
* the `nfsnobody` user and group pair with the ID of 65534, which is the default kernel overflow ID, too.

Both of these have been merged into the `nobody` user and group pair, which uses the 65534 ID in Red Hat Enterprise Linux 8. New installations no longer create the `nfsnobody` pair.

This change reduces the confusion about files that are owned by `nobody` but have nothing to do with NFS.
////
