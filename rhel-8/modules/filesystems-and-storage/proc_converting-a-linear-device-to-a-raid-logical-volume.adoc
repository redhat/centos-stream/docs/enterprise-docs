:_mod-docs-content-type: PROCEDURE

// Module included in the following assemblies:
// <List assemblies here, each on a new line>
// assembly_configuring-raid-logical-volumes.adoc

[id="converting-a-linear-device-to-a-raid-logical-volume_{context}"]
= Converting a linear device to a RAID logical volume

You can convert an existing linear logical volume to a RAID logical volume. To perform this operation, use the `--type` argument of the `lvconvert` command.

RAID logical volumes are composed of metadata and data subvolume pairs. When you convert a linear device to a RAID1 array, it creates a new metadata subvolume and associates it with the original logical volume on one of the same physical volumes that the linear volume is on. The additional images are added in a metadata/data subvolume pair. If the metadata image that pairs with the original logical volume cannot be placed on the same physical volume, the `lvconvert` fails.

.Procedure

. View the logical volume device that needs to be converted:
+
[subs=quotes]
....
# *lvs -a -o name,copy_percent,devices _my_vg_*
  LV     Copy%  Devices
  my_lv         /dev/sde1(0)
....

. Convert the linear logical volume to a RAID device. The following command converts the linear logical volume _my_lv_ in volume group __my_vg, to a 2-way RAID1 array:
+
[subs=quotes]
....
# *lvconvert --type raid1 -m _1 my_vg/my_lv_*
  Are you sure you want to convert linear LV my_vg/my_lv to raid1 with 2 images enhancing resilience? [y/n]: y
  Logical volume _my_vg/my_lv_ successfully converted.
....



.Verification
* Ensure if the logical volume is converted to a RAID device:
+
[subs=quotes]
....
# *lvs -a -o name,copy_percent,devices _my_vg_*
  LV               Copy%  Devices
  my_lv            6.25   my_lv_rimage_0(0),my_lv_rimage_1(0)
  [my_lv_rimage_0]        /dev/sde1(0)
  [my_lv_rimage_1]        /dev/sdf1(1)
  [my_lv_rmeta_0]         /dev/sde1(256)
  [my_lv_rmeta_1]         /dev/sdf1(0)
....

[role="_additional-resources"]
.Additional resources
* `lvconvert(8)` man page on your system