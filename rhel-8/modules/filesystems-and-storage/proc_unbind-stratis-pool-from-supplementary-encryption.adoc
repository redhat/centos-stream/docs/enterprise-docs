:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="unbind-encrypted-stratis-pool-from-supplementary-encryption_{context}"]

= Unbinding a Stratis pool from supplementary encryption

[role="_abstract"]
When you unbind an encrypted Stratis pool from a supported supplementary encryption mechanism, the primary kernel keyring encryption remains in place. This is not true for pools that are created with Clevis encryption from the start.

.Prerequisites
* Stratis v2.3.0 or later is installed on your system. For more information, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#installing-stratis_setting-up-stratis-file-systems[Installing Stratis].

* You have created an encrypted Stratis pool. For more information, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#create-encrypted-stratis-pool_setting-up-stratis-file-systems[Creating an encrypted Stratis pool].

* The encrypted Stratis pool is bound to a supported supplementary encryption mechanism.

.Procedure
* Unbind an encrypted Stratis pool from a supplementary encryption mechanism:
+
[subs="+quotes,attributes"]
----
# *stratis pool unbind clevis [replaceable]_my-pool_*
----
+
where
+
`_my-pool_` specifies the name of the Stratis pool you want to unbind.

[role="_additional-resources"]
.Additional resources
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#bind-stratis-pool-nbde_setting-up-stratis-file-systems[Binding an encrypted Stratis pool to NBDE]


* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#bind-stratis-pool-tpm_setting-up-stratis-file-systems[Binding an encrypted Stratis pool to TPM]

