:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
:_module-type: PROCEDURE
[id='proc_backing-up-an-xfs-file-system-with-xfsdump-{context}']
= Backing up an XFS file system with xfsdump

[role="_abstract"]
This procedure describes how to back up the content of an XFS file system into a file or a tape.

.Prerequisites

* An XFS file system that you can back up.
* Another file system or a tape drive where you can store the backup.

.Procedure

* Use the following command to back up an XFS file system:
+
[subs=+quotes]
----
# *xfsdump -l _level_ [-L _label_] \
          -f _backup-destination_ _path-to-xfs-filesystem_*
----

** Replace _level_ with the dump level of your backup. Use `0` to perform a full backup or `1` to `9` to perform consequent incremental backups.
** Replace _backup-destination_ with the path where you want to store your backup. The destination can be a regular file, a tape drive, or a remote tape device. For example, [filename]`/backup-files/Data.xfsdump` for a file or [filename]`/dev/st0` for a tape drive.
** Replace _path-to-xfs-filesystem_ with the mount point of the XFS file system you want to back up. For example, [filename]`/mnt/data/`. The file system must be mounted.
** When backing up multiple file systems and saving them on a single tape device, add a session label to each backup using the `-L _label_` option so that it is easier to identify them when restoring. Replace _label_ with any name for your backup: for example, `backup_data`.


.Backing up multiple XFS file systems
====
* To back up the content of XFS file systems mounted on the [filename]`/boot/` and [filename]`/data/` directories and save them as files in the [filename]`/backup-files/` directory:
+
[subs=+quotes]
----
# *xfsdump -l 0 -f _/backup-files/boot.xfsdump_ _/boot_*
# *xfsdump -l 0 -f _/backup-files/data.xfsdump_ _/data_*
----

* To back up multiple file systems on a single tape device, add a session label to each backup using the `-L _label_` option:
+
[subs=+quotes]
----
# *xfsdump -l 0 -L _"backup_boot"_ -f _/dev/st0_ _/boot_*
# *xfsdump -l 0 -L _"backup_data"_ -f _/dev/st0_ _/data_*
----
====

[role="_additional-resources"]
.Additional resources
* `xfsdump(8)` man page on your system
