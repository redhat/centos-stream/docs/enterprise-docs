:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="create-encrypted-stratis-pool_{context}"]

= Creating an encrypted Stratis pool
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
To secure your data, you can create an encrypted Stratis pool from one or more block devices.

When you create an encrypted Stratis pool, the kernel keyring is used as the primary encryption mechanism. After subsequent system reboots this kernel keyring is used to unlock the encrypted Stratis pool.

When creating an encrypted Stratis pool from one or more block devices, note the following:

* Each block device is encrypted using the `cryptsetup` library and implements the `LUKS2` format.
* Each Stratis pool can either have a unique key or share the same key with other pools. These keys are stored in the kernel keyring.
* The block devices that comprise a Stratis pool must be  either all encrypted or all unencrypted. It is not possible to have both encrypted and unencrypted block devices in the same Stratis pool.
* Block devices added to the data tier of an encrypted Stratis pool are automatically encrypted.

.Prerequisites
* Stratis v2.1.0 or later is installed. For more information, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#installing-stratis_setting-up-stratis-file-systems[Installing Stratis].

* The `stratisd` service is running.
* The block devices on which you are creating a Stratis pool are not in use and are not mounted.
* The block devices on which you are creating a Stratis pool are at least 1GB in size each.
* On the IBM Z architecture, the `/dev/dasd*` block devices must be partitioned. Use the partition in the Stratis pool.
+
For information about partitioning DASD devices, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/interactively_installing_rhel_over_the_network/configuring-a-linux-instance-on-ibm-z_rhel-installer[Configuring a Linux instance on IBM Z].

.Procedure

. Erase any file system, partition table, or RAID signatures that exist on each block device that you want to use in the Stratis pool:
+
[subs="+quotes"]
----
# *wipefs --all [replaceable]_block-device_*
----
+
where [replaceable]`_block-device_` is the path to the block device; for example, `/dev/sdb`.

. If you have not created a key set already, run the following command and follow the prompts to create a key set to use for the encryption.
+
[subs="+quotes,attributes"]
----
# *stratis key set --capture-key [replaceable]_key-description_*
----
+
where [replaceable]`_key-description_` is a reference to the key that gets created in the kernel keyring.

. Create the encrypted Stratis pool and specify the key description to use for the encryption. You can also specify the key path using the `--keyfile-path` option instead of using the [replaceable]`_key-description_` option.
+
[subs="+quotes,attributes"]
----
# *stratis pool create --key-desc [replaceable]_key-description_ [replaceable]_my-pool_ [replaceable]_block-device_*
----
+
where

`_key-description_`:: References the key that exists in the kernel keyring, which you created in the previous step.
`_my-pool_`:: Specifies the name of the new Stratis pool.
`_block-device_`:: Specifies the path to an empty or wiped block device.
+
You can also specify multiple block devices on a single line by using the following command:
+
[subs="+quotes"]
----
# *stratis pool create --key-desc [replaceable]_key-description_ [replaceable]_my-pool_ [replaceable]_block-device-1_ [replaceable]_block-device-2_*
----

.Verification
* Verify that the new Stratis pool was created:
+
[subs="+quotes"]
----
# *stratis pool list*
----

//[role="_additional-resources"]
//.Additional resources
//* The `stratis(8)` man page on your system
