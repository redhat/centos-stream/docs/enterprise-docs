:_mod-docs-content-type: PROCEDURE
[id="overriding-or-augmenting-autofs-site-configuration-files_{context}"]
= Overriding or augmenting autofs site configuration files

// TODO: When?

[role="_abstract"]
It is sometimes useful to override site defaults for a specific mount point on a client system.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

.Initial conditions
====
For example, consider the following conditions:

* Automounter maps are stored in NIS and the [filename]`/etc/nsswitch.conf` file has the following directive:
+
----
automount:    files nis
----

* The `auto.master` file contains:
+
----
+auto.master
----

* The NIS `auto.master` map file contains:
+
----
/home auto.home
----

* The NIS `auto.home` map contains:
+
----
beth    fileserver.example.com:/export/home/beth
joe     fileserver.example.com:/export/home/joe
*       fileserver.example.com:/export/home/&
----

* The `autofs` configuration option `BROWSE_MODE` is set to `yes`:
+
----
BROWSE_MODE="yes"
----

* The file map [filename]`/etc/auto.home` does not exist.

====

.Procedure

This section describes the examples of mounting home directories from a different server and augmenting `auto.home` with only selected entries.

.Mounting home directories from a different server
====
Given the preceding conditions, let's assume that the client system needs to override the NIS map `auto.home` and mount home directories from a different server.

* In this case, the client needs to use the following [filename]`/etc/auto.master` map:
+
----
/home ­/etc/auto.home
+auto.master
----

* The [filename]`/etc/auto.home` map contains the entry:
+
----
*    host.example.com:/export/home/&
----

Because the automounter only processes the first occurrence of a mount point, the [filename]`/home` directory contains the content of [filename]`/etc/auto.home` instead of the NIS `auto.home` map.

====

.Augmenting auto.home with only selected entries
====
Alternatively, to augment the site-wide `auto.home` map with just a few entries:

. Create an [filename]`/etc/auto.home` file map, and in it put the new entries. At the end, include the NIS `auto.home` map. Then the [filename]`/etc/auto.home` file map looks similar to:
+
----
mydir someserver:/export/mydir
+auto.home
----

. With these NIS `auto.home` map conditions, listing the content of the [filename]`/home` directory outputs:
+
[subs="quotes"]
----
$ *ls /home*

beth joe mydir
----

This last example works as expected because `autofs` does not include the contents of a file map of the same name as the one it is reading. As such, `autofs` moves on to the next map source in the `nsswitch` configuration.

====

// .Additional resources
//
// * A bulleted list of links to other material closely related to the contents of the procedure module.
// * For more details on writing procedure modules, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
