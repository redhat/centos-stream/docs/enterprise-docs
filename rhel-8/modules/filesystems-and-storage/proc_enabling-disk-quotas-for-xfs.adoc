:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_file-system-quota-management-in-xfs.adoc

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="enabling-disk-quotas-for-xfs_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Enabling disk quotas for XFS
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
Enable disk quotas for users, groups, and projects on an XFS file system. Once quotas are enabled, the `xfs_quota` tool can be used to set limits and report on disk usage.

.Procedure

. Enable quotas for users:
+
[subs=+quotes]
----
# *mount -o uquota /dev/xvdb1 /xfs*
----
Replace `uquota` with `uqnoenforce` to allow usage reporting without enforcing any limits.
+

. Enable quotas for groups:
+
[subs=+quotes]
----
# *mount -o gquota /dev/xvdb1 /xfs*
----
Replace `gquota` with `gqnoenforce` to allow usage reporting without enforcing any limits.
+

. Enable quotas for projects:
+
[subs=+quotes]
----
# *mount -o pquota /dev/xvdb1 /xfs*
----
Replace `pquota` with `pqnoenforce` to allow usage reporting without enforcing any limits.
+
. Alternatively, include the quota mount options in the `/etc/fstab` file. The following example shows entries in the `/etc/fstab` file to enable quotas for users, groups, and projects, respectively, on an XFS file system. These examples also mount the file system with read/write permissions:
+
[subs=+quotes]
----
# *vim /etc/fstab*
/dev/xvdb1    /xfs    xfs    rw,quota       0  0
/dev/xvdb1    /xfs    xfs    rw,gquota      0  0
/dev/xvdb1    /xfs    xfs    rw,prjquota    0  0
----

[role="_additional-resources"]
.Additional resources
* `xfs(5)` and `xfs_quota(8)` man pages on your system
