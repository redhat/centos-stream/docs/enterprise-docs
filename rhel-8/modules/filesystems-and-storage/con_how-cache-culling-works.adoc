:_newdoc-version: 2.17.0
:_template-generated: 2024-05-31

:_mod-docs-content-type: CONCEPT

[id="how-cache-culling-works_{context}"]
= How cache culling works

The `cachefilesd` service works by caching remote data from shared file systems to free space on the local disk. This could potentially consume all available free space, which could cause problems if the disk also contains the root partition. To control this, `cachefilesd` tries to maintain a certain amount of free space by discarding old objects, such as less-recently accessed objects, from the cache. This behavior is known as cache culling. 	

Cache culling is done on the basis of the percentage of blocks and the percentage of files available in the underlying file system. There are settings in `/etc/cachefilesd.conf` which control six limits:

brun _N%_ (percentage of blocks), frun _N%_ (percentage of files)::
If the amount of free space and the number of available files in the cache rises above both these limits, then culling is turned off. 

bcull _N%_ (percentage of blocks), fcull _N%_ (percentage of files)::
If the amount of available space or the number of files in the cache falls below either of these limits, then culling is started. 		

bstop _N%_ (percentage of blocks), fstop _N%_ (percentage of files)::
If the amount of available space or the number of available files in the cache falls below either of these limits, then no further allocation of disk space or files is permitted until culling has raised things above these limits again. 		

The default value of `N` for each setting is as follows:

* `brun/frun`: 10% 	
* `bcull/fcull`: 7% 	
* `bstop/fstop`: 3% 	

When configuring these settings, the following must hold true: 	

* 0 ≤ `bstop` < `bcull` < `brun` < 100 	
* 0 ≤ `fstop` < `fcull` < `frun` < 100 	

These are the percentages of available space and available files and do not appear as 100 minus the percentage displayed by the `df` program.

[IMPORTANT]
====
Culling depends on both b__xxx__ and f__xxx__ pairs simultaneously; the user can not treat them separately. 
====