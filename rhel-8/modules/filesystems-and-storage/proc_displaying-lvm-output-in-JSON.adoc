:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_customizing-lvm-display.adoc

[id='proc_diplaying-lvm-output-in-JSON-{context}']

= Displaying LVM command output in JSON format

You can use the [option]`--reportformat` option of the LVM display commands to display the output in JSON format.

The following example shows the output of the [command]`lvs` in standard default format.

[literal,subs="+quotes,verbatim,macros,attributes"]
....

# pass:quotes[`lvs`]
  LV      VG            Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  my_raid my_vg         Rwi-a-r---  12.00m                                    100.00
  root    rhel_host-075 -wi-ao----   6.67g
  swap    rhel_host-075 -wi-ao---- 820.00m

....

The following command shows the output of the same LVM configuration when you specify JSON format.

[literal,subs="+quotes,verbatim,macros,attributes"]
....

# pass:quotes[`lvs --reportformat json`]
  {
      "report": [
          {
              "lv": [
                  {"lv_name":"my_raid", "vg_name":"my_vg", "lv_attr":"Rwi-a-r---", "lv_size":"12.00m", "pool_lv":"", "origin":"", "data_percent":"", "metadata_percent":"", "move_pv":"", "mirror_log":"", "copy_percent":"100.00", "convert_lv":""},
                  {"lv_name":"root", "vg_name":"rhel_host-075", "lv_attr":"-wi-ao----", "lv_size":"6.67g", "pool_lv":"", "origin":"", "data_percent":"", "metadata_percent":"", "move_pv":"", "mirror_log":"", "copy_percent":"", "convert_lv":""},
                  {"lv_name":"swap", "vg_name":"rhel_host-075", "lv_attr":"-wi-ao----", "lv_size":"820.00m", "pool_lv":"", "origin":"", "data_percent":"", "metadata_percent":"", "move_pv":"", "mirror_log":"", "copy_percent":"", "convert_lv":""}
              ]
          }
      ]
  }

....

You can also set the report format as a configuration option in the `/etc/lvm/lvm.conf` file, using the `output_format` setting. The [option]`--reportformat` setting of the command line, however, takes precedence over this setting.

