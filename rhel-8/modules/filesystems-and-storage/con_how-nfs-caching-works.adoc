:_newdoc-version: 2.17.0
:_template-generated: 2024-05-31

:_mod-docs-content-type: CONCEPT

[id="how-nfs-caching-works_{context}"]
= How NFS caching works

The following diagram is a high-level illustration of how FS-Cache works:

image::fs-cache.png[FS-Cache overview]

FS-Cache is designed to be as transparent as possible to the users and administrators of a system. FS-Cache allows a file system on a server to interact directly with a client’s local cache without creating an over-mounted file system. With NFS, a mount option instructs the client to mount the NFS share with FS-cache enabled. The mount point will cause automatic upload for two kernel modules: `fscache` and `cachefiles`. The `cachefilesd` daemon communicates with the kernel modules to implement the cache. 

FS-Cache does not alter the basic operation of a file system that works over the network. It merely provides that file system with a persistent place in which it can cache data. For example, a client can still mount an NFS share whether or not FS-Cache is enabled. In addition, cached NFS can handle files that will not fit into the cache (whether individually or collectively) as files can be partially cached and do not have to be read completely up front. FS-Cache also hides all I/O errors that occur in the cache from the client file system driver. 	

To provide caching services, FS-Cache needs a cache back end, the `cachefiles` service. FS-Cache requires a mounted block-based file system, that supports block mapping (`bmap`) and extended attributes as its cache back end:

* XFS
* ext3
* ext4

FS-Cache cannot arbitrarily cache any file system, whether through the network or otherwise: the shared file system’s driver must be altered to allow interaction with FS-Cache, data storage or retrieval, and metadata setup and validation. FS-Cache needs _indexing keys_ and _coherency data_ from the cached file system to support persistence: indexing keys to match file system objects to cache objects, and coherency data to determine whether the cache objects are still valid. 	

Using FS-Cache is a compromise between various factors. If FS-Cache is being used to cache NFS traffic, it may slow the client down, but can massively reduce the network and server loading by satisfying read requests locally without consuming network bandwidth.