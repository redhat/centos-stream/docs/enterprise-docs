:_newdoc-version: 2.15.1
:_template-generated: 2024-02-02
:_mod-docs-content-type: PROCEDURE

[id="enabling-quota-support-on-an-nfs-server_{context}"]
= Enabling quota support on an NFS server

[role="_abstract"]
If you want to restrict the amount of data a user or a group can store, you can configure quotas on the file system. On an NFS server, the `rpc-rquotad` service ensures that the quota is also applied to users on NFS clients.

.Prerequisites

* The NFS server is running and configured.
*  Quotas have been configured on the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_file_systems/limiting-storage-space-usage-on-ext4-with-quotas_managing-file-systems#doc-wrapper[ext] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_file_systems/assembly_limiting-storage-space-usage-on-xfs-with-quotas_managing-file-systems[XFS] file system.

.Procedure

. Verify that quotas are enabled on the directories that you export:
+
* For ext file system, enter:
+
[literal,subs="+quotes"]
----
# *quotaon -p _/nfs/projects/_*
group quota on /nfs/projects (/dev/sdb1) is on
user quota on /nfs/projects (/dev/sdb1) is on
project quota on /nfs/projects (/dev/sdb1) is off
----

* For an XFS file system, enter:
+
[literal,subs="+quotes"]
----
# *findmnt _/nfs/projects_*
TARGET    	SOURCE	FSTYPE OPTIONS
/nfs/projects /dev/sdb1 xfs	rw,relatime,seclabel,attr2,inode64,logbufs=8,logbsize=32k,*usrquota,grpquota*
----

. Install the `quota-rpc` package:
+
[literal,subs="+quotes"]
----
# *dnf install quota-rpc*
----

. Optional: By default, the quota RPC service runs on port 875. If you want to run the service on a different port, append `-p <port_number>` to the `RPCRQUOTADOPTS` variable in the `/etc/sysconfig/rpc-rquotad` file:
+
----
RPCRQUOTADOPTS="-p __<port_number>__"
----

. Optional: By default, remote hosts can only read quotas. To allow clients to set quotas, append the `-S` option to the `RPCRQUOTADOPTS` variable in the `/etc/sysconfig/rpc-rquotad` file:
+
----
RPCRQUOTADOPTS="-S"
----

. Open the port in `firewalld`:
+
[literal,subs="+quotes"]
----
# *firewall-cmd --permanent --add-port=_875_/udp*
# *firewall-cmd --reload*
----

. Enable and start the `rpc-rquotad` service:
+
[literal,subs="+quotes"]
----
# *systemctl enable --now rpc-rquotad*
----

.Verification

. On the client:
.. Mount the exported share:
+
[literal,subs="+quotes"]
----
# *mount _server.example.com:/nfs/projects/ /mnt/_*
----

.. Display the quota. The command depends on the file system of the exported directory. For example:

* To display the quota of a specific user on all mounted ext file systems, enter:
+
[literal,subs="+quotes"]
----
# *quota -u <user_name>*
Disk quotas for user demo (uid 1000):
     Filesystem     space     quota     limit     grace     files     quota      limit     grace
server.example.com:/nfs/projects
             0K       100M      200M                  0         0         0
----

* To display the user and group quota on an XFS file system, enter:
+
[literal,subs="+quotes"]
----
# *xfs_quota -x -c "report -h" _/mnt/_*
User quota on /nfs/projects (/dev/vdb1)
            Blocks         	 
User ID     Used     Soft     Hard     Warn/Grace   
---------- ---------------------------------
root        0        0        0        00 [------]
demo        0        100M     200M     00 [------]
----

[role="_additional-resources"]
.Additional resources
* `quota(1)` and `xfs_quota(8)` man pages on your system