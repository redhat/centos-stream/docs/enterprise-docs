:_mod-docs-content-type: REFERENCE

[id="tape-commands_{context}"]
= Tape commands

The following are the common `mt` commands:

.mt commands
[options="header"]
|====
|Command|Description
|`mt -f /dev/_st0_ status`| Displays the status of the tape device.
|`mt -f /dev/_st0_ erase` | Erases the entire tape.
|`mt -f /dev/_nst0_ rewind`| Rewinds the tape device.
|`mt -f /dev/_nst0_ fsf _n_`| Switches the tape head to the forward record. Here, _n_ is an optional file count. If a file count is specified, tape head skips _n_ records.
|`mt -f /dev/_nst0_ bsfm _n_`| Switches the tape head to the previous record.
|`mt -f /dev/_nst0_ eod`| Switches the tape head to the end of the data.
|====
