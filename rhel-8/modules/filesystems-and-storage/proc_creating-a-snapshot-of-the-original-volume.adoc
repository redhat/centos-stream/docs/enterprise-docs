:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//assembly_snapshot-logical-volumes.adoc
// <List assemblies here, each on a new line>

[id="creating-a-snapshot-of-the-original-volume_{context}"]
= Creating a Copy-On-Write snapshot

Upon creation, Copy-on-Write (COW) snapshots do not contain any data. Instead, it references the data blocks of the original volume at the moment of snapshot creation. When data on the original volume changes, the COW system copies the original, unchanged data to the snapshot before the change is made. This way, the snapshot grows in size only as changes occur, storing the state of the original volume at the time of the snapshot's creation. COW snapshots are efficient for short-term backups and situations with minimal data changes, offering a space-saving method to capture and revert to a specific point in time. When you create a COW snapshot, allocate enough storage for it based on the expected changes to the original volume.

Before creating a snapshot, it is important to consider the storage requirements and the intended lifespan of your snapshot. The size of the snapshot should be sufficient to capture changes during its intended lifespan, but it cannot exceed the size of the original LV. If you expect a low rate of change, a smaller snapshot size of 10%-15% might be sufficient. For LVs with a high rate of change, you might need to allocate 30% or more.

It is important to regularly monitor the snapshot's storage usage. If a snapshot reaches 100% of its allocated space, it will become invalid. You can display the information about the snapshot with the `lvs` command.

It is essential to extend the snapshot before it gets completely filled. This can be done manually by using the `lvextend` command. Alternatively, you can set up automatic extension by setting `snapshot_autoextend_threshold` and `snapshot_autoextend_percent` parameters in the `/etc/lvm/lvm.conf` file. This configuration allows `dmeventd` to automatically extend the snapshot when its usage reaches a defined threshold.

The COW snapshot allows you to access a read-only version of the file system as it was at the time the snapshot was taken. This enables backups or data analysis without interrupting the ongoing operations on the original file system. While the snapshot is mounted and being used, the original logical volume and its file system can continue to be updated and used normally.

// This seems to apply to both COW and thinly-provisioned snapshots. Well, to all LVM snapshots.
//To create a snapshot for backup purposes in a cluster environments, ensure that the LV is active exclusively on one node. Snapshots are not supported on LVs that are active across multiple nodes. Additionally, for the duration of the snapshot's existence, the LV must remain exclusively active on that single node and cannot be accessed by other nodes in the cluster.

The following procedure outlines how to create a logical volume named _origin_ from the volume group _vg001_ and then create a snapshot of it named _snap_.

.Prerequisites

* Administrative access.
* You have created a volume group. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_logical_volumes/managing-lvm-volume-groups_configuring-and-managing-logical-volumes#creating-lvm-volume-group_managing-lvm-volume-groups[Creating LVM volume group].

.Procedure

. Create a logical volume named _origin_ from the volume group _vg001_:
+
[subs=quotes]
....
# lvcreate -L _1G_ -n _origin vg001_
....

. Create a snapshot named _snap_ of _/dev/vg001/origin_ LV that is _100 MB_ in size:
+
[subs=quotes]
....
# lvcreate --size _100M_ --name _snap_ --snapshot _/dev/vg001/origin_
....

. Display the origin volume and the current percentage of the snapshot volume being used:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
....
# lvs -a -o +devices
  LV      VG    Attr       LSize  Pool Origin Data% Meta% Move Log Cpy%Sync Convert Devices
 _origin  vg001_  owi-a-s---  1.00g                                                  /dev/sde1(0)
  _snap    vg001_  swi-a-s--- 100.00m     origin 0.00                                 /dev/sde1(256)
....
// I assume there's a better way to display the Data%. Does lvs data_percent as an argument? I was unable to find a better way on the man page.

[role="_additional-resources"]
.Additional resources
* `lvcreate(8)`, `lvextend(8)`, and `lvs(8)` man pages on your system
* `/etc/lvm/lvm.conf` file
