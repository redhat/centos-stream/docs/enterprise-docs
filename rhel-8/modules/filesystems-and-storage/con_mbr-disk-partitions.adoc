:_mod-docs-content-type: CONCEPT

[id="mbr-disk-partitions_{context}"]
= MBR disk partitions

The partition table is stored at the very start of the disk, before any file system or user data. For a more clear example, the partition table is shown as being separate in the following diagrams.

.Disk with MBR partition table
// image::unused-partitioned-drive.svg[]
image::unused-partitioned-drive.png[]

As the previous diagram shows, the partition table is divided into four sections of four unused primary partitions. A primary partition is a partition on a hard disk drive that contains only one logical drive (or section). Each logical drive holds the information necessary to define a single partition, meaning that the partition table can define no more than four primary partitions.

Each partition table entry contains important characteristics of the partition:

* The points on the disk where the partition starts and ends
* The state of the partition, as only one partition can be flagged as `active`
* The type of partition

The starting and ending points define the size and location of the partition on the disk. Some of the operating systems boot loaders use the `active` flag. That means that the operating system in the partition that is marked "active" is booted.

The type is a number that identifies the anticipated usage of a partition. Some operating systems use the partition type to:

* Denote a specific file system type
* Flag the partition as being associated with a particular operating system
* Indicate that the partition contains a bootable operating system

The following diagram shows an example of a drive with a single partition. In this example, the first partition is labeled as `DOS` partition type:

.Disk with a single partition
// image::dos-single-partition.svg[]
image::dos-single-partition.png[]

[role="_additional-resources"]
.Additional resources
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/disk-partitions_managing-storage-devices#mbr-partition-types_disk-partitions[MBR partition types]
