:_mod-docs-content-type: PROCEDURE
[id="proc_authenticating-to-an-smb-share-using-a-credentials-file_{context}"]
= Creating a credentials file to authenticate to an SMB share

[role="_abstract"]
In certain situations, such as when mounting a share automatically at boot time, a share should be mounted without entering the user name and password. To implement this, create a credentials file.


.Prerequisites

* The [package]`cifs-utils` package is installed.


.Procedure

. Create a file, such as `/root/smb.cred`, and specify the user name, password, and domain name that file:
+
[literal,subs="+quotes,attributes"]
----
username=_user_name_
password=_password_
domain=_domain_name_
----

. Set the permissions to only allow the owner to access the file:
+
[literal,subs="+quotes,attributes"]
----
# *chown user_name /root/smb.cred*
# *chmod 600 /root/smb.cred*
----

You can now pass the `credentials=_file_name_` mount option to the `mount` utility or use it in the `/etc/fstab` file to mount the share without being prompted for the user name and password.

