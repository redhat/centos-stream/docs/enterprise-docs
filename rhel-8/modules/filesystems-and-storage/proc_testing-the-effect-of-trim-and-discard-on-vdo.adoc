:_mod-docs-content-type: PROCEDURE
[id="testing-the-effect-of-trim-and-discard-on-vdo_{context}"]
= Testing the effect of TRIM and DISCARD on VDO

This procedure tests whether the `TRIM` and `DISCARD` commands properly free up blocks from deleted files on a VDO test volume. It demonstrates that discards inform VDO that the space is no longer used.

.Prerequisites

* A newly created VDO test volume is mounted. For details, see xref:creating-a-vdo-test-volume_{context}[].


.Procedure

. Prepare a table where you can record the test results:
+
[options="header"]
|====
| Step | File space used (MB) | Data blocks used | Logical blocks used
| Initial | | |
| Add 1 GiB file | | |
| Run `fstrim` | | |
| Delete 1 GiB file | | |
| Run `fstrim` | | |
|====

. Trim the file system to remove unneeded blocks:
+
----
# fstrim /mnt/vdo-test
----
+
The command might take a long time.

. Record the initial space usage in the file system:
+
----
$ df -m /mnt/vdo-test
----

. See how many physical and logical data blocks the VDO volume uses:
+
----
# vdostats --verbose | grep "blocks used"
----

. Create a 1 GiB file with non-duplicate data on the VDO volume:
+
----
$ dd if=/dev/urandom of=/mnt/vdo-test/file bs=1M count=1K
----

. Record the space usage again:
+
----
$ df -m /mnt/vdo-test

# vdostats --verbose | grep "blocks used"
----
+
The file system should use an additional 1 GiB. The `data blocks used` and `logical blocks used` values should increase similarly.

. Trim the file system again:
+
----
# fstrim /mnt/vdo-test
----

. Inspect the space usage again to confirm that the trim had no impact on the physical volume usage:
+
----
$ df -m /mnt/vdo-test

# vdostats --verbose | grep "blocks used"
----

. Delete the 1 GiB file:
+
----
$ rm /mnt/vdo-test/file
----

. Check and record the space usage again:
+
----
$ df -m /mnt/vdo-test

# vdostats --verbose | grep "blocks used"
----
+
The file system is aware that a file has been deleted, but there is no change to the number of physical or logical blocks because the file deletion has not been communicated to the underlying storage.

. Trim the file system again:
+
----
# fstrim /mnt/vdo-test
----

. Check and record the space usage again:
+
----
$ df -m /mnt/vdo-test

# vdostats --verbose | grep "blocks used"
----
+
The `fstrim` utility looks for free blocks in the file system and sends a `TRIM` command to the VDO volume for unused addresses, which releases the associated logical blocks. VDO processes the `TRIM` command to release the underlying physical blocks.


// .Verification
// 
// . Optional: Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.
// . Use an unnumbered bullet (*) if the procedure includes only one step.

[role="_additional-resources"]
.Additional resources
* For more information about the `TRIM` and `DISCARD` commands, the `fstrim` utility, and the `discard` mount option, see xref:discarding-unused-blocks_{ProjectNameID}[]

