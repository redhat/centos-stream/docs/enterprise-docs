:_mod-docs-content-type: REFERENCE
[id="information-to-record-before-each-vdo-test_{context}"]
= Information to record before each VDO test

You must record the following information at the start of each test to ensure that the test environment is fully understood. You can capture much of the required information by using the `sosreport` utility.

.Required information
* The used Linux build, including the kernel build number
* The complete list of installed packages, as obtained from the [command]`rpm -qa` command
* Complete system specifications
** CPU type and quantity; available in the [filename]`/proc/cpuinfo` file
** Installed memory and the amount available after the rase OS is running; available in the [filename]`/proc/meminfo` file
** Types of used drive controllers
** Types and quantity of used disks
* A complete list of running processes; available from the [command]`ps aux` command or a similar listing
* Name of the physical volume and the volume group created for use with VDO; available from the [command]`pvs` and [command]`vgs` commands
* File system used when formatting the VDO volume, if any
* Permissions on the mounted directory
* Content of the [filename]`/etc/vdoconf.yaml` file
* Location of the VDO files

