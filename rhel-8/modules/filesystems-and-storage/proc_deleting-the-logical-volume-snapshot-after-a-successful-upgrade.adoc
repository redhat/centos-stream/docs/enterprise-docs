:_mod-docs-content-type: PROCEDURE

[id="deleting-the-logical-volume-snapshot-after-a-successful-upgrade_{context}"]
= Deleting the logical volume snapshot after a successful upgrade
[[deleting-the-logical-volume-snapshot_managing-system-upgrades-with-snapshots]]

If you have successfully upgraded your system by using the Boom Boot Manager, you can remove the snapshot boot entry and the logical volume (LV) snapshot to use the upgraded system. 

[IMPORTANT]
====
You cannot perform any further operations with the LV snapshot after you delete it.
====

.Prerequisites

* You have recently upgraded your {RHEL} to a later version xref:upgrading-to-another-version-using-boom-boot-manager_managing-system-upgrades-with-snapshots[by using the Boom Boot Manager].

.Procedure

. Boot into {RHEL} {ProductNumber} from the GRUB boot loader screen.

. After the system loads, view the available boot entries. The following output shows the snapshot boot entry in the list of boom boot entries:
+
[subs="+quotes,attributes"]
----
# *boom list*
ifeval::[{ProductNumber} == 8]
WARNING - Options for BootEntry(boot_id=cae29bf) do not match OsProfile: marking read-only
BootID  Version                      Name                            RootDevice                            
_e0252ad_ 3.10.0-1160.118.1.el7.x86_64 Red Hat Enterprise Linux Server /dev/rhel/root_snapshot_before_changes
_611ad14_ 3.10.0-1160.118.1.el7.x86_64 Red Hat Enterprise Linux Server /dev/mapper/rhel-root
_3bfed71- 3.10.0-1160.el7.x86_64       Red Hat Enterprise Linux Server /dev/mapper/rhel-root
_cae29bf_ 4.18.0-513.24.1.el8_9.x86_64 Red Hat Enterprise Linux        /dev/mapper/rhel-root
endif::[]
ifeval::[{ProductNumber} == 9]
BootID  Version                      Name                     RootDevice                            
_1e1a9b4_ 4.18.0-513.5.1.el8_9.x86_64  Red Hat Enterprise Linux /dev/mapper/rhel-root                 
_4ea37b9_ 4.18.0-513.24.1.el8_9.x86_64 Red Hat Enterprise Linux /dev/mapper/rhel-root                 
_c919f89_ 4.18.0-513.24.1.el8_9.x86_64 Red Hat Enterprise Linux /dev/rhel/root_snapshot_before_changes
endif::[]
----
+
ifeval::[{ProductNumber} == 8]
The warning is expected, you cannot change or delete these entries by using boom because the `kernel` and `grubby` packages manage them.
endif::[]

. Delete the snapshot entry by using the `BootID` value:
+
[subs="+quotes"]
----
ifeval::[{ProductNumber} == 8]
# *boom delete --boot-id _e0252ad_*
endif::[]
ifeval::[{ProductNumber} == 9]
# *boom delete --boot-id _c919f89_*
endif::[]
Deleted 1 entry
----
+
This deletes the boot entry from the GRUB menu.

. Remove the LV snapshot:
+
[subs="+quotes,attributes"]
----
# *lvremove rhel/_root_snapshot_before_changes_*
Do you really want to remove active logical volume _rhel/root_snapshot_before_changes_? [y/n]: y
      Logical volume "_root_snapshot_before_changes_" successfully removed
----

. Complete the remaining post-upgrade tasks. For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/index[Upgrade from RHEL 7 to RHEL 8].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/upgrading_from_rhel_8_to_rhel_9/index[Upgrading from RHEL 8 to RHEL 9].
endif::[]

[role="_additional-resources"]
.Additional resources
* `boom(1)` man page on your system
