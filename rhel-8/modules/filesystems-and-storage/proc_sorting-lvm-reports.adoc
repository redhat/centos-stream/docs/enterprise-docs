:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_customizing-lvm-display.adoc

[id='proc_sorting-lvm-reports-{context}']

= Sorting LVM reports

Normally the entire output of the [command]`lvs`, [command]`vgs`, or [command]`pvs` command has to be generated and stored internally before it can be sorted and columns aligned correctly. You can specify the [option]`--unbuffered` argument to display unsorted output as soon as it is generated.

To specify an alternative ordered list of columns to sort on, use the [option]`-O` argument of any of the reporting commands. It is not necessary to include these fields within the output itself.

The following example shows the output of the [command]`pvs` command that displays the physical volume name, size, and free space.

[literal,subs="+quotes,verbatim,macros,attributes"]
....

# pass:quotes[`pvs -o pv_name,pv_size,pv_free`]
  PV         PSize  PFree
  /dev/sdb1  17.14G 17.14G
  /dev/sdc1  17.14G 17.09G
  /dev/sdd1  17.14G 17.14G

....

The following example shows the same output, sorted by the free space field.

[literal,subs="+quotes,verbatim,macros,attributes"]
....

# pass:quotes[`pvs -o pv_name,pv_size,pv_free -O pv_free`]
  PV         PSize  PFree
  /dev/sdc1  17.14G 17.09G
  /dev/sdd1  17.14G 17.14G
  /dev/sdb1  17.14G 17.14G

....

The following example shows that you do not need to display the field on which you are sorting.

[literal,subs="+quotes,verbatim,macros,attributes"]
....

# pass:quotes[`pvs -o pv_name,pv_size -O pv_free`]
  PV         PSize
  /dev/sdc1  17.14G
  /dev/sdd1  17.14G
  /dev/sdb1  17.14G

....

To display a reverse sort, precede a field you specify after the [option]`-O` argument with the [option]`-` character.

[literal,subs="+quotes,verbatim,macros,attributes"]
....

# pass:quotes[`pvs -o pv_name,pv_size,pv_free -O -pv_free`]
  PV         PSize  PFree
  /dev/sdd1  17.14G 17.14G
  /dev/sdb1  17.14G 17.14G
  /dev/sdc1  17.14G 17.09G

....

