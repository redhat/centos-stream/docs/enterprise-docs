:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="bind-stratis-pool-nbde_{context}"]

////
The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

= Binding a Stratis pool to NBDE

[role="_abstract"]
Binding an encrypted Stratis pool to Network Bound Disk Encryption (NBDE) requires a Tang server. When a system containing the Stratis pool reboots, it connects with the Tang server to automatically unlock the encrypted pool without you having to provide the kernel keyring description.

[NOTE]
Binding a Stratis pool to a supplementary Clevis encryption mechanism does not remove the primary kernel keyring encryption.

.Prerequisites
* Stratis v2.3.0 or later is installed. For more information, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#installing-stratis_setting-up-stratis-file-systems[Installing Stratis].

* The `stratisd` service is running.
* You have created an encrypted Stratis pool, and you have the key description of the key that was used for the encryption. For more information, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#create-encrypted-stratis-pool_setting-up-stratis-file-systems[Creating an encrypted Stratis pool].

* You can connect to the Tang server. For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption_security-hardening#deploying-a-tang-server-with-selinux-in-enforcing-mode_configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption[Deploying a Tang server with SELinux in enforcing mode].

.Procedure
* Bind an encrypted Stratis pool to NBDE:
+
[subs="+quotes"]
----
# *stratis pool bind nbde --trust-url [replaceable]_my-pool_ [replaceable]_tang-server_*
----
+
where

`_my-pool_`:: Specifies the name of the encrypted Stratis pool.
`_tang-server_`:: Specifies the IP address or URL of the Tang server.

[role="_additional-resources"]
.Additional resources
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption_security-hardening[Configuring automated unlocking of encrypted volumes using policy-based decryption]
