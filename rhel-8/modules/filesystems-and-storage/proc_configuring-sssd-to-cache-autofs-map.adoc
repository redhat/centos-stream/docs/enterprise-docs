// Module included in the following assemblies:
//
// assembly_using-sssd-component-from-idm-to-cache-the-autofs-map.adoc

:_mod-docs-content-type: PROCEDURE
[id="configuring-sssd-to-cache-autofs-map_{context}"]
= Configuring SSSD to cache autofs maps

[role="_abstract"]
The SSSD service can be used to cache `autofs` maps stored on an IdM server without having to configure `autofs` to use the IdM server at all.

.Prerequisites

* The [package]`sssd` package is installed.

.Procedure

. Open the SSSD configuration file:
+
[literal,subs="+quotes,attributes,verbatim"]
----
# *vim /etc/sssd/sssd.conf*
----

. Add the `autofs` service to the list of services handled by SSSD.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
----
[sssd]
domains = ldap
services = nss,pam,`autofs`
----

. Create a new `[autofs]` section. You can leave this blank, because the default settings for an `autofs` service work with most infrastructures.
+
[literal,subs="+quotes,verbatim,macros,attributes"]
----
[nss]

[pam]

[sudo]

`[autofs]`

[ssh]

[pac]
----
+
For more information, see the `sssd.conf` man page on your system.

. Optional: Set a search base for the `autofs` entries. By default, this is the LDAP search base, but a subtree can be specified in the `ldap_autofs_search_base` parameter.
+
[literal,subs="+quotes,attributes,verbatim"]
----
[domain/EXAMPLE]

ldap_search_base = "dc=example,dc=com"
ldap_autofs_search_base = "ou=automount,dc=example,dc=com"
----

. Restart SSSD service:
+
[literal,subs="+quotes,attributes,verbatim"]
----
# *systemctl restart sssd.service*
----

. Check the `/etc/nsswitch.conf` file, so that SSSD is listed as a source for automount configuration:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
----
automount: `sss` files
----

. Restart `autofs` service:
+
[literal,subs="+quotes,attributes,verbatim"]
----
# *systemctl restart autofs.service*
----

. Test the configuration by listing a user's `/home` directory, assuming there is a master map entry for `/home`:
+
[literal,subs="+quotes,verbatim,macros,attributes"]
----
# *ls /home/pass:quotes[_userName_]*
----
+
If this does not mount the remote file system, check the `/var/log/messages` file for errors. If necessary, increase the debug level in the `/etc/sysconfig/autofs` file by setting the `logging` parameter to `debug`.
