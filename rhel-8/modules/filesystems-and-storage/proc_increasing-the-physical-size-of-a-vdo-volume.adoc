:_mod-docs-content-type: PROCEDURE
[id="increasing-the-physical-size-of-a-vdo-volume_{context}"]
= Increasing the physical size of a VDO volume

This procedure increases the amount of physical storage available to a VDO volume.

It is not possible to shrink a VDO volume in this way.

.Prerequisites

* The underlying block device has a larger capacity than the current physical size of the VDO volume.
+
If it does not, you can attempt to increase the size of the device. The exact procedure depends on the type of the device. For example, to resize an MBR or GPT partition, see the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/getting-started-with-partitions_managing-storage-devices#proc_resizing-a-partition-with-parted_getting-started-with-partitions[Resizing a partition] section in the _Managing storage devices_ guide.

.Procedure

* Add the new physical storage space to the VDO volume:
+
[subs=+quotes]
----
# vdo growPhysical --name=[replaceable]__my-vdo__
----
