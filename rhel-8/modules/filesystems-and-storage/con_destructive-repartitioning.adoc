:_mod-docs-content-type: CONCEPT

[id="destructive-repartitioning_{context}"]
= Destructive repartitioning

Destructive repartitioning destroys the partition on your hard drive and creates several smaller partitions instead. Backup any needed data from the original partition as this method deletes the complete contents.

After creating a smaller partition for your existing operating system, you can:

* Reinstall software.
* Restore your data.
* Start your Red Hat Enterprise Linux installation.

The following diagram is a simplified representation of using the destructive repartitioning method.

.Destructive repartitioning action on disk
// image::dstrct-reprt.svg[]
image::dstrct-reprt.png[]

[WARNING]
====
This method deletes all data previously stored in the original partition.
====
