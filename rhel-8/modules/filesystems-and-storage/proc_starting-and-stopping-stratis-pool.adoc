////
Base the file name and the ID on the module title. For example:
* file name: proc-doing-procedure-a.adoc
* ID: [id="proc-doing-procedure-a_{context}"]
* Title: = Doing procedure A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken. The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in an assembly file.

Indicate the module type in one of the following
ways:
Add the prefix proc- or proc_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: PROCEDURE

[id="proc_starting-and-stopping-stratis-pool_{context}"]
= Starting and stopping Stratis pool
////
Start the title of a procedure module with a gerund, such as Creating, Installing, or Deploying.
////

You can start and stop Stratis pools. This gives you the option to dissasemble or bring down all the objects that were used to construct the pool, such as file systems, cache devices, thin pool, and encrypted devices. Note that if the pool actively uses any device or file system, it might issue a warning and not be able to stop.

The stopped state is recorded in the pool's metadata. These pools do not start on the following boot, until the pool receives a start command.

.Prerequisites
* Stratis is installed. For more information, see
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#installing-stratis_setting-up-stratis-file-systems[Installing Stratis].

* The `stratisd` service is running.
* You have created either an unencrypted or an encrypted Stratis pool. See
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#create-unencrypted-stratis-pool_setting-up-stratis-file-systems[Creating an unencrypted Stratis pool]
or
link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_file_systems/setting-up-stratis-file-systems_managing-file-systems#create-encrypted-stratis-pool_setting-up-stratis-file-systems[Creating an encrypted Stratis pool].


////
If you have only one prerequisite, list it as a single bullet point.
Do not write prerequisites in the imperative.
You can include links to more information about the prerequisites.
Delete the .Prerequisites section title and bullets if the module has no prerequisites.
////

.Procedure

* Use the following command to start the Stratis pool. The `--unlock-method` option specifies the method of unlocking the pool if it is encrypted:
+
[subs="+quotes"]
----
# *stratis pool start _pool-uuid_ --unlock-method <keyring|clevis>*
----

* Alternatively, use the following command to stop the Stratis pool. This tears down the storage stack but leaves all metadata intact:
+
[subs="+quotes"]
----
# *stratis pool stop _pool-name_*
----

.Verification

* Use the following command to list all pools on the system:
+
[subs="+quotes"]
----
# *stratis pool list*
----

* Use the following command to list all not previously started pools. If the UUID is specified, the command prints detailed information about the pool corresponding to the UUID:
+
[subs="+quotes"]
----
# *stratis pool list --stopped --uuid _UUID_*
----
