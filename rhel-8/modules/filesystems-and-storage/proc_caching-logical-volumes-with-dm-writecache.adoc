:_newdoc-version: 2.16.0
:_template-generated: 2024-03-21
:_mod-docs-content-type: PROCEDURE

[id="caching-logical-volumes-with-dm-writecache_{context}"]
= Caching logical volumes with dm-writecache
[[caching-lv-with-dm-writecache_{context}]]

[role="_abstract"]

When caching LVs with `dm-writecache`, a caching layer between the logical volume and the physical storage device is created. `dm-writecache` operates by temporarily storing write operations in a faster storage medium, such as an SSD, before eventually writing them back to the primary storage device, optimizing write-intensive workloads.


.Prerequisites
* Administrative access.

.Procedure

. Display the logical volume you want to cache and its volume group:
[subs=quotes]
+
----
# *lvs -o lv_name,vg_name*
  LV                   VG             
  LogicalVolumeName    VolumeGroupName
----

. Create a cache volume:
+
[subs=quotes]
----
# *lvcreate --name _CacheVolumeName_ --size _Size_  _VolumeGroupName /FastDevicePath_*
----
+
Replace _CacheVolumeName_ with the name of the cache volume. Replace _Size_ with the size for your cache pool. Replace _VolumeGroupName_ with the name of the volume group. Replace _/FastDevicePath_ with the path to your fast device, for example SSD or NVME.

. Attach the cache volume to the LV:
+
[subs=quotes]
----
# *lvconvert --type writecache --cachevol _CacheVolumeName VolumeGroupName/LogicalVolumeName_*
----
+
Replace _CacheVolumeName_ with the name of the cache volume. Replace _VolumeGroupName_ with the name of the volume group. Replace _LogicalVolumeName_ with the name of the logical volume.

.Verification

* Verify that the LV is now cached:
+
[subs=quotes]
----
# *lvs -o lv_name,pool_lv*

  LV                   Pool                  
  LogicalVolumeName    [CacheVolumeName_cvol]
----

[role="_additional-resources"]
.Additional resources
* `lvcreate(8)`, `lvconvert(8)`, `lvs(8)` man pages


