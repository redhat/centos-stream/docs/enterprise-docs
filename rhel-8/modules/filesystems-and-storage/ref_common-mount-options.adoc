:_mod-docs-content-type: REFERENCE
[id="common-mount-options_{context}"]
= Common mount options

The following table lists the most common options of the `mount` utility. You can apply these mount options using the following syntax:

[subs=+quotes]
----
# *mount --options [replaceable]_option1,option2,option3_ [replaceable]_device_ [replaceable]_mount-point_*
----

.Common mount options
[options="header",cols="1,3"]
|===
| Option | Description
| `async` | Enables asynchronous input and output operations on the file system.
| `auto` | Enables the file system to be mounted automatically using the [command]`mount -a` command.
| `defaults` | Provides an alias for the `async,auto,dev,exec,nouser,rw,suid` options.
| `exec` | Allows the execution of binary files on the particular file system.
| `loop` | Mounts an image as a loop device.
| `noauto` | Default behavior disables the automatic mount of the file system using the [command]`mount -a` command.
| `noexec` | Disallows the execution of binary files on the particular file system.
| `nouser` | Disallows an ordinary user (that is, other than root) to mount and unmount the file system.
| `remount` | Remounts the file system in case it is already mounted.
| `ro` | Mounts the file system for reading only.
| `rw` | Mounts the file system for both reading and writing.
| `user` | Allows an ordinary user (that is, other than root) to mount and unmount the file system.
|===
