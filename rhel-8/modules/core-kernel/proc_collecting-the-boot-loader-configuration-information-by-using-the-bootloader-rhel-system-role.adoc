:_newdoc-version: 2.18.3
:_template-generated: 2024-07-10
:_mod-docs-content-type: PROCEDURE

[id="collecting-the-boot-loader-configuration-information-by-using-the-bootloader-rhel-system-role_{context}"]
= Collecting the boot loader configuration information by using the `bootloader` RHEL system role

You can use the `bootloader` RHEL system role to gather information about the GRUB boot loader entries in an automated fashion. You can use this information to verify the correct configuration of system boot parameters, such as kernel and initial RAM disk image paths.

As a result, you can for example:

* Prevent boot failures.
* Revert to a known good state when troubleshooting.
* Be sure that security-related kernel command-line parameters are correctly configured.


.Prerequisites

include::common-content/snip_common-prerequisites.adoc[]
// The following are prerequisites that are specific for this task:


.Procedure
. Create a playbook file, for example `~/playbook.yml`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---                              
- name: Configuration and management of GRUB boot loader    
  hosts: managed-node-01.example.com
  tasks:                         
    - name: Gather information about the boot loader configuration
      ansible.builtin.include_role:
        name: rhel-system-roles.bootloader
      vars:                      
        bootloader_gather_facts: true
                                   
    - name: Display the collected boot loader configuration information
      debug:                     
        var: bootloader_facts
....
+
// Explain only variables without a self-explaining name and if values are not clear.
// If the whole playbook is self-explaining, remove both the lead-in sentence and the definition list, and keep only the "For details..." sentence.
For details about all variables used in the playbook, see the `/usr/share/ansible/roles/rhel-system-roles.bootloader/README.md` file on the control node.


. Validate the playbook syntax:
+
[subs="+quotes"]
....
$ *ansible-playbook --syntax-check ~/playbook.yml*
....
+
Note that this command only validates the syntax and does not protect against a wrong but valid configuration.


. Run the playbook:
+
[subs="+quotes"]
....
$ *ansible-playbook ~/playbook.yml*
....


.Verification

* After you run the preceding playbook on the control node, you will see a similar command-line output as in the following example:
+
[literal,subs="+quotes"]
....
...
    "bootloader_facts": [
        {
            "args": "ro crashkernel=1G-4G:256M,4G-64G:320M,64G-:576M rd.lvm.lv=rhel/root rd.lvm.lv=rhel/swap $tuned_params quiet",
            "default": true,
            "id": "2c9ec787230141a9b087f774955795ab-5.14.0-362.24.1.el9_3.aarch64",
            "index": "1",
            "initrd": "/boot/initramfs-5.14.0-362.24.1.el9_3.aarch64.img $tuned_initrd",
            "kernel": "/boot/vmlinuz-5.14.0-362.24.1.el9_3.aarch64",
            "root": "/dev/mapper/rhel-root",
            "title": "Red Hat Enterprise Linux (5.14.0-362.24.1.el9_3.aarch64) 9.4 (Plow)"
        }
    ]
...
....
+
The command-line output shows the following notable configuration information about the boot entry:

`args`:: Command-line parameters passed to the kernel by the GRUB2 boot loader during the boot process. They configure various settings and behaviors of the kernel, initramfs, and other boot-time components. 
`id`:: Unique identifier assigned to each boot entry in a boot loader menu. It consists of machine ID and the kernel version. 
`root`:: The root filesystem for the kernel to mount and use as the primary filesystem during the boot.


[role="_additional-resources"]
.Additional resources

* `/usr/share/ansible/roles/rhel-system-roles.bootloader/README.md` file
* `/usr/share/doc/rhel-system-roles/bootloader/` directory
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumber}/html-single/managing_monitoring_and_updating_the_kernel/index#what-boot-entries-are_configuring-kernel-command-line-parameters[Understanding boot entries]
