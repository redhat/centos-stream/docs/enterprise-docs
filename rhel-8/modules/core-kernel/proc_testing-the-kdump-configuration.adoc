:_mod-docs-content-type: PROCEDURE
:experimental:


[id="testing-the-kdump-configuration_{context}"]
= Testing the kdump configuration

[role="_abstract"]
After configuring `kdump`, you must manually test a system crash and ensure that the `vmcore` file is generated in the defined `kdump` target. The `vmcore` file is captured from the context of the freshly booted kernel. Therefore, `vmcore` has critical information for debugging a kernel crash. 

[WARNING]
====
Do not test `kdump` on active production systems. The commands to test `kdump` will cause the kernel to crash with loss of data. Depending on your system architecture, ensure that you schedule significant maintenance time because `kdump` testing might require several reboots with a long boot time. 

If the `vmcore` file is not generated during the `kdump` test, identify and fix issues before you run the test again for a successful `kdump` testing. 
====

If you make any manual system modifications, you must test the `kdump` configuration at the end of any system  modification. For example, if you make any of the following changes, ensure that you test the `kdump` configuration for an optimal `kdump` performances for: 

* Package upgrades. 
* Hardware level changes, for example, storage or networking changes.
* Firmware upgrades.
* New installation and application upgrades that include third party modules.
* If you use the hot-plugging mechanism to add more memory on hardware that support this mechanism.  
* After you make changes in the `/etc/kdump.conf` or `/etc/sysconfig/kdump` file.

.Prerequisites

* You have root permissions on the system.
* You have saved all important data. The commands to test `kdump` cause the kernel to crash with loss of data.  
* You have scheduled significant machine maintenance time depending on the system architecture.


.Procedure

. Enable the `kdump` service:
+
[subs="+quotes"]
....
# *kdumpctl restart*
....

. Check the status of the `kdump` service with the `kdumpctl`:
+
[subs="+quotes,verbatim,normal"]
----
# *kdumpctl status*
  kdump:Kdump is operational
----
+
Optionally, if you use the `systemctl` command, the output prints in the `systemd` journal.

. Start a kernel crash to test the `kdump` configuration. The `sysrq-trigger` key combination causes the kernel to crash and might reboot the system if required.
+
[subs="+quotes"]
....
# *echo c > /proc/sysrq-trigger*
....
+
On a kernel reboot, the `__address__-__YYYY-MM-DD__-__HH:MM:SS__/vmcore` file is created at the location you have specified in the `/etc/kdump.conf` file. The default is `/var/crash/`.



[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#configuring-the-kdump-target_configuring-kdump-on-the-command-line[Configuring the kdump target]
