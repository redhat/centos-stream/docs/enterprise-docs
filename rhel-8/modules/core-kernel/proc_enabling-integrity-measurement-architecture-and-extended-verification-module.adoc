:_mod-docs-content-type: PROCEDURE
:experimental:
[id="enabling-integrity-measurement-architecture-and-extended-verification-module_{context}"]
= Enabling IMA and EVM

[role="_abstract"]
You can enable and configure Integrity measurement architecture (IMA) and extended verification module (EVM) to improve the security of the operating system.

[IMPORTANT]
--
Always enable EVM together with IMA.

Although you can enable EVM alone, EVM appraisal is only triggered by an IMA appraisal rule. Therefore, EVM does not protect file metadata such as SELinux attributes. If file metadata is tampered with offline, EVM can only prevent file metadata changes. It does not prevent file access, such as executing the file.
--

.Prerequisites

* Secure Boot is temporarily disabled.
+
[NOTE]
====
When Secure Boot is enabled, the `ima_appraise=fix` kernel command-line parameter does not work.
====


* The `securityfs` file system is mounted on the `/sys/kernel/security/` directory and the `/sys/kernel/security/integrity/ima/` directory exists.
You can verify where `securityfs` is mounted by using the `mount` command:
+
[subs="+quotes"]
....
# *mount*
...
securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
...
....

* The `systemd` service manager is patched to support IMA and EVM on boot time. Verify by using the following command:
+
[literal,subs="+quotes,verbatim,normal"]
----
# grep <pass:quotes[_options_]> pattern <pass:quotes[_files_]>
----
+
For example: 
+
[literal,subs="+quotes"]
ifeval::[{ProductNumber} == 8]
....
# *dmesg | grep -i -e EVM -e IMA -w*
[ 0.598533] ima: No TPM chip found, activating TPM-bypass!
[ 0.599435] ima: Allocated hash algorithm: sha256
[ 0.600266] ima: No architecture policies found
[ 0.600813] evm: Initialising EVM extended attributes:
[ 0.601581] evm: security.selinux
[ 0.601963] evm: security.ima
[ 0.602353] evm: security.capability
[ 0.602713] evm: HMAC attrs: 0x1
[ 1.455657] systemd[1]: systemd 239 (239-74.el8_8) running in system mode. (+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=legacy)
[ 2.532639] systemd[1]: systemd 239 (239-74.el8_8) running in system mode. (+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=legacy)
....
endif::[]

+
ifeval::[{ProductNumber} >= 9] 
[literal,subs="+quotes"]
----
# *dmesg | grep -i -e EVM -e IMA -w*
[ 0.943873] ima: No TPM chip found, activating TPM-bypass!
[ 0.944566] ima: Allocated hash algorithm: sha256
[ 0.944579] ima: No architecture policies found
[ 0.944601] evm: Initialising EVM extended attributes:
[ 0.944602] evm: security.selinux
[ 0.944604] evm: security.SMACK64 (disabled)
[ 0.944605] evm: security.SMACK64EXEC (disabled)
[ 0.944607] evm: security.SMACK64TRANSMUTE (disabled)
[ 0.944608] evm: security.SMACK64MMAP (disabled)
[ 0.944609] evm: security.apparmor (disabled)
[ 0.944611] evm: security.ima
[ 0.944612] evm: security.capability
[ 0.944613] evm: HMAC attrs: 0x1
[ 1.314520] systemd[1]: systemd 252-18.el9 running in system mode (+PAM +AUDIT +SELINUX -APPARMOR +IMA +SMACK +SECCOMP +GCRYPT +GNUTLS +OPENSSL +ACL +BLKID +CURL +ELFUTILS -FIDO2 +IDN2 -IDN -IPTC +KMOD +LIBCRYPTSETUP +LIBFDISK +PCRE2 -PWQUALITY +P11KIT -QRENCODE +TPM2 +BZIP2 +LZ4 +XZ +ZLIB +ZSTD -BPF_FRAMEWORK +XKBCOMMON +UTMP +SYSVINIT default-hierarchy=unified)
[ 1.717675] device-mapper: core: CONFIG_IMA_DISABLE_HTABLE is disabled. Duplicate IMA measurements will not be recorded in the IMA log.
[ 4.799436] systemd[1]: systemd 252-18.el9 running in system mode (+PAM +AUDIT +SELINUX -APPARMOR +IMA +SMACK +SECCOMP +GCRYPT +GNUTLS +OPENSSL +ACL +BLKID +CURL +ELFUTILS -FIDO2 +IDN2 -IDN -IPTC +KMOD +LIBCRYPTSETUP +LIBFDISK +PCRE2 -PWQUALITY +P11KIT -QRENCODE +TPM2 +BZIP2 +LZ4 +XZ +ZLIB +ZSTD -BPF_FRAMEWORK +XKBCOMMON +UTMP +SYSVINIT default-hierarchy=unified)
----
endif::[]

.Procedure

. Enable IMA and EVM in the _fix_ mode for the current boot entry and allow users to gather and update the IMA measurements by adding the following kernel command-line parameters:
+
[literal,subs="+quotes"]
....
# *grubby --update-kernel=/boot/vmlinuz-$(uname -r) --args="ima_policy=appraise_tcb ima_appraise=fix evm=fix"*
....
+
The command enables IMA and EVM in the _fix_ mode for the current boot entry to gather and update the IMA measurements.
+
The `ima_policy=appraise_tcb` kernel command-line parameter ensures that the kernel uses the default Trusted Computing Base (TCB) measurement policy and the appraisal step.
The appraisal step forbids access to files whose prior and current measures do not match.

. Reboot to make the changes come into effect.

. Optional: Verify the parameters added to the kernel command line:
+
ifeval::[{ProductNumber} == 8]
[literal,subs="quotes,attributes"]
....
# *cat /proc/cmdline*
BOOT_IMAGE=(hd0,msdos1)/vmlinuz-4.18.0-167.el8.x86_64 root=/dev/mapper/rhel-root ro crashkernel=auto resume=/dev/mapper/rhel-swap rd.lvm.lv=rhel/root rd.lvm.lv=rhel/swap rhgb quiet ima_policy=appraise_tcb ima_appraise=fix evm=fix
....
endif::[]
ifeval::[{ProductNumber} == 9]
[literal,subs="quotes,attributes"]
....
# *cat /proc/cmdline*
BOOT_IMAGE=(hd0,msdos1)/vmlinuz-5.14.0-1.el9.x86_64 root=/dev/mapper/rhel-root ro crashkernel=1G-4G:192M,4G-64G:256M,64G-:512M resume=/dev/mapper/rhel-swap rd.lvm.lv=rhel/root rd.lvm.lv=rhel/swap rhgb quiet ima_policy=appraise_tcb ima_appraise=fix evm=fix
....
endif::[]

. Create a kernel master key to protect the EVM key:
+
[subs="+quotes"]
....
# *keyctl add user kmk "$(dd if=/dev/urandom bs=1 count=32 2> /dev/null)" @u*
748544121
....
+
The `kmk` is kept entirely in the kernel space memory. The 32-byte long value of the `kmk` is generated from random bytes from the `/dev/urandom` file and placed in the user (`@u`) keyring. The key serial number is on the first line of the previous output.

. Create an encrypted EVM key based on the `kmk`:
+
[subs="+quotes"]
....
# *keyctl add encrypted evm-key "new user:kmk 64" @u*
641780271
....
+
The command uses the `kmk` to generate and encrypt a 64-byte long user key (named `evm-key`) and places it in the user (`@u`) keyring. The key serial number is on the first line of the previous output.
+
[IMPORTANT]
====

It is necessary to name the user key as *evm-key* because that is the name the EVM subsystem is expecting and is working with.

====

. Create a directory for exported keys.
+
[subs="+quotes"]
....
# *mkdir -p /etc/keys/*
....

. Search for the `kmk` and export its unencrypted value into the new directory.
+
[subs="+quotes"]
....
# *keyctl pipe $(keyctl search @u user kmk) > /etc/keys/kmk*
....

. Search for the `evm-key` and export its encrypted value into the new directory.
+
[subs="+quotes"]
....
# *keyctl pipe $(keyctl search @u encrypted evm-key) > /etc/keys/evm-key*
....
+
The `evm-key` has been encrypted by the kernel master key earlier.

. Optional: View the newly created keys:
+
[subs="+quotes"]
....
# *keyctl show*
Session Keyring
974575405   --alswrv     0        0      keyring: _ses
299489774   --alswrv     0    65534       \_ keyring: _uid.0
748544121   --alswrv     0        0           \_ user: kmk
641780271   --alswrv     0        0           \_ encrypted: evm-key

# *ls -l /etc/keys/*
total 8
-rw-r--r--. 1 root root 246 Jun 24 12:44 evm-key
-rw-r--r--. 1 root root  32 Jun 24 12:43 kmk
....

. Optional: If the keys are removed from the keyring, for example after system reboot,
you can import the already exported `kmk` and `evm-key` instead of creating new ones.
+
.. Import the `kmk`.
+
[literal,subs="+quotes,verbatim,normal"]
....
# *keyctl add user kmk "$(cat /etc/keys/kmk)" @u*
451342217
....
+
.. Import the `evm-key`.
+
[literal,subs="+quotes,verbatim,normal"]
....
# *keyctl add encrypted evm-key "load $(cat /etc/keys/evm-key)" @u*
924537557
....

. Activate EVM.
+
[subs="+quotes"]
....
# *echo 1 > /sys/kernel/security/evm*
....

. Relabel the whole system.
+
[subs="+quotes"]
....
# *find / -fstype xfs -type f -uid 0 -exec head -n 1 '{}' >/dev/null \;*
....
+
[WARNING]
====
Enabling IMA and EVM without relabeling the system might make the majority of the files on the system inaccessible.
====


.Verification

* Verify that EVM has been initialized:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *dmesg | tail -1*
[...] evm: key initialized
....
