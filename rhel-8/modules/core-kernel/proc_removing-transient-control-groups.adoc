:_mod-docs-content-type: PROCEDURE
:experimental:
[id="removing-transient-control-groups_{context}"]
= Removing transient control groups

[role="_abstract"]
You can use the `systemd` system and service manager to remove transient control groups (`cgroups`) if you no longer need to limit, prioritize, or control access to hardware resources for groups of processes.

Transient `cgroups` are automatically released when all the processes that a service or a scope unit contains finish.


.Procedure

* To stop the service unit with all its processes, enter:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl stop <pass:quotes[_name_]>.service*
....

* To terminate one or more of the unit processes, enter:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl kill <pass:quotes[_name_]>.service --kill-who=_PID,..._ --signal=<pass:quotes[_signal_]>*
....
+
The command uses the `--kill-who` option to select process(es) from the control group you want to terminate.
To kill multiple processes at the same time, pass a comma-separated list of PIDs.
The `--signal` option determines the type of POSIX signal to be sent to the specified processes. The default signal is _SIGTERM_.


[role="_additional-resources"]
.Additional resources
* xref:understanding-control-groups_setting-limits-for-applications[What are control groups]
* xref:what-kernel-resource-controllers-are_setting-limits-for-applications[What are kernel resource controllers]
* `systemd.resource-control(5)` and `cgroups(7)` man pages on your system

ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#role-of-systemd-in-control-groups-version-1_using-control-groups-version-1-with-systemd[Role of systemd in control groups version 1]
endif::[]
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/managing_monitoring_and_updating_the_kernel/index#setting-limits-for-applications_managing-monitoring-and-updating-the-kernel[Understanding control groups]
endif::[]
* [citetitle]_link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/managing-systemd_configuring-basic-system-settings[Managing systemd]_ in {ProductShortName}
