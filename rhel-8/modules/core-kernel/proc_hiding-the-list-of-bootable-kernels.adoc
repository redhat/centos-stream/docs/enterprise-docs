// Module included in the following assemblies:
//
// assembly_building-a-customized-boot-menu


[id="proc_hiding-the-list-of-bootable-kernels_{context}"]
= Hiding the list of bootable kernels
////
Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.
////

[role="_abstract"]
You can prevent GRUB from displaying the list of bootable kernels when the system starts up.

.Procedure

. Set the [option]`GRUB_TIMEOUT_STYLE` option in the `/etc/default/grub` file as follows:
+
----
GRUB_TIMEOUT_STYLE=hidden
----

. Rebuild the [filename]`grub.cfg` file for the changes to take effect.
+
* On BIOS-based machines, enter:
+
[subs="quotes"]
....
# *grub2-mkconfig -o /boot/grub2/grub.cfg*
....
+
* On UEFI-based machines, enter:
ifeval::[{ProductNumber} == 9]
+
[subs="quotes"]
....
# *grub2-mkconfig -o /boot/grub2/grub.cfg*
....
endif::[]
ifeval::[{ProductNumber} == 8]
+
[subs="quotes"]
....
# *grub2-mkconfig -o /boot/efi/EFI/redhat/grub.cfg*
....
endif::[]

. Press the kbd:[Esc] key to display the list of bootable kernels when booting.


[IMPORTANT]
====
Do not set [option]`GRUB_TIMEOUT` to _0_ in the `/etc/default/grub` file to hide the list of bootable kernels.
With such a setting, the system always boots immediately on the default menu entry, and if the default kernel fails to boot,
it is not possible to boot any previous kernel.
====
