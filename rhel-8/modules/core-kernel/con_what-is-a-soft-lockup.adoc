:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/con_what-is-a-soft-lockup.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.
[id="what-is-a-soft-lockup_{context}"]
= What is a soft lockup

[role="_abstract"]
A soft lockup is a situation usually caused by a bug, when a task is executing in kernel space on a CPU without rescheduling. The task also does not allow any other task to execute on that particular CPU. As a result, a warning is displayed to a user through the system console. This problem is also referred to as the soft lockup firing.


[role="_additional-resources"]
.Additional resources
* [citetitle]_link:++https://access.redhat.com/articles/371803++[What is a CPU soft lockup?]_
