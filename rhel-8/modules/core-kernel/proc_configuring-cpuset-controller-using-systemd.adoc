:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// assembly_configuring-resource-management-using-systemd

[id="proc_configuring-cpuset-controller-using-systemd_{context}"]
= Configuring CPUSET controller by using systemd

[role="_abstract"]
With the `systemd` resource management API, you can configure limits on a set of CPUs and NUMA nodes that a service can use.
This limit restricts access to system resources used by the processes. The requested configuration is written in the `cpuset.cpus` and `cpuset.mems` files.

However, the requested configuration might not be used, as the parent `cgroup` limits either `cpus` or `mems`.
To access the current configuration, the `cpuset.cpus.effective` and `cpuset.mems.effective` files are exported to the users.

.Procedure

* To set `AllowedCPUs`:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl set-property <pass:quotes[_service name_]>.service AllowedCPUs=<pass:quotes[_value_]>*
....
+
For example:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl set-property <pass:quotes[_service name_]>.service AllowedCPUs=0-5*
....

* To set `AllowedMemoryNodes`:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl set-property <pass:quotes[_service name_]>.service AllowedMemoryNodes=<pass:quotes[_value_]>*
....
+
For example:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl set-property <pass:quotes[_service name_]>.service AllowedMemoryNodes=0*
....
