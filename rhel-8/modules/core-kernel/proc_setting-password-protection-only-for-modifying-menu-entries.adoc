:_mod-docs-content-type: PROCEDURE

[id="proc_setting-password-protection-only-for-modifying-menu-entries_{context}"]
= Setting password protection only for modifying menu entries

You can configure GRUB to support password authentication for modifying GRUB menu entries.
This procedure creates a `/boot/grub2/user.cfg` file that provides the password in the hash format.

[IMPORTANT]
====
Setting a password using the `grub2-setpassword` command prevents menu entries from unauthorized modification but not from unauthorized booting.
====

.Procedure

. Issue the `grub2-setpassword` command as root:
+
[subs="quotes"]
....
# *grub2-setpassword*
....

. Enter the password for the user and press the kbd:[Enter] key to confirm the password:
+
[subs="quotes"]
....
Enter password:
Confirm the password:
....


[NOTE]
====
The root user is defined in the `/boot/grub2/grub.cfg` file with the password changes.
Therefore, modifying a boot entry during booting requires the name and password of the root user.
====
