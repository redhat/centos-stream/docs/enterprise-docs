:_mod-docs-content-type: REFERENCE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_booting-process.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
[id="booting-process_{context}"]
= Booting process

[role="_abstract"]
== How to install and boot custom kernels in RHEL

The Boot Loader Specification (BLS) defines a scheme and file format to manage boot loader configurations for each boot option in a drop-in directory. There is no need to manipulate the individual drop-in configuration files.
This premise is particularly relevant in {ProductShortName} {ProductNumber} because not all architectures use the same boot loader:

* `x86_64`, `aarch64` and `ppc64le` with open firmware use `GRUB`.
* `ppc64le` with Open Power Abstraction Layer (OPAL) uses `Petitboot`.
* `s390x` uses `zipl`.

Each boot loader has a different configuration file and format that must be modified when a new kernel is installed or removed. In the previous versions of {ProductShortName}, the `grubby` utility permitted this work. However, for {ProductShortName}{nbsp}8, the boot loader configuration was standardized by implementing the BLS file format, where `grubby` works as a thin wrapper around the BLS operations.


== Early kdump support in RHEL

Previously, the `kdump` service started too late to register the kernel crashes that occurred in early stages of the booting process. As a result, the crash information together with a chance for troubleshooting was lost.

To address this problem, {ProductShortName}{nbsp}8 introduced an `early kdump` support. For more details about `early kdump`, see the `/usr/share/doc/kexec-tools/early-kdump-howto.txt` file. Also, see the Red{nbsp}Hat Knowledgebase solution link:https://access.redhat.com/solutions/3700611[What is early kdump support and how do I configure it?].
