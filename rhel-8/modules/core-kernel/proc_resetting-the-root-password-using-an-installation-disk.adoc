// Module included in the following assemblies:
//
// rhel-8/assemblies/assembly_editing-grub-2-during-boot


[id="proc_resetting-the-root-password-using-an-installation-disk_{context}"]
= Resetting the root password using an installation disk

[role="_abstract"]
In case you forget or lose the `root` password, you can reset it.

.Procedure

. Boot the host from an installation source.

. In the boot menu for the installation media, select the `Troubleshooting` option.
+
image::sosreport-rescue-8-1.png[RHEL Anaconda Installer screen with the Troubleshooting option selected]

. In the Troubleshooting menu, select the `Rescue a Red Hat Enterprise Linux system` option.
+
image::sosreport-rescue-8-2.png[Troubleshooting screen with the Rescue option selected]

. At the Rescue menu, select `1` and press the kbd:[Enter] key to continue.
+
image::sosreport-rescue-8-3.png[Rescue screen prompting you to continue and mount the target host under /mnt/sysimage]

. Change the file system `root` as follows:
+
[literal,subs="+quotes,verbatim"]
....
sh-4.4# chroot /mnt/sysimage
....
+
image::change-the-file-system-root.png[Change the file system root]

. Enter the [command]`passwd` command and follow the instructions displayed on the command line to change the `root` password.
+
image::resetting-the-root-password.png[Resetting the Root Password]

. Remove the `autorelable` file to prevent a time consuming SELinux relabel of the disk:
+
[literal,subs="+quotes,verbatim"]
....
sh-4.4# rm -f /.autorelabel
....

. Enter the [command]`exit` command to exit the [command]`chroot` environment.

. Enter the [command]`exit` command again to resume the initialization and finish the system boot.
