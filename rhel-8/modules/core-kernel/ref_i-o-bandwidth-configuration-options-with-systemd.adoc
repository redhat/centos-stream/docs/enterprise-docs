:_mod-docs-content-type: REFERENCE
:experimental:
// Module included in the following assemblies:
//
// assembly_understanding-resource-management-with-systemd.adoc

[id="ref_i-o-bandwidth-configuration-options-with-systemd_{context}"]
= I/O bandwidth configuration options for systemd


[role="_abstract"]
To manage the block layer I/O policies by using `systemd`, the following configuration options are available:


`IOWeight`:: Sets the default I/O weight. The weight value is used as a basis for the calculation of how much of the real I/O bandwidth the service receives in relation to the other services.

`IODeviceWeight`:: Sets the I/O weight for a specific block device.
+
[NOTE]
====
Weight-based options are supported only if the block device is using the CFQ I/O scheduler.
No option is supported if the device uses the Multi-Queue Block I/O queuing mechanism.
====

For example, `IODeviceWeight=/dev/disk/by-id/dm-name-rhel-root 200`.

`IOReadBandwidthMax`, `IOWriteBandwidthMax`:: Sets the absolute bandwidth per device or a mount point.
+
For example, `IOWriteBandwith=/var/log 5M`.
+
[NOTE]
====
`systemd` handles the file-system-to-device translation automatically.

`IOReadIOPSMax`, `IOWriteIOPSMax`:: Sets the absolute bandwidth in Input/Output Operations Per Second (IOPS).
====
