:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="displaying-information-about-kernel-modules_{context}"]
= Displaying information about kernel modules

[role="_abstract"]
Use the [command]`modinfo` command to display some detailed information about the specified kernel module. 

.Prerequisites

* The [package]`kmod` package is installed.

.Procedure

* To display information about any kernel module, enter:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *modinfo <pass:quotes[_KERNEL_MODULE_NAME_]>*
....
+
For example:
+
ifeval::[{ProductNumber} == 8]
[literal,subs="+quotes,verbatim,normal"]
....
$ *modinfo virtio_net*

filename:       /lib/modules/4.18.0-94.el8.x86_64/kernel/drivers/net/virtio_net.ko.xz
license:        GPL
description:    Virtio network driver
rhelversion:    8.1
srcversion:     2E9345B281A898A91319773
alias:          virtio:d00000001v*
depends:        net_failover
intree:         Y
name:           virtio_net
vermagic:       4.18.0-94.el8.x86_64 SMP mod_unload modversions
...
parm:           napi_weight:int
parm:           csum:bool
parm:           gso:bool
parm:           napi_tx:bool
....
endif::[]
ifeval::[{ProductNumber} == 9]
[literal,subs="+quotes,verbatim,normal"]
....
$ *modinfo virtio_net*

filename:       /lib/modules/5.14.0-1.el9.x86_64/kernel/drivers/net/virtio_net.ko.xz
license:        GPL
description:    Virtio network driver
rhelversion:    9.0
srcversion:     8809CDDBE7202A1B00B9F1C
alias:          virtio:d00000001v*
depends:        net_failover
retpoline:      Y
intree:         Y
name:           virtio_net
vermagic:       5.14.0-1.el9.x86_64 SMP mod_unload modversions
...
parm:           napi_weight:int
parm:           csum:bool
parm:           gso:bool
parm:           napi_tx:bool
....
endif::[]
+
You can query information about all available modules, regardless of whether they are loaded. The `parm` entries show parameters the user is able to set for the module, and what type of value they expect.
+
[NOTE]
====
When entering the name of a kernel module, do not append the `.ko.xz` extension to the end of the name. Kernel module names do not have extensions; their corresponding files do.
====


[role="_additional-resources"]
.Additional resources
* The `modinfo(8)` manual page
