:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_enabling-the-kdump-service.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="enabling-and-disabling-the-kdump-service_{context}"]
= Enabling and disabling the kdump service

[role="_abstract"]
You can configure to enable or disable the `kdump` functionality on a specific kernel or on all installed kernels. You must routinely test the `kdump` functionality and validate its operates correctly.

.Prerequisites

* You have root permissions on the system.
* You have completed `kdump` requirements for configurations and targets. See link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_monitoring_and_updating_the_kernel/supported-kdump-configurations-and-targets_managing-monitoring-and-updating-the-kernel[Supported kdump configurations and targets].

* All configurations for installing `kdump` are set up as required.

.Procedure

. Enable the `kdump` service for `multi-user.target`:
+
[subs="+quotes,attributes"]
----
# systemctl enable kdump.service
----

. Start the service in the current session:
+
[subs="+quotes,attributes"]
----
# systemctl start kdump.service
----

. Stop the `kdump` service:
+
[subs="+quotes,attributes"]
----
# systemctl stop kdump.service
----

. Disable the `kdump` service:
+
[subs="+quotes,attributes"]
----
# systemctl disable kdump.service
----

[WARNING]
====
It is recommended to set `kptr_restrict=1` as default. When `kptr_restrict` is set to (1) as default, the `kdumpctl` service loads the crash kernel regardless of whether the Kernel Address Space Layout (`KASLR`) is enabled.

If `kptr_restrict` is not set to `1` and KASLR is enabled, the contents of `/proc/kore` file are generated as all zeros. The `kdumpctl` service fails to access the `/proc/kcore` file and load the crash kernel. The `kexec-kdump-howto.txt` file displays a warning message, which recommends you to set `kptr_restrict=1`. Verify for the following in the `sysctl.conf` file to ensure that `kdumpctl` service loads the crash kernel:

* Kernel `kptr_restrict=1` in the `sysctl.conf` file.
====
