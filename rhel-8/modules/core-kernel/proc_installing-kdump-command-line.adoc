:_mod-docs-content-type: PROCEDURE
:experimental:

[id="installing-kdump-command-line{context}"]

= Installing kdump on the command line

[role="_abstract"]
Installation options such as custom *Kickstart* installations, in some cases does *not* install or enable `kdump` by default. The following procedure helps you enable `kdump` in this case.

.Prerequisites

* An active {ProductShortName} subscription.
* A repository containing the [package]`kexec-tools` package for your system CPU architecture. 
* Fulfilled requirements for `kdump` configurations and targets.
For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#supported-kdump-configurations-and-targets_managing-monitoring-and-updating-the-kernel[Supported kdump configurations and targets].

.Procedure

. Check if `kdump` is installed on your system:
+
[subs="quotes, macros"]
....
# rpm -q kexec-tools
....
+
Output if the package is installed:
+
ifeval::[{ProductNumber} == 8]
[subs="quotes"]
....
kexec-tools-2.0.17-11.el8.x86_64
....
endif::[]
ifeval::[{ProductNumber} == 9]
[subs="quotes"]
....
# kexec-tools-2.0.22-13.el9.x86_64
....
endif::[]
+
Output if the package is not installed:
+
[subs="quotes, macros"]
....
package kexec-tools is not installed
....

. Install `kdump` and other necessary packages:
+
[subs="quotes"]
....
# dnf install kexec-tools
....

ifeval::[{ProductNumber} == 8]
[IMPORTANT]
====

From `kernel-3.10.0-693.el7` onwards, the `Intel IOMMU` driver is supported for `kdump`. For `kernel-3.10.0-514[.XYZ].el7` and early versions, you must ensure that `Intel IOMMU` is disabled to prevent an unresponsive capture kernel. 
====
endif::[]
