:_mod-docs-content-type: PROCEDURE
:experimental:
[id="configuring-memory-resource-control-settings-with-unit-files_{context}"]
= Configuring memory resource control settings with unit files

[role="_abstract"]
Each persistent unit is supervised by the `systemd` system and service manager, and has a unit configuration file in the `/usr/lib/systemd/system/` directory. To change the resource control settings of the persistent units, modify its unit configuration file either manually in a text editor or from the command line.

Manually modifying unit files is one of the ways how to set limits, prioritize, or control access to hardware resources for groups of processes.


.Procedure

. To limit the memory usage of a service, modify the `/usr/lib/systemd/system/example.service` file as follows:
+
[literal,subs="+quotes,verbatim,normal"]
....
...
[Service]
MemoryMax=1500K
...
....
+
This configuration places a limit on maximum memory consumption of processes executed in a control group, which `example.service` is a part of.
+
[NOTE]
====
Use suffixes K, M, G, or T to identify Kilobyte, Megabyte, Gigabyte, or Terabyte as a unit of measurement.
====

. Reload all unit configuration files:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl daemon-reload*
....

. Restart the service:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl restart example.service*
....

. Reboot the system.

.Verification

. Check that the changes took effect:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *cat /sys/fs/cgroup/memory/system.slice/example.service/memory.limit_in_bytes*
1536000
....
+
The memory consumption was limited to approximately 1,500 KB.
+
[NOTE]
====
The `memory.limit_in_bytes` file stores the memory limit as a multiple of 4096 bytes - one kernel page size specific for AMD64 and Intel 64. The actual number of bytes depends on a CPU architecture.
====


[role="_additional-resources"]
.Additional resources
* `systemd.resource-control(5)`, `cgroups(7)` manual pages
* [citetitle]_link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/managing-systemd_configuring-basic-system-settings#managing-system-services-with-systemctl_managing-systemd[Managing system services with systemctl]_ in {ProductShortName}
ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_monitoring_and_updating_the_kernel/assembly_using-cgroupfs-to-manually-manage-cgroups_managing-monitoring-and-updating-the-kernel#setting-cpu-limits-to-applications-using-cgroups-v1_assembly_using-cgroupfs-to-manually-manage-cgroups
endif::[]
