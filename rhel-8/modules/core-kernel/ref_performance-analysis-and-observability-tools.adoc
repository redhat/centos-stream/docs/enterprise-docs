:_mod-docs-content-type: REFERENCE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_performance-analysis-and-observability-tools.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
[id="performance-analysis-and-observability-tools_{context}"]
= Performance analysis and observability tools

[role="_abstract"]
== `bpftool` in kernel

You can use the `bpftool` utility for inspection and simple manipulation of programs and maps based on extended Berkeley Packet Filtering (eBPF). `bpftool` is a part of the kernel source tree and is provided by the *bpftool* package that is included as a sub-package of the *kernel* package.

== eBPF available as a Technology Preview

The *extended Berkeley Packet Filtering (eBPF)* feature is available as a Technology Preview for both networking and tracing. *eBPF* enables the user space to attach custom programs onto a variety of points (sockets, trace points, packet reception) to receive and process data. The feature includes a new system call `bpf()`, which supports creating various types of maps, and also to insert various types of programs into the kernel. Note that the `bpf()` syscall can be successfully used only by a user with the `CAP_SYS_ADMIN` capability, such as a root user. See the `bpf`(2) man page for more information.

== BCC is available as a Technology Preview

`BPF Compiler Collection (BCC)` is a user space toolkit for creating efficient kernel tracing and manipulation programs that is available as a Technology Preview in {ProductShortName} 8. `BCC` provides tools for I/O analysis, networking, and monitoring of Linux operating systems by using the `extended Berkeley Packet Filtering (eBPF)`.
