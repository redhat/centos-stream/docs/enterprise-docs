:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="unloading-kernel-modules-at-system-runtime_{context}"]
= Unloading kernel modules at system runtime

[role="_abstract"]
To unload certain kernel modules from the running kernel, use the [command]`modprobe` command to find and unload a kernel module at system runtime from the currently loaded kernel.

[WARNING]
====
You must not unload the kernel modules that are used by the running system because it can lead to an unstable or non-operational system.
====

[IMPORTANT]
====
After finishing the unloading of inactive kernel modules, the modules that are defined to be automatically loaded on boot, will not remain unloaded after rebooting the system. For information about how to prevent this outcome, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#preventing-kernel-modules-from-being-automatically-loaded-at-system-boot-time_managing-kernel-modules[Preventing kernel modules from being automatically loaded at system boot time].
====

.Prerequisites

* You have root permissions.
* The [package]`kmod` package is installed.

.Procedure

. List all the loaded kernel modules:
+
[subs="+quotes"]
----
# *lsmod*
----

. Select the kernel module you want to unload.
+
If a kernel module has dependencies, unload those prior to unloading the kernel module. For details on identifying modules with dependencies, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#listing-currently-loaded-kernel-modules_managing-kernel-modules[Listing currently loaded kernel modules] and link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#kernel-module-dependencies_managing-kernel-modules[Kernel module dependencies].

. Unload the relevant kernel module:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *modprobe -r <pass:quotes[_MODULE_NAME_]>*
....
+
When entering the name of a kernel module, do not append the `.ko.xz` extension to the end of the name. Kernel module names do not have extensions; their corresponding files do.

.Verification

* Optionally, verify the relevant module was unloaded:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *lsmod | grep <pass:quotes[_MODULE_NAME_]>*
....
+
If the module is unloaded successfully, this command does not display any output.

[role="_additional-resources"]
.Additional resources
* `modprobe(8)` manual page
