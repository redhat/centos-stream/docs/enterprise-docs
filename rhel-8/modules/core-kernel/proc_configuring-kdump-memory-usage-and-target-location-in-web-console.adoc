:_mod-docs-content-type: PROCEDURE
:experimental:

[id="configuring-kdump-memory-usage-and-target-location-in-web-console_{context}"]
= Configuring kdump memory usage and target location in web console

[role="_abstract"]
You can configure the memory reserve for the `kdump` kernel and also specify the target location to capture the `vmcore` dump file with the {ProductShortName} web console interface.

.Prerequisites

* The web console must be installed and accessible. For details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/managing_systems_using_the_rhel_{ProductNumber}_web_console/getting-started-with-the-rhel-{ProductNumber}-web-console_system-management-using-the-rhel-{ProductNumber}-web-console#installing-the-web-console_getting-started-with-the-rhel-{ProductNumber}-web-console[Installing the web console].

.Procedure

. In the web console, open the [button]*Kernel dump* tab and start the `kdump` service by setting the *Kernel crash dump* switch to on.
. Configure the `kdump` memory usage in the terminal, for example:
+
[subs="+quotes"]
----
$ *sudo grubby --update-kernel ALL --args crashkernel=512M*
----
+
Restart the system to apply the changes.

. In the *Kernel dump* tab, click *Edit* at the end of the *Crash dump location* field.
+

. Specify the target directory for saving the `vmcore` dump file:
+

* For a local filesystem, select [option]*Local Filesystem* from the drop-down menu.
+
* For a remote system by using the SSH protocol, select [option]*Remote over SSH* from the drop-down menu and specify the following fields:

** In the *Server* field, enter the remote server address.
** In the *SSH key* field, enter the SSH key location.
** In the *Directory* field, enter the target directory.
+
* For a remote system by using the NFS protocol, select [option]*Remote over NFS* from the drop-down menu and specify the following fields:

** In the *Server* field, enter the remote server address.
** In the *Export* field, enter the location of the shared folder of an NFS server.
** In the *Directory* field, enter the target directory.
+
[NOTE]
====
You can reduce the size of the `vmcore` file by selecting the *Compression* checkbox.
====

. Optional: Display the automation script by clicking *View automation script*.
+
A window with the generated script opens. You can browse a shell script and an Ansible playbook generation options tab.

. Optional: Copy the script by clicking *Copy to clipboard*.
+
You can use this script to apply the same configuration on multiple machines.

.Verification

. Click btn:[Test configuration].

. Click *Crash system* under *Test kdump settings*.
+
[WARNING]
====
When you start the system crash, the kernel operation stops and results in a system crash with data loss.
====


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_monitoring_and_updating_the_kernel/supported-kdump-configurations-and-targets_managing-monitoring-and-updating-the-kernel#supported-kdump-targets_supported-kdump-configurations-and-targets[Supported kdump targets]
