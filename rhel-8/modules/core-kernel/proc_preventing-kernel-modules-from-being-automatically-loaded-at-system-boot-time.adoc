:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_managing-kernel-modules.adoc

[id="preventing-kernel-modules-from-being-automatically-loaded-at-system-boot-time_{context}"]
= Preventing kernel modules from being automatically loaded at system boot time

You can prevent the system from loading a kernel module automatically during the boot process by listing the module in `modprobe` configuration file with a corresponding command.

.Prerequisites

* The commands in this procedure require root privileges. Either use `su -` to switch to the root user or preface the commands with `sudo`.
* The `kmod` package is installed.
* Ensure that your current system configuration does not require a kernel module you plan to deny.

.Procedure

. List modules loaded to the currently running kernel by using the `lsmod` command:
+
[subs="quotes"]
----
$ *lsmod*
Module                  Size  Used by
tls                   131072  0
uinput                 20480  1
snd_seq_dummy          16384  0
snd_hrtimer            16384  1
…
----
+
In the output, identify the module you want to prevent from getting loaded.
+
* Alternatively, identify an unloaded kernel module you want to prevent from potentially loading in the `/lib/modules/_&lt;KERNEL-VERSION&gt;_/kernel/_&lt;SUBSYSTEM&gt;_/` directory, for example:
+
[subs="quotes"]
----
$ *ls /lib/modules/4.18.0-477.20.1.el8_8.x86_64/kernel/crypto/*
ansi_cprng.ko.xz        chacha20poly1305.ko.xz  md4.ko.xz               serpent_generic.ko.xz
anubis.ko.xz            cmac.ko.xz…
----

. Create a configuration file serving as a denylist:
+
[subs="quotes"]
----
# *touch /etc/modprobe.d/denylist.conf*
----

. In a text editor of your choice, combine the names of modules you want to exclude from automatic loading to the kernel with the `blacklist` configuration command, for example:
+
[subs="quotes"]
----
# Prevents _&lt;KERNEL-MODULE-1&gt;_ from being loaded
blacklist _&lt;MODULE-NAME-1&gt;_
install _&lt;MODULE-NAME-1&gt;_ /bin/false

# Prevents _&lt;KERNEL-MODULE-2&gt;_ from being loaded
blacklist _&lt;MODULE-NAME-2&gt;_
install _&lt;MODULE-NAME-2&gt;_ /bin/false
…
----
+
Because the `blacklist` command does not prevent the module from getting loaded as a dependency for another kernel module that is not in a denylist, you must also define the `install` line. In this case, the system runs `/bin/false` instead of installing the module. The lines starting with a hash sign are comments you can use to make the file more readable.
+
[NOTE]
====
When entering the name of a kernel module, do not append the `.ko.xz` extension to the end of the name. Kernel module names do not have extensions; their corresponding files do.
====

. Create a backup copy of the current initial RAM disk image before rebuilding:
+
[subs="quotes"]
----
# *cp /boot/initramfs-$(uname -r).img /boot/initramfs-$(uname -r).bak.$(date +%m-%d-%H%M%S).img*
----
+
* Alternatively, create a backup copy of an initial RAM disk image which corresponds to the kernel version for which you want to prevent kernel modules from automatic loading:
+
[subs="quotes"]
----
# *cp /boot/initramfs-_&lt;VERSION&gt;_.img /boot/initramfs-_&lt;VERSION&gt;_.img.bak.$(date +%m-%d-%H%M%S)*
----

. Generate a new initial RAM disk image to apply the changes:
+
[subs="quotes"]
----
# *dracut -f -v*
----
+
* If you build an initial RAM disk image for a different kernel version than your system currently uses, specify both target `initramfs` and kernel version:
+
[subs="quotes"]
----
# *dracut -f -v /boot/initramfs-_&lt;TARGET-VERSION&gt;_.img _&lt;CORRESPONDING-TARGET-KERNEL-VERSION&gt;_*
----

. Restart the system:
+
[subs="quotes"]
----
$ *reboot*
----


[IMPORTANT]
====
The changes described in this procedure *will take effect and persist* after rebooting the system. If you incorrectly list a key kernel module in the denylist, you can switch the system to an unstable or non-operational state.
====


[role="_additional-resources"]
.Additional resources
* link:https://access.redhat.com/solutions/41278[How do I prevent a kernel module from loading automatically?] (Red Hat Knowledgebase)
* `modprobe.d(5)` and `dracut(8)` man pages on your system
