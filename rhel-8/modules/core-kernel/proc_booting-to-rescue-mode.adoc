// Module included in the following assemblies:
//
// assembly_making-temporary-changes-to-a-grub-2-menu


[id="proc_booting-to-rescue-mode_{context}"]
= Booting to rescue mode

[role="_abstract"]
Rescue mode provides a convenient single-user environment in which you can repair your system in situations when it is unable to complete a normal booting process. In rescue mode, the system attempts to mount all local file systems and start some important system services. However, it does not activate network interfaces or allow more users to be logged into the system at the same time.

.Procedure

. On the GRUB boot screen, press the kbd:[e] key for edit.

. Add the following parameter at the end of the `linux` line:
+
....
systemd.unit=rescue.target
....
+
image::grub2-booting-to-rescue-mode-1.png[Booting to Rescue Mode]

. Press kbd:[Ctrl + x] to boot to rescue mode.
+
image::grub2-booting-to-rescue-mode-2.png[Booting to Rescue Mode]
