:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// assembly_applying-kernel-patches-without-restarting-the-system.adoc

// This module can be included from assemblies using the following include statement:
// include::<path>/con_access-to-kernel-patches.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: con_my-concept-module-a.adoc
// * ID: [id='con_my-concept-module-a_{context}']
// * Title: = My concept module A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
// Do not start the title with a verb. See also _Wording of headings_
// in _The IBM Style Guide_.

[id="access-to-kernel-live-patches_{context}"]
= Access to kernel live patches

[role="_abstract"]
A kernel module (`kmod`) implements kernel live patching capability and is provided as an RPM package.

ifeval::[{ProductNumber} == 8]
All customers have access to kernel live patches, which are delivered through the usual channels. However, customers who do not subscribe to an extended support offering will lose access to new patches for the current minor release once the next minor release becomes available. For example, customers with standard subscriptions will only be able to live patch RHEL 8.2 kernel until the RHEL 8.3 kernel is released.
endif::[]

ifeval::[{ProductNumber} == 9]
All customers have access to kernel live patches, which are delivered through the usual channels. However, customers who do not subscribe to an extended support offering will lose access to new patches for the current minor release once the next minor release becomes available. For example, customers with standard subscriptions will only be able to live patch RHEL 9.1 kernel until the RHEL 9.2 kernel is released.
endif::[]

The components of kernel live patching are as follows:

Kernel patch module:: {blank}
+
** The delivery mechanism for kernel live patches.
+
** A kernel module built specifically for the kernel being patched.
+
** The patch module contains the code of the required fixes for the kernel.
+
** Patch modules register with the `livepatch` kernel subsystem and specify the original functions to replace, along with pointers to the replacement functions. Kernel patch modules are delivered as RPMs.
+
** The naming convention is `kpatch_<kernel version>_<kpatch version>_<kpatch release>`. The "kernel version" part of the name has _dots_ replaced with _underscores_.

The `kpatch` utility:: A command-line utility for managing patch modules.

The `kpatch` service:: A `systemd` service required by `multiuser.target`. This target loads the kernel patch module at boot time.

The `kpatch-dnf` package:: A DNF plugin delivered in the form of an RPM package. This plugin manages automatic subscription to kernel live patches.
