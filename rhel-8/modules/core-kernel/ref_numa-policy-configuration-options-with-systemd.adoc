:_mod-docs-content-type: REFERENCE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="ref_numa-policy-configuration-options-with-systemd_{context}"]
= NUMA policy configuration options for systemd


[role="_abstract"]
`Systemd` provides the following options to configure the NUMA policy:


`NUMAPolicy`:: Controls the NUMA memory policy of the executed processes. You can use these policy types:

* default
* preferred
* bind
* interleave
* local


`NUMAMask`:: Controls the NUMA node list that is associated with the selected NUMA policy.
+
Note that you do not have to specify the `NUMAMask` option for the following policies:

* default
* local

+
For the preferred policy, the list specifies only a single NUMA node.


[role="_additional-resources"]
.Additional resources
* `systemd.resource-control(5)`, `systemd.exec(5)`, and `set_mempolicy(2)` man pages on your system

ifeval::[{ProductNumber} == 8]
* xref:proc_configuring-numa-using-systemd_assembly_configuring-cpu-affinity-and-numa-policies-using-systemd[NUMA policy configuration options for systemd]
endif::[]
