:_mod-docs-content-type: PROCEDURE
:experimental:

// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="preventing-kernel-drivers-from-loading-for-kdump_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Preventing kernel drivers from loading for kdump
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.


[role="_abstract"]
You can control the capture kernel from loading certain kernel drivers by adding the `KDUMP_COMMANDLINE_APPEND=` variable  in the `/etc/sysconfig/kdump` configuration file. By using this method, you can prevent the `kdump` initial RAM disk image `initramfs` from loading the specified kernel module. This helps to prevent the out-of-memory (OOM) killer errors or other crash kernel failures.

You can append the `KDUMP_COMMANDLINE_APPEND=` variable by using one of the following configuration options:

* `rd.driver.blacklist=__<modules>__`
* `modprobe.blacklist=__<modules>__`

.Prerequisites
* You have root permissions on the system.

.Procedure

. Display the list of modules that are loaded to the currently running kernel. Select the kernel module that you intend to block from loading:
+
[subs="+quotes,attributes"]
----
$ *lsmod*

Module                  Size  Used by
fuse                  126976  3
xt_CHECKSUM            16384  1
ipt_MASQUERADE         16384  1
uinput                 20480  1
xt_conntrack           16384  1
----

. Update the `KDUMP_COMMANDLINE_APPEND=` variable in the `/etc/sysconfig/kdump` file. For example:
+
[subs="+quotes,attributes"]
----
KDUMP_COMMANDLINE_APPEND="rd.driver.blacklist=__hv_vmbus,hv_storvsc,hv_utils,hv_netvsc,hid-hyperv__"
----
+
Also, consider the following example by using the `modprobe.blacklist=__<modules>__` configuration option:
+
[subs="+quotes,attributes"]
----
KDUMP_COMMANDLINE_APPEND="modprobe.blacklist=__emcp__ modprobe.blacklist=__bnx2fc__ modprobe.blacklist=__libfcoe__ modprobe.blacklist=__fcoe__"
----
+
. Restart the `kdump` service:
+
[subs="+quotes,attributes"]
----
# systemctl restart kdump
----

[role="_additional-resources"]
.Additional resources
* `dracut.cmdline` man page on your system.
