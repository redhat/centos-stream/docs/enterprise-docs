:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// assembly_configuring-resource-management-using-systemd.adoc

[id="proc_managing-memory-with-systemd_{context}"]
= Allocating memory resources by using systemd

[role="_abstract"]
To allocate memory resources by using `systemd`, use any of the memory configuration options:

* `MemoryMin`
* `MemoryLow`
* `MemoryHigh`
* `MemoryMax`
* `MemorySwapMax`

.Procedure

To set a memory allocation configuration option when using `systemd`:

. Check the assigned values of the memory allocation configuration option in the service of your choice.
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *systemctl show --property <pass:quotes[_memory allocation configuration option_]> <pass:quotes[_service name_]>*
....

. Set the required value of the memory allocation configuration option as a `root`.
+
[literal,subs="+quotes,verbatim,normal"]
....
# *systemctl set-property <pass:quotes[_service name_]> <pass:quotes[_memory allocation configuration option_]>=<pass:quotes[_value_]>*
....


[NOTE]
====
The cgroup properties are applied immediately after they are set. Therefore, the service does not require a restart.
====

.Verification

* To verify whether you have successfully changed the required value of the memory allocation configuration option for your service, enter:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *systemctl show --property <pass:quotes[_memory allocation configuration option_]> <pass:quotes[_service name_]>*
....
