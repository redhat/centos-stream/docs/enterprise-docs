// Module included in the following assemblies:
//
// assembly_aer.adoc

[id="con_what-is-advanced-error-reporting"]
== Overview of AER

[role="_abstract"]
`Advanced Error Reporting` (`AER`) is a kernel feature that provides enhanced error reporting for `Peripheral Component Interconnect Express` (`PCIe`) devices.
The `AER` kernel driver attaches root ports which support `PCIe` `AER` capability in order to:

* Gather the comprehensive error information
* Report errors to the users
* Perform error recovery actions

When `AER` captures an error, it sends an _error_ message to the console. For a repairable error, the console output is a _warning_.

[[ex-An-example-AER-error-output]]
.Example AER output
====
[literal,subs="+quotes"]
....
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0: AER: Corrected error received: id=ae00
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0: AER: Multiple Corrected error received: id=ae00
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0: PCIe Bus Error: severity=Corrected, type=Data Link Layer, id=0000(Receiver ID)
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0:   device [8086:2030] error status/mask=000000c0/00002000
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0:    [ 6] Bad TLP
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0:    [ 7] Bad DLLP
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0: AER: Multiple Corrected error received: id=ae00
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0: PCIe Bus Error: severity=Corrected, type=Data Link Layer, id=0000(Receiver ID)
Feb  5 15:41:33 hostname kernel: pcieport 10003:00:00.0:   device [8086:2030] error status/mask=00000040/00002000
....
====
