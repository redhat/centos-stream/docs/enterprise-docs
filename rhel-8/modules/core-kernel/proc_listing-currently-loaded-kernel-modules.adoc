:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="listing-currently-loaded-kernel-modules_{context}"]
= Listing currently loaded kernel modules

[role="_abstract"]
View the currently loaded kernel modules.

.Prerequisites

* The [package]`kmod` package is installed.

.Procedure

* To list all currently loaded kernel modules, enter:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *lsmod*

Module                  Size  Used by
fuse                  126976  3
uinput                 20480  1
xt_CHECKSUM            16384  1
ipt_MASQUERADE         16384  1
xt_conntrack           16384  1
ipt_REJECT             16384  1
nft_counter            16384  16
nf_nat_tftp            16384  0
nf_conntrack_tftp      16384  1 nf_nat_tftp
tun                    49152  1
bridge                192512  0
stp                    16384  1 bridge
llc                    16384  2 bridge,stp
nf_tables_set          32768  5
nft_fib_inet           16384  1
...
....
+
In the example above:

.. The `Module` column provides the *names* of currently loaded modules.
.. The `Size` column displays the amount of *memory* per module in kilobytes.
.. The `Used by` column shows the number, and optionally the names of modules that are *dependent* on a particular module.


[role="_additional-resources"]
.Additional resources
* The `/usr/share/doc/kmod/README` file
* The `lsmod(8)` manual page
