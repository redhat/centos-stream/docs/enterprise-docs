// Module included in the following assemblies:
//
// rhel-8/assemblies/assembly_making-temporary-changes-to-a-grub-2-menu.adoc


[id="proc_resetting-the-root-password-using-rd-break_{context}"]
= Resetting the root password using rd.break

[role="_abstract"]
In case you forget or lose the `root` password, you can reset it.

.Procedure

. Start the system and, on the GRUB boot screen, press the kbd:[e] key for edit.

. Add the [parameter]`rd.break` parameter at the end of the `linux` line:
+
image::resetting-the-root-password-rd-break.png[Resetting the Root Password]

. Press kbd:[Ctrl + x] to boot the system with the changed parameters.
+
image::resetting-the-root-password-initramfs.png[Resetting the Root Password]

. Remount the file system as writable.
+
[literal,subs="+quotes,verbatim"]
....
switch_root:/# *mount -o remount,rw /sysroot*
....

. Change the file system's `root`.
+
[literal,subs="+quotes,verbatim"]
....
switch_root:/# *chroot /sysroot*
....

. Enter the [command]`passwd` command and follow the instructions displayed on the command line.
+
image::resetting-the-root-password.png[Resetting the Root Password]

. Relabel all files on the next system boot.
+
[subs="quotes"]
....
sh-4.4# *touch /.autorelabel*
....

. Remount the file system as *read only*:
+
[literal,subs="+quotes,verbatim"]
....
sh-4.4# *mount -o remount,ro /*
....

. Enter the [command]`exit` command to exit the [command]`chroot` environment.

. Enter the [command]`exit` command again to resume the initialization and finish the system boot.
+
[NOTE]
====

The SELinux relabeling process can take a long time. A system reboot occurs automatically when the process is complete.

====

[TIP]
====

You can omit the time consuming SELinux relabeling process by adding the [option]`enforcing=0` option.

.Procedure

. When adding the [parameter]`rd.break` parameter at the end of the `linux` line, append [parameter]`enforcing=0` as well.
+
[literal,subs="+quotes,verbatim"]
....
rd.break enforcing=0
....

. Restore the `/etc/shadow` file's SELinux security context.
+
[literal,subs="+quotes,verbatim"]
....
# *restorecon /etc/shadow*
....

. Turn SELinux policy enforcement back on and verify that it is on.
+
[literal,subs="+quotes,verbatim"]
....
# *setenforce 1*
# *getenforce*
Enforcing
....

Note that if you added the [option]`enforcing=0` option in step 3 you can omit entering the [command]`touch /.autorelabel` command in step 8.

====
