// Module included in the following assemblies:
//
// assembly_building-a-customized-boot-menu


[id="con_the-grub-2-configuration-file_{context}"]
= The GRUB configuration file

[role="_abstract"]
Learn about the boot loader configuration file that is [filename]`/boot/grub2/grub.cfg` on BIOS-based machines and [filename]`/boot/efi/EFI/redhat/grub.cfg` on UEFI-based machines.

GRUB scripts search the user's computer and build a boot menu based on what operating systems the scripts find. To reflect the latest system boot options, the boot menu is rebuilt automatically when the kernel is updated or a new kernel is added.

GRUB uses a series of scripts, located in the [filename]`/etc/grub.d/` directory, to build the menu. The scripts include the following files:

* `00_header`, which loads GRUB settings from the [filename]`/etc/default/grub` file.

* `01_users`, which reads the root password from the [filename]`user.cfg` file.

* `10_linux`, which locates kernels in the default partition of Red Hat Enterprise Linux.

* `30_os-prober`, which builds entries for operating systems found on other partitions.

* `40_custom`, a template used to create additional menu entries.

GRUB reads scripts from the `/etc/grub.d/` directory in alphabetical order and therefore you can rename them to change the boot order of specific menu entries.

////
The DEFAULTKERNEL option in /etc/sysconfig/kernel no longer applies and instead
DEFAULTDEBUG=yes can be used to make the debug kernels as default when installing.
////
