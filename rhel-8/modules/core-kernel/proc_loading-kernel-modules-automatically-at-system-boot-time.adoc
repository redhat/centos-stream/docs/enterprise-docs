:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="loading-kernel-modules-automatically-at-system-boot-time_{context}"]
= Loading kernel modules automatically at system boot time

[role="_abstract"]
Configure a kernel module to load it automatically during the boot process.

.Prerequisites

* Root permissions
* The [package]`kmod` package is installed.

.Procedure

. Select a kernel module you want to load during the boot process.
+
The modules are located in the `/lib/modules/$(uname -r)/kernel/<SUBSYSTEM>/` directory.

. Create a configuration file for the module:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *echo <pass:quotes[_MODULE_NAME_]> > /etc/modules-load.d/<pass:quotes[_MODULE_NAME_]>.conf*
....
+
[NOTE]
====
When entering the name of a kernel module, do not append the `.ko.xz` extension to the end of the name. Kernel module names do not have extensions; their corresponding files do.
====

.Verification

. After reboot, verify the relevant module is loaded:
+
[literal,subs="+quotes,verbatim,normal"]
....
$ *lsmod | grep <pass:quotes[_MODULE_NAME_]>*
....
+


[IMPORTANT]
====
The changes described in this procedure *will persist* after rebooting the system.
====


[role="_additional-resources"]
.Additional resources
* `modules-load.d(5)` manual page
