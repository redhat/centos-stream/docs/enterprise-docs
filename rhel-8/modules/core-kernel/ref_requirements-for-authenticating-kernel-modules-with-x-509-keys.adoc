:_mod-docs-content-type: REFERENCE
:experimental:
[id="ref_requirements-for-authenticating-kernel-modules-with-x-509-keys_{context}"]
= Requirements for authenticating kernel modules with X.509 keys

[role="_abstract"]
In {ProductShortName}{nbsp}{ProductNumber}, when a kernel module is loaded, the kernel checks the signature of the module against the public X.509 keys from the kernel system keyring (`.builtin_trusted_keys`)
and the kernel platform keyring (`.platform`). The `.platform` keyring provides keys from third-party platform providers and custom public keys.
The keys from the kernel system `.blacklist` keyring are excluded from verification.

You need to meet certain conditions to load kernel modules on systems with enabled UEFI Secure Boot functionality:

* If UEFI Secure Boot is enabled or if the [option]`module.sig_enforce` kernel parameter has been specified:

** You can only load those signed kernel modules whose signatures were authenticated against keys from the system keyring (`.builtin_trusted_keys`) and the platform keyring (`.platform`).
** The public key must not be on the system revoked keys keyring (`.blacklist`).

* If UEFI Secure Boot is disabled and the [option]`module.sig_enforce` kernel parameter has not been specified:

** You can load unsigned kernel modules and signed kernel modules without a public key.


* If the system is not UEFI-based or if UEFI Secure Boot is disabled:

** Only the keys embedded in the kernel are loaded onto `.builtin_trusted_keys` and `.platform`.
** You have no ability to augment that set of keys without rebuilding the kernel.


.Kernel module authentication requirements for loading

[options="header", cols="2,2,2,2,2,1"]
|===
|Module signed|Public key found and signature valid|UEFI Secure Boot state|`sig_enforce`|Module load|Kernel tainted
.3+|Unsigned
.3+|-
|Not enabled
|Not enabled
|Succeeds
|Yes
|Not enabled
|Enabled
|Fails
|-
|Enabled
|-
|Fails
|-
.3+|Signed
.3+|No
|Not enabled
|Not enabled
|Succeeds
|Yes
|Not enabled
|Enabled
|Fails
|-
|Enabled
|-
|Fails
|-
.3+|Signed
.3+|Yes
|Not enabled
|Not enabled
|Succeeds
|No
|Not enabled
|Enabled
|Succeeds
|No
|Enabled
|-
|Succeeds
|No
|===
