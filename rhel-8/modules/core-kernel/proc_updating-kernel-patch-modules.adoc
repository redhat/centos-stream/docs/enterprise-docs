:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/proc_updating-kernel-patch-modules.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: proc_doing-procedure-a.adoc
// * ID: [id='proc_doing-procedure-a_{context}']
// * Title: = Doing procedure A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// Start the title with a verb, such as Creating or Create. See also
// _Wording of headings_ in _The IBM Style Guide_.
[id="updating-kernel-patch-modules_{context}"]
= Updating kernel patch modules

[role="_abstract"]
The kernel patch modules are delivered and applied through RPM packages. The process of updating a cumulative kernel patch module is similar to updating any other RPM package.

.Prerequisites

* The system is subscribed to the live patching stream, as described in xref:subscribing-the-currently-installed-kernels-to-the-live-patching-stream_applying-patches-with-kernel-live-patching[Subscribing the currently installed kernels to the live patching stream].


.Procedure

* Update to a new cumulative version for the current kernel:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *{PackageManagerCommand} update "kpatch-patch = $(uname -r)"*
....
+
The command above automatically installs and applies any updates that are available for the currently running kernel. Including any future released cumulative live patches.


* Alternatively, update all installed kernel patch modules:
+
[literal,subs="+quotes,verbatim,normal"]
....
# *{PackageManagerCommand} update "kpatch-patch*"
....


[NOTE]
====
When the system reboots into the same kernel, the kernel is automatically live patched again by the `kpatch.service` systemd service.
====

[role="_additional-resources"]
.Additional resources
ifeval::[{ProductNumber} == 8]
* [citetitle]_link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/configuring_basic_system_settings/index#updating-software-packages_managing-software-packages[Configuring basic system settings]_ in {ProductShortName}
endif::[]
ifeval::[{ProductNumber} >= 9]
* [citetitle]_link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_software_with_the_dnf_tool/assembly_updating-rhel-9-content_managing-software-with-the-dnf-tool#proc_updating-packages-with-yum_assembly_updating-rhel-9-content[Updating software packages]_ in {ProductShortName}
endif::[]


