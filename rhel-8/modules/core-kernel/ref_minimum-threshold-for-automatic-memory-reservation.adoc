:_mod-docs-content-type: REFERENCE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This module can be included from assemblies using the following include statement:
// include::<path>/ref_minimum-threshold-for-automatic-memory-reservation.adoc[leveloffset=+1]

// The file name and the ID are based on the module title. For example:
// * file name: ref_my-reference-a.adoc
// * ID: [id='ref_my-reference-a_{context}']
// * Title: = My reference A
//
// The ID is used as an anchor for linking to the module. Avoid changing
// it after the module has been published to ensure existing links are not
// broken.
//
// The `context` attribute enables module reuse. Every module's ID includes
// {context}, which ensures that the module has a unique ID even if it is
// reused multiple times in a guide.
//
// In the title, include nouns that are used in the body text. This helps
// readers and search engines find information quickly.
[id="minimum-threshold-for-automatic-memory-reservation_{context}"]
= Minimum threshold for automatic memory reservation

[role="_abstract"]
By default, the `kexec-tools` utility configures the `crashkernel` command line parameter and reserves a certain amount of memory for `kdump`. On some systems however, it is still possible to assign memory for `kdump` either by using the [option]`crashkernel=auto` parameter in the boot loader configuration file, or by enabling this option in the graphical configuration utility. For this automatic reservation to work, a certain amount of total memory needs to be available in the system. The memory requirement varies based on the system's architecture. If the system memory is less than the specified threshold value, you must configure the memory manually.

ifeval::[{ProductNumber} == 8]
.Minimum amount of memory required for automatic memory reservation

[options="header"]
|===
|Architecture|Required Memory
|AMD64 and Intel{nbsp}64 (`x86_64`)|2{nbsp}GB
|IBM Power Systems (`ppc64le`)|2{nbsp}GB
|IBM {nbsp}Z (`s390x`)|4{nbsp}GB
|===
endif::[]


ifeval::[{ProductNumber} == 9]
.Minimum amount of memory required for automatic memory reservation

[options="header"]
|===
|Architecture|Required Memory
|AMD64 and Intel{nbsp}64 (`x86_64`)|1{nbsp}GB
|IBM Power Systems (`ppc64le`)|2{nbsp}GB
|IBM {nbsp}Z (`s390x`)|1{nbsp}GB
|64-bit ARM|1 GB
|===
endif::[]


[NOTE]
====
The `crashkernel=auto` option in the boot command line is no longer supported on RHEL 9 and later releases.
====