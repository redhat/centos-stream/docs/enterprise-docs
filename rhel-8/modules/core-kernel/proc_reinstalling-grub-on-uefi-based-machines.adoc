:_mod-docs-content-type: PROCEDURE

[id="proc_reinstalling-grub-on-uefi-based-machines_{context}"]
= Reinstalling GRUB on UEFI-based machines

You can reinstall the GRUB boot loader on your UEFI-based system.

[IMPORTANT]
====
Ensure that the system does not cause data corruption or boot crash during the installation.
====

.Procedure

. Reinstall the `grub2-efi` and `shim` boot loader files:
+
[subs="quotes"]
....
# *yum reinstall grub2-efi shim*
....

. Reboot your system for the changes to take effect:
+
[subs="quotes"]
....
# *reboot*
....
