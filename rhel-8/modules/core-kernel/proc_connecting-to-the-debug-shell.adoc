// Module included in the following assemblies:
//
// assembly_making-temporary-changes-to-a-grub-2-menu

[id="proc_connecting-to-the-debug-shell_{context}"]
= Connecting to the debug shell

[role="_abstract"]
During the boot process, the `systemd-debug-generator` configures the debug shell on TTY9.

.Prerequisites

* You have booted to the debug shell successfully. See xref:proc_booting-to-the-debug-shell_assembly_making-temporary-changes-to-the-grub-menu[Booting to the debug shell].

.Procedure

. Press kbd:[Ctrl + Alt + F9] to connect to the debug shell.
+
If you work with a virtual machine, sending this key combination requires support from the virtualization application.
For example, if you use [application]*Virtual Machine Manager*, select menu:Send Key[pass:attributes[{blank}]`Ctrl+Alt+F9`pass:attributes[{blank}]] from the menu.

. The debug shell does not require authentication, therefore you can see a prompt similar to the following on TTY9:
[literal,subs="+quotes,verbatim"]
....
sh-4.4#
....

.Verification

* Enter a command as follows:
+
[literal,subs="+quotes,verbatim"]
....
sh-4.4# *systemctl status $$*
....
+
image::debug-shell-prompt.png[Connecting to the Debug Shell]

* To return to the default shell, if the boot succeeded, press kbd:[Ctrl + Alt + F1].

[role="_additional-resources"]
.Additional resources
* The `systemd-debug-generator(8)` manual page

////
To diagnose start up problems, certain `systemd` units can be masked by adding [command]`systemd.mask=pass:attributes[{blank}]_unit_name_pass:attributes[{blank}]` one or more times on the kernel command line.
To start additional processes during the boot process, add [command]`systemd.wants=pass:attributes[{blank}]_unit_name_pass:attributes[{blank}]` to the kernel command line.
////
