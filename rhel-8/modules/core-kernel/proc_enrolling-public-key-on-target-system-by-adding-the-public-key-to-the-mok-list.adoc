:_mod-docs-content-type: PROCEDURE
:experimental:
[id="enrolling-public-key-on-target-system-by-adding-the-public-key-to-the-mok-list_{context}"]
= Enrolling public key on target system by adding the public key to the MOK list

[role="_abstract"]
 
You must authenticate your public key on a system for kernel or kernel module access and enroll it in the platform keyring (`.platform`) of the target system. When {ProductShortName} {ProductNumber} boots on a UEFI-based system with Secure Boot enabled, the kernel imports public keys from the `db` key database and excludes revoked keys from the `dbx` database.

The Machine Owner Key (MOK) facility allows expanding the UEFI Secure Boot key database. When booting {ProductShortName} {ProductNumber} on UEFI-enabled systems with Secure Boot enabled, keys on the MOK list are added to the platform keyring (`.platform`), along with the keys from the Secure Boot database. The list of MOK keys is stored securely and persistently in the same way, but it is a separate facility from the Secure Boot databases.

The MOK facility is supported by `shim`, `MokManager`, `GRUB`, and the `mokutil` utility that enables secure key management and authentication for UEFI-based systems.

[NOTE]
====
To get the authentication service of your kernel module on your systems, consider requesting your system vendor to incorporate your public key into the UEFI Secure Boot key database in their factory firmware image.
====

.Prerequisites

* You have generated a public and private key pair and know the validity dates of your public keys. For details, see xref:generating-a-public-and-private-key-pair_{context}[Generating a public and private key pair].

.Procedure

. Export your public key to the `sb_cert.cer` file:
+
[subs="+quotes"]
----
# *certutil -d /etc/pki/pesign \*
           *-n '_Custom Secure Boot key_' \*
           *-Lr \*
           *> sb_cert.cer*
----

. Import your public key into the MOK list:
+
[subs="+quotes,attributes,verbatim"]
----
# *mokutil --import sb_cert.cer*
----

. Enter a new password for this MOK enrollment request.

. Reboot the machine.
+
The `shim` boot loader notices the pending MOK key enrollment request and it launches `MokManager.efi` to enable you to complete the enrollment from the UEFI console.

. Choose `Enroll MOK`, enter the password you previously associated with this request when prompted, and confirm the enrollment.
+
Your public key is added to the MOK list, which is persistent.
+
Once a key is on the MOK list, it will be automatically propagated to the `.platform` keyring on this and subsequent boots when UEFI Secure Boot is enabled.
