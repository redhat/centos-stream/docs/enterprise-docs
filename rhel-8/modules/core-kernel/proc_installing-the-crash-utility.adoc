:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

[id="installing-the-crash-utility_{context}"]
= Installing the crash utility

[role="_abstract"]
With the provided information, understand the required packages and the procedure to install the [application]*crash* utility. The `crash` utility might not be installed by default on your {ProductShortName}{nbsp}{ProductNumber} systems. `crash` is a tool to interactively analyze a system's state while it is running or after a kernel crash occurs and a core dump file is created. The core dump file is also known as the `vmcore` file. 

.Procedure

. Enable the relevant repositories:
+
[subs="quotes,attributes,macros"]
----
# subscription-manager repos --enable pass:quotes[_baseos repository_]
----
+
[subs="quotes,attributes,macros"]
----
# subscription-manager repos --enable pass:quotes[_appstream repository_]
----

ifeval::[{ProductNumber} == 8]
+
[subs="quotes,attributes,macros"]
----
# subscription-manager repos --enable pass:quotes[_rhel-8-for-x86_64-baseos-debug-rpms_]
----
endif::[]

ifeval::[{ProductNumber} >= 9]
+
[subs="quotes,attributes,macros"]
----
# subscription-manager repos --enable pass:quotes[_rhel-9-for-x86_64-baseos-debug-rpms_]
----
endif::[]
+
. Install the [package]`crash` package:
+
[subs="quotes,attributes,macros"]
----
# {PackageManagerCommand} install crash
----

. Install the [package]`kernel-debuginfo` package:
+
[subs="quotes,attributes,macros"]
----
# {PackageManagerCommand} install kernel-debuginfo
----
+
The package `kernel-debuginfo` will correspond to the running kernel and provides the data necessary for the dump analysis.
