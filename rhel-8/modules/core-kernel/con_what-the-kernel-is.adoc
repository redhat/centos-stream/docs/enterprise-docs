:_mod-docs-content-type: CONCEPT
:experimental:
// Module included in the following assemblies:
//
// assembly_the-linux-kernel.adoc

[id="what-the-kernel-is_{context}"]
= What the kernel is

[role="_abstract"]
The kernel is a core part of a Linux operating system that manages the system resources and provides interface between hardware and software applications.

The {RH}{nbsp}kernel is a custom-built kernel based on the upstream Linux mainline kernel that {RH}{nbsp}engineers further develop and harden with a focus on stability and compatibility with the latest technologies and hardware.

Before {RH} releases a new kernel version, the kernel needs to pass a set of rigorous quality assurance tests.

The {RH} kernels are packaged in the RPM format so that they are easily upgraded and verified by the [application]*{PackageManagerName}* package manager.

[WARNING]
====
Kernels that are not compiled by {RH} are *not* supported by {RH}.
====
