:_mod-docs-content-type: PROCEDURE
:experimental:
[id="enabling-early-kdump_{context}"]
= Enabling early kdump

[role="_abstract"]
The `early kdump` feature sets up the crash kernel and the initial RAM disk image (`initramfs`) to load early enough to capture the `vmcore` information for an early crash. This helps to eliminate the risk of losing information about the early boot kernel crashes.

.Prerequisites

* An active {ProductShortName} subscription.
* A repository containing the [package]`kexec-tools` package for your system CPU architecture.
ifeval::[{ProductNumber} == 8]
* Fulfilled `kdump` configuration and targets requirements. For more information see, link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_monitoring_and_updating_the_kernel/index#supported-kdump-configurations-and-targets_managing-monitoring-and-updating-the-kernel[Supported kdump configurations and targets].
endif::[]
ifeval::[{ProductNumber} == 9]
* Fulfilled `kdump` configuration and targets requirements. For more information see, link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/managing_monitoring_and_updating_the_kernel/index#supported-kdump-configurations-and-targets_managing-monitoring-and-updating-the-kernel[Supported kdump configurations and targets].
endif::[]



.Procedure

. Verify that the `kdump` service is enabled and active:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# *systemctl is-enabled kdump.service && systemctl is-active kdump.service*
enabled
active
....
+
If `kdump` is not enabled and running, set all required configurations and verify that `kdump` service is enabled.

. Rebuild the `initramfs` image of the booting kernel with the `early kdump` functionality:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# dracut -f --add earlykdump
....


. Add the `rd.earlykdump` kernel command line parameter:
+
[literal,subs="+quotes,attributes,verbatim"]
....
# grubby --update-kernel=/boot/vmlinuz-$(uname -r) --args="rd.earlykdump"
....

. Reboot the system to reflect the changes:
+
[subs=quotes]
----
# reboot
----

.Verification

* Verify that `rd.earlykdump` is successfully added and `early kdump` feature is enabled:
+
ifeval::[{ProductNumber} == 8]
[literal,subs="quotes,attributes"]
....
# *cat /proc/cmdline*
BOOT_IMAGE=(hd0,msdos1)/vmlinuz-4.18.0-187.el8.x86_64 root=/dev/mapper/rhel-root ro crashkernel=auto resume=/dev/mapper/rhel-swap rd.lvm.lv=rhel/root rd.lvm.lv=rhel/swap rhgb quiet rd.earlykdump

# *journalctl -x | grep early-kdump*
Mar 20 15:44:41 redhat dracut-cmdline[304]: early-kdump is enabled.
Mar 20 15:44:42 redhat dracut-cmdline[304]: kexec: loaded early-kdump kernel
....
endif::[]
ifeval::[{ProductNumber} == 9]
[literal,subs="quotes,attributes"]
....
# *cat /proc/cmdline*
BOOT_IMAGE=(hd0,msdos1)/vmlinuz-5.14.0-1.el9.x86_64 root=/dev/mapper/rhel-root ro crashkernel=auto resume=/dev/mapper/rhel-swap rd.lvm.lv=rhel/root rd.lvm.lv=rhel/swap rhgb quiet rd.earlykdump

# *journalctl -x | grep early-kdump*
Sep 13 15:46:11 redhat dracut-cmdline[304]: early-kdump is enabled.
Sep 13 15:46:12 redhat dracut-cmdline[304]: kexec: loaded early-kdump kernel
....
endif::[]


[role="_additional-resources"]
.Additional resources
* The `/usr/share/doc/kexec-tools/early-kdump-howto.txt` file
* link:https://access.redhat.com/solutions/3700611[What is early kdump support and how do I configure it?] (Red Hat Knowledgebase)
