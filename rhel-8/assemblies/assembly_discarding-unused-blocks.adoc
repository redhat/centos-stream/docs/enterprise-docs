ifdef::context[:parent-context-of-discarding-unused-blocks: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="discarding-unused-blocks"]
endif::[]
ifdef::context[]
[id="discarding-unused-blocks_{context}"]
endif::[]
= Discarding unused blocks

:context: discarding-unused-blocks

[role="_abstract"]
You can perform or schedule discard operations on block devices that support them. The block discard operation communicates to the underlying storage which file system blocks are no longer in use by the mounted file system. Block discard operations allow SSDs to optimize garbage collection routines, and they can inform thinly-provisioned storage to repurpose unused physical blocks. 

[discrete]
== Requirements

* The block device underlying the file system must support physical discard operations.
+
Physical discard operations are supported if the value in the [filename]`/sys/block/[replaceable]_<device>_/queue/discard_max_bytes` file is not zero.

include::modules/filesystems-and-storage/ref_types-of-block-discard-operations.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_performing-batch-block-discard.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_enabling-online-block-discard.adoc[leveloffset=+1]

include::modules/core-services/ref_an-example-ansible-playbook-to-enable-online-block-discard.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_enabling-periodic-block-discard.adoc[leveloffset=+1]

ifdef::parent-context-of-discarding-unused-blocks[:context: {parent-context-of-discarding-unused-blocks}]
ifndef::parent-context-of-discarding-unused-blocks[:!context:]
