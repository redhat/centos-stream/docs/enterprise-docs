:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-tuning-scheduling-policy: {context}]

ifndef::context[]
[id="tuning-scheduling-policy"]
endif::[]
ifdef::context[]
[id="tuning-scheduling-policy_{context}"]
endif::[]
= Tuning scheduling policy

:context: tuning-scheduling-policy

[role="_abstract"]
In {RHEL}, the smallest unit of process execution is called a thread. The system scheduler determines which processor runs a thread, and for how long the thread runs. However, because the scheduler's primary concern is to keep the system busy, it may not schedule threads optimally for application performance.

For example, say an application on a NUMA system is running on Node A when a processor on Node B becomes available. To keep the processor on Node B busy, the scheduler moves one of the application's threads to Node B. However, the application thread still requires access to memory on Node A. But, this memory will take longer to access because the thread is now running on Node B and Node A memory is no longer local to the thread. Thus, it may take longer for the thread to finish running on Node B than it would have taken to wait for a processor on Node A to become available, and then to execute the thread on the original node with local memory access.

include::modules/performance/con_categories-of-scheduling-policies.adoc[leveloffset=+1]

include::modules/performance/con_static-priority-scheduling-with-sched_fifo.adoc[leveloffset=+1]

include::modules/performance/con_round-robin-priority-scheduling-with-sched_rr.adoc[leveloffset=+1]

include::modules/performance/con_normal-scheduling-with-sched_other.adoc[leveloffset=+1]

include::modules/performance/proc_setting-scheduler-policies.adoc[leveloffset=+1]

include::modules/performance/ref_policy-options-for-the-chrt-command.adoc[leveloffset=+1]

include::modules/performance/proc_changing-the-priority-of-service-during-the-boot-process.adoc[leveloffset=+1]

include::modules/performance/ref_priority-map.adoc[leveloffset=+1]

include::modules/performance/con_tuned-cpu-partitioning-profile.adoc[leveloffset=+1]

include::modules/performance/proc_using-the-tuned-cpu-partitioning-profile-for-low-latency-tuning.adoc[leveloffset=+1]

include::modules/performance/proc_customizing-the-cpu-partitioning-tuned-profile.adoc[leveloffset=+1]


ifdef::parent-context-of-tuning-scheduling-policy[:context: {parent-context-of-tuning-scheduling-policy}]
ifndef::parent-context-of-tuning-scheduling-policy[:!context:]
