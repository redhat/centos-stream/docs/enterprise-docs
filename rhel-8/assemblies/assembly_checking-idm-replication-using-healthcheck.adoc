:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

:parent-context-of-checking-idm-replication-using-healthcheck: {context}

[id='checking-idm-replication-using-healthcheck_{context}']
= Checking IdM replication using Healthcheck

:context: checking-idm-replication-using-healthcheck

[role="_abstract"]
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

You can test Identity Management (IdM) replication using the Healthcheck tool.

.Prerequisites

* You are using RHEL version 8.1 or newer.


include::modules/identity-management/con_replication-healthcheck-tests.adoc[leveloffset=+1]

include::modules/identity-management/proc_screening-replication-using-healthcheck.adoc[leveloffset=+1]



== Additional resources

ifeval::[{ProductNumber} == 9]
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_idm_healthcheck_to_monitor_your_idm_environment/installing-and-running-the-ipa-healthcheck-tool_using-idm-healthcheck-to-monitor-your-idm-environment#healthcheck-in-idm_installing-and-running-the-ipa-healthcheck-tool[Healthcheck in IdM]
endif::[]
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_and_managing_identity_management/index#healthcheck-in-idm_collecting-idm-healthcheck-information[Healthcheck in IdM]
endif::[]

// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-checking-idm-replication-using-healthcheck}
