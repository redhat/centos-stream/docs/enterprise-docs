:_mod-docs-content-type: ASSEMBLY

:parent-context-of-debugging-nftables-rules: {context}

[id='debugging-nftables-rules_{context}']
= Debugging nftables rules

:context: debugging-nftables-rules

[role="_abstract"]
The `nftables` framework provides different options for administrators to debug rules and if packets match them.


include::modules/packet-filtering/proc_creating-a-rule-with-a-counter.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_adding-a-counter-to-an-existing-rule.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_monitoring-packets-that-match-an-existing-rule.adoc[leveloffset=+1]


:context: {parent-context-of-debugging-nftables-rules}

