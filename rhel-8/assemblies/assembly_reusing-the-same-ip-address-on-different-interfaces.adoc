:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_reusing-the-same-ip-address-on-different-interfaces.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-reusing-the-same-ip-address-on-different-interfaces: {context}

// The file name and the ID are based on the assembly title.
// For example:
// * file name: assembly_my-assembly-a.adoc
// * ID: [id='assembly_my-assembly-a_{context}']
// * Title: = My assembly A
//
// The ID is used as an anchor for linking to the module.
// Avoid changing it after the module has been published
// to ensure existing links are not broken.
//
// In order for  the assembly to be reusable in other assemblies in a guide,
// include {context} in the ID: [id='a-collection-of-modules_{context}'].
//
// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.
[id="reusing-the-same-ip-address-on-different-interfaces_{context}"]
= Reusing the same IP address on different interfaces

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: reusing-the-same-ip-address-on-different-interfaces

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
As a network administrator, I need to be able isolate the network traffic to and from specific routes and addresses, such as in a multi-tenancy server.
| Jira |
link:https://projects.engineering.redhat.com/browse/RHELPLAN-8072[]
| BZ |
link:https://bugzilla.redhat.com/show_bug.cgi?id=1688125[]
| SMEs |
Paolo Abeni <pabeni@redhat.com>
| SME Ack |
YES
| Peer Ack |
YES
|===
endif::[]

[role="_abstract"]
With Virtual routing and forwarding (VRF), administrators can use multiple routing tables simultaneously on the same host. For that, VRF partitions a network at layer 3. This enables the administrator to isolate traffic using separate and independent route tables per VRF domain. This technique is similar to virtual LANs (VLAN), which partitions a network at layer 2, where the operating system uses different VLAN tags to isolate traffic sharing the same physical medium.

One benefit of VRF over partitioning on layer 2 is that routing scales better considering the number of peers involved.

Red Hat Enterprise Linux uses a virtual [systemitem]`vrt` device for each VRF domain and adds routes to a VRF domain by adding existing network devices to a VRF device. Addresses and routes previously attached to the original device will be moved inside the VRF domain.

Note that each VRF domain is isolated from each other.


// The following include statements pull in the module files that comprise
// the assembly. Include any combination of concept, procedure, or reference
// modules required to cover the user story. You can also include other
// assemblies.

include::modules/networking/proc_permanently-reusing-the-same-ip-address-on-different-interfaces.adoc[leveloffset=+1]

include::modules/networking/proc_temporarily-reusing-the-same-ip-address-on-different-interfaces.adoc[leveloffset=+1]



// [leveloffset=+1] ensures that when a module starts with a level-1 heading
// (= Heading), the heading will be interpreted as a level-2 heading
// (== Heading) in the assembly.

[role="_additional-resources"]
== Additional resources
* `/usr/share/doc/kernel-doc-<__kernel_version__>/Documentation/networking/vrf.txt` from the [package]`kernel-doc` package

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-reusing-the-same-ip-address-on-different-interfaces}
