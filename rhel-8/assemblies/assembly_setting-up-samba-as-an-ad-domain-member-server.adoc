:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// assembly_using-samba-as-a-server.adoc

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_setting-up-samba-as-an-ad-domain-member-server.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-assembly_setting-up-samba-as-an-ad-domain-member-server: {context}

// The file name and the ID are based on the assembly title.
// For example:
// * file name: assembly_my-assembly-a.adoc
// * ID: [id='assembly_my-assembly-a_{context}']
// * Title: = My assembly A
//
// The ID is used as an anchor for linking to the module.
// Avoid changing it after the module has been published
// to ensure existing links are not broken.
//
// In order for  the assembly to be reusable in other assemblies in a guide,
// include {context} in the ID: [id='a-collection-of-modules_{context}'].
//
// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.
[id='assembly_setting-up-samba-as-an-ad-domain-member-server_{context}']
= Setting up Samba as an AD domain member server

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: assembly_setting-up-samba-as-an-ad-domain-member-server

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

If you are running an AD or NT4 domain, use Samba to add your Red Hat Enterprise Linux server as a member to the domain to gain the following:

* Access domain resources on other domain members

* Authenticate domain users to local services, such as `sshd`

* Share directories and printers hosted on the server to act as a file and print server

// The following include statements pull in the module files that comprise
// the assembly. Include any combination of concept, procedure, or reference
// modules required to cover the user story. You can also include other
// assemblies.

include::modules/core-services/proc_joining-samba-to-a-domain.adoc[leveloffset=+1]

include::modules/core-services/proc_using-the-local-authorization-plug-in-for-mit-kerberos.adoc[leveloffset=+1]

// [leveloffset=+1] ensures that when a module starts with a level-1 heading
// (= Heading), the heading will be interpreted as a level-2 heading
// (== Heading) in the assembly.

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-assembly_setting-up-samba-as-an-ad-domain-member-server}
