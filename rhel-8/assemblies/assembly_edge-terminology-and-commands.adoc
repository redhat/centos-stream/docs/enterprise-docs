:_mod-docs-content-type: ASSEMBLY
ifndef::context[]
[id="edge-terminology-and-commands"]
endif::[]
ifdef::context[]
[id="edge-terminology-and-commands_{context}"]
endif::[]
//[id="edge-terminology-and-commands_{context}"]
[appendix]
= Terminology and commands

:context: edge-terminology-and-commands

[role="_abstract"]
Learn more about the `rpm ostree` terminology and commands.

include::modules/edge/ref_edge-rpmostree-terminology.adoc[leveloffset=+1]

include::modules/edge/ref_edge-ostree-commands.adoc[leveloffset=+1]

include::modules/edge/ref_rpm-ostree-commands.adoc[leveloffset=+1]

include::modules/edge/ref_fdo-terminology.adoc[leveloffset=+1]

include::modules/edge/ref_fdo-automatic-onboarding-technologies.adoc[leveloffset=+1]

:context: {parent-edge-terminology-and-commands}
