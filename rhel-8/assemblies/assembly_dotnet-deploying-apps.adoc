:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_dotnet-deploying-apps: {context}]

ifndef::context[]
[id="assembly_dotnet-deploying-apps"]
endif::[]
ifdef::context[]
[id="assembly_dotnet-deploying-apps_{context}"]
endif::[]
= Deploying applications with {os} Client

:context: assembly_dotnet-deploying-apps

[role="_abstract"]
You can use OpenShift Client (oc) for application deployment. You can deploy applications from source or from binary artifacts.

include::modules/dotnet/proc_deploying-applications-from-source.adoc[leveloffset=+1]

include::modules/dotnet/proc_deploying-applications-from-binary-artifacts.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_dotnet-deploying-apps[:context: {parent-context-of-assembly_dotnet-deploying-apps}]
ifndef::parent-context-of-assembly_dotnet-deploying-apps[:!context:]
