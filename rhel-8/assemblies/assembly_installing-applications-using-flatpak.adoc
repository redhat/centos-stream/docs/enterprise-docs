:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_installing-applications-using-flatpak: {context}]

ifndef::context[]
[id="assembly_installing-applications-using-flatpak"]
endif::[]
ifdef::context[]
[id="assembly_installing-applications-using-flatpak_{context}"]
endif::[]
= Installing applications using Flatpak

:context: assembly_installing-applications-using-flatpak

[role="_abstract"]
You can install certain applications using the Flatpak package manager. The following sections describe how to search for, install, launch, and update Flatpak applications on the command line and in the graphical interface.

[IMPORTANT]
====

{RH} provides Flatpak applications only as a Technology Preview feature. Technology Preview features are not supported with Red Hat production service level agreements (SLAs) and might not be functionally complete. Red Hat does not recommend using them in production. These features provide early access to upcoming product features, enabling customers to test functionality and provide feedback during the development process. For more information about the support scope of Red Hat Technology Preview features, see link:https://access.redhat.com/support/offerings/techpreview[https://access.redhat.com/support/offerings/techpreview]. 

The Flatpak package manager itself is fully supported.

====

include::modules/desktop/con_the-flatpak-technology.adoc[leveloffset=+1]

include::modules/desktop/proc_setting-up-flatpak.adoc[leveloffset=+1]

include::modules/desktop/proc_enabling-the-red-hat-flatpak-remote.adoc[leveloffset=+1]

include::modules/desktop/proc_searching-for-flatpak-applications.adoc[leveloffset=+1]

include::modules/desktop/proc_installing-flatpak-applications.adoc[leveloffset=+1]

include::modules/desktop/proc_launching-flatpak-applications.adoc[leveloffset=+1]

include::modules/desktop/proc_updating-flatpak-applications.adoc[leveloffset=+1]

include::modules/desktop/proc_installing-flatpak-applications-in-the-graphical-interface.adoc[leveloffset=+1]

include::modules/desktop/proc_updating-flatpak-applications-in-the-graphical-interface.adoc[leveloffset=+1]

// [role="_additional-resources"]
// == Additional resources (or Next steps)
// * A bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

ifdef::parent-context-of-assembly_installing-applications-using-flatpak[:context: {parent-context-of-assembly_installing-applications-using-flatpak}]
ifndef::parent-context-of-assembly_installing-applications-using-flatpak[:!context:]

