:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly_configuring-virtual-domain-as-a-resource: {context}

[id='assembly_configuring-virtual-domain-as-a-resource-{context}']
= Configuring a virtual domain as a resource
:context: configuring-virtual-domain-as-a-resource

[role="_abstract"]
You can configure a virtual domain that is managed by the `libvirt` virtualization framework as a cluster resource with the [command]`pcs resource create` command, specifying `VirtualDomain` as the resource type.

When configuring a virtual domain as a resource, take the following considerations into account:

* A virtual domain should be stopped before you configure it as a cluster resource.

* Once a virtual domain is a cluster resource, it should not be started, stopped, or migrated except through the cluster tools.

* Do not configure a virtual domain that you have configured as a cluster resource to start when its host boots.

* All nodes allowed to run a virtual domain must have access to the necessary configuration files and storage devices for that virtual domain.

If you want the cluster to manage services within the virtual domain itself, you can configure the virtual domain as a guest node.


// LINK TO
// For information about configuring guest nodes, see <<pacemaker_remote>>

// LINK TO
// For information about configuring virtual domains, see the
// link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Virtualization_Deployment_and_Administration_Guide/index.html++[Virtualization Deployment and Administration Guide].

include::modules/high-availability/ref_virtual-domain-resource-options.adoc[leveloffset=+1]

include::modules/high-availability/proc_creating-virtual-domain-resource.adoc[leveloffset=+1]

:context: {parent-context-of-assembly_configuring-virtual-domain-as-a-resource}



