:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-preparing-an-environment-for-vdo-performance-testing: {context}]


[id="preparing-an-environment-for-vdo-performance-testing_{context}"]
= Preparing an environment for VDO performance testing

:context: preparing-an-environment-for-vdo-performance-testing

Before testing VDO performance, you must consider the host system configuration, VDO configuration, and the workloads that will be used during testing. These choices affect the benchmarking of space efficiency, bandwidth, and latency.

To prevent one test from affecting the results of another, you must create a new VDO volume for each iteration of each test.

// .Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

include::modules/filesystems-and-storage/ref_considerations-before-testing-vdo-performance.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_special-considerations-for-testing-vdo-read-performance.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_preparing-the-system-for-testing-vdo-performance.adoc[leveloffset=+1]


ifdef::parent-context-of-preparing-an-environment-for-vdo-performance-testing[:context: {parent-context-of-preparing-an-environment-for-vdo-performance-testing}]
ifndef::parent-context-of-preparing-an-environment-for-vdo-performance-testing[:!context:]
