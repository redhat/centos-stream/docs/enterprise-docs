ifdef::context[:parent-context-of-assembly_configuring-firewalld-using-system-roles: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_configuring-firewalld-using-system-roles"]
endif::[]
ifdef::context[]
[id="assembly_configuring-firewalld-using-system-roles_{context}"]
endif::[]
= Configuring `firewalld` by using {RHELSystemRoles}s

:context: assembly_configuring-firewalld-using-system-roles

{RHELSystemRoles}s is a set of contents for the Ansible automation utility. This content together with the Ansible automation utility provides a consistent configuration interface to remotely manage multiple systems at once.

The `rhel-system-roles` package contains the `rhel-system-roles.firewall` {RHELSystemRoles}. This role was introduced for automated configurations of the `firewalld` service.

With the `firewall` {RHELSystemRoles} you can configure many different `firewalld` parameters, for example:

* Zones
* The services for which packets should be allowed
* Granting, rejection, or dropping of traffic access to ports
* Forwarding of ports or port ranges for a zone


include::modules/packet-filtering/proc_resetting-the-firewalld-settings-by-using-a-rhel-system-role.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_forwarding-incoming-traffic-in-firewalld-from-one-local-port-to-a-different-local-port-by-using-a-rhel-system-role.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_configuring-a-firewalld-dmz-zone-by-using-a-rhel-system-role.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_configuring-firewalld-using-system-roles[:context: {parent-context-of-assembly_configuring-firewalld-using-system-roles}]
ifndef::parent-context-of-assembly_configuring-firewalld-using-system-roles[:!context:]

