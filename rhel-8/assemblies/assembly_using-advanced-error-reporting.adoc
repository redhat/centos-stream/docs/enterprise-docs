:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_advanced-error-reporting: {context}]

////
Base the file name and the ID on the assembly title. For example:
* file name: assembly-my-user-story.adoc
* ID: [id="assembly-my-user-story_{context}"]
* Title: = My user story

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken. Include {context} in the ID so the assembly can be reused.
////

ifndef::context[]
[id="assembly_advanced-error-reporting"]
endif::[]
ifdef::context[]
[id="assembly_advanced-error-reporting_{context}"]
endif::[]
= Using Advanced Error Reporting

:context: assembly_advanced-error-reporting


[role="_abstract"]
When you use the `Advanced Error Reporting` (`AER`), you receive notifications of error events for `Peripheral Component Interconnect Express` (`PCIe`) devices. {ProductShortName} enables this kernel feature by default and collects the reported errors in the kernel logs. If you use the `rasdaemon` program, these errors are parsed and stored in its database.

include::modules/core-kernel/con_what-is-advanced-error-reporting.adoc[]

include::modules/core-kernel/proc_collecting-and-displaying-aer-messages.adoc[]

////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_advanced-error-reporting[:context: {parent-context-of-assembly_advanced-error-reporting}]
ifndef::parent-context-of-assembly_advanced-error-reporting[:!context:]
