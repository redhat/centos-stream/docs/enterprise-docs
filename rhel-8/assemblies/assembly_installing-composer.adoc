:_mod-docs-content-type: ASSEMBLY
:parent-context-of-installing-composer: {context}

[id="installing-composer_{context}"]
= Installing RHEL image builder

:context: installing-composer

[role="_abstract"]
ifndef::composer-guide[]
RHEL image builder is a tool for creating custom system images.
endif::[]
Before using RHEL image builder, you must install it.

//include::modules/composer/proc_preparing-a-repository-mirror-for-composer.adoc[leveloffset=+1]

include::modules/composer/ref_composer-system-requirements.adoc[leveloffset=+1]

include::modules/composer/proc_installing-composer-in-a-virtual-machine.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 8]
include::modules/composer/proc_reverting-to-previous-backend.adoc[leveloffset=+1]
endif::[]

:context: {parent-context-of-installing-composer}
