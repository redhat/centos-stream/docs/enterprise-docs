:_mod-docs-content-type: ASSEMBLY
:parent-context-of-managing-hosts-using-Ansible-playbooks: {context}

[id="managing-hosts-using-Ansible-playbooks_{context}"]
= Managing hosts using Ansible playbooks

:context: managing-hosts-using-Ansible-playbooks

[role="_abstract"]
Ansible is an automation tool used to configure systems, deploy software, and perform rolling updates. Ansible includes support for Identity Management (IdM), and you can use Ansible modules to automate host management.

The following concepts and operations are performed when managing hosts and host entries using Ansible playbooks:

//* xref:hosts_{context}[Hosts in IdM]
//* xref:assembly_host-enrollment_{context}[Host enrollment]
* xref:ensuring-the-presence-of-an-IdM-host-entry-with-FQDN-using-Ansible-playbooks_{context}[Ensuring the presence of IdM host entries that are only defined by their `FQDNs`]
* xref:ensuring-the-presence-of-an-IdM-host-entry-with-DNS-information-using-Ansible-playbooks_{context}[Ensuring the presence of IdM host entries with IP addresses]
* xref:ensuring-the-presence-of-multiple-IdM-host-entries-with-random-passwords-using-Ansible-playbooks_{context}[Ensuring the presence of multiple IdM host entries with random passwords]
* xref:ensuring-the-presence-of-an-IdM-host-entry-with-multiple-IP-addresses-using-Ansible-playbooks_{context}[Ensuring the presence of an IdM host entry with multiple IP addresses]
* xref:ensuring-the-absence-of-an-IdM-host-entry-using-Ansible-playbooks_{context}[Ensuring the absence of IdM host entries]

//include::modules/identity-management/con_hosts.adoc[leveloffset=+1]

//include::assembly_host-enrollment.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-the-presence-of-an-IdM-host-entry-with-FQDN-using-Ansible-playbooks.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-the-presence-of-an-IdM-host-entry-with-DNS-information-using-Ansible-playbooks.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-the-presence-of-multiple-IdM-host-entries-with-random-passwords-using-Ansible-playbooks.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-the-presence-of-an-IdM-host-entry-with-multiple-IP-addresses-using-Ansible-playbooks.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-the-absence-of-an-IdM-host-entry-using-Ansible-playbooks.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* See the `/usr/share/doc/ansible-freeipa/README-host.md` Markdown file.
* See the additional playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/host` directory.



:context: {parent-context-of-managing-hosts-using-Ansible-playbooks}
