ifdef::context[:parent-context-of-preventing-devices-from-multipathing: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="preventing-devices-from-multipathing"]
endif::[]
ifdef::context[]
[id="preventing-devices-from-multipathing_{context}"]
endif::[]
= Preventing devices from multipathing

:context: preventing-devices-from-multipathing

You can configure DM Multipath to ignore selected devices when it configures multipath devices. DM Multipath does not group these ignored devices into a multipath device.


// == Prerequisites

// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.
// * X is installed. For information about installing X, see <link>.
// * You can log in to X with administrator privileges.


include::modules/filesystems-and-storage/con_conditions-when-dm-multipath-creates-a-multipath-device-for-a-path.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_criteria-for-disabling-multipathing-on-certain-devices.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_disabling-multipathing-by-wwid.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_disabling-multipathing-by-device-name.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_disabling-multipathing-by-device-type.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_disabling-multipathing-by-udev-property.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_disabling-multipathing-by-device-protocol.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_adding-exceptions-for-devices-with-disabled-multipathing.adoc[leveloffset=+1]


// [role="_additional-resources"]
// == Additional resources (or Next steps)

// * A bulleted list of links to other material closely related to the contents of the assembly, including xref links to other assemblies in your collection.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

ifdef::parent-context-of-preventing-devices-from-multipathing[:context: {parent-context-of-preventing-devices-from-multipathing}]
ifndef::parent-context-of-preventing-devices-from-multipathing[:!context:]
