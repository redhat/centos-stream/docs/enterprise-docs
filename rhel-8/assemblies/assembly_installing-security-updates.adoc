:_mod-docs-content-type: ASSEMBLY
:parent-context-of-installing-security-updates: {context}

[id="installing-security-updates_{context}"]
= Installing security updates

:context: installing-security-updates

In {ProductShortName}, you can install a specific security advisory and all available security updates. You can also configure the system to download and install security updates automatically.


ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
As a sysadmin, I need information about how security updates are applied to RHEL 8, including information about the overall process, entitlements, frequency, and everything I need to know to prepare for these updates. I also want information about reviewing updates that have already been applied – so I can ensure the security status of the systems I'm responsible for as well as confidently report on my systems whenever needed.
| Jira |
https://projects.engineering.redhat.com/browse/RHELPLAN-3724
| BZ |
BUGZILLA LINK
| SMEs |
Dave Russo <drusso@redhat.com>, Huzaifa Sidhpurwala <huzaifas@redhat.com>
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]


include::modules/security/proc_installing-all-available-security-updates.adoc[leveloffset=+1]

include::modules/security/proc_installing-a-security-update-provided-by-a-specific-advisory.adoc[leveloffset=+1]

include::modules/security/proc_installing-security-updates-automatically.adoc[leveloffset=+1]

////
[role="_additional-resources"]
== Additional resources
* See practices of securing workstations and servers in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/index[Security Hardening] document.
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/index[Security-Enhanced Linux] documentation.
////


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-installing-security-updates}
