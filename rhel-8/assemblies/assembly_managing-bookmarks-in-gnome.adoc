:_mod-docs-content-type: ASSEMBLY

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-managing-bookmarks-in-gnome: {context}]

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="managing-bookmarks-in-gnome_{context}"]
= Managing bookmarks in GNOME

:context: managing-bookmarks-in-gnome
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

In GNOME, you can edit the bookmarks that are displayed in applications and dialogs that manage files.

include::modules/desktop/con_bookmarks-in-gnome.adoc[leveloffset=+1]

include::modules/desktop/proc_adding-a-bookmark-in-files.adoc[leveloffset=+1]

include::modules/desktop/proc_adding-a-bookmark-for-all-users.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-managing-bookmarks-in-gnome[:context: {parent-context-of-managing-bookmarks-in-gnome}]
ifndef::parent-context-of-managing-bookmarks-in-gnome[:!context:]
