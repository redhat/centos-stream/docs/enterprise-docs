ifdef::context[:parent-context-of-enabling-ad-users-to-administer-idm: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="enabling-ad-users-to-administer-idm"]
endif::[]
ifdef::context[]
[id="enabling-ad-users-to-administer-idm_{context}"]
endif::[]
= Enabling AD users to administer IdM

:context: enabling-ad-users-to-administer-idm

////
The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID so you can include it multiple times in the same guide.
////

include::modules/identity-management/con_id-overrides-for-ad-users.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-id-overrides-to-enable-ad-users-to-administer-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-enable-ad-users-to-administer-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_verifying-that-an-ad-user-can-perform-correct-commands-in-the-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-enable-an-ad-user-to-administer-idm.adoc[leveloffset=+1]

ifdef::parent-context-of-enabling-ad-users-to-administer-idm[:context: {parent-context-of-enabling-ad-users-to-administer-idm}]
ifndef::parent-context-of-enabling-ad-users-to-administer-idm[:!context:]
