:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
// This is a topmost assembly.  You can find a reference
// to this assembly in the master.adoc file for this book:
// enterprise/titles/configuring-and-maintaining/configuring-and-managing-cloud-init-for-rhel-8/master.adoc

ifdef::context[:parent-context-of-configuring-cloud-init: {context}]

[id="configuring-cloud-init_{context}"]
= Configuring cloud-init

:context: configuring-cloud-init

By using `cloud-init`, you can perform a variety of configuration tasks.

// We are most-likely removing mention of KVM Guest images...  Red Hat KVM Guest images come with `cloud-init` installed and enabled. You can also install `cloud-init` on an ISO image.

Your `cloud-init` configuration can require that you add directives to the `cloud.cfg` file and the `cloud.cfg.d` directory. Alternatively, your specific data source might require that you add directives to files, such as a user data file and a metadata file. A data source might require that you upload your directives to an HTTP server. Check the requirements of your data source and add directives accordingly.

include::modules/cloud/cloud-init/proc_creating-a-VM-that-includes-cloud-init-for-a-nocloud-datasource.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_expiring-a-cloud-user-password-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_changing-a-default-user-name-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_setting-a-root-password-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_managing-red-hat-subscriptions-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init//proc_adding-users-and-user-options-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_running-first-boot-commands-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_adding-additional-sudoers-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_setting-up-a-static-networking-configuration-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_configuring-only-a-root-user-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_setting-up-storage-with-container-storage-setup-in-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_changing-the-system-locale-with-cloud-init.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/con_cloud-init-and-shell-scripts.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_preventing-cloud-init-from-updating-config-files.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_modifying-a-vm-created-from-a-kvm-guest-image-after-cloud-init-has-run.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_modifying-a-vm-for-a-specific-datasource-after-cloud-init-has-run.adoc[leveloffset=+1]

include::modules/cloud/cloud-init/proc_troubleshooting-cloud-init.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-configuring-cloud-init[:context: {parent-context-of-configuring-cloud-init}]
ifndef::parent-context-of-configuring-cloud-init[:!context:]
