:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-using-ansible-playbooks-to-manage-role-based-access-control-in-idm: {context}]

[id="using-ansible-playbooks-to-manage-role-based-access-control-in-idm_{context}"]
= Using Ansible playbooks to manage role-based access control in IdM

:context: using-ansible-playbooks-to-manage-role-based-access-control-in-idm

[role="_abstract"]
Role-based access control (RBAC) is a policy-neutral access-control mechanism defined around roles and privileges. The components of RBAC in {IPA} (IdM) are roles, privileges and permissions:

* *Permissions* grant the right to perform a specific task such as adding or deleting users, modifying a group, and enabling read-access.
* *Privileges* combine permissions, for example all the permissions needed to add a new user.
* *Roles* grant a set of privileges to users, user groups, hosts or host groups.

Especially in large companies, using RBAC can help create a hierarchical system of administrators with their individual areas of responsibility.

This chapter describes the following operations performed when managing RBAC using Ansible playbooks:


* xref:permissions-in-idm_{context}[Permissions in IdM]
* xref:default-managed-permissions_{context}[Default managed permissions]
* xref:privileges-in-idm_{context}[Privileges in IdM]
* xref:roles-in-idm_{context}[Roles in IdM]
* xref:predefined-roles-in-identity-management_{context}[Predefined roles in IdM]

//* xref:creating-an-ansible-inventory-file-for-idm_{context}[Preparing your Ansible control node for managing IdM]
* xref:using-ansible-to-ensure-an-idm-rbac-role-with-privileges-is-present_{context}[Using Ansible to ensure an IdM RBAC role with privileges is present]
* xref:using-ansible-to-ensure-an-idm-rbac-role-is-absent_{context}[Using Ansible to ensure an IdM RBAC role is absent]
* xref:using-ansible-to-ensure-that-a-group-of-users-is-assigned-to-an-idm-rbac-role_{context}[Using Ansible to ensure that a group of users is assigned to an IdM RBAC role]
* xref:using-ansible-to-ensure-that-specific-users-are-not-assigned-to-an-idm-rbac-role_{context}[Using Ansible to ensure that specific users are not assigned to an IdM RBAC role]
* xref:using-ansible-to-ensure-a-service-is-a-member-of-an-idm-rbac-role_{context}[Using Ansible to ensure a service is a member of an IdM RBAC role]
* xref:using-ansible-to-ensure-a-host-is-a-member-of-an-idm-rbac-role_{context}[Using Ansible to ensure a host is a member of an IdM RBAC role]
* xref:using-ansible-to-ensure-a-host-group-is-a-member-of-an-idm-rbac-role_{context}[Using Ansible to ensure a host group is a member of an IdM RBAC role]


include::modules/identity-management/con_permissions-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/ref_default-managed-permissions.adoc[leveloffset=+1]

include::modules/identity-management/con_privileges-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/con_roles-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/ref_predefined-roles-in-identity-management.adoc[leveloffset=+1]

//include::modules/identity-management/proc_creating-an-ansible-inventory-file-for-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-an-idm-rbac-role-with-privileges-is-present.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-an-idm-rbac-role-is-absent.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-that-a-group-of-users-is-assigned-to-an-idm-rbac-role.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-that-specific-users-are-not-assigned-to-an-idm-rbac-role.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-a-service-is-a-member-of-an-idm-rbac-role.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-a-host-is-a-member-of-an-idm-rbac-role.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-a-host-group-is-a-member-of-an-idm-rbac-role.adoc[leveloffset=+1]


ifdef::parent-context-of-using-ansible-playbooks-to-manage-role-based-access-control-in-idm[:context: {parent-context-of-using-ansible-playbooks-to-manage-role-based-access-control-in-idm}]
ifndef::parent-context-of-using-ansible-playbooks-to-manage-role-based-access-control-in-idm[:!context:]
