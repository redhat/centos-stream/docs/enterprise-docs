:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-using-idm-user-vaults-storing-and-retrieving-secrets: {context}]
[id="using-idm-user-vaults-storing-and-retrieving-secrets_{context}"]
= Using IdM user vaults: storing and retrieving secrets

:context: using-idm-user-vaults-storing-and-retrieving-secrets

[role="_abstract"]
This chapter describes how to use user vaults in {IPA}. Specifically, it describes how a user can store a secret in an IdM vault, and how the user can retrieve it. The user can do the storing and the retrieving from two different IdM clients.

.Prerequisites

* The Key Recovery Authority (KRA) Certificate System component has been installed on one or more of the servers in your IdM domain. For details, see xref:installing-the-key-recovery-authority-component-in-idm_vaults-in-idm[Installing the Key Recovery Authority in IdM].

include::modules/identity-management/proc_storing-a-secret-in-a-user-vault.adoc[leveloffset=+1]

include::modules/identity-management/proc_retrieving-a-secret-from-a-user-vault.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* See
ifdef::c-m-idm[]
xref:using-ansible-to-manage-idm-user-vaults-storing-and-retrieving-secrets_configuring-and-managing-idm[Using Ansible to manage IdM user vaults: storing and retrieving secrets].
endif::[]
ifndef::c-m-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/using-ansible-to-manage-idm-service-vaults-storing-and-retrieving-secrets_configuring-and-managing-idm[Using Ansible to manage IdM service vaults: storing and retrieving secrets].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/using-ansible-to-manage-idm-service-vaults-storing-and-retrieving-secrets_using-ansible-to-install-and-manage-identity-management[Using Ansible to manage IdM service vaults: storing and retrieving secrets].
endif::[]
endif::[]


ifdef::parent-context-of-using-idm-user-vaults-storing-and-retrieving-secrets[:context: {parent-context-of-using-idm-user-vaults-storing-and-retrieving-secrets}]
ifndef::parent-context-of-using-idm-user-vaults-storing-and-retrieving-secrets[:!context:]
