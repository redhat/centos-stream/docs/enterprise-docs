:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-managing-the-default-gateway-setting: {context}]

// Base the file name and the ID on the assembly title. For example:
// * file name: my-assembly-a.adoc
// * ID: [id="my-assembly-a"]
// * Title: = My assembly A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules-{context}"].
[id="managing-the-default-gateway-setting_{context}"]
= Managing the default gateway setting
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: managing-the-default-gateway-setting
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

[role="_abstract"]
The default gateway is a router that forwards network packets when no other route matches the destination of a packet. In a local network, the default gateway is typically the host that is one hop closer to the internet.



// The following include statements pull in the module files that comprise the assembly. Include any combination of concept, procedure, or reference modules required to cover the user story. You can also include other assemblies.

// include::modules/TEMPLATE_CONCEPT_explaining_a_concept.adoc[leveloffset=+1]
// [leveloffset=+1] ensures that when a module starts with a level-1 heading (= Heading), the heading will be interpreted as a level-2 heading (== Heading) in the assembly.

include::modules/networking/proc_setting-the-default-gateway-on-an-existing-connection-by-using-nmcli.adoc[leveloffset=+1]

include::modules/networking/proc_setting-the-default-gateway-on-an-existing-connection-by-using-the-nmcli-interactive-mode.adoc[leveloffset=+1]

include::modules/networking/proc_setting-the-default-gateway-on-an-existing-connection-by-using-nm-connection-editor.adoc[leveloffset=+1]

include::modules/networking/proc_setting-the-default-gateway-on-an-existing-connection-by-using-control-center.adoc[leveloffset=+1]

include::modules/networking/proc_setting-the-default-gateway-on-an-existing-connection-by-using-nmstatectl.adoc[leveloffset=+1]

include::modules/networking/proc_setting-the-default-gateway-on-an-existing-connection-by-using-the-network-rhel-system-role.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 8]
include::modules/networking/proc_setting-the-default-gateway-on-an-existing-connection-when-using-the-legacy-network-scripts.adoc[leveloffset=+1]
endif::[]

include::modules/networking/con_how-networkmanager-manages-multiple-default-gateways.adoc[leveloffset=+1]

include::modules/networking/proc_configuring-networkmanager-to-avoid-using-a-specific-profile-to-provide-a-default-gateway.adoc[leveloffset=+1]

include::modules/networking/proc_fixing-unexpected-routing-behavior-due-to-multiple-default-gateways.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-managing-the-default-gateway-setting[:context: {parent-context-of-managing-the-default-gateway-setting}]
ifndef::parent-context-of-managing-the-default-gateway-setting[:!context:]
