:_mod-docs-content-type: ASSEMBLY
:parent-context-of-invalidating-a-specific-group-of-related-certificates-quickly: {context}

[id="invalidating-a-specific-group-of-related-certificates-quickly_{context}"]
= Invalidating a specific group of related certificates quickly

:context: invalidating-a-specific-group-of-related-certificates-quickly
:invalidating-a-specific-group-of-certs:

[role="_abstract"]
As a system administrator, if you want to be able to invalidate a specific group of related certificates quickly:

* Design your applications so that they only trust certificates that were issued by a specific lightweight {IPA} (IdM) sub-CA. Afterwards, you will be able to invalidate all these certificates by only revoking the certificate of the {IPA} (IdM) sub-CA that issued these certificates. For details on how to create and use a lightweight sub-CA in IdM, see xref:restricting-an-application-to-trust-only-a-subset-of-certificates_{parent-context-of-invalidating-a-specific-group-of-related-certificates-quickly}[Invalidating a specific group of related certificates quickly].

* To ensure that all the certificates that have been issued by the to-be-revoked IdM sub-CA are immediately invalid, configure applications that rely on such certificates to use the IdM OCSP responders. For example, to configure the Firefox browser to use OCSP responders, make sure that the `Query OCSP responder servers to confirm the current validity of certificates` checkbox is checked in Firefox Preferences.

+
In IdM, the certificate revocation list (CRL) is updated every four hours.
d
To invalidate all the certificates issued by an IdM sub-CA, xref:revoking-certificates-with-the-integrated-IdM-CAs_managing-the-validity-of-certificates-in-idm[revoke the IdM sub-CA certificate]. In addition, xref:disabling-CA-ACLs-in-IdM-CLI_{context}[disable the relevant CA ACLs], and consider xref:disabling-an-IdM-sub-CA_{context}[disabling the IdM sub-CA]. Disabling the sub-CA prevents the sub-CA from issuing new certificates, but allows Online Certificate Status Protocol (OCSP) responses to be produced for previously issued certificates because the sub-CA's signing keys are retained.

[IMPORTANT]
====
Do not delete the sub-CA if you use OCSP in your environment. Deleting the sub-CA deletes the signing keys of the sub-CA, preventing production of OCSP responses for certificates issued by that sub-CA.

The only scenario when deleting a sub-CA is preferable to disabling it is when you want to create a new sub-CA with the same Subject distinguished name (DN) but a new signing key.
====


include::modules/identity-management/proc_disabling-CA-ACLs-in-IdM-CLI.adoc[leveloffset=+1]

include::modules/identity-management/proc_disabling-an-IdM-sub-CA.adoc[leveloffset=+1]

:context: {parent-context-of-invalidating-a-specific-group-of-related-certificates-quickly}
