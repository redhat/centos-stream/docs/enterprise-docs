:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-writing-a-custom-selinux-policy: {context}]


[id="writing-a-custom-selinux-policy_{context}"]
= Writing a custom SELinux policy

:context: writing-a-custom-selinux-policy

To run your applications confined by SELinux, you must write and use a custom policy.

include::modules/security/con_custom-selinux-policies-and-related-tools.adoc[leveloffset=+1]

include::modules/security/proc_creating-and-enforcing-an-selinux-policy-for-a-custom-application.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* link:http://redhatgov.io/workshops/selinux_policy/[SELinux Policy Workshop] (Red&nbsp;Hat | Public Sector)

ifdef::parent-context-of-writing-a-custom-selinux-policy[:context: {parent-context-of-writing-a-custom-selinux-policy}]
ifndef::parent-context-of-writing-a-custom-selinux-policy[:!context:]
