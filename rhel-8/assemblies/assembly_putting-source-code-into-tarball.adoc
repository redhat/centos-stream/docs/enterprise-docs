
ifdef::context[:parent-context-of-putting-source-code-into-tarball: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="putting-source-code-into-tarball"]
endif::[]
ifdef::context[]
[id="putting-source-code-into-tarball_{context}"]
endif::[]
= Creating a source code archive for distribution

:context: putting-source-code-into-tarball

An archive file is a file with the `.tar.gz` or `.tgz` suffix. Putting source code into the archive is a common way to release the software to be later packaged for distribution.

include::modules/core-services/proc_putting-the-bello-program-into-tarball.adoc[leveloffset=+1]

include::modules/core-services/proc_putting-the-pello-program-into-tarball.adoc[leveloffset=+1]

include::modules/core-services/proc_putting-the-cello-program-into-tarball.adoc[leveloffset=+1]


ifdef::parent-context-of-putting-source-code-into-tarball[:context: {parent-context-of-putting-source-code-into-tarball}]
ifndef::parent-context-of-putting-source-code-into-tarball[:!context:]

