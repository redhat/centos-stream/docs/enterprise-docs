:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_setting-up-a-remote-diskless-system.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-setting-up-a-remote-diskless-system: {context}

// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.
[id='setting-up-a-remote-diskless-system_{context}']
= Setting up a remote diskless system

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: setting-up-a-remote-diskless-system

In a network environment, you can setup multiple clients with the identical configuration by deploying a remote diskless system. By using current {RHEL} server version, you can save the cost of hard drives for these clients as well as configure the gateway on a separate server.

The following diagram describes the connection of a diskless client with the server through Dynamic Host Configuration Protocol (DHCP) and Trivial File Transfer Protocol (TFTP) services.

[#remote-diskless-system]
.Remote diskless system settings diagram
image::../images/remote-diskless-system-schema.png[Remote diskless system settings diagram]

include::modules/filesystems-and-storage/proc_preparing-environments-for-the-remote-diskless-system.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_configuring-a-tftp-service-for-diskless-client.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_configuring-a-dhcp-for-diskless-clients.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_configuring-an-exported-file-system-for-diskless-clients.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_re-configuring-a-remote-diskless-system.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_troubleshooting-common-issues-with-loading-a-remote-diskless-system.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-setting-up-a-remote-diskless-system}
