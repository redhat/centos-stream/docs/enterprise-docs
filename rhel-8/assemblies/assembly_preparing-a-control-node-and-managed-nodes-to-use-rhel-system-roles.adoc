ifdef::context[:parent-context-of-assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles"]
endif::[]
ifdef::context[]
[id="assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_{context}"]
endif::[]
= Preparing a control node and managed nodes to use {RHELSystemRoles}s

:context: assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles
Before you can use individual {RHELSystemRoles}s to manage services and settings, you must prepare the control node and managed nodes.


include::modules/system-roles/proc_preparing-a-control-node.adoc[leveloffset=+1]


include::modules/system-roles/proc_preparing-a-managed-node.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[:context: {parent-context-of-assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles}]
ifndef::parent-context-of-assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles[:!context:]
