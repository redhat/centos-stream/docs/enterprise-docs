:_newdoc-version: 2.18.2
:_template-generated: 2024-06-12

ifdef::context[:parent-context-of-preparing-ibm-power-systems: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="preparing-ibm-power-systems"]
endif::[]
ifdef::context[]
[id="preparing-ibm-power-systems_{context}"]
endif::[]
= Preparing IBM Power Systems

:context: preparing-ibm-power-systems

include::assembly_preparing-a-rhel-installation-on-ibm-power-system-lc-servers.adoc[leveloffset=+1]

include::assembly_preparing-a-rhel-installation-on-ibm-power-system-ac-servers.adoc[leveloffset=+1]

include::assembly_preparing-a-rhel-installation-on-ibm-power-system-l-servers.adoc[leveloffset=+1]

ifdef::parent-context-of-preparing-ibm-power-systems[:context: {parent-context-of-preparing-ibm-power-systems}]
ifndef::parent-context-of-preparing-ibm-power-systems[:!context:]

