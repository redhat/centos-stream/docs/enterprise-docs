:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-replicating-mariadb-with-galera: {context}]

[id="replicating-mariadb-with-galera_{context}"]
= Replicating MariaDB with Galera

:context: replicating-mariadb-with-galera

This section describes how to replicate a *MariaDB* database using the Galera solution on {RHEL} {ProductNumber}.

include::modules/core-services/con_introduction-to-galera-replication.adoc[leveloffset=+1]

include::modules/core-services/con_components-to-build-mariadb-galera-cluster.adoc[leveloffset=+1]

include::modules/core-services/proc_configuring-mariadb-galera-cluster.adoc[leveloffset=+1]

include::modules/core-services/proc_adding-a-new-node-mariadb-galera-cluster.adoc[leveloffset=+1]

include::modules/core-services/proc_restarting-mariadb-galera-cluster.adoc[leveloffset=+1]

:context: {parent-context-of-replicating-mariadb-with-galera}

ifdef::parent-context-of-replicating-mariadb-with-galera[:context: {parent-context-of-replicating-mariadb-with-galera}]
ifndef::parent-context-of-replicating-mariadb-with-galera[:!context:]
