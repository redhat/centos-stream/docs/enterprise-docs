:_mod-docs-content-type: ASSEMBLY
:parent-context-of-install-replica: {context}

[id="installing-an-ipa-replica_{context}"]

= Installing an IdM replica

:assembly: installing-replica
:context: install-replica

[role="_abstract"]
The following sections describe how to install an {IPA} (IdM) replica interactively, by using the command line (CLI).
The replica installation process copies the configuration of the existing server and installs the replica based on that configuration.

[NOTE]
====
See xref:installing-an-Identity-Management-replica-using-an-ansible-playbook_installing-identity-management[Installing an Identity Management server using an Ansible playbook]. Use Ansible roles to consistently install and customize multiple replicas. 

Interactive and non-interactive methods that do not use Ansible are useful in topologies where, for example, the replica preparation is delegated to a user or a third party. You can also use these methods in geographically distributed topologies where you do not have access from the Ansible controller node.
====

.Prerequisites

* You are installing one IdM replica at a time. The installation of multiple replicas at the same time is not supported.
* Ensure your system is xref:preparing-the-system-for-ipa-replica-installation_installing-identity-management[prepared for IdM replica installation].

+
IMPORTANT: If this preparation is not performed, installing an IdM replica will fail.

include::modules/identity-management/proc_installing-an-idm-replica-with-integrated-dns-and-a-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-an-idm-replica-with-integrated-dns-and-no-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-an-idm-replica-without-integrated-dns-and-with-a-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-an-idm-replica-without-integrated-dns-and-without-a-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-an-idm-hidden-replica.adoc[leveloffset=+1]

include::modules/identity-management/proc_testing-an-ipa-replica.adoc[leveloffset=+1]

include::modules/identity-management/ref_connection-requests-an-ipa-replica.adoc[leveloffset=+1]

:context: {parent-context-of-install-replica}
