ifdef::context[:parent-context-of-enabling-multipathing-on-nvme-devices: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="enabling-multipathing-on-nvme-devices"]
endif::[]
ifdef::context[]
[id="enabling-multipathing-on-nvme-devices_{context}"]
endif::[]
= Enabling multipathing on NVMe devices

:context: enabling-multipathing-on-nvme-devices

You can multipath Non-volatile Memory Express™ (NVMe™) devices that are connected to your system over a fabric transport, such as Fibre Channel (FC). You can select between multiple multipathing solutions.

// == Prerequisites

// * NVMe devices are connected to your system over a fabric transport.
// +
// For more information, see link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/overview-of-nvme-over-fabric-devices_managing-storage-devices[Overview of NVMe over fabric devices].


include::modules/filesystems-and-storage/con_native-nvme-multipathing-and-dm-multipath.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 9]
include::modules/filesystems-and-storage/proc_enabling-dm-multipath-on-nvme-devices.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_enabling-native-nvme-multipathing.adoc[leveloffset=+1]

endif::[]

ifeval::[{ProductNumber} == 8]
include::modules/filesystems-and-storage/proc_enabling-native-nvme-multipathing.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_enabling-dm-multipath-on-nvme-devices.adoc[leveloffset=+1]

endif::[]


ifdef::parent-context-of-enabling-multipathing-on-nvme-devices[:context: {parent-context-of-enabling-multipathing-on-nvme-devices}]
ifndef::parent-context-of-enabling-multipathing-on-nvme-devices[:!context:]