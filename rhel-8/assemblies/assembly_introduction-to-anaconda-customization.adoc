:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-introduction-to-anaconda-customization: {context}]

// Base the file name and the ID on the assembly title. For example:
// * file name: my-assembly-a.adoc
// * ID: [id="my-assembly-a"]
// * Title: = My assembly A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules-{context}"].
[id="introduction-to-anaconda-customization_{context}"]
= Introduction to Anaconda customization
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: introduction-to-anaconda-customization

include::modules/anaconda-customization/con_introduction-to-anaconda-customization.adoc[leveloffset=+1]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-introduction-to-anaconda-customization[:context: {parent-context-of-introduction-to-anaconda-customization}]
ifndef::parent-context-of-introduction-to-anaconda-customization[:!context:]

