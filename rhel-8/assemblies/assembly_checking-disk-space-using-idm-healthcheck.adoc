:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_checking-disk-space-using-the-idm-healthcheck-tool.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-checking-disk-space-using-idm-healthcheck: {context}

[id='checking-disk-space-using-idm-healthcheck_{context}']
= Checking disk space using IdM Healthcheck

:context: checking-disk-space-using-idm-healthcheck

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.

[role="_abstract"]
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

You can monitor the Identity Management server's free disk space using the Healthcheck tool.

For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/configuring_and_managing_identity_management#healthcheck-in-idm_collecting-idm-healthcheck-information[Healthcheck in IdM].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/using_idm_healthcheck_to_monitor_your_idm_environment/index#healthcheck-in-idm_installing-and-running-the-ipa-healthcheck-tool[Healthcheck in IdM].
endif::[]


// Only display the following if this is the RHEL 8 Installation guide
ifeval::[{ProductNumber} == 8]
.Prerequisites

* The Healthcheck tool is only available on RHEL 8.1 and newer.
endif::[]

include::modules/identity-management/con_disk-space-healthcheck-test.adoc[leveloffset=+1]

include::modules/identity-management/proc_screening-disk-space-using-the-healthcheck-tool.adoc[leveloffset=+1]


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-checking-disk-space-using-idm-healthcheck}
