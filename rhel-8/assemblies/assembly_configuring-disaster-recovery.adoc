:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly-configuring-disaster-recovery: {context}
  
[id='assembly_configuring-disaster-recovery-{context}']
= Configuring disaster recovery clusters
:context: configuring-disaster-recovery

[role="_abstract"]
One method of providing disaster recovery for a high availability cluster is to configure two clusters. You can then configure one cluster as your primary site cluster, and the second cluster as your disaster recovery cluster.

In normal circumstances, the primary cluster is running resources in production mode. The disaster recovery cluster has all the resources configured as well and is either running them in demoted mode or not at all. For example, there may be a database running in the primary cluster in promoted mode and running in the disaster recovery cluster in demoted mode. The database in this setup would be configured so that data is synchronized from the primary to disaster recovery site. This is done through the database configuration itself rather than through the `pcs` command interface.

When the primary cluster goes down, users can use the `pcs` command interface to  manually fail the resources over to the disaster recovery site. They can then log in to the disaster site and promote and start the resources there. Once the primary cluster has recovered, users can use the `pcs` command interface to manually move resources back to the primary site.

ifeval::[{ProductNumber} == 8]
As of Red Hat Enterprise Linux 8.2, you can use the `pcs` command
endif::[]
ifeval::[{ProductNumber} == 9]
You can use the `pcs` command
endif::[]
to display the status of both the primary and the disaster recovery site cluster from a single node on either site.

include::modules/high-availability/ref_recovery-considerations.adoc[leveloffset=+1]

include::modules/high-availability/proc_disaster-recovery-display.adoc[leveloffset=+1]

:context: {parent-context-of-assembly-configuring-disaster-recovery}



