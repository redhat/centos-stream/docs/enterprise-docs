:_mod-docs-content-type: ASSEMBLY
:parent-context-of-creating-custom-circular-buffers-to-collect-specific-data-with-perf: {context}

[id="creating-custom-circular-buffers-to-collect-specific-data-with-perf_{context}"]
= Monitoring processes for performance bottlenecks using perf circular buffers

:context: assembly_creating-custom-circular-buffers-to-collect-specific-data-with-perf

[role="_abstract"]
You can create circular buffers that take event-specific snapshots of data with the `perf` tool in order to monitor performance bottlenecks in specific processes or parts of applications running on your system. In such cases, `perf` only writes data to a `perf.data` file for later analysis if a specified event is detected.

include::modules/performance/con_circular-buffers-and-event-specific-snapshots.adoc[leveloffset=+1]

include::modules/performance/proc_using-perf-to-create-custom-circular-buffers-that-perform-event-specific-snapshots.adoc[leveloffset=+1]


:context: {parent-context-of-creating-custom-circular-buffers-to-collect-specific-data-with-perf}
