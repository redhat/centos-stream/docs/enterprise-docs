:_mod-docs-content-type: ASSEMBLY

:parent-context-of-using-the-system-wide-cryptographic-policies: {context}

[id="using-the-system-wide-cryptographic-policies_{context}"]
= Using system-wide cryptographic policies

:context: using-the-system-wide-cryptographic-policies

[role="_abstract"]
The system-wide cryptographic policies is a system component that configures the core cryptographic subsystems, covering the TLS, IPsec, SSH, DNSSec, and Kerberos protocols. It provides a small set of policies, which the administrator can select.

include::modules/security/con_system-wide-crypto-policies.adoc[leveloffset=+1]

include::modules/security/proc_changing-the-system-wide-cryptographic-policy.adoc[leveloffset=+1]

include::modules/security/proc_switching-the-system-wide-crypto-policy-to-mode-compatible-with-previous-systems.adoc[leveloffset=+1]

include::modules/security/proc_setting-up-system-wide-crypto-policies-in-the-web-console.adoc[leveloffset=+1]

include::modules/security/ref_excluding-an-application-from-following-the-system-wide-crypto-policies.adoc[leveloffset=+1]

include::modules/security/proc_customizing-system-wide-cryptographic-policies-with-policy-modifiers.adoc[leveloffset=+1]

include::modules/security/proc_disabling-sha-1-by-customizing-a-system-wide-cryptographic-policy.adoc[leveloffset=+1]

include::modules/security/proc_creating-and-setting-a-custom-system-wide-cryptographic-policy.adoc[leveloffset=+1]

include::modules/security/proc_setting-a-custom-cryptographic-policy-using-the-crypto-policies-system-role.adoc[leveloffset=+1]

[id='related-information-{context}']
[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/articles/3666211[System-wide crypto policies in RHEL 8] and link:https://access.redhat.com/articles/3642912[Strong crypto defaults in RHEL 8 and deprecation of weak crypto algorithms] Knowledgebase articles

:context: {parent-context-of-using-the-system-wide-cryptographic-policies}
