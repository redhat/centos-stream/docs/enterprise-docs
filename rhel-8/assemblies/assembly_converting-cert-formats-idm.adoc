:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}


[id="convert-cert-formats-idm_{context}"]
= Converting certificate formats to work with IdM

:context: convert-cert-formats-idm

[role="_abstract"]
This user story describes how to make sure that you as an IdM system administrator are using the correct format of a certificate with specific IdM commands. This is useful, for example, in the following situations:

* You are loading an external certificate into a user profile. For details, see xref:assembly_converting-an-external-certificate-to-load-into-an-idm-user-account_convert-cert-formats-idm[Converting an external certificate to load into an IdM user account].

* You are using an external CA certificate when link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#conf-idm-server-for-smart-card-auth_configuring-idm-for-smart-card-auth[configuring the IdM server for smart card authentication] or link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication#conf-idm-client-for-smart-card-auth_configuring-idm-for-smart-card-auth[configuring the IdM client for smart card authentication] so that users can authenticate to IdM using smart cards with certificates on them that have been issued by the external certificate authority.

* You are exporting a certificate from an NSS database into a pkcs #12 format that includes both the certificate and the private key. For details, see xref:proc_exporting-a-certificate-and-private-key-from-an-nss-database-into-a-pkcs-12-file_assembly_preparing-to-load-a-certificate-into-the-browser[Exporting a certificate and private key from an NSS database into a PKCS #12 file].
// prepare-load-cert-into-browser-idm_convert-cert-formats-idm
//step 7 of requesting-and-exporting-a-user-certificate_dc-web-ui-auth

////
Content:
- Loading an external certificate into a user profile in IdM
- Using a certificate with ipa-advise config-server-for-smart-card-auth
////

include::modules/identity-management/con_cert-types-idm.adoc[leveloffset=+1]

//include::modules/identity-management/proc_load-cert-into-user-profile-idm.adoc[leveloffset=+1]

include::assembly_converting-an-external-certificate-to-load-into-an-idm-user-account.adoc[leveloffset=+1]

include::assembly_preparing-to-load-a-certificate-into-the-browser.adoc[leveloffset=+1]

//include::modules/identity-management/proc_load-cert-into-browser-idm.adoc[leveloffset=+1]

include::modules/identity-management/ref_other-cert-commands-idm.adoc[leveloffset=+1]

:context: {parent-context}
