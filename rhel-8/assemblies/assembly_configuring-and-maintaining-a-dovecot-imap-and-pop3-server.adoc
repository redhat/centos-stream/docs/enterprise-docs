ifdef::context[:parent-context-of-configuring-and-maintaining-a-dovecot-imap-and-pop3-server: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-and-maintaining-a-dovecot-imap-and-pop3-server"]
endif::[]
ifdef::context[]
[id="configuring-and-maintaining-a-dovecot-imap-and-pop3-server_{context}"]
endif::[]
= Configuring and maintaining a Dovecot IMAP and POP3 server

:context: configuring-and-maintaining-a-dovecot-imap-and-pop3-server

Dovecot is a high-performance mail delivery agent (MDA) with a focus on security. You can use IMAP or POP3-compatible email clients to connect to a Dovecot server and read or download emails.

Key features of Dovecot:

* The design and implementation focuses on security
* Two-way replication support for high availability to improve the performance in large environments
* Supports the high-performance `dbox` mailbox format, but also `mbox` and `Maildir` for compatibility reasons
* Self-healing features, such as fixing broken index files
* Compliance with the IMAP standards
* Workaround support to bypass bugs in IMAP and POP3 clients



include::assembly_setting-up-a-dovecot-server-with-pam-authentication.adoc[leveloffset=+1]

include::assembly_setting-up-a-dovecot-server-with-ldap-authentication.adoc[leveloffset=+1]

include::assembly_setting-up-a-dovecot-server-with-mariadb-sql-authentication.adoc[leveloffset=+1]

include::modules/core-services/proc_configuring-replication-between-two-dovecot-servers.adoc[leveloffset=+1]

include::modules/core-services/proc_automatically-subscribing-users-to-imap-mailboxes.adoc[leveloffset=+1]

include::modules/core-services/proc_configuring-an-lmtp-socket-and-lmtps-listener.adoc[leveloffset=+1]

include::modules/core-services/proc_disabling-the-imap-or-pop3-service-in-dovecot.adoc[leveloffset=+1]

include::modules/core-services/proc_enabling-server-side-email-filtering-using-sieve-on-a-dovecot-imap-server.adoc[leveloffset=+1]

include::modules/core-services/ref_how-dovecot-processes-configuration-files.adoc[leveloffset=+1]


ifdef::parent-context-of-configuring-and-maintaining-a-dovecot-imap-and-pop3-server[:context: {parent-context-of-configuring-and-maintaining-a-dovecot-imap-and-pop3-server}]
ifndef::parent-context-of-configuring-and-maintaining-a-dovecot-imap-and-pop3-server[:!context:]

