ifdef::context[:parent-context-of-assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image"]
endif::[]
ifdef::context[]
[id="assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image_{context}"]
endif::[]
= Pushing a container to a registry and embedding it into an image

:context: assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image

With RHEL image builder, you can build security hardened images using the OpenSCAP tool. You can take advantage of the support for container customization in the blueprints to create a container and embed it directly into the image you create. 

include::modules/composer/con_the-container-image-builder-customization.adoc[leveloffset=+1]

include::modules/composer/con_the-container-registry-credentials.adoc[leveloffset=+1]

include::modules/composer/proc_pushing-a-container-artifact-directly-to-a-container-registry.adoc[leveloffset=+1]

include::modules/composer/proc_building-an-image-and-pulling-the-container-into-the-image.adoc[leveloffset=+1]


////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image[:context: {parent-context-of-assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image}]
ifndef::parent-context-of-assembly_pushing-a-container-to-a-register-and-embedding-it-into-a-image[:!context:]

