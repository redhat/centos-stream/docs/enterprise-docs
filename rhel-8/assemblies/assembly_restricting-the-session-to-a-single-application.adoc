:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_restricting-the-session-to-a-single-application: {context}]

ifndef::context[]
[id="assembly_restricting-the-session-to-a-single-application"]
endif::[]
ifdef::context[]
[id="assembly_restricting-the-session-to-a-single-application_{context}"]
endif::[]
= Restricting the session to a single application

:context: assembly_restricting-the-session-to-a-single-application

[role="_abstract"]
You can start the GNOME session in single-application mode, also known as kiosk mode. In this session, GNOME displays only a full-screen window of the application that you have selected.

// == Prerequisites

// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.
// * X is installed. For information about installing X, see <link>.
// * You can log in to X with administrator privileges.

include::modules/desktop/con_single-application-mode.adoc[leveloffset=+1]

include::modules/desktop/proc_enabling-single-application-mode.adoc[leveloffset=+1]

// [role="_additional-resources"]
// == Additional resources (or Next steps)
// * A bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

ifdef::parent-context-of-assembly_restricting-the-session-to-a-single-application[:context: {parent-context-of-assembly_restricting-the-session-to-a-single-application}]
ifndef::parent-context-of-assembly_restricting-the-session-to-a-single-application[:!context:]

