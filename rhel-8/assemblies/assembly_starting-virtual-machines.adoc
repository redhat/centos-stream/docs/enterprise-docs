:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// getting-started-with-virtualization-in-rhel-8

:parent-context-of-assembly_starting-virtual-machines: {context}

[id='assembly_starting-virtual-machines_{context}']
= Starting virtual machines

:context: assembly_starting-virtual-machines

To start a virtual machine (VM) in {ProductShortName} {ProductNumber}, you can use xref:starting-a-virtual-machine-using-the-command-line-interface_assembly_starting-virtual-machines[the command line] or xref:powering-up-vms-using-the-rhel-web-console_assembly_starting-virtual-machines[the web console GUI].

.Prerequisites

* Before a VM can be started, it must be created and, ideally, also installed with an OS. For instruction to do so, see
ifeval::[{ProductNumber} == 8]
xref:assembly_creating-virtual-machines_virt-getting-started[Creating virtual machines].
endif::[]
ifeval::[{ProductNumber} == 9]
xref:assembly_creating-virtual-machines_configuring-and-managing-virtualization[Creating virtual machines].
endif::[]

include::modules/virtualization/proc_starting-a-virtual-machine-using-the-command-line-interface.adoc[leveloffset=+1]

include::modules/virtualization/proc_powering-up-vms-using-the-rhel-8-web-console.adoc[leveloffset=+1]

include::modules/virtualization/proc_starting-virtual-machines-automatically-when-the-host-starts.adoc[leveloffset=+1]

:context: {parent-context-of-assembly_starting-virtual-machines}
