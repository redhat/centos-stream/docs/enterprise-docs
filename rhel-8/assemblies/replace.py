import re
import glob

def nahrad(expr):
    files = glob.glob(expr)
    for i in range(len(files)):
        print(files[i])
        nahradREAL(files[i])


def nahradREAL(somefileTXT):
    text = ''.join(open(somefileTXT).readlines())
    #textNEW = text.replace("xref:using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line","link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_and_managing_identity_management/index#using-kinit-to-log-in-to-ipa_logging-in-to-ipa-from-the-command-line")
    #textNEW = text.replace("\* You have installed the IdM replicas and clients on the managed nodes by following the link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumber}/html/using_ansible_to_install_and_manage_identity_management/index[official IdM documentation].","The target node, that is the node on which the `ansible-freeipa` module is executed, is part of the IdM domain as an IdM client, server or replica.")
    textNEW = text.replace("Ansible version 2.14","Ansible version 2.15")
    outfile = open(somefileTXT+"updated", 'w')
    outfile.write(textNEW)
    outfile.close()

if __name__ == "__main__":
    # execute only if run as a script
    nahrad("*.adoc")