:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-configuring-nat-using-nftables: {context}]

[id="configuring-nat-using-nftables_{context}"]
= Configuring NAT using nftables

:context: configuring-nat-using-nftables

[role="_abstract"]
With `nftables`, you can configure the following network address translation (NAT) types:

* Masquerading
* Source NAT (SNAT)
* Destination NAT (DNAT)
* Redirect

[IMPORTANT]
====
You can only use real interface names in `iifname` and `oifname` parameters, and alternative names (`altname`) are not supported.
====


include::modules/packet-filtering/con_nat-types.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_configuring-masquerading-using-nftables.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_configuring-source-nat-using-nftables.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_configuring-destination-nat-using-nftables.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_configuring-a-redirect-using-nftables.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_configuring-flowtable-using-nftables.adoc[leveloffset=+1]


ifdef::parent-context-of-configuring-nat-using-nftables[:context: {parent-context-of-configuring-nat-using-nftables}]
ifndef::parent-context-of-configuring-nat-using-nftables[:!context:]

