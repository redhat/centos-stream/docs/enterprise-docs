:_mod-docs-content-type: ASSEMBLY
// This Assembly is included in titles/installing/installing-identity-management-and-access-control/,
// which has a context of "installing-identity-management"

:parent-context-of-preparing-the-system-for-ipa-client-installation: {context}

// Commented out blank parent-context
//:parent-context: {context}

[id="preparing-the-system-for-ipa-client-installation_{context}"]
= Preparing the system for IdM client installation

:context: preparing-the-system-for-ipa-client-installation

[role="_abstract"]
This chapter describes the conditions your system must meet to install an {IPA} (IdM) client.

include::modules/identity-management/ref_supported-versions-of-rhel-for-installing-idm-clients.adoc[leveloffset=+1]

include::modules/identity-management/con_dns-requirements-for-idm-clients.adoc[leveloffset=+1]

include::modules/identity-management/con_port-requirements-for-ipa-clients.adoc[leveloffset=+1]

include::modules/identity-management/con_ipv6-requirements-for-idm-clients.adoc[leveloffset=+1]

//include::modules/identity-management/fips-support-in-ipa.adoc[leveloffset=+1]

//include::modules/identity-management/nscd-support-in-ipa.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-idm-client-packages-from-the-idm-client-stream.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-idm-client-packages-from-the-idm-dl1-stream.adoc[leveloffset=+1]

:context: {parent-context-of-preparing-the-system-for-ipa-client-installation}
