:_mod-docs-content-type: ASSEMBLY
:parent-context-of-creating-cloud-images-with-composer: {context}

[id="creating-cloud-images-with-composer_{context}"]
= Preparing and uploading cloud images by using RHEL image builder

:context: creating-cloud-images-with-composer

ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

[role="_abstract"]
RHEL image builder can create custom system images ready for use on various cloud platforms. To use your customized RHEL system image in a cloud, create the system image with RHEL image builder by using the chosen output type, configure your system for uploading the image, and upload the image to your cloud account.
You can push customized image clouds through the `Image Builder` application in the RHEL web console, available for a subset of the service providers that we support, such as *AWS* and *Microsoft Azure* clouds. See xref:pushing-ami-images-to-aws-cloud_creating-cloud-images-with-composer[Creating and automatically uploading images directly to AWS Cloud AMI] and xref:pushing-vhd-imaged-to-azure-cloud_creating-cloud-images-with-composer[Creating and automatically uploading VHD images directly to Microsoft Azure cloud].

== Preparing and uploading AMI images to AWS 
You can create custom images and can update them, either manually or automatically, to the AWS cloud with RHEL image builder. 

include::modules/composer/proc_preparing-for-uploading-aws-ami-images.adoc[leveloffset=+2]

include::modules/composer/proc_uploading-an-ami-image-to-aws.adoc[leveloffset=+2]

include::modules/composer/proc_pushing-ami-images-to-aws-cloud.adoc[leveloffset=+2]

== Preparing and uploading VHD images to Microsoft Azure

You can create custom images and can update them, either manually or automatically, to the Microsoft Azure cloud with RHEL image builder.  

include::modules/composer/proc_preparing-for-uploading-azure-vhd-images.adoc[leveloffset=+2]

include::modules/composer/proc_uploading-vhd-images-to-azure.adoc[leveloffset=+2]

include::modules/composer/proc_pushing-vhd-imaged-to-azure-cloud.adoc[leveloffset=+2]

ifeval::[{ProductNumber} == 9]
== Preparing and uploading VMDK custom images to vSphere

You can create custom images and can update them, either manually or automatically, to the VMware vSphere cloud with RHEL image builder.  

include::modules/composer/proc_creating-a-customized-rhel-vmdk-system-image-by-using-rhel-image-builder.adoc[leveloffset=+2]
endif::[]

include::modules/composer/proc_uploading-vmdk-images-to-vsphere.adoc[leveloffset=+2]

include::modules/composer/proc_pushing-vmware-images-to-vsphere.adoc[leveloffset=+2]

== Preparing and uploading custom GCE images to GCP

You can create custom images and then automatically update them to the Oracle Cloud Infrastructure (OCI) instance with RHEL image builder.  

include::assembly_uploading-images-to-gcp-with-image-builder.adoc[leveloffset=+2]

== Preparing and uploading custom images directly to OCI

You can create custom images and then automatically update them to the Oracle Cloud Infrastructure (OCI) instance with RHEL image builder.  

include::modules/composer/proc_pushing-customized-images-to-oracle-cloud-infrastructure.adoc[leveloffset=+2]

== Preparing and uploading customized QCOW2 images directly to OpenStack

You can create custom `.qcow2` images with RHEL image builder, and manually upload them to the OpenStack cloud deployments.  

include::modules/composer/proc_uploading-and-qcow2-image-on-openstack.adoc[leveloffset=+2]

== Preparing and uploading customized RHEL images to the Alibaba Cloud

You can upload a customized `.ami` images that you created by using RHEL image builder to the Alibaba Cloud.

include::modules/composer/proc_preparing-uploading-images-to-alibaba.adoc[leveloffset=+2]

include::modules/composer/proc_uploading-images-to-alibaba.adoc[leveloffset=+2]

include::modules/composer/proc_importing-images-to-alibaba.adoc[leveloffset=+2]

include::modules/composer/proc_creating-an-instance-on-alibaba.adoc[leveloffset=+2]


:context: {parent-context-of-creating-cloud-images-with-composer}
