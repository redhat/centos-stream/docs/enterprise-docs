ifdef::context[:parent-context-of-managing-file-system-permissions: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="managing-file-system-permissions"]
endif::[]
ifdef::context[]
[id="managing-file-system-permissions_{context}"]
endif::[]
= Managing file system permissions

:context: managing-file-system-permissions

[role="_abstract"]
File system permissions control the ability of user and group accounts to read, modify, and execute the contents of the files and to enter directories. Set permissions carefully to protect your data against unauthorized access.


include::assembly_managing-file-permissions.adoc[leveloffset=+1]

include::assembly_managing-access-control-list.adoc[leveloffset=+1]

include::assembly_managing-the-umask.adoc[leveloffset=+1]


ifdef::parent-context-of-managing-file-system-permissions[:context: {parent-context-of-managing-file-system-permissions}]
ifndef::parent-context-of-managing-file-system-permissions[:!context:]

