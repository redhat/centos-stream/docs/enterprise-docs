:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly-configuring-cluster-resources: {context}

[id='assembly_configuring-cluster-resources-{context}']
= Configuring cluster resources
:context: configuring-cluster-resources

[role="_abstract"]
Create and delete cluster resources with the following commands.

The format for the command to create a cluster resource is as follows:

ifeval::[{ProductNumber} == 8]
[literal,subs="+quotes"]
....

pcs resource create _resource_id_ [_standard_:[_provider_:]]_type_ [_resource_options_] [op _operation_action operation_options_ [_operation_action_ _operation options_]...] [meta _meta_options_...] [clone [_clone_options_] | master [_master_options_] [--wait[=_n_]]

....
endif::[]
ifeval::[{ProductNumber} == 9]
[subs="+quotes"]
....

pcs resource create _resource_id_ [_standard_:[_provider_:]]_type_ [_resource_options_] [op _operation_action operation_options_ [_operation_action_ _operation options_]...] [meta _meta_options_...] [clone [_clone_id_] [_clone_options_] | promotable [_clone_id_] [_clone_options_] [--wait[=_n_]]

....
endif::[]

Key cluster resource creation options include the following:

// LINK TO
// For information about resource groups, see <<s1-resourcegroups-HAAR>>.

* The [option]`--before` and [option]`--after` options specify the position of the added resource relative to a resource that already exists in a resource group.

* Specifying the [option]`--disabled` option indicates that the resource is not started automatically.

There is no limit to the number of resources you can create in a cluster.

You can determine the behavior of a resource in a cluster by configuring constraints for that resource.


[discrete]
== Resource creation examples

The following command creates a resource with the name `VirtualIP` of standard `ocf`, provider `heartbeat`, and type `IPaddr2`. The floating address of this resource is 192.168.0.120, and the system will check whether the resource is running every 30 seconds.

[literal,subs="+quotes"]
....

# *pcs resource create VirtualIP ocf:heartbeat:IPaddr2 ip=192.168.0.120 cidr_netmask=24 op monitor interval=30s*

....

Alternately, you can omit the _standard_ and 
_provider_ fields and use the following command.
This will default to a standard of `ocf` and a provider
of `heartbeat`.

[literal,subs="+quotes"]
....

# *pcs resource create VirtualIP IPaddr2 ip=192.168.0.120 cidr_netmask=24 op monitor interval=30s*

....

[discrete]
== Deleting a configured resource

Delete a configured resource with the following command.

[literal,subs="+quotes"]
....

pcs resource delete _resource_id_

....

For example, the following command deletes an existing
resource with a resource ID of `VirtualIP`.

[literal,subs="+quotes"]
....

# *pcs resource delete VirtualIP*

....


include::modules/high-availability/ref_resource-properties.adoc[leveloffset=+1]

include::modules/high-availability/proc_displaying-resource-specific-parameters.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-resource-meta-options.adoc[leveloffset=+1]

include::modules/high-availability/proc_creating-resource-groups.adoc[leveloffset=+1]

include::modules/high-availability/con_determining-resource-behavior.adoc[leveloffset=+1]




// LINK TO:
// * For information about the _resource_id_, _standard_, _provider_, and
// _type_ fields of the [command]`pcs resource create` command, see
// <<s1-resourceprops-HAAR>>.
// 
// * For information about defining resource parameters for individual resources, see
// <<s1-genresourceparams-HAAR>>.
// 
// * For information about defining resource meta options, which are used by the cluster to decide how a resource should behave, see
// <<s1-resourceopts-HAAR>>.
// 
// * For information about defining the operations to perform on a resource, see
// <<s1-resourceoperate-HAAR>>.
// 
// * Specifying the [option]`clone` option creates a clone resource. Specifying the [option]`master` option creates a master/slave resource.  For information about resource clones and resources with multiple modes, see <<ch-advancedresource-HAAR>>.
// 


:context: {parent-context-of-assembly-configuring-cluster-resources}

