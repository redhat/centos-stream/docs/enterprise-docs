:_mod-docs-content-type: ASSEMBLY

:parent-context-of-getting-started-with-nftables: {context}

[id="getting-started-with-nftables_{context}"]
= Getting started with nftables

:context: getting-started-with-nftables

[role="_abstract"]
The `nftables` framework classifies packets and it is the successor to the `iptables`, `ip6tables`, `arptables`, `ebtables`, and `ipset` utilities. It offers numerous improvements in convenience, features, and performance over previous packet-filtering tools, most notably:

 * Built-in lookup tables instead of linear processing
 * A single framework for both the `IPv4` and `IPv6` protocols
 * All rules applied atomically instead of fetching, updating, and storing a complete rule set
 * Support for debugging and tracing in the rule set (`nftrace`) and monitoring trace events (in the `nft` tool)
 * More consistent and compact syntax, no protocol-specific extensions
 * A Netlink API for third-party applications

The `nftables` framework uses tables to store chains. The chains contain individual rules for performing actions. The `nft` utility replaces all tools from the previous packet-filtering frameworks. You can use the `libnftnl` library for low-level interaction with `nftables` Netlink API through the `libmnl` library.

To display the effect of rule set changes, use the `nft list ruleset` command. Because these utilities add tables, chains, rules, sets, and other objects to the `nftables` rule set, be aware that `nftables` rule-set operations, such as the `nft flush ruleset` command, might affect rule sets installed using the `iptables` command.


include::assembly_creating-and-managing-nftables-tables-chains-and-rules.adoc[leveloffset=+1]

include::assembly_migrating-from-iptables-to-nftables.adoc[leveloffset=+1]

include::assembly_writing-and-executing-nftables-scripts.adoc[leveloffset=+1]

include::assembly_configuring-nat-using-nftables.adoc[leveloffset=+1]

include::assembly_using-sets-in-nftables-commands.adoc[leveloffset=+1]

include::assembly_using-verdict-maps-in-nftables-commands.adoc[leveloffset=+1]

include::assembly_example-protecting-a-lan-and-dmz-using-an-nftables-script.adoc[leveloffset=+1]

include::assembly_configuring-port-forwarding-using-nftables.adoc[leveloffset=+1]

include::assembly_using-nftables-to-limit-the-amount-of-connections.adoc[leveloffset=+1]

include::assembly_debugging-nftables-rules.adoc[leveloffset=+1]

include::assembly_backing-up-and-restoring-the-nftables-rule-set.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* link:https://www.redhat.com/en/blog/using-nftables-red-hat-enterprise-linux-8[Using nftables in Red Hat Enterprise Linux 8]
* link:https://developers.redhat.com/blog/2016/10/28/what-comes-after-iptables-its-successor-of-course-nftables/[What comes after iptables? Its successor, of course: nftables]
* link:https://developers.redhat.com/blog/2018/08/10/firewalld-the-future-is-nftables/[Firewalld: The Future is nftables]


:context: {parent-context-of-getting-started-with-nftables}

