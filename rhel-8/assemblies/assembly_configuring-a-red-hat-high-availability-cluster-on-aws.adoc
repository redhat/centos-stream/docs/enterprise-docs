:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-configuring-a-red-hat-high-availability-cluster-on-aws: {context}]
[id="configuring-a-red-hat-high-availability-cluster-on-aws_{context}"]
= Configuring a Red Hat High Availability cluster on AWS
:context: configuring-a-red-hat-high-availability-cluster-on-aws

To create a cluster where RHEL nodes automatically redistribute their workloads if a node failure occurs, use the Red Hat High Availability Add-On. Such high availability (HA) clusters can also be hosted on public cloud platforms, including AWS. Creating RHEL HA clusters on AWS is similar to creating HA clusters in non-cloud environments.

To configure a Red Hat HA cluster on Amazon Web Services (AWS) using EC2 instances as cluster nodes, see the following sections. Note that you have a number of options for obtaining the {ProductName} ({ProductShortName}) images you use for your cluster. For information on image options for AWS, see xref:aws-image-options_deploying-a-virtual-machine-on-aws[Red Hat Enterprise Linux Image Options on AWS].

////
REDUNDANT?
* Follow prerequisite procedures for setting up your environment for AWS. Once you have set up your environment, you can create and configure EC2 instances.

* Follow procedures specific to the creation of HA clusters, which transform individual nodes into a cluster of HA nodes on AWS. These include procedures for installing the High Availability packages and agents on each cluster node, configuring fencing, and installing AWS network resource agents.
////

.Prerequisites

* Sign up for a link:https://access.redhat.com/[Red Hat Customer Portal] account.
* Sign up for AWS and set up your AWS resources. See link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html[Setting Up with Amazon EC2] for more information.

include::modules/cloud/con_the-benefits-of-using-high-availability-clusters-on-public-cloud-platforms.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-creating-access-key-and-secret-key.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-installing-aws-command-line-interface.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-creating-a-rhel-ha-ec2-instance.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-configuring-the-private-key-in-ha.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-connecting-to-an-instance.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-installing-rhel-ha-packages-and-agents.adoc[leveloffset=+1]

include::modules/cloud/azure/proc_azure-create-cluster-in-ha.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-configuring-fencing.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-installing-the-aws-cli-on-cluster-nodes.adoc[leveloffset=+1]

include::assembly_configuring-aws-network-resource-agents.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-configuring-shared-block-storage.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* link:https://docs.redhat.com/en/documentation/subscription_central/1-latest/html/red_hat_cloud_access_reference_guide/index[Red Hat Cloud Access Reference Guide]
* link:https://access.redhat.com/public-cloud[Red Hat in the Public Cloud]
* link:https://aws.amazon.com/partners/redhat/faqs/[Red Hat Enterprise Linux on Amazon EC2 - FAQs]
* link:https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html[Setting Up with Amazon EC2]
* link:https://access.redhat.com/public-cloud/aws[Red Hat on Amazon Web Services]

:context: {parent-context-of-configuring-a-red-hat-high-availability-cluster-on-aws}
ifdef::parent-context-of-configuring-a-red-hat-high-availability-cluster-on-aws[:context: {parent-context-of-configuring-a-red-hat-high-availability-cluster-on-aws}]
ifndef::parent-context-of-configuring-a-red-hat-high-availability-cluster-on-aws[:!context:]
