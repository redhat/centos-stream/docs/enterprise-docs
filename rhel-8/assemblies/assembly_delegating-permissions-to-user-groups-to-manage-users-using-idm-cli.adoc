:_mod-docs-content-type: ASSEMBLY
// This assembly was previously named assembly_delegating-permissions-over-users-using-idm-cli.adoc

ifdef::context[:parent-context-of-delegating-permissions-to-user-groups-to-manage-users-using-idm-cli: {context}]

[id="delegating-permissions-to-user-groups-to-manage-users-using-idm-cli_{context}"]
= Delegating permissions to user groups to manage users using IdM CLI

:context: delegating-permissions-to-user-groups-to-manage-users-using-idm-cli

// This is the new introduction, which was the product of peer review of the Ansible assembly about delegation. We removed the content that the concept module con_delegation-rules.adoc explains.

[role="_abstract"]
Delegation is one of the access control methods in IdM, along with self-service rules and role-based access control (RBAC). You can use delegation to assign permissions to one group of users to manage entries for another group of users.

This section covers the following topics:

// Below is the old introduction
//Delegation is one of the access control methods in IdM, along with self-service rules and role-based access control.

//Delegation is similar to roles in that one group of users is assigned permission to manage the entries for another group of users. However, the delegated authority is limited to editing the values of specific attributes; it does not grant the ability to add or remove whole entries or control over unspecified attributes. Also, the groups in delegated authority are existing IdM user groups instead of roles specifically created for access controls.

//This chapter covers the following topics:

* xref:delegation-rules_{context}[Delegation rules]
* xref:creating-a-delegation-rule-using-idm-cli_{context}[Creating a delegation rule using IdM CLI]
* xref:viewing-existing-delegation-rules-using-idm-cli_{context}[Viewing existing delegation rules using IdM CLI]
* xref:modifying-a-delegation-rule-using-idm-cli_{context}[
Modifying a delegation rule using IdM CLI]
* xref:deleting-a-delegation-rule-using-idm-cli_{context}[Deleting a delegation rule using IdM CLI]

include::modules/identity-management/con_delegation-rules.adoc[leveloffset=+1]

include::modules/identity-management/proc_creating-a-delegation-rule-using-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_viewing-existing-delegation-rules-using-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_modifying-a-delegation-rule-using-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-a-delegation-rule-using-idm-cli.adoc[leveloffset=+1]

ifdef::parent-context-of-delegating-permissions-to-user-groups-to-manage-users-using-idm-cli[:context: {parent-context-of-delegating-permissions-to-user-groups-to-manage-users-using-idm-cli}]
ifndef::parent-context-of-delegating-permissions-to-user-groups-to-manage-users-using-idm-cli[:!context:]
