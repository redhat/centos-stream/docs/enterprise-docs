:_mod-docs-content-type: ASSEMBLY

:parent-context-of-packaging-software: {context}

[id="packaging-software_{context}"]
= Packaging software

:context: packaging-software

ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]


[role="_abstract"]
In the following sections, learn the basics of the packaging process with the RPM package manager.

//Moved to Introduction to RPM: include::modules/core-services/con_rpm-packages.adoc[leveloffset=+1]

//Moved to Introduction to RPM: include::modules/core-services/proc_listing-rpm-packaging-tools-utilities.adoc[leveloffset=+1]

//moved under the assembly below: include::modules/core-services/proc_setting-up-rpm-packaging-workspace.adoc[leveloffset=+1]

include::assembly_setting-up-rpm-packaging-workspace.adoc[leveloffset=+1]

include::assembly_what-a-spec-file-is.adoc[leveloffset=+1]

include::modules/core-services/con_buildroots.adoc[leveloffset=+1]

include::modules/core-services/con_rpm-macros.adoc[leveloffset=+1]

//include::modules/core-services/con_working-with-spec-files.adoc[leveloffset=+1]
include::assembly_working-with-spec-files.adoc[leveloffset=+1]


//include::modules/core-services/con_building-rpms.adoc[leveloffset=+1]
include::assembly_building-rpms.adoc[leveloffset=+1]


//include::modules/core-services/con_checking-rpms-for-sanity.adoc[leveloffset=+1]
include::assembly_checking-rpms-for-common-errors.adoc[leveloffset=+1]

include::modules/core-services/proc_logging-rpm-activity.adoc[leveloffset=+1]

include::modules/core-services/proc_extracting-rpm-content.adoc[leveloffset=+1]



// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-packaging-software}
