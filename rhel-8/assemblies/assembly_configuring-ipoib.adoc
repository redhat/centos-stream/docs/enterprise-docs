:_newdoc-version: 2.18.3
:_template-generated: 2024-08-01

ifdef::context[:parent-context-of-configuring-ipoib: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-ipoib"]
endif::[]
ifdef::context[]
[id="configuring-ipoib_{context}"]
endif::[]
= Configuring IPoIB

:context: configuring-ipoib

By default, InfiniBand does not use the internet protocol (IP) for communication. However, IP over InfiniBand (IPoIB) provides an IP network emulation layer on top of InfiniBand remote direct memory access (RDMA) networks. This allows existing unmodified applications to transmit data over InfiniBand networks, but the performance is lower than if the application would use RDMA natively.

[NOTE]
====
The Mellanox devices, starting from ConnectX-4 and above, on RHEL 8 and later use Enhanced IPoIB mode by default (datagram only). Connected mode is not supported on these devices.
====


include::modules/infiniband-rdma/con_the-ipoib-communication-modes.adoc[leveloffset=+1]

include::modules/infiniband-rdma/con_understanding-ipoib-hardware-addresses.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_renaming-ipoib-devices.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-an-ipoib-connection-using-nmcli-commands.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-an-ipoib-connection-by-using-the-network-system-role.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-an-ipoib-connection-by-using-nmstatectl.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_configuring-an-ipoib-connection-using-nm-connection-editor.adoc[leveloffset=+1]

include::modules/infiniband-rdma/proc_testing-an-rdma-network-using-qperf-after-ipoib-is-configured.adoc[leveloffset=+1]


ifdef::parent-context-of-configuring-ipoib[:context: {parent-context-of-configuring-ipoib}]
ifndef::parent-context-of-configuring-ipoib[:!context:]
