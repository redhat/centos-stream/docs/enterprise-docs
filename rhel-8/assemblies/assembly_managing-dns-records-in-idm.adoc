:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-managing-dns-records-in-idm: {context}]
[id="managing-dns-records-in-idm_{context}"]
= Managing DNS records in IdM

:context: managing-dns-records-in-idm

[role="_abstract"]
This chapter describes how to manage DNS records in {IPA} (IdM). As an IdM administrator, you can add, modify and delete DNS records in IdM. The chapter contains the following sections:

* xref:dns-records-in-idm_{context}[DNS records in IdM]
* xref:adding-dns-resource-records-in-the-idm-web-ui_{context}[Adding DNS resource records from the IdM Web UI]
* xref:adding-dns-resource-records-from-the-idm-cli_{context}[Adding DNS resource records from the IdM CLI]
* xref:common-ipa-dnsrecord-options_{context}[Common ipa dnsrecord-add options]
* xref:deleting-dns-records-in-the-idm-web-ui_{context}[Deleting DNS records in the IdM Web UI]
* xref:deleting-an-entire-dns-record-in-the-idm-web-ui_{context}[Deleting an entire DNS record in the IdM Web UI]
* xref:deleting-dns-records-in-the-idm-cli_{context}[Deleting DNS records in the IdM CLI]

.Prerequisites

* Your IdM deployment contains an integrated DNS server. For information how to install IdM with integrated DNS, see one of the following links:

** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/installing_identity_management/index#installing-an-ipa-server-with-integrated-dns_installing-identity-management[Installing an IdM server: With integrated DNS, with an integrated CA as the root CA].

** link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/installing_identity_management/index#installing-an-ipa-server-with-external-ca_installing-identity-management[Installing an IdM server: With integrated DNS, with an external CA as the root CA].


include::modules/identity-management/con_dns-records-in-idm.adoc[leveloffset=+1]

//include::modules/identity-management/con_dns-wildcard-support-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-dns-resource-records-in-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-dns-resource-records-from-the-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/ref_common-ipa-dnsrecord-options.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-dns-records-in-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-an-entire-dns-record-in-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_deleting-dns-records-in-the-idm-cli.adoc[leveloffset=+1]




[role="_additional-resources"]
== Additional resources
* See xref:using-ansible-to-manage-dns-records-in-idm_{parent-context-of-managing-dns-records-in-idm}[Using Ansible to manage DNS records in IdM].

ifdef::parent-context-of-managing-dns-records-in-idm[:context: {parent-context-of-managing-dns-records-in-idm}]
ifndef::parent-context-of-managing-dns-records-in-idm[:!context:]
