ifdef::context[:parent-context-of-checking-cello-for-common-errors: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="checking-cello-for-common-errors"]
endif::[]
ifdef::context[]
[id="checking-cello-for-common-errors_{context}"]
endif::[]
= Checking a sample C program for common errors

:context: checking-cello-for-common-errors

In the following sections, investigate possible warnings and errors that can occur when validating RPM content on the example of the `cello` `spec` file and `cello` binary RPM.

include::modules/core-services/con_checking-the-cello-spec-file.adoc[leveloffset=+1]

include::modules/core-services/con_checking-the-cello-binary-rpm.adoc[leveloffset=+1]

ifdef::parent-context-of-checking-cello-for-common-errors[:context: {parent-context-of-checking-cello-for-common-errors}]
ifndef::parent-context-of-checking-cello-for-common-errors[:!context:]

