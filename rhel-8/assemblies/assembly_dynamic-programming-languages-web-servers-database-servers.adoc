:_mod-docs-content-type: ASSEMBLY

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-dynamic-programming-languages-web-servers-database-servers: {context}


[id="dynamic-programming-languages-web-servers-database-servers_{context}"]
= Dynamic programming languages, web servers, database servers

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: dynamic-programming-languages-web-servers-database-servers


include::modules/core-services/ref_dynamic-programming-languages.adoc[leveloffset=+1]

include::assembly_tcl.adoc[leveloffset=+1]

include::modules/core-services/ref_web-servers.adoc[leveloffset=+1]

include::modules/core-services/ref_proxy-caching-servers.adoc[leveloffset=+1]

include::modules/core-services/ref_database-servers.adoc[leveloffset=+1]


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-dynamic-programming-languages-web-servers-database-servers}
