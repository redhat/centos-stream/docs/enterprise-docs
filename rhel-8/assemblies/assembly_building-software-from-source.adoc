
ifdef::context[:parent-context-of-building-software-from-source: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="building-software-from-source"]
endif::[]
ifdef::context[]
[id="building-software-from-source_{context}"]
endif::[]
= Building software from source

:context: building-software-from-source

During the software building process, the source code is turned into software artifacts that you can package by using RPM.


//DELETE if decided include::modules/core-services/proc_building-software-from-natively-compiled-code.adoc[leveloffset=+1]

include::assembly_building-software-from-natively-compiled-code.adoc[leveloffset=+1]

// DELETE if decided include::modules/core-services/proc_interpreting-code.adoc[leveloffset=+1]

include::assembly_interpreting-source-code.adoc[leveloffset=+1]



ifdef::parent-context-of-building-software-from-source[:context: {parent-context-of-building-software-from-source}]
ifndef::parent-context-of-building-software-from-source[:!context:]

