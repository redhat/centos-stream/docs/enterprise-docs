:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_using-powertop.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-using-powertop: {context}

// The file name and the ID are based on the assembly title.
// For example:
// * file name: assembly_my-assembly-a.adoc
// * ID: [id='assembly_my-assembly-a_{context}']
// * Title: = My assembly A
//
// The ID is used as an anchor for linking to the module.
// Avoid changing it after the module has been published
// to ensure existing links are not broken.
//
// In order for  the assembly to be reusable in other assemblies in a guide,
// include {context} in the ID: [id='a-collection-of-modules_{context}'].
//
// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.
[id="using-powertop_{context}"]
= Using PowerTOP

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: using-powertop

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]


.Prerequisites

* To be able to use [application]*PowerTOP*, make sure that the [package]`powertop` package has been installed on your system:
+
[subs="+quotes,attributes"]
----
# {PackageManagerCommand} install powertop
----

include::modules/core-services/proc_starting-powertop.adoc[leveloffset=+1]

include::modules/core-services/proc_calibrating-powertop.adoc[leveloffset=+1]

include::modules/core-services/proc_setting-the-measuring-interval.adoc[leveloffset=+1]


[id='related-information-{context}']
[role="_additional-resources"]
== Additional resources
For more details on how to use [application]*PowerTOP*, see the `powertop` man page on your system

:context: {parent-context-of-using-powertop}
