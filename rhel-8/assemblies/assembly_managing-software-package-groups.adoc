:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-managing-software-package-groups: {context}]

[id="managing-software-package-groups_{context}"]
= Managing software package groups

:context: managing-software-package-groups

[role="_abstract"]
[subs="+attributes"]
A package group is a collection of packages that serve a common purpose ([application]*System Tools*, [application]*Sound and Video*). Installing a package group pulls a set of dependent packages, which saves time considerably.

[subs="+attributes"]
The following section describes how to use *{PackageManagerCommand}* to:

* List package groups.
* Install a package group.
* Remove a package group.
* Specify global expressions in {PackageManagerCommand} input.

include::modules/core-services/proc_listing-package-groups-with-yum.adoc[leveloffset=+1]

include::modules/core-services/proc_installing-a-package-group-with-yum.adoc[leveloffset=+1]

include::modules/core-services/proc_removing-a-package-group-with-yum.adoc[leveloffset=+1]

include::modules/core-services/con_specifying-global-expressions-in-yum-input.adoc[leveloffset=+1]


ifdef::parent-context-of-managing-software-package-groups[:context: {parent-context-of-managing-software-package-groups}]
ifndef::parent-context-of-managing-software-package-groups[:!context:]
