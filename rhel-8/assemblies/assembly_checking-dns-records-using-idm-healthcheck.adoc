:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

ifdef::context[:parent-context-of-checking-dns-records-using-idm-healthcheck: {context}]

[id="checking-dns-records-using-idm-healthcheck_{context}"]
= Checking DNS records using IdM Healthcheck

:context: checking-dns-records-using-idm-healthcheck

[role="_abstract"]
You can identify issues with DNS records in Identity Management (IdM) using the Healthcheck tool.

ifeval::["{parent-context-of-checking-dns-records-using-idm-healthcheck}" == "collecting-idm-healthcheck-information"]
For details, see xref:healthcheck-in-idm_collecting-idm-healthcheck-information[Healthcheck in IdM].
endif::[]

// Only display the following if this is the RHEL 8 Conf&Man guide
ifeval::[{ProductNumber} == 8]
.Prerequisites

* The DNS records Healthcheck tool is only available on RHEL 8.2 or newer.
endif::[]

include::modules/identity-management/con_dns-records-healthcheck-test.adoc[leveloffset=+1]

include::modules/identity-management/proc_screening-dns-records-using-the-healthcheck-tool.adoc[leveloffset=+1]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-checking-dns-records-using-idm-healthcheck[:context: {parent-context-of-checking-dns-records-using-idm-healthcheck}]
ifndef::parent-context-of-checking-dns-records-using-idm-healthcheck[:!context:]
