:_newdoc-version: 2.15.0
:_template-generated: 2024-6-18

ifdef::context[:parent-context-of-delegating-access-to-hosts-and-services: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="delegating-access-to-hosts-and-services"]
endif::[]
ifdef::context[]
[id="delegating-access-to-hosts-and-services_{context}"]
endif::[]
= Delegating access to hosts and services

:context: delegating-access-to-hosts-and-services

[role="_abstract"]
By delegating access to hosts and services within an IdM domain, you can retrieve keytabs and certificates for another host or service.

Each host and service has a `managedby` entry that lists what hosts and services can manage it. By default, a host can manage itself and all of its services. You can configure a host to manage other hosts, or services on other hosts within the IdM domain.

NOTE: When you delegate authority of a host to another host through a `managedby` entry, it does not automatically grant management rights for all services on that host. You must perform each delegation independently.

.Host and service delegation
image:idm-host-service-delegation.png[Diagram illustrates an IdM domain with three hosts: Host 1, Host 2, and Host 3. Each of the hosts has two services listed under them. Host 1 can manage host 2 and host 3, and has access to the services of host 3.]

include::modules/identity-management/proc_delegating-service-management.adoc[leveloffset=+1]

include::modules/identity-management/proc_delegating-host-management.adoc[leveloffset=+1]

include::modules/identity-management/proc_accessing-delegated-services.adoc[leveloffset=+1]

ifdef::parent-context-of-delegating-access-to-hosts-and-services[:context: {parent-context-of-delegating-access-to-hosts-and-services}]
ifndef::parent-context-of-delegating-access-to-hosts-and-services[:!context:]
