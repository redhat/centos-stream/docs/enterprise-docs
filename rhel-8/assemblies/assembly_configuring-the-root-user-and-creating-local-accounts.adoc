:_newdoc-version: 2.18.2
:_template-generated: 2024-06-11

ifdef::context[:parent-context-of-configuring-the-root-user-and-creating-local-accounts: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-the-root-user-and-creating-local-accounts"]
endif::[]
ifdef::context[]
[id="configuring-the-root-user-and-creating-local-accounts_{context}"]
endif::[]
= Configuring the root user and creating local accounts

:context: configuring-the-root-user-and-creating-local-accounts

include::modules/installer/proc_configuring-a-root-password.adoc[leveloffset=+1]

include::modules/installer/proc_creating-a-user-account.adoc[leveloffset=+1]

include::modules/installer/proc_editing-advanced-user-settings.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-the-root-user-and-creating-local-accounts[:context: {parent-context-of-configuring-the-root-user-and-creating-local-accounts}]
ifndef::parent-context-of-configuring-the-root-user-and-creating-local-accounts[:!context:]

