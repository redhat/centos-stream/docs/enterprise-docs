:_mod-docs-content-type: ASSEMBLY
:parent-context-of-building-code-with-gcc: {context}
[id="building-code-with-gcc_{context}"]
= Building Code with GCC
:context: building-code-with-gcc


ifdef::user-stories[__As a developer, I want to turn my C/C++ code into the program__]

Learn about situations where source code must be transformed into executable code.

include::modules/platform-tools/con_relationship-between-code-forms.adoc[leveloffset=+1]

//include::modules/platform-tools/create-executable-from-source.adoc[leveloffset=+1]

include::modules/platform-tools/con_compiling-source-files-to-object-code.adoc[leveloffset=+1]

include::modules/platform-tools/con_enabling-debugging-of-c-and-c-applications-with-gcc.adoc[leveloffset=+1]

include::modules/platform-tools/con_code-optimization-with-gcc.adoc[leveloffset=+1]

include::modules/platform-tools/con_options-for-hardening-code-with-gcc.adoc[leveloffset=+1]

include::modules/platform-tools/con_linking-code-to-create-executable-files.adoc[leveloffset=+1]

// include::modules/platform-tools/ref_c-compatibility-of-various-red-hat-products.adoc[leveloffset=+1]

include::modules/platform-tools/proc_example-building-a-c-program-with-gcc.adoc[leveloffset=+1]

include::modules/platform-tools/proc_example-building-a-c-program-with-gcc-2-steps.adoc[leveloffset=+1]

include::modules/platform-tools/proc_example-building-a-cpp-program-with-gcc.adoc[leveloffset=+1]

include::modules/platform-tools/proc_example-building-a-cpp-program-with-gcc-2-steps.adoc[leveloffset=+1]


:context: {parent-context-of-building-code-with-gcc}
