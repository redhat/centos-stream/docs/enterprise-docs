:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>


ifdef::context[:parent-context-of-managing-user-groups-using-ansible-playbooks: {context}]

[id="managing-user-groups-using-ansible-playbooks_{context}"]
= Managing user groups using Ansible playbooks

:context: managing-user-groups-using-ansible-playbooks

[role="_abstract"]
This section introduces user group management using Ansible playbooks.

A user group is a set of users with common privileges, password policies, and other characteristics.

A user group in {IPA} (IdM) can include:

* IdM users
* other IdM user groups
* external users, which are users that exist outside of IdM

The section includes the following topics:

* xref:the-different-group-types-in-idm_{context}[The different group types in IdM]
* xref:direct-and-indirect-group-members_{context}[Direct and indirect group members]
* xref:ensuring-the-presence-of-IdM-groups-and-group-members-using-Ansible-playbooks_managing-user-groups-using-ansible-playbooks[Ensuring the presence of IdM groups and group members using Ansible playbooks]
* xref:proc_using-ansible-to-enable-ad-users-to-administer-idm_{context}[Using Ansible to enable AD users to administer IdM]
* xref:ensuring-the-presence-of-member-managers-in-idm-user-groups-using-ansible-playbooks_managing-user-groups-using-ansible-playbooks[Ensuring the presence of member managers in IDM user groups using Ansible playbooks]
* xref:ensuring-the-absence-of-member-managers-in-idm-user-groups-using-ansible-playbooks_managing-user-groups-using-ansible-playbooks[Ensuring the absence of member managers in IDM user groups using Ansible playbooks]

include::modules/identity-management/con_the-different-group-types-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/con_direct-and-indirect-group-members.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-IdM-groups-and-group-members-using-Ansible-playbooks.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-add-multiple-idm-groups-in-a-single-task.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-enable-ad-users-to-administer-idm.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-the-presence-of-member-managers-in-idm-user-groups-using-ansible-playbooks.adoc[leveloffset=+1]

include::modules/identity-management/proc_ensuring-the-absence-of-member-managers-in-idm-user-groups-using-ansible-playbooks.adoc[leveloffset=+1]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-managing-user-groups-using-ansible-playbooks[:context: {parent-context-of-managing-user-groups-using-ansible-playbooks}]
ifndef::parent-context-of-managing-user-groups-using-ansible-playbooks[:!context:]
