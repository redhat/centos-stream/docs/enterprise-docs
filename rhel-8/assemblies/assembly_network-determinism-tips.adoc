ifdef::context[:parent-context-of-assembly_network-determinism-tips: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_network-determinism-tips"]
endif::[]
ifdef::context[]
[id="assembly_network-determinism-tips_{context}"]
endif::[]
= Network determinism tips

:context: assembly_network-determinism-tips

[role="_abstract"]
TCP can have a large effect on latency. TCP adds latency in order to obtain efficiency, control congestion, and to ensure reliable delivery. When tuning, consider the following points:

* Do you need ordered delivery?
* Do you need to guard against packet loss?
+
Transmitting packets more than once can cause delays.

*  Do you need to use TCP?
+
Consider disabling the Nagle buffering algorithm by using `TCP_NODELAY` on your socket. The Nagle algorithm collects small outgoing packets to send all at once, and can have a detrimental effect on latency.
// +
// See also Section 31.4, “Reduce TCP performance spikes” and Section 32.1, “Reducing the TCP delayed ACK timeout”.




include::modules/networking/proc_optimizing-rhel-for-latency-or-throughput-sensitive-services.adoc[leveloffset=+1]

include::modules/networking/ref_flow-control-in-ethernet-networks.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* `ethtool(8)` and `netstat(8)` man pages on your system

ifdef::parent-context-of-assembly_network-determinism-tips[:context: {parent-context-of-assembly_network-determinism-tips}]
ifndef::parent-context-of-assembly_network-determinism-tips[:!context:]
