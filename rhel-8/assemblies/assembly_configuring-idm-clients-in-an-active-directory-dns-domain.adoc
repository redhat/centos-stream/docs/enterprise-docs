:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_configuring-idm-clients-in-an-active-directory-dns-domain: {context}]

ifndef::context[]
[id="assembly_configuring-idm-clients-in-an-active-directory-dns-domain"]
endif::[]
ifdef::context[]
[id="assembly_configuring-idm-clients-in-an-active-directory-dns-domain_{context}"]
endif::[]
= Configuring IdM clients in an Active Directory DNS domain

:context: assembly_configuring-idm-clients-in-an-active-directory-dns-domain

[role="_abstract"]
If you have client systems in a DNS domain controlled by Active Directory (AD) and you require those clients to join the IdM Server to benefit from its RHEL features, you can configure users to access a client using a host name from the AD DNS domain.

[IMPORTANT]
====
This configuration is not recommended and has limitations. Always deploy IdM clients in a DNS zone separate from the ones owned by AD and access IdM clients using their IdM host names.
====

Your IdM client configuration depends on whether you require single sign-on with Kerberos.

include::modules/identity-management/proc_configuring-an-idm-client-without-kerberos-single-sign-on.adoc[leveloffset=+1]

include::modules/identity-management/proc_handling-ssl-certificates-without-single-sign-on-available.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-an-idm-client-with-kerberos-single-sign-on.adoc[leveloffset=+1]

include::modules/identity-management/proc_handling-ssl-certificates-with-single-sign-on-available.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_configuring-idm-clients-in-an-active-directory-dns-domain[:context: {parent-context-of-assembly_configuring-idm-clients-in-an-active-directory-dns-domain}]
ifndef::parent-context-of-assembly_configuring-idm-clients-in-an-active-directory-dns-domain[:!context:]
