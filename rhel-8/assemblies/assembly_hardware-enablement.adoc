:_mod-docs-content-type: ASSEMBLY

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_hardware-enablement.adoc[leveloffset=+1]

:parent-context-of-hardware-enablement: {context}

[id="hardware-enablement_{context}"]
= Hardware enablement

:context: hardware-enablement


include::modules/upgrades-and-differences/ref_removed-hardware-support.adoc[leveloffset=+1]


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-hardware-enablement}
