:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-tablets: {context}]


[id="tablets_{context}"]
= Graphics tablets

:context: tablets

To manage Wacom tablets connected to your system, use the following tools:

* The `gnome-settings-daemon` service
* The `Wacom Tablet` settings panel in the GNOME environment
+
.The Wacom Tablet settings panel for a tablet
image:../images/Wacon_tablet_settings.png[]
+
.The Wacom Tablet settings panel for a grip pen
image:../images/Wacom_tablet_grip_pen.png[]

Both these tools, as well as the `libinput` stack, use the `libwacom` tablet client library, which stores the data about Wacom tablets.

If you want to add support for a new tablet into the `libwacom` library, you must ensure that a definition file for this new tablet exists.

include::modules/desktop/proc_ensuring-tablet-definition-file.adoc[leveloffset=+1]

include::modules/desktop/proc_adding-support-for-a-new-tablet.adoc[leveloffset=+1]

include::modules/desktop/proc_listing-available-wacom-tablet-configuration-paths.adoc[leveloffset=+1]



// Restore the context to what it was before this assembly.
ifdef::parent-context-of-tablets[:context: {parent-context-of-tablets}]
ifndef::parent-context-of-tablets[:!context:]

