:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id="conf-certmap-for-ad-certs_{context}"]
= Configuring certificate mapping for users whose AD user entry contains the whole certificate

:context: conf-certmap-for-ad-certs

[role="_abstract"]
This user story describes the steps necessary for enabling certificate mapping in IdM if the IdM deployment is in trust with Active Directory (AD), the user is stored in AD and the user entry in AD contains the whole certificate.

.Prerequisites

* The user does not have an account in IdM.
* The user has an account in AD which contains a certificate.
* The IdM administrator has access to data on which the IdM certificate mapping rule can be based.

[NOTE]
====
To ensure PKINIT works for a user, one of the following conditions must apply:

* The certificate in the user entry includes the user principal name or the SID extension for the user.
* The user entry in AD has a suitable entry in the `altSecurityIdentities` attribute.
====

include::modules/identity-management/proc_add-maprule-webui-ad-cert.adoc[leveloffset=+1]

include::modules/identity-management/proc_add-maprule-cli-ad-cert.adoc[leveloffset=+1]

//include::modules/identity-management/proc_add-certmapdata-to-user-ad-cert.adoc[leveloffset=+2]

:context: {parent-context}
