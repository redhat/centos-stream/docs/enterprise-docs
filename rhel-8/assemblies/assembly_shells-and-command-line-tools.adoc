:_mod-docs-content-type: ASSEMBLY

:parent-context-of-shells-and-command-line-tools: {context}


[id="shells-and-command-line-tools_{context}"]
= Shells and command-line tools

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: shells-and-command-line-tools


// Info about system roles should be delivered post-GA according to PM (wait when Ansible for RHEL 8 is officially released)
//include::modules/core-services/ref_system-roles.adoc[leveloffset=+1]

include::modules/core-services/ref_localization-is-distributed-in-multiple-packages.adoc[leveloffset=+1]

include::modules/core-services/ref_removed-support-for-all-numeric-user-and-group-names.adoc[leveloffset=+1]

include::modules/core-services/ref_the-nobody-user-replaces-nfsnobody.adoc[leveloffset=+1]

include::modules/core-services/ref_version-control-systems.adoc[leveloffset=+1]

include::modules/core-services/ref_packages-moved-from-crontab-entries-to-systemd-timer.adoc[leveloffset=+1]



// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-shells-and-command-line-tools}
