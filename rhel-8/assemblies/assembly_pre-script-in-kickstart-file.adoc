:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-pre-script-in-kickstart-file: {context}]

[id="pre-script-in-kickstart-file_{context}"]
= %pre script

:context: pre-script-in-kickstart-file

[role="_abstract"]
The `%pre` scripts are run on the system immediately after the Kickstart file has been loaded, but before it is completely parsed and installation begins. Each of these sections must start with `%pre` and end with `%end`.

The `%pre` script can be used for activation and configuration of networking and storage devices. It is also possible to run scripts, using interpreters available in the installation environment. Adding a `%pre` script can be useful if you have networking and storage that needs special configuration before proceeding with the installation, or have a script that, for example, sets up additional logging parameters or environment variables.

Debugging problems with `%pre` scripts can be difficult, so it is recommended only to use a `%pre` script when necessary.

[IMPORTANT]
====
The `%pre` section of Kickstart is executed at the stage of installation which happens after the installer image (`inst.stage2`) is fetched: it means *after* root switches to the installer environment (the installer image) and *after* the `Anaconda` installer itself starts. Then the configuration in `%pre` is applied and can be used to fetch packages from installation repositories configured, for example, by URL in Kickstart. However, it *cannot* be used to configure network to fetch the image (`inst.stage2`) from network.

====

Commands related to networking, storage, and file systems are available to use in the `%pre` script, in addition to most of the utilities in the installation environment `/sbin` and `/bin` directories.

You can access the network in the `%pre` section. However, the name service has not been configured at this point, so only IP addresses work, not URLs.

NOTE: The pre script does not run in the chroot environment.



include::modules/installer/ref_pre-script-section-options.adoc[leveloffset=+1]



ifdef::parent-context-of-pre-script-in-kickstart-file[:context: {parent-context-of-pre-script-in-kickstart-file}]
ifndef::parent-context-of-pre-script-in-kickstart-file[:!context:]
