:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_changing-basic-environment-settings: {context}]



ifndef::context[]
[id="assembly_changing-basic-environment-settings"]
endif::[]
ifdef::context[]
[id="assembly_changing-basic-environment-settings_{context}"]
endif::[]
= Changing basic environment settings

:context: assembly_changing-basic-environment-settings

[role="_abstract"]
Configuration of basic environment settings is a part of the installation process. The following sections guide you when you change them later. The basic configuration of the environment includes:

* Date and time
* System locales
* Keyboard layout
* Language

include::modules/core-services/proc_configuring-the-date-and-time.adoc[leveloffset=+1]

include::modules/cockpit/proc_configuring-time-settings-using-the-web-console.adoc[leveloffset=+1]

include::modules/core-services/proc_configuring-the-system-locale.adoc[leveloffset=+1]

include::modules/core-services/proc_configuring-the-keyboard-layout.adoc[leveloffset=+1]

include::modules/core-services/ref_changing-the-font-size-in-the-text-console-mode.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_changing-basic-environment-settings[:context: {parent-context-of-assembly_changing-basic-environment-settings}]
ifndef::parent-context-of-assembly_changing-basic-environment-settings[:!context:]
