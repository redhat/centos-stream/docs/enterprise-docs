:_newdoc-version: 2.17.0
:_template-generated: 2024-05-07

ifdef::context[:parent-context-of-optional-customizing-boot-options: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="optional-customizing-boot-options"]
endif::[]
ifdef::context[]
[id="optional-customizing-boot-options_{context}"]
endif::[]
= Optional: Customizing boot options

:context: optional-customizing-boot-options

When you are installing RHEL on `x86_64` or `ARM64` architectures, you can edit the boot options to customize the installation process based on your specific environment.

// include::modules/installer/ref_boot-menu.adoc[leveloffset=+1] Removed as a part of RHELDOCS-19381

include::modules/installer/con_types-of-boot-options.adoc[leveloffset=+1]

include::modules/installer/proc_editing-the-boot-prompt-in-bios.adoc[leveloffset=+1]

include::modules/installer/proc_editing-the-prompt.adoc[leveloffset=+1]

include::modules/installer/proc_editing-the-grub2-menu.adoc[leveloffset=+1]

include::assembly_updating-drivers-during-installation.adoc[leveloffset=+1]

ifdef::interactive-install-media[]
[role="_additional-resources"]
== Additional resources
* For a list of all boot options to customize the installation program's behavior, see xref:custom-boot-options_rhel-installer[Boot options reference].
endif::[]

ifdef::parent-context-of-optional-customizing-boot-options[:context: {parent-context-of-optional-customizing-boot-options}]
ifndef::parent-context-of-optional-customizing-boot-options[:!context:]

