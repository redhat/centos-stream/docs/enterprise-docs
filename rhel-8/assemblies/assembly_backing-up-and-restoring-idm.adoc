:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following titles:
// titles/planning/planning-identity-management-and-access-control  with a context of  planning-identity-management


ifdef::context[:parent-context-of-backing-up-and-restoring-idm: {context}]

include::modules/identity-management/_attributes-identity-management-and-access-control.adoc[]

[id="backing-up-and-restoring-idm_{context}"]
= Backing up and restoring IdM

:context: backing-up-and-restoring-idm

[role="_abstract"]
{IPA} lets you manually back up and restore the IdM system after a data loss event.

During a backup, the system creates a directory that stores information about your IdM setup. You can use this backup directory to restore your original IdM setup.

[NOTE]
====
The IdM backup and restore features are designed to help prevent data loss. To mitigate the impact of server loss and ensure continued operation, provide alternative servers to clients. For information on establishing a replication topology see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/preparing_for_disaster_recovery_with_identity_management/preparing-for-server-loss-with-replication_preparing-for-disaster-recovery[Preparing for server loss with replication].
====

include::modules/identity-management/ref_backup-types.adoc[leveloffset=+1]

include::modules/identity-management/ref_naming-conventions-for-idm-backup-files.adoc[leveloffset=+1]

include::modules/identity-management/ref_considerations-when-creating-a-backup.adoc[leveloffset=+1]

include::modules/identity-management/proc_creating-an-idm-backup.adoc[leveloffset=+1]

include::modules/identity-management/proc_encrypting-a-backup.adoc[leveloffset=+1]

include::modules/identity-management/proc_creating-gpg-key.adoc[leveloffset=+1]

include::modules/identity-management/con_when-to-restore-from-idm-backup.adoc[leveloffset=+1]

include::modules/identity-management/con_considerations-when-restoring-from-idm-backup.adoc[leveloffset=+1]

include::modules/identity-management/proc_restoring-a-backup.adoc[leveloffset=+1]

include::modules/identity-management/proc_restoring-encrypted-backup.adoc[leveloffset=+1]

// Removed assembly_backing-up-and-restoring-idm-servers-using-ansible-playbooks.adoc
// from here and moved it to title level at planning/planning-identity-management/master.adoc

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-backing-up-and-restoring-idm[:context: {parent-context-of-backing-up-and-restoring-idm}]
ifndef::parent-context-of-backing-up-and-restoring-idm[:!context:]
