:_mod-docs-content-type: ASSEMBLY
:parent-context-of-file-systems-and-storage: {context}

[id="file-systems-and-storage_{context}"]
= File systems and storage

:context: file-systems-and-storage

ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

include::modules/filesystems-and-storage/ref_file-systems-in-rhel-8.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/ref_storage-in-rhel-8.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/ref_lvm-in-rhel-8.adoc[leveloffset=+1]


// [id='related-information-{context}']
// == Related information
// 
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

:context: {parent-context-of-file-systems-and-storage}
