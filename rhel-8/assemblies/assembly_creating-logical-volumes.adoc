:_newdoc-version: 2.16.0
:_template-generated: 2024-03-25

ifdef::context[:parent-context-of-creating-logical-volumes: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="creating-logical-volumes"]
endif::[]
ifdef::context[]
[id="creating-logical-volumes_{context}"]
endif::[]
= Creating logical volumes

:context: creating-logical-volumes

[role="_abstract"]
LVM provides a flexible approach to handling disk storage by abstracting the physical layer into logical volumes that can be created and adjusted based on your needs.

// linear
include::modules/filesystems-and-storage/proc_creating-thick-lv.adoc[leveloffset=+1]

include::modules/core-services/ref_an-example-playbook-to-manage-logical-volumes.adoc[leveloffset=+1]

// striped
include::modules/filesystems-and-storage/proc_creating-striped-lv.adoc[leveloffset=+1]

// raid
include::modules/filesystems-and-storage/proc_creating-raid-lv.adoc[leveloffset=+1]

// thin
include::modules/filesystems-and-storage/proc_creating-thin-lv.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 9]
include::modules/filesystems-and-storage/proc_creating-a-vdo-lv.adoc[leveloffset=+1]
endif::[]

ifdef::parent-context-of-creating-logical-volumes[:context: {parent-context-of-creating-logical-volumes}]
ifndef::parent-context-of-creating-logical-volumes[:!context:]

