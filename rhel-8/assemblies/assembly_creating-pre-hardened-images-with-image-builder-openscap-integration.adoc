ifdef::context[:parent-context-of-assembly_creating-pre-hardened-images-with-image-builder-openscap-integration: {context}]

:_mod-docs-content-type: ASSEMBLY


ifndef::context[]
[id="assembly_creating-pre-hardened-images-with-image-builder-openscap-integration"]
endif::[]
ifdef::context[]
[id="assembly_creating-pre-hardened-images-with-image-builder-openscap-integration_{context}"]
endif::[]
= Creating pre-hardened images with RHEL image builder OpenSCAP integration

:context: assembly_creating-pre-hardened-images-with-image-builder-openscap-integration

With the RHEL image builder on-premise support for the OpenSCAP integration, you can create customized blueprints with specific security profiles, and use the blueprints to build your pre-hardened images. You can then use this pre-hardened image to deploy systems that need to be compliant with a specific profile. You can add a set of packages or add-on files to customize your blueprints. With that, you can build a pre-hardened customized RHEL image ready to deploy compliant systems.

During the image build process, an OSBuild `oscap.remediation` stage runs the OpenSCAP tool in the `chroot` environment, on the filesystem tree. The OpenSCAP tool runs the standard evaluation for the profile you choose and applies the remediations to the image. With this, you can build an image that can be configured according to the security profile requirements even before it boots for the first time.

Red&nbsp;Hat provides regularly updated versions of the security hardening profiles that you can choose when you build your systems so that you can meet your current deployment guidelines.


include::modules/composer/con_the-openscap-blueprint-customization.adoc[leveloffset=+1]

include::modules/composer/proc_creating-a-pre-hardened-image-with-image-builder.adoc[leveloffset=+1]

include::modules/composer/proc_adding-customized-tailoring-options-for-a-profile-to-the-blueprint.adoc[leveloffset=+1]



ifdef::parent-context-of-assembly_creating-pre-hardened-images-with-image-builder-openscap-integration[:context: {parent-context-of-assembly_creating-pre-hardened-images-with-image-builder-openscap-integration}]
ifndef::parent-context-of-assembly_creating-pre-hardened-images-with-image-builder-openscap-integration[:!context:]
