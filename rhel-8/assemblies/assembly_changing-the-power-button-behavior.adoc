:_newdoc-version: 2.17.0
:_template-generated: 2024-05-03

ifdef::context[:parent-context-of-changing-the-power-button-behavior: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="changing-the-power-button-behavior"]
endif::[]
ifdef::context[]
[id="changing-the-power-button-behavior_{context}"]
endif::[]
= Changing the power button behavior

:context: changing-the-power-button-behavior

When you press the power button on your computer, it suspends or shuts down the system by default. You can customize this behavior according to your preferences.

include::modules/desktop/proc_changing-the-power-button-behavior-in-systemd.adoc[leveloffset=+1]

include::modules/desktop/proc_changing-the-power-button-behavior-in-gnome.adoc[leveloffset=+1]


ifdef::parent-context-of-changing-the-power-button-behavior[:context: {parent-context-of-changing-the-power-button-behavior}]
ifndef::parent-context-of-changing-the-power-button-behavior[:!context:]

