ifdef::context[:parent-context-of-configuring-aws-network-resource-agents: {context}]

:_content-type: ASSEMBLY

ifndef::context[]
[id="configuring-aws-network-resource-agents"]
endif::[]
ifdef::context[]
[id="configuring-aws-network-resource-agents_{context}"]
endif::[]
// TODO: Change ID if the new title gets green-lit

= Setting up IP address resources on AWS


:context: configuring-aws-network-resource-agents

To ensure that clients that use IP addresses to access resources managed by the cluster over the network can access the resources if a failover occurs, the cluster must include _IP address resources_, which use specific network resource agents.

The RHEL HA Add-On provides a set of resource agents, which create IP address resources to manage various types of IP addresses on AWS. To decide which resource agent to configure, consider the type of AWS IP addresses that you want the HA cluster to manage:

* If you need to manage an IP address exposed to the internet, xref:creating-a-cluster-resource-to-manage-an-external-ip-address-exposed-to-the-internet_configuring-aws-network-resource-agents[use the `awseip` network resource].

* If you need to manage a private IP address limited to a single AWS Availability Zone (AZ), xref:creating-a-cluster-resource-to-manage-a-private-ip-address-limited-to-a-single-aws-availability-zone_configuring-aws-network-resource-agents[use the `awsvip` and `IPaddr2` network resources].

* If you need to manage an IP address that can move across multiple AWS AZs within the same AWS region, xref:creating-a-cluster-resource-to-manage-an-ip-address-that-can-move-across-multiple-aws-availability-zones_configuring-aws-network-resource-agents[use the `aws-vpc-move-ip` network resource].

// * `aws-vpc-route53` - used for DNS failover - TODO - WHAT IS THIS RELEVANT FOR? WHERE TO MENTION IT?

[NOTE]
====
If the HA cluster does not manage any IP addresses, the resource agents for managing virtual IP addresses on AWS are not required. If you need further guidance for your specific deployment, consult with your AWS provider.
====

include::modules/cloud/aws/proc_creating-a-cluster-resource-to-manage-an-external-ip-address-exposed-to-the-internet.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_creating-a-cluster-resource-to-manage-a-private-ip-address-limited-to-a-single-aws-availability-zone.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_creating-a-cluster-resource-to-manage-an-ip-address-that-can-move-across-multiple-aws-availability-zones.adoc[leveloffset=+1]


////
TODO find out:

* Why use the same resource group for the individual resources? Can it be left out? Maybe define constrainsn explicitly?
* Whete did IPaddr2 come from in the procedures? Why is it in the cluster status without being used the commands in 3.10.1, but has to be used in the commands in 3.10.2 and 3.10.3?
////

[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/assembly_overview-of-high-availability-configuring-and-managing-high-availability-clusters[High Availability Add-On overview]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_high_availability_clusters/index[Configuring and managing high availability clusters]
* link:https://access.redhat.com/articles/3569621#configure-overlayip[An example of an configuration for `aws-vpc-move-ip`]
* link:http://www.scalingbits.com/index.php/aws/dnsfailover[DNS Name Failover for Highly Available AWS Services using `aws-vpc-route53`]


ifdef::parent-context-of-configuring-aws-network-resource-agents[:context: {parent-context-of-configuring-aws-network-resource-agents}]
ifndef::parent-context-of-configuring-aws-network-resource-agents[:!context:]

