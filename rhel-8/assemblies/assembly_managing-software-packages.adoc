:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-managing-software-packages: {context}]

[id="managing-software-packages_{context}"]

= Managing software packages

:context: managing-software-packages

include::modules/core-services/con_software-management-tools-in-rhel.adoc[leveloffset=+1]

include::modules/core-services/con_application-streams.adoc[leveloffset=+1]

// include::modules/core-services/con_intro-to-yum-functionality.adoc[leveloffset=+1]

include::assembly_searching-for-software-packages.adoc[leveloffset=+1]

include::assembly_installing-software-packages.adoc[leveloffset=+1]

include::assembly_updating-software-packages.adoc[leveloffset=+1]

include::assembly_uninstalling-software-packages.adoc[leveloffset=+1]

include::assembly_managing-software-package-groups.adoc[leveloffset=+1]

include::assembly_handling-package-management-history.adoc[leveloffset=+1]

include::assembly_managing-software-repositories.adoc[leveloffset=+1]

include::assembly_configuring-yum.adoc[leveloffset=+1]


ifdef::parent-context-of-managing-software-packages[:context: {parent-context-of-managing-software-packages}]
ifndef::parent-context-of-managing-software-packages[:!context:]
