:_mod-docs-content-type: ASSEMBLY
include::modules/identity-management/_attributes-identity-management-and-access-control.adoc[]
//:internal:

:parent-context-of-planning-integration-with-ad: {context}

[id="planning-integration-with-ad_{context}"]
= Planning integration with AD

:context: planning-integration-with-ad

The following sections introduce the options for integrating {RHEL} with {AD} (AD).

////
.Additional resources

For details on the architecture of AD forests, see link:++https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc759073(v=ws.10)++[What are Domains and Forests?] on TechNet.
////

include::modules/identity-management/con_direct-integration-of-linux-systems-into-active-directory.adoc[leveloffset=+1]

include::modules/identity-management/con_indirect-integration-of-linux-systems-into-active-directory-by-using-identity-management.adoc[leveloffset=+1]

include::modules/identity-management/con_guidelines-for-deciding-between-direct-and-indirect-integration.adoc[leveloffset=+1]

//include::modules/identity-management/ref_services_ports_in_ad.adoc[leveloffset=+1]

:context: {parent-context-of-planning-integration-with-ad}
