:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-assessing-upgrade-suitability: {context}]

// Base the file name and the ID on the assembly title. For example:
// * file name: my-assembly-a.adoc
// * ID: [id="my-assembly-a"]
// * Title: = My assembly A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules_{context}"].
ifndef::context[]
[id="assessing-upgrade-suitability"]
endif::[]
ifdef::context[]
[id="assessing-upgrade-suitability_{context}"]
endif::[]
= Assessing upgrade suitability
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: assessing-upgrade-suitability
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

The Preupgrade Assistant assesses your system for any potential problems that might occur during an in-place upgrade before any changes to your system are made. 

The Preupgrade Assistant does the following:

* Leaves your system unchanged except for storing information or logs. It does not modify the assessed system.
* Assesses the system for possible in-place upgrade limitations, such as package removals, incompatible obsoletes, name changes, or deficiencies in some configuration file compatibilities.
* Provides a report with the assessment result.
* Provides post-upgrade scripts to address more complex problems after the in-place upgrade.

You should run the Preupgrade Assistant multiple times. Always run the Preupgrade Assistant after you resolve problems identified by the pre-upgrade report to ensure that no critical problems remain before performing the upgrade.

You can review the system assessment results using one of the following methods:

* Locally on the assessed system using the command line.
* Remotely over the network using the web user interface (UI). You can use the web UI to view multiple reports at once. 

[IMPORTANT]
====
The Preupgrade Assistant is a modular system. You can create your own custom modules to assess the possibility of performing an in-place upgrade. For more information, see link:https://access.redhat.com/node/1224543[How to create custom Preupgrade Assistant modules for upgrading from RHEL 6 to RHEL 7].
====

include::modules/upgrades-and-differences/proc_assessing-upgrade-suitability-from-the-command-line.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/proc_assessing-upgrade-suitability-from-a-web-ui.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_assessment-result-codes.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-assessing-upgrade-suitability[:context: {parent-context-of-assessing-upgrade-suitability}]
ifndef::parent-context-of-assessing-upgrade-suitability[:!context:]

