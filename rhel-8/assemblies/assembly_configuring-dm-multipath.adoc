ifdef::context[:parent-context-of-configuring-dm-multipath: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-dm-multipath"]
endif::[]
ifdef::context[]
[id="configuring-dm-multipath_{context}"]
endif::[]
= Configuring DM Multipath

:context: configuring-dm-multipath

You can set up DM Multipath with the `mpathconf` utility. This utility creates or edits the `/etc/multipath.conf` multipath configuration file based on the following scenarios:

* If the `/etc/multipath.conf` file already exists, the `mpathconf` utility will edit it.
* If the `/etc/multipath.conf` file does not exist, the `mpathconf` utility will create the `/etc/multipath.conf` file from scratch.

include::modules/filesystems-and-storage/proc_checking-for-the-device-mapper-multipath-package.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_setting-up-basic-failover-configuration-with-dm-multipath.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_ignoring-local-disks-for-multipathing.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_configuring-additional-storage-with-dm-multipath.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_configuring-multipathing-in-initramfs.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-dm-multipath[:context: {parent-context-of-configuring-dm-multipath}]
ifndef::parent-context-of-configuring-dm-multipath[:!context:]
