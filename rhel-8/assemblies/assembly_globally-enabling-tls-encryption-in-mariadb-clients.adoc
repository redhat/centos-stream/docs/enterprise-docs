:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_globally-enabling-tls-encryption-in-mariadb-clients: {context}]

ifndef::context[]
[id="assembly_globally-enabling-tls-encryption-in-mariadb-clients"]
endif::[]
ifdef::context[]
[id="assembly_globally-enabling-tls-encryption-in-mariadb-clients_{context}"]
endif::[]
= Globally enabling TLS encryption in MariaDB clients

:context: assembly_globally-enabling-tls-encryption-in-mariadb-clients

[role="_abstract"]
If your *MariaDB* server supports TLS encryption, configure your clients to establish only secure connections and to verify the server certificate. This procedure describes how to enable TLS support for all users on the server.

include::modules/core-services/proc_configuring-the-mariadb-client-to-use-tls-encryption-by-default.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_globally-enabling-tls-encryption-in-mariadb-clients[:context: {parent-context-of-assembly_globally-enabling-tls-encryption-in-mariadb-clients}]
ifndef::parent-context-of-assembly_globally-enabling-tls-encryption-in-mariadb-clients[:!context:]
