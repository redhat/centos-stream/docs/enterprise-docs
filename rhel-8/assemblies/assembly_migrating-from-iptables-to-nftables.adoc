:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_migrating-from-iptables-to-nftables: {context}]

ifndef::context[]
[id="assembly_migrating-from-iptables-to-nftables"]
endif::[]
ifdef::context[]
[id="assembly_migrating-from-iptables-to-nftables_{context}"]
endif::[]
= Migrating from iptables to nftables

:context: assembly_migrating-from-iptables-to-nftables

[role="_abstract"]
If your firewall configuration still uses `iptables` rules, you can migrate your `iptables` rules to `nftables`.

ifeval::[{ProductNumber} == 9]
[IMPORTANT]
====
The `ipset` and `iptables-nft` packages have been deprecated in Red Hat Enterprise Linux 9. This includes deprecation of `nft-variants` such as `iptables`, `ip6tables`, `arptables`, and `ebtables` utilities. If you are using any of these tools, for example, because you upgraded from an earlier RHEL version, Red Hat recommends migrating to the `nft` command line tool provided by the `nftables` package.
====
endif::[]


include::modules/packet-filtering/con_when-to-use-firewalld-nftables-or-iptables.adoc[leveloffset=+1]

include::modules/packet-filtering/con_concepts-in-the-nftables-framework.adoc[leveloffset=+1]

include::modules/packet-filtering/con_concepts-in-the-deprecated-iptables-framework.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_converting-iptables-and-ip6tables-rule-sets-to-nftables.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_converting-single-iptables-and-ip6tables-rules-to-nftables.adoc[leveloffset=+1]

include::modules/packet-filtering/ref_comparison-of-common-iptables-and-nftables-commands.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_migrating-from-iptables-to-nftables[:context: {parent-context-of-assembly_migrating-from-iptables-to-nftables}]
ifndef::parent-context-of-assembly_migrating-from-iptables-to-nftables[:!context:]

