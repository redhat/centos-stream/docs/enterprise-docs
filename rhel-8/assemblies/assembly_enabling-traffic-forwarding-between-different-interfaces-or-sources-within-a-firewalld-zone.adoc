:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-assembly_enabling-traffic-forwarding-between-different-interfaces-or-sources-within-a-firewalld-zone: {context}]

ifndef::context[]
[id="assembly_enabling-traffic-forwarding-between-different-interfaces-or-sources-within-a-firewalld-zone"]
endif::[]
ifdef::context[]
[id="assembly_enabling-traffic-forwarding-between-different-interfaces-or-sources-within-a-firewalld-zone_{context}"]
endif::[]
= Enabling traffic forwarding between different interfaces or sources within a firewalld zone

:context: assembly_enabling-traffic-forwarding-between-different-interfaces-or-sources-within-a-firewalld-zone

[role="_abstract"]
Intra-zone forwarding is a [systemitem]`firewalld` feature that enables traffic forwarding between interfaces or sources within a [systemitem]`firewalld` zone.


include::modules/packet-filtering/con_the-difference-between-intra-zone-forwarding-and-zones-with-the-default-target-set-to-accept.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_using-intra-zone-forwarding-to-forward-traffic-between-an-ethernet-and-wi-fi-network.adoc[leveloffset=+1]



ifdef::parent-context-of-assembly_enabling-traffic-forwarding-between-different-interfaces-or-sources-within-a-firewalld-zone[:context: {parent-context-of-assembly_enabling-traffic-forwarding-between-different-interfaces-or-sources-within-a-firewalld-zone}]
ifndef::parent-context-of-assembly_enabling-traffic-forwarding-between-different-interfaces-or-sources-within-a-firewalld-zone[:!context:]
