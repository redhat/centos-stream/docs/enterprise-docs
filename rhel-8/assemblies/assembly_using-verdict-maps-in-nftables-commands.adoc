:_mod-docs-content-type: ASSEMBLY
:parent-context-of-using-verdict-maps-in-nftables-commands: {context}

[id='using-verdict-maps-in-nftables-commands_{context}']
= Using verdict maps in nftables commands

:context: using-verdict-maps-in-nftables-commands

[role="_abstract"]
Verdict maps, which are also known as dictionaries, enable `nft` to perform an action based on packet information by mapping match criteria to an action.


include::modules/packet-filtering/proc_using-anonymous-maps-in-nftables.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_using-named-maps-in-nftables.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* The `Maps` section in the `nft(8)` man page on your system

:context: {parent-context-of-using-verdict-maps-in-nftables-commands}

