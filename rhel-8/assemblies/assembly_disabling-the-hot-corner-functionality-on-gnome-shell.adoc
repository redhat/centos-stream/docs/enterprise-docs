:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-disabling-the-hot-corner-functionality-on-gnome-shell: {context}]

[id="disabling-the-hot-corner-functionality-on-gnome-shell_{context}"]
= Disabling the hot corner functionality on GNOME Shell

:context: disabling-the-hot-corner-functionality-on-gnome-shell

// The GNOME environment provides the hot corner functionality, which is enabled by default. This means that when you roll the cursor over the area of the top left corner, the `Activities Overview` menu opens automatically.

The GNOME environment provides the hot corner functionality, which is enabled by default.
This means that when you move the cursor to the area of the upper-left corner and push the cursor to the screen corner, the *Activities Overview* menu opens automatically.

However, you may want to disable this feature to not open *Activities Overview* unintentionally.

To do so, you can use the following tools:

* The [application]*dconf Editor* application
* The [application]*gsettings* command-line utility
* The [application]*No topleft hot corner* extension

The selection of the tool might depend on whether you want to disable the hot corner functionality for a single user or for all users on the system.
By using [application]*dconf Editor* or [application]*gsettings*, you can disable hot corner only for a single user.
To disable hot corner system-wide, use the [application]*No topleft hot corner* extension.


include::assembly_disabling-the-hot-corner-functionality-for-a-single-user.adoc[leveloffset=+1]

include::modules/desktop/proc_disabling-the-hot-corner-functionality-for-all-users.adoc[leveloffset=+1]




:context: {parent-context-of-disabling-the-hot-corner-functionality-on-gnome-shell}

ifdef::parent-context-of-disabling-the-hot-corner-functionality-on-gnome-shell[:context: {parent-context-of-disabling-the-hot-corner-functionality-on-gnome-shell}]
ifndef::parent-context-of-disabling-the-hot-corner-functionality-on-gnome-shell[:!context:]
