:_newdoc-version: 2.15.0
:_template-generated: 2023-11-6

ifdef::context[:parent-context-of-ensuring-support-for-common-encryption-types-in-ad-and-rhel: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="ensuring-support-for-common-encryption-types-in-ad-and-rhel"]
endif::[]
ifdef::context[]
[id="ensuring-support-for-common-encryption-types-in-ad-and-rhel_{context}"]
endif::[]
= Ensuring support for common encryption types in AD and RHEL

:context: ensuring-support-for-common-encryption-types-in-ad-and-rhel

[role="_abstract"]

By default, Identity Management establishes a cross-realm trust with support for AES-128 and AES-256 Kerberos encryption types. Additionally, by default SSSD and Samba Winbind support AES-128 and AES-256 Kerberos encryption types.

RC4 encryption is deprecated and disabled by default since RHEL 8.3 and RHEL 9, as it is considered less secure than the newer AES-128 and AES-256 encryption types. In contrast, Active Directory (AD) user credentials and trusts between AD domains support RC4 encryption and they might not support all AES encryption types.

Without any common encryption types, communication between RHEL hosts and AD domains might not work, or some AD accounts might not be able to authenticate. To address this situation, perform one of the configurations outlined in the following sections.

[IMPORTANT]
====
If IdM is in FIPS mode, the IdM-AD integration does not work due to AD only supporting the use of RC4 or AES HMAC-SHA1 encryptions, while RHEL 9 in FIPS mode allows only AES HMAC-SHA2 by default.
For more information, see the link:https://access.redhat.com/solutions/7003853[AD Domain Users unable to login in to the FIPS-compliant environment] KCS solution.

IdM does not support the more restrictive `FIPS:OSPP` crypto policy, which should only be used on Common Criteria evaluated systems.
====

ifeval::[{ProductNumber} == 9]
[NOTE]
====
Establishing a two-way cross-forest trust between AD and Identity Management IdM with FIPS mode enabled fails because the New Technology LAN Manager Security Support Provider (NTLMSSP) authentication is not FIPS-compliant. IdM in FIPS mode does not accept the RC4 NTLM hash that the AD domain controller uses when attempting to authenticate.
====
endif::[]

include::modules/identity-management/proc_enabling-aes-encryption-in-ad.adoc[leveloffset=+1]

include::modules/core-services/proc_enabling-the-aes-encryption-type-in-active-directory-using-a-gpo.adoc[leveloffset=+1]

include::modules/identity-management/proc_enabling-rc4-support-in-rhel.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening[Using system-wide cryptographic policies]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/planning-a-cross-forest-trust-between-idm-and-ad_planning-identity-management#trust-controllers-and-trust-agents_planning-a-cross-forest-trust-between-idm-and-ad[Trust controllers and trust agents]

ifdef::parent-context-of-ensuring-support-for-common-encryption-types-in-ad-and-rhel[:context: {parent-context-of-ensuring-support-for-common-encryption-types-in-ad-and-rhel}]
ifndef::parent-context-of-ensuring-support-for-common-encryption-types-in-ad-and-rhel[:!context:]
