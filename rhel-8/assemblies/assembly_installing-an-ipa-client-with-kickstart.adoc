:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id="installing-an-ipa-client-with-kickstart_{context}"]

= Installing an IdM client with Kickstart

:context: client-kickstart

[role="_abstract"]
A Kickstart enrollment automatically adds a new system to the {IPA} (IdM) domain at the time Red{nbsp}Hat Enterprise{nbsp}Linux is installed.
// See link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Installation_Guide/chap-kickstart-installations.html++[Kickstart Installations] in the [citetitle]_Installation Guide_ for details on Kickstart.

include::modules/identity-management/proc_installing-a-client-with-kickstart.adoc[leveloffset=+1]

include::modules/identity-management/con_kickstart-file-for-client-installation.adoc[leveloffset=+1]

//include::modules/identity-management/con_removing-pre-ipa-configuration-after-client1.adoc[leveloffset=+1]

include::modules/identity-management/proc_testing-an-ipa-client.adoc[leveloffset=+1]


:context: {parent-context}
