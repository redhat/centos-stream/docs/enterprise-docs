ifdef::context[:parent-context-of-assembly_troubleshooting-certificate-mapping-and-matching-rules: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_troubleshooting-certificate-mapping-and-matching-rules"]
endif::[]
ifdef::context[]
[id="assembly_troubleshooting-certificate-mapping-and-matching-rules_{context}"]
endif::[]
= Troubleshooting certificate mapping and matching rules

:context: assembly_troubleshooting-certificate-mapping-and-matching-rules

[role="_abstract"]
If you are having issues authenticating with a smart card, check that you have linked your smart card certificate correctly to a user. By default, a certificate is associated with a user when the user entry contains the full certificate as part of the `usercertificate` attribute. However, if you have defined certificate mapping rules, you may have changed how certificates are associated with users. To troubleshoot certificate mapping and matching rules, refer to the following sections:

* xref:proc_checking-how-the-certificates-are-mapped-to-users_{context}[Checking how the certificates are mapped to users]
* xref:proc_checking-the-user-associated-with-a-smart-card-certificate_{context}[Checking the user associated with a smart card certificate]

[NOTE]
====
If you are using your smart card to authenticate using SSH, you need to add the full certificate to the user entry in Identity Management (IdM). If you are not using your smart card to authenticate using SSH, you can add certificate mapping data using the `ipa user-add-certmapdata` command.
====


include::modules/identity-management/proc_checking-how-the-certificates-are-mapped-to-users.adoc[leveloffset=+1]

include::modules/identity-management/proc_checking-the-user-associated-with-a-smart-card-certificate.adoc[leveloffset=+1]



ifdef::parent-context-of-assembly_troubleshooting-certificate-mapping-and-matching-rules[:context: {parent-context-of-assembly_troubleshooting-certificate-mapping-and-matching-rules}]
ifndef::parent-context-of-assembly_troubleshooting-certificate-mapping-and-matching-rules[:!context:]
