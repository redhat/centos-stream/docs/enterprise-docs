:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly-configuring-resources-to-remain-stopped: {context}
  
[id='assembly_configuring-resources-to-remain-stopped-{context}']
= Configuring resources to remain stopped on clean node shutdown
:context: configuring-resources-to-remain-stopped

[role="_abstract"]
When a cluster node shuts down, Pacemaker's default response is to stop all resources running on that node and recover them elsewhere, even if the shutdown is a clean shutdown.
ifeval::[{ProductNumber} == 8]
As of RHEL 8.2, you can configure Pacemaker so that
endif::[]
ifeval::[{ProductNumber} == 9]
You can configure Pacemaker so that
endif::[]
when a node shuts down cleanly, the resources attached to the node will be locked to the node and unable to start elsewhere until  they start again when the node that has shut down rejoins the cluster. This allows you to power down nodes during maintenance windows when service outages are acceptable without causing that node's resources to fail over to other nodes in the cluster.

include::modules/high-availability/ref_cluster-properties-shutdown-lock.adoc[leveloffset=+1]

include::modules/high-availability/proc_setting-shutdown-lock.adoc[leveloffset=+1]

:context: {parent-context-of-assembly-configuring-resources-to-remain-stopped}


