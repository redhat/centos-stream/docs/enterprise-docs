:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-configuring-huge-pages: {context}]

ifndef::context[]
[id="configuring-huge-pages"]
endif::[]
ifdef::context[]
[id="configuring-huge-pages_{context}"]
endif::[]
= Configuring huge pages

:context: configuring-huge-pages

[role="_abstract"]
Physical memory is managed in fixed-size chunks called pages. On the x86_64 architecture, supported by {RHEL} {ProductNumber}, the default size of a memory page is `4 KB`. This default page size has proved to be suitable for general-purpose operating systems, such as Red Hat Enterprise Linux, which supports many different kinds of workloads.

However, specific applications can benefit from using larger page sizes in certain cases. For example, an application that works with a large and relatively fixed data set of hundreds of megabytes or even dozens of gigabytes can have performance issues when using `4 KB` pages. Such data sets can require a huge amount of `4 KB` pages, which can lead to overhead in the operating system and the CPU.

This section provides information about huge pages available in RHEL {ProductNumber} and how you can configure them.

include::modules/performance/con_available-hugepage-features.adoc[leveloffset=+1]

include::modules/performance/ref_parameters-for-reserving-hugetlb-pages-at-boot-time.adoc[leveloffset=+1]

include::modules/performance/proc_configuring-hugetlb-at-boot-time.adoc[leveloffset=+1]

include::modules/performance/ref_parameters-for-reserving-hugetlb-pages-at-run-time.adoc[leveloffset=+1]

include::modules/performance/proc_configuring-hugetlb-at-run-time.adoc[leveloffset=+1]

include::modules/performance/con_managing-transparent-hugepages.adoc[leveloffset=+1]

include::modules/performance/proc_managing-transparent-hugepages-with-runtime-configuration.adoc[leveloffset=+2]

include::modules/performance/proc_managing-transparent-hugepages-with-tuned-profiles.adoc[leveloffset=+2]

include::modules/performance/proc_managing-transparent-hugepages-with-kernel-command-line-parameters.adoc[leveloffset=+2]

include::modules/performance/proc_managing-transparent-hugepages-with-a-systemd-unit-file.adoc[leveloffset=+2]

[role="_additional-resources"]
=== Additional resources
* You can also disable Transparent Huge Pages (THP) by setting up TuneD profile or using predefined TuneD profiles. See xref:tuned-profiles-distributed-with-rhel_getting-started-with-tuned[TuneD profiles distributed with RHEL] and xref:available-tuned-plug-ins_customizing-tuned-profiles[Available TuneD plug-ins].

include::modules/performance/con_impact-of-page-size-on-translation-lookaside-buffer-size.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-configuring-huge-pages[:context: {parent-context-of-configuring-huge-pages}]
ifndef::parent-context-of-configuring-huge-pages[:!context:]
