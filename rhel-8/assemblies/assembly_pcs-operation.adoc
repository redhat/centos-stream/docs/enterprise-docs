:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id='assembly_pcs-operation-{context}']
= The pcs command-line interface
:context: pcs-operation

[role="_abstract"]
The [command]`pcs` command-line interface controls and configures cluster services such as `corosync`, `pacemaker`,`booth`, and `sbd` by providing an easier interface to their configuration files.

Note that you should not edit the `cib.xml` configuration file directly. In most cases, Pacemaker will reject a directly modified `cib.xml` file.

include::modules/high-availability/proc_pcs-help.adoc[leveloffset=+1]

include::modules/high-availability/proc_raw-config.adoc[leveloffset=+1]

include::modules/high-availability/proc_configure-testfile.adoc[leveloffset=+1]

include::modules/high-availability/proc_cluster-status.adoc[leveloffset=+1]

include::modules/high-availability/proc_config-display.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 9]
include::modules/high-availability/proc_resource-status.adoc[leveloffset=+1]
endif::[]

include::modules/high-availability/proc_pcs-corosync-manage.adoc[leveloffset=+1]

include::modules/high-availability/proc_pcs-corosync-display.adoc[leveloffset=+1]



:context: {parent-context}

