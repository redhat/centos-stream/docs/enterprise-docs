ifdef::context[:parent-context-of-configuring-lvm-on-shared-storage: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-lvm-on-shared-storage"]
endif::[]
ifdef::context[]
[id="configuring-lvm-on-shared-storage_{context}"]
endif::[]
= Configuring LVM on shared storage

:context: configuring-lvm-on-shared-storage

[role="_abstract"]

Shared storage is storage that can be accessed by multiple nodes at the same time. You can use LVM to manage shared storage. Shared storage is commonly used in cluster and high-availability setups and there are two common scenarios for how shared storage appears on the system:

* LVM devices are attached to a host and passed to a guest VM to use. In this case, the device is never intended to be used by the host, only by the guest VM.

* Machines are attached to a storage area network (SAN), for example using Fiber Channel, and the SAN LUNs are visible to multiple machines:

include::modules/filesystems-and-storage/proc_configuring-lvm-on-VM.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_configuring-lvm-on-san-one-machine.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_configuring-lvm-on-san-failover.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_configuring-lvm-on-san-multiple.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_creating-shared-lvm-devices-using-storage-system-role.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-lvm-on-shared-storage[:context: {parent-context-of-configuring-lvm-on-shared-storage}]
ifndef::parent-context-of-configuring-lvm-on-shared-storage[:!context:]
