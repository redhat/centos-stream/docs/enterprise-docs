:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// configuring-and-managing-virtualization (included in master.adoc)

:parent-context-of-managing-virtual-devices: {context}

[id="managing-virtual-devices_{context}"]
= Managing virtual devices

:context: managing-virtual-devices

ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
As a system or virtualization administrator, I want to configure what
| Jira |
https://projects.engineering.redhat.com/browse/RHELPLAN-5973
| BZ |
https://bugzilla.redhat.com/show_bug.cgi?id=1589965
| SMEs |
Christophe de Dinechin, TBD
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

One of the most effective ways to manage the functionality, features, and performance of a virtual machine (VM) is to adjust its _virtual devices_.

The following sections provide a xref:how-virtual-devices-work_managing-virtual-devices[general overview] of what virtual devices are, and instructions on how to manage them using the xref:assembly_managing-virtual-devices-using-the-cli_managing-virtual-devices[CLI] or the xref:assembly_managing-virtual-devices-using-the-web-console_managing-virtual-devices[web console].

////
.Prerequisites

* A virtual machine xref:assembly_creating-virtual-machines_virt-getting-started[created and installed].
////

include::modules/virtualization/con_how-virtual-devices-work.adoc[leveloffset=+1]

//include::modules/virtualization/proc_viewing-devices-attached-to-virtual-machines-using-the-web-console.adoc[leveloffset=+1]

include::modules/virtualization/ref_types-of-virtual-devices.adoc[leveloffset=+1]

//include::modules/virtualization/proc_attaching-devices-to-virtual-machines.adoc[leveloffset=+1]

//include::modules/virtualization/proc_modifying-devices-attached-to-virtual-machines.adoc[leveloffset=+1]

//include::modules/virtualization/proc_removing-devices-from-virtual-machines.adoc[leveloffset=+1]

include::assembly_managing-virtual-devices-using-the-cli.adoc[leveloffset=+1]

include::assembly_managing-virtual-devices-using-the-web-console.adoc[leveloffset=+1]

include::assembly_managing-virtual-usb-devices.adoc[leveloffset=+1]


include::assembly_managing-virtual-optical-drives.adoc[leveloffset=+1]

include::assembly_managing-sr-iov-devices.adoc[leveloffset=+1]

//Reverting changes. See:BZ#2142492
//include::assembly_managing-virtual-scsi-devices.adoc[leveloffset=+1]


include::modules/virtualization/proc_attaching-dasd-devices-to-virtual-machines-on-ibm-z.adoc[leveloffset=+1]

include::modules/virtualization/proc_attaching-a-watchdog-device-to-a-virtual-machine-using-the-web-console.adoc[leveloffset=+1]

include::modules/virtualization/proc_attaching-zpci-devices-to-virtual-machines-on-ibm-z.adoc[leveloffset=+1]


////
[id='related-information-{context}']
== Related information

* TBD?
////

:context: {parent-context-of-managing-virtual-devices}
