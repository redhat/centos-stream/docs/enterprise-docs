ifdef::context[:parent-context-of-assembly_protecting-grub-with-a-password: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_protecting-grub-with-a-password"]
endif::[]
ifdef::context[]
[id="assembly_protecting-grub-with-a-password_{context}"]
endif::[]
= Protecting GRUB with a password


:context: assembly_protecting-grub-with-a-password

You can protect GRUB with a password in two ways:

* Password is required for modifying menu entries but not for booting existing menu entries.
* Password is required for modifying menu entries and for booting existing menu entries.


include::modules/core-kernel/proc_setting-password-protection-only-for-modifying-menu-entries.adoc[leveloffset=+1]

include::modules/core-kernel/proc_setting-password-protection-for-modifying-and-booting-menu-entries.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_protecting-grub-with-a-password[:context: {parent-context-of-assembly_protecting-grub-with-a-password}]
ifndef::parent-context-of-assembly_protecting-grub-with-a-password[:!context:]
