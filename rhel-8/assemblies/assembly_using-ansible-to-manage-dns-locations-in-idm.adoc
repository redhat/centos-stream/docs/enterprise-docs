:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-using-ansible-to-manage-dns-locations-in-idm: {context}]

[id="using-ansible-to-manage-dns-locations-in-idm_{context}"]
= Using Ansible to manage DNS locations in IdM
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: using-ansible-to-manage-dns-locations-in-idm
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

[role="_abstract"]
As Identity Management (IdM) administrator, you can manage IdM DNS locations using the `location` module available in the `ansible-freeipa` package. 

* xref:dns-based-service-discovery_{context}[DNS-based service discovery]
* xref:deployment-considerations-for-dns-locations_{context}[Deployment considerations for DNS locations]
* xref:dns-time-to-live-ttl_{context}[DNS time to live (TTL)]
* xref:using-ansible-to-ensure-an-idm-location-is-present_{context}[Using Ansible to ensure an IdM location is present]
* xref:using-ansible-to-ensure-an-idm-location-is-absent_{context}[Using Ansible to ensure an IdM location is absent]

include::modules/identity-management/con_dns-based-service-discovery.adoc[leveloffset=+1]

include::modules/identity-management/con_deployment-considerations-for-dns-locations.adoc[leveloffset=+1]

include::modules/identity-management/con_dns-time-to-live-ttl.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-an-idm-location-is-present.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-an-idm-location-is-absent.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* See the `README-location.md` file in the `/usr/share/doc/ansible-freeipa/` directory.
* See sample Ansible playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/location` directory.


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-using-ansible-to-manage-dns-locations-in-idm[:context: {parent-context-of-using-ansible-to-manage-dns-locations-in-idm}]
ifndef::parent-context-of-using-ansible-to-manage-dns-locations-in-idm[:!context:]
