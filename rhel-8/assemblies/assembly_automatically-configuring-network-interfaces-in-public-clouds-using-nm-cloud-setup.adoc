:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-automatically-configuring-network-interfaces-in-public-clouds-using-nm-cloud-setup: {context}]

[id="automatically-configuring-network-interfaces-in-public-clouds-using-nm-cloud-setup_{context}"]
= Automatically configuring network interfaces in public clouds using nm-cloud-setup

[role="_abstract"]
Usually, a virtual machine (VM) has only one interface that is configurable by DHCP. However, DHCP cannot configure VMs with multiple network entities, such as interfaces, IP subnets, and IP addresses. Additionally, you cannot apply settings when the VM instance is running. To solve this runtime configuration issue, the `nm-cloud-setup` utility automatically retrieves configuration information from the metadata server of the cloud service provider and updates the network configuration of the host. The utility automatically picks up multiple network interfaces, multiple IP addresses, or IP subnets on one interface and helps to reconfigure the network of the running VM instance.

:context: automatically-configuring-network-interfaces-in-public-clouds-using-nm-cloud-setup

include::modules/networking/proc_configuring-and-pre-deploying-nm-cloud-setup.adoc[leveloffset=+1]

include::modules/networking/con_understanding-the-role-of-imdsv2-and-nm-cloud-setup-in-the-rhel-ec2-instance.adoc[leveloffset=+1]

ifdef::parent-context-of-automatically-configuring-network-interfaces-in-public-clouds-using-nm-cloud-setup[:context: {parent-context-of-automatically-configuring-network-interfaces-in-public-clouds-using-nm-cloud-setup}]
ifndef::parent-context-of-automatically-configuring-network-interfaces-in-public-clouds-using-nm-cloud-setup[:!context:]
