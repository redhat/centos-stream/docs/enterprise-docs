:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// managing-storage-for-virtual-machines

ifdef::context[:parent-context-of-assembly_parameters-for-creating-storage-pools: {context}]


ifndef::context[]
[id="assembly_parameters-for-creating-storage-pools"]
endif::[]
ifdef::context[]
[id="assembly_parameters-for-creating-storage-pools_{context}"]
endif::[]
= Parameters for creating storage pools

:context: assembly_parameters-for-creating-storage-pools

[role="_abstract"]
Based on the type of storage pool you require, you can modify its XML configuration file and define a specific type of storage pool.
This section provides information about the XML parameters required for creating various types of storage pools along with examples.

include::modules/virtualization/ref_directory-based-storage-pool-parameters.adoc[leveloffset=+1]

include::modules/virtualization/ref_disk-based-storage-pool-parameters.adoc[leveloffset=+1]

include::modules/virtualization/ref_filesystem-based-storage-pool-parameters.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 8]
include::modules/virtualization/ref_glusterfs-based-storage-pool-parameters.adoc[leveloffset=+1]

endif::[]

ifeval::[{ProductNumber} == 9]
endif::[]
include::modules/virtualization/ref_iscsi-based-storage-pool-parameters.adoc[leveloffset=+1]

include::modules/virtualization/ref_lvm-based-storage-pool-parameters.adoc[leveloffset=+1]

include::modules/virtualization/ref_nfs-based-storage-pool-parameters.adoc[leveloffset=+1]

include::modules/virtualization/ref_vhba-based-storage-pool-parameters.adoc[leveloffset=+1]




////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_parameters-for-creating-storage-pools[:context: {parent-context-of-assembly_parameters-for-creating-storage-pools}]
ifndef::parent-context-of-assembly_parameters-for-creating-storage-pools[:!context:]
