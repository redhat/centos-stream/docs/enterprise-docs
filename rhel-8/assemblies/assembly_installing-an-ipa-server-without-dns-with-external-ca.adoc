:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_installing-an-ipa-server-without-dns-with-external-ca: {context}]


ifndef::context[]
[id="assembly_installing-an-ipa-server-without-dns-with-external-ca"]
endif::[]
ifdef::context[]
[id="assembly_installing-an-ipa-server-without-dns-with-external-ca_{context}"]
endif::[]
= Installing an IdM server: Without integrated DNS, with an external CA as the root CA

:context: assembly_installing-an-ipa-server-without-dns-with-external-ca


[role="_abstract"]
You can install a new {IPA} (IdM) server, without integrated DNS, that uses an external certificate authority (CA) as the root CA.

[NOTE]
====
Install IdM-integrated DNS for basic usage within the IdM deployment. When the IdM server also manages DNS, there is tight integration between DNS and native IdM tools which enables automating some of the DNS record management.

For more details, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/planning_identity_management/planning-your-dns-services-and-host-names-planning-identity-management[Planning your DNS services and host names].
====


include::modules/identity-management/ref_options-used-when-installing-an-idm-ca-with-an-external-ca-as-the-root-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-an-ipa-server-without-dns-with-external-ca-interactive-installation.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-an-ipa-server-non-interactive-installation-without-dns-with-external-ca.adoc[leveloffset=+1]

include::modules/identity-management/ref_idm-dns-records-for-external-dns-systems.adoc[leveloffset=+1]

//[role="_additional-resources"]
//== Additional resources (or Next steps)

//* A bulleted list of links to other material closely related to the contents of the assembly, including xref links to other assemblies in your collection.

ifdef::parent-context-of-assembly_installing-an-ipa-server-without-dns-with-external-ca[:context: {parent-context-of-assembly_installing-an-ipa-server-without-dns-with-external-ca}]
ifndef::parent-context-of-assembly_installing-an-ipa-server-without-dns-with-external-ca[:!context:]
