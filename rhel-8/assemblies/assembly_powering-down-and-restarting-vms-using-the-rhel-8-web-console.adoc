:_mod-docs-content-type: ASSEMBLY
:parent-context-of-powering-down-and-restarting-vms-using-the-rhel-web-console: {context}

// This assembly is included in the following assemblies:
//
// <managing-virtual-machine-network-interfaces-using-the-web-console>

[id="powering-down-and-restarting-vms-using-the-rhel-web-console_{context}"]
= Shutting down and restarting virtual machines by using the web console

:context: powering-down-and-restarting-vms-using-the-rhel-web-console

Using the {ProductShortName} {ProductNumber} web console, you can xref:powering-down-vms-using-the-rhel-web-console_powering-down-and-restarting-vms-using-the-rhel-web-console[shut down] or xref:restarting-vms-using-the-rhel-web-console_powering-down-and-restarting-vms-using-the-rhel-web-console[restart] running virtual machines. You can also send a non-maskable interrupt to an unresponsive virtual machine.

include::modules/virtualization/proc_powering-down-vms-using-the-rhel-8-web-console.adoc[leveloffset=+1]

include::modules/virtualization/proc_restarting-vms-using-the-rhel-8-web-console.adoc[leveloffset=+1]

include::modules/virtualization/proc_sending-NMIs-to-vms-using-the-rhel-8-web-console.adoc[leveloffset=+1]

:context: {parent-context-of-powering-down-and-restarting-vms-using-the-rhel-web-console}
