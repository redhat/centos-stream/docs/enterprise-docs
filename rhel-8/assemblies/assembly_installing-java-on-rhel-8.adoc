:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// master.adoc

// This assembly can be included from other assemblies using the following
// include statement:
// include::assemblies/assembly_installing-java-on-rhel-8.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-installing-java-on-rhel-8: {context}

[id='installing-java-on-rhel-8_{context}']
= Installing Java on RHEL 8

:context: installing-java-on-rhel-8

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

Java is an environment for developing and running a wide range of platform-agnostic applications, from mobile applications to desktop and web applications and enterprise systems. Java is a general-purpose programming language. Red Hat provides an open-source implementation of the Java Platform SE (Standard Edition) called OpenJDK. It is included in the RHEL repository.

Applications are developed using the JDK (Java Development Kit). Applications are run on a JVM (Java Virtual Machine), which is included in the JRE (Java Runtime Environment) and the JDK. There is also a headless version of Java which has the smallest footprint and does not include the libraries needed for a user interface. The headless version is packaged in the `headless subpackage`.

[NOTE]
If you are unsure whether you need the JRE or the JDK, it is recommended that you install the JDK.

[NOTE]
To get short-term supported Java versions, use Extra Packages for Enterprise Linux (EPEL). For information see link:https://fedoraproject.org/wiki/EPEL[EPEL]. The name of package is `java-latest-openjdk`.

The following sections provide instructions for installing Java on RHEL.

include::modules/development/proc_installing-jvm-on-rhel-using-yum.adoc[leveloffset=+1]

include::modules/development/proc_installing-jvm-on-rhel-using-an-archive.adoc[leveloffset=+1]

include::modules/development/proc_installing-the-jdk-on-rhel-8-using-yum.adoc[leveloffset=+1]

include::modules/development/proc_installing-the-jdk-on-rhel-8-using-an-archive.adoc[leveloffset=+1]

include::modules/development/proc_installing-multiple-major-versions-of-openjdk.adoc[leveloffset=+1]

include::modules/development/proc_installing-multiple-major-versions-of-openjdk-on-rhel-using-an-archive-portable-bundle.adoc[leveloffset=+1]

include::modules/development/proc_installing-multiple-minor-versions-of-openjdk-on-rhel.adoc[leveloffset=+1]

include::modules/development/proc_installing-multiple-minor-versions-of-openjdk-on-rhel-using-an-archive-portable-bundle.adoc[leveloffset=+1]

// [id='related-information-{context}']
// == Related information
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-installing-java-on-rhel-8}
