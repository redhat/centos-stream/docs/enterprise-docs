:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly_creating-an-xfs-file-system: {context}

ifdef::context[:parent-context-of-assembly_creating-an-xfs-file-system: {context}]

////
Base the file name and the ID on the assembly title. For example:
* file name: assembly-my-user-story.adoc
* ID: [id="assembly-my-user-story_{context}"]
* Title: = My user story

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken. Include {context} in the ID so the assembly can be reused.
////

ifndef::context[]
[id="assembly_creating-an-xfs-file-system"]
endif::[]
ifdef::context[]
[id="assembly_creating-an-xfs-file-system_{context}"]
endif::[]
= Creating an XFS file system
:context: creating-an-xfs-file-system

ifdef::internal[]

[cols="1,4"]
|===

| Included in |
* getting-started-with-xfs

| Converted from |
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/ch-xfs#xfscreating

|===

endif::[]

[role="_abstract"]
As a system administrator, you can create an XFS file system on a block device to enable it to store files and directories.


include::modules/filesystems-and-storage/proc_creating-an-xfs-file-system-with-mkfs-xfs.adoc[leveloffset=+1]

// [id='additional-resources-{context}']
// == Additional resources

// * `mkfs.xfs(8)` man page on your system

ifdef::parent-context-of-assembly_creating-an-xfs-file-system[:context: {parent-context-of-assembly_creating-an-xfs-file-system}]
ifndef::parent-context-of-assembly_creating-an-xfs-file-system[:!context:]
