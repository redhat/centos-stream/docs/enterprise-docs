:_newdoc-version: 2.15.0
:_template-generated: 2023-10-25

ifdef::context[:parent-context-of-optimizing-vdo-performance: {context}]

:_mod-docs-content-type: ASSEMBLY

:experimental:

ifndef::context[]
[id="optimizing-vdo-performance"]
endif::[]
ifdef::context[]
[id="optimizing-vdo-performance_{context}"]
endif::[]
= Optimizing LVM-VDO performance

:context: optimizing-vdo-performance

The VDO kernel driver speeds up tasks by using multiple threads. Instead of one thread doing everything for an I/O request, it splits the work into smaller parts assigned to different threads. These threads talk to each other as they handle the request. This way, one thread can handle shared data without constant locking and unlocking.

When one thread finishes a task, VDO already has another task ready for it. This keeps the threads busy and reduces the time spent switching tasks. VDO also uses separate threads for slower tasks, such as adding I/O operations to the queue or handling messages to the deduplication index.

include::modules/performance/ref_vdo-thread-types.adoc[leveloffset=+1]

include::assembly_identifying-performance-bottlenecks.adoc[leveloffset=+1]

include::assembly_redistributing-vdo-threads.adoc[leveloffset=+1]

include::modules/performance/proc_increasing-block-cache-size-to-enhance-performance.adoc[leveloffset=+1]

include::modules/performance/proc_speeding-up-discard-operations.adoc[leveloffset=+1]

include::modules/performance/proc_optimizing-cpu-frequency-scaling-for-vdo-performance.adoc[leveloffset=+1]


ifdef::parent-context-of-optimizing-vdo-performance[:context: {parent-context-of-optimizing-vdo-performance}]
ifndef::parent-context-of-optimizing-vdo-performance[:!context:]