:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-changing-file-permissions: {context}]

[id="changing-file-permissions_{context}"]
= Changing file permissions

:context: changing-file-permissions

[role="system:abstract"]
You can use the `chmod` utility to change permissions for a file or directory. You can change file permissions using symbolic values or octal values.

include::modules/core-services/proc_changing-file-permissions-using-symbolic-values.adoc[leveloffset=+1]

include::modules/core-services/proc_changing-file-permissions-using-octal-values.adoc[leveloffset=+1]


ifdef::parent-context-of-changing-file-permissions[:context: {parent-context-of-changing-file-permissions}]
ifndef::parent-context-of-changing-file-permissions[:!context:]
