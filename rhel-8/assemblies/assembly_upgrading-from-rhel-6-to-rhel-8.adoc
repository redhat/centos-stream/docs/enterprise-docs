:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-upgrading-from-rhel-6-to-rhel-8: {context}]

// Base the file name and the ID on the assembly title. For example:
// * file name: my-assembly-a.adoc
// * ID: [id="my-assembly-a"]
// * Title: = My assembly A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules-{context}"].
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

// The following include statements pull in the module files that comprise the assembly. Include any combination of concept, procedure, or reference modules required to cover the user story. You can also include other assemblies.

include::modules/upgrades-and-differences/proc_planning-an-upgrade.adoc[]

include::modules/upgrades-and-differences/proc_preparing-a-rhel-6-system-for-an-upgrade-to-rhel-7.adoc[]

include::modules/upgrades-and-differences/proc_upgrading-from-rhel-6-10-to-rhel-7-9.adoc[]

include::modules/upgrades-and-differences/proc_preparing-the-rhel-7-system-for-an-upgrade-to-rhel-8.adoc[]

include::modules/upgrades-and-differences/proc_upgrading-from-rhel-7-9-to-rhel-8.adoc[]

include::modules/upgrades-and-differences/proc_performing-post-upgrade-tasks.adoc[]

include::modules/upgrades-and-differences/proc_troubleshooting-rhel-6-to-rhel-8.adoc[]


[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/articles/4263361[Supported in-place upgrade paths for Red Hat Enterprise Linux]
* link:https://access.redhat.com/support/policy/updates/errata/[Red Hat Enterprise Linux Life Cycle]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-upgrading-from-rhel-6-to-rhel-8[:context: {parent-context-of-upgrading-from-rhel-6-to-rhel-8}]
ifndef::parent-context-of-upgrading-from-rhel-6-to-rhel-8[:!context:]

