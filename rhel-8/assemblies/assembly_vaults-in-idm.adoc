:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-vaults-in-idm: {context}]
[id="vaults-in-idm_{context}"]
= Vaults in IdM
:context: vaults-in-idm

[role="_abstract"]
This chapter describes vaults in {IPA} (IdM). It introduces the following topics:

* xref:vaults-and-their-benefits_{context}[The concept of the vault].
* xref:vault-owners-members-and-administrators_{context}[The different roles associated with a vault].
* xref:standard-symmetric-and-asymmetric-vaults_{context}[The different types of vaults available in IdM based on the level of security and access control].
* xref:user-service-and-shared-vaults_{context}[The different types of vaults available in IdM based on ownership].
* xref:vault-containers_{context}[The concept of vault containers].
* xref:basic-idm-vault-commands_{context}[The basic commands for managing vaults in IdM].
* xref:installing-the-key-recovery-authority-component-in-idm_{context}[Installing the key recovery authority (KRA), which is a prerequisite for using vaults in IdM].


include::modules/identity-management/con_vaults-and-their-benefits.adoc[leveloffset=+1]

include::modules/identity-management/con_vault-owners-members-and-administrators.adoc[leveloffset=+1]

include::modules/identity-management/con_standard-symmetric-and-asymmetric-vaults.adoc[leveloffset=+1]

include::modules/identity-management/con_user-service-and-shared-vaults.adoc[leveloffset=+1]

include::modules/identity-management/con_vault-containers.adoc[leveloffset=+1]

include::modules/identity-management/ref_basic-idm-vault-commands.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-the-key-recovery-authority-component-in-idm.adoc[leveloffset=+1]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-vaults-in-idm[:context: {parent-context-of-vaults-in-idm}]
ifndef::parent-context-of-vaults-in-idm[:!context:]
