:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// master.adoc

// This assembly can be included from other assemblies using the following
// include statement:
// include::assemblies/assembly_setting-persistent-tuning-parameters.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-setting-persistent-tuning-parameters: {context}

[id='setting-persistent-tuning-parameters_{context}']
= Setting persistent kernel tuning parameters
:experimental:
:context: setting-persistent-tuning-parameters

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

[role="_abstract"]
When you have decided on a tuning configuration that works for your system, you can make the changes persistent across reboots.

By default, edited kernel tuning parameters only remain in effect until the system reboots or the parameters are explicitly changed. This is effective for establishing the initial tuning configuration. It also provides a safety mechanism. If the edited parameters cause the machine to behave erratically, rebooting the machine returns the parameters to the previous configuration.

include::modules/rt-kernel/proc_making-proc-sys-parameter-changes-persistent.adoc[leveloffset=+1]

// include::modules/proc_making-proc-sys-parameter-changes-persistent-using-the-etc-rc-d-rc-local-file.adoc[leveloffset=+1]

// [id='related-information-{context}']
// == Related information
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-setting-persistent-tuning-parameters}
