:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-assembly_working-with-systemd-unit-files: {context}]


ifndef::context[]
[id="assembly_working-with-systemd-unit-files"]
endif::[]
ifdef::context[]
[id="assembly_working-with-systemd-unit-files_{context}"]
endif::[]
= Working with systemd unit files

:context: assembly_working-with-systemd-unit-files

[role="_abstract"]
The `systemd` unit files represent your system resources. As a system administrator, you can perform the following advanced tasks:

* Create custom unit files
* Modify existing unit files
* Work with instantiated units

include::modules/core-services/con_introduction-to-unit-files.adoc[leveloffset=+1]

include::modules/core-services/ref_unit-files-locations.adoc[leveloffset=+1]

include::modules/core-services/con_unit-file-structure.adoc[leveloffset=+1]

include::modules/core-services/ref_important-unit-section-options.adoc[leveloffset=+1]

include::modules/core-services/ref_important-service-section-options.adoc[leveloffset=+1]

include::modules/core-services/ref_important-install-section-options.adoc[leveloffset=+1]

include::modules/core-services/proc_creating-custom-unit-files.adoc[leveloffset=+1]

include::modules/core-services/proc_creating-a-custom-unit-file-by-using-the-second-instance-of-the-sshd-service.adoc[leveloffset=+1]

//include::modules/core-services/con_converting-sysv-init-scripts-to-unit-files.adoc[leveloffset=+1]

include::modules/core-services/con_finding-the-systemd-service-description.adoc[leveloffset=+1]

include::modules/core-services/ref_finding-the-systemd-service-dependencies.adoc[leveloffset=+1]

include::modules/core-services/con_finding-default-targets-of-the-service.adoc[leveloffset=+1]

include::modules/core-services/con_finding-files-used-by-the-service.adoc[leveloffset=+1]

include::modules/core-services/proc_modifying-existing-unit-files.adoc[leveloffset=+1]

include::modules/core-services/proc_extending-the-default-unit-configuration.adoc[leveloffset=+1]

include::modules/core-services/proc_overriding-the-default-unit-configuration.adoc[leveloffset=+1]

include::modules/core-services/proc_changing-the-timeout-limit.adoc[leveloffset=+1]

include::modules/core-services/proc_monitoring-overriden-units.adoc[leveloffset=+1]

include::modules/core-services/con_working-with-instantiated-units.adoc[leveloffset=+1]

include::modules/core-services/ref_important-unit-specifiers.adoc[leveloffset=+1]


[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/solutions/1257953[How to set limits for services in RHEL and systemd] (Red Hat Knowledgebase)
* link:https://access.redhat.com/solutions/3120581[How to write a service unit file which enforces that particular services have to be started] (Red Hat Knowledgebase)
* link:https://access.redhat.com/solutions/3116611[How to decide what dependencies a systemd service unit definition should have] (Red Hat Knowledgebase)

ifdef::parent-context-of-assembly_working-with-systemd-unit-files[:context: {parent-context-of-assembly_working-with-systemd-unit-files}]
ifndef::parent-context-of-assembly_working-with-systemd-unit-files[:!context:]
