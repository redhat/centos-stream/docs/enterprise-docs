:_mod-docs-content-type: ASSEMBLY
:parent-context-of-ansible-server: {context}

[id="installing-an-Identity-Management-server-using-an-Ansible-playbook_{context}"]
= Installing an Identity Management server using an Ansible playbook

:context: server-ansible

Learn more about how to configure a system as an IdM server by using https://www.ansible.com/[Ansible].
Configuring a system as an IdM server establishes an IdM domain and enables the system to offer IdM services to IdM clients.
You can manage the deployment by using the `ipaserver` Ansible role.

.Prerequisites
* You understand the general https://docs.ansible.com/ansible/latest/index.html[Ansible] and IdM concepts.

include::modules/identity-management/con_ansible-and-its-advantages-for-installing-IdM.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-the-ansible-freeipa-package.adoc[leveloffset=+1]

include::modules/identity-management/con_ansible-roles-location-in-the-file-system.adoc[leveloffset=+1]

include::modules/identity-management/proc_setting-the-parameters-for-a-deployment-with-an-integrated-dns-and-an-integrated-ca-as-the-root-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_setting-the-parameters-for-a-deployment-with-external-dns-and-an-integrated-ca-as-the-root-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_deploying-an-IdM-server-with-an-integrated-CA-using-an-Ansible-playbook.adoc[leveloffset=+1]

//include::modules/identity-management/proc_setting-the-parameters-for-a-deployment-with-an-external-CA.adoc[leveloffset=+2]
include::modules/identity-management/proc_setting-the-parameters-for-a-deployment-with-an-integrated-dns-and-an-external-ca-as-the-root-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_setting-the-parameters-for-a-deployment-with-external-dns-and-an-external-ca-as-the-root-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_deploying-an-IdM-server-with-externally-signed-CA-using-an-Ansible-playbook.adoc[leveloffset=+1]


include::modules/identity-management/proc_uninstalling-an-idm-server-using-an-ansible-playbook.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-an-ansible-playbook-to-uninstall-an-idm-server-even-if-this-leads-to-a-disconnected-topology.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/planning_identity_management/index#planning-the-replica-topology_planning-identity-management[Planning the replica topology]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/planning_identity_management/index#assembly_backing-up-and-restoring-idm-servers-using-ansible-playbooks_planning-identity-management[Backing up and restoring IdM servers using Ansible playbooks]
* link:https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#inventory-basics-formats-hosts-and-groups[Inventory basics: formats, hosts, and groups]


:context: {parent-context-of-ansible-server}
