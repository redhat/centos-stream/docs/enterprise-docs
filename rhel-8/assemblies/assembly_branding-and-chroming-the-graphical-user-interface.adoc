:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-branding-and-chroming-the-graphical-user-interface: {context}]

// Base the file name and the ID on the assembly title. For example:
// * file name: my-assembly-a.adoc
// * ID: [id="my-assembly-a"]
// * Title: = My assembly A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules-{context}"].
[id="branding-and-chroming-the-graphical-user-interface_{context}"]
= Branding and chroming the graphical user interface
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: branding-and-chroming-the-graphical-user-interface
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

The customization of Anaconda user interface may include the customization of graphical elements and the customization of product name.

This section provides information about how to customize the graphical elements and the product name.

.Prerequisites

. You have downloaded and extracted the ISO image.
. You have created your own branding material.

For information about downloading and extracting boot images, see xref:extracting-red-hat-enterprise-linux-boot-images_working-with-iso-images[Extracting Red Hat Enterprise Linux boot images]

The user interface customization involves the following high-level tasks:

. Complete the prerequisites.
. Create custom branding material (if you plan to customize the graphical elements)
. Customize the graphical elements (if you plan to customize it)
. Customize the product name (if you plan to customize it)
. Create a product.img file
. Create a custom Boot image

NOTE: To create the custom branding material, first refer to the default graphical element files type and dimensions. You can accordingly create the custom material.
Details about default graphical elements are available in the sample files that are provided in the xref:customizing-graphical-elements_branding-and-chroming-the-graphical-user-interface[Customizing graphical elements] section.

include::modules/anaconda-customization/proc_customizing-graphical-elements.adoc[leveloffset=+1]

include::modules/anaconda-customization/proc_customizing-the-product-name.adoc[leveloffset=+1]

include::modules/anaconda-customization/proc_customizing-the-default-configuration.adoc[leveloffset=+1]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-branding-and-chroming-the-graphical-user-interface[:context: {parent-context-of-branding-and-chroming-the-graphical-user-interface}]
ifndef::parent-context-of-branding-and-chroming-the-graphical-user-interface[:!context:]
