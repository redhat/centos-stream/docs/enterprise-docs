:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-configuring-ha-cluster-using-system-roles: {context}]

[id="using-the-hacluster-system-role_{context}"]
= Configuring a high-availability cluster by using the `ha_cluster` system role

:context: configuring-ha-cluster-using-system-roles

[role="_abstract"]
With the `ha_cluster` system role, you can configure and manage a high-availability cluster that uses the Pacemaker high availability cluster resource manager.


include::modules/high-availability/ref_ha-role-parameters.adoc[leveloffset=+1]

include::modules/high-availability/ref_ha-role-inventory.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-TLScert-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-no-resource-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-with-resource-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-with-constraints-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-corosync-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-with-sbd-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-quorum-device-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-http-ha-using-system-roles.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[Preparing a control node and managed nodes to use RHEL system roles]
* Documentation installed with the `rhel-system-roles` package in `/usr/share/ansible/roles/rhel-system-roles.logging/README.html`
* link:https://access.redhat.com/node/3050101[{RHELSystemRoles}s] KB article
* `ansible-playbook(1)` man page on your system

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-configuing-ha-cluster-using-system-roles[:context: {parent-context-of-using-the-logging-system-role}]
ifndef::parent-context-of-configuring-ha-cluster-using-system-roles[:!context:]
