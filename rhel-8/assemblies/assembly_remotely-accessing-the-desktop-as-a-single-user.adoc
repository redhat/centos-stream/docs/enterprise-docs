:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-remotely-accessing-the-desktop-as-a-single-user: {context}]


[id="remotely-accessing-the-desktop-as-a-single-user_{context}"]
= Remotely accessing the desktop as a single user

:context: remotely-accessing-the-desktop-as-a-single-user

You can remotely connect to the desktop on a RHEL server using graphical GNOME applications. Only a single user can connect to the desktop on the server at a given time.


include::modules/desktop/proc_enabling-desktop-sharing-on-the-server-using-gnome.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 8]
include::modules/desktop/proc_connecting-to-a-shared-desktop-using-gnome.adoc[leveloffset=+1]
endif::[]

ifeval::[{ProductNumber} == 9]
include::../../modules/desktop/proc_connecting-to-a-shared-desktop-using-connections.adoc[leveloffset=+1]
endif::[]

include::modules/desktop/proc_disabling-encryption-in-gnome-vnc.adoc[leveloffset=+1]


ifdef::parent-context-of-remotely-accessing-the-desktop-as-a-single-user[:context: {parent-context-of-remotely-accessing-the-desktop-as-a-single-user}]
ifndef::parent-context-of-remotely-accessing-the-desktop-as-a-single-user[:!context:]
