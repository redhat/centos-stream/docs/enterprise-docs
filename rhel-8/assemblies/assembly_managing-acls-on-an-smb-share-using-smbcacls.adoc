:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// assembly_configuring-file-shares-on-a-samba-server.adoc
// assembly_frequently-used-samba-command-line-utilities.adoc

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_managing-acls-on-an-smb-share-using-smbcacls.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-assembly_managing-acls-on-an-smb-share-using-smbcacls: {context}

// The file name and the ID are based on the assembly title.
// For example:
// * file name: assembly_my-assembly-a.adoc
// * ID: [id='assembly_my-assembly-a_{context}']
// * Title: = My assembly A
//
// The ID is used as an anchor for linking to the module.
// Avoid changing it after the module has been published
// to ensure existing links are not broken.
//
// In order for  the assembly to be reusable in other assemblies in a guide,
// include {context} in the ID: [id='a-collection-of-modules_{context}'].
//
// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.


[id='assembly_managing-acls-on-an-smb-share-using-smbcacls_{context}']
= Managing ACLs on an SMB share using smbcacls
:context: assembly_managing-acls-on-an-smb-share-using-smbcacls


// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

The `smbcacls` utility can list, set, and delete ACLs of files and directories stored on an SMB share. You can use `smbcacls` to manage file system ACLs:

* On a local or remote Samba server that uses advanced Windows ACLs or POSIX ACLs

* On {RHEL} to remotely manage ACLs on a share hosted on Windows

// The following include statements pull in the module files that comprise
// the assembly. Include any combination of concept, procedure, or reference
// modules required to cover the user story. You can also include other
// assemblies.

include::modules/core-services/con_access-control-entries.adoc[leveloffset=+1]

include::modules/core-services/proc_displaying-acls-using-smbcacls.adoc[leveloffset=+1]

include::modules/core-services/con_ace-mask_calculation.adoc[leveloffset=+1]

include::modules/core-services/proc_adding-updating-and-removing-an-acl-using-smbcacls.adoc[leveloffset=+1]

// [leveloffset=+1] ensures that when a module starts with a level-1 heading
// (= Heading), the heading will be interpreted as a level-2 heading
// (== Heading) in the assembly.

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-assembly_managing-acls-on-an-smb-share-using-smbcacls}
