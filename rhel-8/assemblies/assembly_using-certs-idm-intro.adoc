:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}
:parent-context-of-using-certs-in-idm-intro: {context}

[id="cert-intro_{context}"]
= Public key certificates in Identity Management

:context: cert-intro

[role="_abstract"]
X.509 public key certificates are used to authenticate users, hosts and services in {IPA} (IdM). In addition to authentication, X.509 certificates also enable digital signing and encryption to provide privacy, integrity and non-repudiation.

//{IPA} (IdM) uses X.509 public key certificates for the authentication of users, hosts and services.

A certificate contains the following information:

* The subject that the certificate authenticates.
* The issuer, that is the CA that has signed the certificate.
* The start and end date of the validity of the certificate.
* The valid uses of the certificate.
* The public key of the subject.

A message encrypted by the public key can only be decrypted by a corresponding private key. While a certificate and the public key it includes can be made publicly available, the user, host or service must keep their private key secret.


//[leveloffset=-1]
include::modules/identity-management/con_cert-auth-idm.adoc[leveloffset=+1]

include::modules/identity-management/con_compare-certs-kerberos.adoc[leveloffset=+1]

include::modules/identity-management/con_using-certs-to-authenticate-users.adoc[leveloffset=+1]


:context: {parent-context}
