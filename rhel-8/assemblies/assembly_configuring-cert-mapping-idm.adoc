:_mod-docs-content-type: ASSEMBLY
:superparent-context: {context}

[id="conf-certmap-idm_{context}"]

= Configuring certificate mapping rules in Identity Management

:context: conf-certmap-idm

[role="_abstract"]
Certificate mapping rules are a convenient way of allowing users to authenticate using certificates in scenarios when the {IPA} (IdM) administrator does not have access to certain users' certificates. This is typically because the certificates have been issued by an external certificate authority.


include::assembly_certmap-intro.adoc[leveloffset=+1]

include::modules/identity-management/con_create-idm-certmapdata.adoc[leveloffset=+1]

include::modules/identity-management/proc_obtain-idm-certmapdata.adoc[leveloffset=+1]

include::assembly_conf-certmap-for-users-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/con_idm-ad-certmapdata.adoc[leveloffset=+1]

include::assembly_conf-certmap-for-ad-certs.adoc[leveloffset=+1]

include::assembly_conf-certmap-for-ad-map.adoc[leveloffset=+1]

include::assembly_conf-certmap-ad-no-cert-no-map.adoc[leveloffset=+1]

include::modules/identity-management/con_add-certmaprule-all-options.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* `sss-certmap(5)` man page on your system


:context: {superparent-context}
