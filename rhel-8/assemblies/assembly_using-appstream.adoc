:_mod-docs-content-type: ASSEMBLY
:parent-context-of-using-appstream: {context}
[id="using-appstream_{context}"]
= Using AppStream

:context: using-appstream
// ustory: as a user or admin, I need to understand what is appstream and how it fits into the world of rpm/yum as I know it

In the following sections, learn the concepts related to the AppStream repository in {RHEL}{nbsp}8:

* xref:distribution-of-content-in-rhel8_{context}[Distribution of content in RHEL 8].
* xref:application-streams_{context}[Application Streams].
* xref:packaging-methods-in-rhel-8_{context}[Packaging methods in RHEL 8].
* xref:package-management-using-yum-in-rhel-8_{context}[Package management using YUM in RHEL 8].


include::modules/appstream/con_distribution-of-content-in-rhel8.adoc[leveloffset=+1]

include::modules/appstream/con_application-streams.adoc[leveloffset=+1]

include::modules/appstream/con_packaging-methods-in-rhel-8.adoc[leveloffset=+1]

include::modules/appstream/con_package-management-using-yum-in-rhel-8.adoc[leveloffset=+1]

:context: {parent-context-of-using-appstream}
