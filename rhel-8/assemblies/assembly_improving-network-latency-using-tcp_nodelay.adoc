ifdef::context[:parent-context-of-assembly_improving-network-latency-using-tcp_nodelay: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_improving-network-latency-using-tcp_nodelay"]
endif::[]
ifdef::context[]
[id="assembly_improving-network-latency-using-tcp_nodelay_{context}"]
endif::[]
= Improving network latency using TCP_NODELAY

:context: assembly_improving-network-latency-using-tcp_nodelay

[role="_abstract"]
By default, `TCP` uses Nagle’s algorithm to collect small outgoing packets to send all at once. This can cause higher rates of latency.

.Prerequisites

* You have administrator privileges.

include::modules/rt-kernel/con_the-effects-of-using-tcp_nodelay.adoc[leveloffset=+1]

include::modules/rt-kernel/proc_enabling-tcp_nodelay.adoc[leveloffset=+1]

include::modules/rt-kernel/proc_enabling-tcp_cork.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* `tcp(7)`, `setsockopt(3p)`, and `setsockopt(2)` man pages on your system

ifdef::parent-context-of-assembly_improving-network-latency-using-tcp_nodelay[:context: {parent-context-of-assembly_improving-network-latency-using-tcp_nodelay}]
ifndef::parent-context-of-assembly_improving-network-latency-using-tcp_nodelay[:!context:]
