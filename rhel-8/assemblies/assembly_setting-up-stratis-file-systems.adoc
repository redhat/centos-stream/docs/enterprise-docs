ifdef::context[:parent-context-of-setting-up-stratis-file-systems: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="setting-up-stratis-file-systems"]
endif::[]
ifdef::context[]
[id="setting-up-stratis-file-systems_{context}"]
endif::[]
= Setting up Stratis file systems

:context: setting-up-stratis-file-systems

Stratis runs as a service to manage pools of physical storage devices, simplifying local storage management with ease of use while helping you set up and manage complex storage configurations.

ifeval::[{ProductNumber}==8]
:TechPreviewName: Stratis
include::common-content/snip_techpreview.adoc[]
:TechPreviewName!: Stratis
endif::[]

include::modules/filesystems-and-storage/con_the-purpose-and-features-of-stratis.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_components-of-a-stratis-volume.adoc[leveloffset=+1]

// TODO: Ask Content Strategy whether a section like this is appropriate
// include::modules/filesystems-and-storage/con_differences-between-stratis-and-btrfs-or-zfs.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_block-devices-usable-with-stratis.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_installing-stratis.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_create-unencrypted-stratis-pool.adoc[leveloffset=+1]

include::modules/cockpit/proc_creating-an-unencrypted-stratis-pool-using-the-web-console.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_create-encrypted-stratis-pool.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 9]
include::modules/filesystems-and-storage/proc_creating-an-encrypted-stratis-pool-using-the-storage-rhel-system-role.adoc[leveloffset=+1]
endif::[]

include::modules/cockpit/proc_creating-an-encrypted-stratis-pool-using-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_renaming-a-stratis-pool-using-the-web-console.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_setting-overprovisioning-mode-in-stratis-fs.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_bind-stratis-pool-nbde.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_bind-stratis-pool-tpm.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_unlock-encrypted-stratis-pool-keyring.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_unbind-stratis-pool-from-supplementary-encryption.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_starting-and-stopping-stratis-pool.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_creating-a-stratis-file-system.adoc[leveloffset=+1]

include::modules/cockpit/proc_creating-a-file-system-on-a-stratis-pool-using-the-web-console.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_mounting-a-stratis-file-system.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_setting-up-non-root-stratis-fs-fstab-systemd.adoc[leveloffset=+1]


ifdef::parent-context-of-setting-up-stratis-file-systems[:context: {parent-context-of-setting-up-stratis-file-systems}]
ifndef::parent-context-of-setting-up-stratis-file-systems[:!context:]
