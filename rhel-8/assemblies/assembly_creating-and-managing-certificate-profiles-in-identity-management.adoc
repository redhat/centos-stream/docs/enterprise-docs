:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Retains the context of the parent assembly if this assembly is nested within another assembly.
// For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
// See also the complementary step on the last line of this file.
ifdef::context[:parent-context-of-creating-and-managing-certificate-profiles-in-identity-management: {context}]

// Base the file name and the ID on the assembly title. For example:
// * file name: my-assembly-a.adoc
// * ID: [id="my-assembly-a"]
// * Title: = My assembly A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules-{context}"].
[id="creating-and-managing-certificate-profiles-in-identity-management_{context}"]
= Creating and managing certificate profiles in Identity Management
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: creating-and-managing-certificate-profiles-in-identity-management
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

[role="_abstract"]
Certificate profiles are used by the Certificate Authority (CA) when signing certificates to determine if a certificate signing request (CSR) is acceptable, and if so what features and extensions are present on the certificate. A certificate profile is associated with issuing a particular type of certificate. By combining certificate profiles and CA access control lists (ACLs), you can define and control access to custom certificate profiles.

In describing how to create certificate profiles, the procedures use S/MIME certificates as an example. Some email programs support digitally signed and encrypted email using the Secure Multipurpose Internet Mail Extension (S/MIME) protocol. Using S/MIME to sign or encrypt email messages requires the sender of the message to have an S/MIME certificate.

* xref:what-is-a-certificate-profile_creating-and-managing-certificate-profiles-in-identity-management[What is a certificate profile]
* xref:creating-a-certificate-profile_creating-and-managing-certificate-profiles-in-identity-management[Creating a certificate profile]
* xref:what-is-a-ca-access-control-list_creating-and-managing-certificate-profiles-in-identity-management[What is a CA access control list]
* xref:defining-a-ca-acl-to-control-access-to-certificate-profiles_creating-and-managing-certificate-profiles-in-identity-management[Defining a CA ACL to control access to certificate profiles]
* xref:using-certificate-profiles-and-ca-acls-to-issue-certificates_creating-and-managing-certificate-profiles-in-identity-management[Using certificate profiles and CA ACLs to issue certificates]
* xref:modifying-a-certificate-profile_creating-and-managing-certificate-profiles-in-identity-management[Modifying a certificate profile]
* xref:certificate-profile-configuration-parameters_creating-and-managing-certificate-profiles-in-identity-management[Certificate profile configuration parameters]

include::modules/identity-management/con_what-is-a-certificate-profile.adoc[leveloffset=+1]

include::modules/identity-management/proc_creating-a-certificate-profile.adoc[leveloffset=+1]

include::modules/identity-management/con_what-is-a-ca-access-control-list.adoc[leveloffset=+1]

include::modules/identity-management/proc_defining-a-ca-acl-to-control-access-to-certificate-profiles.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-certificate-profiles-and-ca-acls-to-issue-certificates.adoc[leveloffset=+1]

include::modules/identity-management/proc_modifying-a-certificate-profile.adoc[leveloffset=+1]

include::modules/identity-management/ref_certificate-profile-configuration-parameters.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-creating-and-managing-certificate-profiles-in-identity-management[:context: {parent-context-of-creating-and-managing-certificate-profiles-in-identity-management}]
ifndef::parent-context-of-creating-and-managing-certificate-profiles-in-identity-management[:!context:]
