////
Retains the context of the parent assembly if this assembly is nested within another assembly.
For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
See also the complementary step on the last line of this file.
////

ifdef::context[:parent-context-of-assembly_signing-container-images: {context}]


:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_signing-container-images"]
endif::[]
ifdef::context[]
[id="assembly_signing-container-images_{context}"]
endif::[]
= Signing container images


:context: assembly_signing-container-images


You can use a GNU Privacy Guard (GPG) signature or a sigstore signature to sign your container image. Both signing techniques are generally compatible with any OCI compliant container registries. You can use Podman to sign the image before pushing it into a remote registry and configure consumers so that any unsigned image is rejected. Signing container images helps to prevent supply chain attacks.

Signing using GPG keys requires deploying a separate lookaside server to distribute signatures.
The lookaside server can be any HTTP server. 
Starting with Podman version 4.2, you can use the sigstore format of container signatures. Compared to the GPG keys, the separate lookaside server is not required because the sigstore signatures are stored in the container registry. 


include::modules/containers/proc_signing-container-images-with-gpg-signatures.adoc[leveloffset=+1]

include::modules/containers/proc_verifying-gpg-image-signatures.adoc[leveloffset=+1]

include::modules/containers/proc_signing-container-images-with-sigstore-signatures-using-a-private-key.adoc[leveloffset=+1]

include::modules/containers/proc_verifying-sigstore-image-signatures-using-a-public-key.adoc[leveloffset=+1]

include::modules/containers/proc_signing-container-images-with-sigstore-signatures-using-fulcio-and-rekor.adoc[leveloffset=+1]

include::modules/containers/proc_verifying-container-images-with-sigstore-signatures-using-fulcio-and-rekor.adoc[leveloffset=+1]

include::modules/containers/proc_signing-container-images-with-sigstore-signatures-with-a-private-key-and-rekor.adoc[leveloffset=+1]


////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_signing-container-images[:context: {parent-context-of-assembly_signing-container-images}]
ifndef::parent-context-of-assembly_signing-container-images[:!context:]

