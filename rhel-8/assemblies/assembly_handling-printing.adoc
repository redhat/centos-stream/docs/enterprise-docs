:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_handling-printing.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
ifdef::context[:parent-context-of-handling-printing: {context}]

[id="handling-printing_{context}"]
= Handling printing

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: handling-printing

[role="_abstract"]
In GNOME, you can set up printing using the [application]*Settings* application.

include::modules/desktop/proc_starting-gcc-for-printing.adoc[leveloffset=+1]

include::modules/desktop/proc_adding-a-printer-gcc.adoc[leveloffset=+1]

include::modules/desktop/proc_configuring-printer-gcc.adoc[leveloffset=+1]

include::modules/desktop/proc_printing-a-test-page-new.adoc[leveloffset=+1]

include::modules/desktop/proc_setting-print-options-gcc-desktop.adoc[leveloffset=+1]

ifdef::parent-context-of-handling-printing[:context: {parent-context-of-handling-printing}]
ifndef::parent-context-of-handling-printing[:!context:]
