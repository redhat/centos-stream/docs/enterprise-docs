:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-configuring-an-operating-system-to-optimize-cpu-utilization: {context}]

ifndef::context[]
[id="configuring-an-operating-system-to-optimize-cpu-utilization"]
endif::[]
ifdef::context[]
[id="configuring-an-operating-system-to-optimize-cpu-utilization_{context}"]
endif::[]
= Configuring an operating system to optimize CPU utilization

:context: configuring-an-operating-system-to-optimize-cpu-utilization


[role="_abstract"]
You can configure the operating system to optimize CPU utilization across their workloads.


// include::

include::modules/performance/con_tools-for-monitoring-and-diagnosing-processor-issues.adoc[leveloffset=+1]

include::modules/performance/con_types-of-system-topology.adoc[leveloffset=+1]

include::modules/performance/proc_displaying-system-topologies.adoc[leveloffset=+2]

include::modules/performance/proc_configuring-kernel-tick-time.adoc[leveloffset=+1]

include::modules/performance/con_overview-of-an-interrupt-request.adoc[leveloffset=+1]

include::modules/performance/proc_balancing-interrupts-manually.adoc[leveloffset=+2]

include::modules/performance/proc_setting-the-smp_affinity-mask.adoc[leveloffset=+2]

ifdef::parent-context-of-configuring-an-operating-system-to-optimize-cpu-utilization[:context: {parent-context-of-configuring-an-operating-system-to-optimize-cpu-utilization}]
ifndef::parent-context-of-configuring-an-operating-system-to-optimize-cpu-utilization[:!context:]
