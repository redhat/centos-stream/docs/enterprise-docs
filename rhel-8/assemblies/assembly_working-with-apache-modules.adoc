:_mod-docs-content-type: ASSEMBLY

:parent-context-of-assembly_working-with-apache-modules: {context}

[id="setting-apache-http-server_{context}"]
= Working with Apache modules
:context: working-with-apache-modules

:working-with-apache-modules:

[role="_abstract"]
The [service]`httpd` service is a modular application, and you can extend it with a number of _Dynamic Shared Objects_ (*DSO*pass:attributes[{blank}]s). _Dynamic Shared Objects_ are modules that you can dynamically load or unload at runtime as necessary. You can find these modules in the [filename]`/usr/lib64/httpd/modules/` directory.

include::modules/core-services/proc_loading-a-dso-module.adoc[leveloffset=+1]

include::modules/core-services/proc_compiling-a-custom-apache-module.adoc[leveloffset=+1]


:!working-with-apache-modules:

:context: {parent-context-of-assembly_working-with-apache-modules}
