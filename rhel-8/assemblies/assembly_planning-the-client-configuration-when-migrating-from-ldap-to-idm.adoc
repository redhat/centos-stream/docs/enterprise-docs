ifdef::context[:parent-context-of-planning-the-client-configuration-when-migrating-from-ldap-to-idm: {context}]
[id="planning-the-client-configuration-when-migrating-from-ldap-to-idm_{context}"]
= Planning the client configuration when migrating from LDAP to IdM

:context: planning-the-client-configuration-when-migrating-from-ldap-to-idm

{IPA} (IdM) can support a number of different client configurations, with varying degrees of functionality, flexibility, and security. Decide which configuration is best for each individual client based on its operating system and your IT maintenance priorities. Consider also the client’s functional area: a development machine typically requires a different configuration than production servers or user laptops do.

[IMPORTANT]
====

Most environments have a mixture of different ways in which clients connect to the IdM domain. Administrators must decide which scenario is best for each individual client.

====

include::modules/identity-management/con_initial-pre-migration-client-configuration.adoc[leveloffset=+1]

include::modules/identity-management/con_recommended-configuration-for-rhel-clients.adoc[leveloffset=+1]

include::modules/identity-management/con_alternative-supported-configuration.adoc[leveloffset=+1]




ifdef::parent-context-of-planning-the-client-configuration-when-migrating-from-ldap-to-idm[:context: {parent-context-of-planning-the-client-configuration-when-migrating-from-ldap-to-idm}]
ifndef::parent-context-of-planning-the-client-configuration-when-migrating-from-ldap-to-idm[:!context:]
