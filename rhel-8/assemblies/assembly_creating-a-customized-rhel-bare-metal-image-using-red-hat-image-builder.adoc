ifdef::context[:parent-context-of-assembly_creating-a-customized-rhel-bare-metal-image-using-red-hat-image-builder: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_creating-a-customized-rhel-bare-metal-image-using-red-hat-image-builder"]
endif::[]
ifdef::context[]
[id="assembly_creating-a-customized-rhel-bare-metal-image-using-red-hat-image-builder_{context}"]
endif::[]
= Creating a customized RHEL bare metal image by using Insights image builder

:context: assembly_creating-a-customized-rhel-bare-metal-image-using-red-hat-image-builder

[role="_abstract"]
You can create customized RHEL ISO system images by using the Insights image builder. You can then download these images and install them on a bare metal system according to your requirements.

include::modules/image-builder-cloud/proc_creating-a-customized-rhel-iso-system-image-using-image-builder.adoc[leveloffset=+1]

include::modules/image-builder-cloud/proc_installing-the-customized-rhel-iso-system-image-to-a-bare-metal-system.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_creating-a-customized-rhel-bare-metal-image-using-red-hat-image-builder[:context: {parent-context-of-assembly_creating-a-customized-rhel-bare-metal-image-using-red-hat-image-builder}]
ifndef::parent-context-of-assembly_creating-a-customized-rhel-bare-metal-image-using-red-hat-image-builder[:!context:]

