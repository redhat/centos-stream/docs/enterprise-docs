:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly_configuring-multisite-cluster: {context}

[id='assembly_configuring-multisite-cluster-{context}']
= Multi-site Pacemaker clusters
:context: configuring-multisite-cluster

[role="_abstract"]
When a cluster spans more than one site, issues with network connectivity between the sites can lead to split-brain situations. When connectivity drops, there is no way for a node on one site to determine whether a node on another site has failed or is still functioning with a failed site interlink. In addition, it can be problematic to provide high availability services across two sites which are too far apart to keep synchronous. To address these issues, Pacemaker provides full support for the ability to configure high availability clusters that span multiple sites through the use of a Booth cluster ticket manager.

include::modules/high-availability/con_booth-cluster-ticket-manager.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-multisite-with-booth.adoc[leveloffset=+1]

:context: {parent-context-of-assembly_configuring-multisite-cluster}


