ifdef::context[:parent-context-of-configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role"]
endif::[]
ifdef::context[]
[id="configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role_{context}"]
endif::[]
= Configuring a high-availability cluster by using RHEL system roles

:context: configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role

With the `ha_cluster` system role, you can configure and manage a high-availability cluster that uses the Pacemaker high availability cluster resource manager.


include::modules/high-availability/ref_ha-role-parameters.adoc[leveloffset=+1]

include::modules/high-availability/ref_ha-role-inventory.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-TLScert-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-no-resource-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-with-resource-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-with-resource-defaults-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-with-stonith-levels-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-with-constraints-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-corosync-hacluster-using-system-roles.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 9]

include::modules/high-availability/proc_configuring-hacluster-with-ACLs-using-system-roles.adoc[leveloffset=+1]

endif::[]

ifeval::[{ProductNumber} == 8]

include::modules/high-availability/proc_configuring-with-sbd-hacluster-using-system-roles.adoc[leveloffset=+1]

endif::[]


ifeval::[{ProductNumber} == 9]

include::modules/high-availability/proc_configuring-sbd-with-ha_cluster_nodes_options-hacluster-using-system-roles.adoc[leveloffset=+1]

endif::[]

ifeval::[{ProductNumber} == 9]

include::modules/high-availability/proc_configuring-sbd-with-ha_cluster-hacluster-using-system-roles.adoc[leveloffset=+1]

endif::[]

ifeval::[{ProductNumber} == 9]

include::modules/high-availability/proc_configuring-placement-strategy-hacluster-using-system-roles.adoc[leveloffset=+1]

endif::[]

ifeval::[{ProductNumber} == 9]
include::modules/high-availability/proc_configuring-alerts-hacluster-using-system-roles.adoc[leveloffset=+1]

endif::[]

include::modules/high-availability/proc_configuring-quorum-device-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-with-node-attributes-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-http-ha-using-system-roles.adoc[leveloffset=+1]


ifdef::parent-context-of-configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role[:context: {parent-context-of-configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role}]
ifndef::parent-context-of-configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role[:!context:]

