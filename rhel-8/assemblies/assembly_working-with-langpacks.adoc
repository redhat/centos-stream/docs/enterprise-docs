:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-working-with-langpacks: {context}]

[id="working-with-langpacks_{context}"]
= Working with RPM weak dependency-based langpacks

:context: working-with-langpacks

This section describes multiple actions that you may want to perform when querying RPM weak dependency-based langpacks, installing or removing language support.

include::modules/core-services/proc_listing-already-installed-language-support.adoc[leveloffset=+1]

include::modules/core-services/proc_checking-the-availability-of-language-support.adoc[leveloffset=+1]

include::modules/core-services/proc_listing-packages-installed-for-a-language.adoc[leveloffset=+1]

include::modules/core-services/proc_installing-language-support.adoc[leveloffset=+1]

include::modules/core-services/proc_removing-language-support.adoc[leveloffset=+1]



ifdef::parent-context-of-working-with-langpacks[:context: {parent-context-of-working-with-langpacks}]
ifndef::parent-context-of-working-with-langpacks[:!context:]
