ifdef::context[:parent-context-of-adding-hbac-service-groups: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="adding-hbac-service-groups"]
endif::[]
ifdef::context[]
[id="adding-hbac-service-groups_{context}"]
endif::[]
= Adding HBAC service groups

:context: adding-hbac-service-groups

[role="_abstract"]
HBAC service groups can simplify HBAC rules management. For example, instead of adding individual services to an HBAC rule, you can add a whole service group.

include::modules/identity-management/proc_adding-hbac-service-groups-in-the-webui.adoc[leveloffset=+1]

include::modules/identity-management/proc_adding-hbac-service-groups-in-the-idm-cli.adoc[leveloffset=+1]


ifdef::parent-context-of-adding-hbac-service-groups[:context: {parent-context-of-adding-hbac-service-groups}]
ifndef::parent-context-of-adding-hbac-service-groups[:!context:]

