:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-using-the-php-scripting-language-with-a-web-server: {context}]

[id="using-the-php-scripting-language-with-a-web-server_{context}"]
= Using the PHP scripting language with a web server

:context: using-the-php-scripting-language-with-a-web-server

include::modules/core-services/proc_using-php-with-the-apache-http-server.adoc[leveloffset=+1]

include::modules/core-services/proc_using-php-with-the-nginx-web-server.adoc[leveloffset=+1]

ifdef::parent-context-of-using-the-php-scripting-language-with-a-web-server[:context: {parent-context-of-using-the-php-scripting-language-with-a-web-server}]
ifndef::parent-context-of-using-the-php-scripting-language-with-a-web-server[:!context:]
