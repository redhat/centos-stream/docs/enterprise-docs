ifdef::context[:parent-context-of-disk-partitions: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="disk-partitions"]
endif::[]
ifdef::context[]
[id="disk-partitions_{context}"]
endif::[]
= Disk partitions

:context: disk-partitions

To divide a disk into one or more logical areas, use the disk partitioning utility. It enables separate management of each partition.

include::modules/filesystems-and-storage/con_overview-of-partitions.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/ref_comparison-of-partition-table-types.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_mbr-disk-partitions.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_extended-mbr-partitions.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/ref_mbr-partition-types.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_guid-partition-table.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_partition-types.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_partition-naming-scheme.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_mount-points-and-disk-partitions.adoc[leveloffset=+1]

ifdef::parent-context-of-disk-partitions[:context: {parent-context-of-disk-partitions}]
ifndef::parent-context-of-disk-partitions[:!context:]
