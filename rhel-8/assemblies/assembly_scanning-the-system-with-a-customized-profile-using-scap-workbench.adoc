:_mod-docs-content-type: ASSEMBLY
:parent-context-of-scanning-the-system-with-a-customized-profile-using-scap-workbench: {context}

[id='scanning-the-system-with-a-customized-profile-using-scap-workbench_{context}']
= Scanning the system with a customized profile using SCAP Workbench

:context: scanning-the-system-with-a-customized-profile-using-scap-workbench

ifdef::internal[]
[cols="1,4"]
|===
| Included in |
scanning-the-system-for-configuration-compliance-and-vulnerabilities
| User story |
As an administrator I want to customize a <baseline> and make it stricter/looser, according to my organization's needs
| Jira |
https://projects.engineering.redhat.com/browse/RHELPLAN-11018
| SMEs |
jcerny@redhat.com, mmarhefk@redhat.com
|===
endif::[]

[application]`SCAP Workbench`, which is contained in the [package]`scap-workbench` package, is a graphical utility that enables users to perform configuration and vulnerability scans on a single local or a remote system, perform remediation of the system, and generate reports based on scan evaluations. Note that [application]`SCAP Workbench` has limited functionality compared with the [application]`oscap` command-line utility. [application]`SCAP Workbench` processes security content in the form of data stream files.

include::modules/security/proc_using-scap-workbench-to-scan-and-remediate-the-system.adoc[leveloffset=+1]

include::modules/security/proc_customizing-a-security-profile-with-scap-workbench.adoc[leveloffset=+1]


[id='related-information-{context}']
[role="_additional-resources"]
== Additional resources
* `scap-workbench(8)` man page on your system
* `/usr/share/doc/scap-workbench/user_manual.html` file provided by the `scap-workbench` package
* link:https://access.redhat.com/solutions/2377951[Deploy customized SCAP policies with Satellite 6.x] (Red Hat Knowledgebase)

:context: {parent-context-of-scanning-the-system-with-a-customized-profile-using-scap-workbench}
