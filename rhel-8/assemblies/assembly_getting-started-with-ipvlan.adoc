:_mod-docs-content-type: ASSEMBLY

:parent-context-of-getting-started-with-ipvlan: {context}

[id="getting-started-with-ipvlan_{context}"]
= Getting started with IPVLAN

:context: getting-started-with-ipvlan

[role="_abstract"]
IPVLAN is a driver for a virtual network device that can be used in container environment to access the host network. IPVLAN exposes a single MAC address to the external network regardless the number of IPVLAN device created inside the host network. This means that a user can have multiple IPVLAN devices in multiple containers and the corresponding switch reads a single MAC address. IPVLAN driver is useful when the local switch imposes constraints on the total number of MAC addresses that it can manage.

include::modules/networking/con_ipvlan-modes.adoc[leveloffset=+1]

include::modules/networking/con_ipvlan-and-macvlan.adoc[leveloffset=+1]

include::modules/networking/proc_creating-and-configuring-the-ipvlan-device-using-iproute2.adoc[leveloffset=+1]

:context: {parent-context-of-getting-started-with-ipvlan}
