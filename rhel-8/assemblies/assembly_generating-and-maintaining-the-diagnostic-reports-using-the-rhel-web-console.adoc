:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-generating-and-maintaining-the-diagnostic-reports-using-the-rhel-web-console: {context}]

[id="generating-and-maintaining-the-diagnostic-reports-using-the-rhel-web-console_{context}"]

= Generating and maintaining the diagnostic reports using the RHEL web console

:context: generating-and-maintaining-the-diagnostic-reports-using-the-rhel-web-console

Generate, download, and delete the diagnostic reports in the RHEL web console.

include::modules/sos-report/proc_generating-diagnostic-reports-using-the-rhel-web-console.adoc[leveloffset=+1]

include::modules/sos-report/proc_downloading-diagnostic-reports-using-the-rhel-web-console.adoc[leveloffset=+1]

include::modules/sos-report/proc_deleting-diagnostic-reports-using-the-rhel-web-console.adoc[leveloffset=+1]

ifdef::parent-context-of-generating-and-maintaining-the-diagnostic-reports-using-the-rhel-web-console[:context: {parent-context-of-generating-and-maintaining-the-diagnostic-reports-using-the-rhel-web-console}]
ifndef::parent-context-of-generating-and-maintaining-the-diagnostic-reports-using-the-rhel-web-console[:!context:]

