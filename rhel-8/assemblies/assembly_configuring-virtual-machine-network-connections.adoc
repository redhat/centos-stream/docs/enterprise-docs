:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// configuring-and.managing-virtualization (master.adoc)

ifdef::context[:parent-context-of-configuring-virtual-machine-network-connections: {context}]

[id="configuring-virtual-machine-network-connections_{context}"]
= Configuring virtual machine network connections

:context: configuring-virtual-machine-network-connections

For your virtual machines (VMs) to connect over a network to your host, to other VMs on your host, and to locations on an external network, the VM networking must be configured accordingly. To provide VM networking, the {ProductShortName} {ProductNumber} hypervisor and newly created VMs have a default network configuration, which can also be modified further. For example:

* You can enable the VMs on your host to be discovered and connected to by locations outside the host, as if the VMs were on the same network as the host.
* You can partially or completely isolate a VM from inbound network traffic to increase its security and minimize the risk of any problems with the VM impacting the host.

The following sections explain the various types of VM network configuration and provide instructions for setting up selected VM network configurations.

////
.Prerequisites

* TBD?
////


include::assembly_understanding-networking-overview.adoc[leveloffset=+1]

include::assembly_managing-virtual-machine-network-interfaces-using-the-web-console.adoc[leveloffset=+1]

include::assembly_recommended-virtual-machine-networking-configurations-using-the-command-line-interface.adoc[leveloffset=+1]

include::assembly_types-of-virtual-machine-network-connections.adoc[leveloffset=+1]

include::assembly_booting-virtual-machines-from-a-pxe-server.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 9]

include::../../assemblies/assembly_configuring-bridges-on-a-network-bond-to-connect-virtual-machines-with-the-network.adoc[leveloffset=+1]

include::../../modules/virtualization/proc_configuring-the-passt-user-space-connection.adoc[leveloffset=+1]

endif::[]

[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_networking/index[Configuring and managing networking]
* {blank}
ifeval::[{ProductNumber} == 8]
ifdef::virt-title[]
xref:managing-sr-iov-devices_managing-virtual-devices[Attach specific network interface cards as SR-IOV devices]
endif::[]
ifndef::virt-title[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/managing-virtual-devices_configuring-and-managing-virtualization#managing-sr-iov-devices_managing-virtual-devices[Attach specific network interface cards as SR-IOV devices]
endif::[]
endif::[]
ifeval::[{ProductNumber} == 9]
xref:managing-sr-iov-devices_managing-virtual-devices[Attach specific network interface cards as SR-IOV devices]
endif::[]
to increase VM performance.


ifdef::parent-context-of-configuring-virtual-machine-network-connections[:context: {parent-context-of-configuring-virtual-machine-network-connections}]
ifndef::parent-context-of-configuring-virtual-machine-network-connections[:!context:]
