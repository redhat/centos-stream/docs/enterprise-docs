ifdef::context[:parent-context-of-assembly_optimizing-libvirt-daemons: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_optimizing-libvirt-daemons"]
endif::[]
ifdef::context[]
[id="assembly_optimizing-libvirt-daemons_{context}"]
endif::[]
= Optimizing libvirt daemons


:context: assembly_optimizing-libvirt-daemons


The `libvirt` virtualization suite works as a management layer for the {ProductShortName} hypervisor, and your `libvirt` configuration significantly impacts your virtualization host. Notably, {ProductShortName} {ProductNumber} contains two different types of `libvirt` daemons, monolithic or modular, and which type of daemons you use affects how granularly you can configure individual virtualization drivers.


include::modules/virtualization/con_types-of-libvirt-daemons.adoc[leveloffset=+1]

include::modules/virtualization/proc_enabling-modular-libvirt-daemons.adoc[leveloffset=+1]


// [role="_additional-resources"]
// == Additional resources (or Next steps)
// UPSTREAM SOURCE: https://libvirt.org/daemons.html

ifdef::parent-context-of-assembly_optimizing-libvirt-daemons[:context: {parent-context-of-assembly_optimizing-libvirt-daemons}]
ifndef::parent-context-of-assembly_optimizing-libvirt-daemons[:!context:]
