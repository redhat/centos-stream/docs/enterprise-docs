:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
// assemblies/assembly_using-postgresql.adoc

ifdef::context[:parent-context-of-migrating-to-a-rhel-8-version-of-postgresql: {context}]


[id="migrating-to-a-rhel-8-version-of-postgresql_{context}"]
= Migrating to a RHEL 8 version of PostgreSQL

:context: migrating-to-a-rhel-8-version-of-postgresql

{RHEL} 7 contains *PostgreSQL 9.2* as the default version of the *PostgreSQL* server. In addition, several versions of *PostgreSQL* are provided as Software Collections for RHEL 7.

{RHEL} 8 provides *PostgreSQL 10* as the default `postgresql` stream, *PostgreSQL 9.6*, *PostgreSQL 12*, *PostgreSQL 13*, *PostgreSQL 15*, and *PostgreSQL 16*.

Users of *PostgreSQL* on {RHEL} can use two migration paths for the database files:

* xref:fast-upgrade-using-the-pg_upgrade-tool_{context}[Fast upgrade using the pg_upgrade utility]
* xref:dump-and-restore-upgrade_{context}[Dump and restore upgrade]

The fast upgrade method is quicker than the dump and restore process. However, in certain cases, the fast upgrade does not work, and you can only use the dump and restore process. Such cases include:

* Cross-architecture upgrades
* Systems using the `plpython` or `plpython2` extensions. Note that RHEL 8 AppStream repository includes only the [package]`postgresql-plpython3` package, not the [package]`postgresql-plpython2` package.
* Fast upgrade is not supported for migration from {RH} Software Collections versions of *PostgreSQL*.

As a prerequisite for migration to a later version of *PostgreSQL*, back up all your *PostgreSQL* databases.

Dumping the databases and performing backup of the SQL files is required for the dump and restore process and recommended for the fast upgrade method.

Before migrating to a later version of *PostgreSQL*, see the link:https://www.postgresql.org/docs/16/release.html[upstream compatibility notes] for the version of *PostgreSQL* to which you want to migrate, and for all skipped *PostgreSQL* versions between the one you are migrating from and the target version.


include::modules/core-services/ref_notable-differences-between-postgresql-15-and-postgresql-16.adoc[leveloffset=+1]

include::modules/core-services/ref_notable-differences-between-postgresql-13-and-postgresql-15.adoc[leveloffset=+1]

include::modules/core-services/proc_fast-upgrade-using-the-pg_upgrade-tool.adoc[leveloffset=+1]

include::modules/core-services/proc_dump-and-restore-upgrade.adoc[leveloffset=+1]


:context: {parent-context-of-migrating-to-a-rhel-8-version-of-postgresql}

ifdef::parent-context-of-migrating-to-a-rhel-8-version-of-postgresql[:context: {parent-context-of-migrating-to-a-rhel-8-version-of-postgresql}]
ifndef::parent-context-of-migrating-to-a-rhel-8-version-of-postgresql[:!context:]
