
ifdef::context[:parent-context-of-managing-systemd-units-by-using-the-systemd-rhel-system-role: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="managing-systemd-units-by-using-the-systemd-rhel-system-role"]
endif::[]
ifdef::context[]
[id="managing-systemd-units-by-using-the-systemd-rhel-system-role_{context}"]
endif::[]
= Managing `systemd` units by using RHEL system roles

:context: managing-systemd-units-by-using-the-systemd-rhel-system-role

[role="_abstract"]
By using the `systemd` RHEL system role, you can automate certain systemd-related tasks and perform them remotely. You can use the role for the following actions:

* Manage services
* Deploy units
* Deploy drop-in files


include::modules/system-roles/proc_managing-services-by-using-the-systemd-rhel-system-role.adoc[leveloffset=+1]

include::modules/system-roles/proc_deploying-systemd-drop-in-files-by-using-the-systemd-rhel-system-role.adoc[leveloffset=+1]

include::modules/system-roles/proc_deploying-systemd-units-by-using-the-systemd-rhel-system-role.adoc[leveloffset=+1]


ifdef::parent-context-of-managing-systemd-units-by-using-the-systemd-rhel-system-role[:context: {parent-context-of-managing-systemd-units-by-using-the-systemd-rhel-system-role}]
ifndef::parent-context-of-managing-systemd-units-by-using-the-systemd-rhel-system-role[:!context:]

