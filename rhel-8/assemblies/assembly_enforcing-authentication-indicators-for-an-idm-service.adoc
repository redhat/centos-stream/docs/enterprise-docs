:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-enforcing-authentication-indicators-for-an-idm-service: {context}]
[id="enforcing-authentication-indicators-for-an-idm-service_{context}"]
= Enforcing authentication indicators for an IdM service
// If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.

:context: enforcing-authentication-indicators-for-an-idm-service

[role="_abstract"]
The authentication mechanisms supported by {IPA} (IdM) vary in their authentication strength. For example, obtaining the initial Kerberos ticket-granting ticket (TGT) using a one-time password (OTP) in combination with a standard password is considered more secure than authentication using only a standard password.

By associating authentication indicators with a particular IdM service, you can, as an IdM administrator, configure the service so that only users who used those specific pre-authentication mechanisms to obtain their initial ticket-granting ticket (TGT) will be able to access the service.

In this way, you can configure different IdM services so that:

* Only users who used a stronger authentication method to obtain their initial TGT, such as a one-time password (OTP), can access services critical to security, such as a VPN.

* Users who used simpler authentication methods to obtain their initial TGT, such as a password, can only access non-critical services, such as local logins.


.Example of authenticating using different technologies

image::auth_indicators.png[]

This procedure describes creating an IdM service and configuring it to require particular Kerberos authentication indicators from incoming service ticket requests.

include::modules/identity-management/proc_creating-an-idm-service-entry-and-its-kerberos-keytab.adoc[leveloffset=+1]

include::modules/identity-management/proc_associating-authentication-indicators-with-an-idm-service-using-idm-cli.adoc[leveloffset=+1]

include::modules/identity-management/proc_associating-authentication-indicators-with-an-idm-service-using-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_retrieving-a-kerberos-service-ticket-for-an-idm-service.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* See xref:kerberos-authentication-indicators_managing-kerberos-ticket-policies[Kerberos authentication indicators].

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-enforcing-authentication-indicators-for-an-idm-service[:context: {parent-context-of-enforcing-authentication-indicators-for-an-idm-service}]
ifndef::parent-context-of-enforcing-authentication-indicators-for-an-idm-service[:!context:]
