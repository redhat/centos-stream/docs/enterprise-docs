:_newdoc-version: 2.15.1
:_template-generated: 2024-02-06

ifdef::context[:parent-context-of-managing-lv-snapshots: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="managing-lv-snapshots"]
endif::[]
ifdef::context[]
[id="managing-lv-snapshots_{context}"]
endif::[]
= Managing logical volume snapshots

:context: managing-lv-snapshots

[role="_abstract"]
A snapshot is a logical volume (LV) that mirrors the content of another LV at a specific point in time.

include::modules/filesystems-and-storage/con_understanding-logical-volume-snapshots.adoc[leveloffset=+1]

// cow/thick snapshots for: linear, striped, raid, cache, writecache LVs
include::assembly_managing-thick-logical-volume-snapshots.adoc[leveloffset=+1]

// thin snapshots for thin LVs
include::assembly_managing-thin-logical-volume-snapshots.adoc[leveloffset=+1]


ifdef::parent-context-of-managing-lv-snapshots[:context: {parent-context-of-managing-lv-snapshots}]
ifndef::parent-context-of-managing-lv-snapshots[:!context:]

