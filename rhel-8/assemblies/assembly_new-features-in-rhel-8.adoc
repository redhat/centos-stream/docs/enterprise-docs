:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_new-features-in-rhel-8.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-new-features-in-rhel-8: {context}

// The file name and the ID are based on the assembly title.
// For example:
// * file name: assembly_my-assembly-a.adoc
// * ID: [id='assembly_my-assembly-a_{context}']
// * Title: = My assembly A
//
// The ID is used as an anchor for linking to the module.
// Avoid changing it after the module has been published
// to ensure existing links are not broken.
//
// In order for  the assembly to be reusable in other assemblies in a guide,
// include {context} in the ID: [id='a-collection-of-modules_{context}'].
//
// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.

[id="new-features-in-rhel-8_{context}"]
= New features in RHEL 8

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: new-features-in-rhel-8

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

This section documents the most notable changes in RPM packaging between {RHEL} 7 and 8.

// ifdef::packaging-title[]
// * xref:support-for-weak-dependencies_new-features-in-rhel-8[Support for weak dependencies.]
// endif::[]
//
// ifdef::packaging-title[]
// * xref:support-for-boolean-dependencies_new-features-in-rhel-8[Support for boolean dependencies.]
// endif::[]
//
// ifdef::packaging-title[]
// * xref:support-for-big-files_new-features-in-rhel-8[Support for packaging files above 4 GB in size.]
// endif::[]
//
// ifdef::packaging-title[]
// * xref:support-for-file-triggers_new-features-in-rhel-8[Support for the file triggers feature.]
// endif::[]

// ifdef::packaging-title[]
// * xref:stricter-spec-parser_new-features-in-rhel-8[Stricter SPEC parser.]
// endif::[]

// * Simplified signature checking output in non-verbose mode.
// * Support for the enforced payload verification.
// * Support for the enforcing signature checking mode
// * Additions and deprecations in macros.

include::assembly_support-for-weak-dependencies.adoc[leveloffset=+1]

include::assembly_support-for-boolean-dependencies.adoc[leveloffset=+1]

include::modules/core-services/con_support-for-file-triggers.adoc[leveloffset=+1]

include::modules/core-services/con_stricter-spec-parser.adoc[leveloffset=+1]

include::assembly_support-for-files-above-4-gb.adoc[leveloffset=+1]

include::modules/core-services/con_other-features.adoc[leveloffset=+1]

// Out of scope according to SME
// include::modules/core-services/con_verifying-payload-contents.adoc[leveloffset=+1]

// [leveloffset=+1] ensures that when a module starts with a level-1 heading
// (= Heading), the heading will be interpreted as a level-2 heading
// (== Heading) in the assembly.

// [id='related-information-{context}']
// == Related information
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-new-features-in-rhel-8}
