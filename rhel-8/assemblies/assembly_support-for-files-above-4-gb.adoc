
ifdef::context[:parent-context-of-assembly_support-for-files-above-4-gb: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_support-for-files-above-4-gb"]
endif::[]
ifdef::context[]
[id="assembly_support-for-files-above-4-gb_{context}"]
endif::[]
= Support for files above 4 GB

:context: assembly_support-for-files-above-4-gb

On {RHEL} 8, *RPM* can use 64-bit variables and tags, which enables operating on files and packages bigger than 4 GB.

// RPM originally used 32 bit variables and tags for storing various (file) sizes. With the increasing sizes of files and packages RPM has added support for 64 bit values step by step. If you are an RPM API user you need to go theses steps with us or your code will fail if it enounters packages making use of these new features.

// Even if you are not an RPM API user you might need to care. RPM packages can be bigger than 4GB nowadays. So if you are just handling them as files without looking into them your code still needs to be able to handle 64 bit file sizes.


include::modules/core-services/ref_64-bit-rpm-tags.adoc[leveloffset=+1]

include::modules/core-services/con_using-64-bit-tags-on-command-line.adoc[leveloffset=+1]

ifdef::parent-context-of-assembly_support-for-files-above-4-gb[:context: {parent-context-of-assembly_support-for-files-above-4-gb}]
ifndef::parent-context-of-assembly_support-for-files-above-4-gb[:!context:]
