:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_working-with-container-images: {context}]


ifndef::context[]
[id="assembly_working-with-container-images"]
endif::[]
ifdef::context[]
[id="assembly_working-with-container-images_{context}"]
endif::[]
= Working with container images

:context: assembly_working-with-container-images


[role="_abstract"]
The Podman tool is designed to work with container images. You can use this tool to pull the image, inspect, tag, save, load, redistribute, and define the image signature.

include::modules/containers/proc_pulling-container-images-using-short-name-aliases.adoc[leveloffset=+1]

include::modules/containers/proc_listing-images.adoc[leveloffset=+1]

include::modules/containers/proc_inspecting-local-images.adoc[leveloffset=+1]

include::modules/containers/proc_inspecting-remote-images.adoc[leveloffset=+1]

include::modules/containers/proc_copying-container-images.adoc[leveloffset=+1]

include::modules/containers/proc_copying-image-layers-to-a-local-directory.adoc[leveloffset=+1]

include::modules/containers/proc_tagging-images.adoc[leveloffset=+1]

include::modules/containers/proc_building-multi-architecture-images.adoc[leveloffset=+1]

include::modules/containers/proc_saving-and-loading-images.adoc[leveloffset=+1]

include::modules/containers/proc_redistributing-ubi-images.adoc[leveloffset=+1]

include::modules/containers/proc_removing-images.adoc[leveloffset=+1]


// == Prerequisites


//[role="_additional-resources"]
//== Additional resources (or Next steps)


ifdef::parent-context-of-assembly_working-with-container-images[:context: {parent-context-of-assembly_working-with-container-images}]
ifndef::parent-context-of-assembly_working-with-container-images[:!context:]
