:_mod-docs-content-type: ASSEMBLY
// included in assembly_using-and-configuring-firewalld.adoc

:parent-context-of-using-zones-to-manage-incoming-traffic-depending-on-a-source: {context}

[id="using-zones-to-manage-incoming-traffic-depending-on-a-source_{context}"]
= Using zones to manage incoming traffic depending on a source

:context: using-zones-to-manage-incoming-traffic-depending-on-a-source

[role="_abstract"]
You can use zones to manage incoming traffic based on its source. Incoming traffic in this context is any data that is destined for your system, or passes through the host running `firewalld`. The source typically refers to the IP address or network range from which the traffic originates. As a result, you can sort incoming traffic and assign it to different zones to allow or disallow services that can be reached by that traffic.

Matching by source address takes precedence over matching by interface name. When you add a source to a zone, the firewall will prioritize the source-based rules for incoming traffic over interface-based rules. This means that if incoming traffic matches a source address specified for a particular zone, the zone associated with that source address will determine how the traffic is handled, regardless of the interface through which it arrives. On the other hand, interface-based rules are generally a fallback for traffic that does not match specific source-based rules. These rules apply to traffic, for which the source is not explicitly associated with a zone. This allows you to define a default behavior for traffic that does not have a specific source-defined zone.

include::modules/packet-filtering/proc_adding-a-source.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_removing-a-source.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_removing-a-source-port.adoc[leveloffset=+1]

include::modules/packet-filtering/proc_using-zones-and-sources-to-allow-a-service-for-only-a-specific-domain.adoc[leveloffset=+1]


:context: {parent-context-of-using-zones-to-manage-incoming-traffic-depending-on-a-source}
