:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id="installing-an-ipa-server-without-a-ca_{context}"]

= Installing an IdM server: With integrated DNS, without a CA
:assembly: installing-server-without-ca
:context: install-server-no-ca

[role="_abstract"]
Installing a new {IPA} (IdM) server with integrated DNS has the following advantages:

* You can automate much of the maintenance and DNS record management using native IdM tools. For example, DNS SRV records are automatically created during the setup, and later on are automatically updated.

* You can configure global forwarders during the installation of the IdM server for a stable external internet connection. Global forwarders are also useful for trusts with Active Directory.

* You can set up a DNS reverse zone to prevent emails from your domain to be considered spam by email servers outside of the IdM domain.

Installing IdM with integrated DNS has certain limitations:

* IdM DNS is not meant to be used as a general-purpose DNS server. Some of the advanced DNS functions are not supported. For more information, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/planning_identity_management/index#dns-services-available-in-an-ipa-server_planning-dns[DNS services available in an IdM server].

This chapter describes how you can install a new IdM server without a certificate authority (CA).

include::modules/identity-management/con_certificates-required-to-install-an-ipa-server-without-a-ca.adoc[leveloffset=+1]

include::modules/identity-management/proc_installing-an-ipa-server-interactive-installation.adoc[leveloffset=+1]

:context: {parent-context}
