:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly_configuring-node-placement-strategy: {context}

[id='assembly_configuring-node-placement-strategy-{context}']
= Configuring a node placement strategy
:context: configuring-node-placement-strategy

[role="_abstract"]
Pacemaker decides where to place a resource according to the resource allocation scores on every node. The resource will be allocated to the node where the resource has the highest score. This allocation score is derived from a combination of factors, including resource constraints, `resource-stickiness` settings, prior failure history of a resource on each node, and utilization of each node.

If the resource allocation scores on all the nodes are equal, by the default placement strategy Pacemaker will choose a node with the least number of allocated resources for balancing the load. If the number of resources on each node is equal, the first eligible node listed in the CIB will be chosen to run the resource.

Often, however, different resources use significantly different proportions of a node’s capacities (such as memory or I/O). You cannot always balance the load ideally by taking into account only the number of resources allocated to a node. In addition, if resources are placed such that their combined requirements exceed the provided capacity, they may fail to start completely or they may run with degraded performance. To take these factors into account, Pacemaker allows you to configure the following components:

* the capacity a particular node provides

* the capacity a particular resource requires

* an overall strategy for placement of resources


include::modules/high-availability/proc_configuring-utilization-attributes.adoc[leveloffset=+1]

include::modules/high-availability/ref_pacemaker-resource-allocation.adoc[leveloffset=+1]

include::modules/high-availability/ref_resource-placement-strategy-guidelines.adoc[leveloffset=+1]

include::modules/high-availability/ref_node-utilization-resource-agent.adoc[leveloffset=+1]


:context: {parent-context-of-assembly_configuring-node-placement-strategy}

