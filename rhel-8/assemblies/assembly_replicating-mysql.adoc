:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-replicating-mysql: {context}]

[id="replicating-mysql_{context}"]
= Replicating MySQL with TLS encryption

:context: replicating-mysql

[role="_abstract"]
*MySQL* provides various configuration options for replication, ranging from basic to advanced. This section describes a transaction-based way to replicate in *MySQL* on freshly installed *MySQL* servers using global transaction identifiers (GTIDs). Using GTIDs simplifies transaction identification and consistency verification.

To set up replication in *MySQL*, you must:

* xref:proc_configuring-a-mysql-source-server_{context}[Configure a source server]
* xref:proc_configuring-a-mysql-replica-server_{context}[Configure a replica server]
* xref:proc_creating-a-replication-user-on-the-mysql-source-server_{context}[Create a replication user on the source server]
//Not needed with GTIDs:* xref:proc_configuring-a-starting-point-for-replication-on-the-mysql-source-server_{context}[Configure a starting point for replication on the source server]
* xref:proc_connecting-the-replica-server-to-the-source-server_{context}[Connect the replica server to the source server]

IMPORTANT: If you want to use existing *MySQL* servers for replication, you must first synchronize data. See the link:https://dev.mysql.com/doc/mysql-replication-excerpt/8.0/en/replication-howto.html[upstream documentation] for more information.


include::modules/core-services/proc_configuring-a-mysql-source-server.adoc[leveloffset=+1]

include::modules/core-services/proc_configuring-a-mysql-replica-server.adoc[leveloffset=+1]

include::modules/core-services/proc_creating-a-replication-user-on-the-mysql-source-server.adoc[leveloffset=+1]

// Not needed when using GTIDs:
//include::modules/core-services/proc_configuring-a-starting-point-for-replication-on-the-mysql-source-server.adoc[leveloffset=+1]

include::modules/core-services/proc_connecting-the-replica-server-to-the-source-server.adoc[leveloffset=+1]

include::modules/core-services/proc_verifying-replication-on-a-mysql-server.adoc[leveloffset=+1]


ifdef::parent-context-of-replicating-mysql[:context: {parent-context-of-replicating-mysql}]
ifndef::parent-context-of-replicating-mysql[:!context:]
