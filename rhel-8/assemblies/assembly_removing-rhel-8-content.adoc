:_mod-docs-content-type: ASSEMBLY
:parent-context-of-removing-rhel-8-content: {context}

[id="removing-rhel-8-content_{context}"]
= Removing {ProductShortName} {ProductNumber} content

:context: removing-rhel-8-content

// ustory: As a system administrator, I want to remove content I have installed on my RHEL 8 system so that it is no longer present.

In the following sections, learn how to remove content in {RHEL8} by using *{PackageManagerName}*.

include::modules/appstream/proc_removing-installed-packages.adoc[leveloffset=+1]

include::modules/core-services/proc_removing-a-package-group-with-yum.adoc[leveloffset=+1]

include::assembly_removing-installed-modular-content.adoc[leveloffset=+1]

include::modules/core-services/proc_optimizing-package-removal.adoc[leveloffset=+1]

// Already included in assembly_managing-versions-of-appstream-content.adoc: include::modules/appstream/proc_resetting-module-streams.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* xref:commands-for-removing-content_yum-commands-list[Commands for removing content in RHEL 8]
* `yum(8)` man page on your system



:context: {parent-context-of-removing-rhel-8-content}
