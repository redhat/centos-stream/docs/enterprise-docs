ifdef::context[:parent-context-of-checking-bello-for-common-errors: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="checking-bello-for-common-errors"]
endif::[]
ifdef::context[]
[id="checking-bello-for-common-errors_{context}"]
endif::[]
= Checking a sample Bash program for common errors

:context: checking-bello-for-common-errors

In the following sections, investigate possible warnings and errors that can occur when checking an RPM for common errors on the example of the `bello` `spec` file and `bello` binary RPM.

include::modules/core-services/con_checking-the-bello-spec-file.adoc[leveloffset=+1]


include::modules/core-services/con_checking-the-bello-binary-rpm.adoc[leveloffset=+1]



ifdef::parent-context-of-checking-bello-for-common-errors[:context: {parent-context-of-checking-bello-for-common-errors}]
ifndef::parent-context-of-checking-bello-for-common-errors[:!context:]

