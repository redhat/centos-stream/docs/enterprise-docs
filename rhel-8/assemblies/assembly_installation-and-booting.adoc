:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_installation-and-booting.adoc[leveloffset=+1]


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-installation-and-booting: {context}

// The file name and the ID are based on the assembly title.
// For example:
// * file name: assembly_my-assembly-a.adoc
// * ID: [id='assembly_my-assembly-a_{context}']
// * Title: = My assembly A
//
// The ID is used as an anchor for linking to the module.
// Avoid changing it after the module has been published
// to ensure existing links are not broken.
//
// In order for  the assembly to be reusable in other assemblies in a guide,
// include {context} in the ID: [id='a-collection-of-modules_{context}'].
//
// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.
[id="installer-and-image-creation_{context}"]
= Installer and image creation

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: installation-and-booting

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]



include::modules/installer/ref_add-ons.adoc[leveloffset=+1]

include::modules/installer/ref_installer-networking.adoc[leveloffset=+1]

include::modules/installer/ref_installation-images-and-packages.adoc[leveloffset=+1]

include::modules/installer/ref_graphical-user-interface.adoc[leveloffset=+1]

include::modules/installer/ref_system-purpose.adoc[leveloffset=+1]

include::modules/installer/ref_installer-module-support.adoc[leveloffset=+1]

include::assembly_kickstart-changes.adoc[leveloffset=+1]

include::modules/installer/ref_image-builder.adoc[leveloffset=+1]


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-installation-and-booting}
