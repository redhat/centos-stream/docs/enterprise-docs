:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
// assembly_deploying-a-virtual-machine-on-aws
// enterprise/assemblies/assembly_deploying-a-rhel-image-on-gcp.adoc


ifdef::context[:parent-context: {context}]

[id='creating-a-base-vm-from-an-iso-image_{context}']
= Creating a base VM from an ISO image

// :context: creating-a-base-vm-from-an-iso-image

To create a {ProductShortName} {ProductNumber} base image from an ISO image, enable your host machine for virtualization and create a RHEL virtual machine (VM).

.Prerequisites

ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#enabling-virtualization-in-rhel8_virt-getting-started[Virtualization is enabled] on your host machine.
endif::[]

ifeval::[{ProductNumber} == 9]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/assembly_enabling-virtualization-in-rhel-9_configuring-and-managing-virtualization[Virtualization is enabled] on your host machine.
+
// This xref will only start working when https://gitlab.cee.redhat.com/red-hat-enterprise-linux-documentation/rhel-8-docs/-/merge_requests/2835 is merged
endif::[]

* You have downloaded the latest {ProductName} ISO image from the
ifeval::["{ProductNumberLink}" == "8"]
link:https://access.redhat.com/downloads/content/479/[Red Hat Customer Portal]
endif::[]
ifeval::["{ProductNumberLink}" == "9"]
link:https://access.redhat.com/downloads/content/479/[Red Hat Customer Portal]
endif::[]
ifeval::["{ProductNumberLink}" == "9-beta"]
link:https://access.redhat.com/downloads/content/486/[Red Hat Customer Portal]
endif::[]
and moved the image to `/var/lib/libvirt/images`.


// TODO: The link above needs updating for RHEL 9 (and adding an ifeval, probably)

//If the following ref is needed, conditionalize for AWS and GCP.
// * You must have prepared your system. Refer to xref:preparing-your-system-prior-aws[] for more information.

//* You must have installed the AWS CLI. See xref:installing-aws-command-line-interface[Installing the AWS Command Line Interface]

// .Additional resources
// link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#enabling-virtualization-in-rhel8_virt-getting-started[Enabling virtualization in RHEL 8]


// include::modules/cloud/aws/proc_downloading-the-iso-image.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-creating-a-vm-from-the-iso-image.adoc[leveloffset=+1]

include::modules/cloud/aws/proc_aws-completing-the-rhel-installation.adoc[leveloffset=+1]


ifdef::parent-context[:context: {parent-context}]
ifndef::parent-context[:!context:]
