:_mod-docs-content-type: ASSEMBLY
//include::modules/core-services/attributes-core-services.adoc[]

:parent-context-of-using-databases: {context}

[id='using-databases']
= Database servers

:context: using-databases

include::modules/core-services/con_databases-intro.adoc[leveloffset=+1]

include::assembly_using-mariadb.adoc[leveloffset=+1]

include::assembly_using-mysql.adoc[leveloffset=+1]

include::assembly_using-postgresql.adoc[leveloffset=+1]


:context: {parent-context-of-using-databases}
