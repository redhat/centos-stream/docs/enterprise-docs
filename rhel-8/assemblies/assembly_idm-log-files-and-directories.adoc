////
Retains the context of the parent assembly if this assembly is nested within another assembly.
For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
See also the complementary step on the last line of this file.
////

ifdef::context[:parent-context-of-assembly_idm-log-files-and-directories: {context}]



:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_idm-log-files-and-directories"]
endif::[]
ifdef::context[]
[id="assembly_idm-log-files-and-directories_{context}"]
endif::[]
= IdM log files and directories


:context: assembly_idm-log-files-and-directories

Use the following sections to monitor, analyze, and troubleshoot the individual components of {IPA} (IdM):

* xref:ref_directory-server-log-files_{context}[LDAP]
* xref:ref_the-idm-apache-server-log-files_{context}[Apache web server]
* xref:ref_certificate-system-log-files-in-idm_{context}[Certificate system]
* xref:ref_kerberos-log-files-in-idm_{context}[Kerberos]
* xref:ref_dns-log-files-in-idm_{context}[DNS]
* xref:ref_custodia-log-files-in-idm_{context}[Custodia]

Additionally, you can monitor, analyze, and troubleshoot the xref:ref_idm-server-and-client-log-files-and-directories_{context}[IdM server and client] and xref:proc_enabling-audit-logging-on-an-idm-server_{context}[enable audit logging on an IdM server].

include::modules/identity-management/ref_idm-server-and-client-log-files-and-directories.adoc[leveloffset=+1]

include::modules/identity-management/ref_directory-server-log-files.adoc[leveloffset=+1]

include::modules/identity-management/proc_enabling-audit-logging-on-an-idm-server.adoc[leveloffset=+1]

//include::modules/identity-management/ref_the-directory-server-error-logging-levels.adoc[leveloffset=+1]

include::modules/identity-management/proc_modifying-error-logging-on-an-idm-server.adoc[leveloffset=+1]

include::modules/identity-management/ref_the-idm-apache-server-log-files.adoc[leveloffset=+1]

include::modules/identity-management/ref_certificate-system-log-files-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/ref_kerberos-log-files-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/ref_dns-log-files-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/ref_custodia-log-files-in-idm.adoc[leveloffset=+1]



[role="_additional-resources"]
== Additional resources

* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/assembly_troubleshooting-problems-using-log-files_configuring-basic-system-settings[Viewing Log Files]. You can use `journalctl` to view the logging output of `systemd` unit files.

ifdef::parent-context-of-assembly_idm-log-files-and-directories[:context: {parent-context-of-assembly_idm-log-files-and-directories}]
ifndef::parent-context-of-assembly_idm-log-files-and-directories[:!context:]
