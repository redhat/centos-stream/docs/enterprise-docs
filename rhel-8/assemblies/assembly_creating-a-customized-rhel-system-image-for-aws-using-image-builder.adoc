:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-creating-a-customized-rhel-system-image-for-aws-using-image-builder: {context}]


ifndef::context[]
[id="creating-a-customized-rhel-system-image-for-aws-using-image-builder"]
endif::[]
ifdef::context[]
[id="creating-a-customized-rhel-system-image-for-aws-using-image-builder_{context}"]
endif::[]

//[id="creating-a-customized-rhel-system-image-for-aws-using-image-builder_{context}"]
= Creating and uploading a customized RHEL system image to Amazon Web Services by using Insights image builder

:context: creating-a-customized-rhel-system-image-for-aws-using-image-builder

[role="_abstract"]
You can create customized RHEL system images by using Insights image builder, and upload those images to the Amazon Web Services (AWS) target environment.

=====
WARNING: Red Hat Hybrid Cloud Console does not support uploading support for uploading the images that you created for the Amazon Web Services (AWS) target environment to GovCloud regions.
=====

include::modules/image-builder-cloud/proc_creating-a-customized-rhel-system-image-for-aws-using-image-builder.adoc[leveloffset=+1]

include::modules/image-builder-cloud/proc_accessing-your-customized-rhel-system-image-for-aws-from-your-account.adoc[leveloffset=+1]

include::modules/image-builder-cloud/proc_launching-your-customized-rhel-system-image-for-aws-from-your-aws-ec2.adoc[leveloffset=+1]

include::modules/image-builder-cloud/proc_copying-your-customized-rhel-system-image-for-aws-to-a-different-region-on-your-aws-ec2.adoc[leveloffset=+1]

include::modules/image-builder-cloud/proc_sharing-your-aws-images-to-a-different-region.adoc[leveloffset=+1]

ifdef::parent-context-of-creating-a-customized-rhel-system-image-for-aws-using-image-builder[:context: {parent-context-of-creating-a-customized-rhel-system-image-for-aws-using-image-builder}]
ifndef::parent-context-of-creating-a-customized-rhel-system-image-for-aws-using-image-builder[:!context:]
