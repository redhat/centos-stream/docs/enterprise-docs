:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_handling-interpreter-directives-in-python-scripts: {context}]

ifndef::context[]
[id="assembly_handling-interpreter-directives-in-python-scripts"]
endif::[]
ifdef::context[]
[id="assembly_handling-interpreter-directives-in-python-scripts_{context}"]
endif::[]
= Handling interpreter directives in Python scripts

:context: assembly_handling-interpreter-directives-in-python-scripts

[role="_abstract"]
In {RHEL} 8, executable Python scripts are expected to use interpreter directives (also known as hashbangs or shebangs) that explicitly specify at a minimum the major Python version. For example:

[literal,subs="+quotes,verbatim,normal,normal"]
----
#!/usr/bin/python3
#!/usr/bin/python3.6
#!/usr/bin/python3.8
#!/usr/bin/python3.9
#!/usr/bin/python3.11
#!/usr/bin/python3.12
#!/usr/bin/python2
----

The [filename]`/usr/lib/rpm/redhat/brp-mangle-shebangs` buildroot policy (BRP) script is run automatically when building any RPM package, and attempts to correct interpreter directives in all executable files.

The BRP script generates errors when encountering a Python script with an ambiguous interpreter directive, such as:

[literal,subs="+quotes,verbatim,normal,normal"]
----
#!/usr/bin/python
----

or

[literal,subs="+quotes,verbatim,normal,normal"]
----
#!/usr/bin/env python
----

include::modules/core-services/proc_modifying-interpreter-directives-in-python-scripts.adoc[leveloffset=+1]

include::modules/core-services/proc_changing-interpreter-directives-in-your-custom-packages.adoc[leveloffset=+1]

////
Reusing this assembly in Packaging and distributing SW
[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/packaging_and_distributing_software/[Packaging and distributing software]
////

ifdef::parent-context-of-assembly_handling-interpreter-directives-in-python-scripts[:context: {parent-context-of-assembly_handling-interpreter-directives-in-python-scripts}]
ifndef::parent-context-of-assembly_handling-interpreter-directives-in-python-scripts[:!context:]
