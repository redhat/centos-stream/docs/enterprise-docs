:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-introducing-rhel-for-edge-images: {context}]

ifndef::context[]
[id="introducing-rhel-for-edge-images"]
endif::[]
ifdef::context[]
[id="introducing-rhel-for-edge-images_{context}"]
endif::[]

//[id="introducing-rhel-for-edge-images_{context}"]
= Introduction to RHEL for Edge images
:context: introducing-rhel-for-edge-images

[role="_abstract"]
A RHEL for Edge image is an `rpm-ostree` image that includes system packages to remotely install RHEL on Edge servers. 

The system packages include:

* `Base OS` package
* Podman as the container engine
* Additional RPM Package Manager (RPM) content 

RHEL for Edge is an immutable operating system that contains a `read-only` root directory, and has following characteristics:
 
* The packages are isolated from the root directory.
 
* Each version of the operating system is a separate deployment. Therefore, you can roll back the system to a previous deployment when needed.
 
* The `rpm-ostree` image offers efficient updates over the network.
 
* RHEL for Edge supports multiple operating system branches and repositories.
 
* The image contains a hybrid `rpm-ostree` package system.

You can compose customized RHEL for Edge images by using the RHEL image builder tool. You can also create RHEL for Edge images by accessing the link:https://console.redhat.com/edge[edge management] application in the Red Hat Hybrid Cloud Console platform and configure automated management.
 
Use the edge management application to simplify provisioning and registering your images. To learn more about the edge management, see the link:https://docs.redhat.com/en/documentation/edge_management/1-latest/html/create_rhel_for_edge_images_and_configure_automated_management/index/[Create RHEL for Edge images and configure automated management] documentation.

[WARNING]
====

Using RHEL for Edge customized images that were created by using the *RHEL image builder* on-premise version is not supported in the link:https://console.redhat.com/edge[edge management] application. See link:https://access.redhat.com/articles/7010809[Edge management supportability].
 
The link:https://console.redhat.com/edge[edge management] application supports building and managing only the `edge-commit` and `edge-installer` image types. 

Additionally, you cannot use the link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/composing_installing_and_managing_rhel_for_edge_images/assembly_automatically-provisioning-and-onboarding-rhel-for-edge-devices_composing-installing-managing-rhel-for-edge-images[FIDO Device Onboarding (FDO)] process with images that you create by using the link:https://console.redhat.com/edge[edge management] application.

====

With a RHEL for Edge image, you can achieve the following benefits:

Atomic upgrades::
You know the state of each update, and no changes are seen until you reboot your system.

Custom health checks and intelligent rollbacks::
You can create custom health checks, and if a health check fails, the operating system rolls back to the previous stable state.

Container-focused workflow::
The image updates are staged in the background, minimizing any workload interruptions to the system.

Optimized Over-the-Air updates::
You can make sure that your systems are up-to-date, even with intermittent connectivity, thanks to efficient over-the-air (OTA) delta updates.


include::modules/edge/con_edge-supported-architecture.adoc[leveloffset=+1]

include::modules/edge/con_edge-how-to-compose-and-deploy-a-rhel-for-edge-image.adoc[leveloffset=+1] 

include::modules/edge/con_non-network-based-deployments.adoc[leveloffset=+1]

include::modules/edge/con_network-based-deployments.adoc[leveloffset=+1]

include::modules/edge/con_edge-difference-between-rhel-rpm-images-and-rhel-for-edge-images.adoc[leveloffset=+1]

:context: {parent-context-of-introducing-rhel-for-edge-images}
