ifdef::context[:parent-context-of-assembly_typing-emoji-characters: {context}]

ifndef::context[]
[id="assembly_typing-emoji-characters"]
endif::[]
ifdef::context[]
[id="assembly_typing-emoji-characters_{context}"]
endif::[]
= Typing emoji characters

:context: assembly_typing-emoji-characters

[role="_abstract"]
You can type emoji characters using several different methods in GNOME, depending on the type of the application.

// == Prerequisites
// 
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.
// * X is installed. For information about installing X, see <link>.
// * You can log in to X with administrator privileges.

include::modules/desktop/proc_typing-emoji-characters-in-gtk-applications.adoc[leveloffset=+1]

include::modules/desktop/proc_typing-emoji-characters-in-any-applications.adoc[leveloffset=+1]

// [role="_additional-resources"]
// == Additional resources (or Next steps)
// * A bulleted list of links to other closely-related material. These links can include `link:` and `xref:` macros.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

ifdef::parent-context-of-assembly_typing-emoji-characters[:context: {parent-context-of-assembly_typing-emoji-characters}]
ifndef::parent-context-of-assembly_typing-emoji-characters[:!context:]

