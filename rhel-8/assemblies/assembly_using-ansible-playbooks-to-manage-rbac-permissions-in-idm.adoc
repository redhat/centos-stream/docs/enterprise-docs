:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-using-ansible-playbooks-to-manage-rbac-permissions-in-idm: {context}]
[id="using-ansible-playbooks-to-manage-rbac-permissions-in-idm_{context}"]
= Using Ansible playbooks to manage RBAC permissions in IdM

:context: using-ansible-playbooks-to-manage-rbac-permissions-in-idm
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.

[role="_abstract"]
Role-based access control (RBAC) is a policy-neutral access control mechanism defined around roles, privileges, and permissions. Especially in large companies, using RBAC can help create a hierarchical system of administrators with their individual areas of responsibility.

This chapter describes the following operations performed when managing RBAC permissions in {IPA} (IdM) using Ansible playbooks:

* xref:using-ansible-to-ensure-an-rbac-permission-is-present_{context}[Using Ansible to ensure an RBAC permission is present]
* xref:using-ansible-to-ensure-an-rbac-permission-with-an-attribute-is-present_{context}[Using Ansible to ensure an RBAC permission with an attribute is present]
* xref:using-ansible-to-ensure-an-rbac-permission-is-absent_{context}[Using Ansible to ensure an RBAC permission is absent]
* xref:using-ansible-to-ensure-an-attribute-is-a-member-of-an-idm-rbac-permission_{context}[Using Ansible to ensure an attribute is a member of an IdM RBAC permission]
* xref:using-ansible-to-ensure-an-attribute-is-not-a-member-of-an-idm-rbac-permission_{context}[Using Ansible to ensure an attribute is not a member of an IdM RBAC permission]
* xref:using-ansible-to-rename-an-idm-rbac-permission_{context}[Using Ansible to rename an IdM RBAC permission]

.Prerequisites

*  You understand the xref:using-ansible-playbooks-to-manage-role-based-access-control-in-idm_{ProjectNameID}[concepts and principles of RBAC].

include::modules/identity-management/proc_using-ansible-to-ensure-an-rbac-permission-is-present.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-an-rbac-permission-with-an-attribute-is-present.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-an-rbac-permission-is-absent.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-an-attribute-is-a-member-of-an-idm-rbac-permission.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-ensure-an-attribute-is-not-a-member-of-an-idm-rbac-permission.adoc[leveloffset=+1]

include::modules/identity-management/proc_using-ansible-to-rename-an-idm-rbac-permission.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* See xref:permissions-in-idm_using-ansible-playbooks-to-manage-role-based-access-control-in-idm[Permissions in IdM].
* See xref:privileges-in-idm_using-ansible-playbooks-to-manage-role-based-access-control-in-idm[Privileges in IdM].
* See the `README-permission` file available in the `/usr/share/doc/ansible-freeipa/` directory.
* See the sample playbooks in the `/usr/share/doc/ansible-freeipa/playbooks/ipapermission` directory.

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-using-ansible-playbooks-to-manage-rbac-permissions-in-idm[:context: {parent-context-of-using-ansible-playbooks-to-manage-rbac-permissions-in-idm}]
ifndef::parent-context-of-using-ansible-playbooks-to-manage-rbac-permissions-in-idm[:!context:]
