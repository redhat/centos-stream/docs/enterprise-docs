:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-managing-idm-service-vaults-storing-and-retrieving-secrets: {context}]

[id="managing-idm-service-vaults-storing-and-retrieving-secrets_{context}"]
= Managing IdM service secrets: storing and retrieving secrets

:context: managing-idm-service-vaults-storing-and-retrieving-secrets

[role="_abstract"]
This section shows how an administrator can use a service vault in {IPA} (IdM) to securely store a service secret in a centralized location.
The
ifdef::c-m-idm,u-a-in-idm[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/vaults-in-idm_configuring-and-managing-idm[vault]
endif::[]
ifdef::w-w-v-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/working_with_vaults_in_identity_management/vaults-in-idm_working-with-vaults-in-idm[vault]
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/working_with_vaults_in_identity_management/vaults-in-idm_working-with-vaults-in-identity-management[vault]
endif::[]
endif::[]
used in the example is asymmetric, which means that to use it, the administrator needs to perform the following steps:

. Generate a private key using, for example, the `openssl` utility.
. Generate a public key based on the private key.

The service secret is encrypted with the public key when an administrator archives it into the vault. Afterwards, a service instance hosted on a specific machine in the domain retrieves the secret using the private key. Only the service and the administrator are allowed to access the secret.

If the secret is compromised, the administrator can replace it in the service vault and then redistribute it to those individual service instances that have not been compromised.


.Prerequisites

* The Key Recovery Authority (KRA) Certificate System component has been installed on one or more of the servers in your IdM domain. For details, see xref:installing-the-key-recovery-authority-component-in-idm_vaults-in-idm[Installing the Key Recovery Authority in IdM].


This section includes these procedure

//. xref:creating-an-idm-user-vault-to-store-an-idm-service-password_{context}[Creating an IdM user vault to store an IdM service password]

. xref:storing-an-idm-service-secret-in-an-asymmetric-vault_{context}[Storing an IdM service secret in an asymmetric vault]

. xref:retrieving-a-service-secret-for-an-idm-service-instance_{context}[Retrieving a service secret for an IdM service instance]

. xref:changing-an-idm-service-vault-secret-when-compromised_{context}[Changing an IdM service vault secret when compromised]



.Terminology used

In the procedures:

* *admin* is the administrator who manages the service password.

//* `http_secret` is the name of the private user vault created by the administrator

* *private-key-to-an-externally-signed-certificate.pem* is the file containing the service secret, in this case a private key to an externally signed certificate. Do not confuse this private key with the private key used to retrieve the secret from the vault.

* *secret_vault* is the vault created for the service.

* *HTTP/webserver.idm.example.com* is the service whose secret is being archived.

* *service-public.pem* is the service public key used to encrypt the password stored in *password_vault*.

* *service-private.pem* is the service private key used to decrypt the password stored in *secret_vault*.


//include::modules/identity-management/proc_creating-an-idm-user-vault-to-store-an-idm-service-password.adoc[leveloffset=+1]

include::modules/identity-management/proc_storing-an-idm-service-secret-in-an-asymmetric-vault.adoc[leveloffset=+1]

include::modules/identity-management/proc_retrieving-a-service-secret-for-an-idm-service-instance.adoc[leveloffset=+1]

include::modules/identity-management/proc_changing-an-idm-service-vault-secret-when-compromised.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* See
ifdef::c-m-idm[]
xref:using-ansible-to-manage-idm-service-vaults-storing-and-retrieving-secrets_configuring-and-managing-idm[Using Ansible to manage IdM service vaults: storing and retrieving secrets].
endif::[]
ifndef::c-m-idm[]
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_identity_management/using-ansible-to-manage-idm-service-vaults-storing-and-retrieving-secrets_configuring-and-managing-idm[Using Ansible to manage IdM service vaults: storing and retrieving secrets].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_ansible_to_install_and_manage_identity_management/using-ansible-to-manage-idm-service-vaults-storing-and-retrieving-secrets_using-ansible-to-install-and-manage-identity-management[Using Ansible to manage IdM service vaults: storing and retrieving secrets].
endif::[]
endif::[]

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-managing-idm-service-vaults-storing-and-retrieving-secrets[:context: {parent-context-of-managing-idm-service-vaults-storing-and-retrieving-secrets}]
ifndef::parent-context-of-managing-idm-service-vaults-storing-and-retrieving-secrets[:!context:]
