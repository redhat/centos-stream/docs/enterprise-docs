:_mod-docs-content-type: ASSEMBLY
:parent-context-of-kickstart-installation-basics: {context}

[id="kickstart-installation-basics_{context}"]
= Kickstart installation basics

:context: kickstart-installation-basics

[role="_abstract"]
The following provides basic information about Kickstart and how to use it to automate installing {RHEL}.


include::modules/installer/con_what-are-kickstart-installations.adoc[leveloffset=+1]

include::modules/installer/con_automated-installation-workflow.adoc[leveloffset=+1]


:context: {parent-context-of-kickstart-installation-basics}
