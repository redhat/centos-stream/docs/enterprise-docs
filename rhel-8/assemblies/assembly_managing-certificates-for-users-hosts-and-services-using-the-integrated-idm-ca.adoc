:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca: {context}]
[id="managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca_{context}"]
= Managing certificates for users, hosts, and services using the integrated IdM CA

:context: managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca

[role="_abstract"]
To learn more about how to manage certificates in {IPA} (IdM) using the integrated CA, the `ipa` CA, and its sub-CAs, see the following sections:

* xref:requesting-new-certificates-for-a-user-host-or-service-using-idm-web-ui_{context}[Requesting new certificates for a user, host, or service using the IdM Web UI].
* Requesting new certificates for a user, host, or service from the IdM CA using the IdM CLI:

** xref:requesting-new-certificates-for-a-user-host-or-service-from-idm-ca-using-certutil_{context}[Requesting new certificates for a user, host, or service from IdM CA using certutil]
*** For a specific example of requesting a new user certificate from the IdM CA using the `certutil` utility and exporting it to an IdM client, see xref:requesting-and-exporting-a-user-certificate_dc-web-ui-auth[Requesting a new user certificate and exporting it to the client].
** xref:requesting-new-certificates-for-a-user-host-or-service-from-idm-ca-using-openssl_{context}[Requesting new certificates for a user, host, or service from IdM CA using openssl]

You can also request new certificates for a service from the IdM CA using the `certmonger` utility. For more information, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_and_managing_identity_management/index#using-certmonger_configuring-and-managing-idm[Requesting new certificates for a service from IdM CA using certmonger].
endif::[]
ifeval::[{ProductNumber} == 9]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_certificates_in_idm/using-certmonger_managing-certificates-in-idm#obtain-service-cert-with-certmonger_certmonger-for-issuing-renewing-service-certs[Requesting new certificates for a service from IdM CA using certmonger].
endif::[]

.Prerequisites

* Your IdM deployment contains an integrated CA:

** For information about how to plan your CA services in IdM, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/planning_identity_management/planning-your-ca-services_planning-identity-management[Planning your CA services].

** For information about how to install an IdM server with integrated DNS and integrated CA as the root CA, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/installing_identity_management/index#installing-an-ipa-server-with-integrated-dns_installing-identity-management[Installing an IdM server: With integrated DNS, with an integrated CA as the root CA]
** For information about how to install an IdM server with integrated DNS and an external CA as the root CA, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/installing_identity_management/index#installing-an-ipa-server-with-external-ca_installing-identity-management[Installing an IdM server: With integrated DNS, with an external CA as the root CA]
** For information about how to install an IdM server without integrated DNS and with an integrated CA as the root CA, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/installing_identity_management/index#installing-an-ipa-server-without-integrated-dns_installing-identity-management[Installing an IdM server: Without integrated DNS, with an integrated CA as the root CA].

** Optional: Your IdM deployment supports users authenticating with a certificate:
*** For information about how to configure your IdM deployment to support user authentication with a certificate stored in the IdM client filesystem, see xref:dc-web-ui-auth_{parent-context-of-managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca}[Configuring authentication with a certificate stored on the desktop of an IdM client].
*** For information about how to configure your IdM deployment to support user authentication with a certificate stored on a smart card inserted into an IdM client, see
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-idm-for-smart-card-auth_managing-smart-card-authentication[Configuring Identity Management for smart card authentication].
*** For information about how to configure your IdM deployment to support user authentication with smart cards issued by an Active Directory certificate system, see
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_smart_card_authentication/configuring-certificates-issued-by-adcs-for-smart-card-authentication-in-idm_managing-smart-card-authentication[Configuring certificates issued by ADCS for smart card authentication in IdM].

include::modules/identity-management/proc_requesting-new-certificates-for-a-user-host-or-service-using-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_requesting-new-certificates-for-a-user-host-or-service-from-idm-ca-using-certutil.adoc[leveloffset=+1]

include::modules/identity-management/proc_requesting-new-certificates-for-a-user-host-or-service-from-idm-ca-using-openssl.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* See xref:revoking-certificates-with-the-integrated-IdM-CAs_managing-the-validity-of-certificates-in-idm[Revoking certificates with the integrated IdM CAs].
* See xref:restoring-certificates-with-the-integrated-IdM-CAs_managing-the-validity-of-certificates-in-idm[Restoring certificates with the integrated IdM CAs].
* See xref:restricting-an-application-to-trust-only-a-subset-of-certificates_{parent-context-of-managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca}[Restricting an application to trust only a subset of certificates].

// Restore the context to what it was before this assembly.
ifdef::parent-context-of-managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca[:context: {parent-context-of-managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca}]
ifndef::parent-context-of-managing-certificates-for-users-hosts-and-services-using-the-integrated-idm-ca[:!context:]
