:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-creating-a-deduplicated-and-compressed-logical-volume: {context}]

ifndef::context[]
[id="creating-a-deduplicated-and-compressed-logical-volume"]
endif::[]
ifdef::context[]
[id="creating-a-deduplicated-and-compressed-logical-volume_{context}"]
endif::[]
= Creating a deduplicated and compressed logical volume

:context: creating-a-deduplicated-and-compressed-logical-volume

[role="_abstract"]
You can create an LVM logical volume that uses the VDO feature to deduplicate and compress data.


include::modules/filesystems-and-storage/con_the-physical-and-logical-size-of-an-lvm-vdo-volume.adoc[leveloffset=+1]

//since the recommended logical size information was added to the above module and ceph is currently no supported, commenting out this module-
//include::modules/filesystems-and-storage/con_the-recommended-logical-size-for-vdo-logical-volumes.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/con_slab-size-in-vdo.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_installing-vdo.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_creating-and-mounting-an-lvm-vdo-volume.adoc[leveloffset=+1]

include::modules/core-services/proc_configuring-an-lvm-vdo-volume-using-the-storage-rhel-system-role.adoc[leveloffset=+1]

include::modules/cockpit/proc_creating-lvm-vdo-volumes-in-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_formatting-lvm-vdo-volumes-in-the-web-console.adoc[leveloffset=+1]

include::modules/cockpit/proc_extending-lvm-vdo-volumes-in-the-web-console.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_changing-the-compression-settings-on-an-lvm-vdo-volume.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_changing-the-deduplication-settings-on-an-lvm-vdo-volume.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_managing-thin-provisioned-lvm-vdo-volumes.adoc[leveloffset=+1]

ifdef::parent-context-of-creating-a-deduplicated-and-compressed-logical-volume[:context: {parent-context-of-creating-a-deduplicated-and-compressed-logical-volume}]
ifndef::parent-context-of-creating-a-deduplicated-and-compressed-logical-volume[:!context:]
