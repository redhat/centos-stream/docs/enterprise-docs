:_mod-docs-content-type: ASSEMBLY
// included in assembly_using-and-configuring-firewalld.adoc

:parent-context-of-configuring-firewall-lockdown: {context}

[id="configuring-firewall-lockdown_{context}"]
= Configuring firewall lockdown

:context: configuring-firewall-lockdown

[role="_abstract"]
Local applications or services are able to change the firewall configuration if they are running as [systemitem]`root` (for example, [application]*libvirt*). With this feature, the administrator can lock the firewall configuration so that either no applications or only applications that are added to the lockdown allow list are able to request firewall changes. The lockdown settings default to disabled. If enabled, the user can be sure that there are no unwanted configuration changes made to the firewall by local applications or services.

include::modules/packet-filtering/proc_configuring-lockdown-using-CLI.adoc[leveloffset=+1]

include::modules/packet-filtering/ref_overview-of-lockdown-allowlist-configuration-files.adoc[leveloffset=+1]

:context: {parent-context-of-configuring-firewall-lockdown}


