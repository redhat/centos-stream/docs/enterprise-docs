:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_verifying-system-certificates-using-idm-healthcheck.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-verifying-system-certificates-using-idm-healthcheck: {context}

[id='verifying-system-certificates-using-idm-healthcheck_{context}']
= Verifying system certificates using IdM Healthcheck

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: verifying-dogtag-certificates-using-healthcheck

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.

[role="_abstract"]
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

Learn more about identifying issues with system certificates in Identity Management (IdM) by using the Healthcheck tool.

For details, see
ifeval::[{ProductNumber} == 8]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_and_managing_identity_management/index#healthcheck-in-idm_collecting-idm-healthcheck-information[Healthcheck in IdM].

.Prerequisites

* The Healthcheck tool is only available on RHEL 8.1 or newer.
endif::[]
ifeval::[{ProductNumber} == 9]
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/using_idm_healthcheck_to_monitor_your_idm_environment/installing-and-running-the-ipa-healthcheck-tool_using-idm-healthcheck-to-monitor-your-idm-environment#healthcheck-in-idm_installing-and-running-the-ipa-healthcheck-tool[Healthcheck in IdM].
endif::[]

include::modules/identity-management/con_system-certificates-healthcheck-tests.adoc[leveloffset=+1]

include::modules/identity-management/proc_screening-system-certificates-using-healthcheck.adoc[leveloffset=+1]


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-verifying-system-certificates-using-idm-healthcheck}
