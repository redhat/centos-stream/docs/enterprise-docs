:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// /configuring-and-maintaining/performing-disaster-recovery-with-identity-management

ifdef::context[:parent-context-of-recovering-from-data-loss-with-snapshots: {context}]


[id="recovering-from-data-loss-with-snapshots_{context}"]
= Recovering from data loss with VM snapshots

:context: recovering-from-data-loss-with-snapshots

[role="_abstract"]
If a data loss event occurs, you can restore a Virtual Machine (VM) snapshot of a Certificate Authority (CA) replica to repair the lost data, or deploy a new environment from it.

include::modules/identity-management/proc_recovering-from-only-snapshot.adoc[leveloffset=+1]

include::modules/identity-management/proc_recovering-from-snapshot-partially-working.adoc[leveloffset=+1]

include::modules/identity-management/proc_recovering-from-snapshot-new.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-recovering-from-data-loss-with-snapshots[:context: {parent-context-of-recovering-from-data-loss-with-snapshots}]
ifndef::parent-context-of-recovering-from-data-loss-with-snapshots[:!context:]
