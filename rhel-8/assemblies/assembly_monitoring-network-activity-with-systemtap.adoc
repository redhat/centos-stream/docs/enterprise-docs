:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly_monitoring-network-activity-with-systemtap: {context}

[id="monitoring-network-activity-with-systemtap_{context}"]
= Monitoring network activity with SystemTap


:context: assembly_monitoring-network-activity-with-systemtap

[role="_abstract"]
You can use helpful example SystemTap scripts available in the `/usr/share/systemtap/testsuite/systemtap.examples/` directory, upon installing the `systemtap-testsuite` package, to monitor and investigate the network activity of your system.

include::modules/performance/proc_profiling-network-activity-with-systemtap.adoc[leveloffset=+1]

include::modules/performance/proc_tracing-functions-called-in-network-socket-code-with-systemtap.adoc[leveloffset=+1]

//include::modules/performance/proc_monitoring-incoming-tcp-connections-with-systemtap.adoc[leveloffset=+1]

//commenting out for publishing until we can find out why there is no output with this script
include::modules/performance/proc_monitoring-network-packet-drops-with-systemtap.adoc[leveloffset=+1]


:context: {parent-context-of-assembly_monitoring-network-activity-with-systemtap}
