ifdef::context[:parent-context-of-avoiding-listen-queue-lock-contention: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="avoiding-listen-queue-lock-contention"]
endif::[]
ifdef::context[]
[id="avoiding-listen-queue-lock-contention_{context}"]
endif::[]
= Avoiding listen queue lock contention

:context: avoiding-listen-queue-lock-contention
Queue lock contention can cause packet drops and higher CPU usage and, consequently, a higher latency. You can avoid queue lock contention on the receive (RX) and transmit (TX) queue by tuning your application and using transmit packet steering.


include::modules/networking/con_avoiding-rx-queue-lock-contention-the-so_reuseport-and-so_reuseport_bpf-socket-options.adoc[leveloffset=+1]

include::modules/networking/proc_avoiding-tx-queue-lock-contention-transmit-packet-steering.adoc[leveloffset=+1]

include::modules/networking/proc_disabling-the-generic-receive-offload-feature-on-servers-with-high-udp-traffic.adoc[leveloffset=+1]


ifdef::parent-context-of-avoiding-listen-queue-lock-contention[:context: {parent-context-of-avoiding-listen-queue-lock-contention}]
ifndef::parent-context-of-avoiding-listen-queue-lock-contention[:!context:]

