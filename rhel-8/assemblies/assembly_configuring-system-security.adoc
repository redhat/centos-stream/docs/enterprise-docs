:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// Configuring basic system settings (title)

ifdef::context[:parent-context-of-assembly_configuring-system-security: {context}]



ifndef::context[]
[id="assembly_configuring-system-security"]
endif::[]
ifdef::context[]
[id="assembly_configuring-system-security_{context}"]
endif::[]
= Configuring basic system security

:context: assembly_configuring-system-security

[role="_abstract"]
Computer security is the protection of computer systems and their hardware, software, information, and services from theft, damage, disruption, and misdirection. Ensuring computer security is an essential task, in particular in enterprises that process sensitive data and handle business transactions.

This section covers only the basic security features that you can configure after installation of the operating system.

include::modules/core-services/proc_enabling-the-firewalld-service.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 8]
include::modules/core-services/proc_managing-firewall-in-the-rhel-8-web-console.adoc[leveloffset=+1]
endif::[]

include::modules/core-services/con_managing-basic-selinux-settings.adoc[leveloffset=+1]

ifeval::[{ProductNumber} == 8]
include::modules/core-services/proc_switching-selinux-modes-in-the-rhel-8-web-console.adoc[leveloffset=+1]
endif::[]


[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/assembly_using-secure-communications-between-two-systems-with-openssh_securing-networks#generating-ssh-key-pairs_assembly_using-secure-communications-between-two-systems-with-openssh[Generating SSH key pairs]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/assembly_using-secure-communications-between-two-systems-with-openssh_securing-networks#setting-an-openssh-server-for-key-based-authentication_assembly_using-secure-communications-between-two-systems-with-openssh[Setting an OpenSSH server for key-based authentication]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/security_hardening/[Security hardening]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/using_selinux/[Using SELinux]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/securing_networks/[Securing networks]
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/using_selinux/index#deploying-the-same-selinux-configuration-on-multiple-systems_using-selinux[Deploying the same SELinux configuration on multiple systems]
endif::[]


ifdef::parent-context-of-assembly_configuring-system-security[:context: {parent-context-of-assembly_configuring-system-security}]
ifndef::parent-context-of-assembly_configuring-system-security[:!context:]
