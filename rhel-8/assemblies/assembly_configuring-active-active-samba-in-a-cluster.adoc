:_mod-docs-content-type: ASSEMBLY
:parent-context: {context}

[id='assembly_configuring-active-active-samba-in-a-cluster-{context}']

= Configuring an active/active Samba server in a Red Hat High Availability cluster
:context: configuring-ha-samba

The Red Hat High Availability Add-On provides support for configuring Samba in an active/active cluster configuration. In the following example, you are configuring an active/active Samba server on a two-node RHEL cluster.

For information about support policies for Samba, see link:https://access.redhat.com/articles/3278591[Support Policies for RHEL High Availability - ctdb General Policies] and link:https://access.redhat.com/articles/3252211[Support Policies for RHEL Resilient Storage - Exporting gfs2 contents via other protocols] on the Red Hat Customer Portal. 

To configure Samba in an active/active cluster:

. Configure a GFS2 file system and its associated cluster resources.

. Configure Samba on the cluster nodes.

. Configure the Samba cluster resources.

. Test the Samba server you have configured.

include::modules/high-availability/proc_configuring-gfs2-for-clustered-samba.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-samba-for-ha-cluster.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-samba-cluster-resources.adoc[leveloffset=+1]

include::modules/high-availability/proc_verifying-clustered-samba-configuration.adoc[leveloffset=+1]

:context: {parent-context}


