:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly_controlling-cluster-behavior: {context}

[id='assembly_controlling-cluster-behavior-{context}']
= Pacemaker cluster properties
:context: controlling-cluster-behavior


[role="_abstract"]
Cluster properties control how the cluster behaves when confronted with situations that might occur during cluster operation.

include::modules/high-availability/ref_cluster-properties-options.adoc[leveloffset=+1]

include::modules/high-availability/proc_setting-cluster-properties.adoc[leveloffset=+1]

include::modules/high-availability/proc_querying-cluster-property-settings.adoc[leveloffset=+1]

include::modules/high-availability/proc_exporting-properties.adoc[leveloffset=+1]

:context: {parent-context-of-assembly_controlling-cluster-behavior}

