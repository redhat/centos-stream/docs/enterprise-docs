:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_working-with-containers: {context}]


ifndef::context[]
[id="assembly_working-with-containers"]
endif::[]
ifdef::context[]
[id="assembly_working-with-containers_{context}"]
endif::[]
= Working with containers

:context: assembly_working-with-containers


[role="_abstract"]
Containers represent a running or stopped process created from the files located in a decompressed container image. You can use the Podman tool to work with containers.

include::modules/containers/con_podman-run-command.adoc[leveloffset=+1]

include::modules/containers/proc_running-commands-in-a-container-from-the-host.adoc[leveloffset=+1]

include::modules/containers/proc_running-commands-inside-the-container.adoc[leveloffset=+1]

include::modules/containers/proc_listing-containers.adoc[leveloffset=+1]

include::modules/containers/proc_starting-containers.adoc[leveloffset=+1]

include::modules/containers/proc_inspecting-containers-from-the-host.adoc[leveloffset=+1]

include::modules/containers/proc_mounting-directory-on-localhost-to-the-container.adoc[leveloffset=+1]

include::modules/containers/proc_mounting-a-container-filesystem.adoc[leveloffset=+1]

include::modules/containers/proc_running-a-service-as-a-daemon-with-a-static-ip.adoc[leveloffset=+1]

include::modules/containers/proc_executing-commands-inside-a-running-container.adoc[leveloffset=+1]

include::modules/containers/proc_sharing-files-between-two-containers.adoc[leveloffset=+1]

include::modules/containers/proc_exporting-and-importing-containers.adoc[leveloffset=+1]

include::modules/containers/proc_stopping-containers.adoc[leveloffset=+1]

include::modules/containers/proc_removing-containers.adoc[leveloffset=+1]

include::modules/containers/con_creating-selinux-policies-for-containers.adoc[leveloffset=+1]

include::modules/containers/proc_configuring-pre-execution-hooks-in-podman.adoc[leveloffset=+1]

include::modules/containers/con_debugging-applications-in-containers.adoc[leveloffset=+1]

//== Prerequisites


//[role="_additional-resources"]
//== Additional resources (or Next steps)


ifdef::parent-context-of-assembly_working-with-containers[:context: {parent-context-of-assembly_working-with-containers}]
ifndef::parent-context-of-assembly_working-with-containers[:!context:]
