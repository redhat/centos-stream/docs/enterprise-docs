:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_managing-sr-iov-devices.adoc[leveloffset=+1]

:parent-context-of-managing-sr-iov-devices: {context}


[id='managing-sr-iov-devices_{context}']
= Managing SR-IOV devices

:context: managing-sr-iov-devices


ifdef::internal[]
[cols="1,4"]
|===
| Included in |
Managing Virtual Devices
| User story |
As a system/virt admin, I want to set up use a single physical network device for multiple VMs to maximize the virtual networking performance and safeguarding the transferred data.
| Jira |
https://projects.engineering.redhat.com/browse/RHELPLAN-11264
| BZ |
BUGZILLA LINK
| SMEs |
Laine Stump, Alex Willamson, Michael Tsirkin
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

An emulated virtual device often uses more CPU and memory than a hardware network device. This can limit the performance of a virtual machine (VM). However, if any devices on your virtualization host support Single Root I/O Virtualization (SR-IOV), you can use this feature to improve the device performance, and possibly also the overall performance of your VMs.

////
.Prerequisites

* TBD?
////

include::modules/virtualization/con_what-is-sr-iov.adoc[leveloffset=+1]

include::modules/virtualization/proc_attaching-sr-iov-networking-devices-to-virtual-machines.adoc[leveloffset=+1]

include::modules/virtualization/ref_supported-devices-for-sr-iov-assignment-in-rhel-8.adoc[leveloffset=+1]

////
[id='related-information-{context}']
== Related information

* TBD?
////

:context: {parent-context-of-managing-sr-iov-devices}
