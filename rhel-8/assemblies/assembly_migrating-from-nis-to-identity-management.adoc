:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>


ifdef::context[:parent-context-of-assembly_migrating-from-nis-to-identity-management: {context}]

ifndef::context[]
[id="assembly_migrating-from-nis-to-identity-management"]
endif::[]
ifdef::context[]
[id="assembly_migrating-from-nis-to-identity-management_{context}"]
endif::[]
= Migrating from NIS to Identity Management

:context: assembly_migrating-from-nis-to-identity-management


[role="_abstract"]
A Network Information Service (NIS) server can contain information about users, groups, hosts, netgroups and automount maps. As a system administrator you can migrate these entry types, authentication, and authorization from NIS server to an {IPA} (IdM) server so that all user management operations are performed on the IdM server. Migrating from NIS to IdM will also allow you access to more secure protocols such as Kerberos.

[id="migration-prerequisites_{context}"]

include::modules/identity-management/proc_enabling-nis-in-identity-management.adoc[leveloffset =+ 1]

include::modules/identity-management/proc_migrating-user-entries-from-nis-to-idm.adoc[leveloffset =+ 1]

include::modules/identity-management/proc_migrating-user-group-from-nis-to-idm.adoc[leveloffset =+ 1]

include::modules/identity-management/proc_migrating-host-entries-from-nis-to-idm.adoc[leveloffset =+ 1]

include::modules/identity-management/proc_migrating-netgroup-entries-from-nis-to-idm.adoc[leveloffset =+ 1]

include::modules/identity-management/proc_migrating-automount-map-from-nis-to-idm.adoc[leveloffset =+ 1]

////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_migrating-from-nis-to-identity-management[:context: {parent-context-of-assembly_migrating-from-nis-to-identity-management}]
ifndef::parent-context-of-assembly_migrating-from-nis-to-identity-management[:!context:]
