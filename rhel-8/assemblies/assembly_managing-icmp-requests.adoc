:_mod-docs-content-type: ASSEMBLY
// included in assembly_using-and-configuring-firewalld.adoc

:parent-context-of-managing-icmp-requests: {context}

[id="managing-icmp-requests_{context}"]
= Managing ICMP requests

:context: managing-icmp-requests

[role="_abstract"]
The `Internet Control Message Protocol` (`ICMP`) is a supporting protocol that is used by various network devices for testing, troubleshooting, and diagnostics. `ICMP` differs from transport protocols such as TCP and UDP because it is not used to exchange data between systems.

You can use the [systemitem]`ICMP` messages, especially [systemitem]`echo-request` and [systemitem]`echo-reply`, to reveal information about a network and misuse such information for various kinds of fraudulent activities. Therefore, [systemitem]`firewalld` enables controlling the [systemitem]`ICMP` requests to protect your network information.


include::modules/packet-filtering/proc_configuring-icmp-filtering.adoc[leveloffset=+1]


:context: {parent-context-of-managing-icmp-requests}
