ifdef::context[:parent-context-of-assembly_preparing-to-load-a-certificate-into-the-browser: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_preparing-to-load-a-certificate-into-the-browser"]
endif::[]
ifdef::context[]
[id="assembly_preparing-to-load-a-certificate-into-the-browser_{context}"]
endif::[]
= Preparing to load a certificate into the browser

:context: assembly_preparing-to-load-a-certificate-into-the-browser

[role="_abstract"]
Before importing a user certificate into the browser, make sure that the certificate and the corresponding private key are in a `PKCS #12` format. There are two common situations requiring extra preparatory work:

* The certificate is located in an NSS database. For details how to proceed in this situation, see xref:proc_exporting-a-certificate-and-private-key-from-an-nss-database-into-a-pkcs-12-file_{context}[Exporting a certificate and private key from an NSS database into a PKCS #12 file].
* The certificate and the private key are in two separate `PEM` files. For details how to proceed in this situation, see xref:proc_combining-certificate-and-private-key-pem-files-into-a-pkcs-12-file_{context}[Combining certificate and private key PEM files into a PKCS #12 file].

Afterwards, to import both the CA certificate in the `PEM` format and the user certificate in the `PKCS #12` format into the browser, follow the procedures in xref:configuring-browser-for-cert-auth_dc-web-ui-auth[Configuring a browser to enable certificate authentication] and xref:cert-idm-users-auth-procedure_dc-web-ui-auth[Authenticating to the Identity Management Web UI with a Certificate as an Identity Management User].

include::modules/identity-management/proc_exporting-a-certificate-and-private-key-from-an-nss-database-into-a-pkcs-12-file.adoc[leveloffset=+1]

include::modules/identity-management/proc_combining-certificate-and-private-key-pem-files-into-a-pkcs-12-file.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_preparing-to-load-a-certificate-into-the-browser[:context: {parent-context-of-assembly_preparing-to-load-a-certificate-into-the-browser}]
ifndef::parent-context-of-assembly_preparing-to-load-a-certificate-into-the-browser[:!context:]
