:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// managing-role-based-access-controls-in-idm-using-the-cli
// managing-role-based-access-controls-in-using-the-idm-web-ui

ifdef::context[:parent-context-of-role-based-access-control-in-idm-cli: {context}]

[id="role-based-access-control-in-idm-cli_{context}"]
= Role-based access control in IdM

:context: role-based-access-control-in-idm-cli

[role="_abstract"]
Role-based access control (RBAC) in IdM grants a very different kind of authority to users compared to self-service and delegation access controls.

Role-based access control is composed of three parts:

* *Permissions* grant the right to perform a specific task such as adding or deleting users, modifying a group, and enabling read-access.
* *Privileges* combine permissions, for example all the permissions needed to add a new user.
* *Roles* grant a set of privileges to users, user groups, hosts or host groups.

include::modules/identity-management/con_permissions-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/ref_default-managed-permissions.adoc[leveloffset=+1]

include::modules/identity-management/con_privileges-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/con_roles-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/ref_predefined-roles-in-identity-management.adoc[leveloffset=+1]

ifdef::parent-context-of-role-based-access-control-in-idm-cli[:context: {parent-context-of-role-based-access-control-in-idm-cli}]
ifndef::parent-context-of-role-based-access-control-in-idm-cli[:!context:]
