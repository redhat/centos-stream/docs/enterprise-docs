:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

:parent-context-of-collecting-idm-healthcheck-information: {context}

[id='collecting-idm-healthcheck-information_{context}']
= Collecting IdM Healthcheck information

:context: collecting-idm-healthcheck-information

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.

[role="_abstract"]
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

Healthcheck has been designed as a manual command line tool which should help you to identify possible problems in Identity Management (IdM).

You can create a collection of logs based on the Healthcheck output with 30-day rotation.

// Only display the following if this is the RHEL 8 Installation guide
ifeval::[{ProductNumber} == 8]
.Prerequisites

* The Healthcheck tool is only available on RHEL 8.1 or newer
endif::[]

include::modules/identity-management/con_healthcheck-in-idm.adoc[leveloffset=+1]

include::modules/identity-management/con_log-rotation.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-log-rotation-using-the-idm-healthcheck.adoc[leveloffset=+1]

include::modules/identity-management/con_changing-idm-healthcheck-configuration.adoc[leveloffset=+1]

include::modules/identity-management/proc_configuring-healthcheck-to-change-the-output-logs-format.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-collecting-idm-healthcheck-information}
