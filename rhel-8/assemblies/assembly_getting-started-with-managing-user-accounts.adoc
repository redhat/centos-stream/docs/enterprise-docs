:_mod-docs-content-type: ASSEMBLY


ifdef::context[:parent-context-of-assembly_getting-started-with-managing-user-accounts: {context}]



ifndef::context[]
[id="assembly_getting-started-with-managing-user-accounts"]
endif::[]
ifdef::context[]
[id="assembly_getting-started-with-managing-user-accounts_{context}"]
endif::[]
= Getting started with managing user accounts
:context: assembly_getting-started-with-managing-user-accounts

[role="_abstract"]
{RHEL} is a multi-user operating system, which enables multiple users on different computers to access a single system installed on one machine. Every user operates under its own account, and managing user accounts thus represents a core element of {RHEL} system administration.

The following are the different types of user accounts:

* *Normal user accounts:*
+
Normal accounts are created for users of a particular system. Such accounts can be added, removed, and modified during normal system administration.

* *System user accounts:*
+
System user accounts represent a particular applications identifier on a system. Such accounts are generally added or manipulated only at software installation time, and they are not modified later.
+
[WARNING]
====
System accounts are presumed to be available locally on a system. If these accounts are configured and provided remotely, such as in the instance of an LDAP configuration, system breakage and service start failures can occur.
====
+
For system accounts, user IDs below 1000 are reserved. For normal accounts, you can use IDs starting at 1000. To define the min/max IDs for users and groups, system users and system groups, see the `/etc/login.defs` file.

* *Group:*
+
A group is an entity which ties together multiple user accounts for a common purpose, such as granting access to particular files.


include::modules/core-services/proc_managing-accounts-and-groups-using-command-line-tools.adoc[leveloffset=+1]

// already included later in that chapter
// include::modules/cockpit/proc_adding-new-accounts-using-the-web-console.adoc[leveloffset=+1]



ifdef::parent-context-of-assembly_getting-started-with-managing-user-accounts[:context: {parent-context-of-assembly_getting-started-with-managing-user-accounts}]
ifndef::parent-context-of-assembly_getting-started-with-managing-user-accounts[:!context:]
