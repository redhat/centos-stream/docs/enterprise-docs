ifdef::context[:parent-context-of-configuring-maximum-time-for-storage-error-recovery-with-eh_deadline: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-maximum-time-for-storage-error-recovery-with-eh_deadline"]
endif::[]
ifdef::context[]
[id="configuring-maximum-time-for-storage-error-recovery-with-eh_deadline_{context}"]
endif::[]
= Configuring maximum time for storage error recovery with eh_deadline

:context: configuring-maximum-time-for-storage-error-recovery-with-eh_deadline

You can configure the maximum allowed time to recover failed SCSI devices. This configuration guarantees an I/O response time even when storage hardware becomes unresponsive due to a failure.

// == Prerequisites
//
// * A bulleted list of conditions that must be satisfied before the user starts following this assembly.
// * You can also link to other modules or assemblies the user must follow before starting this assembly.
// * Delete the section title and bullets if the assembly has no prerequisites.

include::modules/filesystems-and-storage/con_the-eh_deadline-parameter.adoc[leveloffset=+1]

include::modules/filesystems-and-storage/proc_setting-the-eh_deadline-parameter.adoc[leveloffset=+1]


// [id='related-information-{context}']
// == Related information
//
// * A bulleted list of links to other material closely related to the contents of the concept module.
// * For more details on writing assemblies, see the link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].
// * Use a consistent system for file names, IDs, and titles. For tips, see _Anchor Names and File Names_ in link:https://github.com/redhat-documentation/modular-docs#modular-documentation-reference-guide[Modular Documentation Reference Guide].

ifdef::parent-context-of-configuring-maximum-time-for-storage-error-recovery-with-eh_deadline[:context: {parent-context-of-configuring-maximum-time-for-storage-error-recovery-with-eh_deadline}]
ifndef::parent-context-of-configuring-maximum-time-for-storage-error-recovery-with-eh_deadline[:!context:]
