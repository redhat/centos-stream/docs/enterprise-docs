:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// master.adoc

// This assembly can be included from other assemblies using the following
// include::assemblies/assembly_setting-bios-parameters-for-system-tuning.adoc[leveloffset=+1]

// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-setting-bios-parameters-for-system-tuning: {context}

[id='setting-bios-parameters-for-system-tuning_{context}']
= Setting BIOS parameters for system tuning

:context: setting-bios-parameters-for-system-tuning
:experimental:
// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]

[role="_abstract"]
The BIOS plays a key role in the functioning of the system. By configuring the BIOS parameters correctly you can significantly improve the system performance.  


[NOTE]
====
Every system and BIOS vendor uses different terms and navigation methods. For more information about BIOS settings, see the BIOS documentation or contact the BIOS vendor.
====

include::modules/rt-kernel/proc_configuring-bios-power-management.adoc[leveloffset=+1]

include::modules/rt-kernel/proc_configuring-edac-units.adoc[leveloffset=+1]

include::modules/rt-kernel/proc_configuring-smis.adoc[leveloffset=+1]

:context: {parent-context-of-setting-bios-parameters-for-system-tuning}
