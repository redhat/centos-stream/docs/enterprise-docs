:_mod-docs-content-type: ASSEMBLY
:parent-context-of-assembly-cluster-permissions: {context}

[id='assembly_cluster-permissions-{context}']

= Setting user permissions for a Pacemaker cluster
:context: cluster-permissions

[role="_abstract"]
You can grant permission for specific users other than user `hacluster` to manage a Pacemaker cluster. There are two sets of permissions that you can grant to individual users:

* Permissions that allow individual users to manage the cluster through the Web UI and to run [command]`pcs` commands that connect to  nodes over a network. Commands that connect to nodes over a network include commands to set up a cluster, or to add or remove nodes from a cluster.

* Permissions for local users to allow read-only or read-write access to the cluster configuration. Commands that do not require connecting over a network include commands that edit the cluster configuration, such as those that create resources and configure constraints.

In situations where both sets of permissions have been assigned, the permissions for commands that connect over a network are applied first, and then permissions for editing the cluster configuration on the local node are applied. Most [command]`pcs` commands do not require network access and in those cases the network permissions will not apply.

include::modules/high-availability/proc_setting-cluster-access-over-network.adoc[leveloffset=+1]

include::modules/high-availability/proc_setting-local-cluster-permissions.adoc[leveloffset=+1]

:context: {parent-context-of-assembly-cluster-permissions}


