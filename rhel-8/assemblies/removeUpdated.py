import re
import glob, os

def nahrad():
    files = glob.glob("*updated")
    for i in range(len(files)):
        staryNazev = files[i]
        print(staryNazev)
        novyNazev = staryNazev[:-7]
        os.rename(staryNazev,novyNazev)


if __name__ == "__main__":
    # execute only if run as a script
    nahrad()
