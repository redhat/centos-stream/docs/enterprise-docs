:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-composer-description: {context}]

ifndef::context[]
[id="composer-description"]
endif::[]
ifdef::context[]
[id="composer-description_{context}"]
endif::[]
= RHEL image builder description

:context: composer-description

[role="_abstract"]
To deploy a system, create a system image. To create RHEL system images, use the RHEL image builder tool. You can use RHEL image builder to create customized system images of {ProductShortName}, including system images prepared for deployment on cloud platforms. RHEL image builder automatically handles the setup details for each output type and is therefore easier to use and faster to work with than manual methods of image creation. You can access the RHEL image builder functionalities by using the command line in the [command]`composer-cli` tool, or the graphical user interface in the RHEL web console.

ifeval::[{ProductNumber} == 8]
NOTE: From RHEL 8.3 onward, the `osbuild-composer` back end replaces `lorax-composer`. The new service provides REST APIs for image building. 
endif::[]

include::modules/composer/ref_composer-terminology.adoc[leveloffset=+1]

include::modules/composer/ref_composer-output-formats.adoc[leveloffset=+1]

include::modules/composer/ref_architectures-supported-on-builds.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/articles/7080686[RHEL image builder additional documentation index]

ifdef::parent-context-of-composer-description[:context: {parent-context-of-composer-description}]
ifndef::parent-context-of-composer-description[:!context:]
