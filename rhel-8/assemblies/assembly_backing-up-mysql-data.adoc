:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-backing-up-mysql-data: {context}]

[id="backing-up-mysql-data_{context}"]
= Backing up MySQL data

:context: backing-up-mysql-data

There are two main ways to back up data from a *MySQL* database:

Logical backup:: Logical backup consists of the SQL statements necessary to restore the data. This type of backup exports information and records in plain text files.
+
The main advantage of logical backup over physical backup is portability and flexibility. The data can be restored on other hardware configurations, *MySQL* versions or Database Management System (DBMS), which is not possible with physical backups.
+
Note that logical backup can only be performed if the `mysqld.service` is running. Logical backup does not include log and configuration files.

Physical backup:: Physical backup consists of copies of files and directories that store the content.
+
Physical backup has the following advantages compared to logical backup:
+
* Output is more compact.
* Backup is smaller in size.
* Backup and restore are faster.
* Backup includes log and configuration files.
+
Note that physical backup must be performed when the `mysqld.service` is not running or all tables in the database are locked to prevent changes during the backup.

You can use one of the following *MySQL* backup approaches to back up data from a *MySQL* database:

* Logical backup with `mysqldump`
* File system backup
* Replication as a backup solution

include::modules/core-services/proc_performing-logical-backup-with-mysqldump-mysql.adoc[leveloffset=+1]

include::modules/core-services/proc_performing-file-system-backup-mysql.adoc[leveloffset=+1]

include::modules/core-services/con_introduction-to-replication-as-a-backup-solution-mysql.adoc[leveloffset=+1]


:context: {parent-context-of-backing-up-mysql-data}

ifdef::parent-context-of-backing-up-mysql-data[:context: {parent-context-of-backing-up-mysql-data}]
ifndef::parent-context-of-backing-up-mysql-data[:!context:]
