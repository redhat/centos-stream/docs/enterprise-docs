:_newdoc-version: 2.17.0
:_template-generated: 2024-05-06

ifdef::context[:parent-context-of-adding-the-kickstart-file-to-a-uefi-http-or-pxe-installation-source: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="adding-the-kickstart-file-to-a-uefi-http-or-pxe-installation-source"]
endif::[]
ifdef::context[]
[id="adding-the-kickstart-file-to-a-uefi-http-or-pxe-installation-source_{context}"]
endif::[]
= Adding the Kickstart file to a UEFI HTTP or PXE installation source


:context: adding-the-kickstart-file-to-a-uefi-http-or-pxe-installation-source

After your Kickstart file is ready, you can make it available for the installation on the destination system. 

include::modules/installer/ref_ports-for-network-based-installation.adoc[leveloffset=+1]

include::modules/installer/proc_making-a-kickstart-file-available-on-an-nfs-server.adoc[leveloffset=+1]

include::modules/installer/proc_making-a-kickstart-file-available-on-an-http-or-https-server.adoc[leveloffset=+1]

include::modules/installer/proc_making-a-kickstart-file-available-on-an-ftp-server.adoc[leveloffset=+1]


ifdef::parent-context-of-adding-the-kickstart-file-to-a-uefi-http-or-pxe-installation-source[:context: {parent-context-of-adding-the-kickstart-file-to-a-uefi-http-or-pxe-installation-source}]
ifndef::parent-context-of-adding-the-kickstart-file-to-a-uefi-http-or-pxe-installation-source[:!context:]

