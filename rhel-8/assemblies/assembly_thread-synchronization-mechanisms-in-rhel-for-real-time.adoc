////
Retains the context of the parent assembly if this assembly is nested within another assembly.
For more information about nesting assemblies, see: https://redhat-documentation.github.io/modular-docs/#nesting-assemblies
See also the complementary step on the last line of this file.
////

ifdef::context[:parent-context-of-assembly_thread-synchronization-mechanisms-in-rhel-for-real-time: {context}]

////
Base the file name and the ID on the assembly title. For example:
* file name: assembly-my-user-story.adoc
* ID: [id="assembly-my-user-story_{context}"]
* Title: = My user story

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken. Include {context} in the ID so the assembly can be reused.
////

////
Indicate the module type in one of the following
ways:
Add the prefix assembly- or assembly_ to the file name.
Add the following attribute before the module ID:
////
:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_thread-synchronization-mechanisms-in-rhel-for-real-time"]
endif::[]
ifdef::context[]
[id="assembly_thread-synchronization-mechanisms-in-rhel-for-real-time_{context}"]
endif::[]
= Thread synchronization mechanisms in RHEL for Real Time
////
If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring.
////

:context: assembly_thread-synchronization-mechanisms-in-rhel-for-real-time

[role="_abstract"]
In real-time, when two or more threads need access to a shared resource at the same time, the threads coordinate using the thread synchronization mechanism. Thread synchronization ensures that only one thread uses the shared resource at a time. The three thread synchronization mechanisms used on Linux: Mutexes, Barriers, and Condition variables (`condvars`).

include::modules/rt-kernel/con_mutexes.adoc[leveloffset=+1]

include::modules/rt-kernel/con_barriers.adoc[leveloffset=+1]

include::modules/rt-kernel/con_condition-variables.adoc[leveloffset=+1]

include::modules/rt-kernel/ref_mutex-classes.adoc[leveloffset=+1]

include::modules/rt-kernel/ref_thread-synchronization-functions.adoc[leveloffset=+1]


////
Restore the context to what it was before this assembly.
////
ifdef::parent-context-of-assembly_thread-synchronization-mechanisms-in-rhel-for-real-time[:context: {parent-context-of-assembly_thread-synchronization-mechanisms-in-rhel-for-real-time}]
ifndef::parent-context-of-assembly_thread-synchronization-mechanisms-in-rhel-for-real-time[:!context:]
