:_mod-docs-content-type: ASSEMBLY
:parent-context-of-cross-instrumentation-of-systemtap: {context}

[id="cross-instrumentation-of-systemtap_{context}"]
= Cross-instrumentation of SystemTap

:context: cross-instrumentation-of-systemtap

[role="_abstract"]
Cross-instrumentation of SystemTap is creating SystemTap instrumentation modules from a SystemTap script on one system to be used on another system that does not have SystemTap fully deployed.

include::modules/performance/con_systemtap-cross-instrumentation.adoc[leveloffset=+1]

include::modules/performance/proc_initializing-cross-instrumentation-of-systemtap.adoc[leveloffset=+1]


:context: {parent-context-of-cross-instrumentation-of-systemtap}
