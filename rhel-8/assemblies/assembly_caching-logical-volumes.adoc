:_newdoc-version: 2.16.0
:_template-generated: 2024-03-21

ifdef::context[:parent-context-of-caching-logical-volumes: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="caching-logical-volumes"]
endif::[]
ifdef::context[]
[id="caching-logical-volumes_{context}"]
endif::[]
= Caching logical volumes

:context: caching-logical-volumes

[role="_abstract"]
You can cache logical volumes by using the `dm-cache` or `dm-writecache` targets.

`dm-cache` utilizes faster storage device (SSD) as cache for a slower storage device (HDD). It caches read and write data, optimizing access times for frequently used data. It is beneficial in mixed workload environments where enhancing read and write operations can lead to significant performance improvements. 

`dm-writecache` optimizes write operations by using a faster storage medium (SSD) to temporarily hold write data before it is committed to the primary storage device (HDD). It is beneficial for write-intensive applications where write performance can slow down the data transfer process.

// == Caching LVs (with dm-cache) (module?)
include::modules/filesystems-and-storage/proc_caching-logical-volumes-with-dm-cache.adoc[leveloffset=+1]

// == Caching LVs (with dm-writecache) (module?)
include::modules/filesystems-and-storage/proc_caching-logical-volumes-with-dm-writecache.adoc[leveloffset=+1]

// == Uncaching LVs (module)
include::modules/filesystems-and-storage/proc_uncaching-lv.adoc[leveloffset=+1]



ifdef::parent-context-of-caching-logical-volumes[:context: {parent-context-of-caching-logical-volumes}]
ifndef::parent-context-of-caching-logical-volumes[:!context:]

