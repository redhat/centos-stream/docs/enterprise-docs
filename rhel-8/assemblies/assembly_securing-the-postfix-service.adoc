ifdef::context[:parent-context-of-assembly_securing-the-postfix-service: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_securing-the-postfix-service"]
endif::[]
ifdef::context[]
[id="assembly_securing-the-postfix-service_{context}"]
endif::[]
= Securing the Postfix service

:context: assembly_securing-the-postfix-service

Postfix is a mail transfer agent (MTA) that uses the Simple Mail Transfer Protocol (SMTP) to deliver electronic messages between other MTAs and to email clients or delivery agents. Although MTAs can encrypt traffic between one another, they might not do so by default. You can also mitigate risks to various attacks by changing setting to more secure values.

include::modules/security/ref_reducing-postfix-network-related-security-risks.adoc[leveloffset=+1]

include::modules/security/ref_postfix-configuration-options-for-limiting-dos-attacks.adoc[leveloffset=+1]

include::modules/security/proc_configuring-postfix-to-use-sasl.adoc[leveloffset=+1]


ifdef::parent-context-of-assembly_securing-the-postfix-service[:context: {parent-context-of-assembly_securing-the-postfix-service}]
ifndef::parent-context-of-assembly_securing-the-postfix-service[:!context:]

