:_mod-docs-content-type: ASSEMBLY

ifdef::context[:parent-context-of-${module_id}: {context}]

ifndef::context[]
[id="creating-and-uploading-customized-rhel-system-image-to-azure-using-image-builder"]
endif::[]
ifdef::context[]
[id="creating-and-uploading-customized-rhel-system-image-to-azure-using-image-builder_{context}"]
endif::[]

//[id="creating-and-uploading-customized-rhel-system-image-to-azure-using-image-builder_{context}"]
= Creating RHEL system image and uploading to Microsoft Azure by using Insights image builder

:context: creating-and-uploading-customized-rhel-system-image-to-azure-using-image-builder

[role="_abstract"]
You can create customized RHEL system images by using Insights image builder, and upload those images to the *Microsoft Azure* cloud target environment. Then, you can create a Virtual Machine (VM) from the image you shared with the Microsoft Azure Cloud account.

=====
WARNING: Red Hat Hybrid Cloud Console does not support uploading the images that you created for the Microsoft Azure target environment to GovCloud regions.
=====

include::modules/image-builder-cloud/proc_authorizing-image-builder-to-push-images-to-microsoft-azure-cloud.adoc[leveloffset=+1]

include::modules/image-builder-cloud/proc_creating-a-customized-rhel-system-image-for-microsoft-azure-using-image-builder.adoc[leveloffset=+1]

include::modules/image-builder-cloud/proc_accessing-your-customized-rhel-system-image-from-your-microsoft-azure-account.adoc[leveloffset=+1]

include::modules/image-builder-cloud/proc_creating-a-virtual-machine-from-the-customized-rhel-system-image-you-uploaded-to-microsoft-azure-account.adoc[leveloffset=+1]

ifdef::parent-context-of-creating-and-uploading-customized-rhel-system-image-to-azure-using-image-builder[:context: {parent-context-of-creating-and-uploading-customized-rhel-system-image-to-azure-using-image-builder}]
ifndef::parent-context-of-creating-and-uploading-customized-rhel-system-image-to-azure-using-image-builder[:!context:]
