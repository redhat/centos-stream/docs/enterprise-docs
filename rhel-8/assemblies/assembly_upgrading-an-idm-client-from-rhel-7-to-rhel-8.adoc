:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-upgrading-an-idm-client-from-rhel-7-to-rhel-8: {context}]


[id="upgrading-an-idm-client-from-rhel-7-to-rhel-8_{context}"]
= Upgrading an IdM client from RHEL 7 to RHEL 8

:context: upgrading-an-idm-client-from-rhel-7-to-rhel-8

[role="_abstract"]
Unlike IdM servers, performing an in-place upgrade of an IdM client from RHEL 7 to RHEL 8 is supported.

In RHEL 8, some uncommon options and unused functionality have been removed from the System Security Services Daemon (SSSD), the service responsible for authentication in an IdM environment. See the following sections for steps to remove those options.

* xref:updating-the-sssd-configuration-after-upgrading-to-rhel-8_upgrading-an-idm-client-from-rhel-7-to-rhel-8[Updating the SSSD configuration after upgrading to RHEL 8]
* xref:list-of-sssd-functionality-removed-in-rhel-8_upgrading-an-idm-client-from-rhel-7-to-rhel-8[List of SSSD functionality removed in RHEL 8]

include::modules/identity-management/proc_updating-the-sssd-configuration-after-upgrading-to-rhel-8.adoc[leveloffset=+1]

include::modules/identity-management/ref_list-of-sssd-functionality-removed-in-rhel-8.adoc[leveloffset=+1]



// Include 2 modules after this to discuss the changes between RHEL 7 authconfig and RHEL 8 authselect:
//    RHELPLAN-11254 - In-place upgrade of the IdM client: authconfig -> authselect"
//    https://issues.redhat.com/browse/RHELPLAN-11254

[role="_additional-resources"]
== Additional resources
* For more details on upgrading to RHEL 8, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/upgrading_from_rhel_7_to_rhel_8/index[Upgrading from RHEL 7 to RHEL 8].

ifdef::parent-context-of-upgrading-an-idm-client-from-rhel-7-to-rhel-8[:context: {parent-context-of-upgrading-an-idm-client-from-rhel-7-to-rhel-8}]
ifndef::parent-context-of-upgrading-an-idm-client-from-rhel-7-to-rhel-8[:!context:]
