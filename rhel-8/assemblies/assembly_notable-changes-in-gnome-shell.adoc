:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:
//
// <List assemblies here, each on a new line>

// This assembly can be included from other assemblies using the following
// include statement:
// include::<path>/assembly_notable-changes-in-gnome-shell.adoc[leveloffset=+1]


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:parent-context-of-notable-changes-in-gnome-shell: {context}

// The file name and the ID are based on the assembly title.
// For example:
// * file name: assembly_my-assembly-a.adoc
// * ID: [id='assembly_my-assembly-a_{context}']
// * Title: = My assembly A
//
// The ID is used as an anchor for linking to the module.
// Avoid changing it after the module has been published
// to ensure existing links are not broken.
//
// In order for  the assembly to be reusable in other assemblies in a guide,
// include {context} in the ID: [id='a-collection-of-modules_{context}'].
//
// If the assembly covers a task, start the title with a verb in the gerund
// form, such as Creating or Configuring.
[id="notable-changes-in-gnome-shell_{context}"]
= Notable changes in GNOME Shell

// The `context` attribute enables module reuse. Every module's ID
// includes {context}, which ensures that the module has a unique ID even if
// it is reused multiple times in a guide.
:context: notable-changes-in-gnome-shell

// The following block is rendered only if the `internal` variable is set.
// The table shows various metadata useful when editing this file.
ifdef::internal[]
[cols="1,4"]
|===
| Included in |
LIST OF ASSEMBLIES
| User story |
USER STORY
| Jira |
JIRA LINK
| BZ |
BUGZILLA LINK
| SMEs |
SME NAMES
| SME Ack |
YES/NO
| Peer Ack |
YES/NO
|===
endif::[]


// RHEL 8 is distributed with GNOME Shell, version 3.28. This version provides multiple enhancements compared to the previous version used with RHEL 7.

RHEL 8 is distributed with GNOME Shell, version 3.28.

This section:

* Highlights enhancements related to GNOME Shell, version 3.28. 
* Informs about the change in default combination of GNOME Shell environment and display protocol. 
* Explains how to access features that are not available by default.
* Explains changes in GNOME tools for software management.

include::modules/desktop/ref_gnome-shell-in-rhel-8.adoc[leveloffset=+1]

include::modules/desktop/ref_gnome-shell-environments.adoc[leveloffset=+1]

include::modules/desktop/ref_desktop-icons.adoc[leveloffset=+1]

include::modules/desktop/ref_fractional-scaling.adoc[leveloffset=+1]

include::modules/desktop/ref_gnome-software-for-package-management.adoc[leveloffset=+1]

include::modules/desktop/ref_opening-graphical-applications-with-sudo.adoc[leveloffset=+1]



// [id='related-information-{context}']
// == Related information


// The following line is necessary to allow assemblies be included in other
// assemblies. It restores the `context` variable to its previous state.
:context: {parent-context-of-notable-changes-in-gnome-shell}

