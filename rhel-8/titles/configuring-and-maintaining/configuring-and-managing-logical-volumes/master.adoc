// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]


// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:lvm-guide:

// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_overview-of-logical-volume-management.adoc[leveloffset=+1]

include::assemblies/assembly_managing-lvm-physical-volumes.adoc[leveloffset=+1]

//some modules can be rewritten
include::assemblies/assembly_managing-lvm-volume-groups.adoc[leveloffset=+1]

// == Primary
include::assemblies/assembly_basic-logical-volume-management.adoc[leveloffset=+1]

// == Secondary
include::assemblies/assembly_advanced-logical-volume-management.adoc[leveloffset=+1]

include::assemblies/assembly_managing-system-upgrades-with-snapshots.adoc[leveloffset=+1]

// needs to be reworked CURRENTLY BEING SPLIT
//include::assemblies/assembly_managing-lvm-logical-volumes.adoc[leveloffset=+1]

//content moved to assemblies/assembly_managing-lvm-logical-volumes.adoc. same needs to be done to rhel 9 master
// include::assemblies/assembly_modifying-logical-volume-size.adoc[leveloffset=+1]

include::assemblies/assembly_customizing-the-lvm-report.adoc[leveloffset=+1]

include::assemblies/assembly_lvm-on-shared-storage.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-raid-logical-volumes.adoc[leveloffset=+1]

// rewritten and included in assemblies/assembly_managing-lvm-logical-volumes.adoc
// include::assemblies/assembly_snapshot-of-logical-volumes.adoc[leveloffset=+1]

// mostly rewritten. need to check if rhel 9 content should be kept
//include::assemblies/assembly_creating-and-managing-thin-provisioned-volumes.adoc[leveloffset=+1]

//commented out for restructuring and rewrite
// include::assemblies/assembly_enabling-caching-to-improve-logical-volume-performance.adoc[leveloffset=+1]

//commented out for restructuring and rewrite
//include::assemblies/assembly_lvm-activation.adoc[leveloffset=+1]

include::assemblies/assembly_limiting-lvm-device-visibility-and-usage.adoc[leveloffset=+1]

include::assemblies/assembly_controlling-lvm-allocation.adoc[leveloffset=+1]

include::assemblies/assembly_grouping-lvm-objects-with-tags.adoc[leveloffset=+1]

include::assemblies/assembly_troubleshooting-lvm.adoc[leveloffset=+1]
