// Show the table of contents
:toc:
// The name of the title
:ProjectName: Recording sessions
// The subtitle of the title
:Subtitle: Using the Session Recording solution in {RHEL8}
// The abstract of the title
:Abstract: This documentation collection provides introduction to using the Session Recording solution based on tlog with RHEL web console embedded player on {RHEL8}.
// The name of the title for the purposes of {context}
:ProjectNameID: getting-started-with-session-recording

// The following are not required
:ProjectVersion: 0.1
