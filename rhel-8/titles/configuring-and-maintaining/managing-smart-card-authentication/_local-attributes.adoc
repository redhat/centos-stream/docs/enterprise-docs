// Show the table of contents
:toc:
// The name of the title
:ProjectName: Managing smart card authentication
// The subtitle of the title
:Subtitle: Configuring and using smart card authentication
// The abstract of the title
:Abstract: With {RH} {IPA} (IdM), you can store credentials in the form of a private key and a certificate on a smart card. You can then use this smart card instead of passwords to authenticate to services. Administrators can configure mapping rules to reduce the administrative overhead.
// The name of the title for the purposes of {context}
:ProjectNameID: managing-smart-card-authentication

// The following are not required
:ProjectVersion: 0.1
