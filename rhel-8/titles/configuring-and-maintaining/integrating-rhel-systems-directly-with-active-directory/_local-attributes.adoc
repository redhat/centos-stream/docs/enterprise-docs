// Show the table of contents
:toc:
// The name of the title
:ProjectName: Integrating RHEL systems directly with Windows Active Directory
// The subtitle of the title
:Subtitle: Joining RHEL hosts to AD and accessing resources in AD
// The abstract of the title
:Abstract: You can join {ProductName} ({ProductShortName}) hosts to an {AD} (AD) domain by using the System Security Services Daemon (SSSD) or the Samba Winbind service to access AD resources. Alternatively, it is also possible to access AD resources without domain integration by using a Managed Service Account (MSA).
// The name of the title for the purposes of {context}
:ProjectNameID: integrating-rhel-systems-directly-with-active-directory

// The following are not required
:ProjectVersion: 0.1
