// Show the table of contents
:toc:
// The name of the title
:ProjectName: Building, running, and managing containers
// The subtitle of the title
:Subtitle: Using Podman, Buildah, and Skopeo on {ProductName}{nbsp}{ProductNumber}
// The abstract of the title
:Abstract: {ProductName}{nbsp}{ProductNumber} provides a number of command-line tools for working with container images. You can manage pods and container images using Podman. To build, update, and manage container images you can use Buildah. To copy and inspect images in remote repositories, you can use Skopeo. 			
// The name of the title for the purposes of {context}
:ProjectNameID: building-running-and-managing-containers

// The following are not required
:ProjectVersion: 0.1
