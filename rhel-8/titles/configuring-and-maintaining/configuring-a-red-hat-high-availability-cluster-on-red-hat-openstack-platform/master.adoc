// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:ha-cluster-openstack:

// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

[id="preface_{context}"]
== Preface

You can use the Red Hat High Availability Add-On to configure a Red Hat High Availability (HA) cluster
on Red Hat OpenStack Platform (RHOSP) instances. This requires that you install the required packages and agents, configure a basic cluster, configure fencing
resources, and configure HA cluster resources.

For RHOSP documentation, see link:https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/16.2[Product Documentation for Red Hat Openstack Platform].

For Red Hat’s policies, requirements, and limitations applicable to the use of RHOSP instances in a RHEL High Availability cluster, see link:https://access.redhat.com/articles/3131311[Support Policies for RHEL High Availability Clusters - OpenStack Virtual Machines as Cluster Members - Red Hat Customer Portal].


// RHEL 9 assembly
// include::assemblies/assembly_mixing-unrelated-items.adoc[leveloffset=+1]

// RHEL 9 module
// include::modules/_examples/con_the-metamorphosis.adoc[leveloffset=+1]

// RHEL 8 module
// include::rhel-8/modules/security/con_system-wide-crypto-policies.adoc[leveloffset=+1]

include::modules/high-availability/ref_recommended-rhosp-server-group-configuration.adoc[leveloffset=+1]

include::modules/high-availability/proc_installing-the-high-availability-and-rhosp-packages-and-agents.adoc[leveloffset=+1]

include::modules/high-availability/ref_authentication-methods-for-rhosp.adoc[leveloffset=+1]

include::modules/high-availability/proc_creating-a-basic-cluster-on-red-hat-openstack-platform.adoc[leveloffset=+1]

include::modules/high-availability/proc_configuring-fencing-for-an-ha-cluster-on-red-hat-openstack-platform.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-ha-cluster-resources-on-red-hat-openstack-platform.adoc[leveloffset=+1]



// RHEL 8 assembly
// include::rhel-8/assemblies/assembly_protecting-systems-against-intrusive-usb-devices.adoc[leveloffset=+1]
