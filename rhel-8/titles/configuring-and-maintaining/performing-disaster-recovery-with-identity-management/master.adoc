// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal debug information in all included assemblies
//:internal:
// include::common-content/making-open-source-more-inclusive-idm.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::modules/identity-management/con_disaster-scenarios-in-idm.adoc[leveloffset=+1]

include::assemblies/assembly_recovering-a-single-server-with-replication.adoc[leveloffset=+1]

include::assemblies/assembly_recovering-multiple-servers-with-replication.adoc[leveloffset=+1]

include::assemblies/assembly_recovering-from-data-loss-with-snapshots.adoc[leveloffset=+1]

include::assemblies/assembly_recovering-from-data-loss-with-backups.adoc[leveloffset=+1]

include::assemblies/assembly_restoring-idm-servers-using-ansible-playbooks.adoc[leveloffset=+1]

include::assemblies/assembly_managing-data-loss.adoc[leveloffset=+1]

include::modules/identity-management/proc_adjusting-idm-clients-during-recovery.adoc[leveloffset=+1]
