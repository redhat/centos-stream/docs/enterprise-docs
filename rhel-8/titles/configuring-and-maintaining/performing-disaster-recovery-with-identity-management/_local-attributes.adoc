// Show the table of contents
:toc:
// The name of the title
:ProjectName: Performing disaster recovery with Identity{nbsp}Management
// The subtitle of the title
:Subtitle: Recovering IdM after a server or data loss
// The abstract of the title
//:Abstract: The abstract is directly in docinfo.xml, because within an attribute, multiple paragraphs are not supported.
// The name of the title for the purposes of {context}
:ProjectNameID: performing-disaster-recovery

// The following are not required
:ProjectVersion: 0.1
