// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

//This is how the offset used to be set -- here but not in the include statemenst
// :leveloffset: +1

// include::common-content/making-open-source-more-inclusive.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_planning-gfs2-deployment.adoc[leveloffset=+1]

include::assemblies/assembly_gfs2-usage-considerations.adoc[leveloffset=+1]

include::assemblies/assembly_creating-mounting-gfs2.adoc[leveloffset=+1]

include::assemblies/assembly_gfs2-disk-quota-administration.adoc[leveloffset=+1]

include::assemblies/assembly_gfs2-filesystem-repair.adoc[leveloffset=+1]

include::assemblies/assembly_gfs2-performance.adoc[leveloffset=+1]

include::assemblies/assembly_troubleshooting-gfs2.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-gfs2-in-a-cluster.adoc[leveloffset=+1]

include::modules/high-availability/con_gfs2-tracepoints.adoc[leveloffset=+1]

include::assemblies/assembly_analyzing-gfs2-with-pcp.adoc[leveloffset=+1]


