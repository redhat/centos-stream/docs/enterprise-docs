// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:kernel-title:

// include::common-content/making-open-source-more-inclusive.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_the-linux-kernel.adoc[leveloffset=+1]

// include::assemblies/assembly_the-linux-kernel-rpm.adoc[leveloffset=+1]

// include::assemblies/assembly_updating-kernel-with-yum.adoc[leveloffset=+1]

include::assemblies/assembly_managing-kernel-modules.adoc[leveloffset=+1]

include::assemblies/assembly_signing-a-kernel-and-modules-for-secure-boot.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-kernel-command-line-parameters.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-kernel-parameters-at-runtime.adoc[leveloffset=+1]

include::assemblies/assembly_making-temporary-changes-to-the-grub-menu.adoc[leveloffset=+1]

include::assemblies/assembly_making-persistent-changes-to-the-grub-boot-loader.adoc[leveloffset=+1]

include::assemblies/assembly_building-a-customized-boot-menu.adoc[leveloffset=+1]

include::assemblies/assembly_reinstalling-grub.adoc[leveloffset=+1]

include::assemblies/assembly_protecting-grub-with-a-password.adoc[leveloffset=+1]

include::assemblies/assembly_keeping-kernel-panic-parameters-disabled-in-virtualized-environments.adoc[leveloffset=+1]

include::assemblies/assembly_adjusting-kernel-parameters-for-database-servers.adoc[leveloffset=+1]

include::assemblies/assembly_getting-started-with-kernel-logging.adoc[leveloffset=+1]

include::assemblies/assembly_installing-kdump.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-kdump-on-the-command-line.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-kdump-in-the-web-console.adoc[leveloffset=+1]

include::assemblies/assembly_enabling-kdump.adoc[leveloffset=+1]

include::assemblies/assembly_supported-kdump-configurations-and-targets.adoc[leveloffset=+1]

include::assemblies/assembly_firmware-assisted-dump-mechanisms.adoc[leveloffset=+1]

include::assemblies/assembly_analyzing-a-core-dump.adoc[leveloffset=+1]

include::assemblies/assembly_using-early-kdump-to-capture-boot-time-crashes.adoc[leveloffset=+1]

include::assemblies/assembly_applying-patches-with-kernel-live-patching.adoc[leveloffset=+1]

include::assemblies/assembly_setting-limits-for-applications.adoc[leveloffset=+1]

include::assemblies/assembly_using-cgroups-v2-to-control-distribution-of-cpu-time-for-applications.adoc[leveloffset=+1]

include::assemblies/assembly_using-control-groups-version-1-with-systemd.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-resource-management-using-systemd.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-cpu-affinity-and-numa-policies-using-systemd.adoc[leveloffset=+1]

include::assemblies/assembly_analyzing-system-performance-with-bpf-compiler-collection.adoc[leveloffset=+1]

include::assemblies/assembly_enhancing-security-with-the-kernel-integrity-subsystem.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-kernel-parameters-permanently-by-using-the-kernel-settings-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-the-grub-2-boot-loader-by-using-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_using-advanced-error-reporting.adoc[leveloffset=+1]

// Commenting out Ansible EE chapter based on confirmation with pkettmann (engineering PO)
// include::assemblies/assembly_using-ansible-automation-platform-and-ansible-execution-environments-to-automate-workload.adoc[leveloffset=+1]

// include::assemblies/assembly_installing-and-configuring-kdump.adoc[leveloffset=+1]
