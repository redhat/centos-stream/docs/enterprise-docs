// Show the table of contents
:toc:
// The name of the title
:ProjectName: Using the desktop environment in RHEL 8
// The subtitle of the title
:Subtitle: Configuring and customizing the GNOME 3 desktop environment on RHEL 8
// The abstract of the title
:Abstract: This document describes how to customize and use GNOME 3, which is the only desktop environment available in RHEL 8. The basics of using GNOME Shell and displaying the graphics are given, as well as the instructions for system administrators for configuring GNOME on a low level and customizing the desktop environment for multiple users. The documentation also describes how to handle selected system administration tasks using the desktop environment.

// The name of the title for the purposes of {context}
:ProjectNameID: using-the-desktop-environment-in-rhel-8

// The following are not required
:ProjectVersion: 0.1
