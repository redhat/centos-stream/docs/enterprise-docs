include::common-content/_attributes.adoc[]

include::_local-attributes.adoc[]

= {ProjectName}

:context: {ProjectNameID}
:managing-users-groups-hosts:

// include::common-content/making-open-source-more-inclusive-idm.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_introduction-to-the-ipa-command-line-utilities.adoc[leveloffset=+1]

// managing user accounts
include::assemblies/assembly_managing-user-accounts-using-the-command-line.adoc[leveloffset=+1]

include::assemblies/assembly_managing-user-accounts-using-the-idm-web-ui.adoc[leveloffset=+1]

include::assemblies/assembly_managing-user-accounts-using-Ansible-playbooks.adoc[leveloffset=+1]

include::assemblies/assembly_modifying-user-and-group-attributes-in-idm.adoc[leveloffset=+1]

include::assemblies/assembly_managing-user-passwords-in-idm.adoc[leveloffset=+1]

include::assemblies/assembly_defining-idm-password-policies.adoc[leveloffset=+1]

include::assemblies/assembly_managing-expiring-password-notifications.adoc[leveloffset=+1]

include::assemblies/assembly_granting-sudo-access-to-an-IdM-user-on-an-IdM-client.adoc[leveloffset=+1]

include::assemblies/assembly_using-ldapmodify-to-manage-IdM-users-externally.adoc[leveloffset=+1]

include::assemblies/assembly_searching-idm-entries-using-the-ldapsearch-command.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-IdM-to-automatically-activate-stage-user-accounts.adoc[leveloffset=+1]

include::assemblies/assembly_managing-kerberos-principal-aliases-for-users-hosts-and-services.adoc[leveloffset=+1]

include::assemblies/assembly_strengthening-kerberos-security-with-pac-information.adoc[leveloffset=+1]

include::assemblies/assembly_managing-kerberos-ticket-policies.adoc[leveloffset=+1]

include::assemblies/assembly_kerberos-pkinit-authentication-in-idm.adoc[leveloffset=+1]

include::assemblies/assembly_maintaining-idm-kerberos-keytab-files.adoc[leveloffset=+1]

include::assemblies/assembly_using-the-kdc-proxy-in-idm.adoc[leveloffset=+1]


// managing self-service rules
include::assemblies/assembly_managing-self-service-rules-in-idm-using-the-cli.adoc[leveloffset=+1]

include::assemblies/assembly_managing-self-service-rules-using-the-idm-web-ui.adoc[leveloffset=+1]

include::assemblies/assembly_using-ansible-playbooks-to-manage-self-service-rules-in-idm.adoc[leveloffset=+1]

// managing user groups
include::assemblies/assembly_managing-user-groups-in-idm-cli.adoc[leveloffset=+1]

include::assemblies/assembly_managing-user-groups-in-idm-web-ui.adoc[leveloffset=+1]

include::assemblies/assembly_managing-user-groups-using-ansible-playbooks.adoc[leveloffset=+1]

// managing group membership
include::assemblies/assembly_automating-group-membership-using-idm-cli.adoc[leveloffset=+1]

include::assemblies/assembly_automating-group-membership-using-idm-web-ui.adoc[leveloffset=+1]

include::assemblies/assembly_using-ansible-to-automate-group-membership-in-idm.adoc[leveloffset=+1]

// delegating permissions
include::assemblies/assembly_delegating-permissions-to-user-groups-to-manage-users-using-idm-cli.adoc[leveloffset=+1]

include::assemblies/assembly_delegating-permissions-to-user-groups-to-manage-users-using-idm-webui.adoc[leveloffset=+1]

include::assemblies/assembly_delegating-permissions-to-user-groups-to-manage-users-using-ansible-playbooks.adoc[leveloffset=+1]

// managing RBAC rules
include::assemblies/assembly_managing-role-based-access-controls-in-idm-using-the-cli.adoc[leveloffset=+1]

include::assemblies/assembly_managing-role-based-access-controls-using-the-idm-web-ui.adoc[leveloffset=+1]

include::modules/identity-management/proc_preparing-your-environment-for-managing-idm-using-ansible-playbooks.adoc[leveloffset=+1]

include::assemblies/assembly_using-ansible-playbooks-to-manage-role-based-access-control-in-idm.adoc[leveloffset=+1]

include::assemblies/assembly_using-ansible-playbooks-to-manage-rbac-privileges-in-idm.adoc[leveloffset=+1]

include::assemblies/assembly_using-ansible-playbooks-to-manage-rbac-permissions-in-idm.adoc[leveloffset=+1]

// ID views
include::assemblies/assembly_using-an-id-view-to-override-a-user-attribute-value-on-an-IdM-client.adoc[leveloffset=+1]

include::assemblies/assembly_using-id-views-for-active-directory-users.adoc[leveloffset=+1]

// ID Ranges
include::assemblies/assembly_adjusting-id-ranges-manually.adoc[leveloffset=+1]

include::assemblies/assembly_managing-subid-ranges-manually.adoc[leveloffset=+1]

// managing hosts
include::assemblies/assembly_managing-hosts-idm-cli.adoc[leveloffset=+1]

include::assemblies/assembly_managing-hosts-idm-webui.adoc[leveloffset=+1]

include::assemblies/assembly_managing-hosts-using-Ansible-playbooks.adoc[leveloffset=+1]

// managing host groups
include::assemblies/assembly_managing-host-groups-using-the-idm-cli.adoc[leveloffset=+1]

include::assemblies/assembly_managing-host-groups-using-the-idm-web-ui.adoc[leveloffset=+1]

include::assemblies/assembly_managing-host-groups-using-Ansible-playbooks.adoc[leveloffset=+1]

// managing HBAC rules

include::assemblies/assembly_configuring-host-based-access-control-rules.adoc[leveloffset=+1]

include::assemblies/assembly_ensuring-the-presence-of-host-based-access-control-rules-in-idm-using-Ansible-playbooks.adoc[leveloffset=+1]


// managing SSH public keys

include::assemblies/assembly_managing-public-ssh-keys.adoc[leveloffset=+1]

// AD-related docs
include::assemblies/assembly_configuring-the-domain-resolution-order-to-resolve-short-ad-user-names.adoc[leveloffset=+1]

include::assemblies/assembly_enabling-authentication-using-AD-User-Principal-Names-in-IdM.adoc[leveloffset=+1]

include::assemblies/assembly_enabling-ad-users-to-administer-idm.adoc[leveloffset=+1]

// External Identity Providers
include::assemblies/assembly_using-external-identity-providers-to-authenticate-to-idm.adoc[leveloffset=+1]
