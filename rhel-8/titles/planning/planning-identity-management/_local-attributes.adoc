// Show the table of contents
:toc:
// The name of the title
:ProjectName: Planning Identity{nbsp}Management
// The subtitle of the title
:Subtitle: Planning the infrastructure and service integration of an IdM environment
// The abstract of the title
//:Abstract: The abstract is directly in docinfo.xml, because within an attribute, multiple paragraphs are not supported.
// The name of the title for the purposes of {context}
:ProjectNameID: planning-identity-management

// The following are not required
:ProjectVersion: 0.1
