// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal debug information in all included assemblies
//:internal:
// include::common-content/making-open-source-more-inclusive.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

// A title shouldn't have just one assembly, but this is meant to prepare
// for future work documenting the `sos collect`, `sos clean` and other
// uses of the `sos` utility.
// Temporarily listing modules here would change their context and affect
// xrefs and just make everything messier and generate more work later.
include::assemblies/assembly_generating-an-sos-report-for-technical-support.adoc[leveloffset=+1]

include::assemblies/assembly_generating-and-maintaining-the-diagnostic-reports-using-the-rhel-web-console.adoc[leveloffset=+1]
