// Include shared, global attributes
//include::attributes-system-roles.adoc[]
//include::common-content/attributes-system-roles.adoc[]
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

// define a unique token to check against, so as to let content verify if it's in this book or not
:system-roles-ansible-7.9:

// include::common-content/making-open-source-more-inclusive.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::modules/con_intro-to-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_installing-and-using-collections.adoc[leveloffset=+1]

include::assemblies/assembly_ansible-ipmi-modules-in-rhel.adoc[leveloffset=+1]

include::assemblies/assembly_the-redfish-modules-in-rhel.adoc[leveloffset=+1]

//include::assemblies/assembly_running-rhel-system-roles-using-ansible-execution-environment.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-kernel-parameters-permanently-by-using-the-kernel-settings-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_using-the-rhc-system-role-to-register-the-system.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-network-settings-by-using-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-firewalld-using-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_postfix-role-variables-in-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-selinux-using-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-logging-by-using-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-the-systemd-journal-by-using-the-journald-RHEL-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-secure-communication-by-using-the-ssh-and-sshd-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-vpn-connections-with-ipsec-by-using-the-rhel-vpn-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_setting-a-custom-cryptographic-policy-by-using-the-crypto-policies-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-nbde-by-using-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_requesting-certificates-using-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-automatic-crash-dumps-by-using-the-kdump-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_managing-local-storage-using-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-time-synchronization-by-using-the-timesync-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_monitoring-performance-by-using-the-metrics-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-microsoft-sql-server-using-microsoft-sql-server-ansible-role.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-a-system-for-session-recording-using-the-tlog-rhel-system-roles.adoc[leveloffset=+1]

include::assemblies/assembly_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_installing-and-configuring-web-console-with-the-cockpit-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_managing-containers-by-using-the-podman-rhel-system-role.adoc[leveloffset=+1]

include::assemblies/assembly_integrating-rhel-systems-directly-with-ad-using-rhel-system-roles.adoc[leveloffset=+1]
