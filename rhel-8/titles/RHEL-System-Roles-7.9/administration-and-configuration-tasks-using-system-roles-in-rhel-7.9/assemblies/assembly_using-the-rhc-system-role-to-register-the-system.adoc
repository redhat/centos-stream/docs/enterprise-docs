
ifdef::context[:parent-context-of-using-the-rhc-system-role-to-register-the-system: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="using-the-rhc-system-role-to-register-the-system"]
endif::[]
ifdef::context[]
[id="using-the-rhc-system-role-to-register-the-system_{context}"]
endif::[]
= Using the rhc system role to register the system

:context: using-the-rhc-system-role-to-register-the-system

The `rhc` {RHELSystemRoles} enables administrators to automate the registration of multiple systems with Red Hat Subscription Management (RHSM) and Satellite servers. The role also supports Insights-related configuration and management tasks by using Ansible.

include::modules/con_introduction-to-the-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_registering-system-using-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_registering-system-with-satellite-using-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_configuring-insights-connection-using-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_configuring-repositories-using-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_setting-release-versions-using-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_configuring-proxy-using-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_configuring-auto-updates-insights-using-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_configuring-insights-remediations-using-rhel-system-role.adoc[leveloffset=+1]

include::modules/proc_configuring-insights-tags-using-rhc-system-role.adoc[leveloffset=+1]

include::modules/proc_unregistering-a-system-using-the-rhc-system-role.adoc[leveloffset=+1]

ifdef::parent-context-of-using-the-rhc-system-role-to-register-the-system[:context: {parent-context-of-using-the-rhc-system-role-to-register-the-system}]
ifndef::parent-context-of-using-the-rhc-system-role-to-register-the-system[:!context:]
