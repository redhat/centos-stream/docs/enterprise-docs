ifdef::context[:parent-context-of-configuring-secure-communication-by-using-the-ssh-and-sshd-rhel-system-roles: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-secure-communication-by-using-the-ssh-and-sshd-rhel-system-roles"]
endif::[]
ifdef::context[]
[id="configuring-secure-communication-by-using-the-ssh-and-sshd-rhel-system-roles_{context}"]
endif::[]
= Configuring secure communication by using the `ssh` and `sshd` RHEL system roles

:context: configuring-secure-communication-by-using-the-ssh-and-sshd-rhel-system-roles

[role="_abstract"]
As an administrator, you can use the `sshd` system role to configure SSH servers and the `ssh` system role to configure SSH clients consistently on any number of RHEL systems at the same time by using Red Hat Ansible Automation Platform.

include::modules/ref_sshd-system-role-variables.adoc[leveloffset=+1]

include::modules/proc_configuring-openssh-servers-using-the-sshd-system-role.adoc[leveloffset=+1]

include::modules/ref_ssh-system-role-variables.adoc[leveloffset=+1]

include::modules/proc_configuring-openssh-clients-using-the-ssh-system-role.adoc[leveloffset=+1]

include::modules/proc_using-the-ssh-server-system-role-for-non-exclusive-configuration.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-secure-communication-by-using-the-ssh-and-sshd-rhel-system-roles[:context: {parent-context-of-configuring-secure-communication-by-using-the-ssh-and-sshd-rhel-system-roles}]
ifndef::parent-context-of-configuring-secure-communication-by-using-the-ssh-and-sshd-rhel-system-roles[:!context:]
