:_mod-docs-content-type: ASSEMBLY
// This assembly is included in the following assemblies:

ifdef::context[:parent-context-of-configuring-selinux-using-system-roles: {context}]

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
// If the assembly is reused in other assemblies in a guide, include {context} in the ID: [id="a-collection-of-modules-{context}"].
[id="configuring-selinux-using-system-roles_{context}"]
= Configuring SELinux using system roles

:context: configuring-selinux-using-system-roles

//include::modules/core-services/con_intro-to-rhel-system-roles.adoc[leveloffset=+1]
include::modules/con_introduction-to-the-selinux-system-role.adoc[leveloffset=+1]

include::modules/proc_using-the-selinux-system-role-to-apply-selinux-settings-on-multiple-systems.adoc[leveloffset=+1]


// Restore the context to what it was before this assembly.
ifdef::parent-context-of-configuring-selinux-using-system-roles[:context: {parent-context-of-configuring-selinux-using-system-roles}]
ifndef::parent-context-of-configuring-selinux-using-system-roles[:!context:]
