ifdef::context[:parent-context-of-configuring-automatic-crash-dumps-by-using-the-kdump-rhel-system-role: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-automatic-crash-dumps-by-using-the-kdump-rhel-system-role"]
endif::[]
ifdef::context[]
[id="configuring-automatic-crash-dumps-by-using-the-kdump-rhel-system-role_{context}"]
endif::[]
= Configuring automatic crash dumps by using the `kdump` RHEL system role

:context: configuring-automatic-crash-dumps-by-using-the-kdump-rhel-system-role

[role="_abstract"]
To manage kdump using Ansible, you can use the `kdump` role, which is one of the {RHELSystemRoles}s available in RHEL 7.9.

Using the `kdump` role enables you to specify where to save the contents of the system’s memory for later analysis.

// ADD A LINK TO UPDATED SECTION ONCE READY: For more information about {RHELSystemRoles}s and how to apply them, see link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/configuring_basic_system_settings/getting-started-with-rhel-system-roles_configuring-basic-system-settings[Introduction to {RHELSystemRoles}s].


include::modules/con_introduction-to-the-kdump-role.adoc[leveloffset=+1]

include::modules/ref_kdump-role-parameters.adoc[leveloffset=+1]

include::modules/proc_configuring-kdump-using-rhel-system-roles.adoc[leveloffset=+1]

ifdef::parent-context-of-configuring-automatic-crash-dumps-by-using-the-kdump-rhel-system-role[:context: {parent-context-of-configuring-automatic-crash-dumps-by-using-the-kdump-rhel-system-role}]
ifndef::parent-context-of-configuring-automatic-crash-dumps-by-using-the-kdump-rhel-system-role[:!context:]
