:_mod-docs-content-type: ASSEMBLY
ifdef::context[:parent-context-of-assembly_postfix-role-variables-in-system-roles: {context}]

ifndef::context[]
[id="assembly_postfix-role-variables-in-system-roles"]
endif::[]
ifdef::context[]
[id="assembly_postfix-role-variables-in-system-roles_{context}"]
endif::[]
= Variables of the `postfix` role in system roles

:context: assembly_postfix-role-variables-in-system-roles

[role="_abstract"]
The `postfix` role variables allow the user to install, configure, and start the `postfix` Mail Transfer Agent (MTA).

The following role variables are defined in this section:

* `postfix_conf`: It includes key/value pairs of all the supported `postfix` configuration parameters. By default, the `postfix_conf` does not have a value.

----
postfix_conf:
  relayhost: example.com
----

If your scenario requires removing any existing configuration and apply the desired configuration on top of a clean `postfix` installation, specify the `previous: replaced` option within the `postfix_conf` dictionary:

An example with the `previous: replaced` option:

----
postfix_conf:
  relayhost: example.com
  previous: replaced
----

* `postfix_check`: It determines if a check has been executed before starting the `postfix` to verify the configuration changes. The default value is true.

For example:

----
postfix_check: true
----

* `postfix_backup`: It determines whether a single backup copy of the configuration is created. By default the `postfix_backup` value is false.

To overwrite any previous backup run the following command:

----
# *cp /etc/postfix/main.cf /etc/postfix/main.cf.backup*
----

If the `postfix_backup` value is changed to `true`, you must also set the `postfix_backup_multiple` value to false.

For example:

----
postfix_backup: true
postfix_backup_multiple: false
----

* `postfix_backup_multiple`: It determines if the role will make a timestamped backup copy of the configuration.

To keep multiple backup copies, run the following command:

----
# *cp /etc/postfix/main.cf /etc/postfix/main.cf.$(date -Isec)*
----

By default the value of `postfix_backup_multiple` is true.
The `postfix_backup_multiple:true` setting overrides `postfix_backup`. If you want to use `postfix_backup` you must set the `postfix_backup_multiple:false`.

* `postfix_manage_firewall`: Integrates the `postfix` role with the `firewall` role to manage port access. By default, the variable is set to `false`. If you want to automatically manage port access from the `postfix` role, set the variable to `true`.

* `postfix_manage_selinux`: Integrates the `postfix` role with the `selinux` role to manage port access. By default, the variable is set to `false`. If you want to automatically manage port access from the `postfix` role, set the variable to `true`.

IMPORTANT: The configuration parameters cannot be removed. Before running the `postfix` role, set the `postfix_conf` to all the required configuration parameters and use the file module to remove `/etc/postfix/main.cf`.

[role="_additional-resources"]
== Additional resources
* `/usr/share/doc/rhel-system-roles/postfix/README.md`

ifdef::parent-context-of-assembly_postfix-role-variables-in-system-roles[:context: {parent-context-of-assembly_postfix-role-variables-in-system-roles}]
ifndef::parent-context-of-assembly_postfix-role-variables-in-system-roles[:!context:]
