ifdef::context[:parent-context-of-assembly_configuring-firewalld-using-system-roles: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="assembly_configuring-firewalld-using-system-roles"]
endif::[]
ifdef::context[]
[id="assembly_configuring-firewalld-using-system-roles_{context}"]
endif::[]
= Configuring `firewalld` using system roles

:context: assembly_configuring-firewalld-using-system-roles

You can use the `firewall` system role to configure settings of the `firewalld` service on multiple clients at once. This solution:

* Provides an interface with efficient input settings.
* Keeps all intended `firewalld` parameters in one place.

After you run the `firewall` role on the control node, the system role applies the `firewalld` parameters to the managed node immediately and makes them persistent across reboots.


include::modules/con_introduction-to-the-firewall-rhel-system-role.adoc[leveloffset=+1]

include::modules/proc_resetting-the-firewalld-settings-using-the-firewall-rhel-system-role.adoc[leveloffset=+1]

include::modules/proc_forwarding-incoming-traffic-from-one-local-port-to-a-different-local-port.adoc[leveloffset=+1]

include::modules/proc_configuring-ports-using-system-roles.adoc[leveloffset=+1]

include::modules/proc_configuring-a-dmz-firewalld-zone-by-using-the-firewalld-rhel-system-role.adoc[leveloffset=+1]




ifdef::parent-context-of-assembly_configuring-firewalld-using-system-roles[:context: {parent-context-of-assembly_configuring-firewalld-using-system-roles}]
ifndef::parent-context-of-assembly_configuring-firewalld-using-system-roles[:!context:]
