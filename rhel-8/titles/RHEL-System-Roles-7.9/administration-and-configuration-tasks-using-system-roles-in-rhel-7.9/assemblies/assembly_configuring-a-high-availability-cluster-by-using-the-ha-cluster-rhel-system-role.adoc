ifdef::context[:parent-context-of-configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role: {context}]

:_mod-docs-content-type: ASSEMBLY

ifndef::context[]
[id="configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role"]
endif::[]
ifdef::context[]
[id="configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role_{context}"]
endif::[]
= Configuring a high-availability cluster by using the ha_cluster RHEL system role

:context: configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role

With the `ha_cluster` system role, you can configure and manage a high-availability cluster that uses the Pacemaker high availability cluster resource manager.


include::modules/ref_ha-role-parameters.adoc[leveloffset=+1]

include::modules/ref_ha-role-inventory.adoc[leveloffset=+1]

include::modules/proc_configuring-TLScert-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/proc_configuring-no-resource-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/proc_configuring-with-resource-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/proc_configuring-with-constraints-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/proc_configuring-corosync-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/proc_configuring-with-sbd-hacluster-using-system-roles.adoc[leveloffset=+1]

include::modules/proc_configuring-quorum-device-using-system-roles.adoc[leveloffset=+1]

include::modules/proc_configuring-http-ha-using-system-roles.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/automating_system_administration_by_using_rhel_system_roles/index#assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[Preparing a control node and managed nodes to use RHEL system roles].
* Documentation installed with the `rhel-system-roles` package in `/usr/share/ansible/roles/rhel-system-roles.logging/README.html`
* link:https://access.redhat.com/node/3050101[{RHELSystemRoles}s] KB article
* `ansible-playbook(1)` man page on your system

ifdef::parent-context-of-configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role[:context: {parent-context-of-configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role}]
ifndef::parent-context-of-configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role[:!context:]
