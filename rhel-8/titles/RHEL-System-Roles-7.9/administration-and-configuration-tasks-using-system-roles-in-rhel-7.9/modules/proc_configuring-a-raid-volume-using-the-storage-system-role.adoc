:_mod-docs-content-type: PROCEDURE

[id="configuring-a-raid-volume-using-the-storage-system-role_{context}"]
= Configuring a RAID volume using the storage system role

With the `storage` system role, you can configure a RAID volume on RHEL using Red Hat Ansible Automation Platform and Ansible-Core. Create an Ansible playbook with the parameters to configure a RAID volume to suit your requirements.


.Prerequisites

* The Ansible Core package is installed on the control machine.

* You have the `rhel-system-roles` package installed on the system from which you want to run the playbook.

* You have an inventory file detailing the systems on which you want to deploy a RAID volume using the `storage` system role.

.Procedure

. Create a new _playbook.yml_ file with the following content:
+
[subs="quotes,attributes"]
....
---
- name: Configure the storage
  hosts: managed-node-01.example.com
  tasks:
  - name: Create a RAID on sdd, sde, sdf, and sdg
    include_role:
      name: rhel-system-roles.storage
    vars:
    storage_safe_mode: false
    storage_volumes:
      - name: data
        type: raid
        disks: [sdd, sde, sdf, sdg]
        raid_level: raid0
        raid_chunk_size: 32 KiB
        mount_point: /mnt/data
        state: present
....
+
WARNING: Device names might change in certain circumstances, for example, when you add a new disk to a system. Therefore, to prevent data loss, do not use specific disk names in the playbook.

. Optional: Verify the playbook syntax:
+
[subs="quotes,attributes"]
----
# *ansible-playbook --syntax-check _playbook.yml_*
----

. Run the playbook:
+
[subs="quotes,attributes"]
----
# *ansible-playbook -i _inventory.file_ _/path/to/file/playbook.yml_*
----

//Verification //(Optional)
//This section is not necessary because Ansible Engine clearly shows whether it succeeds or not. We may advise the user on what to do in case the example fails. Talk to your SME to confirm if Verification are required for the role.


[role="_additional-resources"]
.Additional resources
ifndef::parent-context-of-managing-raid[]
ifeval::[{ProductNumber} == 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/managing_storage_devices/managing-raid_managing-storage-devices[Managing RAID]
endif::[]
endif::parent-context-of-managing-raid[]

* The `/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file

* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/automating_system_administration_by_using_rhel_system_roles/index#assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[Preparing a control node and managed nodes to use RHEL system roles].
