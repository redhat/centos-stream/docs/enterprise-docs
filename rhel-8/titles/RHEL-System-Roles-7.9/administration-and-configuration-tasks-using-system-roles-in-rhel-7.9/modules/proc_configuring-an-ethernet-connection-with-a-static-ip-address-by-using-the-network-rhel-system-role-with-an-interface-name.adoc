:_mod-docs-content-type: PROCEDURE
:experimental:

[id="configuring-an-ethernet-connection-with-a-static-ip-address-by-using-the-network-rhel-system-role-with-an-interface-name_{context}"]
= Configuring an Ethernet connection with a static IP address by using the network {RHELSystemRoles} with an interface name

[role="_abstract"]
You can remotely configure an Ethernet connection using the `network` {RHELSystemRoles}.

For example, the procedure below creates a NetworkManager connection profile for the `enp7s0` device with the following settings:

* A static IPv4 address - `192.0.2.1` with a `/24` subnet mask
* A static IPv6 address - `2001:db8:1::1` with a `/64` subnet mask
* An IPv4 default gateway - `192.0.2.254`
* An IPv6 default gateway - `2001:db8:1::fffe`
* An IPv4 DNS server - `192.0.2.200`
* An IPv6 DNS server - `2001:db8:1::ffbb`
* A DNS search domain - `example.com`

Perform this procedure on the Ansible control node.


.Prerequisites

// Common prerequisites for all system role procedures:
ifdef::system-roles-ansible[]
* xref:assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes].
endif::[]
ifndef::system-roles-ansible[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes]
endif::[]
* You are logged in to the control node as a user who can run playbooks on the managed nodes.
* The account you use to connect to the managed nodes has `sudo` permissions on them.
* The managed nodes or groups of managed nodes on which you want to run this playbook are listed in the Ansible inventory file.

// The following are prerequisites that are specific for this task:
* A physical or virtual Ethernet device exists in the server's configuration.
* The managed nodes use NetworkManager to configure the network.


.Procedure

. Create a playbook file, for example `__~/ethernet-static-IP.yml__`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure the network
  hosts: __managed-node-01.example.com__
  tasks:
  - name: Configure an Ethernet connection with static IP
    include_role:
      name: rhel-system-roles.network

    vars:
      network_connections:
        - name: enp7s0
          interface_name: enp7s0
          type: ethernet
          autoconnect: yes
          ip:
            address:
              - 192.0.2.1/24
              - 2001:db8:1::1/64
            gateway4: 192.0.2.254
            gateway6: 2001:db8:1::fffe
            dns:
              - 192.0.2.200
              - 2001:db8:1::ffbb
            dns_search:
              - example.com
          state: up
....

. Run the playbook:
+
[literal,subs="+quotes"]
....
# **ansible-playbook __~/ethernet-static-IP.yml__**
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file

