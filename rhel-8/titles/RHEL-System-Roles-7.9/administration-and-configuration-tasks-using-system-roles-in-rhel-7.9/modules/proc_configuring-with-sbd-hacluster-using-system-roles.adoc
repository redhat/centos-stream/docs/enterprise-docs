:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role.adoc

[id="configuring-ha-cluster-with-sbd_{context}"]
= Configuring a high availability cluster with SBD node fencing

[role="_abstract"]
The following procedure uses the `ha_cluster` system role to create a high availability cluster that uses SBD node fencing.

.Prerequisites

* You have `ansible-core` installed on the node from which you want to run the playbook.
+
[NOTE]
You do not need to have `ansible-core` installed on the cluster member nodes.

* You have the `rhel-system-roles` package installed on the system from which you want to run the playbook.

* The systems that you will use as your cluster members have active subscription coverage for RHEL and the RHEL High Availability Add-On.

[WARNING]
====

The `ha_cluster` system role replaces any existing cluster configuration on the specified nodes. Any settings not specified in the role will be lost.

====

.Procedure

. Create an inventory file specifying the nodes in the cluster, as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/automating_system_administration_by_using_rhel_system_roles/index#ha-system-role-inventory_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role[Specifying an inventory for the ha_cluster system role].
You can optionally configure watchdog and SBD devices for each node in the cluster in an inventory file.

. Create a playbook file, for example `new-cluster.yml`.
+
[NOTE]
====

When creating your playbook file for production, vault encrypt the password, as described in link:++https://docs.ansible.com/ansible/latest/user_guide/vault.html++[Encrypting content with Ansible Vault].

====
+
The following example playbook file configures a cluster running the `firewalld` and `selinux` services that uses SBD fencing.
+
....

- hosts: node1 node2
  vars:
    ha_cluster_cluster_name: my-new-cluster
    ha_cluster_hacluster_password: password
    ha_cluster_manage_firewall: true
    ha_cluster_manage_selinux: true
    ha_cluster_sbd_enabled: yes
    ha_cluster_sbd_options:
      - name: delay-start
        value: 'no'
      - name: startmode
        value: always
      - name: timeout-action
        value: 'flush,reboot'
      - name: watchdog-timeout
        value: 5

  roles:
    - rhel-system-roles.ha_cluster

....

. Save the file.
. Run the playbook, specifying the path to the inventory file _inventory_ you created in Step 1.
+
[subs="+quotes"]
....
# *ansible-playbook -i _inventory_ new-cluster.yml*
....
