:_mod-docs-content-type: PROCEDURE
[id="proc_installing-and-configuring-mssql-using-mssql-rhel-system-role_{context}"]
= Installing and configuring SQL server using the `microsoft.sql.server` Ansible role

[role="_abstract"]
You can use the `microsoft.sql.server` Ansible role to install and configure SQL server.

.Prerequisites

* The Ansible inventory is created

.Procedure

. Create a file with the `.yml` extension. For example, `mssql-server.yml`.
. Add the following content to your `.yml` file:
+
[source,ansible]
----
---
- hosts: all
  vars:
    mssql_accept_microsoft_odbc_driver_17_for_sql_server_eula: true
    mssql_accept_microsoft_cli_utilities_for_sql_server_eula: true
    mssql_accept_microsoft_sql_server_standard_eula: true
    mssql_password: <password>
    mssql_edition: Developer
    mssql_tcp_port: 1433
  roles:
    - microsoft.sql.server
----
+
Replace _<password>_ with your SQL Server password.

. Run the `mssql-server.yml` ansible playbook:
+
----
# *ansible-playbook mssql-server.yml*
----
