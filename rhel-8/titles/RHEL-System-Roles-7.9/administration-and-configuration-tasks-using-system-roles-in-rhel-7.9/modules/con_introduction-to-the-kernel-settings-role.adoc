:_mod-docs-content-type: CONCEPT
:experimental:
[id="introduction-to-the-kernel-settings-role_{context}"]
= Introduction to the kernel_settings role

[role="_abstract"]
{RHELSystemRoles}s is a set of roles that provide a consistent configuration interface to remotely manage multiple systems.

{RHELSystemRoles}s were introduced for automated configurations of the kernel using the `kernel_settings` system role. The `rhel-system-roles` package contains this system role, and also the reference documentation.

To apply the kernel parameters on one or more systems in an automated fashion, use the `kernel_settings` role with one or more of its role variables of your choice in a playbook. A playbook is a list of one or more plays that are human-readable, and are written in the YAML format.

ifeval::[{ProductNumber} == 8]
You can use an inventory file to define a set of systems that you want Ansible to configure according to the playbook.
endif::[]


With the `kernel_settings` role you can configure:

* The kernel parameters using the `kernel_settings_sysctl` role variable
* Various kernel subsystems, hardware devices, and device drivers using the `kernel_settings_sysfs` role variable
* The CPU affinity for the `systemd` service manager and processes it forks using the `kernel_settings_systemd_cpu_affinity` role variable
* The kernel memory subsystem transparent hugepages using the `kernel_settings_transparent_hugepages` and `kernel_settings_transparent_hugepages_defrag` role variables

[role="_additional-resources"]
.Additional resources
* `README.md` and `README.html` files in the `/usr/share/doc/rhel-system-roles/kernel_settings/` directory
* link:https://docs.ansible.com/ansible/latest/user_guide/playbooks.html[Working with playbooks]
* https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html[How to build your inventory]
