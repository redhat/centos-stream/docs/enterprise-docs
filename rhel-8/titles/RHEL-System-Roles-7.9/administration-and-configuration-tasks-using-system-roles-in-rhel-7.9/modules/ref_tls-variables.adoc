:_mod-docs-content-type: REFERENCE
[id="ref_tls-variables_{context}"]
= TLS variables

[role="_abstract"]
You can use the following variables to configure the Transport Level Security (TLS) protocol.

.TLS role variables
[subs=+quotes,options="header"]
|===
|Role variable|Description
|mssql_tls_enable
a|This variable enables or disables TLS encryption.

The `microsoft.sql.server` Ansible role performs following tasks when the variable is set to `true`:

* Copies TLS certificate to `/etc/pki/tls/certs/` on the SQL Server
* Copies private key to `/etc/pki/tls/private/` on the SQL Server
* Configures SQL Server to use TLS certificate and private key to encrypt connections

When set to `false`, the TLS encryption is disabled. The role does not remove the existing certificate and private key files.

|mssql_tls_cert
|To define this variable, enter the path to the TLS certificate file.

|mssql_tls_private_key
|To define this variable, enter the path to the private key file.

|mssql_tls_remote_src
|Defines if the role searches for `mssql_tls_cert` and `mssql_tls_private_key` files remotely or on the control node.

When set to the default `false`, the role searches for `mssql_tls_cert` or `mssql_tls_private_key` files on the Ansible control node.

When set to `true`, the role searches for `mssql_tls_cert` or `mssql_tls_private_key` files on the Ansible managed node.

|mssql_tls_version
|Define this variable to select which TSL version to use.

The default is `1.2`

|mssql_tls_force
|Set this variable to `true` to replace the certificate and private key files on the host. The files must exist under `/etc/pki/tls/certs/` and `/etc/pki/tls/private/` directories.

The default is `false`.
|===
