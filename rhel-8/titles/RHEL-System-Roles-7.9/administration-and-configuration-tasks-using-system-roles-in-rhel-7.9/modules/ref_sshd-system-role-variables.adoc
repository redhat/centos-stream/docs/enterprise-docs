:_mod-docs-content-type: REFERENCE
:experimental:

[id="sshd-system-role-variables_{context}"]
= `ssh` Server system role variables

[role="_abstract"]
In an `sshd` system role playbook, you can define the parameters for the SSH configuration file according to your preferences and limitations.

If you do not configure these variables, the system role produces an `sshd_config` file that matches the {ProductShortName} defaults.

In all cases, Booleans correctly render as `yes` and `no` in `sshd` configuration. You can define multi-line configuration items using lists. For example:

----
sshd_ListenAddress:
  - 0.0.0.0
  - '::'
----

renders as:

----
ListenAddress 0.0.0.0
ListenAddress ::
----

.Variables for the `sshd` system role
`sshd_enable`::

If set to `False`, the role is completely disabled. Defaults to `True`.

`sshd_skip_defaults`::

If set to `True`, the system role does not apply default values. Instead, you specify the complete set of configuration defaults by using either the `sshd` dict, or `sshd_Key` variables. Defaults to `False`.

`sshd_manage_service`::

If set to `False`, the service is not managed, which means it is not enabled on boot and does not start or reload. Defaults to `True` except when running inside a container or AIX, because the Ansible service module does not currently support `enabled` for AIX.

`sshd_allow_reload`::

If set to `False`, `sshd` does not reload after a change of configuration. This can help with troubleshooting. To apply the changed configuration, reload `sshd` manually. Defaults to the same value as `sshd_manage_service` except on AIX, where `sshd_manage_service` defaults to `False` but `sshd_allow_reload` defaults to `True`.

`sshd_install_service`::

If set to `True`, the role installs service files for the `sshd` service. This overrides files provided in the  operating system. Do not set to `True` unless you are configuring a second instance and you also change the `sshd_service` variable. Defaults to `False`.
+
The role uses the files pointed by the following variables as templates:
+
----
sshd_service_template_service (default: templates/sshd.service.j2)
sshd_service_template_at_service (default: templates/sshd@.service.j2)
sshd_service_template_socket (default: templates/sshd.socket.j2)
----

`sshd_service`::
This variable changes the `sshd` service name, which is useful for configuring a second `sshd` service instance.

`sshd`::

A dict that contains configuration. For example:
+
----
sshd:
  Compression: yes
  ListenAddress:
    - 0.0.0.0
----

`sshd___OptionName__`::

You can define options by using simple variables consisting of the `sshd_` prefix and the option name instead of a dict. The simple variables override values in the `sshd` dict.. For example:
+
----
sshd_Compression: no
----

`sshd_match` and `sshd_match_1` to `sshd_match_9`::

A list of dicts or just a dict for a Match section. Note that these variables do not override match blocks as defined in the `sshd` dict. All of the sources will be reflected in the resulting configuration file.

.Secondary variables for the `sshd` system role
You can use these variables to override the defaults that correspond to each supported platform.

`sshd_packages`::

You can override the default list of installed packages using this variable.

`sshd_config_owner`, `sshd_config_group`, and `sshd_config_mode`::

You can set the ownership and permissions for the `openssh` configuration file that this role produces using these variables.

`sshd_config_file`::

The path where this role saves the `openssh` server configuration produced.

`sshd_config_namespace`::

The default value of this variable is null, which means that the role defines the entire content of the configuration file including system defaults. Alternatively, you can use this variable to invoke this role from other roles or from multiple places in a single playbook on systems that do not support drop-in directory. The `sshd_skip_defaults` variable is ignored and no system defaults are used in this case.
+
When this variable is set, the role places the configuration that you specify to configuration snippets in an existing configuration file under the given namespace. If your scenario requires applying the role several times, you need to select a different namespace for each application.
+
[NOTE]
====
Limitations of the `openssh` configuration file still apply. For example, only the first option specified in a configuration file is effective for most of the configuration options.
====
+
Technically, the role places snippets in "Match all" blocks, unless they contain other match blocks, to ensure they are applied regardless of the previous match blocks in the existing configuration file. This allows configuring any non-conflicting options from different roles invocations.

`sshd_binary`::

The path to the `sshd` executable of `openssh`.

`sshd_service`::

The name of the `sshd` service. By default, this variable contains the name of the `sshd` service that the target platform uses. You can also use it to set the name of the custom `sshd` service when the role uses the `sshd_install_service` variable.

`sshd_verify_hostkeys`::
Defaults to `auto`. When set to `auto`, this lists all host keys that are present in the produced configuration file, and generates any paths that are not present. Additionally, permissions and file owners are set to default values. This is useful if the role is used in the deployment stage to make sure the service is able to start on the first attempt. To disable this check, set this variable to an empty list `[]`.

`sshd_hostkey_owner`, `sshd_hostkey_group`, `sshd_hostkey_mode`::
Use these variables to set the ownership and permissions for the host keys from `sshd_verify_hostkeys`.

`sshd_sysconfig`::
On RHEL-based systems, this variable configures additional details of the `sshd` service. If set to `true`, this role manages also the `/etc/sysconfig/sshd` configuration file based on the following configuration. Defaults to `false`.

`sshd_sysconfig_override_crypto_policy`::
In {ProductShortName}, when set to `true`, this variable overrides the system-wide crypto policy. Defaults to `false`.

`sshd_sysconfig_use_strong_rng`::
On RHEL-based systems, this variable can force `sshd` to reseed the `openssl` random number generator with the number of bytes given as the argument. The default is `0`, which disables this functionality. Do not turn this on if the system does not have a hardware random number generator.
