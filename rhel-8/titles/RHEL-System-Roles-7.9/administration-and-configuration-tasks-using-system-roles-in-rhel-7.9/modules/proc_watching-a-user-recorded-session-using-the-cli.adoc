:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="watching-a-user-recorded-session-using-the-cli_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Watching a recorded session using the CLI

// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
You can play a user session recording from a journal using the command line (CLI).

.Prerequisites

* You have recorded a user session. See
ifdef::automating-system-administration-by-using-rhel-system-roles[]
xref:recording-a-session-using-the-deployed-tlog-system-role_configuring-a-system-for-session-recording-using-the-tlog-rhel-system-roles[Configuring a system for session recording using the tlog RHEL system role]
endif::[]
ifndef::automating-system-administration-by-using-rhel-system-roles[]
link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/automating_system_administration_by_using_rhel_system_roles/index#recording-a-session-using-the-deployed-tlog-system-role_configuring-a-system-for-session-recording-using-the-tlog-rhel-system-roles[Recording a session using the deployed tlog system role in the CLI]
endif::[]
.

.Procedure

. On the CLI terminal, play the user session recording:
+
[subs="quotes,attributes"]
----
# *journalctl -o verbose -r*
----

. Search for the `tlog` recording:
+
[subs="quotes,attributes"]
----
$ */tlog-rec*
----
+
You can see details such as:

* The username for the user session recording
* The `out_txt` field, a raw output encode of the recorded session
* The identifier number TLOG_REC=_ID_number_

. Copy the identifier number TLOG_REC=_ID_number_.

. Playback the recording using the identifier number TLOG_REC=_ID_number_.
+
[subs="quotes,attributes"]
----
# *tlog-play -r journal -M TLOG_REC=_ID_number_*
----

As a result, you can see the user session recording terminal output being played back.
