:_mod-docs-content-type: REFERENCE

[id="parameters-that-identify-a-storage-device-in-the-storage-system-role_{context}"]
= Parameters that identify a storage device in the `storage` {RHELSystemRoles}

[role="_abstract"]
Your `storage` role configuration affects only the file systems, volumes, and pools that you list in the following variables.

`storage_volumes`::
List of file systems on all unpartitioned disks to be managed.
+
`storage_volumes` can also include `raid` volumes.
+
Partitions are currently unsupported.

`storage_pools`::
List of pools to be managed.
+
Currently the only supported pool type is LVM. With LVM, pools represent volume groups (VGs). Under each pool there is a list of volumes to be managed by the role. With LVM, each volume corresponds to a logical volume (LV) with a file system.
