// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

////
Base the file name and the ID on the module title. For example:
* file name: ref-my-reference-a.adoc
* ID: [id="ref-my-reference-a_{context}"]
* Title: = My reference A

The ID is an anchor that links to the module. Avoid changing it after the module has been published to ensure existing links are not broken.

The `context` attribute enables module reuse. Every module ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
////

:_mod-docs-content-type: REFERENCE
[id="example-ansible-playbook-to-resize-an-existing-file-system-using-the-storage-rhel-system-role_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Example Ansible playbook to resize an existing Ext4 or Ext3 file system using the `storage` {RHELSystemRoles}
////
In the title of a reference module, include nouns that are used in the body text. For example, "Keyboard shortcuts for ___" or "Command options for ___." This helps readers and search engines find the information quickly.
////

[role="_abstract"]
This section provides an example Ansible playbook. This playbook applies the `storage` role to resize an existing Ext4 or Ext3 file system on a block device.

.A playbook that set up a single volume on a disk
====

[subs=+quotes]
----
---
- name: Create a disk device mounted on `/opt/barefs`
- hosts: all
  vars:
    storage_volumes:
      - name: [replaceable]__barefs__
        type: disk
        disks:
          - [replaceable]__/dev/sdb__
        size: [replaceable]__12 GiB__
        fs_type: ext4
        mount_point: [replaceable]__/opt/barefs__
  roles:
    - rhel-system-roles.storage
----

====

* If the volume in the previous example already exists, to resize the volume, you need to run the same playbook, just with a different value for the parameter `size`. For example:

.A playbook that resizes `ext4` on `/dev/sdb`
====

[subs=+quotes]
----
---
- name: Create a disk device mounted on `/opt/barefs`
- hosts: all
  vars:
    storage_volumes:
      - name: [replaceable]__barefs__
        type: disk
        disks:
          - [replaceable]__/dev/sdb__
        size: [replaceable]__10 GiB__
        fs_type: ext4
        mount_point: [replaceable]__/opt/barefs__
  roles:
    - rhel-system-roles.storage
----

* The volume name (barefs in the example) is currently arbitrary. The Storage role identifies the volume by the disk device listed under the disks: attribute.
====

NOTE: Using the `Resizing` action in other file systems can destroy the data on the device you are working on.

[role="_additional-resources"]
.Additional resources
* The [filename]`/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file.
