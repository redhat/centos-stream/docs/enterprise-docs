:_mod-docs-content-type: PROCEDURE
[id="using-the-metrics-role-to-monitor-your-local-system-with-visualization_{context}"]
= Using the `metrics` system role to monitor your local system with visualization

[role="_abstract"]
This procedure describes how to use the `metrics` {RHELSystemRoles} to monitor your local system while simultaneously provisioning data visualization via `Grafana`.

.Prerequisites

* The Ansible Core package is installed on the control machine.

* You have the `rhel-system-roles` package installed on the machine you want to monitor.

.Procedure

. Configure `localhost` in the `/etc/ansible/hosts` Ansible inventory by adding the following content to the inventory:
+
[subs=+quotes]
----
localhost ansible_connection=local
----

. Create an Ansible playbook with the following content:
+
[subs=+quotes]
----
---
- name: Manage metrics
  hosts: localhost
  vars:
    metrics_graph_service: yes
    metrics_manage_firewall: true
    metrics_manage_selinux: true
  roles:
    - rhel-system-roles.metrics
----

. Run the Ansible playbook:
+
[subs=+quotes]
----
# *ansible-playbook _name_of_your_playbook_.yml*
----
+
NOTE: Since the `metrics_graph_service` boolean is set to value="yes", `Grafana` is automatically installed and provisioned with `pcp` added as a data source. Since metrics_manage_firewall and metrics_manage_selinux are both set to true, the metrics role will use the firewall and selinux system roles to manage the ports used by the metrics role.

. To view visualization of the metrics being collected on your machine, access the `grafana` web interface as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/monitoring_and_managing_system_status_and_performance/setting-up-graphical-representation-of-pcp-metrics_monitoring-and-managing-system-status-and-performance#accessing-the-grafana-web-ui_setting-up-graphical-representation-of-pcp-metrics[Accessing the Grafana web UI].
