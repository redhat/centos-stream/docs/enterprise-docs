// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
:_mod-docs-content-type: REFERENCE
[id="example-ansible-playbook-to-resize-an-existing-lvm-file-system-using-the-storage-rhel-system-role_{context}"]
= Example Ansible playbook to resize an existing file system on LVM using the `storage` {RHELSystemRoles}


[role="_abstract"]
This section provides an example Ansible playbook. This playbook applies the `storage` {RHELSystemRoles} to resize an LVM logical volume with a file system.

WARNING: Using the `Resizing` action in other file systems can destroy the data on the device you are working on.

.A playbook that resizes existing mylv1 and myvl2 logical volumes in the myvg volume group
====

[subs=+quotes]
----
---

- hosts: all
   vars:
    storage_pools:
      - name: myvg
        disks:
          - /dev/sda
          - /dev/sdb
          - /dev/sdc
        volumes:
            - name: mylv1
              size: [replaceable]__10 GiB__
              fs_type: ext4
              mount_point: [replaceable]__/opt/mount1__
            - name: mylv2
              size: [replaceable]__50 GiB__
              fs_type: ext4
              mount_point: [replaceable]__/opt/mount2__

- name: Create LVM pool over three disks
  include_role:
    name: rhel-system-roles.storage
----

* This playbook resizes the following existing file systems:

** The Ext4 file system on the `mylv1` volume, which is mounted at `/opt/mount1`, resizes to 10 GiB.
** The Ext4 file system on the `mylv2` volume, which is mounted at `/opt/mount2`, resizes to 50 GiB.
====

[role="_additional-resources"]
.Additional resources
* The [filename]`/usr/share/ansible/roles/rhel-system-roles.storage/README.md` file.
