:_mod-docs-content-type: PROCEDURE
:experimental:

[id="configuring-a-static-ethernet-connection-with-802-1x-network-authentication-by-using-the-network-rhel-system-role_{context}"]
= Configuring a static Ethernet connection with 802.1X network authentication by using the network {RHELSystemRoles}

[role="_abstract"]
Using the `network` {RHELSystemRoles}, you can automate the creation of an Ethernet connection that uses the 802.1X standard to authenticate the client. For example, remotely add an Ethernet connection for the `enp1s0` interface with the following settings by running an Ansible playbook:

* A static IPv4 address - `192.0.2.1` with a `/24` subnet mask
* A static IPv6 address - `2001:db8:1::1` with a `/64` subnet mask
* An IPv4 default gateway - `192.0.2.254`
* An IPv6 default gateway - `2001:db8:1::fffe`
* An IPv4 DNS server - `192.0.2.200`
* An IPv6 DNS server - `2001:db8:1::ffbb`
* A DNS search domain - `example.com`
* 802.1X network authentication using the `TLS` Extensible Authentication Protocol (EAP)

Perform this procedure on the Ansible control node.



.Prerequisites
// Common prerequisites for all system role procedures:
ifdef::system-roles-ansible[]
* xref:assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes].
endif::[]
ifndef::system-roles-ansible[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes]
endif::[]
* You are logged in to the control node as a user who can run playbooks on the managed nodes.
* The account you use to connect to the managed nodes has `sudo` permissions on them.
* The managed nodes or groups of managed nodes on which you want to run this playbook are listed in the Ansible inventory file

// The following are prerequisites that are specific for this task:
* The network supports 802.1X network authentication.
* The managed nodes uses NetworkManager.
* The following files required for TLS authentication exist on the control node:
** The client key is stored in the `/srv/data/client.key` file.
** The client certificate is stored in the `/srv/data/client.crt` file.
** The Certificate Authority (CA) certificate is stored in the `/srv/data/ca.crt` file.


.Procedure

. Create a playbook file, for example `__~/enable-802.1x.yml__`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure an Ethernet connection with 802.1X authentication
  hosts: __managed-node-01.example.com__
  tasks:
    - name: Copy client key for 802.1X authentication
      copy:
        src: "/srv/data/client.key"
        dest: "/etc/pki/tls/private/client.key"
        mode: 0600

    - name: Copy client certificate for 802.1X authentication
      copy:
        src: "/srv/data/client.crt"
        dest: "/etc/pki/tls/certs/client.crt"

    - name: Copy CA certificate for 802.1X authentication
      copy:
        src: "/srv/data/ca.crt"
        dest: "/etc/pki/ca-trust/source/anchors/ca.crt"

    - include_role:
        name: rhel-system-roles.network

      vars:
        network_connections:
          - name: enp1s0
            type: ethernet
            autoconnect: yes
            ip:
              address:
                - 192.0.2.1/24
                - 2001:db8:1::1/64
              gateway4: 192.0.2.254
              gateway6: 2001:db8:1::fffe
              dns:
                - 192.0.2.200
                - 2001:db8:1::ffbb
              dns_search:
                - example.com
            ieee802_1x:
              identity: __user_name__
              eap: tls
              private_key: "/etc/pki/tls/private/client.key"
              private_key_password: "password"
              client_cert: "/etc/pki/tls/certs/client.crt"
              ca_cert: "/etc/pki/ca-trust/source/anchors/ca.crt"
              domain_suffix_match: __example.com__
            state: up
....

. Run the playbook:
+
[literal,subs="+quotes"]
....
# **ansible-playbook __~/enable-802.1x.yml__**
....

[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file

