:_mod-docs-content-type: PROCEDURE
:experimental:
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="installing-collections-from-automation-hub_{context}"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Installing Collections from Automation Hub
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

[role="_abstract"]
If you are using the Automation Hub, you can install the {RHELSystemRoles}s Collection hosted on the Automation Hub.

.Prerequisites

* Access and permissions to one or more _managed nodes_.

* Access and permissions to a _control node_, which is a system from which Red Hat Ansible Core configures other systems.
+
On the control node:

** The `ansible-core` and `rhel-system-roles` packages are installed.

** An inventory file which lists the managed nodes.

.Procedure

. Define Red Hat Automation Hub as the default source for content in the `ansible.cfg` configuration file. See
ifdef::getting_started_with_red_hat_ansible_automation_hub[]
xref:proc-configure-automation-hub-server[Configuring Red Hat Automation Hub as the primary source for content]
endif::[]
ifndef::composing_a_customized_rhel_system_image[]
link:https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/1.2/html/getting_started_with_red_hat_ansible_automation_hub/proc-configure-automation-hub-server[Configuring Red Hat Automation Hub as the primary source for content]
endif::[]
.

. Install the `redhat.rhel_system_roles` collection from the Automation Hub:
+
[subs="quotes,attributes"]
----
# *ansible-galaxy collection install redhat.rhel_system_roles*
----
+
After the installation is finished, the roles are available as `redhat.rhel_system_roles.<role_name>`. Additionally, you can find the documentation for each role at `/usr/share/ansible/collections/ansible_collections/redhat/rhel_system_roles/roles/<role_name>/README.md`.

.Verification

To verify the install, run the `kernel_settings` role with `check` mode on your localhost. You must also use the `--become` parameter because it is necessary for the Ansible `package` module. However, the parameter will not change your system:

. Run the following command: 
+
[subs="quotes,attributes"]
----
$ *ansible-playbook -c local -i localhost, --check --become /usr/share/ansible/collections/ansible_collections/redhat/rhel_system_roles/tests/kernel_settings/tests_default.yml*
----

The last line of the command output should contain the value `failed=0`.

NOTE: The comma after `localhost` is mandatory. You must add it even if there is only one host on the list. Without it, `ansible-playbook` would identify `localhost` as a file or a directory.

[role="_additional-resources"]
.Additional resources
* `ansible-playbook` man page on your system
* link:https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html#cmdoption-ansible-playbook-i[The `-i` option of the `ansible-playbook` command]

