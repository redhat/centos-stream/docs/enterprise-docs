:_mod-docs-content-type: PROCEDURE
:experimental:

[id="configuring-an-ethernet-connection-with-a-dynamic-ip-address-by-using-the-network-rhel-system-role-with-a-device-path_{context}"]
= Configuring an Ethernet connection with a dynamic IP address by using the network {RHELSystemRoles} with a device path

[role="_abstract"]
You can remotely configure an Ethernet connection using the `network` {RHELSystemRoles}. For connections with dynamic IP address settings, NetworkManager requests the IP settings for the connection from a DHCP server.

You can identify the device path with the following command:

[literal,subs="+quotes"]
....
# **udevadm info /sys/class/net/__<device_name>__ | grep ID_PATH=**
....

Perform this procedure on the Ansible control node.



.Prerequisites
// Common prerequisites for all system role procedures:
ifdef::system-roles-ansible[]
* xref:assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes].
endif::[]
ifndef::system-roles-ansible[]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html/automating_system_administration_by_using_rhel_system_roles/assembly_preparing-a-control-node-and-managed-nodes-to-use-rhel-system-roles_automating-system-administration-by-using-rhel-system-roles[You have prepared the control node and the managed nodes]
endif::[]
* You are logged in to the control node as a user who can run playbooks on the managed nodes.
* The account you use to connect to the managed nodes has `sudo` permissions on them.
* The managed nodes or groups of managed nodes on which you want to run this playbook are listed in the Ansible inventory file.

// The following are prerequisites that are specific for this task:
* A physical or virtual Ethernet device exists in the server's configuration.
* A DHCP server is available in the network.
* The managed hosts use NetworkManager to configure the network.


.Procedure

. Create a playbook file, for example `__~/ethernet-dynamic-IP.yml__`, with the following content:
+
[source,yaml,subs="+quotes"]
....
---
- name: Configure the network
  hosts: __managed-node-01.example.com__
  tasks:
  - name: Configure an Ethernet connection with dynamic IP
    include_role:
      name: rhel-system-roles.network

    vars:
      network_connections:
        - name: example
          match:
            path:
              - pci-0000:00:0[1-3].0
              - &!pci-0000:00:02.0
          type: ethernet
          autoconnect: yes
          ip:
            dhcp4: yes
            auto6: yes
          state: up
....
+
The `match` parameter in this example defines that Ansible applies the play to devices that match PCI ID `0000:00:0[1-3].0`, but not `0000:00:02.0`. For further details about special modifiers and wild cards you can use, see the `match` parameter description in the `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file.

. Run the playbook:
+
[literal,subs="+quotes"]
....
# **ansible-playbook __~/ethernet-dynamic-IP.yml__**
....


[role="_additional-resources"]
.Additional resources
* `/usr/share/ansible/roles/rhel-system-roles.network/README.md` file

