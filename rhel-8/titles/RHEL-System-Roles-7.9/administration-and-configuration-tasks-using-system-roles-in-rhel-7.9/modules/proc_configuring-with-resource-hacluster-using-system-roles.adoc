:_mod-docs-content-type: PROCEDURE
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
// assembly_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role.adoc

[id="configuring-ha-cluster-with-fencing-and-resources_{context}"]
= Configuring a high availability cluster with fencing and resources

[role="_abstract"]
The following procedure uses the `ha_cluster` system role to create a high availability cluster that includes a fencing device, cluster resources, resource groups, and a cloned resource.

.Prerequisites

* You have `ansible-core` installed on the node from which you want to run the playbook.
+
[NOTE]
You do not need to have `ansible-core` installed on the cluster member nodes.

* You have the `rhel-system-roles` package installed on the system from which you want to run the playbook.

* The systems that you will use as your cluster members have active subscription coverage for RHEL and the RHEL High Availability Add-On.

[WARNING]
====

The `ha_cluster` system role replaces any existing cluster configuration on the specified nodes. Any settings not specified in the role will be lost.

====

.Procedure

. Create an inventory file specifying the nodes in the cluster, as described in link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/{ProductNumberLink}/html-single/automating_system_administration_by_using_rhel_system_roles/index#ha-system-role-inventory_configuring-a-high-availability-cluster-by-using-the-ha-cluster-rhel-system-role[Specifying an inventory for the ha_cluster system role].

. Create a playbook file, for example `new-cluster.yml`.
+
[NOTE]
====

When creating your playbook file for production, vault encrypt the password, as described in link:++https://docs.ansible.com/ansible/latest/user_guide/vault.html++[Encrypting content with Ansible Vault].

====
+
The following example playbook file configures a cluster running the `firewalld` and `selinux` services. The cluster includes fencing, several resources, and a resource group. It also includes a resource clone for the resource group.
+
....

- hosts: node1 node2
  vars:
    ha_cluster_cluster_name: my-new-cluster
    ha_cluster_hacluster_password: password
    ha_cluster_manage_firewall: true
    ha_cluster_manage_selinux: true
    ha_cluster_resource_primitives:
      - id: xvm-fencing
        agent: 'stonith:fence_xvm'
        instance_attrs:
          - attrs:
              - name: pcmk_host_list
                value: node1 node2
      - id: simple-resource
        agent: 'ocf:pacemaker:Dummy'
      - id: resource-with-options
        agent: 'ocf:pacemaker:Dummy'
        instance_attrs:
          - attrs:
              - name: fake
                value: fake-value
              - name: passwd
                value: passwd-value
        meta_attrs:
          - attrs:
              - name: target-role
                value: Started
              - name: is-managed
                value: 'true'
        operations:
          - action: start
            attrs:
              - name: timeout
                value: '30s'
          - action: monitor
            attrs:
              - name: timeout
                value: '5'
              - name: interval
                value: '1min'
      - id: dummy-1
        agent: 'ocf:pacemaker:Dummy'
      - id: dummy-2
        agent: 'ocf:pacemaker:Dummy'
      - id: dummy-3
        agent: 'ocf:pacemaker:Dummy'
      - id: simple-clone
        agent: 'ocf:pacemaker:Dummy'
      - id: clone-with-options
        agent: 'ocf:pacemaker:Dummy'
    ha_cluster_resource_groups:
      - id: simple-group
        resource_ids:
          - dummy-1
          - dummy-2
        meta_attrs:
          - attrs:
              - name: target-role
                value: Started
              - name: is-managed
                value: 'true'
      - id: cloned-group
        resource_ids:
          - dummy-3
    ha_cluster_resource_clones:
      - resource_id: simple-clone
      - resource_id: clone-with-options
        promotable: yes
        id: custom-clone-id
        meta_attrs:
          - attrs:
              - name: clone-max
                value: '2'
              - name: clone-node-max
                value: '1'
      - resource_id: cloned-group
        promotable: yes

  roles:
    - rhel-system-roles.ha_cluster
....

. Save the file.
. Run the playbook, specifying the path to the inventory file _inventory_ you created in Step 1.
+
[subs="+quotes"]
....
# *ansible-playbook -i _inventory_ new-cluster.yml*
....
