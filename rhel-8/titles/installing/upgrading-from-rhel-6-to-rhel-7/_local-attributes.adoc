// Show the table of contents
:toc:
// The name of the title
:ProjectName: Upgrading from RHEL{nbsp}6 to RHEL{nbsp}7
// The subtitle of the title
:Subtitle: Instructions for an in-place upgrade from {ProductName}{nbsp}6 to {RHEL7}
// The abstract of the title
:Abstract: This document provides instructions on how to perform an in-place upgrade from Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}(RHEL){nbsp}6 to RHEL{nbsp}7. During the in-place upgrade, the existing RHEL 6 operating system is replaced by the RHEL 7 version.
// The name of the title for the purposes of {context}
:ProjectNameID: upgrading-from-rhel-6-to-rhel-7

// The following are not required
:ProjectVersion: 0.1

