// Show the table of contents
:toc:
// The name of the title
:ProjectName: Converting from a Linux distribution to RHEL using the Convert2RHEL utility in Red Hat Insights
// The subtitle of the title
:Subtitle: Instructions for a conversion from CentOS Linux 7 to Red{nbsp}Hat Enterprise Linux 7 using the Convert2RHEL utility in Red Hat Insights
// The abstract of the title
:Abstract: This document provides instructions on how to convert your operating system from CentOS Linux to RHEL 7 using the Convert2RHEL utility in Red Hat Insights. 
// The name of the title for the purposes of {context}
:ProjectNameID: converting-from-a-linux-distribution-to-rhel-in-insights

// The following are not required
:ProjectVersion: 0.1

