// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

:composer-guide:

// ustory: SYSTEMS/CLOUD ADMINISTRATOR: “I need to quickly create customized OS images for my hybrid deployment environments, including physical, virtual, and private and public clouds.”

// include::common-content/making-open-source-more-inclusive.adoc[]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::assemblies/assembly_introducing-image-builder-on-cloud.adoc[leveloffset=+1]

include::assemblies/assembly_getting-access-to-image-builder-on-console.adoc[leveloffset=+1]

include::assemblies/assembly_creating-a-customized-system-image-with-an-embed-subscription-using-image-builder.adoc[leveloffset=+1]

include::assemblies/assembly_adding-packages-during-image-creation.adoc[leveloffset=+1]

include::assemblies/assembly_customizing-file-systems-during-the-image-creation.adoc[leveloffset=+1]

include::assemblies/assembly_creating-a-customized-rhel-system-image-for-aws-using-image-builder.adoc[leveloffset=+1]

include::assemblies/assembly_creating-and-uploading-customized-rhel-system-image-to-azure-using-image-builder.adoc[leveloffset=+1]

include::assemblies/assembly_creating-and-uploading-a-customized-rhel-system-image-to-gcp-using-image-builder.adoc[leveloffset=+1]

include::assemblies/assembly_creating-and-uploading-a-customized-rhel-vmdk-system-image-to-vsphere.adoc[leveloffset=+1]

include::assemblies/assembly_creating-a-customized-rhel-guest-image-using-red-hat-image-builder.adoc[leveloffset=+1]

include::assemblies/assembly_creating-a-customized-rhel-bare-metal-image-using-red-hat-image-builder.adoc[leveloffset=+1]

include::assemblies/assembly_locating-the-images-you-created-by-using-red-hat-image-builder.adoc[leveloffset=+1]

