// Show the table of contents
:toc:
// The name of the title
:ProjectName: Installing Identity{nbsp}Management
// The subtitle of the title
:Subtitle: Methods of installing IdM servers and clients
// The abstract of the title
//:Abstract: The abstract is directly in docinfo.xml, because within an attribute, multiple paragraphs are not supported.
// The name of the title for the purposes of {context}
:ProjectNameID: installing-identity-management

// The following are not required
:ProjectVersion: 0.1

:server-host-name: server.idm.example.com
:server1-ipv4: 192.0.2.1
:server1-ipv6: 2001:DB8::1111

:server-container-name: server-container
:replica-container-name: replica-container

//:server-host-name: server.example.com
:replica-host-name: replica.idm.example.com
:client-host-name: client.idm.example.com
:domain-name: idm.example.com
:realm-name: IDM.EXAMPLE.COM
// When it's necessary to reference IPA and AD systems, and distinguish their host names:
//:idm-host-name: idm.example.com
//:ad-host-name: ad.example.com

:server1-ipv4: 192.0.2.1
:server1-ipv6: 2001:DB8::1111
:replica1-ipv4: 192.0.2.2
:client-ipv4: 203.0.113.1
