:converting-to-rhel:

// Include shared, global attributes
include::common-content/_attributes.adoc[]

// Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context for all included assemblies
:context: {ProjectNameID}

// This turns on internal, debug information in all included assemblies
// :internal:

This document provides instructions on how to convert your operating system from the following Linux distributions to Red Hat Enterprise Linux (RHEL) 7, RHEL 8, and RHEL 9:

* Alma Linux
* CentOS Linux
* Oracle Linux
* Rocky Linux

The conversion is performed by the `Convert2RHEL` utility.

[NOTE]
====
The conversion from Scientific Linux to RHEL is not currently supported by Red Hat. For information about an unsupported conversion, see link:https://access.redhat.com/articles/2360841[How to perform an unsupported conversion from a RHEL-derived Linux distribution to RHEL].
====

// include::common-content/making-open-source-more-inclusive.adoc[]

include::modules/common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::modules/upgrades-and-differences/ref_key-migration-terminology.adoc[leveloffset=+1]

include::modules/conversion/con_supported-conversion-paths.adoc[leveloffset=+1]

include::modules/conversion/con_conversion-methods.adoc[leveloffset=+1]

include::modules/conversion/con_planning-a-rhel-conversion.adoc[leveloffset=+1]

include::assemblies/assembly_converting-using-the-command-line.adoc[leveloffset=+1]

include::assemblies/assembly_converting-using-insights.adoc[leveloffset=+1]

include::modules/conversion/con_rollback.adoc[leveloffset=+1]

include::assemblies/assembly_troubleshooting-rhel-conversions.adoc[leveloffset=+1]

[role="_additional-resources"]
== Additional resources
* link:https://access.redhat.com/articles/2360841[How to perform an unsupported conversion from a RHEL-derived Linux distribution to RHEL]
* link:https://access.redhat.com/articles/rhel-limits[Red Hat Enterprise Linux technology capabilities and limits]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8[Red Hat Enterprise Linux documentation]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/migration_planning_guide/index[Migration planning guide - migration to RHEL 7] 
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/considerations_in_adopting_rhel_8/index[Considerations in adopting RHEL 8]
* link:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/upgrading_from_rhel_7_to_rhel_8/index[Upgrading from RHEL 7 to RHEL 8]
* link:https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html-single/upgrading_from_rhel_8_to_rhel_9/index[Upgrading from RHEL 8 to RHEL 9]
* link:https://access.redhat.com/articles/5941531[ Convert2RHEL FAQ (Frequently Asked Questions) ]
