// Include shared, global .NET attributes
//////////////////////////////////////////////////////////////////////////////
// *IMPORTANT*: .NET version MUST be defined in meta/global-dotnet-attributes.adoc before publishing
//////////////////////////////////////////////////////////////////////////////
include::rhel-dotnet-attribute.adoc[]

include::common-content/_global-dotnet-attributes.adoc[]

//Include per-title attributes
include::_local-attributes.adoc[]

// The name and ID of the title is defined in local-attributes.adoc
[id="{ProjectNameID}"]
= {ProjectName}

// Set context fo all included assemblies
:context: {ProjectNameID}

// include::common-content/making-open-source-more-inclusive.adoc[leveloffset=+1]

include::common-content/proc_providing-feedback-on-red-hat-documentation.adoc[leveloffset=+1]

include::modules/dotnet/con_availability.adoc[leveloffset=+1]

include::modules/dotnet/con_dotnet-overview.adoc[leveloffset=+1]

include::assemblies/assembly_features-and-benefits.adoc[leveloffset=+1]

include::modules/dotnet/con_supported-operating-systems-and-architecture.adoc[leveloffset=+1]

include::modules/dotnet/con_customer-privacy.adoc[leveloffset=+1]

include::assemblies/assembly_support.adoc[leveloffset=+1]

include::modules/dotnet/con_known-issues.adoc[leveloffset=+1]
