// Show the table of contents
:toc:
// The name of the title
:ProjectName: Getting started with .NET on RHEL {rhel-ver}
// The subtitle of the title
:Subtitle: Installing and running {dotnet} {dotnet-ver} on RHEL {rhel-ver}

// The abstract of the title
:Abstract: This guide describes how to install and run {dotnet} {dotnet-ver} on RHEL {rhel-ver}.

// The name of the title for the purposes of {context}
:ProjectNameID: getting-started-with-dotnet-on-rhel-7

// The following are not required
:ProjectVersion:
