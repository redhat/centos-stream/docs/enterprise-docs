:_mod-docs-content-type: CONCEPT
[preface]
[id="beta-changes_{context}"]
= Beta changes

// When on a Beta branch, copy this file to your title's directory and add links to the highlighted content.
//
// Only reference major documentation updates and new stories in this module. Lists of fixed DDFs, CLP, MS4, or other internal updates are not needed here. 

The following list highlights parts of the documentation that have been newly added or updated as part of this Beta preview:

* xref:id[Title] OR link:url-or-file-id[Text title of the link] - optional text providing additonal information about the new content


