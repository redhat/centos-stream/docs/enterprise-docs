////
This file is neither module nor an assembly. It can be included anywhere to insert a Beta preview note warning. Use it this way:

	:BetaRFE: Name of your product, component or feature
	include::common-content/snip_beta.adoc[]

If you enclose the text in the BetaRFE attribute (variable) in an appropriate pass:[] macro, it can contain any inline markup such as bold, monospace, italics, certain macros, attribute expansions etc. Here's an example:

	:BetaRFE: pass:c,q,a,r,m,p[blah `monospace` and some {attribute} lorem ipsum]
	include::common-content/snip_beta.adoc[]

Use the magic as shown here and you can put almost anything into BetaRFE. Note that you can not easily use a sentence fragment in BetaRFE, as it is used once at the beginning of a sentence and then in the middle, so capitalization is a problem.
////

[IMPORTANT]
====
{BetaRFE} is only provided as a Beta preview. It is under development and is subject to substantial change. Consider the included information incomplete and use it with caution.

Customers deploying {BetaRFE} are encouraged to provide feedback to {RH}.
====

