:Year: 2024

// Red Hat and divisions
:RH: Red{nbsp}Hat
:CCS: Customer Content Services
:OrgName: {RH}
:OrgDiv: {CCS}

// RHEL
:RHEL: {RH} Enterprise{nbsp}Linux
:RHEL9: {RHEL}{nbsp}9
:RHEL8: {RHEL}{nbsp}8
:RHEL7: {RHEL}{nbsp}7

// Package manager branding

:PackageManagerName: YUM
:PackageManagerCommand: yum

// .NET attributes

//////////////////////////////////////////////////////////////////////////////
IMPORTANT: Set this to `3.1` to publish .NET Core 3.1 content, set to `2.1` to publish .NET Core 2.1 content, set to `5` to publish .NET 5 content, and set to `6` to publish .NET 6 content.
//////////////////////////////////////////////////////////////////////////////
:dotnet-ver: 7.0

// .NET versions 3.1 and below are resolved as .NET Core, version 5 is resolved as just .NET
ifeval::[{dotnet-ver} >= 5.0]
:dotnet: .NET
endif::[]
ifeval::[{dotnet-ver} <= 3.1]
:dotnet: .NET Core
endif::[]

// The product (.NET)
:ProductName: .NET
:ProductShortName: .NET
// This is the version displayed under ".NET"
:ProductNumber: {dotnet-ver}

:imagesdir: images
// resolves all UI elements like btn:[some button name]
:experimental:

// Random
:rhel: {RHEL}
:os: OpenShift
:ocp: {os}{nbsp}Container{nbsp}Platform
:app: application
:apps: applications

// Some attributes used within terminal input/output based on .NET version
ifeval::[{dotnet-ver} >= 3.1]
:instance-cap: 2247483647
endif::[]

ifeval::[{dotnet-ver} == 2.1]
:instance-cap: 2147483647
endif::[]

ifeval::[{dotnet-ver} >= 3.1]
:dotnet-publish: dotnet publish -c Release
endif::[]

ifeval::[{dotnet-ver} == 2.1]
:dotnet-publish: dotnet publish -c Release /p:MicrosoftNETPlatformLibrary=Microsoft.NETCore.App
endif::[]

ifeval::[{dotnet-ver} >= 5]
:dotnet-publish-command: net{dotnet-ver}
endif::[]
ifeval::[{dotnet-ver} < 5]
:dotnet-publish-command: netcoreapp{dotnet-ver}
endif::[]

ifeval::[{dotnet-ver} >= 5]
:dotnet-branch: dotnet-{dotnet-ver}
endif::[]
ifeval::[{dotnet-ver} < 5]
:dotnet-branch: dotnetcore-{dotnet-ver}
endif::[]

ifeval::[{dotnet-ver} == 8.0]
:prod-ver-short: 80
endif::[]
ifeval::[{dotnet-ver} == 7.0]
:prod-ver-short: 70
endif::[]
ifeval::[{dotnet-ver} == 6.0]
:prod-ver-short: 60
endif::[]
ifeval::[{dotnet-ver} == 5.0]
:prod-ver-short: 50
endif::[]
ifeval::[{dotnet-ver} == 3.1]
:prod-ver-short: 31
endif::[]
ifeval::[{dotnet-ver} == 2.1]
:prod-ver-short: 21
endif::[]

ifeval::[{dotnet-ver} >= 5]
:buildconfig-var: netapp
endif::[]
ifeval::[{dotnet-ver} <= 3]
:buildconfig-var: netcoreapp
endif::[]

:prod-scl: rh-dotnet{prod-ver-short}

// Container images
:image-prefix: dotnet-{prod-ver-short}

// NOTE: These attributes are required for the RN & KI doc for containers.
:url-runtime-image-rhel8: registry.access.redhat.com/ubi{rhel-ver}/{image-prefix}-runtime
:url-s2i-image-rhel8: registry.access.redhat.com/ubi8/{image-prefix}

:url-runtime-image-rhel9beta: registry.access.redhat.com/ubi{rhel-ver}-beta/{image-prefix}-runtime

ifeval::[{dotnet-ver} >= 5]
:url-runtime-image-rhel7: registry.redhat.io/ubi8/{image-prefix}-runtime
endif::[]
ifeval::[{dotnet-ver} < 5]
:url-runtime-image-rhel7: registry.redhat.io/dotnet/{image-prefix}-runtime-rhel7
endif::[]

:url-s2i-image-rhel7: registry.redhat.io/dotnet/{image-prefix}-rhel7

ifeval::[{rhel-ver} == 7]
:runtime-image: {image-prefix}-runtime-rhel7

ifeval::[{dotnet-ver} >= 5]
:runtime-image-full: ubi8/{image-prefix}-runtime
endif::[]
ifeval::[{dotnet-ver} < 5]
:runtime-image-full: dotnet/{runtime-image}
endif::[]

:url-runtime-image: {url-runtime-image-rhel7}
endif::[]

//just for Beta
//after Beta delete this conditional and replace the following conditional (ifeval::[{rhel-ver} == 8]) with >= 8
ifeval::[{rhel-ver} >= 8]
:runtime-image: {image-prefix}-runtime
endif::[]

// TODO: verify url-runtime-image for RHEL 9
ifeval::[{rhel-ver} >= 8]
:runtime-image-full: ubi{rhel-ver}/{runtime-image}
:url-runtime-image: {url-runtime-image-rhel8}
endif::[]

ifeval::[{rhel-ver} == 7]
:s2i-image: {image-prefix}-rhel7
:s2i-image-full: dotnet/{s2i-image}
:url-s2i-image: {url-s2i-image-rhel7}
endif::[]
ifeval::[{rhel-ver} == 8]
:s2i-image: {image-prefix}
:s2i-image-full: ubi8/{s2i-image}
:url-s2i-image: {url-s2i-image-rhel8}
endif::[]


ifeval::[{rhel-ver} == 7]
:url-imagestreams-json: https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/master/dotnet_imagestreams.json
endif::[]

ifeval::[{rhel-ver} == 8]
ifeval::[{dotnet-ver} >= 5]
:url-imagestreams-json: https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/master/dotnet_imagestreams.json
endif::[]
ifeval::[{dotnet-ver} < 5]
:url-imagestreams-json: https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/master/dotnet_imagestreams_rhel{rhel-ver}.json
endif::[]
endif::[]
ifeval::[{rhel-ver} > 8]
:url-imagestreams-json: https://raw.githubusercontent.com/redhat-developer/s2i-dotnetcore/master/dotnet_imagestreams.json
endif::[]

// URLs
ifeval::[{dotnet-ver} >= 5]
:url-path: .net
endif::[]
ifeval::[{dotnet-ver} < 5]
:url-path: .net_core
endif::[]
:url-rh-access: https://access.redhat.com
:url-rh-docs: {url-rh-access}/documentation/en-us
:dotnet-url: {url-rh-docs}/net
:ultimate-dotnet-url: {dotnet-url}/{dotnet-ver}
:url-rn-rpms: {ultimate-dotnet-url}/html-single/release_notes_for_{url-path}_{dotnet-ver}_rpm_packages/index
:url-rn-containers: {ultimate-dotnet-url}/html-single/release_notes_for_{url-path}_{dotnet-ver}_containers/
////
:url-dotnet-getting-started-rhel8: {ultimate-dotnet-url}/html-single/getting_started_with_.net_on_rhel_8/
////
:url-dotnet-getting-started-rhel-7: {ultimate-dotnet-url}/html-single/getting_started_with_.net_on_rhel_7/

:url-dotnet-getting-started-rhel: {ultimate-dotnet-url}/html-single/getting_started_with_.net_on_rhel_{rhel-ver}/

:url-dotnet-getting-started-rhel-9: {ultimate-dotnet-url}/html-single/getting_started_with_.net_on_rhel_9/

:url-dotnet-getting-started-openshift:

// Title names
:rn-rpms-title: Release Notes for {dotnet} {dotnet-ver} RPM packages

// Code block
ifeval::[{dotnet-ver} == 5]
:image-snippet: <image>registry.access.redhat.com/ubi8/{image-prefix}-jenkins-slave-rhel7:latest</image>
endif::[]
ifeval::[{dotnet-ver} < 5]
:image-snippet: <image>registry.access.redhat.com/dotnet/{image-prefix}-jenkins-slave-rhel7:latest</image>
endif::[]

ifeval::[{dotnet-ver} >= 5]
:github-path: dotnet
endif::[]
ifeval::[{dotnet-ver} < 5]
:github-path: dotnetcore
endif::[]

//Supported OS
ifeval::[{dotnet-ver} == 8.0]
:supportedOS: RHEL 8.9 and later, RHEL 9.3 and later
endif::[]
ifeval::[{dotnet-ver} == 7.0]
:supportedOS: RHEL 8.7 and later, RHEL 9.1 and later
endif::[]
ifeval::[{dotnet-ver} == 6.0]
:supportedOS: RHEL 7, RHEL 8, RHEL 9
endif::[]
ifeval::[{dotnet-ver} == 5.0]
:supportedOS: RHEL 7, RHEL 8
endif::[]
ifeval::[{dotnet-ver} == 3.1]
:supportedOS: RHEL 7, RHEL 8, RHEL 9
endif::[]
ifeval::[{dotnet-ver} == 2.1]
:supportedOS: RHEL 7, RHEL 8
endif::[]

//Minor RHEL version
ifeval::[{rhel-ver} == 8]
ifeval::[{dotnet-ver} == 8.0]
:rhel-minor-ver: .9
endif::[]
endif::[]
ifeval::[{rhel-ver} == 9]
ifeval::[{dotnet-ver} == 8.0]
:rhel-minor-ver: .3
endif::[]
endif::[]
