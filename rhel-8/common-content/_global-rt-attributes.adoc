:Year: 2021

// Red Hat and divisions
:RH: Red{nbsp}Hat
:CCS: Customer Content Services
:OrgName: {RH}
:OrgDiv: {CCS}

// The product (RHEL for RT)
:ProductName: {RH} Enterprise{nbsp}Linux for Real Time
:RT: {ProductName}
:ProductShortName: RT

// This is the version displayed under "Red Hat Enterprise Linux"
:ProductNumber: 8
:PRODUCT: RHEL{nbsp}8
:RHEL8: {RHEL}{nbsp}8
:RHEL7: {RHEL}{nbsp}7
//:ProductRelease: Beta

// Package manager branding

:PackageManagerName: YUM
:PackageManagerCommand: yum

:imagesdir: images
:experimental:

// Identity Management attributes

:IPA: Identity{nbsp}Management
:aIPA: an Identity{nbsp}Management
:AD: Active{nbsp}Directory
:RH: Red{nbsp}Hat
:RHEL: Red{nbsp}Hat Enterprise{nbsp}Linux
:DS: Directory{nbsp}Server
:CS: Certificate{nbsp}System
:SSSD: System Security Services Daemon
:AD: Active{nbsp}Directory

// Platform tools attributes
:gcct: GCC Toolset
// dts and rhscl references are to be removed in rhel 8, present only to enable building
:gcct-ver: 10
:dts: {rh} Developer{nbsp}Toolset
:rhdts: {dts}
:dts-ver: 10
:scl: {rh} Software{nbsp}Collections
:rhscl: {scl}
:rhah: {ProductName} Atomic{nbsp}Host
:rhdt: {rh} Developer{nbsp}Tools
// ++ for using in things like g++ that are NOT capitalized C++ = {cpp}
:plus2: &#x002b;&#x002b;

// Real time attributes
:RT: Red{nbsp}Hat Enterprise{nbsp}Linux for Real Time
