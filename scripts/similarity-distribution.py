#!/usr/bin/env python3

# Copyright 2022 Marek Suchánek <msuchane@redhat.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This script processes the data produced by the find-duplicates.sh script,
specifically the comparisons.json file. It renders a histogram graph that
displays how the file similarity results are distributed. You can find
the generated histogram in the distribution.pdf file.

Run this script from the root of the documentation repository,
or from the same directory as the previous find-duplicates.sh analysis:

rhel-8-docs]$ scripts/similarity-distribution.py

Note that the graph only captures data for files above the similarity
threshold, which is 0.85 by default. If you want data on the complete
documentation set, adjust the find-duplicates.sh script and lower
the threshold to 0.0. As a side effect, the analysis takes several hours
and produces a comparisons.json file in the order of GiB in size.
"""

import json
import matplotlib.pyplot as plt

DATA_FILE = "comparisons.json"
OUTPUT_FILE = "distribution.pdf"

def main():
    try:
        with open(DATA_FILE) as f:
            obj = json.load(f)
    except FileNotFoundError:
        print("Comparisons file not found.")
        print("1. Run scripts/find-duplicates.sh.")
        print("2. Run scripts/similarity-distribution.py in the same directory.")
        exit(1)

    values = highest_similarities(obj)

    plt.hist(values, bins=40, range=(0, 100), rwidth=0.8)
    plt.savefig(OUTPUT_FILE)

def insert(d: dict, file_name: str, sim: float):
    """
    Record the highest found similarity for this file in the shared dict.
    """
    if d.get(file_name) is None:
        d[file_name] = sim
    else:
        if d[file_name] < sim:
            d[file_name] = sim

def highest_similarities(obj: list) -> list:
    """
    For each compared file, find its highest similarity with any other file.
    Return the list of highest similarities.
    """
    highest = {}

    for d in obj:
        file1 = d["file1"]
        file2 = d["file2"]
        sim = d["pct_similar"]

        insert(highest, file1, sim)
        insert(highest, file2, sim)

    values = highest.values()

    return values

if __name__ == "__main__":
    main()

