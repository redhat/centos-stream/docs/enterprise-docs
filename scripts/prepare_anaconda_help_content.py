#!/usr/bin/python
#
# Red Hat internal script, no license statements intended
# Process the Installation docs material to a format suitable for use as built-in-help in Anaconda
# rhel 7 original by Petr Bokoc (pbokoc), adapted for rhel 8 by Vladimir Slavik (vslavik)
# 2018
#
# input: the rest of repository (implied)
# output: file anaconda-help.xml ready for use in yelp in anaconda
#
# dependencies: asciidoctor, sed

import os, shutil
from lxml import etree as ET

# !!!! This script assumes it is started in the scripts directory

title_dir = "../rhel-9/titles/installing/performing-a-standard-rhel-installation"

print("converting AsciiDoc to DocBook")
os.system("asciidoctor --backend=docbook {}/anaconda-master.adoc".format(title_dir))

print("removing namespace")
os.system("""sed -e 's/xmlns="http\:\/\/docbook\.org\/ns\/docbook" //' -i {}/anaconda-master.xml""".format(title_dir))

print("removing non breakable spaces")
os.system("""sed -e 's/&nbsp;/ /g' -i {}/anaconda-master.xml""".format(title_dir))


print("loading XML for detailed replacements")
known_ids = {}
tree = ET.parse(title_dir + "/anaconda-master.xml")
root = tree.getroot()


print("searching for IDs") # <<<<<<<<< unless needeed it's all to be removed
# find all elements that have an id attribute
for element in root.iter():
		id = element.attrib.get("id") or element.attrib.get("{http://www.w3.org/XML/1998/namespace}id")
		if id:
				# the element has an id attribute
				title = element.find('title')
				if hasattr(title, "text"):
						# store the title text under the id
						known_ids[id] = title.text
				else:
						# some title elements might not have any text property
						print("  WARNING: id %s has no title text" % (id))

print("  %d ids found" % len(known_ids))


removed_figures = 0
root = tree.getroot()
print("removing figures")
for figure in root.findall('.//figure'):
		parent = figure.getparent()
		parent.remove(figure)
		removed_figures += 1
print("  %d figures removed" % (removed_figures))


removed_remarks = 0
root = tree.getroot()
print("removing remark tags")
for remark in root.findall('.//remark'):
		parent = remark.getparent()
		parent.remove(remark)
		removed_remarks += 1
print("  %d remarks removed" % (removed_remarks))


outside_links = 0
print("analyzing xref links")
# rewrite all links to a format digestible by Yelp
for xref in root.findall('.//xref'):
		link_target = xref.attrib.get('linkend')
		if link_target:
				# the logic is such that if (link_target in known_ids) then link can stay as is
				if link_target not in known_ids:
						# this link points outside of the help files currently used by Anaconda, so replace it with "find it somewhere else" template
						print("  INFO: outside link, id: %s" % (link_target))
						# lxml doesn't seem to be able to replace an element with a string, so we will just clear the element and replace it with the templates in a later sed pass
						tail = xref.tail
						# clear() removes the tail, which is in this case pretty much unrelated to the element, so we need to make sure to save & restore it
						xref.clear()
						xref.tail = tail
						outside_links += 1
		else:
				print("  WARNING: xref link with missing linkend")
print("  %d outside links rewritten" % (outside_links))


print("saving modified XML")
tree.write("anaconda-help.xml")

print("cleaning up xref links that go to nowhere")
os.system(r"""sed -e "s/<xref[\s]*\/>/<citetitle>Installing and deploying RHEL<\/citetitle> on the Red Hat Customer Portal/g" -i anaconda-help.xml""")

print("reinserting namespace")
os.system("""sed -e 's#<book#<book xmlns="http://docbook.org/ns/docbook"#g' -i anaconda-help.xml""")

print("copying anchor file")
shutil.copy(title_dir + "/anaconda-gui-help-anchors.json", "anaconda-gui.json")

print("done!")
