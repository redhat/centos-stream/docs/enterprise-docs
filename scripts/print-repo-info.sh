#!/bin/bash

# list-repo-information.sh - list basic information about the repository
# Copyright (C) 2022 Jaromir Hradilek <jhradilek@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# Load common constants:
. "${0%/*}/lib/constants.sh"

# Define the pattern for ignored paths:
declare -r IGNORED_PATHS='*/\.git/*'

# Get the data about the working directory:
declare -r CLONED_SIZE=$(\du -sh "$REPO_TOPLEVEL" | \cut -f 1)
declare -r NUM_FILES=$(\find "$REPO_TOPLEVEL" -not -path "$IGNORED_PATHS" -type f | \wc -l)
declare -r NUM_SYMLINKS=$(\find "$REPO_TOPLEVEL" -not -path "$IGNORED_PATHS" -type l | \wc -l)
declare -r NUM_DIRECTORIES=$(\find "$REPO_TOPLEVEL" -not -path "$IGNORED_PATHS" -type d | \wc -l)
declare -r NUM_MODULES=$(\find "$REPO_TOPLEVEL" -not -path "$IGNORED_PATHS" -type f -regex "$MODULE_PATTERN" | \wc -l)
declare -r NUM_ASSEMBLIES=$(\find "$REPO_TOPLEVEL" -not -path "$IGNORED_PATHS" -type f -regex "$ASSEMBLY_PATTERN" | \wc -l)
declare -r NUM_IMAGES=$(\find "$REPO_TOPLEVEL" -not -path "$IGNORED_PATHS" -type f -iregex "$IMAGE_PATTERN" | \wc -l)
declare -r NUM_ATTRIB_FILES=$(\find "$REPO_TOPLEVEL" -not -path "$IGNORED_PATHS" -not -regex "$FCC_PATTERN" -iregex '.*/[^/]*attributes[^/]*\.adoc' | \wc -l)

cat <<-EOF
Size of the working copy:  $CLONED_SIZE

Number of files:           $NUM_FILES
Number of symbolic links:  $NUM_SYMLINKS
Number of directories:     $NUM_DIRECTORIES

Number of modules:         $NUM_MODULES
Number of assemblies:      $NUM_ASSEMBLIES
Number of images:          $NUM_IMAGES
Number of attribute files: $NUM_ATTRIB_FILES
EOF
