#!/bin/bash

# list-unused-images.sh - list all images that are not included in files
# and list first author email
# Copyright (C) 2021 Jaromir Hradilek <jhradilek@redhat.com>
# Copyright (C) 2023 Levi V <levi@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# IMPORTANT: Be aware that the script does NOT account for the contents of
# the .gitignore file. Consequently, it will, by default, locate all img
# references even in temporary untracked files. That tends to be slow and
# produces false positives.
# Before running the script, create a fresh repository clone or clean up
# your local copy by runninng "git clean -fxd" .


# Load common constants:
. "${0%/*}/lib/constants.sh"

# Locate all images and print those that are not included in any AsciiDoc
# source file:
find "$REPO_TOPLEVEL" -not -path '*/\.*' -type f -iregex "$IMAGE_PATTERN" | xargs -I %% -P 0 bash -c "echo '%%' | sed 's#.*/#^\\\\(image:\\\\|[^/].*image:\\\\).*#' | grep -qr --include '*.adoc' -f - '$REPO_TOPLEVEL' || { git log --follow --find-renames=40% --pretty=format:'%aE' -- %% | tail -n 1; echo -e '\t%%'; }"
