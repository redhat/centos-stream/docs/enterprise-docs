#!/usr/bin/env sh

# Copyright 2022 Marek Suchánek <msuchane@redhat.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage:
# The `near-facsimile` tool is available at <https://github.com/msuchane/near-facsimile>.
# You can pass additional `near-facsimile` options to this script as regular arguments.
# This script might take around an hour to proces the whole respository.

# Ensure compatibility with both Podman and Docker:
if $(which podman); then
    alias docker="podman"
fi

# Download the latest version of near-facsimile:
docker pull quay.io/msuchane/near-facsimile:latest

# Run near-facsimile with settings appropriate to RHEL documentation:
docker run -it -v .:/mnt:Z msuchane/near-facsimile near-facsimile \
    --threshold 85 \
    --progress \
    --require-ext adoc \
    --ignore-file master.adoc \
    --ignore-file main.adoc \
    --ignore-file _attributes.adoc \
    --ignore-file _local-attributes.adoc \
    --ignore-file _title-attributes.adoc \
    --ignore-file README.adoc \
    --ignore-file proc_providing-feedback-on-red-hat-documentation.adoc \
    --ignore-file beta.adoc \
    --ignore-file beta-title.adoc \
    --ignore-file making-open-source-more-inclusive.adoc \
    --skip-lines '^//' \
    --json comparisons.json \
    --csv comparisons.csv \
    "$@"

