#!/bin/bash

# ct10.sh - copy files to rhel-10-doc repository with modifications
# # Copyright (C) 2024 Marc Muehlfeld <mmuehlfeld@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.



# Show help
function usage() {
	echo -e "\nUsage: $0 SOURCE_FILE DESTINATION_DIRECTORY"
	exit 0
}


# Show text passed to this function in red
function red_text() {
	echo -e "\033[0;31m$*\033[0m"
}

# Show text passed to this function in blue
function blue_text() {
	echo -e "\033[0;34m$*\033[0m"
}


if [ "$(uname)" == "Darwin" ]; then

	if ! $(sed --version | head -1 | grep GNU >/dev/null 2>&1) ; then
		echo "This script requires GNU sed."
		echo ""
		echo "1. Install GNU sed from https://formulae.brew.sh/formula/gnu-sed"
		echo ""
		echo "2. Append the following to your ~/.zshrc file:"
		echo "PATH=\"$HOMEBREW_PREFIX/opt/gnu-sed/libexec/gnubin:\$PATH\""
		echo ""
		echo "3. Reload the ~/.zshrc file:"
		echo "source ~/.zshrc"
		exit 1
	fi

	if ! $(awk --version | head -1 | grep GNU >/dev/null 2>&1) ; then
		echo "This script requires GNU awk."
		echo ""
		echo "1. Install GNU awk from https://formulae.brew.sh/formula/gawk"
		echo ""
		echo "2. Append the following to your ~/.zshrc file:"
		echo "PATH=\"$HOMEBREW_PREFIX/opt/gawk/libexec/gnubin:\$PATH\""
		echo ""
		echo "3. Reload the ~/.zshrc file:"
		echo "source ~/.zshrc"
		exit 1
	fi

	blue_text "ct10.sh was not tested on MacOS! It might work, but always verify that the script actually made the changes it reports!"
fi


# Set defaults
DIFF=false
CACHE_DIR=~/.cache/ct10/


while getopts "dh" OPTION ; do
	case $OPTION in
		d)      DIFF=true ;;
		h)      usage ;;
		*)      usage ;;
	esac
done

shift $((OPTIND-1))


SOURCE="$1"
if [ ! -e "$SOURCE" ] ; then
	echo "Source file $SOURCE does not exist."
	usage
	exit 1
fi


# Determine source file type
# Determine file type from _mod-docs-content-type attribute within the file
case "$(grep -E "^:_mod-docs-content-type: " "$SOURCE" | head -1 | sed -e 's/^.* //')" in
	PROCEDURE|CONCEPT|REFERENCE)	FILE_TYPE="module" ;;
	ASSEMBLY)			FILE_TYPE="assembly" ;;
	*)				FILE_TYPE="" ;;
esac

# If file type couldn't be determined from the attribute, try to determine from the file name prefix
if [ -z "$FILE_TYPE" ] ; then
	case "$(basename "$SOURCE" | sed -e 's/_.*$//')" in
		proc|con|ref)	FILE_TYPE="module" ;;
		assembly)	FILE_TYPE="assembly" ;;
		*)		FILE_TYPE="" ;;
	esac
fi


DEST="$2"
if [ ! -d "$DEST" ] ; then
	if [ -z "$DEST" ] ; then
		echo "Destination directory not specified."
	else
		echo "Destination directory $DEST does not exist."
	fi

	# If file type couldn't be determined, we can't use the destination from last time
	if [ -z "$FILE_TYPE" ] ; then
		usage
		exit 1
	fi

	# Read previous destination from history file and test whether it exists
	PREV_DEST="$(head -1 $CACHE_DIR/last_${FILE_TYPE}_dir 2>/dev/null)"
	if [ ! -d "$PREV_DEST" ] ; then
		usage
		exit 1
	fi

	# Should the destination directory from last time be used?
	read -p "Use previous $FILE_TYPE directory $PREV_DEST? (y/n) " ANSWER
	case ${ANSWER:0:1} in
		y|Y)	DEST="$PREV_DEST" ;;
		*)	usage
			exit 1 ;;
	esac

fi


# Generate a new file name based on the module's title
NEW_FILENAME="$(grep -E "^= " "$SOURCE" | head -1 | sed -e 's/ \[\[.*$//' -e 's/[^[:alnum:] -.\/]//g' -e 's/^ //' -e "s/[',]//g" -e 's/[ .\/]/-/g' -e 's/[A-Z]/\L&/g').adoc"


# If no file name can be generated based on the module's title, keep the old file name but remove the prefix
test -z "$NEW_FILENAME" && NEW_FILENAME="$(basename $SOURCE | sed -e 's/\(assembly_\|proc_\|con_\|ref_\)//g')"

if [ "$(basename "$SOURCE")" != "$NEW_FILENAME" ] ; then
	echo "* File name in destination will be adjusted to correspond the module's title and without type prefix."
	echo "  New name: $NEW_FILENAME"
	if $(grep -E "^= " "$SOURCE" | grep -q "{") ; then
		red_text "  The generated file name contains an unresolved attribute. Manually rename the generated file."
	fi
fi


NEW_FILE="$(sed -e 's|/\+|/|g' <<< "$DEST/$NEW_FILENAME")"
test -e "$NEW_FILE" && echo -n "  "
cp -i "$SOURCE" "$NEW_FILE"
if [ $? -ne 0 ] ; then
	echo "Operation canceled."
	exit 1
else
	echo "* File copied."
fi


OLD_ID="$(grep -E "^\[id=[\"\']" "$NEW_FILE" | head -1 | sed -e 's/.*["'\'']\(.*\)["'\''].*/\1/g')"

# The new file's ID is the same as the new file name (but without .adoc suffix)
NEW_ID="$(sed -e 's/\.adoc//' <<< "$NEW_FILENAME")"

if [ "$OLD_ID" != "$NEW_ID" ] ; then
	sed -i -e 's/'$OLD_ID'/'$NEW_ID'/' "$NEW_FILE"
	echo "* ID changed to correspond to the module's title"
	echo "  Previous ID: $OLD_ID"
	echo "  New ID     : $NEW_ID"
	if $(grep -E "^= " "$SOURCE" | grep -q "{") ; then
		red_text "  The generated ID contains an unresolved attribute. Manually fix the ID in the generated file."
	fi
fi


# Remove context-related code (typically in assemblies)
if grep -qE "^ifdef::context\[:parent-context-of-" "$NEW_FILE" || grep -qE "^:parent-context-of-" "$NEW_FILE" ; then
	sed -i -e '/^ifdef::context\[:parent-context-of-/d' \
	       -e '/^:context:/d' \
	       -e '/^ifdef::parent-context-of-/d' \
	       -e '/^ifndef::parent-context-of-/d' \
	       -e '/^ifdef::context\[\]/,+2 d' \
	       -e '/^ifndef::context\[\]/,/endif::\[\]/{/^\[id="/!d}' \
	       -e '/^:parent-context-of-/d' \
	       -e '/^:context: {parent-context-of/d' \
		"$NEW_FILE"
	echo "* Removed context-related conditions and comments from assembly."
fi


# Remove comment bloat (comments originally added by the modular docs templates)
sed -i -e '/^\/\/ Retains the context of the parent assembly if this assembly is nested within another assembly./d' \
       -e '/^\/\/ For more information about nesting assemblies, see:/d' \
       -e '/^\/\/ See also the complementary step on the last line of this file./d' \
       -e '/^\/\/ Base the file name and the ID on the assembly title. For example:/d' \
       -e '/^\/\/ \* file name: my-assembly-a.adoc/d' \
       -e '/^\/\/ \* ID: \[id="my-assembly-a"\]/d' \
       -e '/^\/\/ \* Title: = My assembly A/d' \
       -e '/^\/\/ The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken./d' \
       -e '/^\/\/ If the assembly is reused in other assemblies in a guide, include {context} in the ID: \[id="a-collection-of-modules_{context}"\]./d' \
       -e '/^\/\/ If the assembly covers a task, start the title with a verb in the gerund form, such as Creating or Configuring./d' \
       -e "/^\/\/ The \`context\` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide./d" \
       -e '/^\/\/ The following include statements pull in the module files that comprise the assembly. Include any combination of concept, procedure, or reference modules required to cover the user story. You can also include other assemblies./d' \
       -e '/^\/\/ Restore the context to what it was before this assembly./d' \
       -e '/^\/\/ Base the file name and the ID on the module title. For example:/d' \
       -e '/^\/\/ \* file name: doing-procedure-a.adoc/d' \
       -e '/^\/\/ \* ID: \[id="doing-procedure-a"\]/d' \
       -e '/^\/\/ \* Title: = Doing procedure A/d' \
       -e '/^\/\/ The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken./d' \
       -e '/^\/\/ Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_./d' \
       "$NEW_FILE"
echo "* Removed modular docs comments."


# More cleanups
if grep -qE "^:experimental:" "$NEW_FILE" ; then
	sed -i -e '/^:experimental:/d' "$NEW_FILE"
	echo "* Removed :experimental: attribute."
fi


if grep -qE "^\[role=\"_abstract\"\]" "$NEW_FILE" ; then
	sed -i -e '/^\[role="_abstract"\]/d' "$NEW_FILE"
	echo "* Removed [role="_abstract"\] tag."
fi


if grep -q "options="nowrap",role=white-space-pre" "$NEW_FILE" ; then
	sed -i -e 's///' "$NEW_FILE"
	echo "* Removed unnecessary \"options="nowrap",role=white-space-pre\" subs from code blocks."
fi


if grep -q "{PackageManagerCommand}" "$NEW_FILE" ; then
	sed -i -e 's/{PackageManagerCommand}/dnf/g' "$NEW_FILE"
	echo "* Replaced \"PackageManagerCommand\" attribute with \"dnf\"."
fi


if grep -q "{RHELSystemRoles}" "$NEW_FILE" ; then
	sed -i -e 's/{RHELSystemRoles}/RHEL system role/g' "$NEW_FILE"
	echo "* Replaced \"RHELSystemRoles\" attribute with \"RHEL system role\"."
fi


if grep -q "{ProductName}" "$NEW_FILE" ; then
	sed -i -e 's/{ProductName}/{RHEL}/g' "$NEW_FILE"
	echo "* Replaced \"ProductName\" with \"{RHEL}\" attribute."
fi


if grep -q "{ProductShortName}" "$NEW_FILE" ; then
	sed -i -e 's/{ProductShortName}/RHEL/g' "$NEW_FILE"
	echo "* Replaced \"ProductShortName\" with \"RHEL\"."
fi


if grep -q "{ProductNumber}" "$NEW_FILE" ; then
	if $(grep "{ProductNumber}" "$NEW_FILE" | grep -Eq "^ifeval::\[{ProductNumber}") ; then
		echo "* Replaced \"ProductNumber\" attribute with \"10\", except in ifeval conditions."
	else
		echo "* Replaced \"ProductNumber\" attribute with \"10\"."
	fi
	sed -i -e '/^ifeval::\[{ProductNumber}/b; s/{ProductNumber}/10/' "$NEW_FILE"
fi


if grep -q "common-content/snip_common-prerequisites.adoc" "$NEW_FILE" ; then
	sed -i -e 's/common-content\/snip_common-prerequisites.adoc/common\/sys-roles-common-prerequisites.adoc/' "$NEW_FILE"
	echo "* Updated path to system roles common prerequisites snippet."
fi


if grep -q "common-content/snip_web-console-log-in-step.adoc" "$NEW_FILE" ; then
	sed -i -e 's/common-content\/snip_web-console-log-in-step.adoc/common\/web-console-log-in-step.adoc/' "$NEW_FILE"
	echo "* Updated path to web console login step snippet."
fi


if grep -q "common-content/snip_web-console-prerequisites.adoc" "$NEW_FILE" ; then
	sed -i -e 's/common-content\/snip_web-console-prerequisites.adoc/common\/web-console-prerequisites.adoc/' "$NEW_FILE"
	echo "* Updated path to web console prerequisite snippet."
fi


if grep -q "’" "$NEW_FILE" ; then
	sed -i -e "s/’/'/g" "$NEW_FILE"
	echo "* Replaced ’ with '."
fi


if grep -q "“" "$NEW_FILE" ; then
	sed -i -e 's/“/"/g' "$NEW_FILE"
	echo "* Replaced “ with \"."
fi


if grep -qE "\b(menu:[^ ][^[]*|btn:|kbd:)\[[^]]+(\.{3}|…)\]" "$NEW_FILE" ; then
	sed -i -e 's/\b\(menu:[^ ][^[]*\|btn:\|kbd:\)\(\[[^]]\+\)\(\.\{3\}\|…\)\]/\1\2]/' "$NEW_FILE"
	echo "* Removed ... and … from UI macros."
fi


if grep -q "{NM_.*}" "$NEW_FILE" ; then
	sed -i -e 's/{NM_controller}/controller/g' "$NEW_FILE"
	sed -i -e 's/{NM_port}/port/g' "$NEW_FILE"
	sed -i -e 's/{NM_ports}/ports/g' "$NEW_FILE"
	echo "* Replaced \"NM_*\" attributes with hard-coded values."
fi


if grep -qE "^include::.*(proc_|con_|ref_)" "$NEW_FILE" ; then
	sed -i -e 's/^\(include::.*\)\(proc_\|con_\|ref_\)\(.*\)$/\1\3/g' "$NEW_FILE"
	echo "* Removed file proc_, con_, and ref_ prefixes from file names in include statements."
fi


if grep -qE "xref:(proc_|con_|ref_|assembly_)" "$NEW_FILE" ; then
	sed -i -e 's/\(ref_\|con_\|proc_\|assembly_\)//g' "$NEW_FILE"
	echo "* Removed proc_, con_, ref_, and assembly_ prefixes from IDs in xref statements."
fi


if grep -qE "xref:[a-z0-9-]*_[a-z0-9-]*\[" "$NEW_FILE" ; then
	sed -i -e 's/\(xref:[a-z0-9-]*\)_[a-z0-9-]*\[/\1[/g' "$NEW_FILE"
	echo "* Removed context part from xref statements."
fi


if grep -qE '[^`]?\[[a-z]*\]`' "$NEW_FILE" ; then
	sed -i -e 's/\([^`]\?\)\[[a-z]*\]`/\1`/g' "$NEW_FILE"
	echo "* Removed semantic markup."
fi


if tail -n 1 "$NEW_FILE" | grep -vq '^$' ; then
	echo "" >> "$NEW_FILE"
	echo "* Appended an empty line for cleaner diffs."
fi


if grep -q "access.redhat.com/documentation/en-us/" "$NEW_FILE" ; then
	sed -i -e 's/access.redhat.com\/documentation\/en-us\//docs.redhat.com\/en\/documentation\//g' "$NEW_FILE"
	echo "* Rewrote access.redhat.com/documentation/en-us/... to docs.redhat.com/en/documentation/..."
fi


if [ $(grep -E "^\* \`[A-Za-z\._-]*\([0-9][Pp]?\)\` man page$" "$NEW_FILE" | wc -l) -eq 1 ] ; then
	# This check intentionally updates an entry only if just one reference is found. If more are found, they need to be manually combined (see checks below)
	sed -i -e 's/^\(\* `[A-Za-z\._-]*([0-9])` man page\)$/\1 on your system/g' "$NEW_FILE"
	echo "* Appended \"on your system\" to man page reference."
fi



# Check for things that can't be automatically fixed and report them
if grep -q "{ProductNumberLink}" "$NEW_FILE" ; then
	sed -i -e 's/{ProductNumberLink}/9/g' "$NEW_FILE"
	red_text "* Replaced \"ProductNumberLink\" attribute with \"9\". If the linked content is available in RHEL 10.0 beta, manually replace \"9\" in the URL with \"10-beta\"."
fi


if grep -E "[Uu]sing" "$NEW_FILE" | grep -v "^include::" | grep -qvE "[Bb]y[ -][Uu]sing" ; then
	red_text "* The file contains occurrences of \"using\" instead of \"by using\". Please check if this is correct."
fi


if grep -qE "^ifeval::\[{ProductNumber}" "$NEW_FILE" ; then
	red_text "* File contains \"ifeval::[{ProductNumber}...\" conditions. Please remove them and adjust the code for RHEL 10."
fi


if grep -q "{context}" "$NEW_FILE" ; then
	red_text "* File still contains \"{context}\" attributes. Please remove them and adjust the code."
fi


if grep "man page" "$NEW_FILE" | grep -vE "^//" | grep -qE '`[a-zA-Z0-9-]+` man page?' ; then
	red_text "* File contains man page references without their section numbers, like \`xyz\` instead of \`xyz(1)\`. Add the section number manually."
	red_text "  See https://redhat-documentation.github.io/supplementary-style-guide/#man-pages"
fi


if awk '{if ($0 ~ /man page/) {if (prev ~ /man page/) {found=1; exit 0} prev=$0} else if ($0 !~ /^$/) {prev=""}} END {exit (found ? 0 : 1)}'  $NEW_FILE ; then
	# The awk command ignores empty lines between matches
	red_text "* File contains several consecutive lines with man page references. Combine them manually in a single line."
	red_text "  See https://redhat-documentation.github.io/supplementary-style-guide/#man-pages"
fi


if grep -E "^\*.*man page" $NEW_FILE | grep -vE "^//" | grep -qvE "man page(s)? on your system" ; then
	red_text "* Man page references miss \"...on your system\"."
	red_text "  See https://redhat-documentation.github.io/supplementary-style-guide/#man-pages"
fi


if grep -q "/html-single/" "$NEW_FILE" ; then
	red_text "* File contains at least one link to a html-single page. Manually change these links to the multi-page HTML version."
fi


if grep -q "recommend" "$NEW_FILE" ; then
	red_text "* File contains at least one mention of \"recommend\". Avoid this word. See IBM Style."
fi


if grep -qE "^image::" "$NEW_FILE" ; then
	red_text "* File references images. Do not forget to copy the image files as well."
	red_text "$(grep -E "^image::" "$NEW_FILE" | sed -e 's/^image::\(.*\)\[\]/  \1/')"
fi

if grep -qE '^image:[^:]' "$NEW_FILE" ; then
	red_text "* File contains an \"image\" macro followed by only one colon. Check if this is intentional."
	red_text "  Use \"image:\" only for inline images (image appears in the middle of a sentence)."
	red_text "  For block images, use \"image::\" (two colons)."
	red_text "  For details, see https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/#images."
fi


# Show a diff, if -d was set
if $DIFF ; then
	echo -e "\n"
	echo -e "* Diff of the old and new files:"
	diff --color=always "$SOURCE" "$NEW_FILE"
	echo -e "\n"
fi



# Show the path to the copied file (to allow copy/paste of the path)
echo "Path to the new file: $NEW_FILE"


# Update directory history file if file type is known
if [ ! -z "$FILE_TYPE" ] ; then
	test ! -d ~/.cache/ct10/ && mkdir -p ~/.cache/ct10/
	echo "$DEST" > $CACHE_DIR/last_${FILE_TYPE}_dir
fi

