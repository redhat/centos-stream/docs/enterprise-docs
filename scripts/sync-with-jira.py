#!/usr/bin/python3
import argparse
import logging
import os
import re
import sys

import gitlab
import jira

# Script metadata
NAME = os.path.basename(sys.argv[0])
DESCRIPTION = 'Transfer values of Gitlab MR fields to their linked Jira tickets.'

# Connections
GITLAB_URL='https://gitlab.cee.redhat.com'
# Previous testing project
# GITLAB_DOC_PROJECT_ID = 36256
# The red-hat-enterprise-linux-documentation/rhel-8-docs project
# Note: To determine the project ID, visit a URL such as this for rhel-8-docs:
# https://gitlab.cee.redhat.com/api/v4/projects?search_namespaces=true&search=red-hat-enterprise-linux-documentation/rhel-8-docs
GITLAB_DOC_PROJECT_ID = 9749
JIRA_URL='https://issues.redhat.com'
JIRA_PROJECT='RHELDOCS'

# Jira fields
DEV_REVIEW_FIELD = 'customfield_12322340'
QA_REVIEW_FIELD = 'customfield_12322344'
CCS_REVIEW_FIELD = 'customfield_12322343'
DOC_CONTACT_FIELD = 'customfield_12317360'
CONTRIBUTORS_FIELD = 'customfield_12315950'

# Possible exit codes
RV_DONE = 0
RV_GITLAB_ERROR = 11
RV_GITLAB_NO_PROJECT = 12
RV_NO_JIRA = 21
RV_JIRA_ERROR = 23
RV_JIRA_UPDATE_ERROR = 24

def get_args() -> argparse.Namespace:
    """
    Accept command-line arguments.
    """
    parser = argparse.ArgumentParser(
        prog=NAME,
        description=DESCRIPTION)

    parser.add_argument('-s', '--shaid',
        required=True,
        help="The hash of the current commit.")
    parser.add_argument('-g', '--gitlab_token',
        required=True,
        help="Your token to access Gitlab.")
    parser.add_argument('-j', '--jira_token',
        required=True,
        help="Your token to access Jira.")

    args = parser.parse_args()

    return args

def get_merge_request(shaid: str, url: str, private_token: str) -> tuple[dict, int]:
    """
    Find the merge request ID.
    """
    try:
        gl = gitlab.Gitlab(url, private_token)
        project = gl.projects.get(GITLAB_DOC_PROJECT_ID)
        if project is None:
            sys.exit(RV_GITLAB_NO_PROJECT)
        commit = project.commits.get(shaid)
        mrs = commit.merge_requests()
        if mrs:
            mr = mrs[0]
            mrid = mr['iid']
            if mr['state'] != 'merged':
                logging.info(f"MR {mrid} not merged yet. Exiting with no action.")
                sys.exit(RV_DONE)
            else:
                logging.info(f"Detected MR: {mrid}")
                return mr, mrid
        else:
            logging.info("No MR detected. Exiting with no action.")
            sys.exit(RV_DONE)
    except Exception as e:
        logging.error(f"Gitlab connection failed: {e}")
        sys.exit(RV_GITLAB_ERROR)

def get_jira_names(mr: dict) -> set[str]:
    """
    Find the Jira issue name in the merge request.
    """
    jira_title = re.findall(f'{JIRA_PROJECT}-[0-9]*', mr['title'])
    jira_desc = re.findall(f'[Cc]loses:?\s.*({JIRA_PROJECT}-[0-9]*)', mr['description'])
    all_jiras = jira_title + jira_desc

    # Remove duplicate items
    all_jiras_set = set(all_jiras)

    if len(all_jiras_set) == 0:
        logging.error(f"No {JIRA_PROJECT} Jira found in this MR.")
        sys.exit(RV_NO_JIRA)
    else:
        logging.info(f"Found Jiras: {', '.join(all_jiras_set)}")
        return all_jiras_set

def update_jira_issue(jira_name: str, jira_token: str, mr: dict) -> None:
    """
    Update fields in the Jira issue based on fields from the merge request.
    """
    # Identify fields in the merge request
    dev_review = "Done" if 'SME review' in mr['labels'] else None
    qe_review = "Done" if 'QE review' in mr['labels'] else None
    css_review = "Done" if 'Peer review' in mr['labels'] else None
    reviewers = [x['username'] for x in mr['reviewers']]

    # Get Jira object
    try:
        js = jira.JIRA(server=JIRA_URL, token_auth=jira_token)
        jira_object = js.issue(jira_name)
    except Exception as e:
        logging.error(f"Failed to connect to the Jira issue(s): {e}")
        sys.exit(RV_JIRA_ERROR)

    # Update the Jira issue
    fields = {}
    if dev_review:
        fields[DEV_REVIEW_FIELD] = {'value':dev_review}
        logging.info("ProdDocsReview-Dev Done")
    if qe_review:
        fields[QA_REVIEW_FIELD] = {'value':qe_review}
        logging.info("ProdDocsReview-QE Done")
    if css_review:
        fields[CCS_REVIEW_FIELD] = {'value':css_review}
        logging.info("ProdDocsReview-CCS : Done")
    if len(reviewers) > 0:
        logging.info(f"Reviewers found: {reviewers}")
        # Gitlab identifies users by plain user names, such as `tcapek`.
        # Jira requires the full email address registered for the user.
        reviewer = find_jira_user(reviewers[0], js)
        # Save the reviewers as contributors, following the feedback at
        # https://docs.engineering.redhat.com/display/contentservices/Platform+Docs+Peer+Review+Process?focusedCommentId=416128248
        fields[CONTRIBUTORS_FIELD] = {'name':reviewer}
        logging.info(f"Doc Contact set to {reviewer}")

    try:
        jira_object.update(fields=fields)
    except Exception as e:
        logging.error(f"Failed to update the Jira issue: {e}")
        sys.exit(RV_JIRA_UPDATE_ERROR)

def find_jira_user(user_name: str, js: jira.JIRA) -> str:
    """
    Find a Jira user with the given Kerberos user name.

    Return the email that this user account is registered with in Jira.
    The email might be different from the regular Kerberos email.
    This email serves as the full Jira user name.
    """
    email = user_name + "@redhat.com"
    users = js.search_users(email)

    if len(users) == 0:
        logging.error(f"No Jira user found with email {email}")
        sys.exit(RV_JIRA_ERROR)
    elif len(users) > 1:
        logging.error(f"Several Jira users found with email {email}")
        sys.exit(RV_JIRA_ERROR)
    else:
        user = users[0]

        return user.name

def sync_doc(shaid: str, gitlab_token: str, jira_token: str) -> None:
    """
    Main function.
    """
    mr, mrid = get_merge_request(shaid, GITLAB_URL, gitlab_token)

    jira_names = get_jira_names(mr)

    for jira_name in jira_names:
        update_jira_issue(jira_name, jira_token, mr)

    logging.info(f"MR {mrid} synchronization done.")


if __name__ == '__main__':
    args = get_args()

    # Log messages of level info and more severe.
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

    sync_doc(args.shaid, args.gitlab_token, args.jira_token)

    sys.exit(RV_DONE)
