#!/bin/bash

# list-unused-content.sh - list unused modules and assemblies
# Copyright (C) 2022, 2023 Jaromir Hradilek <jhradilek@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# Load common constants:
. "${0%/*}/lib/constants.sh"
. "${0%/*}/lib/errno.sh"

# General information about the script:
declare -r NAME=${0##*/}
declare -r VERSION='1.0.0'

# Define a temporary file to log error messages to:
declare -r ERROR_OUTPUT=$(mktemp --tmpdir '$NAME.XXXXXX')

# Disable verbose output by default:
declare OPT_VERBOSE=0

# Do not check commented out includes by default:
declare OPT_COMMENTS=0


# Prints an error message to standard error output and terminates the
# script with a selected exit status.
#
# Usage: exit_with_error ERROR_MESSAGE [EXIT_STATUS]
function exit_with_error {
  local -r error_message=${1:-'An unexpected error has occurred.'}
  local -r exit_status=${2:-$EXIT_EPERM}

  # Print the supplied message to standard error output:
  echo -e "$NAME: $error_message" >&2

  # Terminate the script with the selected exit status:
  exit $exit_status
}

# Print an unsorted list of all AsciiDoc files in the repository to standard
# output.
#
# Usage: print_asciidoc_files
function print_asciidoc_files {
  find "$REPO_TOPLEVEL" -not -path "$IGNORE_PATTERN" -type f -name "*.adoc" | sort -u
}

# Print a sorted list of all modules and assemblies in the repository
# to standard output.
#
# Usage: print_fcc_files
function print_fcc_files {
  find "$REPO_TOPLEVEL" -not -path "$IGNORE_PATTERN" -type f -regex "$FCC_PATTERN" | sort -u
}

# Resolve the supplied path and print it to standard output. Report any
# invalid paths to standard error output.
#
# Usage: print_resolved_path INCLUDE_PATH PARENT_FILE PARENT_DIRECTORY
function print_resolved_path {
  local -r include_path="$1"
  local -r parent_file="$2"
  local -r parent_directory="$3"

  # Compose the relative path:
  local relative_path=$(echo "$include_path" | sed -e "s|^|$parent_directory/|")

  # Resolve the path:
  readlink -e "$relative_path"

  # Report invalid paths to standard error output:
  [[ "$?" -gt 0 ]] && readlink -ve "$path" 2>&1 >/dev/null | sed -ne "s|^readlink:[^:]\+:\(.*\)|$parent_file: \1: $include_path|p" >&2
}

# Print an unsorted list of all uncommented includes to standard output and
# capture information about invalid or unresolvable paths in the supplied
# file. Note that this function is unable to detect block comments and will
# therefore incorrectly print them along with the other results.
#
# Usage: print_includes
function print_includes {
  # Process all discovered AsciiDoc files:
  while read file; do
    # Determine the directory the processed file is located in:
    local directory=$(dirname "$file")

    # Extract included paths from the file and print their real paths:
    sed -ne "s|^include::\([^\[]*\)\[.*|\1|p" "$file" | while read path || [[ -n "$path" ]]; do
      print_resolved_path "$path" "$file" "$directory"
    done
  done < <(print_asciidoc_files) 2> "$ERROR_OUTPUT"
}

# Print an unsorted list of all commented includes to standard output and
# capture information about invalid or unresolvable paths in the supplied
# file. Note that this function is unable to detect block comments and will
# therefore not print them along with the other results.
#
# Usage: print_commented_includes
function print_commented_includes {
  # Process all discovered AsciiDoc files:
  while read file; do
    # Determine the directory the processed file is located in:
    local directory=$(dirname "$file")

    # Extract included paths from the file and print their real paths:
    sed -ne "s|^.*// *include::\([^\[]*\)\[.*|\1|p" "$file" | while read path || [[ -n "$path" ]]; do
      print_resolved_path "$path" "$file" "$directory"
    done
  done < <(print_asciidoc_files) 2>> "$ERROR_OUTPUT"
}

# Process command-line options:
while getopts ':chvV' OPTION; do
  case "$OPTION" in
    c)
      # Check files against commented out includes as well:
      OPT_COMMENTS=1
      ;;
    v)
      # Enable verbose output:
      OPT_VERBOSE=1
      ;;
    h)
      # Print usage information to standard output:
      echo "Usage: $NAME [-cv]"
      echo -e "       $NAME -h|V\n"
      echo '  -c   do not report files that have their includes commented out'
      echo '  -v   display additional information about invalid paths'
      echo '  -h   display this help and exit'
      echo '  -V   display version information and exit'

      # Terminate the script:
      exit $EXIT_SUCCESS
      ;;
    V)
      # Print version information to standard output:
      echo "$NAME $VERSION"

      # Terminate the script:
      exit $EXIT_SUCCESS
      ;;
    *)
      # Report an invalid option and terminate the script:
      exit_with_error "Invalid option -- '$OPTARG'" $EXIT_EINVAL
      ;;
  esac
done

# Shift positional parameters:
shift $(($OPTIND - 1))

# Verify the number of supplied command-line arguments:
[[ "$#" -eq 0 ]] || exit_with_error 'Invalid number of arguments' $EXIT_EINVAL

# Get a sorted list of all modules and assemblies:
files=$(print_fcc_files | sort -u)

# Get a sorted list of all includes:
if [[ "$OPT_COMMENTS" -eq 0 ]]; then
  includes=$(print_includes | sort -u)
else
  includes=$( (print_includes; print_commented_includes) | sort -u)
fi

# Determine whether verbose output is requested:
if [[ "$OPT_VERBOSE" -eq 0 ]]; then
  # List modules and assemblies that are present but not on the list of
  # included files:
  comm -23 <(echo "$files") <(echo "$includes")
else
  # List modules and assemblies that are present but not on the list of
  # included files:
  [[ "$OPT_COMMENTS" -eq 0 ]] && echo -e "Unused files:\n" || echo -e "Unused files (including commented out includes):\n"
  comm -23 <(echo "$files") <(echo "$includes") | sed -e 's|^|  |'

  # Print information about unresolved paths if there are any:
  echo -e "\nUnresolved or invalid includes:\n"
  cat "$ERROR_OUTPUT" | sort -u | sed -e 's|^|  |'
fi

# Delete the temporary file:
[[ -e "$ERROR_OUTPUT" ]] && rm -f "$ERROR_OUTPUT"
