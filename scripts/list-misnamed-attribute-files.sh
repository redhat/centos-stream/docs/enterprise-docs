#!/bin/bash

# list-misnamed-attribute-files.sh - list wrongly named attribute files
# Copyright (C) 2021 Jaromir Hradilek <jhradilek@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# Load common constants:
. "${0%/*}/lib/constants.sh"

# Locate all attribute files that do not have an underscore in front of
# their name:
find "$REPO_TOPLEVEL" -not -path '*/\.*' -type f -not -regex "$FCC_PATTERN" -iregex '.*/[^_][^/]*attributes[^/]*\.adoc'
