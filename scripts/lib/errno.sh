#!/bin/bash

# errno.sh - define constants for standard exit codes
# Copyright (C) 2023 Jaromir Hradilek <jhradilek@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# NOTE:  This file  is a direct re-implementation  of constants  defined in
#        /usr/include/asm-generic/errno-base.h

# Standard exit codes:
declare -r EXIT_EPERM=1      # Operation not permitted
declare -r EXIT_ENOENT=2     # No such file or directory
declare -r EXIT_ESRCH=3      # No such process
declare -r EXIT_EINTR=4      # Interrupted system call
declare -r EXIT_EIO=5        # I/O error
declare -r EXIT_ENXIO=6      # No such device or address
declare -r EXIT_E2BIG=7      # Argument list too long
declare -r EXIT_ENOEXEC=8    # Exec format error
declare -r EXIT_EBADF=9      # Bad file number
declare -r EXIT_ECHILD=10    # No child processes
declare -r EXIT_EAGAIN=11    # Try again
declare -r EXIT_ENOMEM=12    # Out of memory
declare -r EXIT_EACCES=13    # Permission denied
declare -r EXIT_EFAULT=14    # Bad address
declare -r EXIT_ENOTBLK=15   # Block device required
declare -r EXIT_EBUSY=16     # Device or resource busy
declare -r EXIT_EEXIST=17    # File exists
declare -r EXIT_EXDEV=18     # Cross-device link
declare -r EXIT_ENODEV=19    # No such device
declare -r EXIT_ENOTDIR=20   # Not a directory
declare -r EXIT_EISDIR=21    # Is a directory
declare -r EXIT_EINVAL=22    # Invalid argument
declare -r EXIT_ENFILE=23    # File table overflow
declare -r EXIT_EMFILE=24    # Too many open files
declare -r EXIT_ENOTTY=25    # Not a typewriter
declare -r EXIT_ETXTBSY=26   # Text file busy
declare -r EXIT_EFBIG=27     # File too large
declare -r EXIT_ENOSPC=28    # No space left on device
declare -r EXIT_ESPIPE=29    # Illegal seek
declare -r EXIT_EROFS=30     # Read-only file system
declare -r EXIT_EMLINK=31    # Too many links
declare -r EXIT_EPIPE=32     # Broken pipe
declare -r EXIT_EDOM=33      # Math argument out of domain of func
declare -r EXIT_ERANGE=34    # Math result not representable

# Exit code for successful operation:
declare -r EXIT_SUCCESS=0
