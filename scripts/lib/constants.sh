#!/bin/bash

# constants.sh - define common constants for repository validation scripts
# Copyright (C) 2021 Jaromir Hradilek <jhradilek@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# Get the top-level directory of the Git working tree:
declare -r REPO_TOPLEVEL=$(git rev-parse --path-format=absolute --show-toplevel)

# Define patterns to match different content types:
declare -r FCC_PATTERN='.*/\(con\|proc\|ref\|assembly\)_[^/]*\.adoc'
declare -r MODULE_PATTERN='.*/\(con\|proc\|ref\)_[^/]*\.adoc'
declare -r ASSEMBLY_PATTERN='.*/assembly_[^/]*\.adoc'
declare -r IMAGE_PATTERN='.*/[^/]*\.\(pn\|jp\|sv\)g'

# Define the pattern for content to ignore:
declare -r IGNORE_PATTERN='*/\.*'

# Define the directory names for different content types:
declare -r MODULE_DIR='modules'
declare -r ASSEMBLY_DIR='assemblies'
declare -r IMAGE_DIR='images'
