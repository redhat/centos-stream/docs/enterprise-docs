#!/bin/bash

# list-suspicious-emails.sh - list emails with non-Red Hat addresses
# Copyright (C) 2023 Jaromir Hradilek <jhradilek@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# Load common constants:
. "${0%/*}/lib/errno.sh"

# General information about the script:
declare -r NAME=${0##*/}
declare -r VERSION='1.0.0'

# Disable verbose output by default:
declare OPT_VERBOSE=0

# Disable the table header by default:
declare OPT_HEADER=0


# Prints an error message to standard error output and terminates the
# script with a selected exit status.
#
# Usage: exit_with_error ERROR_MESSAGE [EXIT_STATUS]
function exit_with_error {
  local -r error_message=${1:-'An unexpected error has occurred.'}
  local -r exit_status=${2:-$EXIT_EPERM}

  # Print the supplied message to standard error output:
  echo -e "$NAME: $error_message" >&2

  # Terminate the script with the selected exit status:
  exit $exit_status
}

# Prints a sorted list of non-Red Hat commit emails along with the relative
# date of the latest commit to standard output.
#
# Usage: print_emails
function print_emails {
  # Print the table header if requested:
  [[ "$OPT_HEADER" -eq 1 ]] && echo -e "EMAIL:LAST COMMIT\n-----:-----------"

  # Print the list of emails:
  if [[ "$OPT_RECENT" -eq 0 ]]; then
    git log --pretty="%ae:%cr" | grep -v '@redhat.com:' | sort -u -t ':' -k 1,1
  else
    git log --pretty="%ae:%cr" --after=$(date --date="3 months ago" +%F) | grep -v '@redhat.com:' | sort -u -t ':' -k 1,1
  fi
}


# Process command-line options:
while getopts ':rhHV' OPTION; do
  case "$OPTION" in
    r)
      # Select to print information for the last three months only:
      OPT_RECENT=1
      ;;

    H)
      # Select to print the optional table header:
      OPT_HEADER=1
      ;;
    h)
      # Print usage information to standard output:
      echo "Usage: $NAME [-a]"
      echo -e "       $NAME -h|V\n"
      echo '  -r   limit the output to commits from the last three months'
      echo '  -H   print the table header'
      echo '  -h   display this help and exit'
      echo '  -V   display version information and exit'

      # Terminate the script:
      exit $EXIT_SUCCESS
      ;;
    V)
      # Print version information to standard output:
      echo "$NAME $VERSION"

      # Terminate the script:
      exit $EXIT_SUCCESS
      ;;
    *)
      # Report an invalid option and terminate the script:
      exit_with_error "Invalid option -- '$OPTARG'" $EXIT_EINVAL
      ;;
  esac
done

# Shift positional parameters:
shift $(($OPTIND - 1))

# Verify the number of supplied command-line arguments:
[[ "$#" -eq 0 ]] || exit_with_error 'Invalid number of arguments' $EXIT_EINVAL

# Print the list of non-Red Hat commit emails for the selected time period:
print_emails | column -s: -t
