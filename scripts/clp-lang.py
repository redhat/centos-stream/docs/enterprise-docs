#!/usr/bin/env python3

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import re
import argparse
import csv
import subprocess

# Do not check ALLOWED_FILE_NAMES for CLP words (file content is still checked)
ALLOWED_FILE_NAMES = ["master.adoc"]
ALLOWED_EXTENSIONS = [".adoc", ".asciidoc"]
# Globals defined in main()
global RUN_ALL_FLAG 

def load_pattern(file):
    """ Creates a regex pattern from the phrases from the file
    @file: path (str) to the file, containing each phrase on one row
    @returns: a regex (str) that will match all the phrases from the file
    """
    with open(file, "r") as file:
        words = [re.escape(word.strip()) for word in file.readlines()]
    return r"(" + "|".join(words) + r")"

def get_matches(text, pattern):
    """ Check if a string matches regex pattern
    @text: str
    @pattern: regex pattern (str)
    @returns: list of containing all matched words
    """
    return re.findall(pattern, text, re.IGNORECASE)

def get_unwanted(dir, pattern):
    """ Top-down approach, start in the initial dir and traverse down to contained dirs/files,
        check each dir/file name and file contents for a match with the pattern
    @dir: a directory to be recursively searched
    @pattern
    @returns: paths: list of file/dir paths containing unwanted phrases in the file/dir NAME, filtered based on pattern
              contents: list of file names containing unwanted phrases in the file CONTENT, filtered based on pattern
    """
    paths = []
    content = []

    for root, dirs, files in os.walk(dir):
        # Ignore files and directories in the .git/ directory.
        relative = os.path.relpath(root)
        if ".git/" in relative:
            continue

        # Check file names and contents
        for file in files:
            path = os.path.join(root, file)
            # File names marked as ALLOWED_FILE_NAMES, don't need to be checked for pattern match (skip).
            if RUN_ALL_FLAG or file not in ALLOWED_FILE_NAMES:
                if get_matches(file, pattern):
                        paths.append((path, get_matches(file, pattern)))
            # Check all files contents regardless of file name.
            matches = []
            for allowed_ext in ALLOWED_EXTENSIONS:
                if file.endswith(allowed_ext):
                    try:
                        #Get the stdout printed by the PresciiDoc, to feed into the check_file_contents function
                        prescii = subprocess.run(["presciidoc", "-c", path], stdout=subprocess.PIPE)
                        prescii_output =prescii.stdout
                        lines = prescii_output.splitlines()
                        for line_no, byte_line in enumerate(lines):
                            line = byte_line.decode('utf-8')
                            if get_matches(line, pattern):
                                matches.append((line_no + 1, get_matches(line, pattern), line))
                    except OSError:
                        print("Missing tool: Presciidoc (https://github.com/msuchane/presciidoc).")
                        quit()
                    content.append((path, matches))
    
        # Check dir names
        for dir in dirs:
            if get_matches(dir, pattern):
                path = os.path.join(root, dir)
                paths.append((path, get_matches(dir, pattern)))

    return paths, content
   
if __name__ == "__main__":
    # Run in pwd
    pwd = os.getcwd()
    script_dir = os.path.dirname(os.path.abspath(__file__))
    parser = argparse.ArgumentParser() 

    # Run in given directory
    parser.add_argument(
        '-d',
        '--directory',
        default=pwd,
        help='Directory to run the script on')
    file_names = ", ".join(ALLOWED_FILE_NAMES)
    parser.add_argument(
        '-a',
        '--all',
        default=False,
        action="store_true",
        help=f'Run script for all file names, including {file_names}')
    args = parser.parse_args()

    #set global variables
    RUN_ALL_FLAG = args.all

    # Hardwired file with blocklist phrases
    pattern_file = os.path.join(script_dir, "blocklist.txt")
    pattern = load_pattern(pattern_file)
    dir = args.directory

    if not os.path.isdir(args.directory):
        print("Invalid directory path.")
    else:
        unwanted_paths, unwanted_content = get_unwanted(dir, pattern)
        if unwanted_paths or unwanted_content:
            result_file = 'unwanted.csv'
            header = ['File/dir name', 'Row number', 'Unwanted phrase in the file/dir CONTENTS', 'Row']
            # Using the 'w' option, @result_file is always rewritten
            with open(result_file, 'w', newline='') as file: 
                writer = csv.writer(file)
                writer.writerow(header)
                for name, phrases in unwanted_paths:
                    for phrase in phrases:
                        writer.writerow([name,'-', phrase])
                for name, phrases in unwanted_content:
                    for row_number, phrase, line in phrases:
                        writer.writerow([name, row_number, (', '.join(phrase)), line.strip()])
            print("Unwanted phrases saved in " + result_file)
        else:
            print("No files found with phrases from file " + pattern_file + " in the name.")
