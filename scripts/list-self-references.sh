#!/bin/bash

# list-self-references.sh - list occurrences of self-referential text
# Copyright (C) 2022 Jaromir Hradilek <jhradilek@redhat.com>
# Copyright (C) 2023 Marek Suchánek <msuchane@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# Load common constants:
. "${0%/*}/lib/constants.sh"

# Check that the presciidoc dependency is installed:
if ! command -v presciidoc; then
    echo "Error: Install the presciidoc utility: https://github.com/msuchane/presciidoc"
    exit 1
fi

# Print all occurrences of self-referential text in AsciiDoc files:

# Find all files.
files=$(find "$REPO_TOPLEVEL" -not -path '*/\.*' -type f -regex "$FCC_PATTERN")

# Print a CSV header.
echo "File,Issues"

# Analyze files one by one.
for path in $files; do
    # Remove line and block comments, then search for self-referential expressions.
    issues=$(presciidoc -c "$path" | grep -ione "\(these\|this\|following\) \(topic\|module\|assembly\|chapter\|section\|subsection\|procedure\)")

    # See how many issues this file has.
    issues_length=${#issues}

    # If there's at least one issue, print the findings.
    if [ $issues_length -gt 0 ]; then
	# Convert the file path from absolute to relative to the current working directory.
	relative_path=$(realpath --relative-to=. $path)
	# Print a CSV row. It might contain several different issues.
        echo \"$relative_path\",\"$issues\"
    fi
done

