#/bin/bash

# mass-ct10.sh - Wrapper for ct10.sh to process multiple files at once
# Copyright (C) 2024 Marc Muehlfeld <mmuehlfeld@redhat.com>

# READ THE WARNING BELOW BEFORE YOU USE THIS SCRIPT!

# Usage: mass-ct10.sh <list_of_file> <destination_directory>
#
# Examples:
#	mass-ct10.sh modules/file_1.adoc modules/file_2.adoc modules/file_n.adoc ... /path/to/destination/directory/
#
#	It also works with wildcards:
#       mass-ct10.sh modules/core-services/* /path/to/destination/directory/
#
# The last parameter must be the destination directory.

######################
# WARNING:
# This script doesn't prevent you from manually checking the results for each
# file that ct10.sh processes (and fixing things, improving the content,...)!
#
# Additionally, the more files you process at once, the harder the debugging!
#
# For cleanup and simplification reasons, ct10.sh uses the module's title as
# file name and ID. Additionally, file names no longer have prefixes and IDs
# have no context attribute. Consequently, at least xrefs and includes need
# to be adjusted because, otherwise, your guide fails to build.
#
# The more files you process at once, the higher the risk that you waste much
# more time with debugging than the time you wanted to save with this script!
######################


DEST=${!#}

if [ ! -d "$DEST" ] ; then
	echo "Last parameter isn't a directory."
	exit 1
fi

for FILE in "${@:1:$#-1}"; do
    ct10.sh "$FILE" "$DEST"
    echo "-------------------------------------------------------"
done

