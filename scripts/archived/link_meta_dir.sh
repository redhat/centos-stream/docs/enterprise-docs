#!/bin/bash
# Ensure that run this script inside the parent directory containing subdirectories into which you want to place the symlinks to the `common-content/` directory.
# Ensure that you observe the relatibve path to the script file.

# Path to the `common-content` directory for product docs.
# For upstream docs, the directory name is: `../community/common-content`
META_DIR="../enterprise/common-content"

# Get the path of your current working directory
DIR=${pwd}

# Display message for user
echo "Creating symlinks for all subdirectories in ${DIR}"

# Get names of all subdirectories within your current working direcotry.
SUBDIRS=$(ls ${DIR})

# Create symlinks to the `common-content/` directory inside each subdirectory of your current working directory
for name in $SUBDIRS
do
# Display message for user	
    echo "Creating symlink for ${META_DIR} in $name"
# Create symlink
    ln -s ${META_DIR} ${name}
done

exit 0
