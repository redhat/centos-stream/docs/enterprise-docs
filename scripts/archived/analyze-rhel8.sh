#! /bin/bash

# Vladimir Slavik
# Red Hat, 2019

# you must be in the repo scripts level directory at start

# edit this to configure the script
use_atree="1"
remove_files="1"
atree_path="~/Documentation/tools/atree/atree"




# from jhradilek check-links
# Usage: print_includes FILE
function print_includes {
  local -r filename="$1"

  # Parse the AsciiDoc file, get a complete list of included files, and
  # print their full paths to standard output:
  ruby <<-EOF 2>/dev/null
#!/usr/bin/env ruby

require 'asciidoctor'

document = Asciidoctor.load_file("$filename", doctype: :book, safe: :safe)
document.reader.includes.each { |filename|
  dirname  = File.dirname("$filename")
  fullpath = File.join(dirname, "#{filename}.adoc")
  puts File.realpath(fullpath)
}
EOF

  [[ "$?" -eq 0 ]] || echo "$filename: Unable to list included files" 1>&2
}


scriptdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $scriptdir/..

if [[ "$use_atree" == "0" ]]; then
  echo "using asciidoctor - statistics will be skewed towards lower reuse"
fi

rm -f 1-include-listing.txt
echo "Listing included files..."
# list all currently included files (with atree/python or asciidoctor/ruby)
for file in $(ls -1 enterprise/titles/*/*/master.adoc); do
  if [[ "$use_atree" == "1" ]]; then
    atree_run="$atree_path -b -p -h $file >> 1-include-listing.txt"
    eval "$atree_run"
  else
    realpath "$file" >> 1-include-listing.txt
    print_includes "$file" >> 1-include-listing.txt
  fi
done
echo "listing done."

# list all adoc files
searchpath=`pwd`"/enterprise/"
find $searchpath -name \*.adoc -print > 2-find-adoc-all.txt

# concatenate the lists = every file is added at least once
cat 1-include-listing.txt > 3-merged-lists.txt
cat 2-find-adoc-all.txt >> 3-merged-lists.txt
if [ $remove_files -eq 1 ]; then
  rm 1-include-listing.txt
  rm 2-find-adoc-all.txt
fi

# remove empty lines and path prefix
escpath=$(echo $searchpath | sed 's_/_\\/_g')
cat 3-merged-lists.txt | sed -e '/^[[:space:]]*$/d' -e "s/$escpath//" > 4-merged-treated.txt
if [ $remove_files -eq 1 ]; then
  rm 3-merged-lists.txt
fi

# turn the list with repeated occurrences into a list with counts
cat 4-merged-treated.txt | sort | uniq -c | sort -nr > 5-all-enumerated.txt
if [ $remove_files -eq 1 ]; then
  rm 4-merged-treated.txt
fi

# reduce the number of occurrences by 1 to compensate for listing all files earlier
awk -F" " '{printf("%d %s\n",$1-1,$2)}' 5-all-enumerated.txt > 6-all-adjusted.txt
if [ $remove_files -eq 1 ]; then
  rm 5-all-enumerated.txt
fi

# finally analyze the numbers and print the outputs

assy0=$(cat 6-all-adjusted.txt | grep "0 assemblies\\/assembly" | wc -l)
assy1=$(cat 6-all-adjusted.txt | grep "1 assemblies\\/assembly" | wc -l)
assyA=$(cat 6-all-adjusted.txt | grep "assemblies\\/assembly" | wc -l)
let "assyR = $assyA - $assy1 - $assy0"
echo "Assemblies"
echo "  unused: $assy0"
echo "  used:   $assy1"
echo "  reused: $assyR"
echo "  total:  $assyA"

mod0=$(cat 6-all-adjusted.txt | grep "0 modules" | wc -l)
mod1=$(cat 6-all-adjusted.txt | grep "1 modules" | wc -l)
modA=$(cat 6-all-adjusted.txt | grep "modules" | wc -l)
let "modR = $modA - $mod1 - $mod0"
echo "Modules"
echo "  unused: $mod0"
echo "  used:   $mod1"
echo "  reused: $modR"
echo "  total:  $modA"

titles=$(cat 6-all-adjusted.txt | grep "master\\.adoc" | wc -l)
echo "Titles"
echo "  total:  $titles"

pngimgs=$(find $searchpath/images/ -name \*.\* -print | grep png | wc -l)
xcfimgs=$(find $searchpath/images/ -name \*.\* -print | grep xcf | wc -l)
jpgimgs=$(find $searchpath/images/ -name \*.\* -print | grep jpg | wc -l)
svgimgs=$(find $searchpath/images/ -name \*.\* -print | grep svg | wc -l)
let "numimages = $pngimgs + $jpgimgs + $svgimgs"
echo "Images"
echo "  png:    $pngimgs"
echo "  jpg:    $jpgimgs"
echo "  svg:    $svgimgs"
echo "  total:  $numimages"
echo "  (and also $xcfimgs of xcf sources)"

