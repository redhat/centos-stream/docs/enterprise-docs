#!/bin/bash

# list-misplaced-files.sh - list files that do not belong to the repository
# Copyright (C) 2021 Jaromir Hradilek <jhradilek@redhat.com>

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.


# Load common constants:
. "${0%/*}/lib/constants.sh"

# Define disallowed directories:
declare -ra DISALLOWED_DIRS=('enterprise')

# Locate all files in the directory with modules that are not modules:
find "$REPO_TOPLEVEL" -not -path '*/\.*' -type d -name "$MODULE_DIR" -exec find {} -not -path '*/\.*' -type f -not -regex "$MODULE_PATTERN" \;

# Locate all files in the directory with assemblies that are not assemblies:
find "$REPO_TOPLEVEL" -not -path '*/\.*' -type d -name "$ASSEMBLY_DIR" -exec find {} -not -path '*/\.*' -type f -not -regex "$ASSEMBLY_PATTERN" \;

# Locate all files in the directory with images that are not images:
find "$REPO_TOPLEVEL" -not -path '*/\.*' -type d -name "$IMAGE_DIR" -exec find {} -not -path '*/\.*' -type f -not -iregex "$IMAGE_PATTERN" \;

# Locate all files in disallowed directories:
for directory in ${DISALLOWED_DIRS[@]}; do
  [[ -d "$REPO_TOPLEVEL/$directory" ]] && find "$REPO_TOPLEVEL/$directory" -not -path '*/\.*' -not -type d
done
